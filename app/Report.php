<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Date;

/**
 * App\Report
 * @property int $id_report
 * @property int $id_mission
 * @property int|null $id_team_config
 * @property string|null $start_datetime
 * @property string|null $end_datetime
 * @property string $status
 * @property int $initialized
 * @property string $team_name
 * @property int $allow_msg_teacher
 * @property int $allow_msg_team
 * @property int $allow_msg_id_class
 * @property int $allow_msg_id_mission
 * @property int $allow_attach_ld
 * @property int $allow_save_ld
 * @property int $allow_import
 * @property int $allow_import_id_mission
 * @property string $creation_time
 * @property string|null $update_time
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Conversation[] $conversations
 * @property-read int|null $conversations_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Labdoc[] $labdocs
 * @property-read int|null $labdocs_count
 * @property-read \App\Mission $mission
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ReportPart[] $reportParts
 * @property-read int|null $report_parts_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Resource[] $resources
 * @property-read int|null $resources_count
 * @property-read \App\TeamConfig|null $teamConfig
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report whereCreationTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report whereEndDatetime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report whereIdMission($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report whereIdReport($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report whereIdTeamConfig($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report whereImportLd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report whereInitialized($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report whereSaveLd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report whereSendClass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report whereSendLd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report whereSendMission($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report whereSendTeacher($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report whereSendTeam($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report whereStartDatetime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report whereTeamName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report whereUpdateTime($value)
 * @mixin \Eloquent
 * @property \Illuminate\Support\Carbon|null $delete_time deletion time
 * @method static \Illuminate\Database\Query\Builder|\App\Report onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report whereDeleteTime($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Report withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Report withoutTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report whereAllowAttachLd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report whereAllowImportIdMission($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report whereAllowMsgIdClass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report whereAllowMsgIdMission($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report whereAllowMsgTeacher($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report whereAllowMsgTeam($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Report whereAllowSaveLd($value)
 */
class Report extends Model
{
    use SoftDeletes;
    
    protected $table = 'report';
    protected $primaryKey = 'id_report';
    const CREATED_AT = 'creation_time';
    const UPDATED_AT = 'update_time';
    const DELETED_AT = 'delete_time';

    // Status possible for a report
    public const NEW = "new"; // after the creation, students not started to work
    public const ON = "on"; // Students work on the report 
    public const WAIT = "wait"; // Report is submitted
    public const ARC = "arc";  // Archived report
    public const TEST = "test"; 
    public const SOLUTION = "solution"; 
    public const TUTO = "tuto"; // Report from tutorial mission
    
    public const SORTING_STATUS = [self::NEW, self::ON, self::WAIT, self::ARC, self::TEST, self::SOLUTION];

    public $timestamps = false;

    public $fillable = ['id_mission', 'team_name', 'status', 'start_datetime',
        'end_datetime', 'allow_msg_teacher', 'allow_msg_team', 'allow_msg_id_class', 'allow_msg_id_mission',
        'allow_attach_ld', 'allow_save_ld', 'allow_import' , 'allow_import_id_mission'];

    /**
     * Returns the learners of the report
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\User', 'link_report_learner', 'id_report', 'id_user')->orderBy('user_name');
    }

    /**
     * Changes the learners of the report
     */
    public function updateLearners ($learners)
    {
        $changes = $this->users()->sync($learners);
        // Unset current_report if required
        \App\User::whereIn('id_user', $changes['detached'])
            ->where('current_report', $this->id_report)
            ->update(['current_report' => null]);
        return $changes;

    }

    /**
     * Returns the teachers of the report
     * @param boolean $strict restrict to teamings in my classes
     * @return \Illuminate\Database\Eloquent\Query\Builder
     */
    public function teachers($strict = false)
    {
        // All teachers of this report's mission with their classes
        $query = \App\Teacher::select('teacher.*')
            ->distinct()
            ->join('link_mission_teacher as lmt', 'lmt.id_teacher', 'teacher.id_teacher')
            ->join('link_class_teacher as lct', 'lct.id_teacher', 'teacher.id_teacher')
            ->where('lmt.id_mission', $this->id_mission);
        // Teacher linked to both report class and mission
        $tc_query = (clone $query)
            ->join('team_config', 'lct.id_class', '=', 'team_config.id_class')
            ->where('id_team_config', $this->id_team_config);
        if ($strict) {
            return $tc_query;
        }
        // All teachers linked to the report's mission and at least one of its students
        $queryByMission = (clone $query)
            ->join('link_class_learner as lcl', 'lcl.id_class', 'lct.id_class')
            ->join('link_report_learner as lrl', 'lcl.id_user', 'lrl.id_user')
            ->where('lrl.id_report', $this->id_report);
        // We recreate a select query encapsulating the union so filters added laters
        // will apply on both parts
        return \App\Teacher::fromSub($tc_query->union($queryByMission), 'teacher');
    }

    /**
     * Returns the ids of the connected teachers to the report
     * @return \Illuminate\Database\Eloquent\Query\Builder
     */
    public function connectedTeachers()
    {
        $time = time() -  1.4 * config('labnbook.refresh_period');
        return DB::table('teacher_report_status')
            ->where('id_report', $this->id_report)
            ->where('last_synchro', '>=', $time)
            ->get();
    }

    /**
     * Is $id_user a member of this report
     * @param in $id_user
     * @return bool
     */
    public function isMember($id_user)
    {
        return $this->users()->whereKey($id_user)->exists();
    }

    /**
     * Returns the report of the given user and teamconfig if any
     * @param int $id_user
     * @param int $id_team_config
     * @return \App\Report
     */
    public static function fromUserAndTeamConfig($id_user, $id_team_config)
    {
        return self::where('id_team_config', $id_team_config)
            ->whereHas('users', function ($query) use ($id_user) {
                $query->where('user.id_user', $id_user);
            })->first();
    }

    /**
     * Returns the mission of the report
     * @return \App\Mission
     */
    public function mission()
    {
        return $this->belongsTo('App\Mission', 'id_mission');
    }


    /**
     * Returns the reportPart of the report
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function reportParts()
    {
        return $this->hasManyThrough('App\ReportPart', 'App\Mission', 'id_mission', 'id_mission', 'id_mission');
    }

    /**
     * Returns the labdocs of the report
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function labdocs()
    {
        return $this->hasMany('App\Labdoc', 'id_report');
    }

    /**
     * Returns labdoc shared for the report
     * @return \Illuminate\Database\Eloquent\Relations\hasManyThrough
     */
    public function sharedLabdocs()
    {
        return $this->hasManyThrough('App\Labdoc', 'App\TeamConfig', 'id_team_config', 'id_team_config', 'id_team_config');
    }

    /**
     * Returns the conversations of the report
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function conversations()
    {
        return $this->hasMany('App\Conversation', 'id_report');
    }

    /**
     * Returns the resources of the report
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function resources()
    {
        return $this->hasMany('App\Resource', 'id_report');
    }

    /**
     * Returns the resources of the report
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function tasks()
    {
        return $this->hasMany('App\Task', 'id_report');
    }

    /**
     * Get the team_config of the report
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function teamConfig()
    {
        return $this->belongsTo('App\TeamConfig', 'id_team_config');
    }

    /**
     * Get the report's assesment
     * @return \App\Assesment|null
     */
    public function assesment()
    {
        return \App\Scoring\Assesment::find($this->id_report);
    }


    /**
     * Get the last visit of the learner
     * @param int $id_user
     * @param boolean $is_follow
     * @return int|null
     */
    public function getLastVisit($id_user, $is_follow=false)
    {
        $table = $is_follow ? 'teacher_report_status' : 'link_report_learner';
        $id_user_col = $is_follow ?  'id_teacher' : 'id_user';
        return DB::table($table)
            ->Where($id_user_col, $id_user)
            ->Where('id_report', $this->id_report)
            ->pluck('last_synchro')
            ->first();
    }

    /**
     * Returns the unread comments of the report for a user
     * @param int id_user
     * @return \Illuminate\Support\Collection|\App\Comment[]
     */
    public function unreadComments($id_user)
    {
        return DB::table('comment')
            ->join("labdoc", "comment.id_labdoc", "=", "labdoc.id_labdoc")
            ->join("labdoc_status", "comment.id_labdoc", "=", "labdoc_status.id_labdoc")
            ->where(function ($query) {
                $query->where("labdoc.id_report", "=", $this->id_report)
                    ->orWhereNotNull("labdoc.id_team_config")
                    ->where("labdoc.id_team_config", "=", $this->id_team_config);
            })
            ->where("labdoc_status.id_user", "=", $id_user)
            ->where("comment.id_user", "<>", $id_user)
            ->whereNull("labdoc.deleted")
            ->where(function ($query) {
                $query->whereRaw("comment_time > com_viewed")
                    ->orWhereNull("com_viewed");
            });
    }

    /**
     * Return the translated status name
     * @param string $status
     * @return string
     */
    public static function statusName($status)
    {
        switch ($status) {
            case self::WAIT :
                return __('rendu');
            case self::ON :
                return __('en cours');
            case self::NEW:
                return __('nouveau');
            case self::ARC :
                return __('archivé');
            case self::TEST :
                return __('test');
            case self::SOLUTION :
                return __('solution');
            case self::TUTO :
                return __('tutoriel');
        }
    }

    /**
     * For scoring tool, return the teamName
     */
    public function getTeamName()
    {
        return $this->team_name;
    }

    /**
     * Return all the labdoc at the right format for displaying them
     * @Return array
     */
    public function fetchAllLabdocs(){
        $labdocs = [];
        foreach($this->labdocs()->get() as $ld){
            $labdocs[$ld->id_labdoc] = $ld->getDisplayData();
        }
        foreach($this->sharedLabdocs()->get() as $ld){
            $labdocs[$ld->id_labdoc] = $ld->getDisplayData();
        }
        return $labdocs;
    }

    /**
     * Return the report's structure with contextual data (report part's names and labdoc's name)
     * @Return array
     */
    public function getStructure(){
        return $this
            ->labdocs()
            ->get()
            ->filter(fn($labdoc) => (is_null($labdoc->deleted) && !is_null($labdoc->position)))
            ->map(function($labdoc) {
                if(isset($labdoc->reportPart)){
                    $title = $labdoc->reportPart->title;
                    $rpPosition = $labdoc->reportPart->position;
                } else {
                    $title = "...";
                    $rpPosition = '0';
                };
                return array(
                    'id_labdoc' => $labdoc->id_labdoc,
                    'labdoc_name' => $labdoc->name,
                    'labdoc_position' => $labdoc->position,
                    'labdoc_type' => $labdoc->type_labdoc,
                    'report_part_name' => $title,
                    'report_part_position' => $rpPosition,
                );
            })
            ->sortBy([['report_part_position', 'asc'],['labdoc_position', 'asc']]);
    } 
    
    
    /**
     * Returns the maximum position of a LD for a given report
     * @param int $id_report_part
     * @return int position
     */
    public function maxLdPos($id_report_part)
    {
        $query = DB::table('labdoc');
        if ($id_report_part) {
            $query->where('id_report_part', $id_report_part);
        } else {
            $query->whereNull('id_report_part');
        }
        return $query->where('id_report', $this->id_report)
            ->whereNull('deleted')
            ->max('position');
    }

    /**
     * Adds or duplicate a ld if $id_ld_dup is defined
     * Returns the newly created labdoc's id
     * @param string $name : name of the ld
     * @param string $type : type
     * @param int $id_ld_dup : id of the ld to be duplicated
     * @param boolean $edition : the ld must be in edition or display mode
     * @param int $id_rp : id of the rp
     * @param int $id_user : id of the user manipulating ld
     * @return int
     */
    public function addLD($name, $type, $id_ld_dup, $edition, $id_rp, $id_user):int
    {
        $id_rp = !empty($id_rp) ? (int) $id_rp : null;

        // champs ld_data
        if ($id_ld_dup) { // on est dans le cas d'une duplication : on recupere le contenu
            $ld_dup = Labdoc::find($id_ld_dup);
            $ld_data = $ld_dup->labdoc_data;
        } elseif ($type === 'dataset') {
            $ld_data = file_get_contents(public_path().'/tool_fitex/default-dataset.xml');
        } elseif ($type === 'procedure') {
            $ld_data = file_get_contents(public_path().'/tool_copex/v2_json/default-protocol.json');
        } else if ($type === 'code') {
            $ld_data = file_get_contents(public_path().'/jupyter/rw/files/new.ipynb');
        } else {
            $ld_data = '';
        }

        // calcul de la position
        $maxPosition = $this->maxLdPos($id_rp);
  
        if ($maxPosition) {
            $pos = $maxPosition + 1;
        } else {
            $pos = 1;
        }

        // Création du LD dans la table
        $lock = $edition ? time() : null;
        $id_report = $this->id_report;
        $ld_args = [
            'id_report_part'=> $id_rp,
            'id_report'=> $id_report,
            'type_labdoc'=> $type,
            'name'=> $name,
            'position'=> $pos,
            'locked'=> $lock,
            'last_editor'=> $id_user,
            'labdoc_data'=> $ld_data,
        ];
        $ld = Labdoc::init($this, $ld_args);
        $id_ld = $ld->id_labdoc;
 
        // TODO : review this code logTracks
        if ($ld->report->status == self::ON) { // ne pas tracer en mode test
            $traceId = null;
            $args = ['id_report' => $id_report, 'id_mission' => $ld->report->id_mission, 'id_labdoc' => $id_ld, 'type' => $type ];
            if ($id_ld_dup) {
                $args['ld_dup'] = (int) $id_ld_dup;
                if ($ld_dup->id_report != $id_report) { // cas d'un import depuis un autre rapport
                    $args['from_report']=$ld_dup->id_report;
                    if ($id_rp != null) {
                        $trace = Trace::IMPORT_LD;
                    } else {
                        $trace = Trace::SAVE_ATTACHED_LD;
                    }
                } else {
                    $trace = Trace::DUPLICATE_LD;
                }
            } else {
                $trace = Trace::ADD_LD;
                $args['id_rp'] = $id_rp > 0 ? (int) $id_rp : null;
            }
            $traceId = Trace::logAction($trace, $args);
            if ($traceId && $ld->id_ld_origin) {
                try {
                    $versioning = new LabdocVersioning($ld->report->id_mission);
                    $versioning->save($ld, $traceId, $id_user);
                    $ld->last_id_version = $traceId;
                    $ld->save();
                    $ld->storeLastIdVersionReviewed($ld->last_id_version, $id_user);
                } catch (\Exception $e) {
                    Log::error($e->getMessage());
                }
            }
        }
        return $ld->id_labdoc;
    }

    /**
     * duplique le LD en fin de rp.
     * creates the new name of the duplicata and then call addLD()
     * Returns the newly created labdoc's id
     *
     * @param \App\Labodc the ld to be duplicated
     * @param bool $edition the ld must be in edition or display mode
     * @param int $id_rp
     * @param bool $incr_name
     * @param int $id_user : id of the user manipulating ld
     * @return int
     */
    public function duplicateLD($ld, $edition, $id_rp, $incr_name, $id_user):int
    {
        // récupération du type et création du nom
        $name = $ld->getNextCopyName($this->id_report, $incr_name);
        return $this->addLD($name, $ld->type_labdoc, $ld->id_labdoc, (boolean) $edition, $id_rp, $id_user);
    }

    /**
     * supprime un LD (si possible)
     *
     * @param int $id_ld
     * @param int $id_user
     * @return array
     */
    public function deleteLD($id_ld, $id_user)
    {
        $ans = [];
        $ld = \App\Labdoc::find($id_ld);
        if (!$ld) {
            return [
                'status' => 'deleted',
            ];
        }
        // si la suppression n'est pas possible
        if ($ld->getUsabilityStatus($id_user) == "locked") {
            $ans['message'] = __("Ce labdoc ne peut pas être supprimé, car il est actuellement modifié par un autre membre de l'équipe.");
            $ans['status'] = 'locked';
        } else {
            // suppression du LD
            if ($ld->remove($id_user)) {
                if ($this->status === self::ON) {
                // ne pas tracer en mode test ou si la suppression a planté
                    Trace::logAction(Trace::DELETE_LD, ['id_report' => $this->id_report, 'id_mission' => $this->id_mission, 'id_labdoc' => $id_ld]);
                }
                $ans['status'] = "ok";
            } else {
                $ans['message'] = __('Erreur lors de la suppression du labdoc');
                $ans['status'] = 'fail';
            }
        }
        return $ans;
    }
    /**
     * duplicate a report model and link it to a specify mission
     *
     * @param Mission $duplicate_mission
     * @param User $user
     * @return Report
     */
    public function duplicate($duplicate_mission, $user) {
        $duplicate_report = $this->replicate();
        $duplicate_report->creation_time = $duplicate_report->update_time = now();
        $duplicate_report->id_mission = $duplicate_mission->id_mission;
        $duplicate_report->save();
        // Link current user to the duplicated solution report
        $duplicate_report->users()->sync([$user->id_user]);
        DB::table('link_report_learner')
            ->where('id_user', $user->id_user)
            ->where('id_report', $duplicate_report->id_report)
            ->update(['last_synchro' => time()]);
        
        // Duplicate the resources
        foreach ($this->resources as $r) {
            $new_r = $r->replicate();
            $new_r->id_report = $duplicate_report->id_report;
            $new_r->save();
        }
        foreach ($this->labdocs as $ld) {
            $new_ld = $ld->replicate();
            $new_ld->last_edition = time();
            $new_ld->last_editor = null;
            $new_ld->id_report = $duplicate_report->id_report;
            // Link the new ld to the duplicate mission
            if($ld->reportPart) {
                $new_ld->id_report_part = $duplicate_mission->reportParts->where('position', $ld->reportPart->position)->first()->id_report_part;
            }
            if($ld->id_ld_origin) {
                $ld_origin = Labdoc::find($ld->id_ld_origin);
                $new_rp_origin = $duplicate_mission->reportParts->where('position', $ld_origin->reportPart->position)->first();
                $new_ld_origin = $new_rp_origin->labdocs->where('position', $ld_origin->position)->first();
                $new_ld->id_ld_origin = $new_ld_origin?$new_ld_origin->id_labdoc:null;
            }
            $new_ld->initLabdocStatus([$user], 0, 0);
            $new_ld->comments()->delete();
            $new_ld->save();
        }
        return $duplicate_report;
    }
    /**
     * Returns a test report
     * @return \App\Report|null
     */
    public static function getTestReport()
    {
        $user_id = config('labnbook.id_user_test');
        $mission_id = config('labnbook.id_mission_test');
        if (!$user_id || !$mission_id) {
            return null;
        }
        // create a new report
        $id_report = DB::table('report')
            ->insertGetId([
                'id_mission' => $mission_id,
                'team_name' => 'anonymous_test',
                'status' => self::TEST,
            ]);
        if (!$id_report) {
            return null;
        }
        DB::table('link_report_learner')
            ->insert([
                'id_report' => $id_report,
                'id_user' => $user_id,
            ]);
        return self::find($id_report);
    }

    /**
     * Initialize a report if required
     * @param \App\User $user
     * @return bool is it the first time the learner opens his report?
     */
    public function init($user)
    {
        // If needed, create the labdocs of the report corresponding to the default labdocs of the report parts of the mission
        // labdoc.id_report = NULL && labdoc.position != NULL
        if (!$this->initialized) {
            $new_id_ld = [];
            $rp_ids = $this->mission->reportParts()->select('id_report_part')->get();
            $existing_ld_ids = $this->labdocs()
                                    ->whereNotNull('id_ld_origin')
                                    ->select('id_ld_origin')
                                    ->get();
            $labdocs = Labdoc::whereIn('id_report_part', $rp_ids)
                ->whereNull('id_report')
                ->where('shared', '=', '0')
                ->whereNotNull('position')
                ->whereNotIn('id_labdoc', $existing_ld_ids)
                ->orderBy('id_report_part')
                ->orderBy('position')
                ->get();
            foreach ($labdocs as $ld) { // création du LD
                $new_ld = $ld->copyIntoReport($this->id_report, $ld->position);
                array_push($new_id_ld, $new_ld->id_labdoc);
            }
            // For solution all labdocs from rps that are shared
            $copy_shared_ld_as_private = in_array($this->status, [self::TEST, self::SOLUTION, self::TUTO]);
            $shared_ld_query = Labdoc::select('labdoc.*');
            if ($copy_shared_ld_as_private) {
                // If the report is a solution or a test report, it does not belong
                // to a team_config thus we clone shared labdocs as private ones
                $id_for_clone = $this->id_report;
            } else {
                $id_for_clone = $this->id_team_config;
                // For other all labdoc
                $shared_ld_query = $shared_ld_query
                    ->leftJoin('labdoc as l1', function($join) {
                        $join->on('l1.id_ld_origin', '=', 'labdoc.id_labdoc')
                             ->on('l1.id_team_config', '=', DB::raw($this->id_team_config));
                    })
                    ->whereNull('l1.id_ld_origin');
            }
            $shared_ld_to_instanciate = $shared_ld_query->whereIn('labdoc.id_report_part', $rp_ids)
                    ->where('labdoc.shared', 1)
                    ->whereNull('labdoc.id_ld_origin')
                    ->whereNotNull('labdoc.position')
                    ->orderBy('id_report_part')
                    ->orderBy('labdoc.position')
                    ->get();
            foreach ($shared_ld_to_instanciate as $ld) {
                $new_ld = $ld->copyIntoReport($id_for_clone, $ld->position, $copy_shared_ld_as_private);
                array_push($new_id_ld, $new_ld->id_labdoc);
            }

            $teachers = $this->teachers(true)->get();
            $inserts = [];

            foreach ($teachers as $teacher) {
                foreach ($new_id_ld as $ld) {
                    $inserts[] = ['id_labdoc' => $ld, 'id_user' => $teacher->id_teacher, 'moved' => 0, 'modified' => 0, 'mod_icon' => 0, 'extend' => 0];
                }
            }
            // In case of simultaneous initialization there might be some duplicate
            // constraint that fails here, we do not care
            DB::table('labdoc_status')->insertOrIgnore($inserts);
        }

        // On the first enter of a learner in the report / on each enter of a teacher in the report
        //   initialize the labdoc_status for each LD created in the report (by default extend = 0)
        $last_learner_synchro = $this->getLastVisit($user->id_user); // for a teacher (scope follow or test) it is allways null
        if ($last_learner_synchro === null) {
            $first_learner_visit = true;
            $raw = DB::raw("SELECT id_labdoc FROM labdoc WHERE id_report = :id_report AND id_labdoc NOT IN (SELECT id_labdoc FROM labdoc_status WHERE id_user = :id_user)");
            $labdocs = \App\Labdoc::fromQuery(
                $raw,
                [':id_report' => $this->id_report, ':id_user' => $user->id_user]
            );
            foreach ($labdocs as $ld) {
                DB::table('labdoc_status')
                    ->insertOrIgnore([
                        'id_labdoc' => $ld->id_labdoc,
                        'id_user' => $user->id_user,
                        'moved' => 0,
                        'modified' => 0,
                        'mod_icon' => 1,
                        'extend' => 0,
                    ]);
            }
        } else {
            $first_learner_visit = false;
        }

        // mise à jour du statut du report
        if ($this->status === self::NEW) {
            // Do not change report status if the user is not related to it
            if($this->users()->whereKey($user->id_user)->exists()) {
                $this->status = self::ON;
            }
            $this->initialized = '1';
            $this->save();
        } elseif ($this->status === self::TEST) {
            $this->initialized = '1';
            $this->save();
        } elseif ($this->status === self::SOLUTION) {
            $this->initialized = '1';
            $this->save();
        }

        return $first_learner_visit;
    }

    // TODO use a policy instead
    /**
     * Can the user edit the report
     * Side effect : changes the report status if on and end_datetime is expired
     * @return array ['allowed' => boolean, 'msg' => string]
     */
    public function updateAllowed()
    {
        $msg = "";
        if (!in_array($this->status, [self::ON, self::NEW, self::TEST, self::TUTO, self::SOLUTION])) {
            $msg = __("Ce rapport ne peut plus être modifié.");
        } elseif (!is_null($this->start_datetime) && $this->start_datetime > date('Y-m-d H:i:s')) {
            $msg = __("La période de travail sur ce rapport n'a pas encore commencée.\nLe rapport ne peut pas être modifié.");
        } elseif (!is_null($this->end_datetime) && $this->end_datetime < date('Y-m-d H:i:s')) {
            if ($this->status == self::ON) {
                $this->status = self::WAIT;
                $this->save();
            }
            $msg = __("La période de travail sur ce rapport est terminée.\nLe rapport ne peut plus être modifié.");
        }
        return ['allowed' => empty($msg), 'msg' => $msg];
    }

    /**
     * @param object $tc TeamConfig
     * @return array
     */
    protected static function mapTeamconfigToReport($tc)
    {
        return [
            'id_team_config' => (int) $tc->id_team_config,
            'id_mission' => (int) $tc->id_mission,
            'start_datetime' => $tc->start_datetime ? $tc->start_datetime : null,
            'end_datetime' => $tc->end_datetime ? $tc->end_datetime : null,
            'allow_msg_teacher' => (int) $tc->allow_msg_teacher ? $tc->allow_msg_teacher : 0,
            'allow_msg_team' => (int) $tc->allow_msg_team ?  $tc->allow_msg_team : 0,
            'allow_msg_id_class' => (int) $tc->allow_msg_id_class ? $tc->allow_msg_id_class : 0,
            'allow_msg_id_mission' => (int) $tc->allow_msg_id_mission ?  $tc->allow_msg_id_mission : 0,
            'allow_attach_ld' => (int) $tc->allow_attach_ld ? $tc->allow_attach_ld : 0,
            'allow_save_ld' => (int) $tc->allow_save_ld ? $tc->allow_save_ld : 0,
            'allow_import' => (int) $tc->allow_import ?  $tc->allow_import : 0,
            'allow_import_id_mission' => (int) $tc->allow_import_id_mission ?  $tc->allow_import_id_mission : null
        ];
    }

    /**
     * Create a new Report instance from a TeamConfig instance.
     *
     * @param object $tc TeamConfig
     * @return \Report
     */
    public static function createFromTeamconfig($tc)
    {
        $report = new Report();
        foreach (self::mapTeamconfigToReport($tc) as $name => $value) {
            $report->$name = $value;
        }
        $report->status = self::NEW;
        $report->initialized = false;
        $report->team_name = "";
        return $report;
    }

    /**
     * Sets the members of the team, removing other members from the report.
     *
     * @param array $userIds List of (int) user.id_user
     * @param int $teamConfigId If set, will remove users from other reports of the same team config.
     * @return int Number of changes (deletions + insertions)
     * @throws Exception
     */
    public function setMembers($userIds, $teamConfigId = null)
    {
        if (empty($this->id_report)) {
            throw new Exception("Cannot remove a member from a report without ID.");
        }
        $id_report = (int) $this->id_report;
        $ids = array_map('intval', $userIds);

        // remove members whose ID is no longer in $ids
        $deletions = DB::table('link_report_learner')
            ->where('id_report', $id_report)
            ->whereNotIn('id_user', $ids)
            ->delete();

        // add members, ignoring those who are already in the DB
        // insert should look link [
        $inserts = [];
        foreach ($ids as $i) {
            $inserts[] = ['id_report'=>$id_report, 'id_user'=>$i];
        }
        $insertions = DB::table('link_report_learner')->insertOrIgnore($inserts);

        if ($teamConfigId > 0) {
            // members of this report are removed from other reports of the same team_config
            $deletions += DB::table('link_report_learner')
                ->join('report', 'id_report')
                ->where('report.id_report', '<>', $id_report)
                ->where('report.id_team_config', $teamConfigId)
                ->whereNotIn('link_report_learner.id_user', $ids)
                ->delete('link_report_learner');
        }

        return $deletions + $insertions;
    }

    /**
     * Update the records in the DB in the list of IDs.
     *
     * @param object $tc TeamConfig
     * @param array $ids
     * @return int Number of changes
     */
    public static function updateAllFromTeamconfig($tc, $ids)
    {
        $updates = [];
        foreach (self::mapTeamconfigToReport($tc) as $name => $value) {
            if ($name !== 'id_report') {
                $updates[$name] = is_bool($value) ? (int) $value : $value;
            }
        }
        return DB::table('report')
            ->whereIn('id_report', array_map('intval', $ids))
            ->update($updates);
    }

    /**
     * Does this report has LockedLabdocs
     * @return boolan
     */
    public function hasLockedLabdocs()
    {
        return DB::table('labdoc')
            ->where(
                function ($query) {
                    $query->where('id_report', $this->id_report)
                    ->orWhere(
                        function ($query) {
                            // Shared labdoc edited by a coworker
                            $query->whereNull('id_report')
                                  ->where('shared', 1)
                                  ->where('id_team_config', $this->id_team_config)
                                  ->whereIn('last_editor', $this->users()->pluck('user.id_user'));
                        });
                })->whereNotNull('locked')
                  ->whereRaw('UNIX_TIMESTAMP() - locked <  ?', config('labnbook.locker_validity'))
                  ->exists();
    }

    /**
     * Set the report status to 'wait' return an array [success => bool, message => string]
     * @return array
     */
    public function submit()
    {
        $ret = ['success' => false, 'message' => ''];
        if ($this->hasLockedLabdocs() > 0) {
            $ret['message'] = __("Des labdocs sont en cours d'édition, vous ne pouvez pas soumettre votre rapport.");
            return $ret;
        }
        if ($this->status != self::ON) {
            $ret['message'] = __("Impossible de soumettre un rapport qui n'est pas 'en cours'");
            return $ret;
        }
        $this->status = self::WAIT;
        if (!$this->save()) {
            $ret['message'] = __("Erreur d'enregistrement en base de donnée");
            return $ret;
        }
        Trace::logAction(Trace::SUBMIT_REPORT, ['id_report' => $this->id_report]);
        $ret['success'] = true;
        return $ret;
    }

    /**
     * Return the names of the report users
     * @return string
     */
    public function getTeamUsers()
    {
        $users_in_group = [];
        foreach ($this->users as $u) {
            $users_in_group[$u->id_user] = $u->user_name . " " . $u->first_name;
        }
        return $users_in_group;
    }

    /**
     * Sets the last_synchro timestamp for a given user
     * @param User $user
     * @param string $scope
     */
    public function setLastVisit($user, $scope)
    {
        try { //
            $time = time();
            if ($scope === "default" || $scope === "view") {
                $user->last_synchro = $time;
                $user->save();
                DB::table('link_report_learner')
                    ->where('id_user', $user->id_user)
                    ->where('id_report', $this->id_report)
                    ->update(['last_synchro' => $time]);
            } else if ($scope === "follow") {
                DB::table('teacher_report_status')
                    ->updateOrInsert(['id_teacher' => $user->id_user, 'id_report' => $this->id_report], ['last_synchro' => $time]);
            }
        } catch (Exception $e) {
            Log::Error($e);
        }
    }

    /**
     * Build the XML used for synchronizing a report.
     * @param \App\User $user
     * @param string $scope : default, view or follow
     * @param integer $edited_ld : id of edited labdoc
     */
    public function synchronize($user, $scope, $edited_ld, $message)
    {
        $time = time();
        $user->last_synchro = $time;
        $user->save();
        $this->setLastVisit($user, $scope);

        if ($scope === "default" && $edited_ld > 0) {
            $ld_edited = \App\Labdoc::find($edited_ld);
            if ($ld_edited) {
                $ld_edited->setLock($user->id_user);
            }
        }

        // XML doc that will be sent as a response
        $XML_doc = new \App\XMLDoc();
        $XML_doc->loadXML("<modification><deleted></deleted><modified></modified><moved></moved><drafted></drafted><ld_list></ld_list><connected_user></connected_user><annotations></annotations><message></message><ressource></ressource><comment></comment></modification>");

        // CONNECTED USERS
        $connected_students = $this->users()->connected(true/*connected to the report*/)->get();
        if (!$this->sharedLabdocs->isEmpty()) {
            foreach ($this->teamConfig->reports as $r) {
                if ($r->id_report != $this->id_report) {
                    $connected_students = $connected_students->concat($r->users()->connected(true)->get());
                }
            }
        }
        foreach ($connected_students as $u) {
            $XML_doc->addXMLNode("connected_user", "user", $u->id_user, null);
        }
        $connected_teachers = $this->connectedTeachers();
        foreach ($connected_teachers as $t) {
            $XML_doc->addXMLNode("connected_user", "teacher", $t->id_teacher, null);
        }

        // LABDOCS OF THE REPORT
        $ld_private = $this->labdocs()
                      ->join('labdoc_status', 'labdoc.id_labdoc', '=', 'labdoc_status.id_labdoc')
                      ->where('id_user', $user->id_user)
                      ->where(function ($query) {
                          $query->whereNull('labdoc.deleted')
                              ->orWhere('labdoc_status.deleted', 1);
                      })->orderByRaw('labdoc.id_report_part, labdoc.position')
                      ->get();
        $ld_shared = $this->sharedLabdocs()
                          ->select('labdoc.*', 'labdoc_status.*')
                          ->join('labdoc_status', 'labdoc.id_labdoc', '=', 'labdoc_status.id_labdoc')
                          ->where('id_user', $user->id_user)
                          ->where(function ($query) {
                              $query->whereNull('labdoc.deleted')
                                    ->orWhere('labdoc_status.deleted', 1);
                          })->orderByRaw('labdoc.id_report_part, labdoc.position')
                            ->get();

        // Laravel does not want to merge the two collection as they come
        // from different relations, we have to merge them manually
        $lds = collect([]);
        foreach ($ld_private as $ld) {
            $lds->push($ld);
        }
        foreach ($ld_shared as $ld) {
            $lds->push($ld);
        }
        foreach ($lds as $ld) {
            $id_ld = $ld->id_labdoc;
            $id_rp = $ld->id_report_part;

            if ($ld->deleted == 1) { // Labdoc must be deleted
                $XML_doc->addXMLLD("deleted", null, $id_ld, $id_rp, $ld->shared);
            } else {
                // Delete lock if LD was not correctly closed by previous user
                $modify_ld = $ld->unsetObsoleteLock($user->id_user);

                // Modified LD => send LD ref, name and content 
                if ($ld->modified == 1 || $modify_ld) {
                    $XML_doc->addXMLModifiedLD($id_ld, $id_rp, $ld->name, $ld->labdoc_data, $ld->shared);
                    $ld->updateStatus(['modified' => 0], $user->id_user);
                }
                // Moved LD => send LD ref, name and content and position in RP
                if ($ld->moved == 1) {
                    $XML_doc->addXMLLD("moved", $ld->position, $id_ld, $id_rp, $ld->shared);
                }
                //  Draft status management  => resent status 
                if ($ld->drafted == 1) {
                    $XML_doc->addXMLLD("drafted", $ld->draft, $id_ld, $id_rp, $ld->shared);
                    $ld->updateStatus(['drafted'=>0], $user->id_user);
                }
                // LD status => send LD ref, status (free_ld / modified_ld / locked_ld) + editor ( modified / locked ) + last_edition (modified) + last_id_version
                if ($ld->locked !== null && $ld->last_editor != $user->id_user) {
                    $lastEditorFullName=  $ld->user_last_editor->first_name . ' ' . $ld->user_last_editor->user_name;
                    $XML_doc->addXMLInfoStatusLD($id_ld, $id_rp, Labdoc::LOCKED, $lastEditorFullName, "", $ld->last_id_version);
                } elseif ($ld->mod_icon == 1) {
                    if ($ld->user_last_editor) {
                        $lastEditorFullName =  $ld->user_last_editor->first_name . ' ' . $ld->user_last_editor->user_name;
                    } else {
                        $lastEditorFullName = "";
                    }
                    $XML_doc->addXMLInfoStatusLD($id_ld, $id_rp, Labdoc::MODIFIED, $lastEditorFullName, $ld->last_edition, $ld->last_id_version);
                } else {
                    $XML_doc->addXMLInfoStatusLD( $id_ld, $id_rp, Labdoc::FREE, "", "", $ld->last_id_version);
                }
            }
        }

        // ANNOTATIONS
        if ($scope === "default" || $scope === "view") {
            $reload_annotations = DB::table('link_report_learner')
                ->where('id_user', $user->id_user)
                ->where('id_report', $this->id_report)
                ->value('reload_annotations');
            if ($reload_annotations) {
                $XML_doc->addXMLNode("annotations", "annotation", null, null);
                self::notifyAnnotation(false, $user->id_user);
            }
        }

        // WIDGET COMMENT
        // get the id_labdoc containing new_comments
        // Highlight the comment icon of each LD : uniquement si le header n'a pas été renvoyé
        if ($scope !== "follow") {
            $result_com = $this->unreadComments($user->id_user)->get();
            foreach ($result_com as $ld) {
                $XML_doc->addXMLLD("comment", null, $ld->id_labdoc, null, $ld->shared);
            }
        }

        // WIDGET MSG
        // get the id & last_msg of the conversations containing new messages
        if ($message) {
            $result = Conversation::getConvWithNewMsg($user->id_user);
            foreach ($result as $conv) {
                $snapshot = Message::getSnapshotFromMsg($conv->msg_content);
                $XML_doc->addXMLNode("message", "conv", $conv->id_conversation, $snapshot);
            }
        }
        //inserting info ressource into XML file
        $result = Report::getNewRessources($user);
        foreach ($result as $ressource){
            $XML_doc->addXMLNode('ressource','lru',$ressource,$this->id_report.':'.$user->id_user);
        }
        return $XML_doc->saveXML();
    }
    

    /**
     * update report and return infos about the updates
     * @param array $updates : associative array of changes
     * @return array json response
     */
    public function updateReport($updates)
    {
        $updated = 0;
        $old_status = $this->status;
        $this->fill($updates);

        if ($this->isDirty()) {
            $updated = 1;
            $this->save();
        }
        
        return [
            'updated' => $updated,
            'statuses' => $this->status != $old_status ? 1 : 0,
        ];
    }

    /**
     * Get number of new messages in the conversations of a report for specific user
     *
     * @param int $id_user  id of the concerned user
     * @return int
     */
    public function getNbNewMessages($id_user)
    {
        $sql = "SELECT COUNT(id_message) AS nb_msg FROM message"
            . " JOIN conversation USING(id_conversation) JOIN link_conversation_user lcu USING(id_conversation)"
            . " WHERE (msg_time > last_seen OR last_seen IS NULL)"
            . " AND id_report = ? AND lcu.id_user = ? AND id_user_sender <> ?";
        $new_msgs = DB::selectOne(DB::raw($sql), [$this->id_report, $id_user, $id_user]);
        //dd($new_msgs);
        return $new_msgs->nb_msg;
    }

    /**
     * Get number of new comments in the non-deleted labdocs of a report for specific user
     *
     * @param int $userId id of the concerned user
     * @return int
     */
    public function getNbNewComments($id_user)
    {
        return $this->unreadComments($id_user)->count();
    }

    /**
     * Get number of new annotations in a report for specific user
     *
     * @param int $userId id of the concerned user
     * @return int
     */
    public function getNbNewAnnotations($id_user){
        $sql_msgs = " SELECT COUNT(id_annotation) AS nb_annot " 
            . "FROM annotation JOIN link_report_learner lrl USING(id_report) LEFT JOIN labdoc USING(id_labdoc) "
            . "WHERE lrl.id_report = ? AND id_user = ?  AND deleted IS NULL "
            . "AND id_annotation NOT IN (SELECT id_annotation FROM `link_annotation_learner` WHERE id_user = ? )"; 
        $new_annotations = DB::selectOne(DB::raw($sql_msgs), [$this->id_report, $id_user, $id_user]);
        // dd($new_annotations);
        return $new_annotations->nb_annot;
    }
    
    /**
     * Amount of non-deleted modified LD in a report for specific user
     *
     * @param int $userId
     * @return int
     */
    public function getNbLDModified($id_user){
        return $this->labdocs()
            ->join('labdoc_status', 'labdoc_status.id_labdoc', '=', 'labdoc.id_labdoc')
            ->where('labdoc_status.mod_icon','=','1')
            ->whereNull('labdoc.deleted')
            ->where('id_user','=', $id_user)
            ->count();
    }

    /**
     * Toggles all labdocs from a report for a user
     * @param int $id_usr
     * @param int extend should we extend or close the LDs
     */
    public function toggleAllLd($id_user, $extend)
    {
        $sql = "UPDATE labdoc_status
            JOIN labdoc USING(id_labdoc)
            SET extend = :extend
            WHERE id_user = :id_user
            AND id_report = :id_report
            AND labdoc.deleted IS NULL
            AND id_report_part IS NOT NULL";
        DB::update($sql, [
            'extend' => $extend,
            'id_user' => $id_user,
            'id_report' => $this->id_report
        ]);
    }

    /**
     * Indicate to the learners of the report that something has changed in the annotations
     * @param boolean $on notify all learners of the report on true and remove notification for current user on false
     * @param int $id_learner
     */
    public function notifyAnnotation(bool $on, int $id_learner = null)
    {
        if ($on) {
            DB::update(DB::raw("UPDATE link_report_learner lrl SET lrl.reload_annotations = 1 WHERE lrl.id_report = ?"), [$this->id_report]);
        } else {
            DB::update(DB::raw("UPDATE link_report_learner lrl SET lrl.reload_annotations = 0 WHERE lrl.id_report = ? AND lrl.id_user = ?"), [$this->id_report, $id_learner]);
        }
    }

    /**
     * Check if report actions need to be traced
     * @return bool
     */
    public function shouldTrace(){
        return in_array($this->status, [Report::ON, Report::TUTO]);
    }

    /**
     * return unread resources
     * @param $user
     * @return \Illuminate\Support\Collection
     */
    private function getNewRessources($user): \Illuminate\Support\Collection
    {
        $resources = $this->resources;
        $resourceByMission = $this->mission->resources;
        $resourceAll= $resourceByMission->merge($resources)->where('res_type','<>','assignment');
        $tabLru = $user->ressources;
        return $resourceAll->pluck('id_ressource')->diff($tabLru->pluck('id_ressource'));
    }

    /**
     * return an array of id_ressource with id_ressource as key
     * id_ressource => id_ressource
     * @param $user
     * @return array
     */
    public function displayNewRessources($user): array
    {
        $newRessources = $this->getNewRessources($user);
        $displayNewDoc = [];
        foreach ($newRessources as $value){
            $displayNewDoc[$value] = $value;
        }
        return $displayNewDoc;
    }

    /**
     * Used in scoring tool app, check if an activity is a student one or a test or solution one
     * @return bool
     */
    public function isStudentActivity(): bool
    {
        return !in_array($this->status,  [self::TEST, self::SOLUTION]);
    }

    /**
     * Return true if and only if the user is already in the report
     * @param int id_user
     * @param boolean is_follow
     * @return boolean
     */
    public function isUserInReport($id_user, $is_follow)
    {
        $last_synchro = $this->getLastVisit($id_user, $is_follow);
        if (!$last_synchro) {
            return false;
        }
        $max_time = 4 * config('labnbook.refresh_period');
        return time() < $last_synchro + $max_time;
    }

    /**
     * Inserts each missing labdoc status for the given user
     *
     * @param \App\User $user the user for who we want to insert missing statuses
     *
     * @return int number of inserts
     */
    public function insertMissingLdStatuses($user)
    {
        $missing_ld_status = Labdoc::leftJoin(
            'labdoc_status',
            function ($join) use ($user) {
                $join->on('labdoc.id_labdoc', 'labdoc_status.id_labdoc')
                    ->where('labdoc_status.id_user', $user->id_user);
            }
        )->where('id_report', $this->id_report)
        ->orWhere(function ($query) {
            $query->whereNull('id_report')
                  ->where('shared', 1)
                  ->where('id_team_config', $this->id_team_config);
        })->select('labdoc.id_labdoc')
         ->whereNull('id_user')
         ->get();
        if (!$missing_ld_status->isEmpty()) {
            $inserts = [];
            foreach ($missing_ld_status as $ls) {
                $inserts[] = [
                    'id_user' => $user->id_user,
                    'id_labdoc' => $ls->id_labdoc,
                    'modified' => 0,
                    'moved' => 0,
                    'mod_icon' => 1,
                    'extend' => 0,
                ];
            }
            return DB::table('labdoc_status')->insertOrIgnore($inserts);
        }
        return 0;
    }

    /**
     * Checks wheter a state transition is legal for a report
     *
     * @param string $old_status in 'new','on','wait','arc','test','tuto','solution'
     * @param string $new_status in 'new','on','wait','arc','test','tuto','solution'
     * @param boolean $initialized is the report initialized ?
     * @param end_datetime string end_datetime of the report
     * @param boolean $is_learner is the user requesting the change a learner of this report ?
     * @param boolean $is_learner is the user requesting the change a teacher of this report ?
     * @return boolean
     */
    public static function statusTransitionIsLegal($old_status, $new_status, $initialized, $end_datetime, $is_learner, $is_teacher)
    {
        if (!$is_learner && !$is_teacher) {
            // Not allowed, should not happens
            // What about admin ?
            return false;
        }
        switch ($new_status) {
        case self::NEW:
            return ($is_teacher && $old_status === self::ARC && !$initialized);
        case self::ON:
            $timeUpdate = date("Y-m-d h:m:s");
            return $initialized &&
                (!$end_datetime || $end_datetime > $timeUpdate) &&
                (
                    ($is_learner && $old_status === self::NEW) ||
                    ($is_teacher && in_array($old_status, [self::WAIT, self::ARC]))
                );
            break;
        case self::WAIT:
            return $initialized &&
                (
                    $old_status === self::ON ||
                    ($is_teacher && $old_status === self::ARC)
                );
        case self::ARC:
            // ONLY for teacher
            return $is_teacher && in_array($old_status, [self::NEW, self::ON, self::WAIT]);
        }
        return false;
    }

    /**
     * @inheritDoc
     */
    public function save(array $options = [])
    {
        if ($this->id_report && $this->isDirty('status')) {
            $user = auth()->user();
            $is_teacher = $user->isReportTeacher($this);
            $is_learner = $user->isReportLearner($this);
            $old_status = $this->getOriginal('status');
            $allowed = self::statusTransitionIsLegal($old_status, $this->status, $this->initialized, $this->end_datetime, $is_learner, $is_teacher);
            if (!$allowed) {
                Log::ERROR("Attempt to change status from '{$old_status}' to '{$this->status}' on report {$this->id_report} not allowed by user {$user->id_user}");
                $this->status = $old_status; // Change not allowed;
            }
        }
        return parent::save($options);
    }
}
