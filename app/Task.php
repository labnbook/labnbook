<?php

namespace App;

use App\Report;
use App\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Task
 * @property int $id_task
 * @property string $title
 * @property string $state
 * @property int $estimated_duration
 * @property string $due_date
 * @property string $priority
 * @property int $id_parent_task
 * @property int $id_report
 * @property int $position
 * @property int $id_user_creator
 * @property int $id_user_last_editor
 * @property boolean $hasSubtasksUnfolded
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $last_modified_at
 */
class Task extends Model
{
    use HasFactory;
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_modified_at';

    // Define the table associated with the model
    protected $table = 'task';

    // Define the primary key for the table
    protected $primaryKey = 'id_task';

    // If the primary key is not an incrementing integer
    public $incrementing = true;

    // If the primary key is not an integer
    protected $keyType = 'int';

    // Define the attributes that are mass assignable
    protected $fillable = [
        'title',
        'state',
        'estimated_duration',
        'due_date',
        'priority',
        'id_parent_task',
        'id_report',
        'position',
        'id_user_creator',
        'id_user_last_editor',
        'hasSubtasksUnfolded'
    ];


// Enum values for the state field
    public const STATE_IN_PROGRESS = 'inprogress';
    public const STATE_DONE = 'done';

    // Enum values for the priority field
    public const PRIORITY_NONE = '';
    public const PRIORITY_LOW = 'low';
    public const PRIORITY_MEDIUM = 'medium';
    public const PRIORITY_HIGH = 'high';
    
    // Define the relationships

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo(User::class, 'id_user_creator');
    }

    // Relationship with the user who last edited the task

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function lastEditor()
    {
        return $this->belongsTo(User::class, 'id_user_last_editor');
    }

    // Relationship with the parent task (if any)
    public function parentTask()
    {
        return $this->belongsTo(Task::class, 'id_parent_task');
    }

    // Relationship with child tasks
    public function childTasks()
    {
        return $this->hasMany(Task::class, 'id_parent_task');
    }

    // Relationship with report through the linking table
    public function report()
    {
        return $this->belongsTo(Report::class, 'id_report');
    }

    // Relationship with users through the linking table
    public function assignees()
    {
        return $this->belongsToMany(User::class, 'link_task_user', 'id_task', 'id_user');
    }
}
