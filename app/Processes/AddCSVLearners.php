<?php

namespace App\Processes;
use Illuminate\Support\Facades\Log;
use App\Trace;
use App\Institution;
use App\Processes\Ldap\Ldap;

/**
 * App\Processes\AddCSVLearners
 */
class AddCSVLearners
{
    const FIRST_NAME = 0;
    const USER_NAME = 1;
    const INST_NUMBER = 2;
    const EMAIL = 3;
    const LOGIN = 4;
    const PASSWORD = 5;
    const CAS_PASSWORD = "Annuaire institutionnel";

    private static function cleanString($str)
    {
        if (function_exists('iconv')) {
            $ascii = strtolower(str_replace("'", "", iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', trim($str))));
        } elseif (extension_loaded('intl')) {
            $ascii = strtolower(str_replace("'", "", normalizer_normalize(trim($str), Normalizer::FORM_D)));
        } else {
            $ascii = strtolower(str_replace("'", "", trim($str)));
        }
        return preg_replace('#[^-\w]+#', '', $ascii);
    }


    /**
     * Creates a login string from a user passed as an array
     * @param array $user
     * @return string
     */
    private static function createUserLogin($user)
    {
        return substr(self::cleanString($user[self::FIRST_NAME]), 0, 1)
            .substr(self::cleanString($user[self::USER_NAME]), 0, 8);
    }

    /**
     * Returns the users with same email and id_inst
     * @param string $email
     * @param int $id_inst
     * @return array associative array of users
     */
    private static function findUsersByEmail($email, $id_inst)
    {
        return \App\User::where('id_inst', $id_inst)
            ->where('email', $email)
            ->get()
            ->toArray();
    }

    /**
     * Returns the ide of the user with the corresponding number and instituion or null
     * @param int $studentnum
     * @param int $id_inst
     * @return array user
     */
    private static function findUserByStudentnum($studentnum, $id_inst)
    {
        $user = \App\User::where('id_inst', $id_inst)->where('inst_number', $studentnum)->first();
        if ($user) {
            return $user->toArray();
        }
        $users = self::findAllByStudentNumbers($id_inst, $studentnum);
        if (!empty($users['found'])) {
            // Search for a user with same email as Cas user or with inst login with same login
            $sql = "id_inst = ? AND (email like ? OR (pwd IS NULL AND login = ?))";
            $params = [$id_inst, $users['found'][0][3], $users['found'][0][4]];
            $user = \App\User::whereRaw($sql, $params)->first();
            if (!$user) {
                return null;
            }
            return $user->toArray();
        }
        return null;
    }

    /**
     * fonction checkAndAddCSVLearners
     * Vérifie les données d'un CSV contenant des étudiants
     * Génère un tableau d'étudiant associé au JSON du fichier d'étudiant passé en paramètre.
     * Si save et pas d'erreur alors enregistre les données dans la base
     * @param JSON $json_array un tableau contenant une série d'utilisateur (un par ligne, de taille 7)
     * @param \App\Classe $classe
     * @param string $save
     * @param string $send_mail
     * @param boolean $genPasswords
     */
    public static function checkAndAddCSVLearners($json_array, $classe, $save, $send_mail, $genPasswords = true)
    {
        $save = $save === "true";
        $send_mail = $send_mail === "true";

        $id_inst = $classe->id_inst;
        $inst_name = $classe->institution->name;

        // first pass: cleanup the data, fill-in the automatic fields
        $alreadyInDb = [];
        $unique = ['emails' => [], 'logins' => [], 'inst_numbers' => []];
        $learners = [];
        if ($json_array) {
            foreach ($json_array as $numl => $learner) {
                if (count($learner) <= 1) {
                    // on ne prend pas en compte les lignes avec un seul élément : probablement une ligne vide
                    $learners[$numl] = [];
                    continue;
                }

                $learner = array_map('trim', $learner);

                // on complète les champs manquant
                if (count($learner) < 6) {
                    for ($i = count($learner); $i < 6; $i++) {
                        $learner[$i] = "";
                    }
                }

                if ($learner[self::EMAIL]) {
                    $email = $learner[self::EMAIL];
                    $alreadyInDb[$email] = self::findUsersByEmail($email, $id_inst);
                    if (isset($unique['emails'][$email])) {
                        $unique['emails'][$email]++;
                    } else {
                        $unique['emails'][$email] = 1;
                    }
                }

                if (empty($learner[self::LOGIN])) {
                    $learner[self::LOGIN] = self::createUserLogin($learner);
                }

                if ($learner[self::PASSWORD]) {
                    $uniqueLogin = $learner[self::LOGIN] . '/' . $learner[self::PASSWORD];
                    if (isset($unique['logins'][$uniqueLogin])) {
                        $unique['logins'][$uniqueLogin]++;
                    } else {
                        $unique['logins'][$uniqueLogin] = 1;
                    }
                }

                if ($learner[self::INST_NUMBER]) {
                    $instNumber = $learner[self::INST_NUMBER];
                    if (isset($unique['inst_numbers'][$instNumber])) {
                        $unique['inst_numbers'][$instNumber]++;
                    } else {
                        $unique['inst_numbers'][$instNumber] = 1;
                    }
                }

                $learners[$numl] = $learner;
            }
        }

        // second pass: validate the content
        $nb_errors = 0;
        $nb_warnings = 0;
        $nb_insert = 0;
        $dbUser = []; // if $createUser[$numl] is true, the user account is not already in DB
        $error_msg = array();
        $warning_msg = array();
        foreach ($learners as $numl => $learner) {
            // initialisation
            $error_msg[$numl] = array();
            $warning_msg[$numl] = array();
            $dbUser[$numl] = null;

            if (!$learner) {
                $dbUser[$numl] = false;
                continue;
            }


            //E-mail
            if ($learner[self::EMAIL]) {
                $email = filter_var($learner[self::EMAIL], FILTER_VALIDATE_EMAIL);
                if (!$email) {
                    $nb_errors++;
                    $error_msg[$numl][] = "Le format de l'e-mail n'est pas reconnu";
                } elseif (!empty($alreadyInDb[$email])) { // doit être unique pour l'institution de la classe (si défini)
                    if (count($alreadyInDb[$email]) === 1) {
                        Log::Debug($alreadyInDb);
                        $nb_warnings++;
                        $warning_msg[$numl][] = __(
                            "L'utilisateur :first :last de la base LabNBook (institution :inst) possède l'adresse mail :email. Cet étudiant sera ajouté à la classe sans modification de son compte LabNBook.",
                            [
                                'first' => $alreadyInDb[$email][0]['first_name'],
                                'last' => $alreadyInDb[$email][0]['user_name'],
                                'email' => $email,
                                'inst' => $inst_name,
                            ]
                        );
                        $dbUser[$numl] = $alreadyInDb[$email];
                        $learners[$numl][self::PASSWORD] = '';
                    } else {
                        $nb_warnings++;
                        $warning_msg[$numl][] = __(
                            "Il existe déjà plusieurs comptes d'étudiants correspondant à l'adresse :email (institution :inst). Trois options s'offrent à vous :
- vous laissez comme tel : un nouveau compte (!) sera créé avec cette adresse mail,
- vous supprimez la ligne de ce tableau pour ensuite pouvoir ajouter manuellement à votre classe l'étudiant avec son compte existant,
- vous indiquez le numéro d'étudiant pour pouvoir formellement identifier l'étudiant qui sera ajouté à la classe sans création de nouveau compte.",
                            [
                                'email' => $email,
                                'inst' => $inst_name,
                            ]
                        );
                    }
                }
                if (isset($unique['emails'][$email]) && $unique['emails'][$email] > 1) {
                    $nb_errors++;
                    $error_msg[$numl][] = __(
                        "Vous avez défini :count fois le mail :email",
                        [
                            'count' => $unique['emails'][$email],
                            'email' => $email,
                        ]
                    );
                }
            }

            if ($learner[self::INST_NUMBER]) {
                $inst_number = $learner[self::INST_NUMBER];
                if (empty($dbUser[$numl])) {
                    $user = self::findUserByStudentnum($inst_number, $id_inst);
                    if ($user) {
                        $nb_warnings++;
                        $warning_msg[$numl][] = __(
                            "L'utilisateur « :first :last » de la base LabNBook possède le numéro :studentnumber dans l'institution :inst.
Cet étudiant sera ajouté à la classe sans modification de son compte LabNBook.",
                            [
                                'first' => $user['first_name'],
                                'last' => $user['user_name'],
                                'studentnumber' => $inst_number,
                                'inst' => $inst_name,
                            ]
                        );
                        $dbUser[$numl] = [$user];
                    }
                }
                if (empty($dbUser[$numl]) && empty($learner[self::PASSWORD]) && !$genPasswords) {
                    $learner[self::PASSWORD] = self::CAS_PASSWORD;
                    $learners[$numl][self::PASSWORD] = self::CAS_PASSWORD;
                }
                if (isset($unique['inst_numbers'][$inst_number]) && $unique['inst_numbers'][$inst_number] > 1) {
                    $nb_errors++;
                    $error_msg[$numl][] = __(
                        "Vous avez défini :count fois le numéro d'étudiant :studentnumber",
                        [
                            'count' => $unique['inst_numbers'][$inst_number],
                            'studentnumber' => $inst_number,
                        ]
                    );
                }
            }

            if (empty($dbUser[$numl])) {
                // Prenom et nom
                if ($learner[self::FIRST_NAME] == "" || $learner[self::USER_NAME] == "") {
                    $nb_errors++;
                    $error_msg[$numl][] = __("Il est nécessaire de définir nom et prénom pour chaque compte");
                } else {
                    // on recherche les homonymes pour prévenir les enseignants
                    $count = \App\User::where('id_inst', $id_inst)->where('user_name', $learner[self::USER_NAME])->where('first_name', $learner[self::FIRST_NAME])->count();
                    if ($count > 0) {
                        $nb_warnings++;
                        $warning_msg[$numl][] = __(
                            "Dans la base LabNBook de :inst, :count étudiant(s) portent ce nom et prénom. Trois options s'offrent à vous :
- vous laissez comme tel : un nouveau compte sera créé pour ce nom et prénom.
- vous supprimez la ligne de ce tableau pour ensuite pouvoir ajouter manuellement à votre classe l'étudiant avec son compte existant.
- vous indiquez le numéro d'étudiant et/ou l'email pour pouvoir formellement identifier l'étudiant qui sera ajouté à la classe sans création de nouveau compte",
                            [
                                'inst' => $inst_name,
                                'count' => $count,
                            ]
                        );
                    }
                }

                //Login et mot de passe
                // Création de l'identifiant si non défini
                // Création du mdp si non défni et vérification que les emails sont bien envoyés
                if ($learner[self::PASSWORD] == "" && $genPasswords) {
                    if (!$send_mail) {
                        $nb_warnings++;
                        $warning_msg[$numl][] = __("Vous avez choisi de ne pas envoyer d'e-mails d'inscription.
                            C'est à vous de transmettre à l'étudiant le mot de passe généré aléatoirement.");
                    } elseif ($learner[self::EMAIL] == "") {
                        $nb_warnings++;
                        $warning_msg[$numl][] = __("Vous n'avez pas indiqué d'e-mail pour cet étudiant.
                            C'est à vous de transmettre le mot de passe généré aléatoirement.");
                    }
                    $learner[self::PASSWORD] = \App\User::generateRandomString();
                    $learners[$numl][self::PASSWORD] = $learner[self::PASSWORD];
                } elseif ($learner[self::PASSWORD] && $learner[self::PASSWORD] !== self::CAS_PASSWORD && !\App\User::validPassword($learner[self::PASSWORD])) {
                    // Vérification du mot de passe fourni
                    $nb_errors++;
                    $error_msg[$numl][] = __("Le mot de passe doit comporter au moins 8 caractères dont une majuscule et une minuscule");
                }
                // Vérification du couple identifiant - mdp
                if ($learner[self::LOGIN] && $learner[self::PASSWORD] && $learner[self::PASSWORD] !== self::CAS_PASSWORD) {
                    // ne doit pas exister dans la base
                    if (\App\User::where('login', $learner[self::LOGIN])->exists()) {
                        $nb_errors++;
                        $error_msg[$numl][] = __("Un compte de ce nom existe déjà. Si vous voulez reprendre un compte existant indiquez le même email ou numéro étudiant, sinon changez le compte.");
                    }
                    // vérifier aussi que cette combinaison n'est pas définie 2 fois à l'identique
                    $uniqueLogin = $learner[self::LOGIN] . '/' . $learner[self::PASSWORD];
                    if (isset($unique['logins'][$uniqueLogin]) && $unique['logins'][$uniqueLogin] > 1) {
                        $nb_errors++;
                        $error_msg[$numl][] = __(
                            "Vous avez défini :count fois la même combinaison compte & mot de passe",
                            [
                                'count' => $unique['logins'][$uniqueLogin],
                            ]
                        );
                    }
                }
            } elseif (count($dbUser[$numl]) === 1) {
                foreach ([self::FIRST_NAME => 'first_name', self::USER_NAME => 'user_name', self::INST_NUMBER => 'inst_number', self::EMAIL => 'email', self::LOGIN => 'login'] as $k => $kDB) {
                    $learners[$numl][$k] = $dbUser[$numl][0][$kDB];
                }
                $learners[$numl][self::PASSWORD] = ''; // visual only
            }
            $learners[$numl]['exists'] = $dbUser[$numl] && count($dbUser[$numl]) === 1;
        }

        if ($nb_errors === 0 && $save) {
            // sauvegarde dans la base de données
            $usersCreated = 0;
            $nb_mail_sent = 0;
            $email_addresses_invited = array();
            foreach ($learners as $numl => $learner) {
                $exist_in_class = false;
                if (empty($dbUser[$numl]) || count($dbUser[$numl]) > 1) {
                    $first_name = $learner[self::FIRST_NAME];
                    $user_name = $learner[self::USER_NAME];
                    $inst_number = $learner[self::INST_NUMBER];
                    $email = $learner[self::EMAIL];
                    $login = $learner[self::LOGIN];
                    if ($learner[self::PASSWORD] === self::CAS_PASSWORD || !$learner[self::PASSWORD]) {
                        $passwordDisplayed = __("(authentification de l'université)");
                        $c_password = null;
                    } else {
                        $passwordDisplayed = $learner[self::PASSWORD];
                        $c_password = $learner[self::PASSWORD];
                    }
                    // gestion de la valeur NULL pour id_inst
                    if (!$inst_number) {
                        $inst_number = null;
                    }
                    $user = new \App\User([
                        'user_name' => $user_name,
                        'first_name' => $first_name,
                        'login' => $login,
                        'email' => $email,
                        'id_inst' => $id_inst,
                        'inst_number' => $inst_number,
                    ]);
                    if ($c_password != null) {
                        $user->setPassword($c_password); //will do the save
                    } else {
                        $user->save();
                    }
                    $id_user = $user->id_user;
                    if ($id_user) {
                        $usersCreated++;
                    }
                    // envoi du mail
                    if ($id_user && $send_mail && $email) {
                        $rawPassword = ($learner[self::PASSWORD] === self::CAS_PASSWORD || !$learner[self::PASSWORD] ? null : $learner[self::PASSWORD]);
                        \App\User::sendInscriptionEmail($email, $learner[self::LOGIN], $rawPassword);
                        $nb_mail_sent++;
                        $email_addresses_invited[]=$email;
                    }
                } else {
                    $id_user = $dbUser[$numl][0]['id_user'];
                    $exist_in_class = $classe->users->contains($id_user);
                }

                if (!$exist_in_class) { // on ajoute l'étudiant dans la classe
                    $classe->users()->attach($id_user);
                    $nb_insert++;
                }
            }
            if ($usersCreated) {
                Trace::logAction(Trace::TEACHER_CREATE_USER, ['count' => $usersCreated]);
            }
            if ($nb_insert) {
                Trace::logAction(Trace::TEACHER_ADD_STUDENT, ['id_class' => $classe->id_class, 'method' => 'csv', 'count' => $nb_insert]);
            }

            sort($email_addresses_invited);
            if(count($email_addresses_invited)>8){
                $email_addresses_invited=array_slice($email_addresses_invited, 0,7);
            }
            $ret = array(
                "save" => true,
                "nb_insert" => $nb_insert,
                "nb_mail_sent" => $nb_mail_sent,
                "email_addresses_invited" => $email_addresses_invited
            );
        } else { // on renvoie le tableau des étudiants pour vérification et modification
            $ret = array(
                "save" => false,
                "warnings" => $warning_msg,
                "errors" => $error_msg,
                "nb_warnings" => $nb_warnings,
                "nb_errors" => $nb_errors,
            );
        }
        $ret['data'] = $learners;
        return $ret;
    }

    /**
     * Return a list of students, similar to the CSV format.
     *
     * @param int $institute
     * @param string $text
     * @return array
     */
    public static function findAllByStudentNumbers($institute, $text)
    {
        $numbers = self::extractNumbers($text);
        if (!$numbers) {
            return ['found' => [], 'missing' => []];
        }
        // Do not query Ldap if instituion does not have Ldap
        $inst = Institution::find($institute);
        if (!$inst || !$inst->hasLdap()) {
            return [];
        }
        $list = [];
        $ldap = new Ldap();
        $students = $ldap->getStudentsByNumbers($numbers);
        foreach ($students as $s) {
            /* @var $s \App\Processes\Ldap\LdapUser */
            $studentId = $s->getApogeeId($numbers);
            $list[$studentId] = [
                $s->givenName,
                $s->sn,
                $studentId,
                $s->mail,
                $s->uid,
                self::CAS_PASSWORD
            ];
        }
        // add student numbers that were not found
        $missing = [];
        foreach ($numbers as $n) {
            if (!isset($list[$n])) {
                $missing[] = $n;
            }
        }
        return [
            "found" => array_values($list),
            "missing" => $missing
        ];
    }

    /**
     * Extract numbers (sequences of at leat 5 digits) from a text.
     *
     * @param string $x
     * @return array
     */
    private static function extractNumbers($x)
    {
        return array_filter(
            preg_split('/[^\d]/', trim($x), -1, PREG_SPLIT_NO_EMPTY),
            function ($x) { return strlen($x) > 4; }
        );
    }
}
