<?php

namespace App\Processes\Cron;

use \App\Classe;

class ExpireClasseCodes extends CronJob
{
    const CLASSE_CODE_VALIDITY_MONTHS = 6;
    public function __invoke()
    {
        $changes = Classe::whereNotNull('class_code')
            ->whereRaw($this->tsDiffSql('MONTH', Classe::UPDATED_AT, self::CLASSE_CODE_VALIDITY_MONTHS))
            ->update(['class_code' => null]);
        $this->log('Expired', $changes, 'Class code that were changed more than '.self::CLASSE_CODE_VALIDITY_MONTHS.' months ago');
    }
}
