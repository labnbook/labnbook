<?php

namespace App\Processes\Cron;

use Illuminate\Support\Facades\Log;

class CronJob
{
    protected function tsDiffSql($type, $field, $value)
    {
        $value = $value -1;
        return 'TIMESTAMPDIFF('.$type.', '.$field.', CURRENT_TIMESTAMP()) > '.$value;
    }

    protected function log ($verb, $changes, $message)
    {
        if ($changes) {
            Log::warning($verb.' '.$changes.' '.$message);
        }
    }
}
