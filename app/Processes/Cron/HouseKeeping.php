<?php

namespace App\Processes\Cron;

class HouseKeeping extends CronJob
{
    const LABDOCS_KEEP_MONTHS_AFTER_TRASH = 6;

    public function __invoke()
    {
        $this->deleteClassesWithoutTeachers();
        $this->deleteLearnersWithoutClasses();
        $this->deleteReportsWithoutUsers();
        $this->deleteLabdocsAttachedToRemovedMessages();
        $this->deleteOldTrashedLabdocs();
    }

    /**
     * Deletes classes without teachers
     */
    protected function deleteClassesWithoutTeachers()
    {
        $changes = \App\Classe::doesntHave('teachers')
            ->join('institution', 'class.id_inst', 'institution.id_inst')
            ->whereRaw('class.id_class <> institution.id_class_teacher')
            ->delete();
        $this->log('Deleted', $changes, "classes without teachers ");
    }

    /**
     * Deletes students without classes
     */
    protected function deleteLearnersWithoutClasses()
    {
        $changes = \App\User::doesntHave('teacher')->doesntHave('classes')->delete();
        $this->log('Deleted', $changes, "students without class");
    }

    /**
     * Delete report without users
     */
    protected function deleteReportsWithoutUsers()
    {
        $changes = \App\Report::doesntHave('users')->delete();
        $this->log('Soft deleted', $changes, "reports without learners ");
    }

    /**
     * Delete conversation without participants
     */
    protected function deleteConversationsWithoutParticipants()
    {
        $changes = \App\Conversation::doesntHave('users')->delete();
        $this->log('Deleted', $changes, "conversations without participants");
    }

    /**
     * Delete Labdoc attached to removed messages
     */
    protected function deleteLabdocsAttachedToRemovedMessages()
    {
        $changes = \App\Labdoc::whereNull('position')->doesntHave('reportPart')->doesntHave('messages')->delete();
        $this->log('Deleted', $changes, 'Labdocs attached to removed messages');
    }

    /**
     * Delete old trashed labdocs
     */
    protected function deleteOldTrashedLabdocs()
    {
        $changes = \App\Labdoc::whereNotNull('deleted')
            ->whereRaw('DATE_ADD(deleted, INTERVAL ' . self::LABDOCS_KEEP_MONTHS_AFTER_TRASH . ' MONTH) < NOW()')
            ->delete();
        $this->log('Hard deleted', $changes, 'Labdocs that where soft deleted more than '.self::LABDOCS_KEEP_MONTHS_AFTER_TRASH.' months ago');
    }
}
