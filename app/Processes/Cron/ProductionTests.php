<?php

namespace App\Processes\Cron;

use \App\Processes\AdminTests;
use Illuminate\Support\Facades\Mail;
use App\Mail\MarkdownMail;

class ProductionTests
{
    public function __invoke()
    {
        $addr = config('labnbook.bug_email_report_to');
        if (!$addr) {
            return;
        }
        $adminTests = new AdminTests();
        $messages = $adminTests->runTests();
        if (count($messages[AdminTests::ERROR]) || count($messages[AdminTests::WARNING])) {
            Mail::to($addr)->send(new MarkdownMail('emails.adminTests', __('Erreur lors des tests d\'administration'), $messages));
        }
    }
}
