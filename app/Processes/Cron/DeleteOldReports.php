<?php

namespace App\Processes\Cron;

use \App\Report;

class DeleteOldReports extends CronJob
{
    const ARCHIVE_MONTHS = 6;
    const DELETE_OLD_NEW_MONTHS = 1;
    const KEEP_YEARS_AFTER_UPDATE = 5;
    const KEEP_MONTHS_AFTER_CREATION = 12;
    const KEEP_MONTHS_AFTER_DELETE = 6;

    public function __invoke()
    {
        $this->archiveOldSubmittedReports();
        $this->deleteOldTestReports();
        $this->deleteOldNewReports();
        $this->deleteOldReports();
    }

    /**
     * Archive submitted reports six months after the end date
     */
    protected function archiveOldSubmittedReports()
    {
        $changes = Report::where('status', 'wait')
            ->whereRaw($this->tsDiffSql('MONTH', 'end_datetime', self::ARCHIVE_MONTHS))
            ->update(['status' => 'arc']);
        $this->log('Archived', $changes, 'reports that were more than '.self::ARCHIVE_MONTHS.' months after the end date');
    }

    /**
     * Delete test reports that are more than one day old
     */
    protected function deleteOldTestReports()
    {
        $changes = Report::where('status', 'test')
            ->whereRaw($this->tsDiffSql('HOUR', 'creation_time', 24))
            ->forceDelete();
        $this->log('Hard deleted', $changes, "test reports that were more than one day old");
    }

    /**
     * Delete new reports 3 months after their end_date
     */
    protected function deleteOldNewReports()
    {
        $changes = Report::where('status', 'new')
            ->whereRaw($this->tsDiffSql('MONTH', 'end_datetime', self::DELETE_OLD_NEW_MONTHS))
            ->delete();
        $this->log('Soft deleted', $changes, "new reports that were more than ".self::DELETE_OLD_NEW_MONTHS." months after the end date");
    }

    /**
     * Remove reports that have not been used for years
     */
    protected function deleteOldReports()
    {
        $changes = Report::whereRaw($this->tsDiffSql('YEAR', Report::UPDATED_AT, self::KEEP_YEARS_AFTER_UPDATE))
            ->delete();
        $this->log('Soft deleted', $changes, 'Report updated more than '.self::KEEP_YEARS_AFTER_UPDATE.' years ago');

        $changes = Report::whereNull(Report::UPDATED_AT)
            ->whereRaw($this->tsDiffSql('MONTH', Report::CREATED_AT, self::KEEP_MONTHS_AFTER_CREATION))
            ->delete();
        $this->log('Soft deleted', $changes, 'Report created more than '.self::KEEP_MONTHS_AFTER_CREATION.' months ago and never updated');

        $changes = Report::onlyTrashed()
            ->whereRaw($this->tsDiffSql('MONTH', Report::DELETED_AT, self::KEEP_MONTHS_AFTER_DELETE))
            ->forceDelete();
        $this->log('Hard deleted', $changes, 'Report That were soft deleted more than '.self::KEEP_MONTHS_AFTER_DELETE.' months ago');
    }
}
