<?php

namespace App\Processes\Cron;

use \App\Report;

class CloseEndedReports extends CronJob
{
    /**
     * Close reports when the end date is reached
     */
    public function __invoke()
    {
        $changes = Report::whereIN('status', ['on'])
            ->whereRaw("NOW() > end_datetime")
            ->update(['status' => 'wait']);
        $this->log('Closed', $changes, 'reports that were past their end date');
    }
}
