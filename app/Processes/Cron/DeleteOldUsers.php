<?php

namespace App\Processes\Cron;

use \App\User;

class DeleteOldUsers extends CronJob
{
    const LEARNER_KEEP_MONTHS = 41;
    const TEACHER_KEEP_MONTHS = 120;

    public function __invoke()
    {
        $this->deleteOldTeachers();
        $this->deleteOldLearners();
    }

    protected function notConnectedRawSQL($delay)
    {
        return "((last_synchro IS NOT NULL AND ".$this->tsDiffSql("MONTH", "FROM_UNIXTIME(last_synchro)", $delay)." )"
        . " OR (last_synchro IS NULL AND ".$this->tsDiffSql("MONTH", "creation_time", $delay)." ))";
    }

    protected function deleteOldLearners()
    {
        $changes = User::leftJoin('teacher', 'id_teacher', 'id_user')
            ->whereNull('id_teacher')
            ->whereRaw($this->notConnectedRawSQL(self::LEARNER_KEEP_MONTHS))
            ->delete();
        $this->log('Hard deleted', $changes, "learners that did not log in for more than ".self::LEARNER_KEEP_MONTHS." months");
    }

    protected function deleteOldTeachers()
    {
        $changes = User::join('teacher', 'id_teacher', 'id_user')
            ->whereRaw($this->notConnectedRawSQL(self::TEACHER_KEEP_MONTHS))
            ->delete();
        $this->log('Hard deleted', $changes, "teachers that did not log in for more than ".self::TEACHER_KEEP_MONTHS." months");
    }
}
