<?php

namespace App\Processes;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ExportLabdocs
{
    /**
     * @param int $id_ld_orig
     * @return array
     */
    public static function getStudentLabdocs($id_ld_orig) {
        return \App\Labdoc::hydrate(DB::Select(
            "SELECT l.*"
            . " FROM labdoc l"
            . " JOIN report r ON l.id_report = r.id_report"
            . " WHERE l.id_ld_origin = :id_ld AND l.last_edition IS NOT NULL"
            . " ORDER BY l.last_edition ASC"
            ,
            [
                ':id_ld' => (int)$id_ld_orig,
            ]
        ));
    }

    /**
     * @param int $idMission
     * @param int $idUser
     * @return array
     */
    public static function getTeacherLabdocs($idMission, $idUser) {
        return \App\Labdoc::hydrate(DB::Select(
            "SELECT l.*"
            . " FROM labdoc l"
            . " JOIN report_part rp ON l.id_report_part = rp.id_report_part AND l.id_report IS NULL"
            . " JOIN link_mission_teacher USING(id_mission)"
            . " WHERE l.type_labdoc = 'dataset' AND id_mission = :id_mission AND id_teacher = :id_user"
            . " ORDER BY rp.position ASC",
            [
                ':id_user' => (int)$idUser,
                ':id_mission' => (int)$idMission,
            ]
        ));
    }

    /**
     * @param array$labdocs
     * @return string
     */
    public static function zipLabdocs($labdocs) {
        if (!$labdocs) {
            return null;
        }
        $zip = new \ZipArchive();
        $zipName = tempnam(sys_get_temp_dir() , "lnb_zip_");
        if ($zip->open($zipName, \ZipArchive::OVERWRITE) !== true) {
            Log::Error("Error while creating ZipArchive");
            return null;
        }
        foreach ($labdocs as $l) {
            $localName = sprintf(
                "labdoc_%d_%s_rapport-%d.csv",
                $l->id_ld_origin,
                date('Y-m-d', $l->last_edition),
                $l->id_report
            );
            $zip->addFromString($localName, self::extractCsvFromLabdoc($l));
        }
        $zip->close();
        return $zipName;
    }

    /**
     * @param array $labdoc
     * @return string
     */
    private static function extractCsvFromLabdoc($labdoc) {
        $csvAcc = fopen('php://temp', 'w');;
        $separator = ";";
        $dataset = self::parseDataset($labdoc->labdoc_data);
        if ($dataset->header) {
            fputcsv($csvAcc, $dataset->header, $separator);
        }
        foreach($dataset->rows as $row) {
            fputcsv($csvAcc, $row, $separator);
        }
        $result = stream_get_contents($csvAcc, -1, 0);
        fclose($csvAcc);
        return $result;
    }

    private static function parseDataset($xml) {
        $data = (object) [
            'header' => null,
            'rows' => [],
        ];
        $doc = new \DOMDocument();
        $doc->loadXML($xml);
        $dataset = $doc->getElementsByTagName('dataset')->item(0);

        $header = $dataset->getElementsByTagName('header');
        if ($header->length > 0) {
            foreach($header->item(0)->getElementsByTagName('name') as $node) {
                $data->header[] = $node->textContent;
            }
        }

        $rows = $dataset->getElementsByTagName('row');
        if ($rows->length > 0) {
            foreach ($rows as $row) {
                $values = [];
                foreach($row->getElementsByTagName('value') as $node) {
                    $values[] = $node->textContent;
                }
                $data->rows[] = $values;
            }
        }

        return $data;
    }
}
