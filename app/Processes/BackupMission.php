<?php

namespace App\Processes;

use Illuminate\Support\Facades\DB;

class BackupMission
{
    /** @var int */
    private $id;

    /** @var Backup */
    private $backupSystem;

    /** @var string */
    private $backupDir;

    public function __construct($id)
    {
        $this->id = (int) $id;
        $this->backupSystem = new Backup('mission');
        $this->backupDir = $this->backupSystem->getBackupdir((int) $id);
    }

    /**
     * @return string
     */
    public function getBackupDir()
    {
        return $this->backupDir;
    }

    /**
     * Backup a mission and its dependencies into CSV files.
     *
     * @param boolean $backupReports
     * @return boolean
     */
    public function backup($backupReports = false)
    {
        $id = $this->id;
        $mission = \App\Mission::find($id)->toArray();
        if (!$mission) {
            return false;
        }

        if (!is_writable($this->backupDir)) {
            throw new Exception("Cannot write into the backup directory '$this->backupDir'.");
        }

        if ($backupReports) {
            $reportIds = \App\Report::Where('id_mission', 1)->select('id_report')->get()->toArray();
            if (!empty($reportIds)) {
                foreach ($reportIds as $reportId) {
                    (new BackupReport($reportId['id_report']))->backup();
                }
            }
            $this->backupSystem->saveRecordsIntoCsv(
                $reportIds,
                "{$this->backupDir}/reports.csv"
            );
        }

        $this->backupSystem->saveRecordsIntoCsv(
            [$mission],
            "{$this->backupDir}/mission.csv"
        );
        $this->backupSystem->saveRecordsIntoCsv(
            \App\ReportPart::Where('id_mission', $id)->get()->toArray(),
            "{$this->backupDir}/report_part.csv"
        );
        $this->backupSystem->saveRecordsIntoCsv(
            DB::table('ressource')->where('id_mission', $id)->get()->toArray(),
            "{$this->backupDir}/ressource.csv"
        );
        $this->backupSystem->saveRecordsIntoCsv(
            DB::table('labdoc as l')
                ->join('report_part as rp', 'l.id_report_part', 'rp.id_report_part')
                ->where('id_mission', $id)
                ->select('l.*')->get()->toArray(),
            "{$this->backupDir}/labdoc.csv"
        );
        $this->backupSystem->saveRecordsIntoCsv(
            DB::table('link_mission_teacher')->where('id_mission', $id)->get()->toArray(),
            "{$this->backupDir}/link_mission_teacher.csv"
        );
        $this->backupSystem->saveRecordsIntoCsv(
            DB::table('team_config')->where('id_mission', $id)->get()->toArray(),
            "{$this->backupDir}/team_config.csv"
        );

        $this->backupSystem->saveRecordsIntoCsv(
            DB::table('report')->where('id_mission', $id)->get()->toArray(),
            "{$this->backupDir}/reports.csv"
        );
        return true;
    }

    /**
     * Restore a mission from its CSV backup. Fails if the mission exists.
     *
     * @param boolean restoreReports
     * @return boolean
     */
    public function restore($restoreReports = false)
    {
        $id = $this->id;
        $mission = \App\Mission::find($id);
        if ($mission) {
            return false; // do not overwrite
        }

        if (!is_dir($this->backupDir) || !is_readable($this->backupDir)) {
            throw new Exception("Cannot read from the backup directory '{$this->backupDir}'.");
        }

        $this->backupSystem->loadFileIntoDb($this->backupDir, 'mission');
        $this->backupSystem->loadFileIntoDb($this->backupDir, 'report_part');
        $this->backupSystem->loadFileIntoDb($this->backupDir, 'ressource');
        $this->backupSystem->loadFileIntoDb($this->backupDir, 'labdoc');
        $this->backupSystem->loadFileIntoDb($this->backupDir, 'link_mission_teacher');
        $this->backupSystem->loadFileIntoDb($this->backupDir, 'team_config');
        if ($restoreReports) {
            $reports = $this->backupSystem->readRecords("{$this->backupDir}/reports.csv");
            foreach ($reports as $report) {
                $id = $report['id_report'];
                $backupR = new BackupReport($id);
                $backupR->restore($id);
            }
        }
        return true;
    }
}
