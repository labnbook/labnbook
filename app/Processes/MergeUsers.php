<?php

namespace App\Processes;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use \App\User;
use \App\Trace;

class MergeUsers extends Merge
{
    /** @var boolean */
    protected $old_log;

    /** @var array */
    protected $labdocs_and_missions = [];

    /** Columns names Names use to reference user or teacher table **/
    public const ID_NAMES = [
        'id_user',
        'id_teacher',
        'id_user_sender',
        'last_editor',
        'id_learner',
        'id_super_manager'
    ];

    /** @var array table */
    /** lists the action to be run for each table, the order matters */
    // Teacher is used twice : first clone a teacher if required, finally delete
    // it after all links are updated
    public const TABLES = [
        'teacher'                   => ['action' => 'custom', 'key' => 'id_teacher'],
        'message_var'               => ['action' => 'delete', 'key' => 'id_user'],
        'user_widget_config'        => ['action' => 'delete', 'key' => 'id_user'],
        'trace'                     => ['action' => 'update', 'key' => 'id_user'],
        'task'                      => ['action' => 'update', 'key' => 'id_user_creator'],
        'trace_old'                 => ['action' => 'custom', 'key' => 'id_user'],
        'labdoc_status'             => ['action' => 'update', 'key' => 'id_user'],
        'labdoc'                    => ['action' => 'update', 'key' => 'last_editor'],
        'annotation'                => ['action' => 'update', 'key' => 'id_teacher'],
        'comment'                   => ['action' => 'update', 'key' => 'id_user'],
        'message'                   => ['action' => 'update', 'key' => 'id_user_sender'],
        'link_class_learner'        => ['action' => 'updateIgnore', 'key' => 'id_user'],
        'link_annotation_learner'   => ['action' => 'updateIgnore', 'key' => 'id_user'],
        'link_class_teacher'        => ['action' => 'updateIgnore', 'key' => 'id_teacher'],
        'link_inst_teacher'         => ['action' => 'updateIgnore', 'key' => 'id_teacher'],
        'link_teacher_tteam'        => ['action' => 'custom', 'key' => 'id_teacher'],
        'link_extplatform_user'     => ['action' => 'custom', 'key' => 'id_user'],
        'link_conversation_user'    => ['action' => 'custom', 'key' => 'id_user'],
        'link_report_learner'       => ['action' => 'custom', 'key' => 'id_user'],
        'link_mission_teacher'      => ['action' => 'custom', 'key' => 'id_teacher'],
        'link_ressource_user'       => ['action' => 'updateIgnore', 'key' => 'id_user'],
        'link_task_user'            => ['action' => 'updateIgnore', 'key' => 'id_user'],
        'teacher_report_status'     => ['action' => 'custom', 'key' => 'id_teacher'],
        'simulation_link_learner_task' => ['action' => 'updateIgnore', 'key' => 'id_learner'],
        'simulation_link_super_manager' => ['action' => 'updateIgnore', 'key' => 'id_super_manager'],
        'simulation_trace'          => ['action' => 'updateIgnore', 'key' => 'id_user'],
        'user'                      => ['action' => 'delete', 'key' => 'id_user'],
    ];

    /** @var $key of the table **/
    public const KEY = 'id_user';
    
    /**
     * Runs a by table custom routine for the update
     * @param string $tablename
     * @param string $key
     */
    protected function custom($tablename, $key)
    {
        if ($tablename === 'trace_old' || !DB::table($tablename)->where($key, $this->source->id_user)->exists()) {
            // nothing to do
            return;
        }
        switch ($tablename) {
            case 'link_extplatform_user':
                $source_extusers = DB::table($tablename)
                    ->where($key, $this->source->id_user)
                    ->get();
                foreach ($source_extusers as $src) {
                    DB::table($tablename)
                        ->where('id_extplatform', $src->id_extplatform)
                        ->where('id_user_ext', $src->id_user_ext)
                        ->update([$key => $this->target->id_user]);
                }
                break;
            case 'link_conversation_user':
                $this->updateHandleDuplicateWithMax(
                    $tablename,
                    $key,
                    'id_conversation',
                    'last_seen',
                    function ($a, $b) {
                        return max($a, $b);
                    }
                );
                break;
            case 'link_report_learner':
                $this->updateHandleDuplicateWithMax($tablename, $key, 'id_report', 'last_synchro', function ($a, $b) {
                    return max($a, $b);
                });
                break;
            case 'link_mission_teacher':
                $this->updateHandleDuplicateWithMax(
                    $tablename,
                    $key,
                    'id_mission',
                    'teacher_type',
                    function ($r1, $r2) {
                        if ($r1 === 'designer') {
                            Log::debug($r1);
                            return $r1;
                        }
                        return $r2;
                    }
                );
                break;
            case 'link_teacher_tteam':
                $this->updateHandleDuplicateWithMax($tablename, $key, 'id_teacher_team', 'status', function ($r1, $r2) {
                    if ($r1 === 'manager') {
                        return $r1;
                    }
                    return $r2;
                });
                break;
            case 'teacher_report_status':
                $this->updateHandleDuplicateWithMax($tablename, $key, 'id_report', 'last_synchro', function ($a, $b) {
                    return max($a, $b);
                });
                break;
            case 'teacher':
                $source_t = \App\Teacher::find($this->source->id_user);
                if (!$source_t) {
                    return;
                }
                $target_t = \App\Teacher::find($this->target->id_user);
                if (!$target_t) {
                    // Copy source teacher
                    $target_t = $source_t->replicate();
                    $target_t->id_teacher = $this->target->id_user;
                    $target_t->save();
                    return;
                }
                foreach (['teacher_domain', 'teacher_level', 'teacher_comment'] as $field) {
                    if ($target_t->$field != $source_t->$field) {
                        $target_t->$field = trim($target_t->$field . " " . $source_t->$field);
                    }
                }
                if (in_array($source_t->role, [$source_t::ROLE_ADMIN, $source_t::ROLE_MANAGER])) {
                    if ($target_t->role != $target_t::ROLE_ADMIN) {
                        // Source is either ADMIN or MANAGER
                        // while target is either MANAGER or TEACHER
                        // thus source role is either identical or more powerful
                        // than target
                        $target_t->role = $source_t->role;
                    }
                }
                if ($target_t->isDirty()) {
                    $target_t->save();
                }
                break;
            default:
                throw new \Exception("Custom method called but not defined for table {$tablename}");
                break;
        }
    }


    private function prepareVersionningUpdate()
    {
        $ACTIONS = [
            Trace::ADD_LD,
            Trace::DUPLICATE_LD,
            Trace::EDIT_LD,
            Trace::MODIFY_LD,
            Trace::VALIDATE_LD,
            Trace::UPDATE_LD_NAME,
            Trace::DELETE_LD,
            Trace::IMPORT_LD,
            Trace::SAVE_ATTACHED_LD,
            Trace::RECOVER_LD,
            Trace::OPEN_LD_VERSIONING,
            Trace::HARD_DELETE_LD,
            ];
        $this->labdocs_and_missions = DB::table('trace')
            ->where('id_user', $this->source->id_user)
            ->whereIn('id_action', $ACTIONS)
            ->select('id_labdoc', 'id_mission')
            ->distinct()
            ->get();
    }

    /**
     * Updates user in labdoc versionning files
     * prepareVersionningUpdate must have been called before changing the user in the DB
     */
    private function updateVersionning()
    {
        foreach ($this->labdocs_and_missions as $lm) {
            $lv = new \App\LabdocVersioning($lm->id_mission);
            $lv->updateUser($lm->id_labdoc, $this->source->id_user, $this->target->id_user, $this->dryRun);
        }
    }

    public function prepare()
    {
        if ($this->dryRun) {
            $this->old_log = config("labnbook.log_sql");
            config(["labnbook.log_sql" => true]);
        }
        $this->prepareVersionningUpdate();
    }
    
    public function finish($failed)
    {
        if ($this->dryRun) {
            config(["labnbook.log_sql" => $this->old_log]);
        }
        if (!$failed) {
            $this->updateVersionning();
            Trace::logAction(Trace::USER_MERGED, ['id_user_orig' => $this->source->id_user], $this->target->id_user);
        }
    }
}
