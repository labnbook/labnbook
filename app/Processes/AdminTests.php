<?php

namespace App\Processes;

use Illuminate\Support\Facades\DB;
use \App\Institution;
use \App\Classe;
use \App\Processes\Ldap\Ldap;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Carbon;

class AdminTests
{
    const WARNING = 'warn';
    const ERROR = 'err';
    // Minimum disc space on /storage in Gio under which issue a warning
    const MIN_STORAGE = 2;
    // Required php extensions
    const REQUIRED_EXTS = ['bcmath', 'curl', 'json', 'ldap', 'mbstring', 'tokenizer', 'xml', 'zip'];
    const PHP_MIN_VERS = 80020; // 10000 * major + 100 * minor + release
    const PHP_UNSUPPORTED = 82000;// 10000 * major + 100 * minor + release
    const NODE_MIN_VERS = 16;
    // Extplatforms
    const MIN_SECRET_LENGTH = 32;
    // Cronjobs
    const CRON_WARN_MIN = 5;
    const CRON_ERR_MIN = 15;

    /** @var boolean is the app in production mode ? */
    private $production;

    public function __construct()
    {
        // Update information on remote repository
        chdir(base_path());
        exec('git config --global --add safe.directory $PWD');
        if (config('app.env') === 'production') {
            exec('git fetch -q');
        }
        $this->production = config('app.env') == 'production';
    }

    /**
     * Return all method of the object with name starting by a prefix
     * @param string $prefix
     * @return array
     */
    private function getMethods($prefix)
    {
        $methods = array_filter(
            get_class_methods($this),
            function ($name) use ($prefix) {
                return substr($name, 0, strlen($prefix)) == $prefix;
            }
        );
        sort($methods, SORT_NATURAL);
        return $methods;
    }

    /**
     * Run all test for the given test and return messages
     * @param $type
     */
    private function getMessages($type)
    {
        $tests = $this->getMethods($type);
        $messages = [];
        foreach ($tests as $t) {
            $ret = $this->$t();
            if ($ret) {
                $messages[] = $ret;
            }
        }
        return $messages;
    }

    /**
     * Run all admin tests and return an array of messages if any
     * An error means that at least one functionnality will not work as intended
     * @return array [ self::ERRORS => [...], self::WARNING => [...]]
     */
    public function runTests()
    {
        return [
            self::ERROR => $this->getMessages(self::ERROR),
            self::WARNING => $this->getMessages(self::WARNING)
        ];
    }

    /* ========================================================================
     *                             HELPERS
     *   Put here any helper needed for your tests
     *   =======================================================================
     */

    /*
     * Checks for a directory permissions
     * @param string path
     * @param boolean writable should it be writable
     */
    private function checkDirectory($dir, $writable)
    {
        if (!is_dir($dir)) {
            return __("Dossier inexistant :dir", ["dir" => $dir]);
        }
        if ($writable && !is_writable($dir)) {
            return __("Le dossier :dir devrait être écribable", ["dir" => $dir]);
        }
        if (!$writable && is_writable($dir)) {
            return __("Le dossier :dir ne devrait pas être écribable", ["dir" => $dir]);
        }
        return null;
    }

    /**
     * Returns the number of file in the givenDirectory
     */
    private function filesInDir($dir, $suffix = "")
    {
        return array_map(
            function ($e) use ($suffix) {
                return basename($e, $suffix);
            },
            glob($dir."/*")
        );
    }

    /**
     * Get tables with the given information schema
     * $field adn $op are considered trusted
     * @param string $field a mysql field ex 'TABLE_COLLATION'
     * @param string $op a mysql operator ex '!='
     * @param string $value the value to test $field on
     */
    private function getTablesByInformationSchema($field, $op, $value)
    {
        $conf = config('database');
        $schema = $conf['connections'][$conf['default']]['database'];
        $tables = DB::SELECT("SELECT table_name FROM information_schema.tables WHERE table_schema = :schema AND ".$field." ".$op." :value", ['schema' => $schema, 'value' => $value]);
        return array_map(function ($e) {return $e->table_name; }, $tables);
    }

    /**
     * Test if the cron job has not run between min and max minutes
     * @param int min minimum minutes to consider the test failed
     * @param int max maximum minutes to consider the test failed, -1 for infinity
     * @return string error
     */
    private function testCronRun($min, $max)
    {
        $tms = Storage::get(\App\Console\Kernel::CRON_LAST_RUN_FILE);
        if (!$tms) {
            if ($max == -1) {
                return __("Les tâches asynchrones n'ont jamais été exécutées");
            }
            return null;
        }
        $minutes = Carbon::createFromTimestamp($tms)->diffInMinutes(now());
        if ($minutes >= $min && ($max == -1 || $minutes < $max)) {
            return __("Les tâches asynchrones (cron) n'ont pas été executés depuis :count minutes", ["count" => $minutes]);
        }
        return null;
    }

    /**
     * Checks a php configuration variable value
     * @param string $name name of the php variable configuration
     * @param string $expected expected value of the variable
     * @param string $type type of the expectation min|max|eq
     * @param bool should we allow the special value 0
     * @return string|null warning/error message
     */
    private function testPhpConfig($name, $expected, $type, $allow_zero = false)
    {
        $value = (int)\App\Helper::parseHumanSize(ini_get($name));
        if ($value === 0 && $allow_zero === true) {
            return null;
        }
        $message = "La configuration php :config vaut :value alors qu'elle devrait être :qualificator :expected";
        $params = ['config' => $name, 'value' => $value, 'expected' => $expected];
        $error = false;
        if ($type === 'min') {
            $params['qualificator'] = __("au moins");
            if ($expected !== $value) {
                $error = true;
            }
        } elseif ($type === 'max') {
            $params['qualificator'] = __("au plus");
            if ($expected !== $value) {
                $error = true;
            }
        } else {
            $params['qualificator'] = __("égale à");
            if ($expected !== $value) {
                $error = true;
            }
        }
        if ($error) {
            return __(
                "La configuration php :config vaut :value alors qu'elle devrait être :qualificator :expected",
                $params
            );
        }
        return null;
    }

    /* ========================================================================
     *                              TESTS
     *  A test is a private method which name's starts by 'err' or 'warn'
     *  indicating if failing the test is a fatal issue or not.
     *  A test returns a html message (with a link to documentation if needed)
     *  if it fails or null if it succeed
     *  Test are run and displayed in natural sort order of method name.
     *  Therefore a test name should be
     *  warn001localCodeChanges
     *  or
     *  err001thisIsAVeryImportantConfig
     *  =======================================================================
     */


    /**
     * Checks if the code is locally changed in production
     */
    private function warn001localCodeChanges()
    {
        if ($this->production && (
            exec('LC_ALL=en_US.utf-8 git status -uno | grep "no changes added\|nothing to commit" -c') == 0 ||
            exec('LC_ALL=en_US.utf-8 git status -uno | grep "Changes not staged" -c') > 0)
        ) {
            return  __("Code localement modifié alors que l'environement est 'production'");
        }
        return null;
    }

    /**
     * Checks if there are some more recente commits on the current branch
     */
    private function warn002remoteCommitsAvailable()
    {
        $branch = \App\Helper::getGitBranch();
        if (exec('git rev-list --count HEAD..origin/'.$branch) != '0') {
            return __("Une nouvelle version est disponible, pensez à faire un <code>git pull</code>");
        }
        return null;
    }

    /**
     * Checks if xdebug is enabled (should be enable if and only if we are not in production)
     */
    private function warn003xdebugEnabled()
    {
        $xdebug = extension_loaded('xdebug');
        if ($this->production && $xdebug) {
            return  __("Xdebug est activé en production, veuillez désactiver cette extension");
        }
        if (!$this->production && !$xdebug) {
            return __("Xdebug n'est pas activé en mode local, il est conseillé de l'installer");
        }
        return null;
    }

    /**
     * Checks that the public storage is writable
     */
    private function warn004appWritable()
    {
        return $this->checkDirectory(base_path(), false);
    }

    /**
     * Checks that mail configuration fits environment
     */
    private function warn0005mailDriver()
    {
        $local_mail = in_array(config('mail.default'), ['log', 'array']);
        if ($this->production && $local_mail) {
            return __("L'envoi de mail n'est pas configuré alors que l'application est en mode production");
        }
        if (!$this->production && !$local_mail) {
            return __("L'envoi de mail est configuré alors que l'application est en mode de test");
        }
        return null;
    }


    /**
     * Check disk space
     */
    private function warn006discSpace()
    {
        $avail = ceil((int)exec('df storage  --output=avail | tail -n 1')/1024/1024);
        if ($avail < self::MIN_STORAGE) {
            return __("Espace de stockage bas, seulement :avail Gio restant", ["avail" => $avail]);
        }
        return null;
    }

    /**
     * Check extplatforms secret reuse
     */
    private function warn007secretReuse()
    {
        $count = DB::SELECT('SELECT count(*) AS count FROM extplatform GROUP BY (secret) HAVING COUNT > 1');
        if (!empty($count)) {
            return __(":count plateformes externes utilisent le même secret d'API", ["count" => $count[0]->count]);
        }
        return null;
    }

    /**
     * Check extplatforms secret lenght
     */
    private function warn008shortSecret()
    {
        $count = DB::SELECT('SELECT count(*) AS count FROM extplatform WHERE LENGTH(secret) < '.self::MIN_SECRET_LENGTH)[0]->count;
        if ($count) {
            return __(":count plateformes ont un jeton trop court (< :len caractères)", ["count" => $count, "len"=> self::MIN_SECRET_LENGTH]);
        }
        return null;
    }

    /**
     * Check extplatforms url reuse
     */
    private function warn009urlReuse()
    {
        $count = DB::SELECT('SELECT count(*) AS count FROM extplatform GROUP BY (url) HAVING COUNT > 1');
        if (!empty($count)) {
            return __(":count plateformes externes ont la même URL", ["count" => $count[0]->count]);
        }
        return null;
    }

    /**
     * check that all tables are utf8mb4
     */
    private function warn010utf8mb4tables()
    {
        $tables = $this->gettablesbyinformationschema('table_collation', 'NOT LIKE', 'utf8mb4_%');
        if (!empty($tables)) {
            $names = implode(',', $tables);
            $cmd = "ALTER TABLE table_name CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci";
            return __("Les tables <code>:tables</code> ne sont pas en <code>utf8mb4</code>. Pour convertir une table, utiliser la commande suivante <pre><code>:cmd</code></pre>", ["tables" => $names, "cmd" => $cmd]);
        }
        return null;
    }

    /**
     * check that all tables are using InnoDB
     */
    private function warn011InnoDBTables()
    {
        $tables = $this->gettablesbyinformationschema('engine', '!=', 'InnoDB');
        if (!empty($tables)) {
            $names = implode(',', $tables);
            $cmd = "ALTER TABLE table_name ENGINE=InnoDB";
            return __("Les tables <code>:tables</code> n'utilisent pas le moteur <code>InnoDB</code>. Pour convertir une table, utiliser la commande suivante <pre><code>:cmd</code></pre>", ["tables" => $names, "cmd" => $cmd]);
        }
        return null;
    }

/**
     * check that all tables are dynamic
     */
    private function warn012Dynamictables()
    {
        $tables = $this->gettablesbyinformationschema('ROW_FORMAT', '!=', 'Dynamic');
        if (!empty($tables)) {
            $names = implode(',', $tables);
            $cmd = "ALTER TABLE table_name ROW_FORMAT='Dynamic';";
            return __("Les tables <code>:tables</code> ne sont pas au format <code>Dynamic</code>. Pour convertir une table, utiliser la commande suivante <pre><code>:cmd</code></pre>", ["tables" => $names, "cmd" => $cmd]);
        }
        return null;
    }


    /**
     * Checks that cronjobs has runned recently
     */
    private function warn012CronJobNotRun()
    {
        return $this->testCronRun(self::CRON_WARN_MIN, self::CRON_ERR_MIN);
    }

    /**
     * Checks that the max upload size is equal to the expected one
     */
    private function warn013WrongMaxUploadSize()
    {
        return $this->testPhpConfig('post_max_size', (int)config("labnbook.php_expected_max_upload"), 'eq');
    }

    /**
     * Checks that the max post size is equal to the expected one
     */
    private function warn014WrongMaxPostSize()
    {
        return $this->testPhpConfig('upload_max_filesize', (int)config("labnbook.php_expected_max_upload"), 'eq');
    }


    /**
     * Checks that the max post size is equal to the expected one
     */
    private function warn015WrongMaxExecutionTime()
    {
        return $this->testPhpConfig('max_execution_time', (int)config("labnbook.php_recommended_max_execution_time"), 'min', true);
    }

    /**
     * Checks that the max post size is equal to the expected one
     */
    private function warn016WrongMaxInputTime()
    {
        return $this->testPhpConfig('max_input_time', (int)config("labnbook.php_recommended_max_input_time"), 'min', true);
    }

    /**
     * Checks that the max post size is equal to the expected one
     */
    private function warn017WrongMaxInputVars()
    {
        return $this->testPhpConfig('max_input_vars', (int)config("labnbook.php_recommended_max_input_vars"), 'min');
    }

    /**
     * Checks that the max post size is equal to the expected one
     */
    private function warn018WrongMemoryLimit()
    {
        return $this->testPhpConfig('memory_limit', (int)config("labnbook.php_recommended_memory_limit"), 'min', true);
    }

    /* ========================================================================
     *                              ERRORS
     *
     * ========================================================================
     */

    /**
     * Checks that the public storage is writable
     */
    private function err001publicStorageNotWritable()
    {
        return $this->checkDirectory(base_path('public/storage'), true);
    }

    /**
     * Checks that the public storage is writable
     */
    private function err002frameworkStorageNotWritable()
    {
        return $this->checkDirectory(storage_path('framework'), true);
    }

    /**
     * Checks if migrations are up to date
     */
    private function err003migrationMissings()
    {
        $customRunned = DB::table('migration')->pluck('filename')->toArray();
        $customFiles = $this->filesInDir(public_path('scripts/migrations'));
        $laravelRunned = DB::table('migrations')->pluck('migration')->toArray();
        $laravelFiles = $this->filesInDir(base_path('database/migrations'), ".php");
        $toRun = array_merge(
            array_diff($customFiles, $customRunned),
            array_diff($laravelFiles, $laravelRunned)
        );

        if (!empty($toRun)) {
            return __(
                "Des migrations n'ont pas été executées, veuillez executer la commande suivante ::cmd",
                ['cmd' => '<pre><code>'.base_path().'/refresh.sh</pre></code>']
            );
        }
        return null;
    }

    /**
     * Checks if scheluded jobs have run
     */
    private function err003cronNotRunning()
    {
        // TODO
        return null;
    }

    /**
     * Checks if CAS is configured ifff one insttitution is connected
     */
    private function err003casUsedNotConfigured()
    {
        $casInst = Institution::where('cas', 1)->first();
        if (config('cas.cas_hostname') != '') {
            $ldap = new Ldap();
            if (!$ldap->isCorrectlyInitialized()) {
                return __("Le CAS est configuré mais le Ldap est en erreur : ':err'", ['err' => $ldap->getError()]);
            }
            if (!$casInst) {
                return __("Le CAS est configuré mais n'est activé pour aucune institution");
            }
        } elseif ($casInst) {
            return  __("Le CAS est activé pour l'institution :inst mais pas configuré", ['inst' => $casInst->name]);
        }
        return null;
    }

    /**
     * Checks if there is more than one institution with CAS connected
     */
    private function err003moreThanOnCASInst()
    {
        if (Institution::where('cas', 1)->count() > 1) {
            return __("Plus d'une institution configurée pour utiliser le CAS, cela peut entrainer des problèmes de connexion");
        }
        return null;
    }

    /**
     * Checks if there are some institutions without teacher classes
     */
    private function err003instWithoutClasses()
    {
        $errors = Institution::whereNull('id_class_teacher')->count();
        if ($errors > 0) {
            return __("Il y a :count institutions sans classes enseignant.", ["count" => $errors]);
        }
        return null;
    }

    /**
     * Checks if there are some users without classes
     */
    private function err003userWithoutClasses()
    {
        $errors = DB::SELECT('SELECT COUNT(*) as count FROM user WHERE id_user NOT IN (SELECT id_user FROM link_class_learner)')[0]->count;
        if ($errors > 0) {
            return __("Il y a :count utilisateurs qui ne sont membre d'aucune classe.", ["count" => $errors]);
        }
        return null;
    }

    /**
     * Check php extensions
     */
    private function err004phpExtMissing()
    {
        $failed = [];
        foreach (self::REQUIRED_EXTS as $e) {
            if (!extension_loaded($e)) {
                $failed []= '<code>'.$e.'</code>';
            }
        }
        if (!empty($failed)) {
            return __("Extension php manquante(s) : :exts", ["exts" => implode(", ", $failed)]);
        }
        return null;
    }

    /**
     * Check if directory indexing is allowed
     */
    private function err005canIndexStorage()
    {
        $ch = curl_init(config('app.url').'/storage/');
        // This should timeout as it will be handled by
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $res = curl_exec($ch);
        //return json_encode(curl_getinfo($ch));
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if ($code > 0 && $code != 403) {
            return __("Le dossier <code>public/storage</code> est indexable, veuillez désactiver l'option index dans la configuration de votre serveur web");
        }
        return null;
    }

    /**
     * Checks php version
     */
    private function err006unsupportedPhpVers()
    {
        if (PHP_VERSION_ID  < self::PHP_MIN_VERS || PHP_VERSION_ID > self::PHP_UNSUPPORTED) {
            return __(
                "Vous utilisez php :vers qui n'est pas supporté, veuillez utiliser php >= :minvers, < :unsupported",
                [ 'vers' => phpversion(), "minvers" => self::PHP_MIN_VERS, "unsupported" => self::PHP_UNSUPPORTED]
            );
        }
        return null;
    }

    /**
     * Checks node version
     */
    private function err006unsupportedNodeVers()
    {
        $vers = exec('node --version');
        if (str_replace('v', '', explode('.', $vers)[0]) < self::NODE_MIN_VERS) {
            return __(
                "Version node trop ancienne :vers, veuillez <a href=':url' target='_blank'>installer nodejs</a> >= :min",
                ["vers" => $vers, "min" => self::NODE_MIN_VERS, "url" => "https://nodejs.org/en/"]
            );
        }
        return null;
    }

    /**
     * Checks that php datetime and mysql datetime are coherent
     */
    private function err007dateTimeIncoherent()
    {
        $mysql = DB::SELECT("SELECT NOW() as now")[0]->now;
        $php = date('Y-m-d H:i:s');
        if ($php != $mysql) {
            return __(
                "Date PHP (:date_php) et MySQL (:date_mysql) différentes.",
                ["date_php" => $php, "date_mysql" => $mysql]
            );
        }
        return null;
    }

    /**
     * Teachers without institutions
     */
    private function err008teachersWithoutInstitutions()
    {
        $count = DB::SELECT("SELECT COUNT(*) AS count FROM teacher t LEFT JOIN link_inst_teacher lit USING (id_teacher) WHERE lit.id_teacher IS NULL;")[0]->count;
        if ($count) {
            return __(":count enseignants non liés à des institutions, veuillez vous assurer que chaque enseignant ait au moins un lien dans la table <code>link_inst_teacher</code>", ["count" => $count]);
        }
        return null;
    }

    /**
     * Checks that cronjobs has runned recently
     */
    private function err009CronJobNotRun()
    {
        return $this->testCronRun(self::CRON_ERR_MIN, -1);
    }

    /*
     * Checks that scoring backend is properly configured
     */
    private function err012scoringEnabledNoURLPrefix()
    {
        if (!config('scoring.url_prefix')) {
            $var = 'SCORING_URL_PREFIX';
            $descr = __("ce régagle correspond doit être : <code>scoring/</code>");
            return __(
                "Les grilles critériées sont activées mais :varname n'est pas configuré :descr",
                ["varname" => $var, "descr" => $descr]
            );
        }
    }

    /*
     * Checks that scoring backend is properly configured
     */
    private function err013scoringEnabledNoLinkedClassActivity()
    {
        if (!config('scoring.linked_model_classes.activity')) {
            $var = 'SCORING_ACTIVITY_CLASS';
            $descr = __("ce régagle correspond doit être : <code>\App\Mission</code>");
            return __(
                "Les grilles critériées sont activées mais :varname n'est pas configuré :descr",
                ["varname" => $var, "descr" => $descr]
            );
        }
    }

    /*
     * Checks that scoring backend is properly configured
     */
    private function err014scoringEnabledNoLinkedClassProduction()
    {
        if (!config('scoring.linked_model_classes.production')) {
            $var = 'SCORING_PRODUCTION_CLASS';
            $descr = __("ce régagle correspond doit être : <code>\App\Production</code>");
            return __(
                "Les grilles critériées sont activées mais :varname n'est pas configuré :descr",
                ["varname" => $var, "descr" => $descr]
            );
        }
    }

    /*
     * Checks that scoring backend is properly configured
     */
    private function err015scoringEnabledNoLinkedClassSection()
    {
        if (!config('scoring.linked_model_classes.section')) {
            $var = 'SCORING_SECTION_CLASS';
            $descr = __("ce régagle correspond doit être : <code>\App\Mission</code>");
            return __(
                "Les grilles critériées sont activées mais :varname n'est pas configuré :descr",
                ["varname" => $var, "descr" => $descr]
            );
        }
    }
}
