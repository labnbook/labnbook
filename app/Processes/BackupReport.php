<?php

namespace App\Processes;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class BackupReport
{
    /** @var int */
    private $id;

    /** @var Backup */
    private $backupSystem;

    /** @var string */
    private $backupDir;

    public function __construct($id)
    {
        $this->id = (int) $id;
        $this->backupSystem = new Backup('report');
        $this->backupDir = $this->backupSystem->getBackupdir((int) $id);
    }

    /**
     * @return string
     */
    public function getBackupDir()
    {
        return $this->backupDir;
    }

    /**
     * Backup a mission and its dependencies into CSV files.
     *
     * @return boolean
     */
    public function backup()
    {
        $id = $this->id;
        $report = \App\Report::find($id);
        if (!$report) {
            return false;
        }

        if (!is_writable($this->backupDir)) {
            throw new Exception("Cannot write into the backup directory '$this->backupDir'.");
        }

        Log::Debug('report');
        $this->backupSystem->saveRecordsIntoCsv(
            [$report->toArray()],
            "{$this->backupDir}/report.csv"
        );
        Log::Debug('learners');
        $this->backupSystem->saveRecordsIntoCsv(
            DB::table('link_report_learner')->where('id_report', $id)->get()->toArray(),
            "{$this->backupDir}/link_report_learner.csv"
        );
        Log::Debug('ressources');
        $this->backupSystem->saveRecordsIntoCsv(
            DB::table('ressource')->where('id_report', $id)->get()->toArray(),
            "{$this->backupDir}/ressource.csv"
        );
        Log::Debug('labdoc');
        $this->backupSystem->saveRecordsIntoCsv(
            \App\Labdoc::Where('id_report', $id)->get()->toArray(),
            "{$this->backupDir}/labdoc.csv"
        );
        Log::Debug('labdoc stauts');
        $this->backupSystem->saveRecordsIntoCsv(
            DB::table('labdoc_status  as ls')
                ->join('labdoc as l', 'l.id_labdoc', 'ls.id_labdoc')
                ->where('l.id_report', $id)
                ->select('ls.*')
                ->get()->toArray(),
            "{$this->backupDir}/labdoc_status.csv"
        );
        Log::Debug('comment');
        $this->backupSystem->saveRecordsIntoCsv(
            DB::table('comment as c')
                ->join('labdoc as l', 'l.id_labdoc', 'c.id_labdoc')
                ->where('l.id_report', $id)
                ->select('c.*')
                ->get()->toArray(),
            "{$this->backupDir}/comment.csv"
        );
        Log::Debug('done');
        return true;
    }

    /**
     * Restore a report from its CSV backup. Fails if the mission exists.
     *
     * @return boolean
     */
    public function restore()
    {
        $report = \App\Report::find($this->id);
        if ($report) {
            return false; // do not overwrite
        }

        if (!is_dir($this->backupDir) || !is_readable($this->backupDir)) {
            throw new \Exception("Cannot read from the backup directory '{$this->backupDir}'.");
        }

        foreach(['report', 'link_report_learner', 'ressource', 'labdoc', 'labdoc_status', 'comment'] as $f) {
            if (!$this->backupSystem->loadFileIntoDb($this->backupDir, $f)) {
                throw new \Exception("Cannot restore '{$f}'.");
            }
        }
        return true;
    }
}
