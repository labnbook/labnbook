<?php

namespace App\Processes;

use Illuminate\Support\Facades\DB;

/**
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class Backup
{
    const BACKUP_ROOT = 'app/backup/';
    const NULL_STRING = '<NULL>';

    private $path;

    /**
     * @param string $category "mission" | "report"
     */
    public function __construct($category)
    {
        if (!in_array($category, ['mission', 'report'], true)) {
            throw new Exception("Wrong backup category.");
        }
        $this->path = storage_path(self::BACKUP_ROOT) . $category;
    }

    /**
     * Return the absolute path of backups, with NO terminal "/".
     *
     * @param int $id (optional)
     * @return string
     */
    public function getBackupdir($id = null)
    {
        $dir = $this->path . ($id ? '/' . (int) $id : '');
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }
        return $dir;
    }

    /**
     * @param array $records
     * @param string $file
     */
    public function saveRecordsIntoCsv($records, $file)
    {
        $records = $this->serialize($records);
        if (empty($records)) {
            return;
        }
        $handle = fopen($file, "w");
        fputcsv($handle, array_keys($records[0]), ';', '"');
        foreach ($records as $record) {
            $values = array_map(
                function ($x) {
                    return ($x === null ? self::NULL_STRING : $x);
                },
                array_values($record)
            );
            fputcsv($handle, $values, ';', '"');
        }
        fclose($handle);
    }

    /**
     *
     * @param string $dir
     * @param string $table
     * @return int #inserts
     */
    public function loadFileIntoDb($dir, $table)
    {
        $records = self::readRecords("$dir/$table.csv");
        return self::loadRecordsIntoDb($records, $table);
    }

    /**
     * @param string $file
     * @return array
     */
    public static function readRecords($file)
    {
        if (!is_readable($file)) {
            return [];
        }
        $handle = fopen($file, "r");
        if (!$handle) {
            return [];
        }
        $keys = fgetcsv($handle, 0, ';', '"');
        if (!$keys) {
            return [];
        }
        $records = [];
        while ($values = fgetcsv($handle, 0, ';', '"')) {
            if (count($keys) !== count($values)) {
                break;
            }
            $records[] = array_combine($keys, $values);
        }
        fclose($handle);
        return $records;
    }

    /**
     * @param array $records
     * @param string $table
     * @return int #inserts
     */
    private static function loadRecordsIntoDb($records, $table)
    {
        if (!$records) {
            return 0;
        }

        $records = self::restoreNull($records);
        return DB::table($table)
            ->insertOrIgnore(
                $records
            );
    }

    /**
     * Restore null values in records before loading them to DB
     */
    private static function restoreNull($records)
    {
        return array_map(function ($value) {
            return is_array($value) ? self::restoreNull($value) : ($value === self::NULL_STRING ? null: $value);
        }, $records);
    }

    /**
     * Serialize an Eloquent object or a collection
     */
    private function serialize($target)
    {
        return array_map(function ($value) {
            return is_object($value) ? (array)$value : $value;
        }, $target);
    }
}
