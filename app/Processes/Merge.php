<?php

namespace App\Processes;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

abstract class Merge
{
    /** @var $key of the table **/
    public const KEY = 'id';

    /** @var array Columns names Names use to reference user or teacher table **/
    public const ID_NAMES = [
    ];

    /** @var array table */
    /** lists the action to be run for each table, the order matters
     * The action function will receive two arguments :
     *      $tablename the name of the table in the first array
     *      $key the $key in the second array
     * format :
     * [
     *  'some_table_name' => [ 'action' => 'local_function_name', 'key' => 'keyname'],
     *  ....
     *  ]
     */
    public const TABLES = [
    ];


    /** Runs any required steps before merge
     * @throws \Exception
     */
    abstract protected function prepare();

    /** Runs any required steps before merge
     * @throws \Exception
     */
    abstract protected function finish($failed);

    /** @var stdclass */
    protected $source;
    /** @var stdclass */
    protected $target;

    /** @var boolean is this a dry run */
    protected $dryRun;

    /**
     * Initializes the merging process
     * @param stdclass $source the user to merge into $target which will be removed if everything goes well
     * @param stdclass $target the user that will be kept
     * @param boolean $dryRun
     */
    public function __construct($source, $target, $dryRun = true)
    {
        if (empty(static::TABLES) || empty(static::ID_NAMES) || static::KEY === null) {
            throw new \Exception("Not implemented : please override ID_NAMES, TABLES and key");
        }
        $this->source = $source;
        $this->target = $target;
        $this->dryRun = $dryRun;
    }

    /**
     * Checks that there are no tables referencing user that are not described in
     * $this->TABLES
     */
    public function ensureAllTablesAreDescribed()
    {
        foreach (static::ID_NAMES as $name) {
            $table = DB::table('INFORMATION_SCHEMA.COLUMNS')
                ->where('TABLE_SCHEMA', env('DB_DATABASE'))
                ->where('COLUMN_NAME', $name)
                ->whereNotIn('TABLE_NAME', array_keys(static::TABLES))
                ->first();
            if ($table) {
                throw new \Exception(
                    "Table {$table->TABLE_NAME} references {$name} but is not described in the merge process"
                );
            }
        }
    }

    /**
     * Updates the entries for the given tables
     * @param string $tablename
     * @param string $key
     */
    protected function update($tablename, $key)
    {
        $local_key = static::KEY;
        DB::Table($tablename)
            ->where($key, $this->source->$local_key)
            ->update([$key => $this->target->$local_key]);
    }

    /**
     * Updates the entries for the given tables, ignoring duplicates
     * @param string $tablename
     * @param string $key
     */
    protected function updateIgnore($tablename, $key)
    {
        $local_key = static::KEY;
        $ROWS = DB::table($tablename)
            ->where($key, $this->source->$local_key)
            ->get();
        foreach ($ROWS as $row) {
            $row->$key = $this->target->$local_key;
            // Insert row with target key ignoring errors
            DB::table($tablename)->insertOrIgnore((array)$row);
        }
        // Delete source rows
        $this->delete($tablename, $key);
    }

    /**
     * Updates if exists or create in case of duplicates runs a custom callback
     * to determine the max of the given column
     * @param string $tablename
     * @param string $key
     * @param string $compositekey
     * @param function $maxfield
     * @param function $maxfunction
     */
    protected function updateHandleDuplicateWithMax($tablename, $key, $compositekey, $maxfield, $maxfunction)
    {
        $local_key = static::KEY;
        $source_links = DB::table($tablename)
            ->where($key, $this->source->$local_key)
            ->get();
        foreach ($source_links as $src) {
            $exists = DB::table($tablename)
                ->where($key, $this->target->$local_key)
                ->where($compositekey, $src->$compositekey)
                ->exists();
            if (!$exists) {
                // Create link
                DB::table($tablename)
                    ->where($key, $this->source->$local_key)
                    ->where($compositekey, $src->$compositekey)
                    ->update([$key => $this->target->$local_key]);
            } else {
                // update last seen
                $value = DB::table($tablename)
                    ->where($key, $this->target->$local_key)
                    ->where($compositekey, $src->$compositekey)
                    ->pluck($maxfield)[0];
                $max = $maxfunction($src->$maxfield, $value);
                DB::table($tablename)
                    ->where($key, $this->target->$local_key)
                    ->where($compositekey, $src->$compositekey)
                    ->update([$maxfield => $max]);
            }
        }
    }

    /**
     * Deletes entries from the given table
     * @param string $tablename
     * @param string $key
     */
    protected function delete($tablename, $key)
    {
        $local_key = static::KEY;
        DB::Table($tablename)
            ->where($key, $this->source->$local_key)
            ->delete();
    }

    protected function logStep($level, $step, $message = "")
    {
        $name = static::class;
        $local_key = static::KEY;
        $more = $message ? ": '" . $message . "'" : "";
        Log::$level(
            "Process {$name} {$step} merging {$this->source->$local_key} into {$this->target->$local_key} {$more}"
        );
    }

    /**
     * Does the merge
     * Side effect : if everything goes well the $source user will be deleted
     * In dryRun mode queries are logged (info level) but nothing is actually done
     */
    public function run()
    {
        $this->logStep("Info", "start");
        try {
            DB::transaction(function () {
                // Checks that theire are no forgotten tables in $TABLES
                $this->ensureAllTablesAreDescribed();
                $this->prepare();

                // Update tables
                foreach (static::TABLES as $table_name => $params) {
                    $action = $params['action'];
                    $this->$action($table_name, $params['key']);
                }

                // Update versionning
                $this->finish(false);

                if ($this->dryRun) {
                    throw new \Exception("Merge aborted : this was only a dry run");
                }
            });
        } catch (\Exception $e) {
            $this->logStep("Error", "failed", $e->getMessage() . '\n' . JSON_ENCODE($e->getTrace(), JSON_PRETTY_PRINT));
            $this->finish(true/*this is a failure*/);
            return false;
        }
            $this->logStep("Info", "succeeded");
        return true;
    }
}
