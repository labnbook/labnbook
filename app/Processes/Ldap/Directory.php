<?php

namespace App\Processes\Ldap;

abstract class Directory
{
    /**
     * This class defines the required methods to implement a custom ldap directory
     */

    /** @var \Symfony\Component\Ldap\Ldap */
    protected $ldap;

    public function __construct($ldap)
    {
        $this->ldap = $ldap;
    }

    /**
     * Returns a LdapUser for the given uid
     * @param string $uid
     * @return \App\Processes\LdapUser|null
     */
    abstract public function getUserByUid($uid);


    /**
     * Query the LDAP about this email, returns the first match
     *
     * @param string $email email
     * @return LdapUser|null
     */
    abstract public function getUserByEmail($email);

    /**
     * Query the LDAP about this student number
     * @param string $number
     * @return \App\Processes\LdapUser|null
     */
    abstract public function getStudentByNumber($number);

    /**
     * Query the LDAP for all the students matching these student numbers
     *
     * @param array $numbers
     * @return LdapUser[]|null
     */
    abstract public function getStudentsByNumbers($numbers);

    /**
     * Returns a LdapUser from its attributes (deserialzation)
     * @param array $attributes
     * @return \App\Processes\LdapUser
     */
    abstract public function getLdapUser($attributes);
}
