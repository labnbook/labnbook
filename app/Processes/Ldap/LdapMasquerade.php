<?php

namespace App\Processes\Ldap;

class LdapMasquerade extends Agalan
{
    /** @var array */
    private $users;

    public function __construct($config)
    {
        $this->users = $config['masquerade_users'];
    }

    /**
     * @inheritdoc
     */
    public function getUserByUid($uid)
    {
        foreach ($this->users as $u) {
            if (self::isEqual($u['uid'], $uid)) {
                return new AgalanUser($u);
            }
        }
        return null;
    }

    /**
     * @inheritdoc
     */
    public function getUserByEmail($email)
    {
        foreach ($this->users as $u) {
            if (self::isEqual($u['mail'], $email)) {
                return new AgalanUser($u);
            }
        }
        return null;
    }

/**
     * @inheritdoc
     */
    public function getStudentByNumber($number)
    {
        $users = $this->getStudentsByNumbers([$number]);
        return $users ? $users[0] : null;
    }

    /**
     * @inheritdoc
     */
    public function getStudentsByNumbers($numbers)
    {
        $aglnOrganizationUids = [];
        foreach (self::$prefixes as $prefix) {
            foreach ($numbers as $number) {
                foreach ($this->numberPatterns as $pattern) {
                    $id = str_replace(
                        ['{{INST}}', '{{NUM}}'],
                        [$prefix, $number],
                        $pattern
                    );
                    $aglnOrganizationUids[$id] = 1;
                }
            }
        }

        $users = [];
        foreach ($this->users as $u) {
            foreach ($u['aglnOrganizationUid'] as $id) {
                if (isset($aglnOrganizationUids[$id])) {
                    $users[] = new AgalanUser($u);
                }
            }
        }
        return $users;
    }

    /**
     * Are the strings the same, ignoring case?
     *
     * @param string $a
     * @param string $b
     * @return bool
     */
    protected static function isEqual($a, $b)
    {
        // far from perfect (lacks Unicode normalization + ambiguous lowercasing)
        return mb_strtolower($a) === mb_strtolower($b);
    }
}
