<?php

namespace App\Processes\Ldap;

use Symfony\Component\Ldap\Adapter\ExtLdap\Adapter;
use Symfony\Component\Ldap\Ldap as SymfonyLdap;
use \App\User;
use Illuminate\Support\Facades\Log;
use Symfony\Component\Ldap\Exception\ConnectionException;

class Ldap
{
    protected static $adapter_keys = ['host', 'port', 'encryption'];

    /** @var \Symfony\Component\Ldap\Ldap */
    private $ldap;

    /** @var \App\Processes\Directory */
    private $directory;

    protected $error = '';

    /**
     * Init an instance of Ldap https://symfony.com/doc/current/components/ldap.html
     * @param array|null $config
     * @param bolean sendReportOnError 
     *
     */
    public function __construct($config=null, $sendReportOnError = true)
    {
        if (!$config) {
            $config = config('ldap');
        }
        try {
            if ($config['masquerade']) {
                // To pass test isCorrectlyInitialized
                if (config('ldap.simulate_error') === "1") {
                    throw new ConnectionException('Debug exception');
                }
                $this->ldap = (object) [1];
                $this->directory = new LdapMasquerade($config);
                return;
            }
            if (empty($config['host'])) {
                $this->ldap = null;
                $this->error = "No ldap host configured";
                return;
            }
            $adapter = new Adapter(array_intersect_key($config, array_flip(self::$adapter_keys)));
            if (config('ldap.simulate_error') === "1") {
                throw new ConnectionException('Debug exception');
            }
            $this->ldap = new SymfonyLdap($adapter);
            $this->ldap->bind($config['credentials_bind'], $config['credentials_password']);
            $directoryClassName = config('labnbook.ldap_directory_class');
            $this->directory = new $directoryClassName($this->ldap);
        } catch (\Exception $ex) {
            if ($sendReportOnError) {
                \App\Helper::reportError($ex);
            } else {
                Log::error($ex);
            }
            $this->error = $ex->getMessage();
            $this->ldap = null;
            $this->directory = null;
        }
    }

    /**
     * Checks that the initialization is ok or dies
     * @throws \Exception
     */
    private function requiresCorrectInitialization()  {
        if (!$this->isCorrectlyInitialized()) {
            throw new \Exception(
                __(
                    "L'annuaire institutionnel (:name) est temporairement injoignable. Veuillez ré-essayer plus tard",
                    ['name' => config('labnbook.cas_name')]
                )
            );
        }
    }

    /**
     * If $ldapUser is not null, and a local user exists corresponding to this
     * ldap user without ambiguïties, updates the local user's data from the
     * ldap
     * Returns the $ldapUser given as a parameter
     * @param $ldapUser
     * @param Boolean $syncTeacherStatus whether to synchronize or not the teacher status with lnb
     * @retun $ldapUser
     */
    private function handleLdapLookupResponse($ldapUser, $syncTeacherStatus=false)
    {
        if ($ldapUser) {
            \App\User::handleLdapLookupResponse($ldapUser, $syncTeacherStatus);
        }
        return $ldapUser;
    }

    /**
     * Query the LDAP about this unique username (AKA login)
     *
     * @param string $username username AKA login
     * @param Boolean $syncTeacherStatus whether to synchronize or not the teacher status with lnb
     * @return LdapUser|null
     */
    public function getUserByUsername($username, $syncTeacherStatus=false)
    {
        $this->requiresCorrectInitialization();
        return $this->handleLdapLookupResponse($this->directory->getUserByUid($username), $syncTeacherStatus);
    }

    /**
     * Query the LDAP about this email, returns the first match
     *
     * @param string $email email
     * @return LdapUser|null
     */
    public function getUserByEmail($email)
    {
        $this->requiresCorrectInitialization();
        return $this->handleLdapLookupResponse($this->directory->getUserByEmail($email));
    }

/**
     * Query the LDAP about this student number
     *
     * @param string $institute
     * @param string $number
     * @return LdapUser|null
     */
    public function getStudentByNumber($number)
    {
        $this->requiresCorrectInitialization();
        $ldapUser = $this->directory->getStudentByNumber($number);
        $this->handleLdapLookupResponse($ldapUser);
        return $ldapUser;
    }

    /**
     * Query the LDAP for all the students matching these student numbers (APOGÉE).
     *
     * @param array $numbers
     * @return LdapUser|null
     */
    public function getStudentsByNumbers($numbers)
    {
        $this->requiresCorrectInitialization();
        $results = $this->directory->getStudentsByNumbers($numbers);
        foreach($results as $ldapUser) {
            $this->handleLdapLookupResponse($ldapUser);
        }
        return $results;
    }

    /**
     * Switch this user to a CAS auth.
     * If possible, update the (first, last) name from LDAP, then return true.
     *
     * @param User $user
     * @param string $login
     * @return bool updated?
     */
    public function switchToCasAuth(User $user, string $login): bool
    {
        if (!$this->isCorrectlyInitialized()) {
            return false;
        }
        // update user profile with LDAP info
        $updated = false;
        $fields = [];
        $ldapUser = $this->directory->getUserByUid($login);
        if ($ldapUser) {
            $fields['user_name'] = $ldapUser->getLastName();
            $fields['first_name'] = $ldapUser->getFirstName();
            $updated = true;
        }
        // switch to CAS auth
        $fields['login'] = $login;
        $user->switchToCas($fields);
        Log::info("User {id: {$user->id_user}} has switched to CAS authentication.");
        return $updated;
    }

    /**
     * Create or update a student account using LDAP.
     *
     * @param string $username
     * @param int $instId
     * @param string $email
     * @return User
     */
    public function createStudent(string $username, int $instId, string $email): ?User
    {
        $this->requiresCorrectInitialization();
        $ldapUser = $this->directory->getUserByUid($username);
        if (!$ldapUser) {
            return null;
        }

        $inst_number = $ldapUser->getApogeeId();
        if ($inst_number) {
            $userByStdNum = User::where([['id_inst', $instId], ['inst_number', $inst_number]])->first();
        } else {
            $userByStdNum = null;
        }
        if (!$email) {
            $email = $ldapUser->getEmail();
        }
        if (!$email && isset($userByStdNum->email)) {
            $email = $userByStdNum->email; // keep DB email unless a new one is provided
        }
        if (!$email) {
            $email = "";
        }
        if ($userByStdNum) {
            // migrate DB user to CAS
            $user = $userByStdNum;
            $fields = ['login' => $username, 'user_name' => $ldapUser->getLastName(), 'first_name' => $ldapUser->getFirstName(), 'email' => $email];
            if ($user->switchToCas($fields)) {
                \App\Helper::addAlert("info", __("Votre connexion se fera désormais par votre compte institutionnel (:name).", ['name' => config('labnbook.CAS_NAME')]));
            }
        } else {
            $user = \App\User::Create([
                'login' => $username,
                'user_name' => $ldapUser->getLastName(),
                'first_name' => $ldapUser->getFirstName(),
                'email' => $email,
                'id_inst' => $instId,
                'inst_number' => $ldapUser->getApogeeId(),
            ]);
            if ($user->id_user) {
                \App\Helper::addAlert("success", __("Vous avez été inscrit à LabNBook avec votre compte institutionnel (:name).", ['name' => config('labnbook.cas_name')]));
            } else {
                Log::error("CAS create user failed: " . print_r($user, true));
            }
        }
        return $user;
    }

    /**
     * Returns a LdapUser from its attributes
     * @param array $attributes
     * @return \App\Processes\LdapUser
     */
    public function getLdapUser($attributes)
    {
        return $this->directory->getLdapUser($attributes);
    }

    /**
     * @param LdapUser $ldapUser
     * @return User
     */
    public function mapLdapUserToUser(LdapUser $ldapUser): User
    {
        $user = new User;
        $user->fill([
            'login' => $ldapUser->uid,
            'user_name' => $ldapUser->getLastName(),
            'first_name' => $ldapUser->getFirstName(),
            'id_inst' => $ldapUser->getInstitutionId(),
            'inst_number' => null,
            'email' => $ldapUser->getEmail(),
        ]);
        return $user;
    }

    /**
     * Checks if the ldap is correctly initialized
     * @return boolean
     */
    public function isCorrectlyInitialized()
    {
        return $this->ldap != null && !$this->hasError() && $this->directory;
    }

    /**
     * @return boolean
     */
    public function hasError()
    {
        return !empty($this->error);
    }

    /**
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * Return true if the $institute has access to ldap
     * @param string|int $institute ID or name
     * @return boolean
     */
    public static function hasInstitute($institute)
    {
        if (is_numeric($institute)) {
            $inst = \App\Institution::find($institute);
        } else {
            $inst = \App\Institution::where('name', $institute);
        }
        return $inst != null && $inst->hasCas();
    }

    /**
     * Does the class belongs to an institution with ldap
     * @param int $id_class id of the class
     * @return boolean
     */
    public static function classHasLdap($id_class)
    {
        $class = \App\Classe::find($id_class);
        return $class && $class->institution->hasCas();
    }
}
