<?php

namespace App\Processes\Ldap;
use \Illuminate\Support\Facades\Log;

class AgalanUser extends LdapUser
{
    /** @var string Full name */
    public $cn;

    /** @var string First name */
    public $givenName;

    /** @var string Last name (family) */
    public $sn;

    public $o;
    /** @var array Student identifiers (BIPER, APOGEE) */
    public $aglnOrganizationUid;
    /** @var string max status */
    public $aglnPrimaryOrganizationName;

    /** @var string max status */
    public $aglnPersonStatus;
    public $employeeType;
    public $objectClass;

    /** @var string Preferred email address */
    public $mail;

    /** @var string nomail | normal | rcopy | rfull | ext */
    public $aglnMailStatus;

    /** @var array */
    public $aglnMailEffectiveAddr;

    /** @var array */
    public $aglnMailRedirectAddr;

    /** @var array teachers' employeeType */
    const teachType = [ 'P'];
    /** @var etumail student prefix on email address */
    const etuMailPattern = '/@etu[\.-]/';

    /**
     * @inheritdoc
     */
    public function getFirstName()
    {
        return $this->givenName;
    }

    /**
     * @inheritdoc
     */
    public function getLastName()
    {
        return $this->sn;
    }

    /**
     * @inheritdoc
     */
    public function getEmail()
    {
        return $this->mail;
    }

    public function __construct(array $attributes)
    {
        $stringTypes = ['uid', 'cn', 'givenName', 'sn', 'aglnPrimaryOrganizationName', 'aglnPersonStatus', 'mail', 'aglnMailStatus'];
        foreach ($attributes as $k => $v) {
            if (in_array($k, $stringTypes)) {
                if (is_string($v)) {
                    $this->$k = $v;
                } else {
                    if (count($v) > 1) {
                        error_log("LDAP: field $k should contain a string, array found: " . print_r($v, true));
                    }
                    $this->$k = (string) $v[0];
                }
            } else {
                $this->$k = $v;
            }
        }
    }

    /**
     * Return the APOGEE identifier (student number, only digits) from this LDAP record.
     *
     * @param array $numbers (opt) Preferred numbers
     * @return string
     */
    public function getApogeeId($numbers = [])
    {
        $m = [];
        $lastNumber = null;
        foreach ($this->aglnOrganizationUid as $id) {
            foreach (Agalan::getApogeePatterns() as $pattern) {
                if (preg_match($pattern, $id, $m)) {
                    $lastNumber = $m[1];
                    if (in_array($lastNumber, $numbers)) {
                        return $lastNumber;
                    }
                }
            }
        }
        return $lastNumber;
    }

    /**
     * @inheritdoc
     */
    public function getInstitutionName(): string
    {
        return $this->aglnPrimaryOrganizationName === 'inpg' ? 'INPG' :
            ($this->aglnPrimaryOrganizationName === 'uds' ? 'UDS' : 'UGA');
    }

    /**
     * @inheritdoc
     */
    public function isTeacher()
    {
        if (empty(array_diff(self::teachType, $this->employeeType))
            && $this->aglnPersonStatus === 'OFFI'
        ) {
            foreach (explode(',', $this->aglnMailEffectiveAddr[0]) as $addr) {
                if (!preg_match(self::etuMailPattern, $addr)) {
                    return true;
                }
            }
        }
        return false;
    }
}
