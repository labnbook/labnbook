<?php

namespace App\Processes\Ldap;

use Illuminate\Support\Facades\Log;
use App\User;

class Agalan extends Directory
{
    protected static $prefixes = ['u123', 'inpg', 'uds'];

    /** @var array The strings {{INST}} and {{NUM}} will be replaced. */
    protected $numberPatterns = [
        "{{INST}}_apo-{{NUM}}", // e.g. "u123_apo-12345678"
        "udg_apo-{{NUM}}", // for Grenoble's PhD students
    ];

    protected $baseDn = 'ou=people,ou=uga,dc=agalan,dc=org';
    protected $userFilter = 'objectClass=person';

    /**
     * Returns the patterns for matching apogee numbers
     * @return array
     */
    public static function getApogeePatterns()
    {
        $patterns = [];
        foreach (array_merge(self::$prefixes, ['udg']) as $prefix) {
            $patterns []= '/'.$prefix.'_apo-(\d+)$/';
        }
        return $patterns;
    }

    /**
     * @param string $operator "|" or "&"
     * @param string[] $conditions
     * @return string
     */
    protected static function combineConditions($operator, $conditions)
    {
        if (count($conditions) > 1) {
            return "($operator" . join("", $conditions) . ")";
        } else {
            return $conditions[0];
        }
    }

    /**
     * @param string $prefix
     * @param int $number
     * @return string[]
     * @throws Exception
     */
    protected function buildNumberConditions($prefix, $number)
    {
        $aglnOrganizationUids = [];
        foreach ($this->numberPatterns as $pattern) {
            $aglnOrganizationUids[] = $this->ldap->escape(
                str_replace(['{{INST}}', '{{NUM}}'], [$prefix, $number], $pattern)
            );
        }
        if (!$aglnOrganizationUids) {
            throw new Exception("no LDAP condition");
        }
        return array_map(
            function ($x) {
                return "(aglnOrganizationUid=$x)";
            },
            $aglnOrganizationUids
        );
    }

    /**
     * @inheritdoc
     */
    public function getUserByUid($uid)
    {
        $uid = $this->ldap->escape($uid);
        $query = $this->ldap->query($this->baseDn, "(&({$this->userFilter})(uid=$uid))");
        $users = $query->execute()->toArray();
        return $users ? new AgalanUser($users[0]->getAttributes()) : null;
    }

    /**
     * @inheritdoc
     */
    public function getUserByEmail($email)
    {
        $uid = $this->ldap->escape($email);
        $query = $this->ldap->query($this->baseDn, "(&({$this->userFilter})(mail=$email))");
        $users = $query->execute()->toArray();
        return $users ? new AgalanUser($users[0]->getAttributes()) : null;
    }


    /**
     * @inheritdoc
     */
    public function getStudentByNumber($number)
    {
        foreach (self::$prefixes as $prefix) {
            $o = $this->ldap->escape($prefix);
            $condition = self::combineConditions("|", $this->buildNumberConditions($prefix, $number));
            $query = $this->ldap->query(
                $this->baseDn,
                "(&({$this->userFilter})(o=$o)$condition)"
            );
            $users = $query->execute()->toArray();
            if ($users) {
                return new AgalanUser($users[0]->getAttributes());
            }
        }
        return null;
    }

    /**
     * @inheritdoc
     */
    public function getStudentsByNumbers($numbers)
    {
        $users = [];
        $count_expected = count($numbers);
        foreach (self::$prefixes as $prefix) {
            $o = $this->ldap->escape($prefix);
            $uidConditions = [];
            foreach ($numbers as $number) {
                foreach ($this->buildNumberConditions($prefix, $number) as $c) {
                    $uidConditions[] = $c;
                }
            }
            $query = $this->ldap->query(
                $this->baseDn,
                sprintf("(&({$this->userFilter})(o=%s)(|%s))", $o, self::combineConditions("|", $uidConditions))
            );
            $result = $query->execute()->toArray();
            foreach ($result as $u) {
                $users[] = new AgalanUser($u->getAttributes());
            }
            if (count($users) >= $count_expected) {
                break;
            }
        }
        return $users;
    }

    /**
     * @inheritdoc
     */
    public function getLdapUser($attributes)
    {
        return new AgalanUser($attributes);
    }
}
