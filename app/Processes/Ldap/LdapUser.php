<?php

namespace App\Processes\Ldap;

abstract class LdapUser
{
    /** @var string unique ID (login) */
    public $uid;

    /**
     * Creates the user
     * @param array $attributes as returned by ldap
     */
    abstract public function __construct(array $attributes);

    /**
     * Returns the users first name
     * @return string
     */
    abstract public function getFirstName();

    /**
     * Returns the users last name
     * @return string
     */
    abstract public function getLastName();

    /**
     * Returns the users email
     * @return string
     */
    abstract public function getEmail();

    /**
     * Return the APOGEE identifier (student number, only digits) from this LDAP record.
     *
     * @param array $numbers (opt) Preferred numbers
     * @return string
     */
    abstract public function getApogeeId($numbers = []);


    /**
     * Get the name of the user's institution
     * @return string
     */
    abstract public function getInstitutionName(): string;

    /**
     * Get the id of the user's institution in LabNBook db
     * @return int
     */
    public function getInstitutionId(): int
    {
        return \App\Institution::where('cas', 1)->select('id_inst')->first()->id_inst;
    }



    /**
     * Checks if the ldapUser is a teacher
     * @return boolean
     */
    abstract public function isTeacher();
}
