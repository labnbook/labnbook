<?php

namespace App\Processes;

use Illuminate\Support\Facades\Log;
use App\User;

class GDPR
{
    /** @var User $user user */
    protected $user;

    /** @var \Illuminate\Database\Eloquent\Collection|\App\Report[] $reports */
    protected $reports;

    public function __construct($user, $format)
    {
        $this->user = $user;
        $this->format = $format;
    }

    /**
     * Returns the GDPR dump in the requested format
     */
    public function generateDump()
    {
        $data = $this->getRawData();
        if ($this->format === 'zip') {
            $data = $this->toZipArchive($data);
        }
        return $data;
    }


    /**
     * Generate user data dump in JSON format
     */
    public function getRawData()
    {
        // Retrieve user data
        $methods = ['getUserInfo', 'getTeacherInfo', 'getReports', 'getLabdocs', 'getComments', 'getConversations', 'getMessages'];
        $data = [];
        foreach ($methods as $m) {
            $name = str_replace('get', '', $m); // UserInfo.csv
            $data[$name] = $this->$m();
        }
        return $data;
    }

    /**
     * Generate a zip file containing all user data as CSV files
     */
    public function toZipArchive($data)
    {
        // Prepare a zip file
        $zip = new \ZipArchive();
        $zipName = tempnam(sys_get_temp_dir(), "rgpd_dump_".$this->user->id_user);
        if ($zip->open($zipName, \ZipArchive::OVERWRITE) !== true) {
            Log::Error("Error while creating ZipArchive");
            return null;
        }
        foreach ($data as $key => $infos) {
            $name = $key.'.csv'; // UserInfo.csv
            $contents = $this->arrayToCsv($infos, $name);
            if (!empty($contents)) {
                $zip->addFromString($name, $contents);
            }
        }
        $zip->close();
        return $zipName;
    }

    /**
     * Create a csv file containning the data of $array
     * @param array $data
     * @param string $name the file name
     * @return $string
     */
    protected function arrayToCsv($data, $name)
    {
        if (empty($data)) {
            return "";
        }
        $csv = fopen('php://temp', 'w');
        $separator = ";";
        fputcsv($csv, array_keys($data[0]), $separator);
        foreach ($data as $row) {
            fputcsv($csv, array_values($row), $separator);
        }
        $result = stream_get_contents($csv, -1, 0);
        fclose($csv);
        return $result;
    }

    /**
     * Return all user related info as an array
     * @return array
     */
    protected function getUserInfo()
    {
        return [$this->user->toArray()];
    }

    /**
     * Return all teacher related info as an array
     * @return array
     */
    protected function getTeacherInfo()
    {
        if ($this->user->hasTeacher()) {
            return [$this->user->teacher->toArray()];
        }
        return null;
    }

    /**
     * Return all reports related to the user
     * @return array
     */
    protected function getReports()
    {
        $this->reports = $this->user->reports()->withTrashed()->get();
        $reports = $this->reports->toArray();
        foreach ($reports as $id => $r) {
            unset($reports[$id]['pivot']);
        }
        return $reports;
    }

    /**
     * Return all labdocs related to the user
     * @return array
     */
    protected function getLabdocs()
    {
        $labdocs = [];
        foreach ($this->reports as $r) {
            $labdocs = array_merge($labdocs, $r->labdocs->toArray());
            $labdocs = array_merge($labdocs, $r->sharedLabdocs->toArray());
        }
        return $labdocs;
    }

    /**
     * Return all comments written by the user
     * @return array
     */
    protected function getComments()
    {
        return \App\Comment::where('id_user', $this->user->id_user)->get()->toArray();
    }

    /**
     * Return all messages written by the user
     * @return array
     */
    protected function getConversations()
    {
        $data = $this->user->conversations;
        $data->map(
            function ($value) {
                unset($value->pivot);
            }
        );
        return $data->toArray();;
    }

    /**
     * Return all messages written by the user
     * @return array
     */
    protected function getMessages()
    {
        return \App\Message::where('id_user_sender', $this->user->id_user)->get()->toArray();
    }
}
