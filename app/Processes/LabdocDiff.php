<?php

namespace App\Processes;

use App\Helper;
use App\Labdoc;
use App\LabdocVersioning;
use App\Report;
use App\User;
use Caxy\HtmlDiff\HtmlDiff;
use Caxy\HtmlDiff\HtmlDiffConfig;
use Illuminate\Support\Facades\Log;
use phpDocumentor\Reflection\Types\Boolean;
use phpDocumentor\Reflection\Types\Integer;

class LabdocDiff
{
    /**
     * @const string[]: array of allowed labdoc types for diff computing
     */
    const LABDOC_ALLOWED_TYPES = array(Labdoc::TEXT);


    /**
     * @var User: current user
     */
    private User $user;


    /**
     * @var Labdoc: current labdoc
     */
    private Labdoc $labdoc;


    /**
     * @var HtmlDiffConfig: diff configuration object
     */
    private HtmlDiffConfig $config;

    
    /**
     * @var string: first string to compare
     */
    private string $lastLdContent = '';
  
    
    /**
     * @var string: second string to compare
     */
    private string $currentLdContent = '';


    /**
     * @var string: difference
     */
    private string $difference = '';
    
    private bool $sameVersion=false;
    
    private array $dataVersioning;
    
    
    public function __construct($user, $labdoc, $lastLdIdVersion, $currentLdIdVersion)
    {
        $this->user = $user;
        $this->labdoc = $labdoc;
        $this->config = new HtmlDiffConfig();
        self::setConfig();
        
        // Avoid error during unit testing
        if (($lastLdIdVersion != -1) || ($currentLdIdVersion != -1)){
            $versioning = new LabdocVersioning($this->labdoc->report->id_mission);
            $loadedVersioning = $versioning->load($this->labdoc->id_labdoc);
        } else {
            $loadedVersioning = null;
        }
        if($loadedVersioning){
           $this->dataVersioning = array_map(function($version) {
               $version->date = \App\Helper::datetimeToFr(date('Y-m-d', $version->ts));
               return $version;
           },$loadedVersioning->contents);
        } else {
            $this->dataVersioning = [];
        }
        // Retreive lastLdContent
        if ((is_numeric($lastLdIdVersion)) && (intval($lastLdIdVersion) > 0)) {
            $dataLast = $versioning->getData($this->labdoc->id_labdoc, intval($lastLdIdVersion));
            $this->lastLdContent = $dataLast->data;
        } else {
            // If this is a pre-filled labdoc and the teacher hasn't reviewed yet, compute diff between pre-filled origin and current_version
            if ($labdoc->id_ld_origin !== null){
                $this->lastLdContent = \App\Labdoc::find($labdoc->id_ld_origin)->labdoc_data;
            }
        }

        // Retreive currentLdContent
        if ((is_numeric($currentLdIdVersion)) && (intval($currentLdIdVersion) > 0)) {
            if ((is_numeric($lastLdIdVersion)) && (intval($lastLdIdVersion) > 0) && (intval($lastLdIdVersion) == intval($currentLdIdVersion))) {
                $this->currentLdContent = $this->lastLdContent;
            } else {
                $dataCurrent = $versioning->getData($this->labdoc->id_labdoc, intval($currentLdIdVersion));
                $this->currentLdContent = $dataCurrent->data;
            }
        } else {
            if ($this->labdoc->labdoc_data !== null) {
                $this->currentLdContent = $this->labdoc->labdoc_data;
            }
        }
    }


    /**
     * Allow to set manually lastLdContent (test purpose)
     */
    public function setLastLdContent($content)
    {
        $this->lastLdContent = $content;
    }


    /**
     * Allow to set manually currentLdContent (test purpose)
     */
    public function setCurrentLdContent($content)
    {
        $this->currentLdContent = $content;
    }

    /**
     * Returns the diff between two labdocs
     * @return string the html diff or an empty string
     */
    public function computeDiff()
    {
        if ($this->lastLdContent !== null) {
            if ($this->labdoc->type_labdoc === 'text'){
                $this->dataKatexPreProcessing();
                $this->imagesPreProcessing();
            }

            $htmlDiff = HtmlDiff::create($this->lastLdContent, $this->currentLdContent, $this->config);

            try {
                $this->difference = $htmlDiff->build();
                if($this->difference == $this->currentLdContent) {
                    $this->sameVersion = true;
                }
            } catch (\Exception $e) {
                Log::Error($e->getMessage());
                return '';
            }

            if ($this->labdoc->type_labdoc === 'text') {
                $this->imagesPostProcessing();
                $this->dataKatexPostProcessing();
            }

            return $this->difference;
        } else {
            Log::error("Labdoc content is null");
            return '';
        }
    }


    /**
     * Retreives the diffs of a given labdoc
     * @param Labdoc $labdoc
     * @param User $user
     * @param array $parameters (keys : 'ld_to_diff' : [{'id_ld':, 'last_ld_id_version_reviewed':, 'current_ld_id_version':}, ...])
     * @return array[] differences with key; labdoc id, value: string diff
     */
    public static function getDiffs(Labdoc $labdoc, User $user, array $parameters)
    {
        abort_if($user->cannot('manage', $labdoc->report), Helper::pullPolicyErrorCode(), Helper::pullPolicyErrorMessage());
        $lastLdIdVersion = $parameters['ld_to_diff']['last_ld_id_version_reviewed'];
        $currentLdIdVersion = $parameters['ld_to_diff']['current_ld_id_version'];
        $labdocDiff = new LabdocDiff($user, $labdoc, $lastLdIdVersion, $currentLdIdVersion);
        return ['diff_content' => $labdocDiff->computeDiff(), 'versioning' => $labdocDiff->dataVersioning, 'same_version' => $labdocDiff->sameVersion];
    }


    /**
     * Check if at least one mod_icon is available (among authorized labdoc diff types) for a given ($report, $user)
     * @param Report $report
     * @param User $user
     * @return Boolean 
     */
    public static function isReportDiffable(Report $report, User $user)
    {
        return $report
            ->labdocs()
            ->get()
            ->filter(fn($labdoc) => in_array($labdoc->type_labdoc, LabdocDiff::LABDOC_ALLOWED_TYPES))
            ->filter(fn($labdoc) => $labdoc->getModIconStatus($user) == 1)
            ->reduce(function ($carry, $item) {
                return ($carry || (bool) $item);
            }, false);
    }
    
    /**
     * Sets the configuration parameters
     */
    private function setConfig()
    {
        $this->config->setSpecialCaseChars(array('.', ',', '(', ')', '\''));
        $this->config
            ->setMatchThreshold(80)
            ->setEncoding('UTF-8')
            ->setInsertSpaceInReplace(false)
            ->setUseTableDiffing(true)
            ->setCacheProvider(null)
            ->setPurifierEnabled(false)
            ->setPurifierCacheLocation(null)
            ->setGroupDiffs(true)
            ->setIsolatedDiffTags(array(
                'a' => '[[REPLACE_A]]',
                'img' => '[[REPLACE_IMG]]',
                //'p' => '[[REPLACE_P]]',
                'span' => '[[REPLACE_SPAN]]',
                'table' => '[[REPLACE_TABLE]]',
            ))
            ->setKeepNewLines(false);
    }


    /**
     * Adds a space to span.datakatex to make diffing catch insertion or deletion
     * Then toggles span.datakatex to a.href to make diffing catch modification 
     */
    private function dataKatexPreProcessing()
    {
        $sourceTag = 'span';
        $sourceAttr = 'data-katex';
        $content = '[[]]';
        $targetTag = 'a';
        $targetAttr = 'href';
        $targetAttrToRemove = null;
        
        $this->lastLdContent = self::fillEmptyNode($this->lastLdContent, $sourceTag, $sourceAttr, $content);
        $this->lastLdContent = self::replaceTag($this->lastLdContent, $sourceTag, $sourceAttr, $targetTag, $targetAttr, $targetAttrToRemove);
        $this->currentLdContent = self::fillEmptyNode($this->currentLdContent, $sourceTag, $sourceAttr, $content);
        $this->currentLdContent = self::replaceTag($this->currentLdContent, $sourceTag, $sourceAttr, $targetTag, $targetAttr, $targetAttrToRemove);
    }

    
    /**
     * Toggles a.href to span.datakatex.
     * Then, if this span has an ins or del child, wrap this span with the child tag.
     * This feature is necesary since katex overwrite a node content, removing ins/del tags catched by the diffing.
     */
    private function dataKatexPostProcessing()
    {
        $sourceTag = 'a';
        $sourceAttr = 'data-katex';
        $targetTag = 'span';
        $targetAttr = null;
        $targetAttrToRemove = 'href';
        
        $this->difference = self::replaceTag($this->difference, $sourceTag, $sourceAttr, $targetTag, $targetAttr, $targetAttrToRemove);
        $this->difference = self::wrapTag($this->difference, sourceTag: 'span', sourceAttr: 'data-katex');
    }
    

    /**
     * Adds temp space before <img>
     */
    private function imagesPreProcessing()
    {
        $this->lastLdContent = self::insertBefore(
            $this->lastLdContent, 
            sourceTag: 'img'
        );
        $this->currentLdContent = self::insertBefore(
            $this->currentLdContent, 
            sourceTag: 'img'
        );
    }


    /**
     * Remove temp space insertion before <img>
     */
    private function imagesPostProcessing()
    {
        // Not used, blank spaces are removed by the diffing
        //$this->difference = self::removeInsertionBefore($this->difference, 'img', '[[chars_placeholder]]');
    }


    /**
     * Fill an empty $sourceTag (showing a specified $sourceAttr) with $content.
     * Note that "empty" only concerns text: $sourceTag with an empty child is considerated as empty.
     * @param string $htmlContent
     * @param string $sourceTag
     * @param string $sourceAttr
     * @param string $content
     * @return string the modified $htmlContent
     */
    private static function fillEmptyNode($htmlContent, $sourceTag, $sourceAttr, $content)
    {
        $dom = new \DOMDocument();
        @$dom->loadHTML('<html><head><meta charset="utf-8"></head><body>' . $htmlContent . '</body></html>');
        $targetNodes = $dom->getElementsByTagName($sourceTag);
        if ($targetNodes->length === 0) {
            return $htmlContent;
        } else {
            // Regressive loop is a security since the process may add new nodes
            for ($i = $targetNodes->length - 1; $i >= 0; $i--) {
                $targetNode = $targetNodes->item($i);
                // Fill iff the node value is empty
                if (($targetNode->getAttribute($sourceAttr) !== '')
                    and ($targetNode->nodeValue == '')) {
                    // Fill the deepest child
                    $childNode =  $targetNode;
                    while ($childNode->hasChildNodes()){
                        $childNode = $childNode->firstChild;
                    }
                    $childNode->nodeValue = $content;
                }
            }
            return substr($dom->saveHTML($dom->getElementsByTagName('body')->item(0)), 6, -7);
        }
    }


    /**
     * Wrap a $sourceTag (showing a specified $sourceAttr) with ins or del tag if $sourceTag contains one of them.
     * Note that "contains" means: "the deepest child contains"
     * @param string $htmlContent
     * @param string $sourceTag
     * @param string $sourceAttr
     * @return string the modified $htmlContent
     */
    private static function wrapTag($htmlContent, $sourceTag, $sourceAttr)
    {
        $dom = new \DOMDocument();
        @$dom->loadHTML('<html><head><meta charset="utf-8"></head><body>' . $htmlContent . '</body></html>');
        $targetNodes = $dom->getElementsByTagName($sourceTag);
        if ($targetNodes->length === 0) {
            return $htmlContent;
        } else {
            
            // Regressive loop is a security since the process may add new nodes
            for ($i = $targetNodes->length - 1; $i >= 0; $i--) {
                $targetNode = $targetNodes->item($i);
                // Invert iff one child is ins or del
                if ($targetNode->getAttribute($sourceAttr) !== ''){
                    $tagName = null;
                    if ($targetNode->getElementsByTagName('ins')->length === 1){
                        $tagName = 'ins';
                    } elseif ($targetNode->getElementsByTagName('del')->length === 1){
                        $tagName = 'del';
                    };
                    if ($tagName !== null) {
                        // Create a wrapper
                        $wrapper = $dom->createElement($tagName);
                        $wrapper->setAttribute('class', 'diff' . $tagName);
                        // Replace targetNode by the wrapper
                        $targetNode->parentNode->replaceChild($wrapper, $targetNode);
                        // Append targetNode to the wrapper
                        $wrapper->appendChild($targetNode);
                    }
                }
            }
            return substr($dom->saveHTML($dom->getElementsByTagName('body')->item(0)), 6, -7);
        }
    }
    
    
    /**
     * Replace a $sourceTag (showing a specified $sourceAttr) with a $targetTag
     * Copy $sourceAttr to $targetAttr if $targetAttr is not null
     * Remove $targetAttrToRemove if $targetAttrToRemove is not null
     * Note that this content is url_encoded
     * @param string $htmlContent
     * @param string $sourceTag
     * @param string $sourceAttr
     * @param string $targetTag
     * @param string $targetAttr
     * @param string $targetAttrToRemove
     * @return string the modified $htmlContent
     */
    private static function replaceTag($htmlContent, $sourceTag, $sourceAttr, $targetTag, $targetAttr, $targetAttrToRemove)
    {
        $dom = new \DOMDocument();
        @$dom->loadHTML('<html><head><meta charset="utf-8"></head><body>' . $htmlContent . '</body></html>');
        $oldNodes = $dom->getElementsByTagName($sourceTag);
        if ($oldNodes->length === 0) {
            return $htmlContent;
        } else {
            // Regressive loop is a security since the process may add new nodes
            for ($i = $oldNodes->length - 1; $i >= 0; $i--) {
                $oldNode = $oldNodes->item($i);
                if ($oldNode->getAttribute($sourceAttr) !== '') {
                    // Build an array of childs
                    $childNodes = array();
                    foreach ($oldNode->childNodes as $child) {
                        $childNodes[] = $child;
                    }
                    // Copy childs tree to newNode
                    $newNode = $oldNode->ownerDocument->createElement($targetTag);
                    foreach ($childNodes as $child) {
                        $child2 = $oldNode->ownerDocument->importNode($child, true);
                        $newNode->appendChild($child2);
                    }
                    // Copy attributes to newNode
                    foreach ($oldNode->attributes as $attrName => $attrNode) {
                        $attrName = $attrNode->nodeName;
                        $attrValue = $attrNode->nodeValue;
                        $newNode->setAttribute($attrName, $attrValue);
                        // Duplicate attribute to $targetAttr
                        if ($attrName == $sourceAttr && $targetAttr !== null) {
                            $newNode->setAttribute($targetAttr, $attrValue);
                        }
                    }
                    // Delete attribute
                    if ($targetAttrToRemove !== null) {
                        $newNode->removeAttribute($targetAttrToRemove);
                    }
                    $oldNode->parentNode->replaceChild($newNode, $oldNode);
                }
            }
            return substr($dom->saveHTML($dom->getElementsByTagName('body')->item(0)), 6, -7);
        }
    }
    
    
    /**
     * Adds a special tag before $sourceTag in case this tag is encapsulated alone in an outer tag
     * @param string $htmlContent
     * @param string $sourceTag
     * @return string the modified $htmlContent
     */
    private static function insertBefore($htmlContent, $sourceTag)
    {
        $dom = new \DOMDocument();
        @$dom->loadHTML('<html><head><meta charset="utf-8"></head><body>' . $htmlContent . '</body></html>');
        $targetNodes = $dom->getElementsByTagName($sourceTag);
        if ($targetNodes->length === 0) {
            return $htmlContent;
        } else {
            for ($i = $targetNodes->length - 1; $i >= 0; $i--) {
                $targetNode = $targetNodes->item($i);
                $targetNodeParent = $targetNode->parentNode;
                if ($targetNodeParent->nodeValue == ''){
                    $space = new \DOMText(' ');
                    $targetNodeParent->insertBefore($space, $targetNode);
                }
            }
        }
        return substr($dom->saveHTML($dom->getElementsByTagName('body')->item(0)), 6, -7);
    }
}


