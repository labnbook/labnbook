<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * App\Resource
 *
 * @property int $id_ressource
 * @property int|null $id_mission
 * @property int|null $id_report
 * @property string $name
 * @property string|null $res_path
 * @property string $res_type
 * @property int|null $position
 * @property-read \App\Report|null $mission
 * @property-read \App\Report|null $report
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Resource newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Resource newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Resource query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Resource whereIdMission($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Resource whereIdReport($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Resource whereIdRessource($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Resource whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Resource wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Resource whereResPath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Resource whereResType($value)
 * @mixin \Eloquent
 */
class Resource extends Model
{
    protected $table = 'ressource';
    protected $primaryKey = 'id_ressource';
    public $timestamps = false;

    const RES_TYPE_ASSIGNEMENT = 'assignment';
    const RES_TYPE_FILE = 'file';
    const RES_TYPE_URL = 'url';

    /**
     * Returns the report of the conversation
     * @return \App\Report
     */
    public function report()
    {
        return $this->belongsTo('App\Report', 'id_report');
    }

    /**
     * Returns the report of the conversation
     * @return \App\Report
     */
    public function mission()
    {
        return $this->belongsTo('App\Mission', 'id_mission');
    }

    /**
     * Return the storage directory for the resource
     */
    public function getBasePath()
    {
        if ($this->id_report) {
            return "/reports/{$this->id_report}";
        } else {
            return "/missions/{$this->id_mission}/resources";
        }
    }

    /**
     * Adds a resource to the report
     * @param int $id_report
     * @param string $path
     * @param string $name
     * @param string $type
     * @param int|null $id_mission
     * @param int|null $position
     * @return int
     */
    public static function addDoc($id_report, $path, $name, $type, $id_mission = null, $position = null)
    {
        if (!in_array($type, ['url', 'file', 'assignment'])) {
            return 0;
        }
        return DB::table('ressource')->insertGetId([
            'id_mission' => $id_mission,
            'id_report' => $id_report,
            'name' => $name,
            'res_path' => $path,
            'res_type' => $type,
            'position' => $position,
        ]);
    }

    private function deleteFile()
    {
        if ($this->res_path) {
            // suppression du fichier
            $path = explode('/', $this->res_path);
            $file_name = $path[count($path)-1];
            $dir = $this->getBasePath();
            try {
                unlink(storage_path('app/public')."{$dir}/$file_name");
            } catch (\Exception $e) {
                Log::Warning("Cannot unlink old ressource ".storage_path('app/public')."{$dir}/$file_name". " error : ".$e->getMessage());
            }
        }
    }

    /**
     * Deletes the resource
     */
    public function delete()
    {
        $this->deleteFile();
        return parent::delete();
    }

    /**
     * Replace the file of an existing resource
     * @param string $name
     * @param string $new_path
     */
    public function replaceFile($name, $new_path)
    {
        $this->deleteFile();
        Log::Debug($name);
        if ($name) {
            $this->name = $name;
        }
        Log::Debug($this->name);
        $this->res_path = $new_path;
        $this->save();
    }



    /**
     * Insert a new read resource
     * Or update the date of an old resource
     * @param $id_ressource
     * @param $id_user
     * @return void
     */
    public static function createOrUpdateLinkToUser($id_ressource, $id_user): void
    {
        DB::table('link_ressource_user')
            ->updateOrInsert(['id_ressource' => $id_ressource, 
                'id_user' => $id_user],
                ['already_seen'=>NOW()]);
    }
}
