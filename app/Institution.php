<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * App\Institution
 *
 * @property int $id_inst
 * @property string|null $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Classe[] $classes
 * @property-read int|null $classes_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Institution newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Institution newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Institution query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Institution whereIdInst($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Institution whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Institution whereSecret($value)
 * @mixin \Eloquent
 */
class Institution extends Model
{
    //
    protected $table = 'institution';
    protected $primaryKey = 'id_inst';
    public $timestamps = false;

    /*
     * Returns a query to get students in a class of this institution
     * @return Illuminate\Database\Query
     */
    protected function learnersQuery(){
        return DB::table('user')
            ->select('user.*')
            ->join('link_class_learner', 'link_class_learner.id_user', 'user.id_user')
            ->join('class', 'class.id_class', 'link_class_learner.id_class')
            ->where('class.id_inst', $this->id_inst);
    }

    /*
     * Returns a query to get teachers in a class of this institution
     * @return Illuminate\Database\Query
     */
    protected function teachersQuery(){
        return DB::table('user')
            ->select('user.*')
            ->join('link_inst_teacher', 'link_inst_teacher.id_teacher', 'user.id_user')
            ->where('link_inst_teacher.id_inst', $this->id_inst);
    }

    /*
     * Returns a query to get teachers in a class of this institution
     * @return Illuminate\Database\Query
     */
    protected function teachersClasseQuery(){
        return DB::table('user')
            ->select('user.*')
            ->join('link_class_teacher', 'link_class_teacher.id_teacher', 'user.id_user')
            ->join('class', 'class.id_class', 'link_class_teacher.id_class')
            ->where('class.id_inst', $this->id_inst);
    }

    /*
     * Returns a query to get users of this institution
     * @return Illuminate\Database\Query
     */
    protected function usersQuery(){
        return DB::table('user')->where('id_inst', $this->id_inst);
    }

    /**
     * Returns the default class to register teachers
     * @return \App\Classe
     */
    public function teacherClasse()
    {
        return $this->hasOne('App\Classe', 'id_class', 'id_class_teacher');
    }

    /**
     * Return the users of the institution
     * @return \Illuminate\Support\Collection|\App\User[]
     */
    public function users(){
        return \App\User::hydrate(
            $this->usersQuery()
                 ->union($this->teachersQuery())
                 ->union($this->teachersClasseQuery())
                 ->union($this->learnersQuery())
                 ->get()
                 ->toArray()
         );
    }

    /**
     * Return the learners of the institution
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function learners()
    {
        return $this->hasMany('App\User', 'id_inst')
                    ->leftJoin('teacher', 'id_teacher', 'id_user')
                    ->whereNull('id_teacher')
                    ->select('user.*');
    }

    /**
     * Return true if $id_user is part of the institution
     * @param \App\User $user
     * @return bool
     */
    public function hasUser($user)
    {
        if ($user->id_inst == $this->id_inst) {
            return true;
        }
        $QUERIES = [
            'usersQuery',
            'teachersQuery',
            'teachersClasseQuery',
            'learnersQuery',
        ];
        foreach ($QUERIES as $query) {
            if ($this->$query()->where('user.id_user', $user->id_user)->exists()) {
                return true;
            }
        }
        return false;
    }

    public function hasCas()
    {
        return $this->cas;
    }


    public function hasLdap()
    {
        return $this->hasCas();
    }

    /**
     * Return the clases of the institution
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function classes()
    {
        return $this->hasMany('App\Classe', 'id_inst');
    }

    /**
     * Return the extplatforms of the institution
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function extplatforms()
    {
        return $this->hasMany('App\Extplatforms', 'id_inst');
    }

    public function teachers()
    {
        return $this->belongsToMany('App\Teacher', 'link_inst_teacher', 'id_inst', 'id_teacher');
    }
    
    public function teachersManagers()
    {
        return $this->belongsToMany('App\Teacher', 'link_inst_teacher', 'id_inst', 'id_teacher')
            ->where('role', "=", Teacher::ROLE_MANAGER);
    }

    public function teachersMembers()
    {
        return $this->belongsToMany('App\Teacher', 'link_inst_teacher', 'id_inst', 'id_teacher')
            ->where('role', "=", Teacher::ROLE_TEACHER);
    }

    public function countUsers() {
        return $this->usersQuery()->count();
    }
    
    

    /**
     * Return the ids of institutions connected to cas / ldap
     * @return array
     */
    public static function getIdInstsWithCas()
    {
        return \App\Institution::where('cas', True)->pluck('id_inst')->toArray();
    }

    /**
     * Returns the institution of the classe having $code
     * @param string $code
     * @return \App\Institution|null
     */
    public static function fromClasseCode($code)
    {
        return self::join('class', 'class.id_inst', '=', 'institution.id_inst')
            ->where('class_code', $code)
            ->first();
    }

    /**
     * Creates an instituion with the associated teacher class
     * @param string $name
     * @param boolean $cas
     * @param \App\Teacher $teacher
     * @return \App\Instituion|null
     */
    public static function createInstWithTeacherClass($name, $cas, $teacher)
    {
        $inst = new self();
        $inst->name = $name;
        $inst->cas = $cas;
        if (!$inst->save()) {
            return null;
        }
        $classe = new \App\Classe();
        $classe->id_inst = $inst->id_inst;
        $classe->class_name = __("Ens. :inst_name", ['inst_name' => $inst->name]);
        if (!$classe->save()) {
            $inst->delete();
            return null;
        }
        $classe->teachers()->attach($teacher);
        $inst->id_class_teacher = $classe->id_class;
        $inst->save();
        return $inst;
    }
}
