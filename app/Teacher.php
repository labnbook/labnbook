<?php

namespace App;

use App\Scoring\Assessment;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;

/**
 * App\Teacher
 *
 * @property int $id_teacher
 * @property string|null $teacher_domain
 * @property string|null $teacher_level
 * @property string|null $teacher_comment
 * @property string|null $current_interface
 * @property string $role
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Classe[] $classes
 * @property-read int|null $classes_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Institution[] $institutions
 * @property-read int|null $institutions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Mission[] $missions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\TeacherTeam[] $teacherTeams
 * @property-read int|null $missions_count
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Teacher newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Teacher newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Teacher query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Teacher whereCurrentInterface($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Teacher whereIdTeacher($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Teacher whereTeacherComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Teacher whereTeacherDomain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Teacher whereTeacherLevel($value)
 * @mixin \Eloquent
 * @property-read int|null $teacher_teams_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Teacher whereRole($value)
 */
class Teacher extends Model
{
    protected $table = 'teacher';
    protected $primaryKey = 'id_teacher';
    protected $fillable = ["teacher_comment"];
    public $timestamps = false;
    public $incrementing = false;

    const ROLE_MANAGER = "manager";
    const ROLE_ADMIN = "admin";
    const ROLE_TEACHER = "teacher";
    
    /**
     * Returns the user linked to the teacher
     * @return App\User
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'id_teacher', 'id_user');
    }

    /**
     * Returns the classes of the teacher
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function classes()
    {
        return $this->belongsToMany('App\Classe', 'link_class_teacher', 'id_teacher', 'id_class');
    }

    /**
     * Return the teacher interface name
     * @param string defaultInterface to use if current_interface is not set, default value 'missions'
     * @return string
     */
    public function getInterface($defaultInterface = 'teacher/missions')
    {
        if ($this->current_interface && Helper::internalUrlExists($this->current_interface)) {
            return $this->current_interface;
        }
        return $defaultInterface;
    }

    /**
     * Return the missions of a teacher
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function missions(){
        return $this->belongsToMany('App\Mission', 'link_mission_teacher', 'id_teacher', 'id_mission');
    }

    /**
     * Return the copyable missions of a teacher
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function missionsToCopyForAClass($id_class) {
        return $this->missions()
            ->select('mission.code','mission.id_mission')
            ->join('team_config', function ($join) use ($id_class) {
                $join->on('team_config.id_mission', '=', 'mission.id_mission')
                    ->where('team_config.id_class', '=', $id_class);
            })
            ->groupBy('mission.id_mission')
            ->orderBy('mission.code')
            ->get();
    }


    /**
     * Return the teacher team of a teacher
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function teacherTeams(){
        return $this->belongsToMany('App\TeacherTeam', 'link_teacher_tteam', 'id_teacher', 'id_teacher_team');
    }

    public function teacherTeamsManaged()
    {
        return DB::table("teacher_team")
            ->select('teacher_team.*')
            ->join('link_teacher_tteam as lttt', 'lttt.id_teacher_team', 'teacher_team.id_teacher_team')
            ->where("lttt.status", "=", TeacherTeam::MANAGER)
            ->where("lttt.id_teacher", "=", $this->id_teacher)
            ->orderBy("teacher_team.name")
            ->get();
    }

    /**
     * Returns the report on for which the teacher is a teacher
     * @param boolean $strict restrict to teamings in my classes
     * @return \Illuminate\Database\Eloquent\Builder query()
     */
    public function reports($strict = false)
    {
        // Reports of one of the teacher missions joined with teacher classes
        $query = \App\Report::select('report.*')
            ->distinct()
            ->join('link_mission_teacher as lmt', 'lmt.id_mission', 'report.id_mission')
            ->where('lmt.id_teacher', $this->id_teacher)
            ->whereNotIn('report.status', ["test","tuto"]);

        // Reports linked to the teacher by both class and mission
        $tc_query = (clone $query)
            ->join('team_config as tc', 'report.id_team_config', 'tc.id_team_config')
            ->join('link_class_teacher as lct', 'lct.id_class', 'tc.id_class')
            ->where('lct.id_teacher', $this->id_teacher);
        if ($strict) {
            return $tc_query;
        }

        // Reports linked to the teacher missions and at least one of its students
        $queryByMission = (clone $query)
            ->join('link_report_learner as lrl', 'lrl.id_report', 'report.id_report')
            ->join('link_class_learner as lcl', 'lcl.id_user', 'lrl.id_user')
            ->join('link_class_teacher as lct', 'lct.id_class', 'lcl.id_class')
            ->where('lct.id_teacher', $this->id_teacher);
        // We recreate a select query encapsulating the union so filters added laters
        // will apply on both parts
        return \App\Report::fromSub($tc_query->union($queryByMission), 'report');
    }

    /**
     * For a report that is not in one of our classes, this methods tries to
     * guess in which class the report belongs
     * Return the smaller id_class that statisfies the bellows requirements
     *      + the current teacher is teacher for this class
     *      + for the given report
     *          + the report's mission is assigner on this class
     *          + at least one of the students of the report is a learner of this class
     *  @param \App\Report $report
     *  @return int[] $id_class
     */
    public function guessExternalReportLinkIdClass($report)
    {
        return array_map(
            function ($e) {
                return (int)$e;
            },
            $this->classes()
                 ->join('team_config as tc', 'tc.id_class', 'class.id_class')
                 ->join('link_class_learner as lcl', 'tc.id_class', 'lcl.id_class')
                 ->join('link_report_learner as lrl', 'lrl.id_user', 'lcl.id_user')
                 ->where('lrl.id_report', $report->id_report)
                 ->where('id_mission', $report->id_mission)
                 ->orderBy('id_class', 'ASC')
                 ->distinct()
                 ->pluck('lcl.id_class')
                 ->toArray()
        );
    }

    /**
     * Adds an institution to this teacher
     * @param int $id_inst
     * @return boolean
     */
    public function addInstitution($id_inst)
    {
        return DB::table('link_inst_teacher')->insert([
            'id_teacher' => $this->id_teacher,
            'id_inst' => $id_inst,
        ]);
    }

    /**
     * Return the institutions of a teacher
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function institutions()
    {
        return $this->belongsToMany('App\Institution', 'link_inst_teacher', 'id_teacher', 'id_inst');
    }

    /**
     * Return the ids of admins
     * @return array
     */
    public static function getAdminIds()
    {
        return self::where('role', 'admin')->pluck('id_teacher')->toArray();
    }

    /**
     * Get the teacher current interface url
     * @return string
     */
    public function getInterfaceUrl()
    {
        return '/'.$this->getInterface();
    }

    /**
     * Determines if the teacher is a teacher for a given report
     * @param \App\Report $report
     * @return boolean
     */
    public function isReportTeacher($report)
    {
        // The strict query is faster and should be enough most of the time
        if ($report->teachers(true)->get()->contains($this->id_teacher)) {
            return true;
        }
        return $report->teachers()->get()->contains($this->id_teacher);
    }

    /**
     * Handle teacher entering in new interface
     * @param int $id_teacher
     * @param string $interface
     * @return int
     */
    public static function enterInterface($id_teacher, $interface)
    {
        if ($interface !== "lb_mission_edit") {
            return DB::table('teacher')
                ->where('id_teacher', $id_teacher)
                ->update(['current_interface' => $interface]);
        }
        return 0;
    }

    /**
     * Reports array to be used in the "Reports" table
     * @param \App\User $user
     * @param array $filters With keys id_class, id_mission, status, columns, draw, start, length, search
     * @return array
     */
    public function filterReports($data_table_filters)
    {
        $custom_filters = $data_table_filters['filters'];
        $id_mission = $custom_filters['id_mission'];
        $id_class = $custom_filters['id_class'];
        $teaming = $custom_filters['teaming'];
        $status = $custom_filters['status'];

        $search = $data_table_filters['search']['value'];
        $orderColumnIndex = $data_table_filters['order'][0]['column'];
        $orderColumnName = $data_table_filters['columns'][$orderColumnIndex]['name'];
        $orderDirection = $data_table_filters['order'][0]['dir'];
        $reportStatusOrder = implode("','",Report::SORTING_STATUS);
        $orderByRaw = match ($orderColumnName) {
            'team' => "report.team_name {$orderDirection}",
            'status' => "FIELD(report.status, '{$reportStatusOrder}') {$orderDirection}",
            'activity' => "last_edition {$orderDirection}",
            'modified_ld' => "modified_ld {$orderDirection}",
            'evaluations' => "evaluations {$orderDirection}",
            default => "m.code {$orderDirection}",
        };
        $start = intval($data_table_filters['start']);
        $length = intval($data_table_filters['length']);
        $draw = intval($data_table_filters['draw']);
        
        if ($this->user->isAdmin()) {
            if (empty($id_mission) && empty($id_class)) {
                return [ "draw" => $draw,"recordsTotal" => 0, "recordsFiltered" => 0, "data" => [] ];
            }

            $query = \App\Report::select('report.*')
                ->distinct()
                ->leftJoin('team_config as tc', 'tc.id_team_config', 'report.id_team_config')
                ->withTrashed();
            $strict = true;
        } else {
            $strict = !$teaming;
            $query = $this->reports($strict);
            if (!$strict) {
                $query = $query->leftJoin('team_config as tc', 'tc.id_team_config', 'report.id_team_config');
            }
        }

        if (!empty($id_mission)) {
            $query = $query->where('report.id_mission', (int)$id_mission);
        }
        if (!empty($id_class) && $strict) {
            $query = $query->where('tc.id_class', (int)$id_class);
        }
        if ($status) {
            $query = $query->whereIn('report.status', $status);
        }
        
        $query = $query->leftJoin('mission as m', 'report.id_mission', 'm.id_mission')
                       ->leftJoin('class as c', 'tc.id_class', 'c.id_class')
                       ->leftJoin('class as msg_class', 'report.allow_msg_id_class', 'msg_class.id_class')
                       ->leftJoin('mission as msg_mission', 'report.allow_msg_id_mission', 'msg_mission.id_mission');
        
        $total_records = $query->count();
        
        if ($search) {
            // We filter the query based on the search term, we keep all row if the search term is in mission code or team name or team members first and last name
            $searchTerm = '%' . $search . '%';

            $query->whereExists(function ($query) use ($searchTerm) {
                // Subquery to check in 'report' and join with 'mission'
                $query->select('r.id_report')
                    ->from('report as r')
                    ->leftJoin('mission as m', 'm.id_mission', '=', 'r.id_mission')
                    ->whereColumn('r.id_report', 'report.id_report')
                    ->where(function ($query) use ($searchTerm) {
                        // Conditions for 'code' or 'team_name'
                        $query->where('m.code', 'LIKE', $searchTerm)
                            ->orWhere('r.team_name', 'LIKE', $searchTerm)
                            ->orWhereExists(function ($query) use ($searchTerm) {
                                // Subquery to check for concatenated first_name and last_name in 'user'
                                $query->select('lrl.id_report')
                                    ->from('user as u')
                                    ->join('link_report_learner as lrl', 'lrl.id_user', '=', 'u.id_user')
                                    ->whereColumn('lrl.id_report', 'r.id_report')
                                    ->whereRaw("CONCAT(u.first_name, ' ', u.user_name) LIKE ?", [$searchTerm]);
                            });
                    });
            });
        }
        
        $modified_lds = Labdoc::whereColumn('labdoc.id_report', 'report.id_report')
                ->whereNotNull('labdoc.last_edition')
                ->whereNull('labdoc.deleted')
                ->selectRaw('COUNT(labdoc.id_labdoc)');
        
        $last_edition = Labdoc::whereColumn('labdoc.id_report', 'report.id_report')
                ->whereNotNull('labdoc.last_edition')
                ->whereNull('labdoc.deleted')
                ->selectRaw('MAX(labdoc.last_edition)');

        $evaluation_status = DB::table('rubric as r')
            ->whereColumn('re.id_report', 'report.id_report')
            ->leftJoin('mission as m', 'r.id_activity', '=', 'm.id_mission')
            ->leftJoin('report as re', 'm.id_mission', '=', 're.id_mission')
            ->leftJoin('assessment as a', 'a.id_production', '=', 'report.id_report')
            ->selectRaw("CASE 
                    WHEN a.published=1 THEN '".Assessment::STATUS_PUBLISHED."'
                    WHEN a.score IS NOT NULL THEN '".Assessment::STATUS_STARTED."'
                    ELSE '".Assessment::STATUS_NOT_STARTED."'
                END"
            )
            ->groupBy('r.id_rubric')
            ->limit(1);

        $query = $query->addSelect([
                'report.*',
                'm.code as mission_code',
                'm.simulation_code as simulation_code',
                'm.name as mission_name',
                'tc.id_class as id_class_tc',
                'c.class_name as tc_classname',
                'msg_class.class_name as msg_class_name',
                'msg_mission.code as msg_mission_code',
                'modified_ld' => $modified_lds,
                'last_edition' => $last_edition,
                'evaluations' => $evaluation_status
            ]);
        
        $total_filtered_records = $query->count();
        
        if ($length>0) {
            $query = $query->offset($start)->limit($length);
        }
        $results = $query->orderByRaw($orderByRaw)->orderBy('report.team_name')->get();
        
        if ($results->isEmpty()) {
            return ["draw" => $draw,
                "recordsTotal" => $total_records,
                "recordsFiltered" => $total_filtered_records,
                "data" => []];
        }

        // Classe allowed for the teacher
        if ($this->user->isAdmin()) {
            $classes = \App\Classe::pluck('id_class')->all();
        } else {
            $classes = $this->classes()->pluck('class.id_class');
        }

        $reports = [];
        foreach ($results as $report) {
            // We add field to the report model object for legacy reason
            $report->id_class_links = [] ;
            if (!$strict) {
                if ($report->id_class_tc && !$classes->contains($report->id_class_tc)) {
                    $report->id_class_links = $this->guessExternalReportLinkIdClass($report);
                }
            }
            $report->allowed_class_name = $report->msg_class_name;
            $report->allowed_mission_code = $report->msg_mission_code;
            
            $report->labdocs = (object) ['num' => 0, 'last_edition' => 0];
            $report->members = [];
            $report->classes = [];
            $report->teachers = [];
            // TODO Warning refactor allowImport
            if ($report->allow_import == 1 && $report->allow_import_id_mission == null ) {
                $report->import_status = __("autorisé depuis toute mission");
            } else if ($report->allow_import_id_mission === 0) {
                $report->import_status = __("non autorisé");
            } else {
                // TODO Warning refactor allowImport
                $res = \App\Mission::where('id_mission', (int)$report->allow_import_id_mission)->select('mission.code', 'code')->first();
                $report->import_status = __("autorisé depuis la mission ") . htmlspecialchars($res ? $res->code: "");
            }

            // Get all team members
            $report->members = DB::table('user as u')
                ->join('link_report_learner as lrl', 'u.id_user', '=', 'lrl.id_user')
                ->select([
                    'u.id_user',
                    'u.user_name',
                    'u.first_name',
                    'u.login',
                    'u.email',
                    'lrl.id_report',
                    DB::raw('(UNIX_TIMESTAMP() - lrl.last_synchro) AS seconds_since_synchro')
                ])
                ->where('lrl.id_report', $report->id_report)
                ->orderBy('u.user_name')
                ->get()->toArray();
            
            // Get all classes
            $report->classes = DB::table('link_report_learner as lrl')
                ->join('link_class_learner as lcl', 'lrl.id_user', '=', 'lcl.id_user')
                ->join('link_class_teacher as lct', 'lcl.id_class', '=', 'lct.id_class')
                ->select(['lcl.id_class'])
                ->distinct()
                ->where('lrl.id_report',$report->id_report)
                ->when(!$this->user->isAdmin(), function ($query) {
                    return $query->where('lct.id_teacher', $this->id_teacher);
                })->pluck('lcl.id_class')->toArray();
            
            // Get all teachers
            $report->teachers = DB::table('user')
                ->join('link_class_teacher', 'user.id_user', '=', 'link_class_teacher.id_teacher')
                ->join('link_mission_teacher', 'user.id_user', '=', 'link_mission_teacher.id_teacher')
                ->join('team_config', function ($join) {
                    $join->on('team_config.id_mission', '=', 'link_mission_teacher.id_mission')->on('team_config.id_class', '=', 'link_class_teacher.id_class');
                })
                ->join('report', 'report.id_team_config', '=', 'team_config.id_team_config')
                ->selectRaw('CONCAT( UPPER( SUBSTRING(user.first_name, 1, 1) ),". ", user.user_name) as teacher_full_name')
                ->where('report.id_report',$report->id_report)
                ->orderBy('user.user_name')->pluck('teacher_full_name')->toArray();

            $report_info = DB::table('labdoc as ld')
                ->leftJoin('labdoc_status as ls', function ($join) {
                    $join->on('ld.id_labdoc', '=', 'ls.id_labdoc')
                        ->where('ls.id_user', '=', $this->id_teacher);
                })
                ->selectRaw('ld.id_report, COUNT(ld.id_labdoc) AS modified_ld')
                ->selectRaw('MAX(ld.last_edition) AS last_edition')
                ->selectRaw('SUM((ls.id_labdoc IS NULL) OR (ls.mod_icon = 1)) AS new_mod_ld')
                ->whereNull('ld.deleted')
                ->whereNotNull('ld.last_edition')
                ->where('ld.id_report', $report->id_report)
                ->groupBy('ld.id_report')
                ->first();
            
            if($report_info) {
                $report->labdocs = [
                    'modified_ld' => (int) $report_info->modified_ld,
                    'new_mod_ld' => (int) $report_info->new_mod_ld,
                    'last_edition' => (int) $report_info->last_edition
                ];
            }
            
            if($report->simulation_code) {
                $report->simulation_data = Simulation::_getReportSimulationData($report->simulation_code, $report->id_report);
            }
            $reports[(int)$report->id_report] = $report;
        }

        return [
            "draw" => $draw,
            "recordsTotal" => $total_records,
            "recordsFiltered" => $total_filtered_records,
            "data" => !empty($reports)?array_values($reports):[]
        ];
    }


    /**
     * Get the teacher's classes
     * @return array [ {id_class, id_inst, class_name, class_code, inst_name, nb_learners, teachers, teamMissions} ]
     */
    public function getClasses()
    {
        $params = [];
        $sql = "SELECT c.id_class, c.id_inst, c.class_name, c.num_ext_participants, c.class_code, c.status, i.name AS inst_name, count(lcl.id_user) AS nb_learners, c.id_extplatform"
            . " FROM class c"
            . " JOIN institution i ON c.id_inst = i.id_inst"
            . " LEFT JOIN link_class_learner lcl ON c.id_class = lcl.id_class";
        if (!$this->user->isAdmin()) { // admin
            $sql .= " JOIN link_class_teacher lct ON c.id_class = lct.id_class"
                . " WHERE lct.id_teacher = :id_teacher";
            $params['id_teacher'] = $this->id_teacher;
        }
        $sql .= " GROUP BY c.id_class, i.id_inst ORDER BY c.class_name";
        $classes = \App\Classe::hydrate(DB::select(DB::raw($sql), $params));
        foreach ($classes as $k => $v) {
            $classes[$k]->id_class = (int) $v->id_class;
            $classes[$k]->id_inst = (int) $v->id_inst;
            $classes[$k]->nb_learners = (int) $v->nb_learners;
            $classes[$k]->teachers = $v->getTeachersNameAndId();
            $classes[$k]->teamMissions = $v->getMissionsNameCodeAndId();
            if ($v->id_extplatform) {
                $ext = \App\Extplatform::find($v->id_extplatform);
                $classes[$k]->extplatform_name = $ext->name;
                $classes[$k]->extplatform_url = $ext->url;
            }
        }
        return $classes;
    }

    /**
     * Get the teacher's teacher team
     * @return array [ {id_class, id_inst, class_name, class_code, inst_name, nb_learners, teachers, teamMissions} ]
     */
    public function getTeacherTeamWithNbTeacher ()
    {        
        // TODO Use query builder and simplify
        $query = "SELECT id_teacher_team, id_inst, i.name as inst_name, tt.name as name, tt.status " .
        "FROM `teacher_team` tt " .
        "JOIN link_teacher_tteam ltt USING (id_teacher_team) ".
        "JOIN institution i USING (id_inst) ".
        "WHERE ltt.id_teacher =  ?  ".
        "AND ltt.status = '" . TeacherTeam::MANAGER . "' " .
        "ORDER BY tt.name";
    
        $teacherTeams =  \App\TeacherTeam::hydrate (DB::select(DB::raw($query), [$this->id_teacher]));
                
        foreach ($teacherTeams as $k => $v) {
            $teacherTeams[$k]->teachers = $v->getTeachersNameAndId();
            $teacherTeams[$k]->teachersManager = $v->getTeachersManagerNameAndId();
            $teacherTeams[$k]->nb_teacher = $v->teachers()->count();
            $teacherTeams[$k]->classes = $v->classes()->select('class_name')->get();
            $teacherTeams[$k]->missions = $v->missions()->select('name')->get();
        }
        
        return $teacherTeams;
    }

    /**
     * Is the user a manager ?
     * @return boolean
     */
    public function isManager (){
        return in_array($this->role, [self::ROLE_MANAGER, self::ROLE_ADMIN]);
    }

    /**
     * Set the user role if to $role
     * @param string $role self::ROLE_TEACHER|self::ROLE_MANAGER|self::ROLE_ADMIN
     * @return boolean did a change occured
     */
    public function changeRole ($role)
    {
        if (in_array($role, [self::ROLE_MANAGER, self::ROLE_TEACHER, self::ROLE_ADMIN])) {
            $this->role = $role;
            return $this->save();
        }
        return false;
    }
  
    public static function addNewManagers ($teachers) {
        foreach ($teachers as $id_teacher) {
            Teacher::find($id_teacher)->changeRole(Teacher::ROLE_MANAGER);
        }
    }

    public static function getTeacherWithInstAndLinkBy($id_inst, $var)
    {
        /**
         * Requete avec plus d'info :
         * SELECT u.id_user, u.user_name, u.first_name, u.login, u.email, COUNT(DISTINCT id_mission) AS nb_missions, COUNT(DISTINCT id_class) AS nb_classes 
         * FROM user u JOIN teacher t ON u.id_user = t.id_teacher 
         * LEFT JOIN link_mission_teacher USING(id_teacher)
         * LEFT JOIN link_class_teacher USING(id_teacher) 
         * WHERE u.id_inst = 2 AND (u.user_name LIKE "%tea%" OR u.first_name LIKE "%tea%" OR u.email LIKE "%tea%") 
         * GROUP BY u.id_user
         */
        
        $sql = "SELECT u.id_user, u.user_name, u.first_name, u.login, u.email, count(l.id_teacher) as hasLink
                FROM user u JOIN teacher t ON u.id_user = t.id_teacher
                LEFT JOIN (SELECT id_teacher 
                      FROM link_mission_teacher 
                      UNION 
                      SELECT id_teacher 
                      FROM link_class_teacher) l ON t.id_teacher = l.id_teacher
                WHERE u.id_inst = :id_inst AND ( u.user_name LIKE :var OR u.first_name LIKE :var OR u.email LIKE :var)
                GROUP BY u.id_user
                ";
        $params = ['id_inst' => $id_inst, 'var' => '%' . $var . '%'];
        return \App\User::Hydrate(DB::select(DB::raw($sql), $params));
    }
}
