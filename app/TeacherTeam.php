<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\belongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

/**
 * App\TeacherTeam
 *
 * @property int $id_teacher_team
 * @property int|null $id_inst
 * @property string $name
 * @property string $status
 * @property Carbon|null $update_time
 * @property Carbon|null $creation_time
 * @method static Builder|TeacherTeam newModelQuery()
 * @method static Builder|TeacherTeam newQuery()
 * @method static Builder|TeacherTeam query()
 * @method static Builder|TeacherTeam whereCreationTime($value)
 * @method static Builder|TeacherTeam whereIdInst($value)
 * @method static Builder|TeacherTeam whereIdTeacherTeam($value)
 * @method static Builder|TeacherTeam whereName($value)
 * @method static Builder|TeacherTeam whereStatus($value)
 * @method static Builder|TeacherTeam whereUpdateTime($value)
 * @property-read Collection|Teacher[] $teachers
 * @property-read Collection|Teacher[] managers
 * @mixin Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Classe[] $classes
 * @property-read int|null $classes_count
 * @property-read \App\Institution|null $institution
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Teacher[] $managers
 * @property-read int|null $managers_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Mission[] $missions
 * @property-read int|null $missions_count
 * @property-read int|null $teachers_count
 */
class TeacherTeam extends Model
{
    protected $table = 'teacher_team';
    protected $primaryKey = 'id_teacher_team';
    const CREATED_AT = 'creation_time';
    const UPDATED_AT = 'update_time';

    const MANAGER = "manager";
    const MEMBER = "member";

    const STATUS_NORMAL = "normal";
    const STATUS_ARCHIVE = "archive";

    public $fillable = ['id_inst', 'name', 'status'];
    
    /**
     * Returns the teachers of the teacher team
     * @return BelongsToMany
     */
    public function teachers()
    {
        return $this->belongsToMany('App\Teacher', 'link_teacher_tteam', 'id_teacher_team', 'id_teacher')
                    ->where('status', "=", TeacherTeam::MEMBER);
    }

    /**
     * Returns the managers of the teacher team
     * @return BelongsToMany
     */
    public function managers()
    {
        return $this->belongsToMany('App\Teacher', 'link_teacher_tteam', 'id_teacher_team', 'id_teacher')
            ->where('status', "=", TeacherTeam::MANAGER);
    }

    public function classes()
    {
        return $this->belongsToMany('App\Classe', 'link_class_tteam', 'id_teacher_team', 'id_class');
    }

    public function missions()
    {
        return $this->belongsToMany('App\Mission', 'link_mission_tteam', 'id_teacher_team', 'id_mission');
    }
    
    public function getTeachersNameAndId() {
        return $this->teachers()
            ->join('user as u', 'u.id_user', 'teacher.id_teacher')
            ->selectRaw('id_user, first_name, user_name')
            ->orderBy('user_name')
            ->get()
            ->toArray();
    }

    /**
     * Returns the institution of the classe
     * @return belongsTo
     */
    public function institution()
    {
        return $this->belongsTo('App\Institution', 'id_inst');
    }
    
    public function getTeachersManagerNameAndId()
    {
        return DB::table('link_teacher_tteam as lttt')
            ->join('user as u', 'u.id_user', 'lttt.id_teacher')
            ->selectRaw('id_user, first_name, user_name')
            ->where('lttt.status', 'LIKE' , TeacherTeam::MANAGER)
            ->where('lttt.id_teacher_team', '=', $this->id_teacher_team)
            ->orderBy('user_name')
            ->get()
            ->toArray();
    }

    public static function addOrUpdate($name, $inst, $manager, $id_teacher_team, $newTeachers, $deletedTeachers, $updateLink = true)
    {
        $params = [
            'name' => (string) $name,
            'id_inst' => (int) $inst,
            'status' => (string) TeacherTeam::STATUS_NORMAL
        ];

        // ajout de la classe dans la bdd (reset des liens class-teachers)
        if ($id_teacher_team > 0) {
            // Mise à jour
            $teacher_team = self::find($id_teacher_team);
            $teacher_team->fill($params);
            $teacher_team->save();
            
        } else { // ajout
            $teacher_team = self::create($params);
            DB::statement("INSERT INTO `link_teacher_tteam` (`id_teacher_team`, `id_teacher`, `status`) VALUES (?, ?, ?)", [$teacher_team->id_teacher_team, $manager->id_teacher, TeacherTeam::MANAGER]);
        }
       
        $classes = $teacher_team->classes()->get();
        $missions = $teacher_team->missions()->get();
        foreach ($deletedTeachers as $id_teacher) {
            DB::statement("DELETE FROM `link_teacher_tteam` WHERE  `id_teacher_team` = ? AND  `id_teacher` = ? AND `status` = ?  ", [$teacher_team->id_teacher_team, $id_teacher, TeacherTeam::MEMBER]);
        }
        foreach ($newTeachers as $id_teacher) {
            if(!$teacher_team->teachers()->get()->contains('id_teacher',$id_teacher)){
                // new teacher in  the tteam
                DB::statement("INSERT IGNORE INTO `link_teacher_tteam` (`id_teacher_team`, `id_teacher`, `status`) VALUES (?, ?, ?)", [$teacher_team->id_teacher_team, $id_teacher,  TeacherTeam::MEMBER]);
                if($updateLink === "true" ){
                    if($classes->count()>0) {
                        foreach ($classes as $classe) {
                            // Link new teacher in all class of the tteam
                            DB::statement("INSERT IGNORE INTO `link_class_teacher` (`id_teacher`, `id_class`) VALUES (?, ?)", [$id_teacher, $classe->id_class]);
                        }
                    }
                    if($missions->count()>0) {
                        foreach ($missions as $mission) {
                            // Link new teacher in all mission of the tteam
                            DB::statement("INSERT IGNORE INTO `link_mission_teacher` (`id_teacher`, `id_mission`, `teacher_type`) VALUES (?, ?, 'teacher')", [$id_teacher, $mission->id_mission]);
                        }
                    }
                }
            }
        }
        
        return $teacher_team->id_teacher_team > 0 ? (int) $teacher_team->id_teacher_team : null;
    }
    
    public function archive()
    {
        $this->status = TeacherTeam::STATUS_ARCHIVE;
        $this->save();
    }

    public function unarchive()
    {
        $this->status = TeacherTeam::STATUS_NORMAL;
        $this->save();
    }

    public function duplicate()
    {
        $params = ["id_inst" => $this->id_inst, "name" => $this->name . " " . __("(copie)"), "status" => "normal"];
        $new = self::create($params);
        $new->save();
        
        $manager=$this->managers()->first();
        DB::statement("INSERT INTO `link_teacher_tteam` (`id_teacher_team`, `id_teacher`, `status`) VALUES (?, ?, ?)", [$new->id_teacher_team, $manager->id_teacher, TeacherTeam::MANAGER]);

        foreach ($this->teachers()->get() as $teacher) {
            DB::statement("INSERT INTO `link_teacher_tteam` (`id_teacher_team`, `id_teacher`, `status`) VALUES (?, ?, ?)", [$new->id_teacher_team, $teacher->id_teacher, TeacherTeam::MEMBER]);
        }
    }
}
