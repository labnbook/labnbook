<?php

namespace App\Views\Policies;

use App\User;
use App\Helper;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Log;

class ReportPolicy
{
    use HandlesAuthorization;
    /**
     * Determine whether the user can view the report view.
     *
     * @param  \App\User  $user
     * @param  \App\Views\Report  $reportView
     * @return mixed
     */
    public function view(User $user, \App\Views\Report $reportView)
    {
        if ($user->cannot('view', $reportView->report)) {
            Helper::setPolicyError(
                403,
                "Ce rapport n'existe pas ou vous n'êtes pas membre de l'équipe qui y travaille."
            );
            return false;
        }
        // In default mode we must check that report update is allowed
        if ($reportView->is_default) {
            if (!$reportView->report->users()->whereKey($user->id_user)->exists()) {
                return false;
            }
            $status = $reportView->report->updateAllowed();
            Log::Debug($status);
            if (!$status['allowed']) {
                Helper::setPolicyError(
                    409,
                    $status['msg'],
                );
            }
            return $status['allowed'];
        }
        if ($reportView->is_follow) {
            return $user->isReportTeacher($reportView->report);
        }
        // Here scope is either pdf, view, or test
        // and we know that the user can view the report i.e they are either
        // a member of the team or a teacher
        return true;
    }
}
