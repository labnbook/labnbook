<?php

namespace App\Views;

class ReportPart
{
    /** @var \App\Report */
    public $report;


    /** @var \App\ReportPart */
    public $reportPart;

    /** @var \Illuminate\Support\Collection|\App\Labdoc[] */
    public $labdocs;

    /** @var string */
    public $scope;

    /** @var array */
    public $ld_chosen;

    /** @var string */
    public $choice_ld;

    /** @var bool             is the scope "default" */
    public $is_default;
    /** @var bool             is the scope "pdf" */
    public $is_pdf;
    /** @var bool             is the scope "view" */
    public $is_view;
    /** @var bool             is the scope "test" */
    public $is_test;
    /** @var bool             is the scope "follow" */
    public $is_follow;

    /** @var bool             is the status "solution" */
    public $is_solution;

    /** @var bool             is the user a teacher */
    public $is_teacher;
    /** @var bool             is the user a teacher */
    public $is_learner;

    /** @var bool            is the report in read only mode */
    public $read_only;


    /**
     * Constructor
     * @param \App\Views\Report $reportView
     * @param \App\ReportPart $reportPart
     */
    public function __construct($reportView, $reportPart)
    {
        if ($reportView) {
            // Recopy fields from reportView
            $FIELDS = [
                'is_test',
                'is_follow',
                'is_default',
                'is_solution',
                'is_pdf',
                'is_view',
                'is_teacher',
                'is_learner',
                'read_only',
                'scope',
                'choice_ld',
                'ld_chosen',
                'report'
            ];
            foreach ($FIELDS as $field) {
                $this->$field = $reportView->$field;
            }
        }

        $this->reportPart = $reportPart;
        $this->init($this->report);
    }

    /**
     * Finalize the report part view's initialization
     * @param \App\Report
     */
    public function init($report)
    {
        if ($this->reportPart) {
            $query = $this->reportPart->labdocs()->where('id_report', $this->report->id_report);
            $query_shared = $this->reportPart
                                 ->labdocs()
                                 ->where('id_team_config', $this->report->id_team_config)
                                 ->where('shared', '=', '1')
                                 ->whereNotNull('id_team_config');
        } else {
            $query = $this->report->labdocs()
                          ->whereNull('id_report_part')
                          ->whereNotNull('position');
            $query_shared = $this->report->sharedLabdocs()
                          ->whereNull('id_report_part')
                          ->whereNotNull('position');
        }
        $this->labdocs = $query
             ->whereNull('deleted')
             ->orderBy('position', 'ASC', 'id_labdoc', 'ASC')
             ->get();

        if ($this->is_solution || $this->is_test) {
            $this->labdocs_shared = [];
            foreach ($this->labdocs as $k => $l) {
                if ($l->id_ld_origin && \App\Labdoc::find($l->id_ld_origin)->shared == 1) {
                    $this->labdocs_shared []= $l;
                    $this->labdocs->forget($k);
                }
            }
        } else {
            $this->labdocs_shared = $query_shared
                 ->whereNull('deleted')
                 ->orderBy('position', 'ASC', 'id_labdoc', 'ASC')
                 ->get();
        }


        if (! $this->reportPart) {
            return;
        }
        // Texte sur les types de LD pouvant être ajoutés
        $this->type_labdoc = "";
        if (!$this->reportPart->text && !$this->reportPart->drawing && !$this->reportPart->dataset && !$this->reportPart->procedure && !$this->reportPart->code) {
            $this->title_text = "Vous ne pouvez pas ajouter de nouveaux labdocs dans cette partie de rapport.";
            $this->add_ld = false;
        } else {
            $this->add_ld = true;
            $this->title_text = "Dans cette partie de rapport, vous pouvez ajouter de nouveaux labdocs : \n";
            if ($this->reportPart->text) {
                $this->title_text .= "- textes \n";
                $this->type_labdoc .= "text";
            }
            if ($this->reportPart->drawing) {
                $this->title_text .= "- dessins \n";
                $this->type_labdoc .= " drawing";
            }
            if ($this->reportPart->dataset) {
                $this->title_text .= "- jeux de données \n";
                $this->type_labdoc .= " dataset";
            }
            if ($this->reportPart->procedure) {
                $this->title_text .= "- protocoles \n";
                $this->type_labdoc .= " procedure";
            }
            if ($this->reportPart->code) {
                $this->title_text .= "- codes \n";
                $this->type_labdoc .= " code";
            }
        }
    }

    /**
     * Constructor
     * @param \App\Report
     * @param \App\ReportPart
     * @param \App\User $user
     * @param string $scope
     * return \App\Views\ReportPart
     */
    public static function fromRP(\App\Report $report, \App\ReportPart $rp, \App\User $user, string $scope)
    {
        $rv = new \StdClass();
        $rv->report = $report;

        $rv->is_default = $scope == 'default';
        $rv->is_pdf = $scope == 'pdf';
        $rv->is_view = $scope == 'view';
        $rv->is_test = $scope == 'test';
        $rv->is_follow = $scope == 'follow';

        $rv->is_teacher = $user->isTeacher();
        $rv->is_learner = $user->isLearner();

        $rv->read_only = $rv->is_view || $rv->is_pdf || $rv->is_follow;
        $rv->scope = $scope;
        $rv->choice_ld = null;
        $rv->ld_chosen = [];
        return new self($rv, $rp);
    }

    /**
     * Returns a labdoc view for the specified labdoc
     * @param \App\Labdoc $ld
     * @return \App\LabdocView
     */
    public function toLabdocView($ld)
    {
        return new \App\Views\Labdoc($this, $ld);
    }
}
