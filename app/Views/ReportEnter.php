<?php

namespace App\Views;

use Illuminate\Support\Facades\DB;
use App\TeamConfig;
use App\User;

class ReportEnter
{
    /**
     * Find the existing report for this student and this teamconfig.
     *
     * @param \App\User $user
     * @param \App\TeamConfig $teamconfig
     * @return ?int id_report
     */
    public static function findReportId(User $user, TeamConfig $teamconfig) : ?int
    {
        $report = DB::selectOne(
            "SELECT id_report FROM report r JOIN link_report_learner lrl USING(id_report)"
            . " WHERE lrl.id_user = :id_user AND r.id_team_config = :id_team_config AND r.delete_time IS NULL LIMIT 1",
            ['id_user' => $user->id_user, 'id_team_config' => $teamconfig->id_team_config]
        );
        return $report ? $report->id_report : null;
    }

    /**
     * Assign a report to the student, if team_config suggests it.
     *
     * @param \App\User $user
     * @param \App\TeamConfig $teamconfig
     * @return ?int id_report
     */
    public static function assignReport(User $user, TeamConfig $teamconfig) : ?int
    {
        if ($teamconfig->method !== TeamConfig::METHOD_RANDOM) {
            return null;
        }
        return ReportIndex::completeRandomTeams($teamconfig, $user);
    }
}
