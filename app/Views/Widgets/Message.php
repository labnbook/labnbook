<?php

namespace App\Views\Widgets;

class Message extends Widget
{
    // TODO constants are not coherent
    public const ID = "messagerie";
    public const TABLE = "message_var";
    public const DIV = "div_msg";
    
    /** @var mixed $div_msg */
    public $div_msg;

    /** @var boolean $vis */
    public $vis;

    /** @var int $id_report */
    public $id_report;

    /**
     * Constructor
     * @param int $id_user
     * @param int $id_report
     */
    public function __construct($id_user, $id_report)
    {

        parent::__construct(self::TABLE, $id_user, self::DIV);
        $this->vis = $this->div_msg->visible ;
        $this->id_report = $id_report;
    }
}
