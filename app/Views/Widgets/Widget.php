<?php

namespace App\Views\Widgets;

use Illuminate\Support\Facades\DB;

class Widget
{
    const VISIBLE = 0;
    const POSX = 50;
    const POSY = 10;
    const WIDTH = 300;
    const HEIGHT = 150;

    /**
     * Constructor
     * @param string $table
     * @param int $id_user
     * @param string $div_name
     */
    public function __construct($table, $id_user, $div_name)
    {
        // récupération des paramètres de la fenêtre
        $this->$div_name = DB::table($table)
            ->where('id_user', $id_user)
            ->first();
        // si ces paramètres n'existent pas, on les initialise
        if (!$this->$div_name) {
            $this->$div_name = $this->insertDiv(
                $id_user,
                $table,
                self::VISIBLE,
                self::POSX,
                self::POSY,
                self::WIDTH,
                self::HEIGHT,
                $div_name
            );
        }
    }

    /**
     * Inserts the div parameters to db
     * @param int $id_user
     * @param string $table
     * @param int visible
     * @param int $posx
     * @param int posy
     * @param int width
     * @param int height
     * @param string div_name
     * @return StdClass
     */
    protected function insertDiv($id_user, $table, $visible, $posx, $posy, $width, $height, $div_name)
    {
            $res = new \StdClass();
            $res->id_user = $id_user;
            $res->visible = $visible;
            $res->pos_x = $posx;
            $res->pos_y = $posy;
            $res->width = $width;
            $res->height = $height;
            DB::table($table)
                ->insert((array)$res);
            return $res;
    }

    /**
     * Updates a labdoc params
     * @param int id_user
     * @param string widg
     * @param int x
     * @param int y
     * @param int h
     * @param int w
     * @param int vis
     */
    public static function saveParams($id_user, $widg, $x, $y, $h, $w, $vis)
    {
        if($widg == Message::ID) {
            $tab = Message::TABLE ;
        }
        $updates = [ ];
        if ($vis) {
            $updates = [
                'visible' => $vis,
                'pos_x' => $x,
                'pos_y' => $y,
                'width' => $w,
                'height' => $h,
            ];
        } else {
            $updates = [
                'visible' => 0,
            ];
        }
        if (!empty($updates)) {
            DB::table($tab)->where('id_user', $id_user)->update($updates);
        }
    }
}
