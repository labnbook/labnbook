<?php

namespace App\Views;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\TeamConfig;
use App\TeamChoice;
use App\Trace;
use PhpParser\Node\Expr\Cast\Object_;

class ReportIndex
{
    /**
     * Get a verified list of reports and missions, grouped by status.
     *
     * @param \App\User $user
     * @return object { new: [{ mission, report?, team_config? }], on: ... }
     */
    public static function getUserMissions($user)
    {
        $recordsByStatus = self::groupUserMissions($user);

        // search for duplicates
        $ids = [];
        foreach ($recordsByStatus as $entries) {
            foreach ($entries as $e) {
                if (isset($ids[$e->mission->id_mission])) {
                    $ids[$e->mission->id_mission]++;
                } else {
                    $ids[$e->mission->id_mission] = 1;
                }
            }
        }

        return $recordsByStatus;
    }

    /**
     * Return the current selected mission for this user.
     *
     * @param \App\User $user
     * @return Object
     */
    public static function getSelectedMission($user, $id_report=null, $id_team_config=null, $id_mission=null) {
        $records = ReportIndex::getUserMissions($user);
        $selected_record = null;
        foreach ($records as $record) {
            foreach ($record as $r) {
                if(isset($r->report) && intval($id_report) === $r->report->id_report) {
                    $selected_record = $r;
                } else if(isset($r->team_config) && intval($id_team_config) === $r->team_config->id_team_config) {
                    $selected_record = $r;
                } else if(isset($r->mission) && intval($id_mission) === $r->mission->id_mission) {
                    $selected_record = $r;
                }
                if($selected_record){
                    break 2;
                }
            }
        }
        return $selected_record;
    }

    /**
     * Add a LEAVE_REPORT trace if the user comes from a report and there was no such trace.
     *
     * This is successful only if the user has a full "Referer" header pointing to a report page.
     */
    public static function traceReportLeave($idUser)
    {
        // TODO this dont works if Adblock is activated
        if (empty($_SERVER['HTTP_REFERER']) || strpos($_SERVER['HTTP_REFERER'], '/report/') === false) {
            return;
        }
        $m = [];
        if (!preg_match('/^report\/(\d+)/', $_SERVER['HTTP_REFERER'], $m)) {
            return;
        }
        $lastTrace = Trace::getLastTraceAction($idUser);
        if ($lastTrace == Trace::LEAVE_REPORT || $lastTrace == Trace::SUBMIT_REPORT) {
            return;
        }
        Trace::logAction(Trace::LEAVE_REPORT, ['id_report' => (int) $m[1]]);
    }

    /**
     * Add a student to random new/existing teams
     *
     * @param object $tc team_config
     * @param \App\User $user
     * @return int id_report created
     */
    public static function completeRandomTeams($tc, $user) : ?int
    {
        // sort: first the small teams, new or not initialized, recently created
        $existingTeams = DB::select(
            DB::raw(
            <<<EOSQL
    SELECT *
    FROM (
        SELECT id_report, status, initialized, creation_time, COUNT(*) AS members FROM report r
        JOIN link_report_learner lrl USING(id_report)
        WHERE id_team_config = ?
        AND delete_time IS NULL
        GROUP BY id_report
    ) reports
    ORDER BY members ASC, (status = 'new') DESC, initialized ASC, creation_time DESC
EOSQL
        ),
            [$tc->id_team_config]
        );
        if (!$existingTeams) {
            $targetTeamId = null;
        } else {
            $firstTeam = $existingTeams[0];
            if (count($existingTeams) === (int) $tc->teams_max) {
                // no more teams, we use the smallest
                $targetTeamId = $firstTeam->id_report;
            } elseif ($firstTeam->members < (int) $tc->size_opt) {
                $targetTeamId = $firstTeam->id_report;
            } else {
                $targetTeamId = null;
            }
        }
        if ($targetTeamId) {
            DB::table('link_report_learner')
                ->insert([
                    'id_report' => $targetTeamId,
                    'id_user' => $user->id_user,
                ]);
        } else {
            $teaming = new TeamChoice($user);
            $targetTeamId = $teaming->createReport($tc, false);
            if (!$targetTeamId) {
                abort(500, "Error while creating the report");
            }
        }
        return $targetTeamId;
    }

    /**
     * Return a list of mission records that have no team for this user.
     *
     * @param \App\User $user
     * @return array [ {mission: x, team_config: y} ]
     */
    private static function getUserMissionsWithoutTeam($user)
    {
        // TODO explain why double join or remove it
        $sqlUserMissions = "SELECT r.id_team_config FROM link_report_learner lrl JOIN report r USING (id_report) JOIN report USING (id_report) WHERE report.delete_time IS NULL AND lrl.id_user = :id_user";
        $sql = <<<EOSQL
SELECT tc.*
FROM team_config tc
JOIN link_class_learner lcl USING(id_class)
LEFT JOIN ($sqlUserMissions) m USING(id_team_config)
WHERE lcl.id_user = :id_user AND m.id_team_config IS NULL
AND (tc.end_datetime IS NULL OR tc.end_datetime > NOW())
AND GREATEST(tc.creation_time, tc.update_time) > DATE_SUB(NOW(), INTERVAL 11 MONTH);
EOSQL;
        // for this user, team configs (by way of her/his classes) that have no report
        $teamConfigs = \App\TeamConfig::fromQuery(DB::raw($sql), [':id_user' => $user->id_user]);

        $toDisplay = [];
        foreach ($teamConfigs as $tc) {
            if ($tc->method == TeamConfig::METHOD_RANDOM) {
                self::completeRandomTeams($tc, $user);
            } else {
                $toDisplay[] = (object) [
                    'mission' => \App\Mission::find($tc->id_mission),
                    'team_config' => $tc,
                ];
            }
        }
        return $toDisplay;
    }

    /**
     * Return a list of mission records that have a team for this user.
     *
     * @param \App\User $user
     * @return array [ {mission: x, report: report} ]
     */
    private static function getUserMissionsWithTeam($user)
    {
        $reports = $user->reports;

        $missionIds = array_filter($reports->map(function($x) {
            return (int) $x->id_mission;
        })->all());
        $missions = [];
        if ($missionIds) {
            foreach (\App\Mission::find($missionIds) as $m) {
                $missions[$m->id_mission] = [$m];
            }
        }

        $toDisplay = [];
        foreach ($reports as $r) {
            $toDisplay[] = (object) [
                'mission' => (isset($missions[$r->id_mission]) ? $missions[$r->id_mission][0] : null),
                'report' => $r,
                'users' => $r->users->all(),
            ];
        }
        return $toDisplay;
    }

    /**
     * Get a raw list of reports and missions, grouped by status.
     *
     * @param \App\User $user
     * @return object { new: [{ mission, report?, team_config? }], on: ... }
     */
    private static function groupUserMissions($user)
    {
        $all = \App\Helper::transaction(function () use ($user) {
            return array_merge(
                self::getUserMissionsWithoutTeam($user), // before the other one, so that team may be auto-created
                self::getUserMissionsWithTeam($user)
            );
        });

        usort($all, function ($a, $b) {
            return strcmp($a->mission->code, $b->mission->code);
        });

        $grouped = (object) [
            'new' => [],
            'on' => [],
            'wait' => [],
            'arc' => [],
            'tuto' => [],
        ];
        $ids = [];
        foreach ($all as $a) {
            if (empty($a->report) || $a->report->status === 'new') {
                $grouped->new[] = $a;
            } elseif ($a->report->status !== 'test') {
                $grouped->{$a->report->status}[] = $a;
            }
            $ids[] = $a->mission->id_mission;
        }
        $tutorials = \App\Mission::where('status', 'tutorial')->whereNotIn('id_mission', $ids)->get();
        foreach ($tutorials as $tuto) {
            $grouped->tuto[] = (object) [
                'mission' => $tuto,
            ];
        }
        return $grouped;
    }
}
