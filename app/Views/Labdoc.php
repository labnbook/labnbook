<?php

namespace App\Views;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Labdoc
{
    /** @var \App\Labdoc */
    public $ld;

    /** @var \App\Report */
    public $report;

    /** @var \App\ReportPart */
    public $reportPart;

    /** @var \Illuminate\Support\Collection of \App\Labdoc */
    public $labdocs;

    /** @var string */
    public $scope;

    /** @var array */
    public $ld_chosen;

    /** @var string */
    public $choice_ld;

    /** @var bool             is the scope "default" */
    public $is_default;
    /** @var bool             is the scope "pdf" */
    public $is_pdf;
    /** @var bool             is the scope "view" */
    public $is_view;
    /** @var bool             is the scope "test" */
    public $is_test;
    /** @var bool             is the scope "follow" */
    public $is_follow;

    /** @var bool             is the status "solution" */
    public $is_solution;

    /** @var bool             is the user a teacher */
    public $is_teacher;
    /** @var bool             is the user a teacher */
    public $is_learner;


    /** @var bool            is the report in read only mode */
    public $read_only;

    /** @var array           classes for the view */
    public $classes;

    /** @var bool */
    public $edition;

    /** @var bool           are we showing the labdoc history*/
    public $versioning;

    /** @var bool            Should we extend the labdoc */
    public $extend;
    
    /** @var integer        last version reviewed by a teacher */
    public $last_id_version_reviewed;

    /** @var ???|null */
    private $ld_status;

    /**
     * Constructor
     * @param \App\Views\ReportPart $reportPartView
     * @param \App\Labodc $ld
     */
    public function __construct($reportPartView, $ld)
    {
        if ($reportPartView) {
            // Recopy fields from reportPartView
            $FIELDS = [
                'is_test',
                'is_follow',
                'is_default',
                'is_solution',
                'is_pdf',
                'is_view',
                'is_teacher',
                'is_learner',
                'read_only',
                'scope',
                'choice_ld',
                'ld_chosen',
                'report'
            ];
            foreach ($FIELDS as $field) {
                $this->$field = $reportPartView->$field;
            }
        }
        $this->edition = 0;
        $this->versioning = false;
        $this->init($ld);
        $this->is_shared = $this->ld->shared || ( $this->ld->parentLD && $this->ld->parentLD->shared && (true ||$this->is_test || $this->is_solution));
    }

    /**
     * Finalize labdoc view's initialization
     * @param \App\Labdoc $ld
     */
    public function init($ld)
    {
        $this->ld = $ld;
        $this->ld_status = null;
        $this->classes = ["labdoc"];
        if ($ld->draft && !$this->is_follow && !$this->is_pdf) {
            $this->classes[] = "ld_draft";
        }
        if (!$ld->editable) {
            $this->classes[] = "editable-no";
        }

        $this->extend = 1;
        if (!$this->is_pdf && $this->getLdStatus()) {
            $this->extend = (int) $this->getLdStatus()->extend;
        }

        if (!empty($this->ld_chosen) && array_key_exists('ld_chosen'.$ld->id_labdoc, $this->ld_chosen)) {
            $this->ld_chosen = $ld->id_labdoc;
        } else {
            $this->ld_chosen = -1;
        }
        $user = Auth::user();
        $this->last_id_version_reviewed = $ld->getLastIdVersionReviewed($user);
    }

    /**
     * Constructor
     * @param \App\Labodc $ld
     * @param \App\User $user
     * @param string $scope
     * @param bool $edition
     * @param bool $versioning
     * @return \App\Views\Labdoc
     */
    public static function fromLd($ld, $user, $scope, $edition, $versioning=false)
    {
        $ldv = new self(null, $ld);
        $ldv->scope = $scope;

        $ldv->is_default = $scope == 'default';
        $ldv->is_pdf = $scope == 'pdf';
        $ldv->is_view = $scope == 'view';
        $ldv->is_test = $scope == 'test';
        $ldv->is_follow = $scope == 'follow';
        $ldv->is_solution = $ld->report && $ld->report->status === 'solution';

        $ldv->is_teacher = $user->isTeacher();
        $ldv->is_learner = $user->isLearner();

        $ldv->read_only = $ldv->is_view || $ldv->is_pdf || $ldv->is_follow;
        $ldv->edition = (int) $edition;
        $ldv->report = $ld->report;
        $ldv->ld_chosen = [];
        $ldv->init($ld);
        $ldv->versioning = $versioning;
        $ldv->last_id_version_reviewed = $ld->getLastIdVersionReviewed($user);

        return $ldv;
    }

    /**
     * Returns labdoc status object
     * @return array
     */
    public function getLdStatus()
    {
        if ($this->ld_status == null) {
            $this->ld_status = DB::table('labdoc_status')
                ->where('id_labdoc', $this->ld->id_labdoc)
                ->where('id_user', Auth::Id())
                ->first();
        }
        return $this->ld_status;
    }

    /**
     * Returns true if the labdocs has unread comments for the given user
     * @param int id_user
     * @return boolean
     */
    public function hasUnreadComments()
    {
        return \App\Labdoc::unreadComments($this->ld->id_labdoc, Auth::id())->exists();
    }

    /**
     * Update fields from labdoc status
     * when calling this method, you are responsible for make sure
     * that field is a correct labdoc_status field and that value is acceptable
     * @param array $values [ 'field' => 'value', ... ]
     */
    private function updateStatus($values)
    {
        DB::table('labdoc_status')
                ->where('id_labdoc', $this->ld->id_labdoc)
                ->where('id_user', Auth::Id())
                ->update($values);
    }
}
