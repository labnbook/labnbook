<?php

namespace App\Views;

use App\Processes\LabdocDiff;
use App\Scoring\Rubric;
use App\Trace;
use App\Views\Widgets\Scoring;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;
use App\Scoring\Assessment;

class Report
{
    /**
     *   Helper class for Report view
     */

    /** @var \App\Report     Report */
    public $report;

    /** @var \App\Mission    Report's mission */
    public $mission;

    /** @var string          View scope : default|view|pdf|test|follow */
    public $scope;

    /** @var bool             is the scope "default" */
    public $is_default;
    /**
     */
    public $is_pdf;
    /**
    * @var bool             is the scope "pdf"
    /** @var bool             is the scope "view" */
    public $is_view;
    /** @var bool             is the scope "test" */
    public $is_test;
    /** @var bool             is the scope "follow" */
    public $is_follow;
    /** @var bool            is the report in read only mode */
    public $read_only;


    /** @var bool             should we show the message widget */
    public $widg_msg;

    /** @var bool             should we freeze the lds on init */
    public $freeze_lds_on_init;
    /** @var bool             should we open the scoring on init */
    public $open_scoring_on_init;


    /** @var bool             are we in an external report ie /report/test */
    public $is_external_test_report;
    /** @var bool             is the user a teacher */
    public $is_teacher;
    /** @var bool             is the user a teacher */
    public $is_learner;

    /** @var string          usersnames in the group */
    public $users_in_group;

    /** @var array           list of selected labdoc ids */
    public $ld_chosen;

    /** @var string          'chose_ld'|'no_draft'|'all_ld'|'' */
     public $choice_ld;


    /** @var bool            should we print empty report parts */
    public $print_empty_rp;
    /** @var bool            should we print assignments */
    public $print_assignment;

    /** @var bool|null       should we show the assignments for learners */
    public $assignment;

    /** @var bool */
    public $puppeteer;

    /** @var string          report title */
    public $title;

    /** @var string          Adress to use for teacher interface */
    public $teacherAddress;

    /** @var string          Should we add resource in widget resource */
    public $add_resource;

    /** @var string          filepath for pdf print */
    public $filepath;

    /** @var bool          Should we  show diff button*/
    public $is_diffable;
    
    /**
     * Constructor
     * @param \App\Report $report
     * @param string $scope how to view the report default|view|pdf|test|follow
     * @param array $ld_chosen list of selected labdoc ids
     * @param string $choice_ld 'chose_ld'|'no_draft'|'all_ld'|''
     * @param bool $print_empty_rp
     * @param bool $print_assignment
     * @param bool|null $assignment
     * @param bool $puppeteer
     */
    public function __construct($report, $scope, $ld_chosen, $choice_ld, $print_empty_rp, $print_assignment, $assignment, $puppeteer)
    {
        $this->report = $report;
        $this->mission = $report->mission;

        $this->scope = $scope;

        $this->is_default = $scope === 'default';
        $this->is_pdf = $scope === 'pdf';
        $this->is_view = $scope === 'view';
        $this->is_test = $scope === 'test';
        $this->is_follow = $scope === 'follow';
        $this->is_solution = $report->status === 'solution';

        $user = Auth::user();
        $this->is_external_test_report = (boolean) ($user->id_user === (int)config('labnbook.id_user_test') && $report->id_mission === (int)config('labnbook.id_mission_test'));
        $this->is_teacher = $user->isTeacher();
        $this->is_learner = $user->isLearner();
        $this->puppeteer = $puppeteer; 

        // Boolean
        $this->widg_msg = $report->allow_msg_teacher+$report->allow_msg_team+$report->allow_msg_id_class+$report->allow_msg_id_mission;
        
        $this->freeze_lds_on_init = false;
        $this->open_scoring_on_init = false;

        $this->read_only = $this->is_view || $this->is_pdf;

        $num_users = $report->users->count();
        $this->users_in_group = "";
        $this->users_in_group_plain = "";
        foreach ($report->users as $u) {
            if (!$u->isAdmin() && ($report->status !== "solution" || $report->mission->designers->contains(fn($d)=>$d->id_teacher === $u->id_user))) {
                $class = $u->id_user == Auth::id() ? "user_self" : "";
                $name = $u->first_name . " " . $u->user_name;
                $this->users_in_group .= '<span id="user_in_team_' . $u->id_user . '" class="user_in_team ' . $class . '" name="' . $name . '">';
                if ($num_users > 3 && $report->status !== "solution") {
                    if ($this->is_follow) {
                        $name = substr($u->first_name, 0, 1) . ". " . $u->user_name;
                    } else {
                        $name = $u->first_name . " " . substr($u->user_name, 0, 1) . ".";
                    }
                }
                $this->users_in_group .= $name . '</span> - ';
                $this->users_in_group_plain .= $name . ' - ';
            }
        }
        $this->users_in_group = substr($this->users_in_group, 0, -3);  // supprime le dernier tiret
        $this->users_in_group_plain = substr($this->users_in_group_plain, 0, -3);

        $this->ld_chosen = $ld_chosen;
        $this->choice_ld = $choice_ld;

        $this->print_empty_rp = (bool) $print_empty_rp;
        $this->print_assignment = (bool) $print_assignment;

        $this->assignment = (bool) $assignment;

        $this->title = $this->mission->code . ' - ';
        if ($this->is_follow) {

            // TODO : remove it in june 2025
            // we hide all lds when a user open a report in follow
            foreach ($this->labdocs() as $labdoc) {
                $labdoc->updateStatus(['extend'=>0], Auth::user()->id_user);
            }

            if ($this->report->team_name) {
                $this->title .= $this->report->team_name;
            } else {
                $this->title .= $this->users_in_group_plain;
            }
        } else {
            if ($this->is_test) {
                $this->title .= "test";
            } elseif ($this->is_pdf) {
                $this->title .= ' ' .  Carbon::now()->toDateTimeString();
                $this->filepath = sys_get_temp_dir() . '/' . $this->title . 'pdf';
                if (!$this->puppeteer) {
                    $user = Auth::user();
                    $this->puppeteerData = [
                        'query' =>  [
                            'choice_ld' => $this->choice_ld,
                            'print_empty_rp' => (int)$this->print_empty_rp,
                            'print_assignment' => (int)$this->print_assignment,
                            'assignment' => (int)$this->assignment,
                            'sc' => 'pdf',
                            'puppeteer' => $user->generateRememberToken(),
                            'id_user' => $user->id_user,
                        ],
                        'report' => $this->report->id_report,
                        'filepath' => $this->filepath,
                        'nom' => $this->report->team_name." : ".$this->users_in_group_plain,
                        'debug' => config('app.debug'),
                    ];
                    $this->puppeteerData['query'] = array_merge($this->puppeteerData['query'], $this->ld_chosen);
                }
            }
        }
        $this->title = trim($this->title, '- ');

        if ($this->is_default || $this->is_follow) {
            $this->add_resource = "active";
        } elseif ($this->is_test) {
            $this->add_resource = "inactive";
        } else {
            $this->add_resource = "invisible";
        }
        
        // States whether diff button is shown
        $this->is_diffable = LabdocDiff::isReportDiffable($report, $user);
    }

    /**
     * Returns a reportPartView for the specified report part
     * @param \App\ReportPart $rp
     * @return \App\ReportPartView
     */
    public function toReportPartView($rp)
    {
        return new \App\Views\ReportPart($this, $rp);
    }

    public function labdocs()
    {
        return $this->report->labdocs()
                            ->join('report_part', 'labdoc.id_report_part', '=', 'report_part.id_report_part')
                            ->whereNotNull('labdoc.id_report_part')
                            ->whereNull('deleted')
                            ->orderBy('report_part.position', 'ASC')
                            ->orderBy('labdoc.position', 'ASC')
                            ->orderBy('id_labdoc', 'ASC')
                            ->get();
    }

    /**
     * Add a report enter trace
     *
     */
    public function traceReportEnter($user)
    {
        $report = $this->report;
        $mission = $report->mission;
        $trace = null;
        if ($this->is_external_test_report) {
            $trace = Trace::ENTER_REPORT;
            $args = ['id_report' => $report->id_report, 'id_mission' => $mission->id_mission, 'code' => $mission->code];
        } elseif ($this->is_follow) {
            $trace = Trace::TEACHER_FOLLOW_REPORT;
            $args = ['id_report' => $report->id_report, 'id_mission' => $mission->id_mission, 'code' => $mission->code];
        } elseif ($this->is_test) {
            $trace = Trace::TEACHER_TEST_MISSION;
            $args = ['id_mission' => $mission->id_mission, 'code' => $mission->code];
        } elseif ($this->is_view  || $this->is_default) {
            $trace = Trace::ENTER_REPORT;
            $args = ['id_report' => $report->id_report, 'id_mission' => $mission->id_mission, 'code' => $mission->code];
        }
        if ($trace) {
            Trace::logAction($trace, $args);
        }
    }

    /**
     * States whether a rubric exists
     * @return boolean
     */
    public function rubricExists()
    {
        $r = Rubric::fromLocalLink($this->report->mission->id_mission);
        return $r !== null;
    }
    
    /**
     * States whether an assessment exists and is published
     * @return boolean
     */
    public function assessmentPublished()
    {
        $r = Assessment::fromLocalLink($this->report->id_report);
        return ($r !== null && $r->published == true);
    }
}
