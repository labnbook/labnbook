<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class SimulationGlobalKnowledge extends Model
{
    protected $table = 'simulation_global_knowledge';
    protected $primaryKey = 'id_global_knowledge';
    public $fillable = ['simulation_code', 'description', 'position'];

    /**
     * @return HasMany
     */
    public function taskTypes(): HasMany
    {
        return $this->HasMany('App\SimulationTaskType', 'id_global_knowledge', 'id_global_knowledge');
    }

}
