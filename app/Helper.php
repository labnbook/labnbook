<?php

namespace App;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Log;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Mail;
use App\Mail\MarkdownMail;

class Helper{

    /**
     * Last modification of a LD in a report : user & timestamp
     * 
     * @param int $report_id
     * @return object {user_name, first_name, last_time}
     */
    public static function lastModificationInReport($report_id) {
        $sql_msgs = "SELECT user_name, first_name, MAX(FROM_UNIXTIME(last_edition)) as last_time "
            ."FROM labdoc JOIN user ON last_editor = id_user "
            ."WHERE labdoc.deleted IS NULL AND id_report = :rep_id";
        return DB::selectOne(DB::raw($sql_msgs), ['rep_id' => $report_id]);
    }

    /**
     * Append the message to the log file.
     *
     * @param string $message
     * @param string $file Path to the log file (default: "storage/logs/laravel_date.log")
     * @param int $time (optional)
     * @param string $trace (optional)
     */
    public static function logIntoFile($message, $file = null, $time = null, $trace = "") {
        if (!$time) {
            $time = $_SERVER['REQUEST_TIME'];
        }
        if (!$trace) {
            $rawTrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 4);
            array_shift($rawTrace);
            $trace = self::beautifyBacktrace($rawTrace);
        }
        $log = sprintf(
            "%s - %s - %s - USER: %d - %s\nTRACE: %s\n\n",
            date('Y-m-d H:i:s', $time),
            (isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : '') . ' ' . (isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : ''),
            $message,
            (Auth::check()) ? Auth::id(): '',
            isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : 'CLI',
            $trace
        );
        if (!$file) {
            Log::Error($log);
        } else {
            error_log($log, 3, storage_path("logs/$file"));
        }
    }

    /**
     * Returns the code from the exception
     * @param \Throwable $exception
     * @return int
     */
    public static function getExceptionCode(\Throwable $exception)
    {
        if (method_exists($exception, 'getStatusCode')) {
            return  $exception->getStatusCode();
        }
        $exceptionCodes = [
            404 => [NotFoundHttpException::Class, ModelNotFoundException::Class],
            403 => [AuthorizationException::Class, AccessDeniedHttpException::Class],
        ];
        foreach ($exceptionCodes as $code => $classes) {
            foreach ($classes as $c) {
                if ($exception instanceof $c) {
                    return $code;
                }
            }
        }
        return $exception->getCode();
    }

    /**
     * Reports an exception to the bug_email_report_to address if defined
     */
    public static function reportError(\Throwable $exception)
    {
        Log::Error($exception);
        try {
            $addr = config('labnbook.bug_email_report_to');
            if (!$addr) {
                return;
            }
            $code = self::getExceptionCode($exception);
            $data = [
                'message' => $exception->getMessage(),
                'class' =>  get_class($exception),
                'trace' => $exception->getTraceAsString(),
                'code' => $code,
                'request' => request()->input(),
                'session' => session()->all(),
            ];
            if (!config('app.debug')) {
                Mail::to($addr)->send(new MarkdownMail('emails.errors', 'Une erreur est survenue', $data));
            }
        } catch (Exception $e) {
            Log::Error($e);
        }
    }

    public static function logJsError($message, $time, $trace) {
        $startsWithTinymce = false;
        if (!empty($trace)) {
            $decodedTrace = json_decode($trace);
            if ($decodedTrace && is_array($decodedTrace) && !empty($decodedTrace[0]->fileName)) {
                $startsWithTinymce = preg_match('#/tinymce/#', $decodedTrace[0]->fileName);
            }
        }
        if ($startsWithTinymce) {
            $logFile = 'tinymce_errors.log';
        } else {
            $logFile = 'js_errors.log';
        }
        self::logIntoFile($message, $logFile, $time, $trace);
    }

    /**
     * Convert a trace into a nice string to display in logs.
     *
     * @param array $trace Produced with debug_backtrace()
     * @return string
     */
    public static function beautifyBacktrace($trace) {
        return join("\n", array_map(
            function ($x) {
                foreach (['class', 'function', 'file', 'line'] as $key) {
                    if (!array_key_exists($key, $x)) {
                        $x[$key] = '';
                    }
                }
                return sprintf("%s::%s() in %s:%d", $x["class"], $x["function"], $x["file"], $x["line"]);
            },
            $trace
        ));
    }

    /**
     * Initialize the asset hash, should be called by configuration
     * @return array
     */
    public static function initAssetsHashes()
    {
        if(!file_exists(base_path().'/'.'assets_hashes')){
            return json_encode([]);
        }
        $hashes = explode("\n", file_get_contents(base_path('assets_hashes')));
        $out = [];
        foreach ($hashes as $line) {
            if (!empty($line)) {
                try {
                    $values = explode(' ', preg_replace('@\s\s*\*?/?public@', ' ',$line));
                    $out[$values[1]] = $values[0]; // $values[0] is the hash
                } catch (\Exception $e) {
                    // Exception during init asset hashes are recoverable
                    Log::Error('Error while generating asset hashes: "'.$e->getMessage().'"');
                }
            }
        }
        return json_encode($out);
    }

    /**
     * Return the url with has of the i18next lang
     * @return string
     */
    public static function geti18nLangFileUrl()
    {
        $lang = self::selectLanguage();
        $url = "/data/languages/".$lang."/translation.json";
        return $url."?hash=".self::getAssetHash($url);
    }

     /*
     * Return the asset hash (md5sum) if it is pre-computed or an empty string
     * @param string the hash path
     * @return string
     */
    public static function getAssetHash($url)
    {
        $hashes = (array)json_decode(config('labnbook.assets_hashes'));
        return array_key_exists($url, $hashes) ? $hashes[$url] : "";
    }

    public static function getAbsoluteUrl($url)
    {
        if (strncmp($url, 'http', 4) === 0 || strncmp($url, '//', 2) === 0) {
            // $url is an URL
            $absoluteUrl = $url;
        } else {
            $absoluteUrl = $url[0] === '/' ? $url : '/' . $url;
            // $url is a path
            $hash = self::getAssetHash($url);
            if ($hash) {
                $absoluteUrl .= "?hash=" . $hash;
            }
        }
        return $absoluteUrl;
    }

    /**
     * From an path relative to LabNBook's root or an URL, return the HTML that loads the asset.
     *
     * E.g. < ?= loadAsset('/js/common.js') ? >
     * (if you copy this, remove the unbreakable spaces)
     *
     * @param string $url /repository/root/to/asset OR https://external.doc/asset OR //local/url/asset
     * @param array|string $htmlAttributes E.g. "async" OR ["charset" => "utf-8"]
     * @return string HTML
     * @throws Exception
     */
    public static function loadAsset($url, $htmlAttributes = []) {

        $templates = [
            'css' => '<link type="text/css" rel="stylesheet" href="%s"%s %s/>' . "\n",
            'js' => '<script src="%s"%s %s></script>' . "\n",
        ];

        try {
            $version = "";
            $m = [];
            if (!preg_match('/\.(\w+)(\?|$)/', $url, $m)) {
                throw new Exception("No file extension in URL: $url");
            }
            $extension = $m[1];
            $absoluteUrl = self::getAbsoluteUrl($url); 
            if (!isset($templates[$extension])) {
                throw new Exception("Unknown extension in the URL: $url");
            }
        } catch (Exception $ex) {
            if (config('app.debug')) {
                throw new Exception($ex->getMessage());
            } else {
                error_log("ERREUR loadAsset() : " . $ex->getMessage());
            }
        }
        if ($htmlAttributes) {
            if (is_array($htmlAttributes)) {
                $attributes = " ";
                foreach ($htmlAttributes as $k => $v) {
                    $attributes .= " $k=\"" . htmlspecialchars($v) . '"';
                }
            } else {
                $attributes = " " . $htmlAttributes;
            }
        } else {
            $attributes = '';
        }
        return sprintf($templates[$extension], $absoluteUrl, $version, $attributes);
    }

    public static function addAlert($category, $message) {
        $alerts = session()->get('alerts');
        if (!$alerts) {
            $alerts = [$category => []];
        } else if (!isset($alerts[$category])) {
            $alerts[$category] = [];
        }
        $alerts[$category][] = $message;
        // do not use flash(): data will be remove when pulled, not on next HTTP request
        session()->put('alerts', $alerts);
    }

    /**
     * @return boolean
     */
    public static function hasAlerts()
    {
        return config('labnbook.maintenance') != '' || !empty(session()->get('alerts'));
    }

    /**
     * @return int error code for the last policy call that failed or 403
     */
    public static function pullPolicyErrorCode()
    {
        return session()->pull('policyErrorCode', 403);
    }

    /**
     * @return string error message for the last policy call that failed or ""
     * @param string $default
     * @return string
     */
    public static function pullPolicyErrorMessage($default = "Forbidden")
    {
        return session()->pull('policyErrorMsg', __($default));
    }

    /**
     * Set policy error code and message
     * @param int $code
     * @param string $message
     */
    public static function setPolicyError($code, $message)
    {
        session()->put('policyErrorCode', $code);
        return session()->put('policyErrorMsg', $message);
    }

    /**
     * check that the given url is an existing internal url
     * @param string $url
     * @return boolean
     */
    public static function internalUrlExists($url)
    {
        try {
            $route = Route::getRoutes()->match(Request::create($url));
        } catch (NotFoundHttpException $e) {
            return false;
        }
        return true;
    }

    /**
     * Build an URL that will change when the file is changed (mtime).
     *
     * @param string $path Path from the LabNBook root to the file, e.g. "/css/common.css"
     * @return string Absolute URL, without protocol
     */
    public static function getCacheproofUrl($path) {
        $file = dirname(__DIR__) .'/public/'. $path;
        if (!is_file($file)) {
            http_response_code(404);
            throw new \Exception("No file matching the URL: $path");
        }
        $version = substr(md5(filemtime($file)), 0, 4);
        return "$path?v=$version";
    }

    /**
     * Returns a mailto url for $type
     * @param string $type 'bug'|'rgpd'|'support'
     */
    public static function getMailto($type)
    {
        if ($type == 'bug') {
            $addr = config('labnbook.bug_email_report_to');
            $params = [
                "subject" => __("Signalement de bug LabNBook"),
                "body" => __("Merci de nous fournir les informations suivantes") . __(" :") . " 
 
   "
                . __("- Adresse (URL) de la page sur laquelle s'est produit le bug") . __(" :") . " 
 
   "
                . __("- Description la plus précise possible du bug") . __(" :") . " 
 
   "
                . __("- Éventuellement, copies d'écran") . __(" :") . " 
 
   "
                . __("- Éventuellement, copie de la console de votre navigateur (ouverture par F12)") . __(" :") . " 
 
 "
                . __("Merci d'avoir pris du temps pour nous aider à améliorer LabNBook.") . " "
            ];
        } elseif ($type == 'rgpd') {
            $addr = config('labnbook.rgpd_email_to');
            $params = [
                "subject" => __("Demande RGPD LabNBook"),
            ];
        } else {
            $addr = config('labnbook.support_email_to');
            $params = [
                "subject" => __("Demande d'assistance LabNBook"),
            ];
        }
        return "mailto:$addr?".http_build_query($params, '', '&', PHP_QUERY_RFC3986);
    }

    /**
     * Checks if an url is outside of LabNBook
     * @param string url
     * @return boolean
     */
    public static function isExternalUrl($url)
    {
        return 0 === strpos($url, 'http') && false === strpos($url, config('app.url'));
    }

    /**
     * Safely redirects back
     * in other word redirects back without creating a loop redirect
     * this function is called by the exception handler after an abort
     * @param string $error error message to display while redirecting
     * @return \Illuminate\Http\Response
     */
    public static function redirectBack($error)
    {
        $loop = session()->pull('redirect', null);
        if ($error && !$loop) {
            self::addAlert('danger', $error);
        }
        $previous = url()->previous();
        // If we are already in a redirect or previous is current or previous is not in LNB : we abbort
        if (url()->current() == $previous || $loop || self::isExternalUrl($previous)) {
            return redirect('/');
        }
        // Keep track of the redirect
        session()->push('redirect', url()->previous());
        return redirect()->back();
    }

// **************************************************************************************************************************
//                                        date_time
// **************************************************************************************************************************

    /**
     * Return a string representing the datetime in the current locale.
     *
     * @param string $datetime E.g. "2019-01-29 23:59:59" ou "2019-02-16 14:10:00"
     * @return string E.g. "29/01/2019" ou "16/02/2019 à 14:10"
     */
    static function datetimeToFr($datetime) {
        $dt = new \DateTimeImmutable($datetime);
        $frDate = $dt->format("d/m/Y à H:i");
        return preg_replace('/ à (00:00|23:59)$/', '', $frDate);
    }

    function datetime2date($date) {  // renvoie la date au format jj/mm/aa
        $year   = substr($date,2,2); // découpage de la chaine date MySql en tronçons
        $month  = substr($date,5,2);
        $day    = substr($date,8,2);
        $date = $day."/".$month."/".$year ;
        return $date;
    }

    function datetime2time($date) {  //renvoie l'heure au format hh:mm
        $hour   = substr($date,11,2);
        $minute = substr($date,14,2);
        $time = $hour.":".$minute ;
        return $time;
    }

    function datetime2time_s($date) {  //renvoie l'heure au format hh:mm:ss
        $sec   = substr($date,17,2);
        $time = datetime2time($date).":".$sec ;
        return $time;
    }

    function datetime2sec($date) { 	// renvoie le nombre de secondes écoulées entre le 1er janvier 1970 et la date passée
        if (!$date || trim($date) === '') {
            return null;
        }
        date_default_timezone_set('Europe/Paris');
        $year   = substr($date,0,4); // découpage de la chaine date MySql en tronçons
        $month  = substr($date,5,2);
        $day    = substr($date,8,2);
        if (strlen($date)>10) { $hour = substr($date,11,2); } else { $hour = 0 ; }
        if (strlen($date)>13) { $minute = substr($date,14,2); } else { $minute = 0 ; }
        if (strlen($date)>16) { $sec = substr($date,17,2); } else { $sec = 0 ; }
        return mktime($hour, $minute, $sec, $month, $day, $year) ;
    }
    /**
     * Remove the <body></body> to extract the content of the body node
     * @param string $html
     * @return string HTML
     */
    public static function innerHTMLOfBodyNode($html) {
        return substr($html,6/*count(<body>)*/,-7/*count(</body>)*/);
    }

    /**
     * Adds CSRF token header to all jquery's XHR
     * @return string HTML
     */
    public static function configureAjax()
    {
        return <<<'EOJS'
<script >
if (window.jQuery) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    }else{
        function requestHeaders(headers) {
            headers['X-CSRF-TOKEN'] = document.head.querySelector("meta[name='csrf-token']").content;
            return headers;
        }
    }
</script>
EOJS;
    }

    public static function relativeDate($date, $full = false)
    {
        date_default_timezone_set('Europe/Paris');
        $format = 'Y-m-d H:i:s';
        $date = \DateTime::createFromFormat($format, $date);
        if ($full) {
            return $date->format("d/m/y H:i");
        }
        $now = new \DateTime("now", new \DateTimeZone('Europe/Paris'));
        $interval = $date->diff($now);
        if (intval($interval->format('%y'))>=1){
            $time = $date->format("d/m/y");
        } elseif (intval($interval->format('%m')) >= 1 || intval($interval->format('%d')) > 1) {
            $time = $date->format("d/m");
        } elseif (intval($interval->format('%d')) <= 1 && $date->format('d') != $now->format('d')) {
            $time = "Hier";
        } else {
            $time = $date->format("H:i");
        }
        return $time ;
    }

    /**
    * Available languages in LabNBook
    *
    * @return array { 2-letters => public-name }
    */
    public static function allLabnbookLanguages()
    {
        return array("fr" => "Français", "en" => "English");
    }

    /**
     * Select the language defined by the browser if available in translations.
     *
     * @return string 2-letters code
     */
    public static function selectLanguage() {
        if (Auth::check() && Auth::user()->lang) {
            return Auth::user()->lang;
        }
        $possibleLanguages = self::allLabnbookLanguages();
        $headers = isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) ? $_SERVER['HTTP_ACCEPT_LANGUAGE'] : "";
        if ($headers) {
            $langs = explode(',', $headers);

            foreach ($langs as $value) {
                $lang = substr($value, 0, 2);
                if (isset($possibleLanguages[$lang])) {
                    return $lang;
                }
            }
        }
        // default language
        return "fr";
    }

    // Retro-compatibility fonction to logout and go to login page
    public static function logout()
    {
        auth()->logout();
        session()->invalidate();
        echo redirect(config('app.url').'/login');
        exit();
    }

    /**
     * Conver relative urls in html to absolute urls
     * @param string $html
     * @return string
     */
    public static function makeUrlsAbsolute($html)
    {
        $baseUrl = url('/');
        $TAGS = ['src', 'href'];
        $PATTERNS = ['"/storage', '"../storage'];
        $replace = '"'.$baseUrl.'/storage';
        foreach ($PATTERNS as $pattern) {
            foreach ($TAGS as $tag) {
                $pat = $tag.'='.$pattern;
                $html = str_replace($pat, $tag.'='.$replace, $html);
            }
        }
        return $html;
    }

    /**
     * Anonymse string (hide all but first and last letter)
     * @param string $str
     * @param boolean $email : treat string as email address anonymse only username
     * @return string
     */
    public static function anonymizeString($str, $email = false)
    {
        if ($email) {
            $parts = explode('@', $str);
            return self::anonymizeString($parts[0]).'@'.$parts[1];
        } else {
            // Replace all but first and last letter
            return preg_replace("/(?!^).(?!$)/", "*", $str);
        }
    }


    /**
     * Remove variables for binding / creating user with API / CAS from session
     */
    public static function cleanBindSession() {
        if (session()->has('bindUrl')) {
            $fields = ['authType', 'bindUrl', 'confirmUrl', 'loginFailUrl', 'url.intended', 'createAllowed'];
            foreach($fields as $name) {
                session()->pull($name);
            }
        }
    }

    /**
     * Runs a function in a DB transaction and return it's return value on success
     * @return mixed
     * @throws \Exception
     */
    public static function transaction($fct)
    {
        DB::beginTransaction();
        $out = $fct();
        try {
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
        return $out;
    }

    /**
     * Returns the current git branch of the code
     * @return string
     */
    public static function getGitBranch()
    {
        return trim(exec("git branch | awk '/^\*/{print $2}'"));
    }

    /**
     * Returns the version string of the app
     * @return string
     */
    public static function getVersion()
    {
        $commitDate = new \DateTime(trim(exec('git log -n1 --pretty=%ci HEAD')));

        return __(
            'Branche :branch, Commit :commit (:date)',
            [
                'branch' => self::getGitBranch(),
                'commit' => trim(exec('git log --pretty="%h" -n1 HEAD')),
                'date' => $commitDate->format('Y-m-d H:i:s')
            ]
        );
    }

    /**
     * Convert a PHP-ini human size (4M) to a number of bytes.
     *
     * @param string $hsize
     * @return int
     */
    public static function parseHumanSize($hsize)
    {
        $matches = [];
        if (preg_match('/^(\d+)([bkmgtpezy]?)$/i', $hsize, $matches)) {
            if ($matches[2]) {
                $power = pow(1024, strpos('bkmgtpezy', strtolower($matches[2])));
                return round($matches[1] * $power);
            } else {
                return round($matches[1]);
            }
        } else {
            return null;
        }
    }

    /**
     * @param string $beta_feature_key
     * @param Report|null $report_for_student_authorization : if user is a student, report on which he or she can perform a beta-test
     * @return bool
     */
    public static function isBetaFeatureAuthorized(string $beta_feature_key, ?Report $report_for_student_authorization=null): bool
    {
        $user = Auth::user();
        $is_feature_enabled_for_beta_testing = isset(config('labnbook.beta_testers')[$beta_feature_key]);
        if (!$is_feature_enabled_for_beta_testing) {
            return false;
        }
        $config_feature = config('labnbook.beta_testers')[$beta_feature_key];
        $is_feature_enabled_for_everybody = $config_feature === "ALL";
        if ($is_feature_enabled_for_everybody) {
            return true;
        }
        $is_authorized = in_array($user->id_user, $config_feature);
        if ($is_authorized) {
            return true;
        }
        if (!is_null($report_for_student_authorization)) {
            abort_if($user->cannot('update', $report_for_student_authorization), 403);
            $teachers_list = $report_for_student_authorization->teachers()->pluck('id_teacher');
            // A student is authorized for beta testing if at least one of his teachers is beta-tester
            $is_student_of_authorized = ($teachers_list
                ->reduce(function (?bool $carry, int $id_user) use ($beta_feature_key, $config_feature) {
                   return  $carry || in_array($id_user, $config_feature);
                }, false));
        } else {
            $is_student_of_authorized = false;
        }
        return $is_student_of_authorized;
    }

    /**
     * @return string: the beta testing features corresponding to the connected user
     */
    public static function getBetaTestingFeatures(): string
    {
        $user = Auth::user();
        $beta_testing_features = [];
        foreach (config('labnbook.beta_testers') as $key => $value) {
            if ($value === 'ALL' || in_array($user->id_user, $value)) {
                $beta_testing_features[] = $key;
            }
        }
        return implode(", ", $beta_testing_features);
    }
}
