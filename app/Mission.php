<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use phpDocumentor\Reflection\Types\Collection;

/**
 * App\Mission
 *
 * @property int $id_mission
 * @property string|null $code
 * @property string|null $name
 * @property string|null $description
 * @property string|null $assignment
 * @property boolean $auto_fit
 * @property boolean $rubric_broadcast
 * @property string $status
 * @property string $simulation_code
 * @property string|null $creation_date
 * @property string|null $modif_date
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ReportPart[] $reportParts
 * @property-read int|null $report_parts_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Report[] $reports
 * @property-read int|null $reports_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Resource[] $resources
 * @property-read int|null $resources_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Teacher[] $teachers
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\TeacheTeam[] $teacherTeams
 * @property-read int|null $teachers_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\TeamConfig[] $teamConfigs
 * @property-read int|null $team_configs_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mission newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mission newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mission query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mission whereAssignment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mission whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mission whereCreationDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mission whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mission whereIdMission($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mission whereModifDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mission whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Mission whereStatus($value)
 * @mixin \Eloquent
 * @property-read int|null $teacher_teams_count
 */
class Mission extends Model
{
    //
    protected $teacher_types = ['teacher','designer'];
    protected $table = 'mission';
    protected $primaryKey = 'id_mission';
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'modif_date';
    protected $fillable = ['name', 'code', 'auto_fit', 'rubric_broadcast', 'simulation_code', self::CREATED_AT, self::UPDATED_AT];

    public $timestamps = false;

    /**
     * Returns the mission's reports
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function reports()
    {
        return $this->hasMany('App\Report', 'id_mission');
    }

    /**
     * Returns the reportParts of the mission
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function reportParts()
    {
        return $this->hasMany('App\ReportPart', 'id_mission');
    }

    /**
     * Returns the resources of the mission
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function resources()
    {
        return $this->hasMany('App\Resource', 'id_mission');
    }

    /**
     * Returns all the labdoc at the right format for displaying them
     * 
     * @return array
     */
    public function fetchAllLabdocs()
    {
        $labdocs = [];
        foreach ($this->reportParts as $rp) {
            foreach ($rp->labdocs()->whereNull('id_report')->get() as $ld) {
                $labdocs[$ld->id_labdoc] = $ld->getDisplayData();
            }
        }
        return $labdocs;
    }


    /**
     *
     * Returns the possible values for status field
     * @return array status => name
     */
    public static function getPossibleStatus()
    {
        return [
            'new' => 'new',
            'on' => 'current',
            'wait' => 'delivered',
            'arc' => 'archived',
        ];
    }

    /**
     * Returns the teacher of the mission
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function teachers()
    {
        return $this->belongsToMany('App\Teacher', 'link_mission_teacher', 'id_mission', 'id_teacher');
    }

    /**
     * Returns the teacher team of the mission
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function teacherTeams()
    {
        return $this->belongsToMany('App\TeacherTeam', 'link_mission_tteam', 'id_mission', 'id_teacher_team');
    }

    /**
     * Returns the designers of the mission
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function designers()
    {
        return $this->teachers()->where('teacher_type', 'designer');
    }

    /**
     * Returns the mission's team_configs
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function teamConfigs()
    {
        return $this->hasMany('App\TeamConfig', 'id_mission');
    }

    /**
     * Is there a report in this mission for the given user
     * @param \App\User $user
     * @return boolean
     */
    public function hasReportForUser($user)
    {
        return DB::table('link_report_learner')
            ->join('report', 'report.id_report', '=', 'link_report_learner.id_report')
            ->join('mission', 'mission.id_mission', '=', 'report.id_mission')
            ->where('id_user', $user->id_user)
            ->where('mission.id_mission', $this->id_mission)
            ->exists();
    }

    /**
     * Returns the actual number of designers ignoring admins
     * @return int
     */
    public function countActualDesigners()
    {
        return $this->designers()
                    ->whereNotIn('teacher.id_teacher', \App\Teacher::getAdminIds())
                    ->count();
    }

    /**
     * Returns the translated status name
     * @param string $status
     * @return string
     */
    public static function statusName($status)
    {
        switch ($status) {
            case 'archive':
                return  __("archivée");
            case 'public':
                return  __("publique");
            case 'tutorial':
                return __('tutoriel');
            default:
                return __("privée");
        }
    }

    /**
     * Gets the mission Rubrics
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rubrics()
    {
        return $this->hasMany('App\Scoring\Rubric', 'id_activity', 'id_mission');
    }

    /**
     * Removes the object
     * @overide
     */
    public function delete()
    {
        $backup = new \App\Processes\BackupMission($this->id_mission);
        $backup->backup(true);
        $missionId = $this->id_mission;
        if (!parent::delete()) {
            Log::info("Deletetin mission ".$this->id_mission." failed");
            return false;
        }
        Trace::logAction(Trace::TEACHER_DELETE_MISSION, ['id_mission' => $missionId]);
        $this->hideDirectory();
        return true;
    }

    public function deleteWithNoBackup(){
        parent::delete();
    }

    protected function hideDirectory()
    {
        $dir_mission = storage_path('app/public') . '/missions/' . (int) $this->id_mission;
        if (is_dir($dir_mission)) {
            rename($dir_mission, "$dir_mission.deleted");
        }
    }

    /**
     * Toggles the "archive" status of a mission.
     */
    public function archive()
    {
        $oldStatus = $this->status;
        $this->status = ($oldStatus === 'archive' ? 'private' : 'archive');
        return $this->save();
    }

    /**
     * Returns the role of the teacher in the mission
     * @param int $id_teacher
     * @param int $id_mission
     * @return string|null
     */
    public static function getTeacherRole($id_teacher, $id_mission)
    {
        return DB::table('link_mission_teacher')
            ->where('id_teacher', $id_teacher)
            ->where('id_mission', $id_mission)
            ->pluck('teacher_type')
            ->first();
    }

    /**
     * Manages link teacher
     * @param int $id_teacher the affected teacher
     * @param string role the new role for insert / update
     * @param string action insert, update or delete
     * @return boolean
     */
    public function linkTeacher($id_teacher, $role='teacher', $action='insert')
    {
        if ($action != 'delete' && !in_array($role, $this->teacher_types)) {
            return false;
        }
        if ($action == 'insert') {
            return DB::table('link_mission_teacher')->insert([
            'id_teacher' => $id_teacher,
            'id_mission' => $this->id_mission,
            'teacher_type' => $role,
            ]);
        } else {
            $query = DB::table('link_mission_teacher')
            ->where('id_teacher', $id_teacher)
            ->where('id_mission', $this->id_mission);
            if ($action == 'delete') {
                return $query->delete();
            } else {
                return $query->update(['teacher_type' => $role]);
            }
        }

    }

    /**
     * Manages link teacherTeam
     * @param int $id_teacher_team the affected teacher team
     * @return boolean
     */
    public function linkTeacherTeam($id_teacher_team)
    {
        // TODO How to do that in laravel ? firstOrNew ?
        DB::statement("INSERT IGNORE INTO `link_mission_tteam` (`id_mission`, `id_teacher_team`) VALUES (?, ?)", [$this->id_mission, $id_teacher_team]);
    
        $teachers = \App\TeacherTeam::find($id_teacher_team)->teachers()->get();
        foreach ($teachers as $teacher) {
            DB::statement("INSERT IGNORE INTO `link_mission_teacher` (`id_mission`, `id_teacher`, `teacher_type`) VALUES (?, ?, 'teacher')", [$this->id_mission, $teacher->id_teacher]);
        }  
    }

    /**
     * Returns an array of mission data.
     *
     * @param integer $id_teacher
     * @return array
     */
    public static function getPublic($id_teacher)
    {
        $sql = <<<EOL
SELECT m.id_mission, m.code, m.name, m.description
    , m.creation_date, DATE_FORMAT(m.creation_date, '%d/%m/%y') AS creation_date_fr
    , m.modif_date, DATE_FORMAT(m.modif_date, '%d/%m/%y') AS modif_date_fr
    , count(r.id_report) AS nbreports
FROM mission m
    LEFT JOIN link_mission_teacher lmt ON m.id_mission = lmt.id_mission AND lmt.id_teacher =  :id_teacher
    LEFT JOIN report r ON r.id_mission = m.id_mission AND r.status NOT IN ('new', 'test')
WHERE m.status IN ('public', 'tutorial') AND lmt.id_mission IS NULL
GROUP BY m.id_mission
ORDER BY m.code
EOL;
        return DB::select(DB::raw($sql), ['id_teacher' => $id_teacher]);
    }


    /**
     * Returns the list of mission visible in a filter for a given user
     * @param \App\User $user
     * @param int|null $id_classe
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function getMissionsForFilters($user, $id_classe, $search_term, $page, $pageSize)
    {
        $missions = \App\Mission::select(['mission.id_mission as id', 'mission.id_mission', 'mission.code', 'mission.code as text', 'mission.status'])
            ->when($search_term, function ($query) use ($search_term) {
                return $query->where(DB::raw('LOWER(mission.code)'), 'like', '%' . strtolower($search_term) . '%');
            });
        if (!$user->isAdmin()) {
            $missions = $missions->join('link_mission_teacher', 'mission.id_mission', 'link_mission_teacher.id_mission')
                ->where('link_mission_teacher.id_teacher', $user->id_user);
        }

        if (!$id_classe) {
            $missions = $missions->join('report', 'mission.id_mission', 'report.id_mission')
                ->whereNotIn('report.status', ['test', 'tutorial'])
                ->whereNull('report.delete_time');
        } else {
            $missions = $missions->join('team_config', 'mission.id_mission', 'team_config.id_mission')
                ->where('team_config.id_class', $id_classe);
        }
        return $missions->groupBy('mission.id_mission')
            ->orderBy('mission.code')
            ->paginate($pageSize, ['*'], 'mission', $page);
    }

    /**
     * Returns an array of mission data.
     *
     * @param \App\User $user
     * @return array
     */
    public static function getTeacherMissions($user) {
        if ($user->isAdmin()) {
            $criteria = "lmt.teacher_type = :type";
            $params = [':type' => 'designer'];
        } else {
            $criteria = "lmt.id_teacher = :id_teacher";
            $params = [':id_teacher' => $user->id_user];
        }

        $designer = __("Concepteur");
        $tutor = __("Tuteur");
        $sql = <<<EOL
SELECT m.id_mission, m.code, m.name, m.description
    , m.status
    , lmt.teacher_type
    , IF(lmt.teacher_type = 'designer','$designer', '$tutor') AS teachertype
    , m.creation_date, DATE_FORMAT(m.creation_date, '%d/%m/%y') AS creation_date_fr
    , m.modif_date, DATE_FORMAT(m.modif_date, '%d/%m/%y') AS modif_date_fr
    , count(DISTINCT r.id_report) AS nbreports
FROM mission m
    JOIN link_mission_teacher lmt ON m.id_mission = lmt.id_mission AND $criteria
    LEFT JOIN report r ON r.id_mission = m.id_mission AND r.status NOT IN ('new', 'test', 'solution') AND r.delete_time IS NULL
GROUP BY m.id_mission
ORDER BY m.code
EOL;
        $missions = self::hydrate(DB::select(DB::raw($sql), $params));
        foreach ($missions as $m) {
            $m->id_mission = (int) $m->id_mission;
            $m->nbreports = (int) $m->nbreports;
            $m->teachers = $m->getListTeachers($user->id_user);
            $m->statusHtml = htmlspecialchars(self::statusName($m->status));
        }
        return $missions;
    }

    /**
     * Returns a text representing a list of teachers with their roles associated with a mission.
     *
     * @param int $id_teacher
     * @return string
     */
    public function getListTeachers($id_teacher)
    {
        $get_mission_sql = "SELECT lmt.teacher_type, u.user_name, u.first_name
            FROM link_mission_teacher lmt
            JOIN user u ON lmt.id_teacher = u.id_user
            WHERE lmt.id_mission = :id_mission
            AND lmt.id_teacher NOT IN (".implode(",", \App\Teacher::getAdminIds()).", :id_teacher)
            ORDER BY lmt.teacher_type, u.user_name";
        $teachers = DB::select(DB::raw($get_mission_sql), [':id_mission' => $this->id_mission, ':id_teacher' => $id_teacher]);

        // création de la liste HTML avec les enseignants et leur rôle
        $result = [];
        foreach ($teachers as $teacher) {
            $result[] = mb_strtoupper(substr($teacher->first_name, 0, 1))
                . ". " . $teacher->user_name
                . " (" . ($teacher->teacher_type === "designer" ? __("concepteur") : __("tuteur")) . ")";
        }
        return join(", ", $result);
    }

    /**
     * Renvoie la liste des enseignants associes sur la mission passee en parametre, avec leur statut
     * @param int(10) : id de la mission
     * @return query result

     */
    public function getLinkedTeachers()
    {
        $sql = "SELECT DISTINCT teacher.id_teacher, first_name, user_name, teacher_type
            FROM user, teacher, link_mission_teacher
            WHERE link_mission_teacher.id_mission = :id_mission and
            link_mission_teacher.id_teacher = teacher.id_teacher and
            user.id_user = teacher.id_teacher
            ORDER BY user_name";
        return \App\Teacher::Hydrate(DB::select(
            DB::raw($sql),
            [':id_mission' => (int)$this->id_mission]
        ));
    }
    
    /**
     * Return the list of users related to all mission reports
     * @return collect $user_list
     */
    public function getLinkedLearners()
    {
        $user_list = collect();
        foreach ($this->reports as $report) {
            $user_list = $user_list->merge($report->users);
        }
        return $user_list;
    }

    /**
     * Duplique une mission (infor, ressources, partie .. labdoc) et affiche l'ID de la mission créé
     *
     * @param int $id_teacher
     * @return \App\Mission
     */
    public function duplicate($id_teacher)
    {
        $code = $this->code;
        $new = $this->replicate();
        $new->modif_date = $new->creation_date = date("Y-m-d");
        $new->code = null;
        $new->status = 'private';
        $new->save();
        $new->code = substr($this->code, 0, 10) . "_".$new->id_mission;
        $new->save();
        $designers =\App\Teacher::getAdminIds();
        if (!in_array($id_teacher, $designers)) {
            // Make sure that id_teacher is not twice in $designers array
            $designers[] = $id_teacher;
        }
        foreach ($designers as $id) {
            $new->linkTeacher($id, 'designer');
        }

        $rootDir = storage_path('app/public');
        $id_mission_new = $new->id_mission;
        $id_mission_old = $this->id_mission;

        // copie des ressources
        foreach ($this->resources as $r) {
            $new_r = $r->replicate();
            $new_r->id_mission = $new->id_mission;
            // creation du dossier de la mission pour les ressources
            if (!is_dir("$rootDir/missions/$id_mission_new/resources")) {
                @mkdir("$rootDir/missions/$id_mission_new/resources", 0777, true);
            }
            $escaped_path = str_replace('%', '%%', $r->res_path);
            $pathPattern = "$rootDir/missions/%d/resources/$escaped_path";
            if (($r->res_type === "file" || $r->res_type === "assignment") && is_file(sprintf($pathPattern, $id_mission_old))) {
                // cas d un fichier local
                @copy(sprintf($pathPattern, $id_mission_old), sprintf($pathPattern, $id_mission_new));
            }
            $new_r->save();
        }


        // copie des parties de rapport
        foreach ($this->reportParts as $rp) {
            $new_rp = $rp->replicate();
            $new_rp->id_mission = $new->id_mission;
            $new_rp->save();
            $id_report_part_new = $new_rp->id_report_part;
            foreach ($rp->labdocs()->whereNull('id_report')->whereNull('id_team_config')->get() as $ld) {
                // copie des labdocs
                $new_ld = $ld->replicate();
                $new_ld->id_report_part = $new_rp->id_report_part;
                $new_ld->save();

                // vérifier si il y a des images inclues dans les LD drawings - si oui, copier
                if ($new_ld->type_labdoc === "drawing") {
                    $drawing = $new_ld->decodeZwibblerData($new_ld->labdoc_data);
                    if ($drawing && is_array($drawing)) {
                        foreach ($drawing as $value) {
                            if ($value->type === "ImageNode") { // il y a une image
                                $path = preg_replace('/.*fileid=([^&]*)[&$].*/', '\1', $value->url);
                                if (!is_dir("$rootDir/missions/$id_mission_new/zw_img")) {
                                    @mkdir("$rootDir/missions/$id_mission_new/zw_img", 0777, true);
                                }
                                @copy("$rootDir/missions/$id_mission_old/zw_img/$path", "$rootDir/missions/$id_mission_new/zw_img/$path");
                                $value->url = str_replace('id_mission='.$id_mission_old, 'id_mission='.$id_mission_new, $value->url);
                            }
                        }
                        $new_ld->labdoc_data = $new_ld->encodeZwibblerData($drawing);
                        $new_ld->save();
                    }
                }
            }
        }

        foreach ($this->rubrics as $rubric) {
            $rubric->duplicateForActivity($new->id_mission);
        }

        Trace::logAction(Trace::TEACHER_DUPLICATE_MISSION, ['id_mission' => $id_mission_new, 'from_mission' => $id_mission_old]);
        return $new;
    }

    /**
     * Get or create a correct report for the mission
     * @param $user
     * @return Report|Model
     */
    public function getSolutionReport() {
        $solution_report = \App\Report::firstOrCreate([
            'id_mission' => $this->id_mission,
            'status' => 'solution',
        ]);
        $solution_report->team_name = __("Rapport enseignants - concepteur(s)");
        $solution_report->save();
        $solution_report->users()->sync($this->teachers->pluck('id_teacher')->all());
        return $solution_report;
    }
    /**
    * Met a jour le champ position de la table cible
    * @param string : [ressource|report_part|labdoc] Table cible
    * @param int(10) : id de l item a modifier
    * @param int(10) : position de l item
    * @return boolean
    */
    public function setItemPos($table, $pos, $id) {
		if (!in_array($table, ['ressource', 'report_part', 'labdoc'])) {
            return false;
        }
        if ($table !== 'labdoc') {
            return DB::table($table)->where("id_".$table, $id)->update(["position"=> $pos]);
        }
        return $this->updateLDPos($id, 'position', $pos);
    }  
    
    /**
     * Marks the mission as modified
     */
    public function traceUpdate() {
        $this->modif_date = date("Y-m-d");
        $this->save();
        Trace::logAction(Trace::TEACHER_MODIFY_MISSION, ['id_mission' => $this->id_mission]);
    }

    /*
     * Returns the position for the next resource document
     */
    public function nextRDPos()
    {
        return  (int)$this->resources()
                                  ->orderBy('position', 'DESC')
                                  ->pluck('position')
                                  ->first() + 1;
    }

    /*
     * Returns the position for the next reportPart
     */
    public function nextRPPos()
    {
        return  (int)$this->reportParts()
                                  ->orderBy('position', 'DESC')
                                  ->pluck('position')
                                  ->first() + 1;
    }

    /**
     * Updates the position of all resources after removing one resource
     * @param int position position of the removed resource
     */
    public function updateResourcesPosition($position)
    {
        // Récupérations de toutes les ressources de cette mission pour mettre à jour le positionnement
        //$ressources = Legacy\DB::Query('SELECT * FROM ressource WHERE res_type <> "assignment" AND id_report IS NULL AND id_mission = :id_mission ORDER BY position ASC', [':id_mission' => $id_mission])->fetchAll(PDO::FETCH_OBJ);
        $resources = $this->resources()
                          ->where('res_type', '<>', 'assignment')
                          ->orderBy('position', 'ASC')
                          ->get();
        foreach ($resources as $r) {
            $current_position = (int)$r->position;
            if ($current_position > $position) {
                $r->position--;
                $r->save();
            }
        }
    }

   /**
     * Removes the object
     * @param int $id_report_part
     */
    public function deleteRP($id_report_part)
    {
        foreach(['report_part', 'labdoc'] as $table) {
            DB::table($table)->where('id_report_part', $id_report_part)->delete();
        }
        DB::statement("SET @rank :=0");
        DB::statement("UPDATE report_part SET position = (@rank := @rank+1) WHERE id_mission = ? ORDER BY position ASC", [$this->id_mission]);
    }

    /**
     * Adds a LD to the mission
     * @param int $id_report_part
     * @param string $type_labdoc
     * @param string $name
     * @param int $position
     * @param int $id_user
     * @return \App\Labdoc
     */
    public function addLD($id_report_part, $type_labdoc, $name, $position, $id_user)
    {
        if (empty($position) || $position === "none") {
            $position = null;
        } else {
            $position = \App\Labdoc::maxPos($id_report_part, 0) + 1;
        }
        // initialisation du contenu du Labdoc en fonction du type
        $labdoc_data = "";
        if ($type_labdoc === 'dataset') {
            $labdoc_data = file_get_contents(public_path('/tool_fitex/default-dataset.xml'));
        } else if ($type_labdoc === 'procedure') {
            $labdoc_data = file_get_contents(public_path('/tool_copex/v2_json/default-protocol.json'));
        } else if ($type_labdoc === 'code') {
            $labdoc_data = file_get_contents(public_path('/jupyter/rw/files/new.ipynb'));
        }

        return \App\Labdoc::create([
            'id_report_part' => $id_report_part,
            'type_labdoc' => $type_labdoc,
            'name' => $name,
            'position' => $position,
            'locked' => time(),
            'last_editor' => $id_user,
            'last_edition' => time(),
            'labdoc_data' => $labdoc_data,
            'duplicatable' => 1,
            'deleteable' => 1,
            'editable' => 1,
            'editable_name' => 1,
            'draggable' => 1,
            'shared' => 0,
        ]);
    }


    /**
     * Gets unmodified children labdocs
     * @param \App\Labdoc $ld
     * @param $modified string 'unmodified'|'modified'|'all'
     * @return \All\Labdoc[]|\Illuminate\Database\Eloquent\Collection
     */
    private function _getChildrenLabdocs($ld, $modified)
    {
        Log::Debug($ld);
        $rp_query = Labdoc::join('report', 'report.id_report', 'labdoc.id_report')
            ->where('shared', 0)
            ->whereNull('report.delete_time')
            ->where('id_ld_origin', $ld->id_labdoc)
            ->whereNotNull('labdoc.id_report_part')
            ->whereIn('report.status', [Report::ON, Report::WAIT, Report::SOLUTION])
            ->select('labdoc.*');

        $shared_query = Labdoc::join('team_config', 'team_config.id_team_config', 'labdoc.id_team_config')
            ->join('class', 'class.id_class', 'team_config.id_class')
            ->where('shared', 1)
            ->where('id_ld_origin', $ld->id_labdoc)
            ->whereNotNull('labdoc.id_report_part')
            ->where('class.status', Classe::STATUS_NORMAL)
            ->select('labdoc.*');
        if ($modified === 'unmodified') {
            $rp_query = $rp_query->whereNull('labdoc.locked')
                                 ->whereNull('labdoc.last_edition');
            $shared_query = $shared_query->whereNull('labdoc.locked')
                                 ->whereNull('labdoc.last_edition');
        } elseif ($modified === 'modified') {
            $rp_query = $rp_query->whereNotNull('labdoc.last_edition');
            $shared_query = $shared_query->whereNotNull('labdoc.last_edition');
        }
        $rp_labdocs = $rp_query->get();

        Log::Debug($rp_labdocs);

        $shared_labdocs = $shared_query->get();
        Log::Debug($shared_labdocs);
        Log::Debug($rp_labdocs->concat($shared_labdocs));

        return $rp_labdocs->concat($shared_labdocs);
    }

    private function _getNumReportsWithLabdoc($ld)
    {
        if ($ld->shared) {
            // Count all the actual TC reports as updated
            return \App\TeamConfig::find($ld->id_team_config)
                ->reports()
                ->whereIn('report.status', [Report::ON, Report::WAIT])
                ->count();
        }
        return 1;
    }

    /**
     * Updates a LD position or report_part and propagates changes if required
     * @param int $id_ld the ld to update
     * @param string $field position|id_report_part
     * @param int $value the new position or id_report_part
     * @return int|false number of changes including the requested labdoc, false in case of error
     */
    public function updateLDPos($id_ld, $field, $value)
    {
        $num_changes = 0;
        $ld = Labdoc::find($id_ld);
        if (!in_array($field, ['position', 'id_report_part']) || !$ld) {
            // Bad request
            return false;
        }
        $data = [$field => $value];
        $num_changes += $ld->updateNoTimestamps($data);
        if (!$num_changes || (!$ld->shared && $field === 'position')) {
            // Nothing to propagate
            return $num_changes;
        }
        $children = $this->_getChildrenLabdocs($ld, 'all');
        foreach ($children as $child) {
            if (!$child->shared) {
                if ($child->id_report_part == $value) {
                    continue;
                }
                // For not shared labdoc we can only update id_report_part,
                // but we need to add them at the end of the new RP
                $data['position'] = Labdoc::maxPos($value, $child->id_report) + 1;
            }
            $num_changes += $child->updateNoTimestamps($data);
        }
        return $num_changes;
    }

    /**
     * Updates a given labdoc with the data
     * @param array $data
     * @param \App\Labdoc $ld
     * @param int $id_user
     * @return int how many labdocs where updated
     */
    public function updateLD($data, $ld, $id_user)
    {
        $updated_ld = 0;
        $data['last_edition'] = time();
        $ld->fill($data);
        if ($ld->updateinDB($id_user, $this->id_mission, $ld->labdoc_data)['internal'] === 'saved') {
            $this->traceUpdate();
            unset($data['last_edition']);
            unset($data['last_editor']);
            unset($data['shared']); // Cannot change shared status of child LD
            // Edit the reports LD related if the report is on or wait and if the LD has never been modified
            $unmodified = $this->_getChildrenLabdocs($ld, 'unmodified');
            foreach ($unmodified as $child_ld) {
                if ($child_ld->updateNoTimestamps($data)) {
                    $updated_ld += $this->_getNumReportsWithLabdoc($child_ld);
                    // update the status of the LD
                    $child_ld->updateStatus(['modified' => 1, 'mod_icon' =>1], 0, false);
                }
            }

            // Update only perms for modified labdocs
            unset($data['name']);
            unset($data['labdoc_data']);
            $modified = $this->_getChildrenLabdocs($ld, 'modified');
            foreach ($modified as $child_ld) {
                $child_ld->updateNoTimestamps($data);
            }
        }
        // Update assignment on all child labdocs
        Labdoc::where('id_ld_origin', $ld->id_labdoc)->update(['assignment' => $ld->assignment]);
        return $updated_ld;
    }

    /**
     * Deletes a labdoc from the mission
     * @param \App\labdoc $labdoc
     * @param int $id_user
     * @return int number of student labdoc removed
     */
    public function deleteLD($labdoc, $id_user)
    {
        $updated_ld = 0;
        // suppress related students'LDs from on and wait reports if not already modified
        // to do before suppressing the mission's default LD because the reference is lost by cascading foreign key

        foreach ($this->_getChildrenLabdocs($labdoc, 'unmodified') as $ld1) {
            // Remove as id_user 0 so that all the update status affects all users
            if ($ld1->remove(0)) {
                if($ld1->report && $ld1->report->status !== 'solution') {
                    // we only count student's report
                    $updated_ld += $this->_getNumReportsWithLabdoc($ld1);
                }
            }
        }
        // suppress the default LD
        if ($labdoc->remove($id_user)) {
            $this->traceUpdate();
        }
        return $updated_ld;
    }
}
