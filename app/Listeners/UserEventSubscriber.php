<?php

namespace App\Listeners;

use Illuminate\Support\Facades\Session;
use App\Trace;

/**
 * This is a dummy event subscriber thats sets and remove the id_user session variable for retrocompatibility
 */
class UserEventSubscriber
{
    /**
     * Handle user login events.
     */
    public function handleUserLogin($event)
    {
        Trace::logAction(Trace::CONNECT_LNB);
    }

    /**
     * Handle user logout events.
     */
    public function handleUserLogout($event)
    {
        Trace::logAction(Trace::DISCONNECT_LNB);
        Session::flush();
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'Illuminate\Auth\Events\Login',
            'App\Listeners\UserEventSubscriber@handleUserLogin'
        );

        $events->listen(
            'Illuminate\Auth\Events\Logout',
            'App\Listeners\UserEventSubscriber@handleUserLogout'
        );
    }
}
