<?php

namespace App\Listeners;

use Illuminate\Mail\Events\MessageSending;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use \Carbon\Carbon;

class PreventDuplicateEmail
{
    protected $prevent_minutes = 60;

    public function handle(MessageSending $event)
    {
        try {
            $message = $event->message;
            $hash = \sha1($message->getHtmlBody());
            $subject = $message->getSubject();

            $toRecipients = $message->getTo();
            DB::table('maillog')
                ->where('sent_at', '<', Carbon::now()->subMinutes($this->prevent_minutes))
                ->delete();
            foreach ($toRecipients as $addr) {
                $email = $addr->getAddress();
                $alreadySent = DB::table('maillog')
                    ->where('subject', $subject)
                    ->where('to', $email)
                    ->where('hash', $hash)
                    ->exists();
                if ($alreadySent) {
                    // Prevent sending
                    Log::info("Preventing to send duplicate mail '{$subject}' to '{$email}'");
                    return false;
                } else {
                    DB::table('maillog')->insert([
                        [
                        'subject' => $subject,
                        'to' => $email,
                        'hash' => $hash,
                        'sent_at' => date('Y-m-d H:i:s'),
                        ],
                    ]);
                }
            }
        } catch (\Exception $e) {
            Log::error($e);
            return false;
        }
    }
}
