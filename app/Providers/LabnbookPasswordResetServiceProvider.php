<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Providers\LabnbookPasswordBrokerManager;

class LabnbookPasswordResetServiceProvider extends ServiceProvider
{
    protected $defer = true;

    public function register()
    {
        $this->registerPasswordBrokerManager();
    }

    protected function registerPasswordBrokerManager()
    {
        $this->app->singleton('auth.password', function ($app) {
            return new LabnbookPasswordBrokerManager($app);
        });
    }

    public function provides()
    {
        return ['auth.password'];
    }
}
