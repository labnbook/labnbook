<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Helper;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::share('helper', new Helper());
        DB::listen(function ($query) {
            if (config('labnbook.log_sql')) {
                Log::Debug("Query : '$query->sql' with bindings : '".
                    json_encode($query->bindings).
                    "' ran in $query->time ms");
            }
        });
    //        $request = app(\Illuminate\Http\Request::class);
    //        Log::info('access.log', array('request' => $request->headers));
    }
}
