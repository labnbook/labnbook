<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use \App\Trace;

/**
 * App\Classe
 *
 * @property int $id_class
 * @property int|null $id_inst
 * @property int|null $id_extplatform
 * @property string|null $class_name
 * @property string|null $class_code
 * @property string $creation_time
 * @property string $update_time
 * @property string $status
 * @property string|null $ext_code Class identifier in external institution
 * @property-read \App\Institution|null $institution
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Teacher[] $teachers
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\TeacheTeam[] $teacherTeams
 * @property-read int|null $teachers_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\TeamConfig[] $teamConfigs
 * @property-read int|null $team_configs_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @property-read int|null $users_count
 * @property-read \App\Extplatform|null $extplatform
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Classe whereIdExtplatform($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Classe newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Classe newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Classe query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Classe whereClassCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Classe whereClassName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Classe whereCreationTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Classe whereExtCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Classe whereIdClass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Classe whereIdInst($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Classe whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Classe whereUpdateTime($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Mission[] $missions
 * @property-read int|null $missions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Report[] $reports
 * @property-read int|null $reports_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Classe withLearners()
 * @property int|null $num_ext_participants
 * @property-read int|null $teacher_teams_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Classe whereNumExtParticipants($value)
 */
class Classe extends Model
{
    protected $table = 'class';
    protected $primaryKey = 'id_class';
    const CREATED_AT = 'creation_time';
    const UPDATED_AT = 'update_time';
    const STATUS_NORMAL = 'normal';
    const STATUS_ARCHIVE = 'archive';

    public $fillable = ['class_name', 'class_code', 'id_inst'];

    /**
     * Returns the users of the classe
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\User', 'link_class_learner', 'id_class', 'id_user');
    }

    /**
     * Returns the teachers of the classe
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function teachers()
    {
        return $this->belongsToMany('App\Teacher', 'link_class_teacher', 'id_class', 'id_teacher');
    }

    public function teacherTeams()
    {
        return $this->belongsToMany('App\TeacherTeam', 'link_class_tteam', 'id_class', 'id_teacher_team');
    }

    /**
     * Returns the reports of the classe
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function reports()
    {
        return $this->hasManyThrough('App\Report', 'App\TeamConfig', 'id_class', 'id_team_config');
    }

    /**
     * Returns the missions of the classe
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function missions()
    {
        return $this->hasManyThrough('App\Mission', 'App\TeamConfig', 'id_class', 'id_mission', 'id_class', 'id_mission');
    }

    /**
     * Returns the classes's team_configs
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function teamConfigs()
    {
        return $this->hasMany('App\TeamConfig', 'id_class');
    }

    /**
     * Returns the institution of the classe
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function institution()
    {
        return $this->belongsTo('App\Institution', 'id_inst');
    }

    /**
     * Returns the extplatform of the classe
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function extplatform()
    {
        return $this->belongsTo('App\Extplatform', 'id_extplatform');
    }

    /**
     * Returns true if the classe if from an external plaform
     * @return boolean
     */
    public function isExternal()
    {
        return $this->extplatform != null;
    }

    /**
     * Get classes with learners
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param bool $admin=true
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithLearners($query)
    {
        return $query->join('link_class_learner', 'link_class_learner.id_class', 'class.id_class');
    }


    /**
     * Returns the user of a class from its id
     * @param int $id_class
     * @return Illuminate\Database\Query\Builder
     */
    public static function learnersFromClassId($id_class)
    {
        return \App\User::WhereHas('classes', function ($query) use ($id_class) {
            $query->where('class.id_class', $id_class);
        });
    }

    /**
     * Generates the external code for the given course, and group
     * @param int $course external course identifier
     * @param int|null $group external course identifier
     */
    private static function getExternalCode(int $course, ?int $group): string
    {
        return $group ? "{$course}_{$group}" : "{$course}_";
    }

    /**
     * Fetches a class from external identifiers
     * @param int $course external course identifier
     * @param int $id_extplatform external platform identifier
     * @param int|null $group external course identifier
     * @return \App\Classe|null
     */
    public static function fromExternal(int $course, int $id_extplatform, ?int $group): ?self
    {
        return self::where('ext_code', self::getExternalCode($course, $group))
            ->where('id_extplatform', $id_extplatform)
            ->first();
    }

    /**
     * Fetches all classes that belongs to a course
     * @param int $course external course identifier
     * @param int $id_extplatform external platform identifier
     * @return Illuminate\Database\Eloquent\Collection
     */
    public static function fromExternalCourse($course, $id_extplatform)
    {
        return self::where('ext_code', 'like', self::getExternalCode($course, null)."%")
            ->where('id_extplatform', $id_extplatform)
            ->get();
    }

    /**
     * Tries to fetch a class from external identifiers and create a new one if none found
     * @param int $course external course identifier
     * @param int|null $group external course identifier
     * @param int $id_extplatform identifier of the external platform
     * @param string $name classe name
     * @param array participants
     * @param \App\User $user
     * @return \App\Classe
     */
    public static function fetchOrCreateFromExternal(int $course, int $id_extplatform, ?int $group, string $name, array $participants, $user): self
    {
        Log::Debug("Searching class for $id_extplatform _ $course _ $group");
        $classe = self::fromExternal($course, $id_extplatform, $group);
        if ($classe == null) {
            $extplatform = \App\Extplatform::find($id_extplatform);
            Log::Debug("Creating new class for $course _ $group");
            $classe = new Classe();
            $classe->id_inst = $extplatform->id_inst;
            $classe->id_extplatform = $id_extplatform;
            $classe->class_name = $name;
            $classe->ext_code = self::getExternalCode($course, $group);
            $instname = $extplatform->name;
            $classe->save();
            $classe->linkTeacher($user);
        }
        $classe->updateExternalParticipants($participants, $id_extplatform);
        return $classe;
    }

    /**
     * Updates the member of the class based on the given participants
     * @param array $participants
     * @param int $id_extplatform
     * @return the number of students in the class, linked to the extplatform
     */
    public function updateExternalParticipants($participants, $id_extplatform)
    {
        if (!empty($participants)) {
            $this->num_ext_participants = count($participants);
            $this->save();
            foreach (\App\User::fromExternal($participants, $id_extplatform) as $u) {
                $this->addLearner($u->id_user);
            }
        }
        return $this->users()
                    ->distinct('user.id_user')
                    ->join('link_extplatform_user as leu', 'leu.id_user', 'user.id_user')
                    ->where('leu.id_extplatform', $id_extplatform)
                    ->count();
    }

    /**
     * Add link between user and teacher
     * @param \App\User $user
     */
    public function linkTeacher($user)
    {
        if (! $this->teachers()->where('link_class_teacher.id_teacher', $user->id_user)->exists()) {
            DB::table('link_class_teacher')
                ->insert([
                    'id_teacher' => $user->id_user,
                    'id_class' => $this->id_class,
                ]);
        }
    }

    /**
     * Returns the class with given code if any
     * @param string code
     * @return \Illuminate\Database\Query\Builder
     */
    public static function fromCode($code)
    {
        return self::where('class_code', $code)
            ->where('status', self::STATUS_NORMAL);
    }

    /**
     * Adds a user to the class
     * @param int $id_user
     * @param string|null $traceMethod wht trace should we leave
     * @return boolean
     */
    public function addLearner($id_user, $traceMethod=null)
    {
        if ($this->contains($id_user, 'learner')) {
            // If we do not leave trace, we consider that user already in classe is OK
            return $traceMethod == null;
        }
        if ($traceMethod) {
            Trace::logAction(Trace::TEACHER_ADD_STUDENT, ['id_class' => $this->id_class, 'method' => $traceMethod]);
        }
        return DB::table('link_class_learner')->insertOrIgnore([
            'id_class' => $this->id_class,
            'id_user' => $id_user,
        ]);
    }

    /**
     * Check if a user is in a classe
     * @param int $id_user
     * @param string $role 'learner'|'teacher'
     * @return bool
     */
    public function contains($id_user, $role)
    {
        if ($role == 'teacher') {
            $query = $this->teachers();
            $id_name = 'teacher.id_teacher';
        } else {
            $query = $this->users();
            $id_name = 'user.id_user';
        }
        return $query->where($id_name, $id_user)->exists();
    }

    /**
     * Vérifie les entrées utilisateur pour une classe
     *
     * @param string $code
     * @param array $teachers liste des enseignants associés à cette classe
     * @param int $id_class Null or 0 on a class creation
     */
    protected static function validateClassEntries($code, $teachers, $id_class)
    {
        if ($code) {
            // le code doit être vide ou unique
            $query = self::where('class_code', $code);
            if ($id_class) {
                $query = $query->where('id_class', '<>', $id_class);
            }
            if ($query->exists()) {
                throw new \Exception(__("Le code de la classe est déjà utilisé, veuillez en choisir un nouveau")."\n");
            }
        }
        // il doit y avoir des enseignants dans la classe
        if (count($teachers) === 0) {
            throw new \Exception(__("Veuillez choisir des enseignants à associer à la classe")."\n");
        }
    }

    /**
     * Si les entrées utilisateurs sont valides alors tentera de créer une nouvelle classe avec ces entrées
     *
     * @param string $name
     * @param string $code
     * @param int $inst
     * @param array $teachers [id_user]
     * @param int $id_class
     * @return int ID of the created|updated class
     */
    public static function addOrUpdate($name, $code, $inst, $teachers, $id_class, $teacherTeams)
    {
        $params = [
            'class_name' => (string) $name,
            'class_code' => $code ? (string) $code : null,
            'id_inst' => (int) $inst,
        ];

        // vérification de la validité des champs critique (Exception on error)
        self::validateClassEntries($code, $teachers, $id_class);

        // ajout de la classe dans la bdds (reset des liens class-teachers)
        if ($id_class > 0) {
            // Mise à jour
            $classe = self::find($id_class);
            $classe->fill($params);
            $classe->save();
            // TODO Pourquoi delete ne prend pas en compte l'id de teacherTeam ?
            DB::statement("DELETE FROM `link_class_teacher` WHERE id_class = ?", [$id_class]);
        } else { // ajout
            $classe = self::create($params);
            Trace::logAction(Trace::TEACHER_ADD_CLASS, ['id_class' => $classe->id_class]);
        }
        foreach ($teachers as $id_teacher) {
            if(!$classe->teachers()->get()->contains('id_teacher',$id_teacher)) {
                DB::statement("INSERT INTO `link_class_teacher` (`id_class`, `id_teacher`) VALUES (?, ?)", [$classe->id_class, $id_teacher]);
            }
        }
        foreach ($teacherTeams as $id_teacher_team) {
            DB::statement("INSERT IGNORE INTO `link_class_tteam` (`id_class`, `id_teacher_team`) VALUES (?, ?)", [$classe->id_class, $id_teacher_team]);
        }
        return $classe->id_class > 0 ? (int) $classe->id_class : null;
    }


    /**
     * Returns the list of classes visible in a filter for a given user
     * @param \App\User $user
     * @param int|null $id_mission
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function getClassesForFilters($user, $id_mission, $search_term, $page, $pageSize)
    {
        $classes = \App\Classe::select(['class.id_class', 'class.id_class as id', 'class.class_name', 'class.class_name as text', 'class.status'])
            ->when($search_term, function ($query) use ($search_term) {
                return $query->where(DB::raw('LOWER(class.class_name)'), 'like', '%' . strtolower($search_term) . '%');
            });

        if (!$user->isAdmin()) {
            $classes = $classes->join('link_class_teacher', 'class.id_class', 'link_class_teacher.id_class')
                ->join('team_config', 'class.id_class', 'team_config.id_class')
                ->where('link_class_teacher.id_teacher', $user->id_user);
        }

        if ($id_mission) {
            if ($user->isAdmin()) {
                $classes = $classes->join('team_config', 'class.id_class', 'team_config.id_class');
            }
            $classes = $classes->where('team_config.id_mission', $id_mission);
        }

        return $classes->groupBy('class.id_class')
            ->orderBy('class.status')
            ->orderBy('class.class_name')
            ->paginate($pageSize, ['*'], 'class', $page);
    }

    /**
     * fonction delete
     * Tente de supprimer une classe de manière définitive.
     * Les contraintes SQL permettent de garantir l'état cohérent de la BDD.
     * On ne supprime que si l'enseignant est associé à cette classe (seul cas où la suppression est autorisée)
     * @param int $id_class id de la classe à supprimer
     * @return boolean
     */
    public function delete()
    {
        $id_class = $this->id_class;
        parent::delete();
        // suppression de TOUS les étudiants sans classe (mais on garde les enseignants)
        DB::statement("DELETE FROM user WHERE id_user NOT IN (SELECT id_user FROM link_class_learner) AND user.id_user NOT IN (SELECT id_teacher FROM teacher)");
        Trace::logAction(Trace::TEACHER_DELETE_CLASS, ['id_class' => $id_class]);
    }

    /**
     * fonction archiveClass
     * Tente d'archiver une classe.
     * @return array
     */
    public function archive()
    {
        $this->status = self::STATUS_ARCHIVE;
        $this->save();

        $toArchive = $this->reports()->whereIn('status', [Report::WAIT, Report::ON])->pluck('id_report');
        $toDelete = $this->reports()->where('status', Report::NEW)->pluck('id_report');

        return [
            'toArchive' => $toArchive,
            'toDelete' => $toDelete
        ];
    }

    /**
     * fonction unpackClass
     * Désarchive une classe.
     * @return array
     */
    public function unpack()
    {
        $this->status = self::STATUS_NORMAL;
        return $this->save();
    }

    /**
     * @param int $idClass
     * @return array [ {id_user, first_name, user_name} ]
     */
    public function getTeachersNameAndId() {
        return $this->teachers()
                    ->join('user as u', 'u.id_user', 'teacher.id_teacher')
                    ->selectRaw('id_user, first_name, user_name')
                    ->orderBy('user_name')
                    ->get()
                    ->toArray();
    }

    /**
     * Lists the missions where there is a team_config for the given class.
     *
     * @return array Mission: {id_mission, code, name}
     */
    public function getMissionsNameCodeAndId() {
        return $this->missions()
                    ->selectRaw('mission.id_mission, mission.code, mission.name, status')
                    ->orderBy('mission.name')
                    ->get()
                    ->toArray();
    }

    /**
     * @return object { id_user (int), lastname, firstname, name }
     */
    public function getStudentsNameAndId() {
        $students = $this->users()
                         ->selectRaw("user.id_user, user.user_name as lastname, user.first_name as firstname")
                         ->orderBy('user.user_name')
                         ->orderBy('user.first_name')
                         ->get();
        foreach ($students as $u) {
            $u->id_user = (int) $u->id_user;
        }
        return $students;
    }


}
