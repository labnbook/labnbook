<?php

namespace App;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class TeamChoice
{
    private $id_user;
    private $user;
    private $lastError;

    public function __construct(\App\User $user)
    {
            Log::Debug($user);
            $this->user =  $user;
            $this->id_user = $user->id_user;
    }

    public function getLastError()
    {
        return $this->lastError;
    }

    /**
     * AJAX action. Get Info about the teams config, the current status, the teams, the selected report, the mission available for reuse.
     *
     * @param \App\Teamconfig $settings
     * @return array ['teams' => [...]]
     */
    public function actionGetFullTeams($settings, $reuse=false)
    {
        $status = $this->getStatus($settings);

        if($reuse) {
            $settings->start_datetime = null;
            $settings->end_datetime = null;
        }

        $reports = \App\Report::Where('id_team_config', $settings->id_team_config)
            ->orderBy('team_name', 'ASC', 'creation_time', 'DESC')
            ->get();
        $teams = [];
        foreach ($reports as $r) {
            $teams[(int) $r->id_report] = (object) [
                'id_report' => (int) $r->id_report,
                'name' => $r->team_name,
                'status' => $r->status,
                'members' => [],
            ];
            foreach ($r->users()->orderBy('user.user_name')->orderBy('user.first_name')->get() as $user) {
                $teams[(int) $r->id_report]->members[] = [
                    'id_user' => (int) $user->id_user,
                    'name' => $user->first_name . " " . strtoupper($user->user_name),
                    'firstname' => $user->first_name,
                    'lastname' => $user->user_name,
                ];
            }
        }

        foreach ($teams as $idr => $team) {
            if ($team->members) {
                $team->extensible = self::allowJoin(count($team->members), $settings, $status->allowMoreThanOpt);
            } else {
                unset($teams[$idr]);
            }
        }

        return [
            'settings' => $settings,
            'teams' => $teams ? array_values($teams) : [],
            'status' => $status,
            'missionsToCopy' => \Auth::getUser()->teacher->missionsToCopyForAClass($settings->id_class),
            'isExternalClass' => $settings->classe->isExternal()
        ];
    }

    /**
     * AJAX action. Get Info about the teams config, the current status, the teams, the selected report.
     *
     * @param \App\TeamConfig $settings
     * @return array ['settings' => [...], 'teams' => [...], 'status' => [...], 'report' => ...]
     */
    public function actionGetTeams($settings)
    {
        $status = $this->getStatus($settings);
        $teams = $settings->getTeams();

        foreach ($teams as $t) {
            $t->extensible_message = self::allowJoin(count($t->members), $settings, $status->allowMoreThanOpt);
            $t->extensible = $t->extensible_message === "";
        }
        usort(
            $teams,
            function ($a, $b) {
                if ($a->members->count() < $b->members->count()) {
                    return 1;
                } elseif ($a->members->count() > $b->members->count()) {
                    return -1;
                } else {
                    return strcmp($a->name, $b->name);
                }
            }
        );

        return [
            'settings' => $settings,
            'teams' => $teams,
            'canAdd' => self::allowCreation($settings)
        ];
    }

    /**
     * AJAX action. Register into a team (report), either new (id_report = NULL) or existing.
     *
     * @param int $id_report
     * @param \App\TeamConfig $settings
     * @return object
     */
    public function actionSelectTeam($id_report, $settings)
    {
        $existingReport = \App\Report::fromUserAndTeamConfig($this->id_user, $settings->id_team_config);
        if ($existingReport && $existingReport->getLastVisit($this->id_user) !== null) {
            return ['status' => 'danger', 'message' => __("Vous êtes déjà inscrit dans une équipe de cette mission.")];
        }
        if ($id_report) {
            // existing team
            Log::Debug('Existing team');
            $status = $this->getStatus($settings);
            if ($this->joinReport((int) $id_report, $settings, $status)) {

                if ($existingReport) {
                    // delete after the insertion was successful, to avoid a broken state
                    $this->removeReportMember($existingReport->id_report);
                    return ['status' => 'success', 'message' => __("Vous avez été inscrit dans cette équipe et désinscrit de la précédente."), 'id_report' => $id_report];
                } else {
                    return ['status' => 'success', 'message' => __("Vous avez été inscrit dans cette équipe."), 'id_report' => $id_report];
                }
            }
        } else {
            // new team
            Log::Debug('New team');
            $id_report = $this->createReport($settings);
            if ($id_report) {
                if ($existingReport) {
                    // delete after the creation was successful, to avoid a broken state
                    $this->removeReportMember($existingReport->id_report);
                }
                return ['status' => 'success', 'message' => __("Vous avez été inscrit dans une nouvelle équipe."), 'id_report' => $id_report];
            } else {
                return ['status' => 'danger', 'message' => __("Impossible de créer une nouvelle équipe.")." $this->lastError"];
            }
        }
        return ['status' => 'danger', 'message' => __("L'inscription dans cette équipe a échoué.")." $this->lastError"];
    }

    private function removeReportMember($id_report)
    {
        $report = \App\Report::find($id_report);
        if ($report->users()->count() == 1) {
            $report->delete();
        } else {
            $report->users()->detach($this->id_user);
        }
    }

    /**
     * @param \App\TeamConfig $settings
     * @return object
     */
    private function getStatus($settings)
    {
        $usersInTeams = $settings->users()->count();
        return (object) [
            'method' => $settings->method,
            'allowCreation' => $this->allowCreation($settings),
            'allowMoreThanOpt' => $usersInTeams >= $settings->size_opt * $settings->teams_max,
            'numTeams' => \App\TeamConfig::countReports($settings->id_team_config),
            'usersInClass' => \App\Classe::learnersFromClassId($settings->id_class)->count(),
            'usersInTeams' => $usersInTeams,
        ];
    }

    /**
     * Register the learner into an existing team (AKA report).
     *
     * @param int $id_report
     * @param \App\TeamConfig $settings
     * @param object $status
     * @return boolean
     */
    private function joinReport($id_report, $settings, $status)
    {
        $this->lastError = null;
        $report = \App\Report::find($id_report);
        if (!$report) {
            $this->lastError = __("Ce rapport n'existe plus");
            return false;
        }
        $size = $report->users()->count();
        $allowed = self::allowJoin($size, $settings, $status->allowMoreThanOpt);
        if ($allowed !== "") {
            $this->lastError = $allowed;
            return false;
        }
        try {
            $report->users()->attach($this->id_user);
        } catch (\Illuminate\Database\QueryException $e) {
            if ($e->getCode() === "23000") {
                // Duplicate key
                $this->lastError = __("Déjà membre de cette équipe");
                return false;
            }
            $this->lastError = __("Erreur SQL en ajoutant un participant au rapport");
            return false;
        }
        return true;
    }

    /**
     * @param int $reportSize
     * @param \App\TeamConfig $settings
     * @param boolean $allowMoreThanOpt
     * @return string ""|"error message"
     */
    private static function allowJoin($reportSize, $settings, $allowMoreThanOpt)
    {
        if ($allowMoreThanOpt) {
            if ($settings->size_opt && $settings->teams_max
                && $reportSize >= $settings->size_max) {
                return __("L'équipe a déjà atteint sa taille maximale.");
            }
        } else if ($settings->size_opt && $reportSize >= $settings->size_opt) {
            return __("L'équipe a temporairement atteint sa taille maximale.");
        }
        return "";
    }

    /**
     * @param \App\TeamConfig $settings
     * @return boolean|string true|"error message"
     */
    private function allowCreation($settings)
    {
        if (empty($settings->size_opt)) {
            return true;
        }
        Log::Debug($settings);
        $numTeams = $settings->reports()->count();
        if ($settings->teams_max) {
            if ($numTeams >= $settings->teams_max) {
                $this->lastError = __("Le nombre d'équipes est déjà au maximum autorisé.");
                return false;
            }
        }
        $numInClass = \App\Classe::learnersFromClassId($settings->id_class)->count();
        $minTeamSize = $settings->getMinTeamSize();
        if ($numTeams >= 0.9 * ($numInClass / $settings->size_opt) && $minTeamSize < $settings->size_min) {
            $this->lastError = __("De nouvelles équipes pourront être créées une fois que les équipes existantes auront été complétées.");
            return false;
        }
        return true;
    }

    /**
     * Register the learner into a new team (report).
     *
     * @param \App\TeamConfig $settings
     * @param bool $checkCompliance Before creation, check if the team will match the settings.
     * @return int|null id_report
     */
    public function createReport($settings, $checkCompliance = true)
    {
        if ($checkCompliance) {
            $allowed = $this->allowCreation($settings);
            if ($allowed !== true) {
                $this->lastError = $allowed;
                return false;
            }
        }
        $teamName = self::buildTeamName($settings);
        $report = \App\Report::createFromTeamConfig($settings);
        $report->team_name = $teamName;
        if (!$report->save()) {
            $this->lastError = __("Erreur SQL en créant un rapport");
            return null;
        }
        try {
            $report->users()->attach($this->id_user);
        } catch (Exception $e) {
            $this->lastError = __("Erreur SQL en ajoutant un participant au nouveau rapport");
            return null;
        }
        return $report->id_report;
    }

    /**
     * @param \App\TeamConfig $settings
     * @return string
     */
    private static function buildTeamName($settings)
    {
        $prefix = $settings->name_prefix ? $settings->name_prefix : __("Equipe_");
        $prefixLike = str_replace(['_', '%'], ['\_', '\%'], $prefix) . '%';
        $max = $settings->reports()->Where('team_name', 'LIKE', $prefixLike)
                        ->OrderBy(DB::Raw('LENGTH(team_name) DESC, team_name'), 'DESC')
                        ->first();
        if ($max) {
            $m = [];
            if (preg_match("/\Q$prefix\E(\d+)$/", $max, $m)) {
                return $prefix . sprintf("%02d", 1 + $m[1]);
            } else {
                // the prefix is used in the DB, but it is completed by NaN
                $count = $settings->reports()->Where('team_name', 'LIKE', $prefixLike)->count();
                return $prefix . sprintf("%02d", 1 + $count);
            }
        } else {
            return $prefix . "01";
        }
    }
}
