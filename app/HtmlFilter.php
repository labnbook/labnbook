<?php

namespace App;

class HtmlFilter
{
    private static $purifier;

    const ALLOWED_CLASSES = [
        '',
        'base',
        'ew_ltxformula',
        'katex-html',
        'katex-mathml',
        'katex',
        'katex-display',
        'simulation',
        'key',
        'math',
        'mathdefault',
        'mfrac',
        'mopen',
        'mord',
        'mbin',
        'mclose',
        'mtight',
        'mdefault',
        'mspace',
        'nulldelimiter',
        'pstrut',
        'sizing',
        'reset-size6',
        'size3',
        'strut',
        'vlist',
        'vlist-r',
        'vlist-t',
        'vlist-t2',
        'frac-line',
        'vlist-s',
        'sqrt',
        'hide-tail'
    ];
    
    const BLANK_NODE_CODE = [
        "\n",
        " "
    ];

    public static function clean($html)
    {
        if (!isset(self::$purifier)) {
            self::$purifier = self::initPurifier();
        }
        // ugly hack for mathml
        $mathBlocks = [];
        /**
         * @todo Extract "math" blocks with DOMDocument
         */
        $position = 0;
        while (strpos($html, '<math xmlns="http://www.w3.org/1998/Math/MathML">') !== false) {
            $m = [];
            if (preg_match('#<math xmlns="http://www.w3.org/1998/Math/MathML">(.*?)</math>#s', $html, $m)) {
                $html = preg_replace('#<math xmlns="http://www.w3.org/1998/Math/MathML">.*?</math>#s', "<math>$position</math>", $html, 1);
                $mathBlocks[$position] = $m[1];
                $position++;
            } else {
                break;
            }
        }
        // Remove the blank line at the end of the text before purifying html
        $domdoc = new \DOMDocument();
        $domdoc->loadHTML('<html><head><meta charset="utf-8"></head><body>' . $html . '</body></html>', LIBXML_NOERROR | LIBXML_NOWARNING);
        $bodyNode = $domdoc->getElementsByTagName('body')->item(0); 
        for ($i = $bodyNode->childNodes->length - 1; $i >= 0; $i--) {
            $node = $bodyNode->childNodes->item($i);
            $a = $node->nodeValue;
            $isRemovable = true;
            for ($j = $node->childNodes->length - 1; $j >= 0; $j--) {
                if ($node->childNodes->item($j) instanceof \DOMElement) {
                    $isRemovable = false;
                    break;
                }
            }
            if($isRemovable && in_array($node->nodeValue, self::BLANK_NODE_CODE, true)) {
                $node->parentNode->removeChild($node);
            } else {
                break;
            }
        }
        $html= Helper::innerHTMLOfBodyNode($domdoc->saveHTML($bodyNode));
        $htmlPurified = self::$purifier->purify($html);
        if ($mathBlocks) {
            foreach ($mathBlocks as $k => $v) {
                $htmlPurified = str_replace("<math>$k</math>", '<math xmlns="http://www.w3.org/1998/Math/MathML">' . $v . '</math>', $htmlPurified);
            }
        }
        return $htmlPurified;
    }

    private static function initPurifier()
    {
        $config = \HTMLPurifier_Config::createDefault();
        $config->set('HTML.DefinitionID', 'HTML5 labNbook');
        $config->set('HTML.DefinitionRev', 3);
        $config->set('HTML.Doctype', 'HTML 4.01 Transitional'); // compatible with HTML5
        $config->set('Attr.AllowedClasses', self::ALLOWED_CLASSES);
        $config->set('Core.Encoding', 'UTF-8');
        $config->set('Core.ConvertDocumentToFragment', true);
        $config->set('Core.NormalizeNewlines', false);
        $config->set('Attr.DefaultImageAlt', '');
        $cachePath = storage_path('cache/htmlpurifier');
        if (!is_dir($cachePath)) {
            mkdir($cachePath, 0777, true);
        }
        $config->set('Cache.SerializerPath', $cachePath);

        $def = $config->maybeGetRawHTMLDefinition();
        if ($def) {
            // HTML5 tags
            $def->addElement('section', 'Block', 'Flow', 'Common');
            $def->addElement('nav', 'Block', 'Flow', 'Common');
            $def->addElement('article', 'Block', 'Flow', 'Common');
            $def->addElement('aside', 'Block', 'Flow', 'Common');
            $def->addElement('header', 'Block', 'Flow', 'Common');
            $def->addElement('footer', 'Block', 'Flow', 'Common');
            $def->addElement('main', 'Block', 'Flow', 'Common');

            $def->addElement('address', 'Block', 'Flow', 'Common');
            $def->addElement('hgroup', 'Block', 'Required: h1 | h2 | h3 | h4 | h5 | h6', 'Common');

            $def->addElement('figure', 'Block', 'Flow', 'Common');
            $def->addElement('figcaption', false, 'Flow', 'Common');
            $def->addElement('picture', 'Block', 'Flow', 'Common');

            $def->addElement('s', 'Inline', 'Inline', 'Common');
            $def->addElement('var', 'Inline', 'Inline', 'Common');
            $def->addElement('sub', 'Inline', 'Inline', 'Common');
            $def->addElement('sup', 'Inline', 'Inline', 'Common');
            $def->addElement('mark', 'Inline', 'Inline', 'Common');
            $def->addElement('wbr', 'Inline', 'Empty', 'Core');

            $def->addElement('ins', 'Block', 'Flow', 'Common', ['cite' => 'URI', 'datetime' => 'Text']);
            $def->addElement('del', 'Block', 'Flow', 'Common', ['cite' => 'URI', 'datetime' => 'Text']);

            // HTML5 media elements
            // https://html.spec.whatwg.org/#the-video-element
            $def->addElement('video', 'Block', 'Optional: #PCDATA | Flow | source | track', 'Common', [
                'src' => 'URI',
                'crossorigin' => 'Enum#anonymous,use-credentials',
                'poster' => 'URI',
                'preload' => 'Enum#auto,metadata,none',
                'autoplay' => 'Bool',
                'playsinline' => 'Bool',
                'loop' => 'Bool',
                'muted' => 'Bool',
                'controls' => 'Bool',
                'width' => 'Length',
                'height' => 'Length',
            ]);
            // https://html.spec.whatwg.org/#the-audio-element
            $def->addElement('audio', 'Block', 'Optional: #PCDATA | Flow | source | track', 'Common', [
                'src' => 'URI',
                'crossorigin' => 'Enum#anonymous,use-credentials',
                'preload' => 'Enum#auto,metadata,none',
                'autoplay' => 'Bool',
                'loop' => 'Bool',
                'muted' => 'Bool',
                'controls' => 'Bool'
            ]);
            // https://html.spec.whatwg.org/#the-source-element
            $def->addElement('source', false, 'Empty', null, [
                'src' => 'URI',
                'type' => 'Text'
            ]);
            // https://html.spec.whatwg.org/#the-track-element
            $def->addElement('track', false, 'Empty', null, [
                'src' => 'URI',
                'kind' => 'Enum#subtitles,captions,descriptions,chapters,metadata',
                'srclang' => 'Text',
                'label' => 'Text',
                'default' => 'Bool',
            ]);

            // mathml
            $def->addElement('math', 'Inline', 'Optional: #PCDATA', 'Core', ['xmlns' => 'URI']);
            $def->addElement('svg', 'Block', 'Flow', 'Common', ['width' => 'Text', 'height' => 'Text', 'viewBox' => 'Text', 'preserveAspectRatio' => 'Text']);
            $def->addElement('path', 'Inline', 'Flow', 'Common', ['d' => 'Text']);
            $def->addAttribute('span', 'data-eplw-math', 'Text');
            $def->addAttribute('span', 'data-eplw', 'Text');
            $def->addAttribute('span', 'data-katex', 'Text');
            $def->addAttribute('span', 'data-simulation', 'Text');
            $def->addAttribute('span', 'data-key', 'Text');
            $def->addAttribute('span', 'data-mce-style', 'Text');
            $def->addAttribute('span', 'aria-hidden', 'Text');
            $def->addAttribute('span', 'style', 'Text');
            $def->addAttribute('span', 'contenteditable', 'Text');

        }
        return new \HTMLPurifier($config);
    }
}
