<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class Kernel extends ConsoleKernel
{
    const CRON_LAST_RUN_FILE =  'last_schedule_run';
 
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $this->monitorCall($schedule, new \App\Processes\Cron\CloseEndedReports)->everyMinute();
        $this->monitorCall($schedule, new \App\Processes\Cron\ExpireClasseCodes)->dailyAt('6:00');
        $this->monitorCall($schedule, new \App\Processes\Cron\DeleteOldReports)->dailyAt('6:00');
        $this->monitorCall($schedule, new \App\Processes\Cron\HouseKeeping)->dailyAt('6:00');
        $this->monitorCall($schedule, new \App\Processes\Cron\DeleteOldUsers)->dailyAt('6:00');
        $this->monitorCall($schedule, new \App\Processes\Cron\ProductionTests)->dailyAt('6:00');
        Storage::write(self::CRON_LAST_RUN_FILE, now()->timestamp);
    }

    /**
     * Run $schedule->call on the given $job an returns it with our curstom parameters for 
     * handling success and failure
     * @param Illuminate\Console\Scheduling\Schedule $schedule
     * @param $job function or invokable
     * @return Illuminate\Console\Scheduling\CallbackEvent
     */
    protected function monitorCall(Schedule $schedule, $job)
    {
        return $schedule->call($job)
                        ->before(function() use ($job) {
                            Log::Info("Cron job : ".$job::class." starting");
                        })
                        ->onSuccess(function() use ($job) {
                            Log::Info("Cron job : ".$job::class." complete");
                        })
                        ->onFailure(function() use ($job){
                            \App\Helper::reportError(new \Exception("Error during job ".$job::class));
                        });
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
