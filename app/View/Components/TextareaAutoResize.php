<?php

namespace App\View\Components;

use Illuminate\View\Component;

class TextareaAutoResize extends Component
{
    
    public $xModel;
    public $xData;
    public $xInit;
    public $xOnBlur;
    public $xOnFocus;
    public $placeholder;
    
    public $customWrapperClass;
    
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($xModel, $xData='', $xInit='', $xOnBlur='', $placeholder='', $xOnFocus='', $customWrapperClass='')
    {
        $this->xModel = $xModel;
        $this->xData = $xData;
        $this->xInit = $xInit;
        $this->xOnBlur = $xOnBlur;
        $this->xOnFocus = $xOnFocus;
        $this->placeholder = $placeholder;
        $this->customWrapperClass = $customWrapperClass;
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.textarea-auto-resize');
    }
}
