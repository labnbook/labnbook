<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Widget extends Component
{
    /**
     * @var $title title of the widget
     */
    public $title;
    /**
     * @var $id id attribute of the main div
     */
    public $id;
    
    public $xShow;
    
    public $headerClass;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($id, $xShow, $title='', $headerClass=null)
    {
        $this->title = $title;
        $this->id = $id;
        $this->xShow = $xShow;
        $this->headerClass = $headerClass;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.widgets.widget');
    }
}
