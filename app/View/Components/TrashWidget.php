<?php

namespace App\View\Components;

use Illuminate\View\Component;

class TrashWidget extends Widget
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($xShow)
    {
        parent::__construct( 'trash', $xShow);
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.widgets.trash.trash-widget');
    }
}
