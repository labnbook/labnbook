<?php

namespace App\View\Components;

use Illuminate\View\Component;

class CalendarWidget extends Widget
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($xShow)
    {
        parent::__construct( 'calendar', $xShow);
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.widgets.calendar.calendar-widget');
    }

}
