<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ScoringWidget extends Widget
{
    /**
     * @var $rv Report view
     */
    public $rv;
    
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($xShow, $rv)
    {
        parent::__construct('scoring', $xShow);
        $this->rv = $rv;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.widgets.scoring.scoring-widget');
    }
}
