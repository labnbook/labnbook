<?php

namespace App\View\Components;

use Illuminate\Support\Facades\DB;
use Illuminate\View\Component;

class ResourceWidget extends Widget
{
    /** @var int $id_report */
    public $id_report;
    
    public $mission;

    /** @var boolean $add_resource */
    public $addResource;
    /** @var array $displayNewDoc */
    public $displayNewDoc;

    /** @var StdClass $doc */
    public $detailedAssignment;

    /** @var StdClass $docs */
    public $docs;
    
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($xShow, $report, $addResource, $displayNewDoc)
    {
        parent::__construct( 'ressource', $xShow);
        $this->addResource = $addResource;
        $this->displayNewDoc = $displayNewDoc;
        if ($report) {//
            $this->id_report = $report->id_report;
            $this->mission = $report->mission;
            // récupération du lien pour la consigne détaillée si elle existe
            $this->detailedAssignment = DB::table('ressource')
                ->where('id_mission', $report->id_mission)
                ->where('res_type', 'assignment')
                ->select('id_ressource', 'name', 'res_path')
                ->first();

            // récupération des documents fournis par l'enseignant
            $this->docs = DB::table('ressource')
                ->where('id_mission', $report->id_mission)
                ->where('res_type', '<>', 'assignment')
                ->orderBy('position', 'ASC')
                ->orderBy('name', 'ASC')
                ->select('id_ressource', 'name', 'res_path', 'res_type')
                ->get();
        } else {
            $this->detailedAssignment = null;
            $this->docs = null;
            $this->mission = null;
            $this->id_report = null;
        }
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.widgets.resource.resource-widget');
    }
}
