<?php

namespace App\View\Components;

use Illuminate\View\Component;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class TaskWidget extends Widget
{
    /** @var boolean $isReportTask is the component for a report or a list of reports */
    public $isReportTask;

    /** @var int $id_report id_report or -1 */
    public $id_report;

    /** @var \Illuminate\Database\Eloquent\Collection|\App\User[] $report_users */
    public $report_users;

    /** @var \App\Report|null $report */
    public $report;

    /** @var array $reportNames */
    public $reportNames;

    /** @var string $reportTitle  */
    public $reportTitle;

    /** @var array $options reports available in report index view*/
    public $options;

    /** @var array $selected  reports shown in report index view*/
    public $selected;


    private function getDisplayName($name) {
        $initials = array_map(fn ($e) => !empty($e) ? strtoupper($e[0]) : '', explode(' ', iconv('utf-8', 'ASCII//TRANSLIT//IGNORE', $name)));
        array_splice($initials, 4);
        return '<span class="small-avatar" title="' . $name .'">' .
            implode('', $initials)
            . '</span>';
    }

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($xShow, $report)
    {
        parent::__construct( 'tasks', $xShow);
        $this->report = $report;
        $this->reportTitle = $report ? $report->mission->name : '';
        $user = Auth::user();
        
        $this->isReportTask = $report != null;
        if (!$this->isReportTask) {
            $this->id_report = -1;
            $reports = auth()->user()
                             ->reports()
                             ->join('mission', 'report.id_mission', 'mission.id_mission')
                             ->join('task', 'task.id_report', 'report.id_report')
                             ->join('link_task_user', 'task.id_task', 'link_task_user.id_task')
                             ->whereIn('report.status', [\App\Report::ON, \App\Report::NEW])
                             ->where('link_task_user.id_user', $user->id_user)
                             ->select('report.id_report', 'report.start_datetime', 'report.end_datetime', 'mission.name')
                             ->distinct()
                             ->get();
            $personal_name = __('Tâches personnelles');
            $this->options = [
                [
                    'value' => -1,
                    'selectValue' => $personal_name,
                    'displayValue' => $this->getDisplayName($personal_name),
                ]
            ];
            $this->selected = [$this->options[0]];
            $this->reportNames =  [[ 'name' => __('Tâches personnelles'), 'id_report' => -1]];
            $this->report_users = null;
            foreach ($reports as $r) {
                $this->options []= [
                    'value' => $r->id_report,
                    'selectValue' => $r->name,
                    'displayValue' => $this->getDisplayName($r->name),
                ];
                $start = $r->start_datetime;
                if (!$start) {
                    $start = DB::Table('trace')->where('id_report', $r->id_report)
                                               ->select('action_time')
                                               ->orderBy('id_trace', 'ASC')
                                               ->first()
                                               ->action_time;
                }
                $this->reportNames[] = [
                    'name' => $r->name,
                    'id_report' => $r->id_report,
                    'start_datetime' => $start,
                    'end_datetime' => $r->end_datetime,
                ];
            }
            $this->report_users = [$user->only('id_user', 'first_name', 'user_name')];
        } else {
            $this->id_report = $report->id_report;
            $this->reportNames = [['name' => $this->reportTitle, 'id_report' => $report->id_report]];
            $this->report_users = $report->users()->select(['user.id_user', 'user.first_name', 'user.user_name'])->get();
        }
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.widgets.task.task-widget');
    }
}
