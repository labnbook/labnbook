<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Modal extends Component
{
    public $id;
    public $headerClass;
    public $title;
    public $xData;
    public $clickSubmitAction;
    public $clickCancelAction;
    public $includeAlert;
    
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($id=null, $headerClass='', $title=null, $xData='{}', $clickSubmitAction=null, $clickCancelAction=null, $includeAlert=false)
    {
        $this->id = $id;
        $this->headerClass = $headerClass;
        $this->title = $title;
        $this->xData = $xData;
        $this->clickSubmitAction = $clickSubmitAction;
        $this->clickCancelAction = $clickCancelAction;
        $this->includeAlert = $includeAlert;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.modal');
    }
}
