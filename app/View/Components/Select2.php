<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Select2 extends Component
{
    // string $id id for the select
    public string $id;
    
    // string $name name for the select
    public string $name;
    // string $selectClasses html classes for the select
    public string $selectClasses;
    // string $onSelectFunction
    public string $onSelectFunction;

    public string $title;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($id='', $name='', $selectClasses='', $onSelectFunction='', $title='')
    {
        //
        $this->id = $id;
        $this->name = $name;
        if(empty($this->name) && !empty($this->id)) {
            $this->name = $this->id;
        }
        $this->selectClasses = $selectClasses;
        $this->onSelectFunction = $onSelectFunction;
        $this->title = $title;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.select2');
    }
}
