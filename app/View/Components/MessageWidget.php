<?php

namespace App\View\Components;

class MessageWidget extends Widget
{
    /** @var int $id_report */
    public $id_report;
    
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($xShow, $idReport)
    {
        parent::__construct( "widget-message", $xShow);
        
        $this->id_report = $idReport;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.widgets.message.message-widget');
    }
}
