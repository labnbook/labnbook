<?php

namespace App\Exceptions;

use Exception;
use Throwable;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Illuminate\Support\Facades\Log;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Auth\AuthenticationException;

class Handler extends ExceptionHandler {

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Validation\ValidationException::class,
        \Symfony\Component\HttpKernel\Exception\NotFoundHttpException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  Throwable  $exception
     * @return void
     */
    public function report(Throwable $exception)
    {
        if ($this->shouldreport($exception)) {
            \App\Helper::reportError($exception);
        }
        parent::report($exception);
    }

    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['message' => 'Authentication required'], 401);
        }

        return redirect()->guest(route('login'));
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Throwable $exception)
    {
        if (config('app.env') === 'production') {
            // Show cleaner errors on production
            if ($exception instanceof \PDOException) {
                abort(500, 'Database error');
            }
        }
        $code = \App\Helper::getExceptionCode($exception);
        if (!$request->ajax()  && !$request->wantsJson() && in_array($code, [404, 403])) {
            if ($code === 403 && session()->pull('dont_render_403_rethrow', false)) {
                // The AddPermissionIfAllowedByAPI Middleware asked us to rethrow the exception so it can intercept it
                throw $exception;
            }
            $message = $exception->getMessage();
            if (preg_match('/^No query results for model \[App\\\(.*)\] .*$/', $message, $matches)) {
                switch ($matches[1]) {
                    case 'Report':
                        $message = __("Ce rapport n'existe pas ou vous n'êtes pas membre de l'équipe qui y travaille.");
                        break;
                    case 'Mission':
                        $message = __("Cette mission n'existe pas");
                        break;
                    default:
                        $message = __("Page non trouvée");
                        break;
                }
            }
            return \App\Helper::redirectBack($message);
        }
        return parent::render($request, $exception);
    }

    /**
     * Render the given HttpException.
     *
     * @param  \Symfony\Component\HttpKernel\Exception\HttpExceptionInterface  $e
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function renderHttpException(HttpExceptionInterface $e)
    {
        if (config('app.debug')) {
            // Force debug rendering in debug mode
            return $this->convertExceptionToResponse($e);
        }
        return parent::renderHttpException($e);
    }
}
