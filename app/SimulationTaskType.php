<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\belongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\DB;

class SimulationTaskType extends Model
{
    protected $table = 'simulation_task_type';
    protected $primaryKey = 'id_task_type';
    public $fillable = [ 'id_global_knowledge', 'simulation_code', 'description', 'technological_feedback', 'learning_goal', 'position'];

    /**
     * @return belongsTo
     */
    public function overtaskType(): belongsTo
    {
        return $this->belongsTo('App\SimulationTaskType', 'id_overtask_type');
    }
    
    /**
     * @return belongsTo
     */
    public function globalKnowledge(): belongsTo
    {
        return $this->belongsTo('App\SimulationGlobalKnowledge', 'id_global_knowledge');
    }

    /**
     * @return BelongsToMany
     */
    public function learners(): BelongsToMany
    {
        return $this->belongsToMany('App\User', 'simulation_link_learner_task', 'id_task_type', 'id_learner')->withPivot('feedback_level', 'knowledge', 'update_number');
    }

    /**
     * @return HasMany
     */
    public function constraints(): HasMany
    {
        return $this->HasMany('App\SimulationConstraint', 'id_task_type', 'id_task_type');
    }

    public static function resetSkills($simulation_code, $user) {
        DB::table('simulation_link_learner_task')
            ->where('simulation_code', $simulation_code)
            ->where('id_learner', $user->id_user)
            ->delete();
    }
}
