<?php

namespace App\Scoring\Processes;

use \App\Scoring\Assessment;
use \App\Scoring\AssessedStudent;
use \App\Scoring\Descrob;
use \App\Scoring\Criterion;
use \Illuminate\Support\Facades\Log;

class UpdateAssessment
{
    /**
     * @var array
     */
    protected $data;

    /**
     * @var Assessment
     */
    protected $assessment;

    public function __construct($assessment, $updates, $students)
    {
        $this->data = [
            'assessment' => $updates,
            'students' => $students,
        ];
        $this->assessment = $assessment;
    }

    protected function updateAssessment()
    {
        $this->assessment->fill($this->data['assessment']);
        Log::Debug(json_encode($this->assessment));
        $this->assessment->save();
    }

    protected function updateCriterionAndDescrobs()
    {
        foreach ($this->data['assessment']['criteria'] as $c) {
            $criterion = Criterion::find($c['id_criterion']);
            if ($criterion) {
                $criterion->fill($c);
                $criterion->save();
                $criterion->descrobs = [];
                foreach ($c['descrobs'] as $d) {
                    $descrob = Descrob::find($d['id_descrob']);
                    if ($descrob) {
                        $descrob->fill($d);
                        $descrob->save();
                    }
                }
            }
        }
    }

    protected function updateStudents()
    {
        foreach ($this->data['students'] as $st) {
            Log::Debug($st);
            $student = AssessedStudent::find($st['id_assessed_student']);
            $student->fill($st);
            $student->save();
        }
    }

    protected function fresh()
    {
        $this->assessment = Assessment::where('id_assessment', $this->assessment->id_assessment)->withAll()->first();
        $this->assessment->FetchStudents();
    }

    public function run()
    {
        $this->updateAssessment();
        $this->updateCriterionAndDescrobs();
        $this->updateStudents();
        $this->fresh();
        return [
            'assessment' => $this->assessment
        ];
    }
}
