<?php

namespace App\Scoring\Processes;

use App\Scoring\Assessment;
use Illuminate\Support\Facades\Log;

class ComputeGradesStats
{
    /** @var Assessment[] */
    protected $assessments;

    /** @var array
     * [
     *  'grades' => $grades
     *  'criteria' => [
     *      'title' => string
     *      'value' => float
     *  ],
     *  'counts' => [
     *      'total' => float
     *      'criteria' => [float, float, ...]
     *      'bonus' => float
     *  ],
     *  'means' => [
     *      'total' => float
     *      'criteria' => [float, float, ...]
     *      'bonus' => float
     *  ],
     *  'stdevs' => [
     *      'total' => float
     *      'criteria' => [float, float, ...]
     *      'bonus' => float
     *  ],
     * ]
     */
    protected $grades;

    protected const NA = '-';

    public function __construct($assessments)
    {
        $this->assessments = $assessments;
        $this->grades = [];
        $this->rubric = null;
    }

    protected static function getNameForSort($grade)
    {
        return strtolower($grade['name']);
    }

    protected function fetchScores()
    {
        foreach ($this->assessments as $a) {
            $grade = $this->toGrade($a);
            if (!empty($grade)) {
                if ($this->rubric === null) {
                    $this->rubric = $a->rubric;
                }
                $this->grades = array_merge($this->grades, $grade);
            }
        }
        usort(
            $this->grades,
            function ($a, $b) {
                return strcmp(self::getNameForSort($a), self::getNameForSort($b));
            }
        );
    }

    /*
     * Return a list of grade for each student from the assesment
     */
    protected function toGrade($a)
    {
        $a->fetchStudents();
        $grade = [];
        $criteria_tmp = array_map(
            fn($c) =>
            [
                'title' => $c->description,
                'value' => 100 * array_reduce(
                    $c->descrobs->all(),
                    function ($carry, $item) use ($c, $a) {
                        if ($item->selected) {
                            if ($carry > 0 && $c->type === $a::DESCR_TYPE) {
                                $carry = ($carry + $item->value) / 2;
                            } else {
                                $carry += $item->value;
                            }
                        }
                        return $carry;
                    }
                ) / (array_reduce(
                    $c->descrobs->all(),
                    function ($carry, $item) use ($c, $a) {
                        if ($c->type === $a::DESCR_TYPE) {
                            return max($carry, $item->value);
                        } else {
                            return $carry + $item->value;
                        }
                    }
                ) ?? 1),
                'activated' => (bool)$c->activated,
                'graded' => array_reduce(
                    $c->descrobs->all(),
                    function ($carry, $item) {
                        if ($item->selected) {
                            $carry += 1;
                        }
                        return $carry;
                    },
                    0
                ),
                // We use the id_criterion of the rubric i.e id_origin
                'id_criterion' => $c->id_origin
            ],
            $a->criteria->all(),
        );
        $criteria = [];
        foreach ($criteria_tmp as $c) {
            $criteria[$c['id_criterion']] = $c;
        }
        foreach ($a->assessed_students as $s) {
            $grade[] = [
                'criteria' => $criteria,
                'missing' => $s->missing,
                'name' => $s->name,
                'total' => $s->missing ? 0 : ($a->score + $s->bonus ?? 0),
                'bonus' => $s->missing ? 0 : $a->bonus,
                'min_bonus' => $a->min_bonus,
                'max_bonus' => $a->max_bonus,
                'individual_bonus' => $s->bonus,
                'published' => (bool)$a->published,
                'team' => $a->getTeamName(),
                'grading' => $a->grading,
                'id_assessment' => $a->id_assessment,
                'id_production' => $a->id_production
            ];
        }
        return $grade;
    }

    /**
     * Computes means, counts and stdev for an array of grades returned by toGrade()
     * @return array
     */
    protected function computeGradesStats()
    {
        $errors = 0;
        // Retrieve criteria from first assessment
        $criteria = [];
        $actual_grades = collect();
        $grading = null;
        $multigraded = false;
        $show_bonus = false;
        $show_individual_bonus = false;
        foreach ($this->grades as $g) {
            if ($grading === null) {
                $grading = $g['grading'];
            } else {
                if ($grading !== $g['grading']) {
                    $multigraded = true;
                }
            }
            if ($g['min_bonus'] != 0 || $g['max_bonus'] != 0 || $g['bonus'] != 0) {
                $show_bonus = true;
            }
            if ($g['individual_bonus'] != 0) {
                $show_individual_bonus = true;
            }
        }
        if ($multigraded) {
            $grading = $this->rubric->grading;
        }
        foreach ($this->grades as $i => $g) {
            if ($g['missing']) {
                continue; // Ignore absent
            }
            if ($multigraded) {
                // normalize total score
                $this->grades[$i]['total'] = $g['total'] * $grading / $g['grading'];
            }
            $actual_grades->push($g);
            foreach ($g['criteria'] as $c) {
                if (!array_key_exists($c['id_criterion'], $criteria)) {
                    $criteria[$c['id_criterion']] = [
                        'title' => $c['title'],
                        'id_criterion' => $c['id_criterion'],
                        'values' => []
                    ];
                }
                if ($c['activated']) {
                    $criteria[$c['id_criterion']]['values'][] = $c['value'];
                }
            }
        }
        $num_criteria = count($criteria);
        $num_students = $actual_grades->count();
        $criteria_counts = [];
        $criteria_means = [];
        $criteria_stdevs = [];

        foreach ($criteria as $id => $c) {
            $len = count($c['values']);
            if ($len > 0) {
                $mean = array_sum($c['values']) / $len;
                $square_sum = array_reduce(
                    $c['values'],
                    function ($carry, $item) {
                        return $carry + $item ** 2;
                    },
                    0
                );
                // sqrt(abs(SUM(elt^2)/n - mean^2)
                // abs is for avoiding issues on float representation errors if near 0
                $stdev = sqrt(abs($square_sum / $len - $mean ** 2));
            } else {
                $mean = self::NA;
                $stdev = self::NA;
            }
            $criteria_counts[$id] = $len;
            $criteria_means[$id] = $mean;
            $criteria_stdevs[$id] = $stdev;
        }

        // Count grades per criteria and total
        $counts = [
            'total' => $num_students,
            'criteria' => $criteria_counts,
            'bonus' => $num_students,
            'individual_bonus' => $num_students
        ];

        // Compute means
        $means = [
            'total' => $num_students === 0 ? self::NA : $actual_grades->reduce(function ($acc, $cur) {
                return $acc + $cur['total'];
            }, 0) / $num_students,
            'criteria' => $criteria_means,
            'bonus' => $num_students === 0 ? self::NA : $actual_grades->reduce(function ($acc, $cur) {
                return $acc + $cur['bonus'];
            }, 0) / $num_students,
            'individual_bonus' => $num_students === 0 ? self::NA : $actual_grades->reduce(function ($acc, $cur) {
                return $acc + $cur['individual_bonus'];
            }, 0) / $num_students,
        ];


        $stdevs = [
            'total' => $num_students === 0 ? self::NA : sqrt(abs(
                $actual_grades->reduce(
                    function ($acc, $cur) {
                        return $acc + $cur['total'] ** 2;
                    },
                    0
                ) / $num_students
                - $means['total'] ** 2
            )),
            'criteria' => $criteria_stdevs,
            'bonus' => $num_students === 0 ? self::NA : sqrt(abs(
                $actual_grades->reduce(
                    function ($acc, $cur) {
                        return $acc + $cur['bonus'] ** 2;
                    },
                    0
                ) / $num_students
                - $means['bonus'] ** 2
            )),
            'individual_bonus' => $num_students === 0 ? self::NA : sqrt(abs(
                $actual_grades->reduce(
                    function ($acc, $cur) {
                        return $acc + $cur['individual_bonus'] ** 2;
                    },
                    0
                ) / $num_students
                - $means['individual_bonus'] ** 2
            ))
        ];

        // Insert missing criteria in grades if any
        Log::Debug(JSON_ENCODE($this->grades, JSON_PRETTY_PRINT));
        foreach ($this->grades as $gid => $g) {
            foreach ($criteria as $id => $c) {
                // Add missing criterias in grading
                if (!array_key_exists($id, $g['criteria'])) {
                    $g['criteria'][$id] = [
                        'title' => $c['title'],
                        'id_criterion' => $c['id_criterion'],
                        'value' => self::NA,
                        'activated' => true
                    ];
                }
            }
            $this->grades[$gid] = $g;
        }

        return [
            'grades' => $this->grades,
            'criteria' => $criteria,
            'counts' => $counts,
            'means' => $means,
            'stdevs' => $stdevs,
            'errors' => $errors,
            'grading' => $grading,
            'show_bonus' => $show_bonus,
            'show_individual_bonus' => $show_individual_bonus
        ];
    }


    public function run()
    {
        if (empty($this->assessments)) {
            return null;
        }
        $this->fetchScores();
        return $this->computeGradesStats();
    }
}
