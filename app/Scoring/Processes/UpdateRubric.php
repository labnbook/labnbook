<?php

namespace App\Scoring\Processes;


use \App\Scoring\Rubric;
use \App\Scoring\CriteriaGroup;
use \App\Scoring\Criterion;
use \App\Scoring\Descrob;

class UpdateRubric
{
    /**
     * @var array
     */
    protected $data;

    /**
     * @var Rubric
     */
    protected $rubric;

    public function __construct($rubric, $groups, $criteria, $deletes)
    {
        $this->data = [
            'rubric' => $rubric,
            // Ignore virtual group added by frontend : id_criteria_group == null
            'groups' => array_filter($groups, fn($g) => $g['id_criteria_group'] != null),
            'criteria' => $criteria,
            'deletes' => $deletes,
        ];
        if (isset($this->data['rubric']['id_rubric'])) {
            $this->rubric = Rubric::find($this->data['rubric']['id_rubric']);
        } else {
            $this->rubric = new Rubric();
        }
    }

    protected function updateRubric()
    {
        $this->rubric->fill($this->data['rubric']);
        $this->rubric->save();

        if (!isset($this->data['rubric']['id_rubric']) || !$this->data['rubric']['id_rubric']) {
            // This is a creation : We need to set the id_rubric in all criteria
            foreach ($this->data['criteria'] as $k => $v) {
                $this->data['criteria'][$k]['id_rubric'] = $this->rubric->id_rubric;
            }
            foreach ($this->data['groups'] as $k => $v) {
                $this->data['groups'][$k]['id_rubric'] = $this->rubric->id_rubric;
            }
        }
    }
    protected function updateCriteriaGroup()
    {
        $data = &$this->data;
        CriteriaGroup::batchUpdate(
            $this->data['groups'],
            function ($index, $g) use (&$data) {
                // Callback : update group <--> criteria link
                $new_id = $g->id_criteria_group;
                $old_id = $data['groups'][$index]['id_criteria_group'];
                if ($new_id !== $old_id) {
                    foreach ($data['criteria'] as $k => $v) {
                        if ($data['criteria'][$k]['id_criteria_group'] === $old_id) {
                            $data['criteria'][$k]['id_criteria_group'] = $new_id;
                        }
                    }
                }
            }
        );
    }

    protected function updateCriteriaAndDescrobs()
    {
        // Update Criteria
        $data = &$this->data;
        Criterion::batchUpdate(
            $this->data['criteria'],
            function ($index, $c) use (&$data) {
                // update criterion <--> descrob link
                $new_id = $c->id_criterion;
                $old_id = $data['criteria'][$index]['id_criterion'];
                if ($new_id !== $old_id) {
                    foreach ($data['criteria'][$index]['descrobs'] as $k => $v) {
                        if ($data['criteria'][$index]['descrobs'][$k]['id_criterion'] === $old_id) {
                            $data['criteria'][$index]['descrobs'][$k]['id_criterion'] = $new_id;
                        }
                    }
                }
                // update descrobs
                Descrob::batchUpdate($data['criteria'][$index]['descrobs'], null);
            }
        );
    }

    protected function deleteById($name, $CLS)
    {
        $CLS::destroy($this->data['deletes'][$name]);
    }

    protected function executeDeletes()
    {
        $this->deleteById('descrobs', Descrob::class);
        $this->deleteById('criteria_group', CriteriaGroup::class);
        $this->deleteById('criteria', Criterion::class);
    }

    public function run()
    {
        $this->updateRubric();
        $this->updateCriteriaGroup();
        $this->updateCriteriaAndDescrobs();
        $this->executeDeletes();
        $activity_corresponding_object = config('scoring.linked_model_classes.activity')::find($this->rubric->id_activity);
        $field = $activity_corresponding_object::UPDATED_AT;
        $activity_corresponding_object->$field = now();
        $activity_corresponding_object->save();
        return [
            'rubric' => Rubric::where('id_rubric', $this->rubric->id_rubric)->withAll()->first()
        ];
    }
}
