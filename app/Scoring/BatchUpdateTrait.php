<?php

namespace App\Scoring;

trait BatchUpdateTrait
{
    /**
     * Update multiple object in batch
     * @param array $data_array containing data for each object
     * @param callable $callback that will be called on each object after update
     */
    public static function batchUpdate($data_array, $callback = null)
    {
        foreach ($data_array as $key => $data) {
            $obj = new static();
            if (array_key_exists($obj->primaryKey, $data) && (int) ($data[$obj->primaryKey]) > 0) {
                $obj = static::find($data[$obj->primaryKey]);
            }
            if ($obj) {
                $obj->fill($data);
                $obj->save();
                if ($callback) {
                    $callback($key, $obj);
                }
            }
        }
    }
}
