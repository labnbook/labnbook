<?php

namespace App\Scoring;

use \Illuminate\Support\Facades\Log;

trait PropagateTrait
{
    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        parent::booted();
        // Register propagate event
        static::created(function ($o) {
            return self::handleCreate($o);
        });
        static::updating(function ($o) {
            return self::handleUpdate($o);
        });
        static::deleting(function ($o) {
            return self::handleDelete($o);
        });
    }

    /**
     * Find the corresponding id_criteria_group on the assesmsent for the given object
     * @param int $id id_criteria_group on the rubric
     * @return int $id_criteria_group on the assessment
     */
    protected function computeAssessmentCriteriaGroup($id, $assessment = null)
    {
        if (!$assessment) {
            $assessment = $this->assessment;
        }
        Log::Debug("Computing criteria group from {$id} for assessment {$this->assessment->id_assessment}");
        foreach ($assessment->criteria_group as $cg) {
            Log::Debug("Criteria group {$cg->id_criteria_group} has origin {$cg->id_origin}");
            if ((int)$cg->id_origin === (int)$id) {
                Log::Debug("Found criteria group {$cg->id_criteria_group}");
                return $cg->id_criteria_group;
            }
        }
        return null;
    }


    /**
     * Find the corresponding id_criteria_group on the assesmsent for the given object
     * @param int $id id_criteria_group on the rubric
     * @param Assessment $assessment the assesment to link
     * @return int $id_criteria_group on the assessment
     */
    protected function computeAssessmentCriterion($id, $assessment)
    {
        foreach ($assessment->criteria as $c) {
            if ($c->id_origin === $id) {
                return $c->id_criterion;
            }
        }
        return null;
    }

    /**
     * Returns the id of an object
     * @param \Illuminate\Database\Eloquent\Model $obj
     * @return int primary key
     */
    protected static function getId($obj)
    {
        $primaryKeyName = $obj->getKeyName();
        return $obj->$primaryKeyName;
    }

    /**
     * Propagate updates to children models
     * @param \Illuminate\Database\Eloquent\Model $object
     */
    public static function handleCreate($object)
    {
        self::logEvent($object, "Handle create for object ");
        if (!$object->rubric) {
            // Either we have an object linked to an assesment or a rubric
            self::logEvent($object, "Aborting", " is not linked to a rubric");
            return;
        }
        foreach ($object->rubric->assessments as $a) {
            self::logEvent($a, "Propagating to objects from");
            if ($a->isPublished()) {
                self::logEvent($a, "Aborting propagate", " is published");
                continue;
            }
            $n = $object->replicate();
            // Laravel seems to replicate the loaded relations ...
            unset($n->rubric);
            $n->id_origin = self::getId($object);
            if ($object->id_rubric) {
                // Descrobs dont have id_rubric / id_assessment
                $n->id_assessment = $a->id_assessment;
                $n->id_rubric = null;
            }
            $keyName = $object->getKeyName() ;
            // Update links
            if ($keyName === 'id_descrob') {
                // Descrob => id_criterion
                $n->id_criterion = $n->computeAssessmentCriterion($object->id_criterion, $a);
            } elseif ($keyName === 'id_criterion') {
                // criterion => id_criteria_group
                $n->id_criteria_group = $n->computeAssessmentCriteriaGroup($object->id_criteria_group, $a);
            }
            $n->save();
            self::logEvent($n, "Created object ", " for assessment " . $a->id_assessment);
        }
    }

    /**
     * Logs a propagate event
     * @param \Illuminate\Database\Eloquent\Model $object
     * @param string $verb event
     * @param string $more more info
     */
    protected static function logEvent($object, $verb, $more = "")
    {
        $id = self::getId($object);
        $type = $object::class;
        Log::Debug($verb . " Object of type " . $type . " and id " . $id . $more);
    }


    /**
     * Handles an update event on object and propagagte changes
     * @param \Illuminate\Database\Eloquent\Model $object
     */
    public static function handleUpdate($object)
    {
        self::logEvent($object, "updating");
        $changes = $object->getDirty();
        if (empty($changes)) {
            return;
        }
        Log::Debug("Original changes " . JSON_ENCODE($changes, JSON_PRETTY_PRINT));
        foreach ($object->children as $child) {
            if ($child->isPublished()) {
                self::logEvent($child, "Aborting propagate", " is published");
                continue;
            }
            $localChanges = $changes;
            // Handle special keys handle id_criteria_group / id_criterion
            if (array_key_exists('id_criteria_group', $localChanges)) {
                    $localChanges['id_criteria_group'] = $child->computeAssessmentCriteriaGroup($localChanges['id_criteria_group']);
            }
            self::logEvent($child, "Propagating to child", " following changes " . JSON_ENCODE($localChanges, JSON_PRETTY_PRINT));
            $child->fill($localChanges);
            $child->save();
        }
    }


    /**
     * Handles a delete event on object and propagagte changes
     * @param \Illuminate\Database\Eloquent\Model $object
     */
    public static function handleDelete($object)
    {
        self::logEvent($object, "deleting");
        foreach ($object->children as $child) {
            if ($child->isPublished()) {
                self::logEvent($child, "Aborting propagate", " is published");
                continue;
            }
            self::logEvent($child, " deleting child ");
            $child->delete();
        }
    }
}
