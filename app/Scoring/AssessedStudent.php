<?php

namespace App\Scoring;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class AssessedStudent extends Model
{
    protected $table = 'assessed_student';
    protected $primaryKey = 'id_assessed_student';
    protected $fillable = ['id_student', 'id_assessment', 'missing', 'bonus', 'bonus_comment'];
    public static array $false_default_value = ['missing'];
    public static array $validation_rules = [
        'id_student' => 'numeric',
        'id_assessment' => 'numeric',
        'missing' => 'nullable|boolean',
    ];

    /**
     * Get the assessment related to an assessed_student.
     */
    public function assessment(): BelongsTo
    {
        return $this->belongsTo(Assessment::class, 'id_assessment', 'id_assessment');
    }
}
