<?php

namespace App\Scoring;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Assessment extends Model
{
    use LocalLinkTrait;

    protected $table = 'assessment';
    protected $primaryKey = 'id_assessment';
    protected $fillable = ['id_production', 'id_rubric_origin', 'bonus', 'bonus_comment', 'score', 'score_comment',
        'published', 'grading', 'min_grade', 'max_grade', 'min_bonus', 'max_bonus'];

    public const STATUS_NOT_STARTED = "TODO";
    public const STATUS_STARTED = "DOING";
    public const STATUS_PUBLISHED = "DONE";

    public const LOCAL_LINK = 'production';
    public const DESCR_TYPE = 'descr';
    public const OBS_TYPE = 'ob';


    /**
     * Get the rubric related to an assessment.
     */
    public function rubric(): BelongsTo
    {
        return $this->belongsTo(Rubric::class, 'id_rubric_origin', 'id_rubric');
    }

    /**
     * Get the criteria related to an assessment.
     */
    public function criteria(): HasMany
    {
        return $this->hasMany(Criterion::class, 'id_assessment')
            ->orderby('criterion.id_criteria_group', 'ASC')
            ->orderby('criterion.position', 'ASC');
    }


    /**
     * Get the criteria_group of an assessment
     */
    public function criteria_group(): hasMany
    {
        return $this->hasMany(CriteriaGroup::class, 'id_assessment')
                    ->distinct('id_criteria_group')
                    ->orderBy('criteria_group.position', 'ASC');
    }

    /**
     * Get the assessed students related to an assessment.
     */
    public function assessed_students(): HasMany
    {
        return $this->hasMany(AssessedStudent::class, 'id_assessment')
            ->whereIn('id_student', self::getLinkedModelById('production', $this->id_production)->users->pluck('id_user'))
            ->orderby('assessed_student.id_student', 'ASC');
    }


    public function production()
    {
        $cls = config('scoring.linked_model_classes.production');
        $o = new $cls();
        return $this->belongsTo($cls, 'id_production', $o->primaryKey);
    }

    /**
     * Loads all related elements
     */
    public function scopeWithAll($query)
    {
        return $query->with('criteria', 'criteria.descrobs', 'criteria_group');
    }

    /**
     * Return the production associated with an id_production
     * @param $id_production
     * @return mixed
     */
    public static function getProduction($id_production)
    {
        $PROD_CLS = config('scoring.linked_model_classes.production');
        return $PROD_CLS::find($id_production);
    }

    /**
     * Generic function to get id_production from id_activity without any
     * knowledge of the model except that there is a relation from production to
     * activity such as if production class is \Some\Namespace\Production then
     * the relation can be accessed via $activity->production
     * @param int $id_production
     * @return int
     */
    public static function getIdActivity($id_production)
    {
        // Activity class
        $ACT_CLS = config('scoring.linked_model_classes.activity');
        $act_name = strtolower(preg_replace('@^.*\\\@', '', $ACT_CLS));
        // Production class
        $PROD_CLS = config('scoring.linked_model_classes.production');
        // Production key name
        $key = (new $ACT_CLS())->getKeyName();
        // Get the key of the activity
        return $PROD_CLS::find($id_production)->$act_name()->pluck($key)[0];
    }

    /**
     * Returns a template assessment from a rubric
     * @param Rubric $rubric
     * @param int id_production
     * @return Assessment
     */
    public static function fromRubric(Rubric $rubric, $id_production)
    {
        $ret = new self();
        $ret->id_rubric_origin = $rubric->id_rubric;
        $ret->id_production = $id_production;
        $ret->published = false;
        $ret->max_grade = $rubric->max_grade;
        $ret->min_grade = $rubric->min_grade;
        $ret->max_bonus = $rubric->max_bonus;
        $ret->min_bonus = $rubric->min_bonus;
        $ret->grading = $rubric->grading;
        $ret->score = null;
        $ret->save();
        $created_groups = [];
        foreach ($rubric->criteria as $crit) {
            $criterion = Criterion::cloneForAssessment($crit, $created_groups, $ret->id_assessment);
        }
        return self::where('id_assessment', $ret->id_assessment)->withAll()->first();
    }

    /**
     * Fetches the assesment students
     */
    public function fetchStudents()
    {
        $production = self::getLinkedModelById('production', $this->id_production);
        $students =  $production->getTeamUsers();
        foreach ($students as $id => $name) {
            $as = $this->assessed_students->first(function ($value, $key) use ($id) {
                return $value->id_student === $id;
            });
            if ($as === null) {
                $as = AssessedStudent::create([
                    'id_assessment' => $this->id_assessment,
                    'id_student' => $id,
                    'name' => $name,
                    'missing' => false,
                    'bonus' => 0,
                    'bonus_comment' => null,
                ]);
                $as->save();
                $this->assessed_students->push($as);
            }
            $as->name = $name;
        }
    }

    /**
     * Clones the assessment and return a copy without selected descrobs,
     * comments and global score
     * (used when a student is allowed to view the assessment criteria before publication)
     * @return mixed
     */
    public function cleanSelectionsAndComments()
    {
        $assessment = clone $this;
        $assessment->score = null;
        $assessment->score_comment = null;
        $assessment->bonus = null;
        $assessment->bonus_comment = null;
        foreach ($assessment->criteria as $criterion_key => $criterion) {
            $assessment->criteria[$criterion_key]->comment = null;
            $assessment->criteria[$criterion_key]->activated = true;
            foreach ($criterion->descrobs as $descrob_key => $descrob) {
                $assessment->criteria[$criterion_key]->descrobs[$descrob_key]->selected = false;
            }
        }
        return $assessment;
    }

    /**
     * Get the team name
     */
    public function getTeamName()
    {
        $production = self::getLinkedModelById('production', $this->id_production);
        return  $production->getTeamName();
    }

    /**
     * Return the status of the assessment
     * @return string
     */
    public function status()
    {
        return $this->published ? self::STATUS_PUBLISHED :
            ($this->score === null ? self::STATUS_NOT_STARTED : self::STATUS_STARTED);
    }

    /**
     * Return all assessments for givent id reports
     * @param int $ids
     * @return \Illuminate\Database\Eloquent\Collection|Assessment[]|
     */
    public static function getAllFromIdReports($ids)
    {
        return self::whereIn('id_production', $ids)->get();
    }

    /**
     * Checks if the criterion is published
     * @return bool
     */
    public function isPublished()
    {
        return $this->published;
    }
}
