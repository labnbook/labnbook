<?php

namespace App\Scoring\Policies;

use App\Scoring\Rubric;
use App\User;
use App\Scoring\Assessment;
use Illuminate\Auth\Access\HandlesAuthorization;

class AssessmentPolicy
{
    /**
     * Determine whether the user can view the assessment.
     *
     * @param  \App\User  $user
     * @param  Assessment $assessment
     * @return mixed
     */
    public function view(User $user, Assessment $assessment)
    {
        return $assessment && (
            ($assessment->published && $user->can('view', $assessment->linkedModel()))
            || $user->can('viewTemplate', $assessment)
            || $user->can('view', $assessment->rubric)
        );
    }

    /**
     * Determine whether the user can view a preprint of an assessment.
     * 
     * @param User $user
     * @param Assessment $assessment
     * @return mixed
     */
    public function viewTemplate(User $user, Assessment $assessment)
    {
        $id_activity = Assessment::getIdActivity($assessment->id_production);
        $activity = Rubric::getActivity($id_activity);
        return $user->can('view', $assessment->linkedModel()) && !is_null($activity) && $activity->rubric_broadcast;
    }
    
    /**
     * Determine whether the user can update the assessment.
     *
     * @param  \App\User  $user
     * @param  Assessment $assessment
     * @return mixed
     */
    public function update(User $user, Assessment $assessment)
    {
        return $user->can('view', $assessment->rubric);
    }

    /**
     * Determine whether the user can delete the assessment.
     *
     * @param  \App\User  $user
     * @param  Assessment $assessment
     * @return mixed
     */
    public function delete(User $user, Assessment $assessment)
    {
        return $user->can('view', $assessment->rubric);
    }
}
