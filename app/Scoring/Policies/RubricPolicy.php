<?php

namespace App\Scoring\Policies;

use App\User;
use App\Scoring\Rubric;
use Illuminate\Auth\Access\HandlesAuthorization;

class RubricPolicy
{
    /**
     * Determine whether the user can view the rubric.
     *
     * @param  \App\User  $user
     * @param  Rubric $rubric
     * @return mixed
     */
    public function view(User $user, Rubric $rubric)
    {
        return $user->can('view', $rubric->linkedModel());
    }

    /**
     * Determine whether the user can update the rubric.
     *
     * @param  \App\User  $user
     * @param  Rubric $rubric
     * @return mixed
     */
    public function update(User $user, Rubric $rubric)
    {
        return $user->can('update', $rubric->linkedModel());
    }

    /**
     * Determine whether the user can delete the rubric.
     *
     * @param  \App\User  $user
     * @param  Rubric $rubric
     * @return mixed
     */
    public function delete(User $user, Rubric $rubric)
    {
        return $user->can('update', $rubric->linkedModel());
    }
}
