<?php

namespace App\Scoring;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Criterion extends Model
{
    use PropagateTrait;
    use BatchUpdateTrait;

    public const DESCR_TYPE = 'descr';
    public const OB_TYPE = 'ob';

    protected $table = 'criterion';
    protected $primaryKey = 'id_criterion';
    protected $fillable = ['id_origin', 'id_rubric', 'id_assessment', 'id_section', 'id_criteria_group', 'type',
        'position', 'description', 'relative_weight', 'comment', 'activated'];
    public static array $false_default_value = ['position'];
    public static array $true_default_value = ['activated'];
    public static array $descr_default_value = ['type'];

    /**
     * Get the descrobs related to a criterion.
     */
    public function descrobs(): HasMany
    {
        return $this->hasMany(Descrob::class, 'id_criterion', 'id_criterion')
            ->orderBy('position', 'ASC')
            ->orderBy('id_descrob', 'ASC');
    }

    /**
     * Get the criteria_group related to a criterion.
     */
    public function criteria_group(): BelongsTo
    {
        return $this->belongsTo(CriteriaGroup::class, 'id_criteria_group', 'id_criteria_group');
    }

    /**
     * Get the assessment related to a criterion.
     */
    public function assessment(): BelongsTo
    {
        return $this->belongsTo(Assessment::class, 'id_assessment', 'id_assessment');
    }

    /**
     * Get the rubric related to a criterion.
     */
    public function rubric(): BelongsTo
    {
        return $this->belongsTo(Rubric::class, 'id_rubric', 'id_rubric');
    }

    /**
     * Get the origin criterion.
     */
    public function origin(): BelongsTo
    {
        return $this->belongsTo(self::class, 'id_origin', 'id_criterion');
    }

    /**
     * Get the children criteria related to a criterion.
     */
    public function children(): HasMany
    {
        return $this->hasMany(Criterion::class, 'id_origin', 'id_criterion')
            ->orderBy('id_criterion', 'ASC');
    }

    /**
     * Checks if the criterion is published
     * @return bool
     */
    public function isPublished()
    {
        return $this->assessment != null && $this->assessment->published;
    }

    /**
     * Clones a criterion, it's group and descrobs from raw data
     * @param StdClass $data
     * @param array &$created_groups
     * @param int $id_assessment
     * @return Criterion
     */
    public static function cloneForAssessment($data, &$created_groups, $id_assessment)
    {
        // Clone criterion
        $ret = new self();
        $ret->id_rubric = null;
        $ret->id_assessment = $id_assessment;
        $ret->id_origin = $data->id_criterion;
        $ret->id_section = $data->id_section;
        $ret->id_criteria_group = $data->id_criteria_group;
        $ret->type = $data->type;
        $ret->position = $data->position;
        $ret->description = $data->description;
        $ret->relative_weight = $data->relative_weight;
        $ret->save();
        // Clone group if any
        if ($ret->id_criteria_group) {
            if (array_key_exists($ret->id_criteria_group, $created_groups)) {
                // This group has already been cloned, fetch it
                $ret->id_criteria_group = $created_groups[$ret->id_criteria_group];
                $ret->save();
                $group = CriteriaGroup::find($ret->id_criteria_group);
            } else {
                // This is a new group, clone it
                $group = new CriteriaGroup();
                $group->id_origin = $data->criteria_group->id_criteria_group;
                $group->position = $data->criteria_group->position;
                $group->description = $data->criteria_group->description;
                $group->id_assessment = $id_assessment;
                $group->relative_weight = $data->criteria_group->relative_weight;
                $group->save();
                // Update group id
                $created_groups[$ret->id_criteria_group] = $group->id_criteria_group;
                $ret->id_criteria_group = $group->id_criteria_group;
                $ret->save();
            }
        }
        // Clone descrobs
        foreach ($data->descrobs as $dob) {
            $descrob = new Descrob();
            $descrob->id_criterion = $ret->id_criterion;
            $descrob->description = $dob->description;
            $descrob->value = $dob->value;
            $descrob->position = $dob->position;
            $descrob->id_origin = $dob->id_descrob;
            $descrob->save();
        }
        return $ret;
    }
}
