<?php

namespace App\Scoring;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class CriteriaGroup extends Model
{
    use PropagateTrait;
    use BatchUpdateTrait;

    protected $table = 'criteria_group';
    protected $primaryKey = 'id_criteria_group';
    protected $fillable = ['id_origin', 'position', 'description', 'relative_weight', 'id_rubric', 'id_assessment'];
    public static array $zero_default_value = ['position'];

    /**
     * Get the criteria related to a criteria group.
     */
    public function criteria(): HasMany
    {
        return $this->hasMany(Criterion::class, 'id_criteria_group')
            ->orderby('criterion.id_criterion', 'ASC');
    }

    /**
     * Get the parent criteria_group related to a criteria_group.
     */
    public function assessment(): BelongsTo
    {
        return $this->BelongsTo(Assessment::class, 'id_assessment', 'id_assessment');
    }

    /**
     * Get the parent criteria_group related to a criteria_group.
     */
    public function rubric(): BelongsTo
    {
        return $this->BelongsTo(Rubric::class, 'id_rubric', 'id_rubric');
    }

    /**
     * Get the children criteria_groups related to a criteria_group.
     */
    public function children(): HasMany
    {
        return $this->hasMany(CriteriaGroup::class, 'id_origin', 'id_criteria_group')
            ->orderBy('id_criteria_group', 'ASC');
    }

    /**
     * Checks if the criterion is published
     * @return bool
     */
    public function isPublished()
    {
        return $this->assessment != null && $this->assessment->published;
    }
}
