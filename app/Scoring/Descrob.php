<?php

namespace App\Scoring;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOneThrough;

class Descrob extends Model
{
    use PropagateTrait;
    use BatchUpdateTrait;

    protected $table = 'descrob';
    protected $primaryKey = 'id_descrob';
    protected $fillable = ['id_origin', 'id_criterion', 'description', 'value', 'position', 'selected'];
    public static array $zero_default_value = ['position'];
    public static array $false_default_value = ['selected'];

    /**
     * Get the criterion related to a descrob.
     */
    public function criterion(): BelongsTo
    {
        return $this->belongsTo(Criterion::class, 'id_criterion', 'id_criterion');
    }

    /**
     * Get the rubric linked to a descrob
     */
    public function rubric(): HasOneThrough
    {
        return $this->hasOneThrough(
            'App\Scoring\Rubric',
            'App\Scoring\Criterion',
            'id_criterion', // fk on criterion
            'id_rubric', // fk on rubric
            'id_criterion', // fk on descrob
            'id_rubric', // fk on rubric
        );
    }

    /**
     * Get the assessment linked to a descrob
     */
    public function assessment(): HasOneThrough
    {
        return $this->hasOneThrough(
            'App\Scoring\Assessment',
            'App\Scoring\Criterion',
            'id_criterion', // fk on criterion
            'id_assessment', // fk on rubric
            'id_criterion', // fk on descrob
            'id_assessment', // fk on rubric
        );
    }

    /**
     * Get the children descrobs related to a descrob.
     */
    public function children(): HasMany
    {
        return $this->hasMany(Descrob::class, 'id_origin', 'id_descrob')
            ->orderBy('id_descrob', 'ASC');
    }

    /**
     * Checks if the criterion is published
     * @return bool
     */
    public function isPublished()
    {
        return $this->criterion != null && $this->criterion->isPublished();
    }
}
