<?php

namespace App\Scoring;

trait LocalLinkTrait
{

    /**
     * Returns the model from the local object to which it is bound
     * @param int $id_local
     * @return Model|null
     */
    public static function fromLocalLink($id_local, $norelated = false)
    {
        $query = static::where(self::linkedModelIdName(), $id_local) ;
        if (!$norelated) {
            $query = $query->withAll();
        }
        return $query->first();
    }

    /**
     * Return the id name of the linked model
     * @return string
     */
    public static function linkedModelIdName()
    {
        return 'id_' . static::LOCAL_LINK;
    }

    /**
     * Returns the locally linked object
     * @param StdClass
     */
    public function linkedModel()
    {
        $id_name = self::linkedModelIdName();
        $id = $this->$id_name;
        return static::getLinkedModelById(static::LOCAL_LINK, $id);
    }

    /**
     * Returns the class of the linked model by name
     * @param string $modelName name of the model in the remote app scoring|production|section
     * @return CLASS
     */
    public static function getLinkedModelClass($modelName)
    {
        $cls = config('scoring.linked_model_classes.' . $modelName, null);
        if (!$cls) {
            throw new \Exception('Unknown model ' . $modelName);
        }
        return $cls;
    }

    /**
     * Returns a model object from model name in scoring by id
     * @param string $modelName name of the model in the remote app scoring|production|section
     * @param int   $id
     * @return \App\Model
     */
    public static function getLinkedModelById($modelName, $id)
    {
        return self::getLinkedModelClass($modelName)::find($id);
    }
}
