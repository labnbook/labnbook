<?php

namespace App\Scoring;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use App\Scoring\Assessment;
use Illuminate\Support\Facades\Log;

class Rubric extends Model
{
    use LocalLinkTrait;
    use PropagateTrait;

    protected $table = 'rubric';
    protected $primaryKey = 'id_rubric';
    protected $fillable = ['id_activity', 'name', 'description', 'grading', 'min_grade', 'max_grade', 'min_bonus', 'max_bonus'];
    public const LOCAL_LINK = 'activity';

    /**
     * Get the assessments related to a rubric.
     */
    public function assessments(): HasMany
    {
        return $this->hasMany(Assessment::class, 'id_rubric_origin', 'id_rubric')
                    ->orderBy('id_assessment', 'ASC');
    }

    public function keepOnlyStudentsAssessments()
    {
        $this->assessments = $this->assessments()->get()->filter(function (Assessment $value, int $key) {
            $r = config('scoring.linked_model_classes.production')::find($value->id_production);
            return (!is_null($r) && $r->isStudentActivity());
        })->values();
    }


    public function activity()
    {
        $cls = config('scoring.linked_model_classes.activity');
        $o = new $cls();
        return $this->belongsTo($cls, 'id_activity', $o->primaryKey);
    }


    /**
     * Get the children assesment related to a rubric.
     */
    public function children()
    {
        return $this->assessments();
    }

    /**
     * Get the criteria related to a rubric.
     */
    public function criteria(): HasMany
    {
        return $this->hasMany(Criterion::class, 'id_rubric', 'id_rubric')
            ->orderBy('position', 'ASC')
            ->orderBy('id_criterion', 'ASC');
    }

    /**
     * Get the criteria_group of an assessment
     */
    public function criteria_group(): HasMany
    {
        return $this->hasMany(CriteriaGroup::class, 'id_rubric')
                    ->distinct('id_criteria_group')
                    ->orderBy('criteria_group.position', 'ASC');
    }

    /**
     * Loads all related elements
     */
    public function scopeWithAll($query)
    {
        return $query->with('criteria', 'criteria.descrobs', 'criteria_group', 'assessments');
    }

    /**
     * @param \App\User $user
     * @return Activity[]
     */
    public static function getUserActivitiesWithRubrics($user)
    {
        // TODO make this call generic
        $activities = \App\Mission::getTeacherMissions($user);
        $ids = $activities->map(function ($e) {
            return $e->getKey();
        })->toArray();
        $rubrics = self::whereIn('id_activity', $ids)->get();
        $ids_to_keep = $rubrics->map(
            function ($e) {
                return $e->id_activity;
            },
        );
        return $activities->filter(function ($value, $key) use ($ids_to_keep) {
            return $ids_to_keep->contains($value->getKey());
        });
    }

    /**
     * Returns the activity object corresponding to its id
     * @param $id_activity
     * @return mixed
     */
    public static function getActivity($id_activity)
    {
        // Activity class
        $ACT_CLS = config('scoring.linked_model_classes.activity');
        return $ACT_CLS::find($id_activity);
    }

    /**
     * Duplicate the rubric for another activity
     * @param int id_activity
     */
    public function duplicateForActivity($id_activity)
    {
        $rubric = $this->replicate();
        $rubric->id_activity = $id_activity;
        $rubric->save();
        $groups = [];
        foreach ($this->criteria_group as $g) {
            $new_g = $g->replicate();
            $new_g->id_rubric = $rubric->id_rubric;
            $new_g->save();
            $groups[$g->id_criteria_group] = $new_g;
        }
        foreach ($this->criteria as $c) {
            $new_c = $c->replicate();
            $new_c->id_rubric = $rubric->id_rubric;
            $new_c->id_criteria_group = $groups[$c->id_criteria_group]->id_criteria_group;
            $new_c->save();
            foreach ($c->descrobs as $d) {
                $new_d = $d->replicate();
                $new_d->id_criterion = $new_c->id_criterion;
                $new_d->save();
            }
        }
    }
}
