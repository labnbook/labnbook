<?php

namespace App;

use App\Simulation\DosageSpectro;
use App\Simulation\Titrab;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

abstract class Simulation
{
    protected Collection $protocol;
    protected Collection $materialList;
    protected string $simulation_code;
    
    protected Collection $constraints;
    
    protected Collection $quantities;
    
    protected array $constraintsDependencies;

    const SIMULATION_PARAMETERS = [
        'messages' => [
            1 => [ 'message' => [ 'constraintError', 'theoreticalTask'], 'help' => 1.0 ],
            2 => [ 'message' => [ 'constraintError', 'theoreticalTask', 'technologicalTask'], 'help' => 1.3 ],
            3 => [ 'message' => [ 'constraintError', 'theoreticalTask', 'technicalTask'], 'help' => 1.8 ],
            4 => [ 'message' => [ 'constraintError', 'theoreticalTask', 'correctionTask'], 'help' => 2.1 ],
            5 => [ 'message' => [ 'constraintError', 'theoreticalTask', 'correctionTask', 'highlight'], 'help' => 2.3 ],
            6 => [ 'message' => [ 'constraintError', 'theoreticalTask', 'correctionTask', 'highlight'], 'help' => 2.3 ],
        ],
        'weight_list' => [
            'error__error' => 0.0,
            'error__success' => 0.6,
            'error_with_feedback__error' => 0.26,
            'error_with_feedback__success' => 0.6,
            'success__error' => 0.4,
            'success__success' => 0.0,
            'last_not_null_with_feedback_error__error' => 0.18,
            'last_not_null_with_feedback_error__success' => 0.4,
            'last_not_null_error__error' => 0.0,
            'last_not_null_error__success' => 0.6,
            'last_not_null_success__error' => 0.4,
            'last_not_null_success__success' => 0.0,
            'unknown__error' => 0.6,
            'unknown__success' => 0.6,
        ]
    ];

    /**
     * @param $previous_states
     * @return void
     *//**
     * @param $previous_states
     * @return void
     */
    public function prepareConstraints($previous_states, $previous_error): void
    {
        $this->constraints = SimulationConstraint::where('simulation_code', $this->simulation_code)->orderBy('position')->get();
        $previous_states = collect($previous_states);
        if($previous_states->isNotEmpty()) {
            foreach ($previous_states as $state) {
                $constraint = $this->constraints->find($state['id_constraint']);
                if ( array_key_exists('current_state', $state) && $state['current_state'] !== null) {
                    $constraint['previous_state'] = intval($state['current_state']);
                    $constraint['last_not_null_state'] = intval($state['current_state']);
                    $constraint['last_with_feedback'] = $previous_error && intval($previous_error['id_constraint']) === intval($state['id_constraint']);
                } else if ( array_key_exists('last_not_null_state', $state) && $state['last_not_null_state'] !== null ) {
                    $constraint['previous_state'] = null;
                    $constraint['last_not_null_state'] = intval($state['last_not_null_state']);
                    $constraint['last_with_feedback'] = $state['last_with_feedback']==='true';
                } else {
                    $constraint['previous_state'] = null;
                    $constraint['last_not_null_state'] = null;
                    $constraint['last_with_feedback'] = null;
                }
            }
        } else {
            foreach ($this->constraints as $constraint) {
                $constraint['previous_state'] = null;
                $constraint['last_not_null_state'] = null;
                $constraint['last_with_feedback'] = null;
            }
        }
    }
    
    public function constraintCheckingIsNotRelevant($constraint)
    {
        // Check whether the evaluation of the constraint is relevant 
        $failed_relevance_condition = false;
        if ($constraint->relevanceConstraints->count() > 0) {
            $allRC = $constraint->allRelevanceConstraints();
            $failed_relevance_condition = $this->constraints
                ->filter(function ($cstr) use ($allRC) {
                    return $allRC->contains('id_constraint', $cstr->id_constraint);
                })->values()->contains(function ($cstr) {
                    return $cstr['current_state'] === 0;
                });
        }
        return $failed_relevance_condition;
    }
    
    /**
     * Remove dataset and text labdoc from report
     * @param $labdocProcedure
     * @return void
     */
    public function removeDatasetAndResponseLabdoc($labdocProcedure, $replaceLdResponse, $replaceLdData) 
    {
        $ldDataset = $labdocProcedure->report->labdocs->whereNull('deleted')->firstWhere('type_labdoc', 'dataset');
        if ($ldDataset && $replaceLdData) {
            $ldDataset->delete();
        }
        $ldText = $labdocProcedure->report->labdocs->whereNull('deleted')->where('type_labdoc', 'text')
            ->first(fn($l) => str_contains($l->labdoc_data,'verificationOfTheRequiredConcentration'));
        if ($ldText && $replaceLdResponse) {
            $ldText->delete();
        }
    }

    /**
     * Get a value of a $variableKey from a step $node at a given $iterationNumber
     * @param $variableKey
     * @param $node
     * @param $stepId
     * @param $iterationNumber
     * @return mixed|null
     */
    public function getVariableValue($variableKey)
    {
        $variable = $this->protocol->flatMap(function($action) {
            return collect(array_keys($action))->map(function($key) use ($action) {
                return ['key' => $key, 'value' => $action[$key]];
            });
        })->where('key', $variableKey)->first();
        if ($variable === null) {
            return null;
        } else {
            // if material is requested, send an element of $materialList rather than one of $protocol for unit conversion reason
            if (is_array($variable['value']) && array_key_exists('properties', $variable['value'])) {
                return collect($this->materialList)->firstWhere('id',  $variable['value']['id']);
            } else {
                return $variable['value'];
            }
        }
    }

    /**
     * Get a step $node with the given $stepKey inside a step $node
     * @param $node
     * @param $stepKey
     * @return Collection|null
     */
    public function getActionsInStep ($stepKey)
    {
        return $this->protocol
            ->filter(fn($action) => array_key_exists('stepSimulationVariable', $action))
            ->whereIn( 'stepSimulationVariable', $stepKey);
    }

    /**
     * Get all the materials matching ($propertyKey, $propertyValue)
     * @param $propertyKey
     * @param $propertyValue
     * @return Collection
     */
    public function getMaterialsByProperty ($propertyKey, $propertyValue) {
        return $this->materialList->filter(function($item) use ($propertyKey, $propertyValue) {
            return count(collect($item['properties'])->where('name', '=', $propertyKey)->whereIn('value',$propertyValue)) > 0;
        });
    }

    /**
     * Get a $material property value given a $propertyName
     * @param $material
     * @param $propertyName
     * @return integer|string|null
     */
    public function getMaterialPropertyValue ($material, $propertyName) {
        $property = collect($material['properties'])->firstWhere('name', $propertyName);
        if($property) {
            return $property['value'];
        } else {
            return null;
        }

    }

    /**
     * Check whether the given $material exposes the $propertyKey
     * @param $material
     * @param $propertyKey
     * @return bool
     */
    public function materialHasPropertyKey($material, $propertyKey) {
        if ($material) {
            return collect($material['properties'])->contains('name', $propertyKey);
        } else {
            return false;
        }
    }
    
    public function getCurrentSkills($user)
    {
        return collect(DB::table('simulation_link_learner_task')
            ->where('simulation_code', $this->simulation_code)
            ->where('id_learner', $user->id_user)
            ->get());
    }
    
    public function computeSkillsChanges ($oldSkills, $newSkills)
    {
        $skillsChanges = collect();
        foreach ($newSkills as $newSkill) {
            $oldSkill = $oldSkills->first(fn($s) => intval($newSkill['id_task_type']) === intval($s->id_task_type)
                && intval($newSkill['id_user']) === intval($s->id_learner)
                && $newSkill['simulation_code'] === $s->simulation_code);
            if ($oldSkill && floatval($newSkill['knowledge']) !== floatval($oldSkill->knowledge)) {
                $skillsChanges->push([
                    'description' => $newSkill['description'],
                    'id_task_type' => $newSkill['id_task_type'],
                    'old_feedback_level' => $oldSkill->feedback_level,
                    'new_feedback_level' => $newSkill['feedback_level'],
                    'old_knowledge' => $oldSkill->knowledge,
                    'new_knowledge' => $newSkill['knowledge'],
                ]);
            } else if(!$oldSkill) {
                $skillsChanges->push([
                    'description' => $newSkill['description'],
                    'id_task_type' => $newSkill['id_task_type'],
                    'old_feedback_level' => null,
                    'new_feedback_level' => $newSkill['feedback_level'],
                    'old_knowledge' => null,
                    'new_knowledge' => $newSkill['knowledge'],
                ]);
            }
        }
        return $skillsChanges;
    }
    
    public function decrementFeedbackLevel($user, $evaluated_constraints, $previous_error, $current_error) {
        $taskTypes = SimulationTaskType::where('simulation_code', $this->simulation_code)->get();
        foreach ($taskTypes as $taskType) {
            $fb_increment = 0;
            $relatedConstraints = $evaluated_constraints->where('id_task_type', $taskType->id_task_type)->sortBy('position');
            $constraintForFeedback = null;
            foreach ($relatedConstraints as $relatedConstraint) {
                $constraintForFeedback = $relatedConstraint;
                if($current_error && intval($relatedConstraint['id_constraint']) === intval($current_error['id_constraint'])) {
                    // Case current error in related constraints => break the loop
                    break;
                }
                if($previous_error && $relatedConstraint['current_state'] === 1 && intval($relatedConstraint['id_constraint']) === intval($previous_error['id_constraint'])) {
                    // Case last with feedback corrected => -3
                    // 0FB -> 1
                    $fb_increment -= 3;
                } else if($relatedConstraint['current_state'] === 1) {
                    // Case not last or current in task type constraints => get first not null and apply standard increment
                    if($relatedConstraint['previous_state'] === 0) {
                        // 0 -> 1
                        $fb_increment -= 2;
                    } else if ($relatedConstraint['last_not_null_state'] === 0) {
                        // O/OFB -> NULL -> 1
                        $fb_increment -= 2;
                    } else if ($relatedConstraint['last_not_null_state'] === null) {
                        // - -> 1
                        $fb_increment -= 1;
                    }
                }
            }
            if($relatedConstraints->whereNotNull('current_state')->count() > 0) {
                $this->updateUserFeedbackLevel($user, $constraintForFeedback, $taskType, $fb_increment);
            }
        }
    }
    
    public function updateUserFeedbackLevel($user, $constraint, $taskType, $fb_increment) {
        $user_task_profile = $taskType->learners->find(['simulation_code' => $this->simulation_code, 'id_learner' => $user->id_user])->first();
        $taskTypeMessageConfig = $this->overLoadLearningGoalConfig($constraint, $taskType);
        $max_message_index_in_lg_table = count($taskTypeMessageConfig) - 1;
        if ($user_task_profile === null) {
            $knowledges = $user->simulationTaskTypes()->where('simulation_link_learner_task.simulation_code', $this->simulation_code)->pluck('knowledge');
            $initialLevelIndex = 0;
            if($knowledges->count()>0) {
                $averageKnowledge = $knowledges->sum()/$knowledges->count();
                if($averageKnowledge>0.6) {
                    $initialLevelIndex = 0;
                } else if($averageKnowledge>0.4) {
                    if($taskType->learning_goal!==0) {
                        $initialLevelIndex = 1;
                    }
                } else if($averageKnowledge>0.2) {
                    if($taskType->learning_goal===1) {
                        $initialLevelIndex = 1;
                    } else if($taskType->learning_goal===2) {
                        $initialLevelIndex = 2;
                    }
                } else if($averageKnowledge>0) {
                    if($taskType->learning_goal===1) {
                        $initialLevelIndex = 2;
                    } else if($taskType->learning_goal===2) {
                        $initialLevelIndex = 3;
                    }
                }
            }
            $message_index_in_lg_table = min($max_message_index_in_lg_table,$initialLevelIndex);
        } else {
            $message_index_in_lg_table = max(0, min(
                $max_message_index_in_lg_table,
                array_search(intval($user_task_profile->pivot->feedback_level), $taskTypeMessageConfig) + $fb_increment
            ));
        }
        $feedback_level = intval($taskTypeMessageConfig[$message_index_in_lg_table]);
        if ($taskType->learners->contains($user->id_user)) {
            $taskType->learners()->updateExistingPivot($user->id_user, ['simulation_code' => $this->simulation_code, 'feedback_level' => $feedback_level]);
        } else {
            $taskType->learners()->attach($user->id_user, ['simulation_code' => $this->simulation_code, 'feedback_level' => $feedback_level]);
        }
    }

    public function updateSkills($user, $evaluated_constraints, $previous_error)
    {
        $new_skills = collect();
        $taskTypes = SimulationTaskType::where('simulation_code', $this->simulation_code)->get();
        foreach ($taskTypes as $taskType) {
            $user_task_profile = $taskType->learners->find(['simulation_code' => $this->simulation_code, 'id_learner' => $user->id_user])->first();
            if ($user_task_profile !== null) {
                $relatedConstraints = $evaluated_constraints->where('id_task_type', $taskType->id_task_type)->sortBy('position');
                $previous_knowledge = $user_task_profile->pivot->knowledge;
                $feedback_level = $user_task_profile->pivot->feedback_level;
                $update_number = $user_task_profile->pivot->update_number;
                // Compute knowledge
                $weightsAndStates = collect();
                foreach ($relatedConstraints as $constraint) {
                    // positive weight increase knowledge, negative weight decrease knowledge;
                    $weightsAndStates->push($this->getWeight($constraint, $previous_error, $feedback_level));
                }
                if ($weightsAndStates->some(fn($weightAndState) => $weightAndState[1] !== null)) {
                    if ($weightsAndStates->count() > 1) {
                        $maxIncreaseWeight = $weightsAndStates
                            ->filter(fn($weightAndState) => intval($weightAndState[1]) === 1)
                            ->map(fn($weightAndState) => floatval($weightAndState[0]))
                            ->max();
                        $maxIncreaseWeight = ($maxIncreaseWeight === null) ? 0 : $maxIncreaseWeight;
                        $maxDecreaseWeight = $weightsAndStates
                            ->filter(fn($weightAndState) => intval($weightAndState[1]) === 0)
                            ->map(fn($weightAndState) => floatval($weightAndState[0]))
                            ->max();
                        $maxDecreaseWeight = ($maxDecreaseWeight === null) ? 0 : $maxDecreaseWeight;
                        $weight = $maxIncreaseWeight - $maxDecreaseWeight;
                        $currentState = $weight > 0 ? 1 : 0;
                        $weight = abs($weight);
                    } else {
                        $weight = $weightsAndStates->first()[0];
                        $currentState = $weightsAndStates->first()[1];
                    }
                    if ($previous_knowledge) {
                        $knowledge = $previous_knowledge + $weight * ($currentState - $previous_knowledge);
                    } else if ($currentState === 1) {
                        $knowledge = 0.8;
                    } else if ($currentState === 0) {
                        $knowledge = 0.2;
                    }
                    $update_number = $update_number + ($knowledge !== $previous_knowledge);
                    // Update knowledge (here we are sure there is an existing pivot)
                    $taskType->learners()->updateExistingPivot($user->id_user, ['simulation_code' => $this->simulation_code, 'feedback_level' => $feedback_level, 'knowledge' => $knowledge, 'update_number' => $update_number]);
                    $new_skills->push(['id_user' => $user->id_user,
                        'simulation_code' => $this->simulation_code,
                        'id_task_type' => $taskType->id_task_type,
                        'description' => $taskType->description,
                        'feedback_level' => $feedback_level,
                        'knowledge' => $knowledge,
                    ]);
                }
            }
        }
        return $new_skills;
    }

    public static function getGlobalKnowledges($user, $simulationCode, $withUnknown = true)
    {
        $globalKnowledges = SimulationGlobalKnowledge::where('simulation_code', $simulationCode)->orderBy('position')->get();
        foreach ($globalKnowledges as $key => $globalKnowledge) {
            $weightedAverage = 0;
            $counter = 0;
            $relatedTaskTypes =  $globalKnowledge->taskTypes->filter(fn($tt) => $tt->learning_goal !== 0);
            foreach ($relatedTaskTypes as $taskType) {
                $userTaskProfile = $taskType->learners->find(['simulation_code' => $simulationCode, 'id_learner' => $user->id_user])->first();
                if($userTaskProfile) {
                    $weightedAverage += $userTaskProfile->pivot->knowledge * $taskType->learning_goal;
                    $counter += $taskType->learning_goal;
                }
            }
            if($counter > 0 ) {
                $globalKnowledges[$key]['knowledge'] = $weightedAverage / $counter;
                $globalKnowledges[$key]['color'] = 'hsl('.($globalKnowledges[$key]['knowledge'] * 120).','.(5+90*abs(0.5-$globalKnowledges[$key]['knowledge'])).'%,50%)';
            } else {
                if($withUnknown) {
                    $globalKnowledges[$key]['knowledge'] = null;
                    $globalKnowledges[$key]['color'] = 'lightgrey';
                } else {
                    unset($globalKnowledges[$key]);
                }
            }
        }
        return $globalKnowledges;
    }
    
    public function getGlobalKnowledgeEvolutions($user, $oldGlobalKnowledges, $onlyEvolution)
    {
        $newGlobalKnowledges = Simulation::getGlobalKnowledges($user, $this->simulation_code);
        if($onlyEvolution) {
            $newGlobalKnowledges = $newGlobalKnowledges->filter(fn($nk) =>
                $oldGlobalKnowledges->contains(
                    fn($ok) => $ok['id_global_knowledge'] === $nk['id_global_knowledge']
                        && $ok['knowledge'] !== $nk['knowledge']
                ))->map(function($k) use ($newGlobalKnowledges, $oldGlobalKnowledges) {
                    $newK = $newGlobalKnowledges->first(fn($nk) => $nk['id_global_knowledge'] === $k['id_global_knowledge']);
                    $oldK = $oldGlobalKnowledges->first(fn($ok) => $ok['id_global_knowledge'] === $k['id_global_knowledge']);
                    $k['evolution'] = $newK->knowledge - $oldK->knowledge;
                    return $k;
                })->values()->all();
        }
        return $newGlobalKnowledges;
    }
    
    public function getProgression($evaluated_constraints)
    {
        $numberValidConstraints = $evaluated_constraints->filter(fn($constraint) => $constraint['current_state'] === 1)->count();
        return $numberValidConstraints / ($evaluated_constraints->count()-$this->irrelevantConstraintsNumber);
    }
    
    public function getWeight ($constraint, $previous_error, $feedback_level)
    {
        $help = floatval(self::SIMULATION_PARAMETERS['messages'][$feedback_level]['help']);
        $weight = 0;
        if ($constraint['current_state'] === 1) {
            // ? -> 1
            if ($constraint['previous_state'] === 0) {
                if ($constraint->id_constraint === $previous_error['id_constraint']) {
                    // 0FB -> 1
                    $weight = self::SIMULATION_PARAMETERS['weight_list']['error_with_feedback__success'] / $help;
                } else {
                    // 0 -> 1
                    $weight = self::SIMULATION_PARAMETERS['weight_list']['error__success'];
                }
            } else if ($constraint['previous_state'] === 1) {
                // 1 -> 1
                $weight = self::SIMULATION_PARAMETERS['weight_list']['success__success'];
            } else if ($constraint['last_not_null_state'] === 0 && $constraint['last_with_feedback']) {
                // 0FB -> NULL -> 1
                $weight = self::SIMULATION_PARAMETERS['weight_list']['last_not_null_with_feedback_error__success'] / $help;
            } else if ($constraint['last_not_null_state'] === 0) {
                // 0 -> NULL -> 1
                $weight = self::SIMULATION_PARAMETERS['weight_list']['last_not_null_error__success'];
            } else if ($constraint['last_not_null_state'] === 1) {
                // 1 -> NULL -> 1
                $weight = self::SIMULATION_PARAMETERS['weight_list']['last_not_null_success__success'];
            } else if ($constraint['last_not_null_state'] === null) {
                // - -> 1
                $weight = self::SIMULATION_PARAMETERS['weight_list']['unknown__success'];
            }
        } else if ($constraint['current_state'] === 0) {
            // ? -> 0
            if ($constraint['previous_state'] === 0) {
                if ($constraint->id_constraint === $previous_error['id_constraint']) {
                    // 0FB -> 0
                    $weight = self::SIMULATION_PARAMETERS['weight_list']['error_with_feedback__error'] * $help;
                } else {
                    // 0 -> 0
                    $weight = self::SIMULATION_PARAMETERS['weight_list']['error__error'];
                }
            } else if ($constraint['previous_state'] === 1) {
                // 1 -> 0
                $weight = self::SIMULATION_PARAMETERS['weight_list']['success__error'];
            } else if ($constraint['last_not_null_state'] === 0 && $constraint['last_with_feedback']) {
                // 0FB -> NULL -> 0
                $weight = self::SIMULATION_PARAMETERS['weight_list']['last_not_null_with_feedback_error__error'] * $help;
            } else if ($constraint['last_not_null_state'] === 0) {
                // 0 -> NULL -> 0
                $weight = self::SIMULATION_PARAMETERS['weight_list']['last_not_null_error__error'];
            } else if ($constraint['last_not_null_state'] === 1) {
                // 1 -> NULL -> 0
                $weight = self::SIMULATION_PARAMETERS['weight_list']['last_not_null_success__error'];
            } else if ($constraint['last_not_null_state'] === null) {
                // - -> 0
                $weight = self::SIMULATION_PARAMETERS['weight_list']['unknown__error'];
            }
        }
        return [$weight, $constraint['current_state']];
    }

    public function generateFeedback ($user, $current_error)
    {
        $messages = [];
        if ($current_error) {
            $learner_task = $current_error->taskType->learners->find(['simulation_code' => $this->simulation_code, 'id_learner' => $user->id_user])->first();
            $task = $current_error->taskType;
            $overtask = $task->overtaskType;
            if ($learner_task) {
                $feedback_level = $learner_task->pivot->feedback_level;
            } else {
                $taskTypeMessageConfig = $this->overLoadLearningGoalConfig($current_error, $task);
                $feedback_level = intval($taskTypeMessageConfig[0]);
            }
            
            $messages = $this->getFeedback($feedback_level, $task, $overtask, $current_error);
            
            if (count($messages['message_parts']) === 0) {
                $messages = [
                    "message_parts" => [
                        $current_error->constraint_message
                    ],
                    "highlighted" => null,
                    "feedback_level" => $feedback_level,
                ];
            }
        }
        return $messages;
    }
    public function nRand($errorModel)
    {
        $x = mt_rand() / mt_getrandmax();
        $y = mt_rand() / mt_getrandmax();
        return sqrt(-2 * log($x)) * cos(2 * pi() * $y) * $errorModel['std'] + $errorModel['mean'];
    }
    
    public function getFeedback ($feedback_level, $task, $overtask, $current_error)
    {
        $messages_keys = self::SIMULATION_PARAMETERS['messages'][$feedback_level]['message'];
        $messages = [ "message_parts" => [], "highlighted" => null, "feedback_level" => $feedback_level ];
        foreach ($messages_keys as $key) {
            switch ($key) {
                case 'constraintError':
                    $messages['message_parts'][] = $current_error->constraint_message;
                    break;
                case 'theoreticalTask':
                    $messages['message_parts'][] = $this->getUrl($current_error->theoretical_feedback);
                    break;
                case 'technologicalOvertask':
                    $messages['message_parts'][] = $overtask?$overtask->technological_feedback:'';
                    break;
                case 'technologicalTask':
                    $messages['message_parts'][] = $task->technological_feedback;
                    break;
                case 'technicalTask':
                    $messages['message_parts'][] = preg_replace_callback(
                        "|<parameter>([a-zA-Z0-9]*)</parameter>|",
                        function ($matches) {
                            return $this->replaceParameter($matches);
                        },
                        $current_error->technical_feedback);
                    break;
                case 'correctionTask':
                    $messages['message_parts'][] = preg_replace_callback(
                        "|<parameter>([a-zA-Z0-9]*)</parameter>|",
                        function ($matches) {
                            return $this->replaceParameter($matches);
                        },
                        $current_error->correction_feedback);
                    break;
                case 'highlight':
                    $messages["highlighted"] = ["simulationVariables" => $current_error['simulationVariables'], "nodesId" => $current_error['nodesId']];
                    break;
                    
            }
        }
        return $messages;
    }
    
    public static function _updateSimulationTableInBd($simulationData, $simulationCode)
    {
        // Update global knowledge
        $globalKnowledges = $simulationData['globalKnowledges'];
        $globalKnowledgesIds = [];
        foreach ($globalKnowledges as $globalKnowledge) {
            $updatedGlobalKnowledge = \App\SimulationGlobalKnowledge::updateOrCreate(
                [
                    'id_global_knowledge' => $globalKnowledge['id_global_knowledge'], 'simulation_code' => $simulationCode,
                ],[
                'description' => $globalKnowledge['description'], 'position' => intval($globalKnowledge['position'])*10+10
            ]);
            $globalKnowledgesIds[] = $updatedGlobalKnowledge->id_global_knowledge;
        }
        SimulationGlobalKnowledge::whereNotIn('id_global_knowledge', $globalKnowledgesIds)->where('simulation_code', $simulationCode)->delete();

        // Update taskType and get id
        $taskTypes = $simulationData['taskTypes'];
        $taskTypeIds = [];
        foreach ($taskTypes as $taskType) {
            $globalKnowledgeId = $globalKnowledgesIds[intval($taskType['globalKnowledgeIndex'])] ?? null;
            $updatedTaskType = SimulationTaskType::updateOrCreate(
                [
                    'id_task_type' => $taskType['id_task_type'], 'simulation_code' => $simulationCode,
                ],[
                   'id_overtask_type' => null, 'id_global_knowledge' => $globalKnowledgeId, 'description' => $taskType['description'], 'position' => intval($taskType['position'])*10+10,
                    'learning_goal' => intval($taskType['learningGoal']), 'technological_feedback' => $taskType['technological']
            ]);
            $taskTypeIds[] = $updatedTaskType->id_task_type;
        }
        SimulationTaskType::whereNotIn('id_task_type', $taskTypeIds)->where('simulation_code', $simulationCode)->delete();

        // Update constraint and get id
        $constraints = $simulationData['constraints'];
        $constraintIds = [];
        foreach ($constraints as $index => $constraint) {
            $taskTypeId = $taskTypeIds[intval($constraint['taskTypeIndex'])] ?? null;
            $updatedConstraint = SimulationConstraint::updateOrCreate([
                'id_constraint' => $constraint['id_constraint'],
                'simulation_code' => $simulationCode,
            ], [
                'id_task_type' => $taskTypeId,
                'description' => $constraint['description'], 'position' => $constraint['position']*10+10,
                'satisfaction_condition_key' => $constraint['key'],'constraint_message' => $constraint['message'],
                'simulation_enabled' => $constraint['simulationEnabled'],
                'theoretical_feedback' => $constraint['theoretical'], 'technical_feedback' => $constraint['technical'], 'correction_feedback' => $constraint['correction']
            ]);
            $constraintIds[] = $updatedConstraint->id_constraint;
        }
        SimulationConstraint::whereNotIn('id_constraint', $constraintIds)->where('simulation_code', $simulationCode)->delete();
        
        \Schema::disableForeignKeyConstraints();
        // Update relevant constraints
        foreach ($constraints as $index => $constraint) {
            $constraintId = $constraintIds[$index] ?? null;
            if($constraintId) {
                $newRelevanceIds = [];
                foreach ($constraint['relevanceConstraintIndexes'] as $relevanceIndex) {
                    $relevanceId = $constraintIds[$relevanceIndex];
                    if($relevanceId) {
                        $newRelevanceIds[] = $relevanceId;
                        DB::table('simulation_link_relevance_constraint')->updateOrInsert([
                            'simulation_code' => $simulationCode,
                            'id_constraint' => $constraintId,
                            'id_relevance_constraint' => $relevanceId,
                        ]);
                    }
                }
                // delete old relevance link
                DB::table('simulation_link_relevance_constraint')
                    ->where('id_constraint','=',$constraintId)
                    ->whereNotIn('id_relevance_constraint', $newRelevanceIds)
                    ->delete();
            }
        }
        \Schema::enableForeignKeyConstraints();
    }
    
    public static function _getSimulationTableInJson($simulationCode) 
    {
        $simulationData = [];
        $simulationData['globalKnowledges'] = SimulationGlobalKnowledge::where('simulation_code',$simulationCode)
            ->orderBy('position')
            ->get(['id_global_knowledge',
                'description',
                'position']);
        $simulationData['taskTypes'] = SimulationTaskType::where('simulation_code', $simulationCode)
            ->orderBy('position')
            ->get(['id_task_type', 'description', 'learning_goal as learningGoal', 'position',
                'technological_feedback as technological', 'id_global_knowledge']);
        $query = SimulationConstraint::where('simulation_constraint.simulation_code', $simulationCode)
            ->leftJoin('simulation_link_relevance_constraint','simulation_link_relevance_constraint.id_constraint','=','simulation_constraint.id_constraint')
            ->select('simulation_constraint.id_constraint',
                'simulation_constraint.description',
                'simulation_constraint.constraint_message as message',
                'simulation_constraint.satisfaction_condition_key as key',
                'simulation_constraint.simulation_enabled as simulationEnabled',
                'simulation_constraint.id_task_type',
                'simulation_constraint.theoretical_feedback as theoretical',
                'simulation_constraint.technical_feedback as technical',
                'simulation_constraint.correction_feedback as correction',
                DB::raw('group_concat(simulation_link_relevance_constraint.id_relevance_constraint)  as relevanceConstraintIds'))
            ->groupByRaw('simulation_constraint.id_constraint, simulation_constraint.description, simulation_constraint.constraint_message, 
            simulation_constraint.satisfaction_condition_key, simulation_constraint.simulation_enabled, simulation_constraint.id_task_type,
            simulation_link_relevance_constraint.id_constraint')
            ->orderBy('simulation_constraint.position');
        $simulationData['constraints'] = $query->get();

        return $simulationData;
    }
    
    public function overLoadLearningGoalConfig ($constraint, $taskType)
    {
        $emptyTheoreticalFeedback = $constraint->theoretical_feedback === "";
        $emptyTechnologicalFeedback = $taskType->technological_feedback === "";
        $emptyTechnicalFeedback = $constraint->technical_feedback === "";
        $learningGoalArray = collect($this->learningGoalsConfig[$taskType->learning_goal]);
        if ($emptyTheoreticalFeedback) {
            $learningGoalArray = $learningGoalArray->filter(fn($index) => $index !== 1);
        }
        if ($emptyTechnologicalFeedback) {
            $learningGoalArray = $learningGoalArray->filter(fn($index) => $index !== 2);
        }
        if ($emptyTechnicalFeedback) {
            $learningGoalArray = $learningGoalArray->filter(fn($index) => $index !== 3);
        }
        return array_values($learningGoalArray->toArray());
    }
    
    public static function _getReportSimulationData($simulationCode, $idReport)
    {
        $progression = null;
        $queryProgression = DB::table('simulation_trace')
            ->where('simulation_code', $simulationCode)
            ->where('id_report', $idReport)
            ->where('trace_action','student_progression');
        $lastProgression = null;
        if($queryProgression->count()>0) {
            $simulationTraceIds = $queryProgression->pluck('id_simulation_trace');
            $lastProgression = DB::table('simulation_trace')->whereIn('id_simulation_trace', $simulationTraceIds)
                ->latest('id_simulation_trace')->first();
        }
        $queryCorrectAnswer = DB::table('simulation_trace as st')
            ->where('simulation_code', $simulationCode)
            ->where('id_report', $idReport)
            ->where('trace_action','student_correct_answer');
        $isCorrectAnswer = $queryCorrectAnswer->count()>0;
        if($lastProgression){
            $progression = floatval(json_decode($lastProgression->attributes)->progression) * 0.9;
            $progression = $isCorrectAnswer?$progression+0.1:$progression;
            $progression = round($progression*100,2);
        }
        return ['progression' => $progression];
    }
    
    public static function _getReportDashboardData($simulationCode, $report)
    {
        $dashboardData = [];
        // Every learner have the same knowledge so we take the first one
        $learner = $report->users->first();
        $dashboardData['globalKnowledges'] = Simulation::getGlobalKnowledges($learner, $simulationCode, false);
        return $dashboardData;
    }
    public static function _getClassDashboardData($mission, $class)
    {
        $dashboardData = [];
        $reports = $class->reports()->where('team_config.id_mission', $mission->id_mission)->get();
        $learnerClassIds = $reports->flatMap(function ($report) {
            return $report->users->pluck('id_user')->toArray();
        });
        if($reports->count() > 0){
            $averageKnowledges = collect();
            foreach ($reports as $report) {
                // Every learner have the same knowledge so we take the first one
                $learner = $report->users->first();
                $reportKnowledge = Simulation::getGlobalKnowledges($learner, $mission->simulation_code, false);
                foreach ($reportKnowledge as $knowledge) {
                    $avgK = $averageKnowledges->first(fn($k) => $k->id_global_knowledge == $knowledge->id_global_knowledge);
                    if($avgK){
                        if($knowledge->knowledge !== null) {
                            $avgK->sumKnowledge += $knowledge->knowledge;
                            $avgK->counterKnowledge += 1;
                        }
                    } else {
                        if($knowledge->knowledge !== null) {
                            $knowledge->sumKnowledge = $knowledge->knowledge;
                            $knowledge->counterKnowledge = 1;
                        }
                        $knowledge->averageKnowledgeByTaskType = $knowledge->taskTypes()
                            ->leftJoin('simulation_link_learner_task as l', 'l.id_task_type', '=', 'simulation_task_type.id_task_type')
                            ->whereIn('l.id_learner', $learnerClassIds)
                            ->select('simulation_task_type.id_task_type', 'simulation_task_type.description as description', 'simulation_task_type.learning_goal', DB::raw('AVG(knowledge) as tt_average_knowledge'))
                            ->groupBy('simulation_task_type.id_task_type')
                            ->get();
                        // add the unknown task type
                        $knowledge->averageKnowledgeByTaskType = $knowledge->averageKnowledgeByTaskType->union($knowledge->taskTypes);
                        $averageKnowledges->push($knowledge);
                    }
                }
            }
            $averageKnowledges = $averageKnowledges->map(function ($knowledge) use ($reports){
                if($knowledge->counterKnowledge !== null){
                    $knowledge->averageKnowledge = $knowledge->sumKnowledge / $knowledge->counterKnowledge;
                }
                $knowledge->color = $knowledge->averageKnowledge===null?'lightgrey'
                    :'hsl('.($knowledge->averageKnowledge * 120).','.(5+90*abs(0.5-$knowledge->averageKnowledge)).'%,50%)';
                return $knowledge;
            });
            $dashboardData['averageKnowledges'] = $averageKnowledges;
            
            $dashboardData['frequentConstraints'] = collect();
            if(count($averageKnowledges) > 0){
                $constraints = SimulationConstraint::where('simulation_constraint.simulation_code', $mission->simulation_code)
                    ->join('simulation_task_type as tt', 'tt.id_task_type', '=', 'simulation_constraint.id_task_type')
                    ->whereNot('tt.learning_goal', 0)
                    ->get();
                $constraintsStateTraces = DB::table('simulation_trace as st')
                    ->where('simulation_code', $mission->simulation_code)
                    ->whereIn('id_report', $reports->pluck('id_report'))
                    ->where('trace_action','student_constraints_state')
                    ->get();
                foreach ($constraintsStateTraces as $constraintsStateTrace) {
                    $errors = collect(json_decode($constraintsStateTrace->attributes))->filter(fn($c) => $c->current_state === 0);
                    foreach ($errors as $error) {
                        $relatedC = $constraints->first(fn($c) => $c->id_constraint == $error->id_constraint);
                        if($relatedC){
                            $relatedC->frequency += 1;
                        }
                    }
                }
                $constraints = $constraints->whereNotNull('frequency')->sortByDesc('frequency')->slice(0,6);
                $dashboardData['frequentConstraints'] = $constraints;
            }
        }
        return $dashboardData;
    }
    
    public static function _getSynchronizeData( $labdoc, $simulationCode ) {
        // Every learner have the same knowledge so we take the first one
        $learner = $labdoc->report->users->first();
        $synchronizeData = [];
        $lastProgression = DB::table('simulation_trace as st')
            ->where('simulation_code', $simulationCode)
            ->where('id_user', $learner->id_user)
            ->where('trace_action','student_progression')
            ->latest('id_simulation_trace')->first();
        $synchronizeData['progression'] = $lastProgression?floatval(json_decode($lastProgression->attributes)->progression):0;
        $lastConstraintsState = DB::table('simulation_trace as st')
            ->where('simulation_code', $simulationCode)
            ->where('id_user', $learner->id_user)
            ->where('trace_action','student_constraints_state')
            ->latest('id_simulation_trace')->first();
        $synchronizeData['failedConstraints'] = [];
        if($lastConstraintsState){
            $constraintsState = collect(json_decode($lastConstraintsState->attributes));
            // We get the failed constraints not related to a learning goal 0
            $synchronizeData['failedConstraints'] = SimulationConstraint::whereIn('id_constraint', 
                $constraintsState->filter(fn($c) => $c->current_state === 0)->pluck('id_constraint')->toArray())
                ->join('simulation_task_type as tt', 'tt.id_task_type', '=', 'simulation_constraint.id_task_type')
                ->whereNot('tt.learning_goal', 0)
                ->get();
        }
        $globalKnowledges = Simulation::getGlobalKnowledges($learner, $simulationCode, false);
        foreach ($globalKnowledges as $globalKnowledge) {
            $globalKnowledge->relatedTaskTypesKnowledge = collect();
            foreach ($globalKnowledge->taskTypes as $taskType) {
                $learner_task = $taskType->learners->find(['simulation_code' => $simulationCode, 'id_learner' => $learner->id_user])->first();
                $globalKnowledge->relatedTaskTypesKnowledge->push(['id_task_type' => $taskType->id_task_type,
                    'description' => $taskType->description,
                    'knowledge' => $learner_task?$learner_task->pivot->knowledge:null]);
            }
        }
        $synchronizeData['globalKnowledges'] = $globalKnowledges;
        return $synchronizeData;
    }
}
