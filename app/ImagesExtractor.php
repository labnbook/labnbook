<?php

namespace App;

use Illuminate\Support\Facades\DB;

class ImagesExtractor
{
    private $idMission;
    private $idLabdoc;

    private $count = 0;
    private $files = [];
    private $content;
    private $rootPath;
    private $directory;

    /**
     * @param string $directory Path relative to `storage/app/`, e.g. `mission/1/ld_img`
     * @param string $contents
     */
    public function __construct($idMission, $idLabdoc, $contents = '')
    {
        $this->idMission = (int) $idMission;
        $this->idLabdoc = (int) $idLabdoc;
        $this->rootPath = storage_path('app/public/');
        $this->directory = $this->getDirectory();
        if ($contents) {
            $this->extractImages($contents);
        }
    }

    /**
     * Number of images extracted to files.
     *
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * The HTML|XML|JSON after replacing the base64 encoded images.
     *
     * @return int
     */
    public function getContent()
    {
        if ($this->count === 0) {
            throw new \Exception("No HTML/XML/JSON change.");
        }
        return $this->content;
    }

    /**
     * @param string $data XML or HTML
     * @return boolean True if images were extracted
     * @throws Exception
     */
    public function extractImages($data)
    {
        $this->count = 0;
        $this->files = [];
        $this->content = '';
        
        if (strncmp($data, 'zwibbler3.', 10) === 0) {
            // Zwibbler doc
            $dom = new ZwibblerDOM($data);
            $attribute = "url";
            $tag = "ImageNode";
            $xmlFormat = true;
        } else {
            libxml_use_internal_errors(true);
            $attribute = "src";
            $tag = "img";
            if (strncmp($data, '<?xml', 5) === 0) {
                $dom = new \DOMDocument();
                if (!$dom->loadXML($data)) {
                    throw new \Exception(__("Impossible de charger ce XML."));
                }
                $xmlFormat = true;
            } else {
                $dom = new \DOMDocument();
                if (!$dom->loadHTML(
                    '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head><body><div id="html-wrapper">'
                    . $data
                    . "</div></body></html>",
                    LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD)
                ) {
                    throw new \Exception(__("Impossible de charger ce HTML."));
                }
                $xmlFormat = false;
            }
        }
        $imgs = $dom->getElementsByTagName($tag);
        if ($imgs->length === 0) {
            return false;
        }
        foreach ($imgs as $img) {
            /* @var $img DOMElement */
            $src = $img->getAttribute($attribute);
            if (strncmp($src, "data:", 5) === 0) {
                $filename = self::decodeToFile($src);
                if ($filename) {
                    $this->count++;
                    $this->files[] = $filename;
                    // Path can created non desired diff when validating labdoc. Saved image in LD get the path "../storage/..." instead "/storage".
                    $img->setAttribute($attribute, "/storage/$filename");
                }
            }
        }
        if ($this->count) {
            $dom->formatOutput = false;
            $dom->preserveWhiteSpace = false;
            if ($xmlFormat) {
                $this->content = $dom->saveXML();
            } else {
                $wrapper = $dom->saveHTML($dom->getElementById("html-wrapper"));
                $this->content = substr(
                    $wrapper,
                    23, // length of <div id="html-wrapper">
                    strlen($wrapper) - 23 - 6 // length of </div>
                );
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * The path relative to storage/app/ where to save these images.
     *
     * @return string
     */
    private function getDirectory()
    {
        return sprintf("missions/%d/ld_img", $this->idMission);
    }

    /**
     * @param string $data The value of a "src" attribute
     * @return string Full path under `storage/app/`, e.g. `missions/1/ld_img/xyz.png`
     * @throws Exception
     */
    private function decodeToFile($data)
    {
        $start = strtolower(substr($data, 0, 22));
        if ($start === "data:image/png;base64,") {
            $base64 = substr($data, 22);
            $extension = ".png";
        } else if ($start === "data:image/jpg;base64,") {
            $base64 = substr($data, 22);
            $extension = ".jpg";
        } else if ($start === "data:image/jpeg;base64") {
            $base64 = substr($data, 23);
            $extension = ".jpg";
        } else if ($start === "data:image/pdf;base64,") {
            $base64 = substr($data, 22);
            $extension = ".pdf";
        } else if ($start === "data:image/bmp;base64,") {
            $base64 = substr($data, 22);
            $extension = ".bmp";
        } else {
            return null;
        }
        $raw = base64_decode($base64, true);
        if ($raw === false) {
            throw new \Exception(__("Le décodage a échoué. Données corrompues ?"));
        }
        return $this->saveToFile($raw, $extension);
    }

    /**
     * @param string $raw
     * @param string $extension
     * @return string
     * @throws Exception
     */
    private function saveToFile($raw, $extension)
    {
        $directory = "{$this->rootPath}/{$this->directory}";
        if (!is_dir($directory)) {
            mkdir($directory, 0777, true);
        }
        if (!is_writable($directory)) {
            throw new \Exception(__("Le répertoire :directory ne permet pas l'écriture."), ['directory'=>$directory]);
        }
        $hash = sha1($raw);
        $filename = $hash . $extension;
        if (!file_exists("$directory/$filename")) {
            if (file_put_contents("$directory/$filename", $raw) === false) {
                throw new \Exception(__("Erreur en écrivant le fichier"). " $directory /$filename.");
            }
        }
        DB::insert(
            DB::raw(
                "INSERT IGNORE INTO labdocfile VALUES (UNHEX(:hash), :id_ld, :id_m, :time)",
            ),
            [
                ':hash' => "'$hash'",
                ':id_ld' => (int)$this->idLabdoc,
                ':id_m' => (int)$this->idMission,
                ':time' => time(),
            ]
        );
        return "{$this->directory}/$filename";
    }
}
