<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;

class MarkdownMail extends Mailable
{
    public $data;
    public $to;
    public $subject;
    public $viewName;

    /**
     * Create a new message instance.
     *
     * @param string $viewName the view used to render the mail
     * @param  string $subject
     * @param array $data data available in view
     *
     * @return void
     */
    public function __construct($viewName, $subject, $data)
    {
        $this->viewName = $viewName;
        $this->subject = $subject;
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->subject)->markdown($this->viewName);
    }
}
