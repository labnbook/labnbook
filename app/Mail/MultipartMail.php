<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;

class MultipartMail extends Mailable
{
    public $data;
    public $to;
    public $subject;
    public $viewName;
    public $textViewName;

    public function __construct($viewName, $textViewName, $subject, $data)
    {
        $this->viewName = $viewName;
        $this->subject = $subject;
        $this->data = $data;
        $this->textViewName = $textViewName;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->subject)->view($this->viewName)->text($textViewName);
    }
}
