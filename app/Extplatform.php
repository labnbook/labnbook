<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

/**
 * App\Extplatform
 *
 * @property int $id_extplatform
 * @property int|null $id_inst
 * @property string|null $name
 * @property string|null $secret
 * @property string|null $url
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Extplatform whereIdExtplatform($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Extplatform whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Extplatform whereSecret($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Extplatform whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Classe newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Classe newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Classe query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Classe whereClassCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Classe whereClassName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Classe whereCreationTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Classe whereExtCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Classe whereIdClass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Classe whereIdInst($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Classe whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Classe whereUpdateTime($value)
 * @mixin \Eloquent
 * @property-read \App\Institution $institution
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $teachers
 * @property-read int|null $teachers_count
 */
class Extplatform extends Model
{
    protected $table = 'extplatform';
    protected $primaryKey = 'id_extplatform';
    public $timestamps = false;
    protected $fillable = ['name', 'url', 'id_inst', 'secret', 'plugin_version','platform_version', 'contacts'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'secret',
    ];

    /**
     * Returns the users of the external platform
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\User', 'link_extplatform_user', 'id_extplatform', 'id_user');
    }

    /**
     * Returns the teachers of the external platform
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function teachers()
    {
        return $this->users()->whereRaw('user.id_user IN (SELECT id_teacher FROM teacher)');
    }

    /**
     * Returns the institution of the external platform
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function institution()
    {
        return $this->BelongsTo('App\Institution', 'id_inst');
    }

    /**
     * Returns true if $id_user is part of the external platform
     * @param int $id_user
     * @return boolean
     */
    public function hasUser($id_user)
    {
        return $this->users()->find($id_user) != null;
    }
}
