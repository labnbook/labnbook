<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

/**
 * Save, load and handle the Labdoc history of versions.
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 */
class LabdocVersioning
{
    private $path;

    public function __construct($missionId)
    {
        $this->path = storage_path(env('VERSIONING_STORAGE_PATH', "app/versioning")) . '/' . (int) $missionId;

        $this->checkDirectory($this->path);
    }

    /**
     * Returns the versioning directory path
     * @return string
     */
    public function getVerioningPath()
    {
        return $this->path;
    }

    /**
     * Return an object, or null if there is no history.
     *
     * Example of a returned value:
     * <code><pre>
     * {
     *	id_labdoc: X,
     *	id_report: Y, // éventuellement NULL
     *	contents: [
     *	  {
     *		id_trace: null, // NULL pour le premier
     *		id_user: ID-de-l-enseignant, // éventuellement NULL
     *		ts: TIME1,
     *		name: "...",
     *		data: "...",
     *	  },
     *	  {
     *		id_trace: A, // non NULL
     *		id_user: B, // non NULL
     *		ts: TIME2,
     *		name: "...",
     *		data: "...",
     *	  },
     *	]
     * }
     *</pre></code>
     *
     * @param int $labdocId
     * @return ?object
     * @throws \Exception
     */
    public function load($labdocId)
    {
        $filename = $this->getFileName($labdocId);
        if (!file_exists($filename)) {
            return null;
        }
        if (!is_readable($filename)) {
            throw new \Exception("Error, could not read '$filename'");
        }
        $content = file_get_contents($filename);
        return json_decode(gzdecode($content));
    }

    /**
     * Add a new labdoc version to the history of this labdoc.
     *
     * @param object $labdoc Record of the new version to add to the labdoc history.
     * @param ?int $traceId
     * @param ?int $userId
     */
    public function save($labdoc, $traceId, $userId)
    {
        if (empty($labdoc->id_labdoc)) {
            throw new \Exception("Cannot save a labdoc history without its ID.");
        }
        $versions = $this->load($labdoc->id_labdoc);
        $labdoc_data = $this->cleanUp($labdoc);
        if ($versions) {
            if (empty($traceId) || empty($userId)) {
                throw new \Exception("An edited labdoc must have an updating trace and an editing user (ld: {$labdoc->id_labdoc}, trace: {$traceId}, user {$userId}).");
            }
            $versions->contents[] = [
                'id_trace' => (int) $traceId,
                'id_user' => (int) $userId,
                'ts' => Carbon::now()->timestamp,
                'name' => $labdoc->name,
                'data' => $labdoc_data,
            ];
        } else {
            $versions = [
                'id_labdoc' => (int) $labdoc->id_labdoc,
                'id_report' => (int) $labdoc->id_report,
                'contents' => [
                    [
                        'id_trace' => (int) $traceId,
                        'id_user' => (int) $userId,
                        'ts' => Carbon::now()->timestamp,
                        'name' => $labdoc->name,
                        'data' => $labdoc_data,
                    ],
                ],
            ];
        }
        $this->write($labdoc->id_labdoc, $versions);
    }

    /**
     * Find the data associated with of a versioning file.
     * @param int $labdocId
     * @param int $versionId = traceId
     * @return string: the corresponding data
     */
    public function getData($labdocId, $versionId)
    {
        try {
            $contents = $this->load($labdocId)->contents;
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return '';
        }
        return $contents[array_search(
            $versionId,
            array_column(
                json_decode(json_encode($contents), True),
                'id_trace'
            )
        )];
    }
    
    private function getFileName($labdocId)
    {
        return $this->path . sprintf('/%011d.json.gz', (int) $labdocId);
    }

    private function write($labdocId, $data)
    {
        $filename = $this->getFileName($labdocId);
        if (file_put_contents($filename, gzencode(json_encode($data))) === false) {
            throw new \Exception("Error while writing '$filename'");
        }
    }

    private static function checkDirectory($path)
    {
        if (!is_dir($path)) {
            mkdir($path, 0775, true);
        }
        if (!is_writable($path)) {
            throw new \Exception("Could not write in directory '$path'.");
        }
    }

    /**
     * Retrieves last labdoc version reviewed in archives.
     * Assuming last version == max id_trace (sometimes False when user restores a version)
     * @param int $labdocId
     * @return int: the corresponding version id or 0
     */
    public function lastVersionIdInArchives($labdocId)
    {
        try {
            $contents = $this->load($labdocId)->contents;
        } catch (\Exception $e) {
            Log::error($e->getMessage()." labdoc id {$labdocId}");
            // No version found in archives
            return 0;
        }
        return max(
            array_map(
                fn($e) => $e->id_trace, 
                $contents
            )
        );
    }

    /**
     * Retrieves last labdoc content version stored in archives.
     * @param int $labdocId
     * @return string: the last stored labdoc data
     */
    public function lastVersionContentData($labdocId)
    {
        try {
            $contents = $this->load($labdocId)->contents;
        } catch (\Exception $e) {
            Log::error($e->getMessage()." labdoc id {$labdocId}");
            // No version found in archives
            return null;
        }
        return collect($contents)->sortBy('ts')->last()->data;
    }

    /**
     * Repace $id_user_source by $id_user_target on the LD versionning
     * for labdoc $id_labdoc
     * @param int $id_labdoc
     * @param int $id_user_source
     * @param int $id_user_target
     * @param boolean $dryRun
     */
    public function updateUser($id_labdoc, $id_user_source, $id_user_target, $dryRun)
    {
        $versions = $this->load($id_labdoc);
        if (!$versions) {
            // Versions are generated on validateLD from students so, Labdoc
            // that have been only edited by teacher or that have never been edited
            // does not have version, so this is not an error case.
            return;
        }
        foreach ($versions->contents as $v) {
            if ($v->id_user && $v->id_user == $id_user_source) {
                $v->id_user = $id_user_target;
            }
        }
        if (!$dryRun) {
            $this->write($id_labdoc, $versions);
        }
    }

    /**
     * @param object $labdoc
     * @return false|string the data to be versioned
     */
    public function cleanUp($labdoc) {
        if ($labdoc->type_labdoc === "code") {
            // If type_labdoc is code, do not save outputs
            $data = json_decode($labdoc->labdoc_data, true);
            foreach ($data['cells'] as $key => $value) {
                $data['cells'][$key]['outputs'] = [];
            }
            return json_encode($data);
        } else {
            return $labdoc->labdoc_data;
        }
    }

}
