<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * App\Conversation
 *
 * @property int $id_conversation
 * @property int|null $id_report
 * @property string $title
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Message[] $messages
 * @property-read int|null $messages_count
 * @property-read \App\Report|null $report
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Conversation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Conversation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Conversation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Conversation whereIdConversation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Conversation whereIdReport($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Conversation whereTitle($value)
 * @mixin \Eloquent
 */
class Conversation extends Model
{
    protected $table = 'conversation';
    protected $primaryKey = 'id_conversation';
    protected $fillable = ['id_report', 'title'];
    public $timestamps = false;

    /**
     * Returns the report of the conversation
     * @return \App\Report
     */
    public function report()
    {
        return $this->belongsTo('App\Report', 'id_report');
    }
 
    /**
     * Returns the users of the conversation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\User', 'link_conversation_user', 'id_conversation', 'id_user');
    }

    /**
     * Returns the messages of the conversation
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function messages()
    {
        return $this->hasMany('App\Message', 'id_conversation');
    }

    /**
     * Returns the users participating to conversation
     * Ordered by name and excluding the given user
     * @param int id_user
     * @return \Illuminate\Database\Eloquent\Collection|\App\User[]
     */
    public function getParticipants($id_user)
    {
        return $this->users()
                    ->whereKeyNot($id_user)
                    ->orderBy('user_name', 'ASC')
                    ->get();
    }

    /**
     * Initialize a new conversation from $user
     * @param \App\User $user
     * @param int $id_report
     * @return \App\Conversation
     */
    public static function init($user, $id_report)
    {
        $name = $user->first_name." ".$user->user_name;
        // new conversation
        $conv = self::create([
            'id_report' => $id_report != 0 ? $id_report : null,
        ]);
        // add the user as participant of the conversation
        DB::table('link_conversation_user')
            ->insert([
                'id_conversation' => $conv->id_conversation,
                'id_user' => $user->id_user,
            ]);
        // first msg : mandatory for the conversation to be retrieved
        DB::table('message')
            ->insert([
                'id_conversation' => $conv->id_conversation,
                'msg_content' => __("Conversation créée par")." $name"
            ]);
        return $conv;
    }

    /**
     * Returns the mission tag to show in conversation title
     * @return string
     */
    public function missionTag()
    {
        if ($this->report && $this->report->mission) {
            return  $this->report->mission->code;
        }
        return "";
    }

    /**
     * Send welcome message to users on the conversation
     * @param \Illuminate\Database\Eloquent\Collection $users
     */
    public function welcomeUsers($users)
    {
        $inserts = [];
        foreach ($users as $user) {
            $inserts[] = [
                'msg_content' => "{$user->first_name} {$user->user_name} a été invité(e) dans la conversation",
                'id_conversation' => $this->id_conversation,
            ];
        }
        DB::table('message')->insert($inserts);
    }

    /**
     * Add users to the conversation
     * @param \Illuminate\Database\Eloquent\Collection $users
     */
    public function addUsers($users)
    {
        $this->users()->attach($users);
    }

    /**
     * Mark the conversation as seen by user now
     * @param int $id_user
     */
    public function updateLastSeen($id_user)
    {
        DB::Update(
            DB::raw(
                "UPDATE link_conversation_user SET last_seen = NOW() "
                ." WHERE id_user = :id_user "
                ." AND id_conversation = :id_conv "
            ),
            [':id_user' => $id_user, ':id_conv' => $this->id_conversation ]
        );
    }

    /**
     * @param string $sql query sql with joins
     * @param array $params params for sql
     * @param Illuminate\Database\Eloquent\Collection|null $participants participants already included that should not be added in list
     * @return mysq_result
     */
    public static function selectParticipants($sql, $params, $participants)
    {
        $select_sql = "SELECT u.id_user, CONCAT(u.user_name, ' ', u.first_name) as name, ";
        $select_sql .= \App\User::connectedSqlString(false, true)." as connected";
        $select_sql .= " FROM user as u ";

        // Exclude participants already included
        $ids = $participants ? $participants->modelKeys() : [];
        // Exclude us
        $ids[] = (int)Auth::id();
        $end_sql = " AND u.id_user NOT IN (".implode(',', $ids).")";

        $end_sql .= " GROUP BY u.id_user ORDER BY u.user_name ASC";
        return \App\User::hydrate(DB::select(DB::raw($select_sql.$sql.$end_sql), $params));
    }

    /**
     * Remove the conversations and everythin DB links
     * @param \App\User $user
     */
    public function remove($user)
    {
        // supprimer son propre lien à la conv
        DB::table('link_conversation_user')
            ->where('id_conversation', $this->id_conversation)
            ->where('id_user', $user->id_user)
            ->delete();
        if ($this->users->isEmpty()) {
            // si il n'y a pas d'autre participants, supprimer la conversation ainsi que tous les messages et leurs LD attachés
            DB::table('message')
                ->where('id_conversation', $this->id_conversation)
                ->delete();
            $this->delete();
            // TODO : are the attached LD autmatically deleted ??
        } else {
            DB::table('message')
                ->insert([
                    "msg_content" => "{$user->first_name} {$user->user_name} ".__("a quitté la conversation"),
                    "id_conversation" => $this->id_conversation,
                ]);
        }
    }

    /**
     * Adds a message to the conversation
     * @param int $id_user
     * @param string $txt
     * @param int id_ld
     */
    public function addMessage($id_user, $txt, $id_ld = null)
    {
        DB::table('message')->insert([
            'id_conversation' => $this->id_conversation,
            'id_user_sender' => $id_user,
            'msg_content' => nl2br(strip_tags($txt)),
            'id_labdoc' => $id_ld,
        ]);
    }

    /**
     * @param int $id_user
     * @return query result containing (id_conversation, msg_content)
     */
    public static function getConvWithNewMsg($id_user)
    {
        $query = "SELECT m.id_conversation, m.msg_content"
            ." FROM message m"
            ." INNER JOIN ("
            ." SELECT m2.id_conversation, max(m2.msg_time) max_t"
            ." FROM message m2 JOIN link_conversation_user lcu USING(id_conversation) "
            ." WHERE lcu.id_user = ? AND (lcu.last_seen IS NULL OR (m2.msg_time > lcu.last_seen AND m2.id_user_sender <> 1))"
            ." GROUP BY id_conversation"
            ." ) t ON t.id_conversation = m.id_conversation AND t.max_t = m.msg_time";
        return DB::select(DB::raw($query), [$id_user]);
    }

    /**
     * Returns the participant list formated as a string
     * @return string
     */
    public function displayParticipants()
    {
        $participants = "" ;
        $result = $this->getParticipants($this->id_conversation);
        foreach ($result as $user) {
            $participants .= substr($user->first_name, 0, 1) . ". " . $user->user_name . ", ";
        }
        $participants = substr($participants, 0, -2); // on enlève la virgule finale
        return htmlspecialchars($participants, ENT_QUOTES|ENT_SUBSTITUTE);
    }


    /**
     * Return a Collection of conversations ordered by last message and filtered
     * @param string $filter
     * @param int $id_report_context
     * @param \App\User $user
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public static function getConversationList($filter, $id_report_context, $user)
    {
        // Get the conversations with the current user
        $query = "SELECT MAX(msg_time), conv.title, conv.id_report, lcu.*"
            ." FROM link_conversation_user AS lcu"
            ." JOIN conversation conv USING (id_conversation)"
            ." JOIN message ON message.id_conversation = lcu.id_conversation"
            ." WHERE lcu.id_user = :id_user ";
        $params = [':id_user' => $user->id_user];
        if ($filter) {
            // on filtre les conversations sur le nom des protagonistes ou le contenu des messages
            $query .= <<<EOSQL
 AND lcu.id_conversation IN (
    SELECT id_conversation FROM message WHERE msg_content LIKE :filter
    UNION
    SELECT id_conversation FROM link_conversation_user INNER JOIN user WHERE link_conversation_user.id_user = user.id_user AND (user_name LIKE :filter OR first_name LIKE :filter)
    UNION
    SELECT id_conversation FROM conversation WHERE title LIKE :filter
    UNION
    SELECT id_conversation FROM conversation JOIN report ON report.id_report = conversation.id_report JOIN mission ON report.id_mission = mission.id_mission AND mission.code LIKE :filter
)
EOSQL;
            $params[':filter'] = "%$filter%";
        }
        $query .= " GROUP BY lcu.id_conversation, lcu.id_user, lcu.last_seen, conv.title, conv.id_report ";
        $query .= " ORDER BY MAX(message.msg_time) DESC" ;
        return Conversation::fromQuery(DB::raw($query), $params);
    }
}
