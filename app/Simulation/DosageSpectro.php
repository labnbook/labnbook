<?php

namespace App\Simulation;

use App\Helper;
use App\Labdoc;
use App\Simulation;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class DosageSpectro extends Simulation
{
    protected Collection $spectra;

    protected float $alpha;

    protected float $threshold;
    protected float $largeLambdaError;
    protected int $irrelevantConstraintsNumber;
    protected array $lambdaErrorModel;
    protected array $absorbanceErrorModel;
    protected array $largeLambdaErrorModel;
    protected array $learningGoalsGonfig;

    public function __construct(Collection $protocol, Collection $materialList, Collection $quantities)
    {
        $this->protocol = $protocol;
        $this->materialList = $materialList;
        $this->quantities = $quantities;
        $this->simulation_code = 'dosage-spectro';
        $this->spectra = collect(json_decode(file_get_contents(public_path('/tool_copex/v2_json/dosage-spectro/spectra.json'))));
        // Percentage of maximum absorbance at which Beer Lambert's law becomes non-linear
        $this->alpha = 0.8;
        // Percentage of maximum absorbance at which the concentration is still acceptable
        $this->threshold = 0.98;
        // Error model for absorbance computation
        $this->lambdaErrorModel = ['mean' => 0, 'std' => 0.75];
        $this->absorbanceErrorModel = ['mean' => 0, 'std' => 0.002];
        $this->largeLambdaErrorModel = ['mean' => 0, 'std' => 0.01];
        $this->largeLambdaError = 950;
        $this->buildDependencies();
        // Last level is duplicated
        $this->learningGoalsConfig = [
            0 => [5, 6],
            1 => [2, 3, 5, 6],
            2 => [1, 2, 3, 4, 5, 6],
        ];
        $this->irrelevantConstraintsNumber = 0;
    }

    public function getUrl($theoretical_feedback)
    {
        if (!$theoretical_feedback) {
            return "";
        }
        $data = explode(',', $theoretical_feedback);
        if(count($data) === 1) {
            $data[] = '1';
        }
        if ($data[0] == "colorants") {
            $filename = "colorants.pdf";
        } else if ($data[0] == "manipulations") {
            $filename = "manipulations.pdf";
        } else if ($data[0] == "spectro") {
            $filename = "spectro.pdf";
        } else if($data[0] == "dosage") {
            $filename = "dosage.pdf";
        } else {
            return "";
        }
        $url = "/tool_copex/v2_json/dosage-spectro/resources/$filename#page=$data[1]";

        return "<a class='lb-a' href='$url' target='_blank' rel='noopener noreferrer' 
            x-on:click.stop=\"proNS.simulation.traceSimulationUserAction('student_open_feedback_link', {url:'$url'})\">Lien vers le cours</a>";
    }

    public function replaceParameter($matches)
    {
        $stockSolution = $this->getMaterialsByProperty('shortName', 'stockSolution')->first();
        $spectro = $this->getMaterialsByProperty('shortName', 'spectrophotometer')->first();
        $graduatedFlask = $this->getMaterialsByProperty('shortName', 'graduatedFlask')->first();
        switch ($matches[1]) {
            case 'stockSolution':
                return $stockSolution['name'];
            case 'minAcceptableWaveLength':
                return floatval($this->getMaterialPropertyValue($stockSolution, 'minAcceptableSpectrumWaveLength'));
            case 'maxAcceptableWaveLength':
                return floatval($this->getMaterialPropertyValue($stockSolution, 'maxAcceptableSpectrumWaveLength'));
            case 'unknowConcentrationSolution':
                return $this->getMaterialsByProperty('known', [0])->first()['name'];
            case 'minAbsorbanceLinear':
                return floatval($this->getMaterialPropertyValue($spectro, 'UA')) * 5;
            case 'maxAbsorbanceLinear':
                return floatval($this->getMaterialPropertyValue($spectro, 'AmaxLinear'));
            case 'graduatedFlaskVolumeAndUnit':
                $volumeAndUnit = collect($graduatedFlask['properties'])->firstWhere('name', 'volume');
                $unit = collect($this->quantities->firstWhere('id', $volumeAndUnit['physicalQuantityId'])['unit'])
                    ->firstWhere('id', $volumeAndUnit['unitId'])['symbol'][Helper::selectLanguage()];
                return $volumeAndUnit['value'] . ' ' . $unit;
            case 'testTubeNumber':
                return intval($this->getMaterialsByProperty('shortName', 'testTube')->first()['quantity']);
        }
    }
    
    public function getSimulationData($brokenConstraints)
    {
        // We extract only the simulated 
        $brokenConstraintsNodeIds = $brokenConstraints->pluck('nodesId')->flatten()->unique()->toArray();
        $firstBlockingNode = $this->protocol->first(fn($a) => in_array($a['nodeId'], $brokenConstraintsNodeIds));
        $index = $firstBlockingNode?$this->protocol->search(fn($a) => intval($a['nodeId']) === intval($firstBlockingNode['nodeId'])):$this->protocol->count();
        $slicedProtocol = $this->protocol->slice(0, $index);
        
        $simulatedActionNames = ['referenceSpectrumRecording', 'spectrumProduction', 'absorbanceMeasurement'];
        $simulatedActions = $slicedProtocol->filter(fn($a) => in_array($a['structuredAction'], $simulatedActionNames));

        $simulationData = [];
        $currentReference = null;
        foreach ($simulatedActions as $action) {
            switch ($action['structuredAction']) {
                case 'referenceSpectrumRecording':
                    $currentReference = $this->getMaterialPropertyValue($action['referenceSpectrumRecordingSolution'], 'shortName');
                    break;
                case 'spectrumProduction':
                    if($currentReference === 'distilledWater') 
                    {
                        $spectro = $this->getMaterialsByProperty('shortName', 'spectrophotometer')->first();
                        $simulationData[] = ['spectrum'=>['solutionName' => $action['spectrumProductionSolution']['name'],
                                                        'data' => $this->getSpectrumData($action, $spectro) ]];
                    }
                    break;
                case 'absorbanceMeasurement':
                    $spectrumKey = $this->getMaterialPropertyValue($action['absorbanceMeasurementSolution'], 'spectrumKey');
                    $concentration = $this->getMaterialPropertyValue($action['absorbanceMeasurementSolution'], 'concentration');
                    $lambda = floatval($action['absorbanceMeasurementWaveLength']);
                    $spectro = $this->getMaterialsByProperty('shortName', 'spectrophotometer')->first();
                    $simulationData[] = ['absorbance'=>
                        [ 
                            'solutionName' => $action['absorbanceMeasurementSolution']['name'], 
                            'absorbanceValue' => $this->measureAbsorbance($spectrumKey, $concentration, $lambda, $spectro), 
                            'lambdaValue' => $lambda,
                            'concentrationValue' => $concentration,
                            'stepSimulationVariable' => array_key_exists('stepSimulationVariable', $action)?$action['stepSimulationVariable']:''
                        ]
                    ];
                    break;
            }
        }
        // return null if no simulation data
        return count($simulationData)>0?$simulationData:null;
    }
    
    public function getSimulationTraceInfo($simulation_data) {
        $lastAbsorbanceAction = collect($simulation_data)->last(fn($a) => array_key_exists('absorbance', $a));
        $lastAbsorbanceValue = $lastAbsorbanceAction?number_format(floatval($lastAbsorbanceAction['absorbance']['absorbanceValue']), 3, '.', ""):null;
        return ['lastAbsorbanceMeasure'=>$lastAbsorbanceValue];
    }

    public function checkConstraints($variableList=null)
    {
        $error_msg = '';
        $referenceSolutionsStep = $this->getActionsInStep('preparationOfReferenceSolutions');
        $curveElaborationStep = $this->getActionsInStep('referenceCurveElaboration');
        $absorbanceMeasuresStep = $this->getActionsInStep('absorbanceMeasures');
        $unknownSolutionConcentrationStep = $this->getActionsInStep('E124ConcentrationComputation');
        $spectro = $this->getMaterialsByProperty('shortName', 'spectrophotometer')->first();
        $stockSolution = $this->getMaterialsByProperty('shortName', 'stockSolution')->first();
        
        // Find user min and max linear absorbance on all absorbance measurements
        $absorbanceMeasureActionsOnStockSolutionOrDilution = collect($absorbanceMeasuresStep)
            ->whereIn('structuredAction', ['absorbanceMeasurement'])
            ->filter(fn($sa) => $this->getMaterialPropertyValue($sa['absorbanceMeasurementSolution'], 'originShortName') === 'stockSolution'
                || $this->getMaterialPropertyValue($sa['absorbanceMeasurementSolution'], 'shortName') === 'stockSolution'
            );
        [$minLinearUserAbsorbance, $maxLinearUserAbsorbance] = $this->getMinAndMaxUserAbsorbance($absorbanceMeasureActionsOnStockSolutionOrDilution, $spectro);
        
        foreach ($this->constraints as $constraint) {
            $failed_relevance_condition = $this->constraintCheckingIsNotRelevant($constraint);
            if ($variableList) {
                $variables = $this->getConstraintsFromVariable($variableList)->all();
                $constraintIsCheckable = !$failed_relevance_condition && in_array($constraint->satisfaction_condition_key, $variables);
            } else {
                $constraintIsCheckable = !$failed_relevance_condition;
            }
            if ($constraintIsCheckable) {
                switch ($constraint->satisfaction_condition_key) {
                    case 'oneDilutionShouldExistAtStep1':
                        $constraint['current_state'] = +(collect($referenceSolutionsStep)
                                    ->whereIn('structuredAction', ['dilution'])
                                    ->count() > 0);
                        $constraint['simulationVariables'] = null;
                        $constraint['nodesId'] = null;
                        break;
                    case 'eachReferenceSolutionIsPreparedWithStockSolutionAtStep1':
                        if ($referenceSolutionsStep->count() > 0) {
                            $failedNode = collect($referenceSolutionsStep)
                                ->whereIn('structuredAction', ['dilution'])
                                ->first(fn($sa) => !array_key_exists('properties', $sa['dilutionBaseSolution']) || $this->getMaterialPropertyValue($sa['dilutionBaseSolution'], 'shortName') !== 'stockSolution');
                            $constraint['current_state'] = +(!$failedNode);
                            $constraint['simulationVariables'] = ['dilutionBaseSolution'];
                            if ($failedNode) {
                                $constraint['nodesId'] = [$failedNode['nodeId']];
                            }
                        } else {
                            $constraint['current_state'] = null;
                            $constraint['simulationVariables'] = null;
                            $constraint['nodesId'] = null;
                        }
                        break;
                    case 'AtLeast6ReferenceSolutionsAtStep1':
                        if ($referenceSolutionsStep->count() > 0) {
                            $constraint['current_state'] = +(collect($referenceSolutionsStep)
                                    ->whereIn('structuredAction', ['dilution'])
                                    ->count() >= 6);
                        } else {
                            $constraint['current_state'] = null;
                        }
                        $constraint['simulationVariables'] = null;
                        $constraint['nodesId'] = null;
                        break;
                    case 'smallestReferenceConcentrationShouldMatchSpectroAtStep1':
                        if ($referenceSolutionsStep->count() > 0) {
                            $smallestConcentrationAndNode = collect($referenceSolutionsStep)
                                ->whereIn('structuredAction', ['dilution'])
                                ->map(fn($sa) => ['concentration' => floatval($this->getMaterialPropertyValue($sa['dilutionSolutionCreated'], 'concentration')), 'nodeId' => $sa['nodeId']])
                                ->sortBy('concentration')
                                ->first();
                            $minAcceptableConcentration = (floatval($this->getMaterialPropertyValue($spectro, 'UA')) * 5) / floatval($this->getMaterialPropertyValue($stockSolution, 'epsilonApprox'));
                            $smallestConcentration = floatval($smallestConcentrationAndNode['concentration']);
                            $constraint['current_state'] = +($smallestConcentration >= $minAcceptableConcentration*0.8 && $smallestConcentration <= 2.2 * $minAcceptableConcentration);
                            $constraint['simulationVariables'] = ['dilutionBaseSolutionVolumeAndUnit'];
                            $constraint['nodesId'] = [$smallestConcentrationAndNode['nodeId']];
                        } else {
                            $constraint['current_state'] = null;
                            $constraint['simulationVariables'] = null;
                            $constraint['nodesId'] = null;
                        }
                        break;
                    case 'largestReferenceConcentrationShouldMatchSpectroAtStep1':
                        if ($referenceSolutionsStep->count() > 0) {
                            $largestConcentrationAndNode = collect($referenceSolutionsStep)
                                ->whereIn('structuredAction', ['dilution'])
                                ->map(fn($sa) => ['concentration' => floatval($this->getMaterialPropertyValue($sa['dilutionSolutionCreated'], 'concentration')), 'nodeId' => $sa['nodeId']])
                                ->sortByDesc('concentration')
                                ->first();
                            $maxAcceptableConcentration = floatval($this->getMaterialPropertyValue($spectro, 'AmaxLinear')) / floatval($this->getMaterialPropertyValue($stockSolution, 'epsilonApprox'));
                            $largestConcentration = $largestConcentrationAndNode['concentration'];
                            $constraint['current_state'] = +($largestConcentration >= 0.935 * $maxAcceptableConcentration && $largestConcentration <= 1.32 * $maxAcceptableConcentration);
                            $constraint['simulationVariables'] = ['dilutionBaseSolutionVolumeAndUnit'];
                            $constraint['nodesId'] = [$largestConcentrationAndNode['nodeId']];
                        } else {
                            $constraint['current_state'] = null;
                            $constraint['simulationVariables'] = null;
                            $constraint['nodesId'] = null;
                        }
                        break;
                    case 'referenceConcentrationsShouldBeEvenlyDistributedAtStep1':
                        if ($referenceSolutionsStep->count() > 0) {
                            $dilutions = collect($referenceSolutionsStep)->whereIn('structuredAction', ['dilution']);
                            $sortedDilutionsVolume = $dilutions->pluck('dilutionBaseSolutionVolumeAndUnit')->pluck(0)->sort()->values();
                            $theoreticalDifference = (floatval($sortedDilutionsVolume->last()) - floatval($sortedDilutionsVolume->first())) / ($dilutions->count() - 1);
                            $toleranceFactor = 1.4;
                            $maxDelta = 0;
                            for ($i = 0; $i < count($sortedDilutionsVolume)-1; $i++) {
                                $deltaI = floatval($sortedDilutionsVolume[$i + 1]) - floatval($sortedDilutionsVolume[$i]);
                                if ($deltaI > $maxDelta) {
                                    $maxDelta = $deltaI;
                                }
                            }
                            $constraint['current_state'] = +($maxDelta <= ($theoreticalDifference * $toleranceFactor));
                            $constraint['simulationVariables'] = ['dilutionBaseSolutionVolumeAndUnit'];
                            $constraint['nodesId'] = $dilutions->pluck('nodeId')->all();
                        } else {
                            $constraint['current_state'] = null;
                            $constraint['simulationVariables'] = null;
                            $constraint['nodesId'] = null;
                        }
                        break;
                    case 'oneAbsorbanceMeasureOrSpectrumShouldExist':
                        $constraint['current_state'] = +($this->protocol
                                ->whereIn('structuredAction', ['absorbanceMeasurement', 'spectrumProduction'])
                                ->count() > 0);
                        $constraint['simulationVariables'] = null;
                        $constraint['nodesId'] = null;
                        break;
                    case 'oneAbsorbanceMeasureOnStockSolutionOrItsDilutionAtStep3':
                        $constraint['current_state'] = +(
                            collect($absorbanceMeasuresStep)
                                ->whereIn('structuredAction', ['absorbanceMeasurement'])
                                ->filter(fn($sa) => $this->getMaterialPropertyValue($sa['absorbanceMeasurementSolution'], 'originShortName') === 'stockSolution'
                                    || $this->getMaterialPropertyValue($sa['absorbanceMeasurementSolution'], 'shortName') === 'stockSolution')
                                ->count() > 0
                        );
                        $constraint['simulationVariables'] = ['absorbanceMeasurementSolution'];
                        $constraint['nodesId'] = collect($absorbanceMeasuresStep)
                            ->whereIn('structuredAction', ['absorbanceMeasurement'])->pluck('nodeId')->all();
                        break;
                    case 'oneSpectrumShouldExistAtStep2':
                        $spectrumProductionActions = collect($curveElaborationStep)
                            ->whereIn('structuredAction', ['spectrumProduction']);
                        $constraint['current_state'] = +($spectrumProductionActions->count() > 0);
                        $constraint['simulationVariables'] = null;
                        $constraint['nodesId'] = null;
                        break;
                    case 'oneSpectrumOnStockSolutionOrItsDilutionAtStep2':
                        if ($curveElaborationStep->count() > 0) {
                            $spectrumProductionActions = collect($curveElaborationStep)
                                ->whereIn('structuredAction', ['spectrumProduction'])
                                ->filter(fn($sa) => $this->getMaterialPropertyValue($sa['spectrumProductionSolution'], 'originShortName') === 'stockSolution'
                                    || $this->getMaterialPropertyValue($sa['spectrumProductionSolution'], 'shortName') === 'stockSolution');
                            $failedSpectrumProductionActions = collect($curveElaborationStep)
                                ->whereIn('structuredAction', ['spectrumProduction'])
                                ->filter(fn($sa) => $this->getMaterialPropertyValue($sa['spectrumProductionSolution'], 'originShortName') !== 'stockSolution'
                                    && $this->getMaterialPropertyValue($sa['spectrumProductionSolution'], 'shortName') !== 'stockSolution');
                            $constraint['current_state'] = +($spectrumProductionActions->count() > 0);
                            $constraint['simulationVariables'] = ['spectrumProductionSolution'];
                            $constraint['nodesId'] = $failedSpectrumProductionActions->pluck('nodeId')->all();
                        } else {
                            $constraint['current_state'] = null;
                            $constraint['simulationVariables'] = null;
                            $constraint['nodesId'] = null;
                        }
                        break;
                    case 'spectrumShouldContainsSpecificMeasureZoneAtStep2':
                        if ($curveElaborationStep->count() > 0) {
                            $minAcceptableSpectrumWaveLength = floatval($this->getMaterialPropertyValue($stockSolution, 'minAcceptableSpectrumWaveLength'));
                            $maxAcceptableSpectrumWaveLength = floatval($this->getMaterialPropertyValue($stockSolution, 'maxAcceptableSpectrumWaveLength'));
                            $spectrumProductionActionsFailed = collect($curveElaborationStep)->whereIn('structuredAction', ['spectrumProduction'])
                                ->filter(fn($sa) => min(floatval($sa['spectrumProductionMin']), floatval($sa['spectrumProductionMax'])) > $minAcceptableSpectrumWaveLength || max(floatval($sa['spectrumProductionMax']), floatval($sa['spectrumProductionMax'])) < $maxAcceptableSpectrumWaveLength);
                            $constraint['current_state'] = +($spectrumProductionActionsFailed->count() === 0);
                            $constraint['simulationVariables'] = ["spectrumProductionMin", "spectrumProductionMax"];
                            $constraint['nodesId'] = $spectrumProductionActionsFailed->pluck('nodeId')->all();
                        } else {
                            $constraint['current_state'] = null;
                            $constraint['simulationVariables'] = null;
                            $constraint['nodesId'] = null;
                        }
                        break;
                    case 'oneSpectrumOnStockSolutionDilutionWithRelevantConcentrationAtStep2':
                        if ($curveElaborationStep->count() > 0) {
                            $spectrumProductionOnDilutedStockSolution = collect($curveElaborationStep)
                                ->whereIn('structuredAction', ['spectrumProduction'])
                                ->filter(fn($sa) => $this->getMaterialPropertyValue($sa['spectrumProductionSolution'], 'originShortName') === 'stockSolution'
                                    && $this->getMaterialPropertyValue($sa['spectrumProductionSolution'], 'concentration') < $this->computeMaxConcentration($spectro));
                            $failedSpectrumProductionOnDilutedStockSolution = collect($curveElaborationStep)
                                ->whereIn('structuredAction', ['spectrumProduction'])
                                ->filter(fn($sa) => $this->getMaterialPropertyValue($sa['spectrumProductionSolution'], 'originShortName') !== 'stockSolution'
                                    || $this->getMaterialPropertyValue($sa['spectrumProductionSolution'], 'concentration') >= $this->computeMaxConcentration($spectro));
                            $constraint['current_state'] = +($spectrumProductionOnDilutedStockSolution->count() > 0);
                            $constraint['simulationVariables'] = ["spectrumProductionSolution"];
                            $constraint['nodesId'] = $failedSpectrumProductionOnDilutedStockSolution->pluck('nodeId')->all();
                        } else {
                            $constraint['current_state'] = null;
                            $constraint['simulationVariables'] = null;
                            $constraint['nodesId'] = null;
                        }
                        break;
                    case 'firstAbsorbanceMeasurementShouldBeInTheAppropriateRangeAtStep3':
                        $absorbanceMeasurement = collect($absorbanceMeasuresStep)
                            ->first(fn($sa) => $sa['structuredAction'] === 'absorbanceMeasurement');
                        if ($absorbanceMeasuresStep->count() > 0 && $absorbanceMeasurement) {
                            $lambdaUser = floatval($absorbanceMeasurement['absorbanceMeasurementWaveLength']);
                            if ($this->getMaterialPropertyValue($absorbanceMeasurement['absorbanceMeasurementSolution'], 'shortName') === 'stockSolution') {
                                $originShortName = 'stockSolution';
                            } else {
                                $originShortName = $this->getMaterialPropertyValue($absorbanceMeasurement['absorbanceMeasurementSolution'], 'originShortName');
                            }
                            if (!$originShortName) {
                                $constraint['current_state'] = 0;
                                $constraint['simulationVariables'] = ["absorbanceMeasurementSolution"];
                            } else {
                                $originSolution = $this->getMaterialsByProperty('shortName', $originShortName)->first();
                                $minAcceptableSpectrumWaveLength = floatval($this->getMaterialPropertyValue($originSolution, 'minAcceptableSpectrumWaveLength'));
                                $maxAcceptableSpectrumWaveLength = floatval($this->getMaterialPropertyValue($originSolution, 'maxAcceptableSpectrumWaveLength'));
                                if (!($minAcceptableSpectrumWaveLength && $maxAcceptableSpectrumWaveLength)) {
                                    $constraint['current_state'] = 0;
                                    $constraint['simulationVariables'] = ["absorbanceMeasurementSolution"];
                                } else {
                                    $spectrumKey = $this->getMaterialPropertyValue($originSolution, 'spectrumKey');
                                    [$lambdaAMaxMin, $lambdaAMaxMax] = $this->getLambdaAMaxRelevant($spectrumKey, $minAcceptableSpectrumWaveLength, $maxAcceptableSpectrumWaveLength);
                                    $constraint['current_state'] = +($lambdaUser >= $lambdaAMaxMin - 3 && $lambdaUser <= $lambdaAMaxMax + 3);
                                    $constraint['simulationVariables'] = ["absorbanceMeasurementWaveLength"];
                                }
                            }
                            $constraint['nodesId'] = [$absorbanceMeasurement['nodeId']];
                        } else {
                            $constraint['current_state'] = null;
                            $constraint['simulationVariables'] = null;
                            $constraint['nodesId'] = null;
                        }
                        break;
                    case 'atLeast3AbsorbanceMeasureInTheLinearRangeAtStep3':
                        if ($absorbanceMeasuresStep->count() > 0) {
                            $absorbanceMeasurementActions = collect($absorbanceMeasuresStep)->whereIn('structuredAction', ['absorbanceMeasurement']);
                            $AMaxLinear = floatval($this->getMaterialPropertyValue($spectro, 'AmaxLinear'));
                            $absorbanceMeasurementActionsInLinearRange = $absorbanceMeasurementActions
                                ->filter(function($sa) use ($AMaxLinear, $spectro) {
                                    $isRelevantSolution = $this->getMaterialPropertyValue($sa['absorbanceMeasurementSolution'], 'shortName') === "stockSolution"
                                                        || $this->getMaterialPropertyValue($sa['absorbanceMeasurementSolution'], 'originShortName') === "stockSolution";
                                    $measuredAbsorbance = $this->measureAbsorbance(
                                        $this->getMaterialPropertyValue($sa['absorbanceMeasurementSolution'], 'spectrumKey'),
                                        floatval($this->getMaterialPropertyValue($sa['absorbanceMeasurementSolution'], 'concentration')),
                                        floatval($sa['absorbanceMeasurementWaveLength']),
                                        $spectro);
                                    return $isRelevantSolution && $measuredAbsorbance < $AMaxLinear;
                                });
                            $constraint['current_state'] = +($absorbanceMeasurementActionsInLinearRange->count() >= 3);
                            $constraint['simulationVariables'] = ['absorbanceMeasurementSolution', 'absorbanceMeasurementWaveLength'];
                            $constraint['nodesId'] = $absorbanceMeasurementActions->pluck('nodeId')->all();
                        } else {
                            $constraint['current_state'] = null;
                            $constraint['simulationVariables'] = null;
                            $constraint['nodesId'] = null;
                        }
                        break;
                    case 'oneAbsorbanceMeasureAtStep4':
                        if ($unknownSolutionConcentrationStep->count() > 0) {
                            $absorbanceMeasureActions = collect($unknownSolutionConcentrationStep)->whereIn('structuredAction', ['absorbanceMeasurement']);
                            $constraint['current_state'] = +($absorbanceMeasureActions->count() > 0);
                            $constraint['simulationVariables'] = ['absorbanceMeasurementSolution', 'absorbanceMeasurementWaveLength'];
                            $constraint['nodesId'] = $absorbanceMeasureActions->pluck('nodeId')->all();
                        } else {
                            $constraint['current_state'] = null;
                            $constraint['simulationVariables'] = null;
                            $constraint['nodesId'] = null;
                        }
                        break;
                    case 'oneAbsorbanceMeasureOnUnknownSolutionOrItsDilutionAtStep4':
                        $constraint['current_state'] = +(
                            collect($unknownSolutionConcentrationStep)
                                ->whereIn('structuredAction', ['absorbanceMeasurement'])
                                ->filter(fn($sa) => $this->getMaterialPropertyValue($sa['absorbanceMeasurementSolution'], 'known') === "0")
                                ->count() > 0
                        );
                        $constraint['simulationVariables'] = ['absorbanceMeasurementSolution'];
                        $constraint['nodesId'] = collect($unknownSolutionConcentrationStep)
                            ->whereIn('structuredAction', ['absorbanceMeasurement'])->pluck('nodeId')->all();
                        break;
                    case 'absorbanceMeasureOnUnknownSolutionOrItsDilutionShouldFallWithinSpectroRangeAtStep4':
                        if ($unknownSolutionConcentrationStep->count() > 0) {
                            $validAbsorbanceMeasureActions = collect($unknownSolutionConcentrationStep)
                                ->whereIn('structuredAction', ['absorbanceMeasurement'])
                                ->filter(function ($sa) use ($spectro) {
                                    $isRelevantSolution = $this->getMaterialPropertyValue($sa['absorbanceMeasurementSolution'], 'known') === "0";
                                    $measuredAbsorbance = $this->measureAbsorbance(
                                        $this->getMaterialPropertyValue($sa['absorbanceMeasurementSolution'], 'spectrumKey'),
                                        floatval($this->getMaterialPropertyValue($sa['absorbanceMeasurementSolution'], 'concentration')),
                                        floatval($sa['absorbanceMeasurementWaveLength']),
                                        $spectro);
                                    return $isRelevantSolution
                                        && $measuredAbsorbance
                                        && $measuredAbsorbance >= (floatval($this->getMaterialPropertyValue($spectro, 'UA'))*5)
                                        && $measuredAbsorbance <= floatval($this->getMaterialPropertyValue($spectro, 'AmaxLinear'));
                                });
                            $constraint['current_state'] = +($validAbsorbanceMeasureActions->count() > 0);
                            $constraint['simulationVariables'] = ['absorbanceMeasurementSolution'];
                            $constraint['nodesId'] = collect($unknownSolutionConcentrationStep)
                                ->whereIn('structuredAction', ['absorbanceMeasurement'])->pluck('nodeId')->all();
                        } else {
                            $constraint['current_state'] = null;
                            $constraint['simulationVariables'] = null;
                            $constraint['nodesId'] = null;
                        }
                        break;
                    case 'absorbanceMeasureOnUnknownSolutionOrItsDilutionShouldBeLargeEnoughAtStep4':
                        if($unknownSolutionConcentrationStep->count() > 0 && $minLinearUserAbsorbance) {
                            $invalidAbsorbanceMeasureActionsOnUnknownSolution = collect($unknownSolutionConcentrationStep)
                                    ->whereIn('structuredAction', ['absorbanceMeasurement'])
                                    ->filter(function ($sa) use ($spectro, $minLinearUserAbsorbance) {
                                        $isRelevantSolution = $this->getMaterialPropertyValue($sa['absorbanceMeasurementSolution'], 'known') === "0";
                                        $measuredAbsorbance = $this->measureAbsorbance(
                                            $this->getMaterialPropertyValue($sa['absorbanceMeasurementSolution'], 'spectrumKey'),
                                            floatval($this->getMaterialPropertyValue($sa['absorbanceMeasurementSolution'], 'concentration')),
                                            floatval($sa['absorbanceMeasurementWaveLength']),
                                            $spectro);
                                        return $isRelevantSolution
                                            && $measuredAbsorbance
                                            && $measuredAbsorbance < $minLinearUserAbsorbance;
                                    });
                            $constraint['current_state'] = +($invalidAbsorbanceMeasureActionsOnUnknownSolution->count() === 0);
                            $constraint['simulationVariables'] = ['absorbanceMeasurementSolution'];
                            $constraint['nodesId'] = collect($unknownSolutionConcentrationStep)
                                ->whereIn('structuredAction', ['absorbanceMeasurement'])->pluck('nodeId')->all();
                        } else {
                            $constraint['current_state'] = null;
                            $constraint['simulationVariables'] = null;
                            $constraint['nodesId'] = null;
                        }
                        break;
                    case 'absorbanceMeasureOnUnknownSolutionOrItsDilutionShouldBeSmallEnoughAtStep4':
                        if($unknownSolutionConcentrationStep->count() > 0 && $maxLinearUserAbsorbance) {
                            $invalidAbsorbanceMeasureActionsOnUnknownSolution = collect($unknownSolutionConcentrationStep)
                                    ->whereIn('structuredAction', ['absorbanceMeasurement'])
                                    ->filter(function ($sa) use ($spectro, $maxLinearUserAbsorbance) {
                                        $isRelevantSolution = $this->getMaterialPropertyValue($sa['absorbanceMeasurementSolution'], 'known') === "0";
                                        $measuredAbsorbance = $this->measureAbsorbance(
                                            $this->getMaterialPropertyValue($sa['absorbanceMeasurementSolution'], 'spectrumKey'),
                                            floatval($this->getMaterialPropertyValue($sa['absorbanceMeasurementSolution'], 'concentration')),
                                            floatval($sa['absorbanceMeasurementWaveLength']),
                                            $spectro);
                                        return $isRelevantSolution
                                            && $measuredAbsorbance
                                            && $measuredAbsorbance > $maxLinearUserAbsorbance;
                                    });
                            $constraint['current_state'] = +($invalidAbsorbanceMeasureActionsOnUnknownSolution->count() === 0);
                            $constraint['simulationVariables'] = ['absorbanceMeasurementSolution'];
                            $constraint['nodesId'] = collect($unknownSolutionConcentrationStep)
                                ->whereIn('structuredAction', ['absorbanceMeasurement'])->pluck('nodeId')->all();
                        } else {
                            $constraint['current_state'] = null;
                            $constraint['simulationVariables'] = null;
                            $constraint['nodesId'] = null;
                        }
                        break;
                    case 'dilutionSolventShouldMatchOriginSolutionSolvent':
                        $dilutionAction = $this->protocol
                            ->whereIn('structuredAction', ['dilution'])
                            ->first(fn($sa) => $this->getMaterialPropertyValue($sa['dilutionSolvent'], 'shortName') !== 'distilledWater');
                        $constraint['current_state'] = +(!$dilutionAction);
                        $constraint['simulationVariables'] = ['dilutionSolvent'];
                        if ($dilutionAction) {
                            $constraint['nodesId'] = [$dilutionAction['nodeId']];
                        }
                        break;
                    case 'dilutionBaseSolutionShouldBeCollectedWithPrecisionMaterial':
                        $dilutionActions = $this->protocol
                            ->whereIn('structuredAction', ['dilution'])
                            ->filter(fn($sa) => $this->getMaterialPropertyValue($sa['dilutionBaseSolutionMeasurer'], 'shortName') !== 'micropipette');
                        $constraint['current_state'] = +($dilutionActions->count()===0);
                        $constraint['simulationVariables'] = ['dilutionBaseSolutionMeasurer'];
                        if ($dilutionActions->count()>0) {
                            $constraint['nodesId'] = $dilutionActions->pluck('nodeId')->all();
                        }
                        break;
                    case 'dilutionShouldBeProcessedWithGraduatedFlask':
                        $dilutionActions = $this->protocol
                            ->whereIn('structuredAction', ['dilution'])
                            ->filter(fn($sa) => $this->getMaterialPropertyValue($sa['dilutionContainer'], 'shortName') !== 'graduatedFlask');
                        $constraint['current_state'] = +($dilutionActions->count()===0);
                        $constraint['simulationVariables'] = ['dilutionContainer'];
                         if ($dilutionActions->count()>0) {
                             $constraint['nodesId'] = $dilutionActions->pluck('nodeId')->all();
                         }
                        break;
                    case 'graduatedFlaskShouldBeFilledToItsMark':
                        $dilutionAction = $this->protocol
                            ->whereIn('structuredAction', ['dilution'])
                            ->first(fn($sa) => $this->getMaterialPropertyValue($sa['dilutionContainer'], 'shortName') === 'graduatedFlask'
                                && floatval($this->getMaterialPropertyValue($sa['dilutionContainer'], 'volume')) !== floatval($sa['dilutionSolutionCreatedVolumeAndUnit'][0])
                            );
                        $constraint['current_state'] = +(!$dilutionAction);
                        $constraint['simulationVariables'] = ['dilutionSolutionCreatedVolumeAndUnit', 'dilutionContainer'];
                        if ($dilutionAction) {
                            $constraint['nodesId'] = [$dilutionAction['nodeId']];
                        }
                        break;
                    case 'aGraduatedFlaskShouldBeConditioned':
                        $conditionedGraduatedFlasks = [];
                        foreach ($this->protocol as $action) {
                            if ($action['structuredAction'] === 'conditioning'
                                && $this->getMaterialPropertyValue($action['conditioningContainer'], 'shortName') === 'graduatedFlask'
                                && !in_array($this->getMaterialsByProperty('shortName', 'graduatedFlask')->first()['id'], $conditionedGraduatedFlasks)) {
                                $conditionedGraduatedFlasks[] = $this->getMaterialsByProperty('shortName', 'graduatedFlask')->first()['id'];
                            }
                            if ($action['structuredAction'] !== 'dilution') {
                                $dirtyMaterialKeys = ['transferContainer'];
                                $dirtySimulationVariables = collect($action)->filter(fn($v, $k) => in_array($k, $dirtyMaterialKeys) && $this->getMaterialPropertyValue($v, 'shortName') === 'graduatedFlask');
                                foreach ($dirtySimulationVariables as $dirtySimulationVariable) {
                                    $valueToBeDeleted = collect($conditionedGraduatedFlasks)->last(fn($id) => intval($id) === intval($action[$dirtySimulationVariable]['id']));
                                    $keyToBeDeleted = array_search($valueToBeDeleted, array_reverse($conditionedGraduatedFlasks, true));
                                    if ($keyToBeDeleted !== false) {
                                        unset($conditionedGraduatedFlasks[$keyToBeDeleted]);
                                    }
                                }
                            }
                            if ($action['structuredAction'] === 'dilution' && $this->getMaterialPropertyValue($action['dilutionContainer'], 'shortName') === 'graduatedFlask') {
                                $valueToBeDeleted = collect($conditionedGraduatedFlasks)->last(fn($id) => intval($id) === intval($action['dilutionContainer']['id']));
                                $keyToBeDeleted = array_search($valueToBeDeleted, array_reverse($conditionedGraduatedFlasks, true));
                                if ($keyToBeDeleted === false) {
                                    $constraint['current_state'] = 0;
                                    $constraint['simulationVariables'] = ['dilutionContainer'];
                                    $constraint['nodesId'] = [$action['nodeId']];
                                    break 2;
                                } else {
                                    unset($conditionedGraduatedFlasks[$keyToBeDeleted]);
                                }
                            }
                        }
                        $constraint['current_state'] = 1;
                        $constraint['simulationVariables'] = null;
                        $constraint['nodesId'] = null;
                        break;
                    case 'aGraduatedFlaskShouldBeConditionedWithRelevantSolvent':
                        $conditionedGraduatedFlasks = collect();
                        foreach ($this->protocol as $action) {
                            if ($action['structuredAction'] === 'conditioning'
                                && $this->getMaterialPropertyValue($action['conditioningContainer'], 'shortName') === 'graduatedFlask'
                                && !$conditionedGraduatedFlasks->some(fn($v) => intval($v['id']) === intval($this->getMaterialsByProperty('shortName', 'graduatedFlask')->first()['id']))) {
                                $conditionedGraduatedFlasks->push([
                                    'id' => $this->getMaterialsByProperty('shortName', 'graduatedFlask')->first()['id'],
                                    'action' => $action
                                ]);
                            }
                            if ($action['structuredAction'] === 'dilution' && $conditionedGraduatedFlasks->count() > 0
                                && $this->getMaterialPropertyValue($action['dilutionContainer'], 'shortName') === 'graduatedFlask') {
                                $graduatedFlask = $conditionedGraduatedFlasks->last(fn($v, $k) => $v['id'] === $action['dilutionContainer']['id']);
                                if ($graduatedFlask) {
                                    if (intval($graduatedFlask['action']['conditioningSolution']['id']) !== intval($action['dilutionSolvent']['id'])) {
                                        $constraint['current_state'] = 0;
                                        $constraint['simulationVariables'] = ['conditioningSolution', 'dilutionSolvent'];
                                        $constraint['nodesId'] = [$graduatedFlask['action']['nodeId'], $action['nodeId']];
                                        break 2;
                                    } else {
                                        $conditionedGraduatedFlasks = collect($conditionedGraduatedFlasks)->reject(fn($v, $k) => intval($v['id']) === intval($graduatedFlask['id']));
                                    }
                                }
                            }
                        }
                        $constraint['current_state'] = 1;
                        $constraint['simulationVariables'] = null;
                        $constraint['nodesId'] = null;
                        break;
                    case 'betweenTwoDilutionsSolutionShouldBeTransferred':
                        $previousAction = null;
                        foreach ($this->protocol as $action) {
                            if ( $action['structuredAction'] === 'dilution' ) {
                                if($previousAction && $previousAction['name'] === $action['structuredAction']) {
                                    $constraint['current_state'] = 0;
                                    $constraint['simulationVariables'] = ['dilutionSolutionCreated'];
                                    $constraint['nodesId'] = [$previousAction['nodeId']];
                                    break 2;
                                }
                                $previousAction = ['nodeId' => $action['nodeId'], 'name' => $action['structuredAction'], 'nameCreatedSolution' => $action['dilutionSolutionCreated']['name']];
                            }
                            if ( $previousAction && $action['structuredAction'] === 'transfer' && 
                                $action['transferSolution']['name'] === $previousAction['nameCreatedSolution']) {
                                $previousAction['name'] = $action['structuredAction'];
                                $previousAction['nodeId'] = null;
                                $previousAction['idCreatedSolution'] = null;
                            }
                        }
                        $constraint['current_state'] = 1;
                        $constraint['simulationVariables'] = null;
                        $constraint['nodesId'] = null;
                        break;
                    case 'graduatedFlaskSolutionShouldBeTransferredToTube':
                        $transferAction = $this->protocol
                            ->whereIn('structuredAction', ['transfer'])
                            ->first(fn($sa) => $this->getMaterialPropertyValue($sa['transferContainer'], 'shortName') !== 'testTube');
                        $constraint['current_state'] = +(!$transferAction);
                        $constraint['simulationVariables'] = ['transferContainer'];
                        if ($transferAction) {
                            $constraint['nodesId'] = [$transferAction['nodeId']];
                        }
                        break;
                    case 'noMoreThanXTransfers':
                        $testTubeNumber = intval($this->getMaterialsByProperty('shortName', 'testTube')->first()['quantity']);
                        $constraint['current_state'] = +($this->protocol
                            ->whereIn('structuredAction', ['transfer'])
                            ->count() <= $testTubeNumber);
                        $constraint['simulationVariables'] = ['transferContainer', 'transferSolution'];
                        if ($constraint['current_state']) {
                            $constraint['nodesId'] = null;
                        } else {
                            $constraint['nodesId'] = $this->protocol
                                ->whereIn('structuredAction', ['transfer'])
                                ->pluck('nodeId')->all();
                        }
                        break;
                    case 'spectrophotometerTankShouldBeConditioned':
                        $conditionedAction=null;
                        $constraint['current_state'] = 1;
                        $constraint['simulationVariables'] = null;
                        $constraint['nodesId'] = null;
                        foreach ($this->protocol as $action) {
                            if ($action['structuredAction'] === 'conditioning'
                                && $this->getMaterialPropertyValue($action['conditioningContainer'], 'shortName') === 'tank') {
                                $conditionedAction = $action;
                            }
                            if ( in_array($action['structuredAction'], ['absorbanceMeasurement', 'spectrumProduction', 'referenceSpectrumRecording']) ) {
                                if(!$conditionedAction) {
                                    $constraint['current_state'] = 0;
                                    $constraint['simulationVariables'] = ['absorbanceMeasurementSolution', 'spectrumProductionSolution'];
                                    $constraint['nodesId'] = [$action['nodeId']];
                                    break 2;
                                }
                            }
                        }
                        break;
                    case 'spectrophotometerTankShouldBeCorrectlyConditioned':
                        $conditionedAction=null;
                        $constraint['current_state'] = 1;
                        $constraint['simulationVariables'] = null;
                        $constraint['nodesId'] = null;
                        foreach ($this->protocol as $action) {
                            if ($action['structuredAction'] === 'conditioning'
                                && $this->getMaterialPropertyValue($action['conditioningContainer'], 'shortName') === 'tank') {
                                $conditionedAction = $action;
                            }
                            if ( in_array($action['structuredAction'], ['absorbanceMeasurement', 'spectrumProduction', 'referenceSpectrumRecording']) ) {
                                if(!$conditionedAction) {
                                    $constraint['current_state'] = 0;
                                    $constraint['simulationVariables'] = ['absorbanceMeasurementSolution', 'spectrumProductionSolution', 'referenceSpectrumRecordingSolution'];
                                    $constraint['nodesId'] = [$action['nodeId']];
                                    break 2;
                                } else {
                                    $conditionedSolutionTank = $conditionedAction['conditioningSolution'];
                                    if ($action['structuredAction'] === 'absorbanceMeasurement') {
                                        $measuredSolution = $action['absorbanceMeasurementSolution'];
                                    } else if ($action['structuredAction'] === 'spectrumProduction'){
                                        $measuredSolution = $action['spectrumProductionSolution'];
                                    } else {
                                        $measuredSolution = $action['referenceSpectrumRecordingSolution'];
                                    }
                                    if($measuredSolution) {
                                        if($this->getMaterialPropertyValue($measuredSolution, 'concentration')) {
                                            $constraint['current_state'] = +(
                                                ($this->getMaterialPropertyValue($measuredSolution, 'shortName') === $this->getMaterialPropertyValue($conditionedSolutionTank, 'shortName')
                                                || $this->getMaterialPropertyValue($measuredSolution, 'originShortName') === $this->getMaterialPropertyValue($conditionedSolutionTank, 'originShortName'))
                                                && $this->getMaterialPropertyValue($conditionedSolutionTank, 'concentration')
                                                && floatval($this->getMaterialPropertyValue($measuredSolution, 'concentration')) >= floatval($this->getMaterialPropertyValue($conditionedSolutionTank, 'concentration')));
                                        } else {
                                            $constraint['current_state'] = +($this->getMaterialPropertyValue($measuredSolution, 'shortName') === $this->getMaterialPropertyValue($conditionedSolutionTank, 'shortName'));
                                        }
                                        $constraint['simulationVariables'] = ['conditioningSolution', 'absorbanceMeasurementSolution', 'spectrumProductionSolution'];
                                        $constraint['nodesId'] = [$conditionedAction['nodeId'], $action['nodeId']];
                                        if($constraint['current_state'] === 0) {
                                            break 2;
                                        }
                                    } else {
                                        $conditionedAction['conditioningSolution'] = $measuredSolution;
                                    }
                                }
                            }
                        }
                        break;
                    case 'aReferenceMustBeRecordedBeforeTheFirstMeasure':
                        $referenceFirstIndex = $this->protocol->search(fn($action, $index) => $action['structuredAction'] === 'referenceSpectrumRecording');
                        $measurementFirstIndex = $this->protocol->search(fn($action, $index) => $action['structuredAction'] === 'spectrumProduction' || $action['structuredAction'] === 'absorbanceMeasurement');
                        if( $measurementFirstIndex && ( !$referenceFirstIndex || $referenceFirstIndex > $measurementFirstIndex ) ) {
                            $constraint['current_state'] = 0;
                            if($referenceFirstIndex) {
                                $constraint['simulationVariables'] = ["referenceSpectrumRecordingSolution"];
                                $constraint['nodesId'] = [$this->protocol[$referenceFirstIndex]['nodeId']];
                            } else {
                                $constraint['simulationVariables'] = null;
                                $constraint['nodesId'] = null;
                            }
                        } else {
                            $constraint['current_state'] = 1;
                            $constraint['simulationVariables'] = null;
                            $constraint['nodesId'] = null;
                        }
                        break;
                    case 'aReferenceShouldBeRecordedOnlyOnce':
                        $referenceActions = $this->protocol
                            ->whereIn('structuredAction', ['referenceSpectrumRecording']);
                        $constraint['current_state'] = +($referenceActions->count() === 1);
                        $constraint['simulationVariables'] = ["referenceSpectrumRecordingSolution"];
                        $constraint['nodesId'] = $referenceActions->filter(fn($action, $index) => $index >= 1)->pluck('nodeId')->all();
                        break;
                    case 'referenceShouldBeRecordedWithDistilledWater':
                        $referenceAction = $this->protocol
                            ->first(fn($a) => $a['structuredAction'] === 'referenceSpectrumRecording');
                        if ($referenceAction) {
                            $constraint['current_state'] = +($this->getMaterialPropertyValue($referenceAction['referenceSpectrumRecordingSolution'], 'shortName') === 'distilledWater');
                            $constraint['simulationVariables'] = ["referenceSpectrumRecordingSolution"];
                            $constraint['nodesId'] = [$referenceAction['nodeId']];
                        } else {
                            $constraint['current_state'] = null;
                            $constraint['simulationVariables'] = null;
                            $constraint['nodesId'] = null;
                        }
                        break;
                    case 'noPipetConditioned':
                        $pipetConditionedActions = $this->protocol
                            ->filter(fn($a) => $a['structuredAction'] === 'conditioning' && $this->getMaterialPropertyValue($a['conditioningContainer'], 'shortName') === 'micropipette');
                        $constraint['current_state'] = +($pipetConditionedActions->count()===0);
                        $constraint['simulationVariables'] = ["conditioningContainer"];
                        $constraint['nodesId'] = $pipetConditionedActions->pluck('nodeId')->all();
                        break;
                    case 'eachAbsorbanceMeasurementShouldBeProcessedAtTheSameWaveLength':
                        $absorbanceMeasurementActions = $this->protocol->filter(fn($a) => $a['structuredAction'] === 'absorbanceMeasurement');
                        $constraint['current_state'] = +($absorbanceMeasurementActions->pluck('absorbanceMeasurementWaveLength')->unique()->count() === 1);
                        $constraint['simulationVariables'] = ["absorbanceMeasurementWaveLength"];
                        $constraint['nodesId'] = $absorbanceMeasurementActions->pluck('nodeId')->all();
                        break;
                    case 'noMoreThan4ConditioningOrMeasureOnSameDilutedSolution':
                        $dilutedSolutions = $this->protocol
                            ->filter(fn($a) => $a['structuredAction'] === 'dilution')
                            ->pluck('dilutionSolutionCreated');
                        foreach ($dilutedSolutions as $dilutedSolution) {
                            $conditioningActions = $this->protocol
                                ->filter(fn($a) => 
                                    $a['structuredAction'] === 'conditioning'
                                    && intval($a['conditioningSolution']['id']) === intval($dilutedSolution['id']));
                            if ($conditioningActions->count() > 4) {
                                $constraint['current_state'] = 0;
                                $constraint['simulationVariables'] = ["conditioningSolution"];
                                $constraint['nodesId'] = $conditioningActions->pluck('nodeId')->all();
                                break 2;
                            }
                            $measurementActions = $this->protocol
                                ->filter(fn($a) => 
                                    $a['structuredAction'] === 'absorbanceMeasurement'
                                    && intval($a['absorbanceMeasurementSolution']['id']) === intval($dilutedSolution['id']));
                            if ($measurementActions->count() > 4) {
                                $constraint['current_state'] = 0;
                                $constraint['simulationVariables'] = ["absorbanceMeasurementSolution"];
                                $constraint['nodesId'] = $measurementActions->pluck('nodeId')->all();
                                break 2;
                            }
                            $spectrumProductionActions = $this->protocol
                                ->filter(fn($a) => 
                                    $a['structuredAction'] === 'spectrumProduction'
                                    && intval($a['spectrumProductionSolution']['id']) === intval($dilutedSolution['id']));
                            if ($spectrumProductionActions->count() > 4) {
                                $constraint['current_state'] = 0;
                                $constraint['simulationVariables'] = ["spectrumProductionSolution"];
                                $constraint['nodesId'] = $spectrumProductionActions->pluck('nodeId')->all();
                                break 2;
                            }
                        }
                        $constraint['current_state'] = 1;
                        $constraint['simulationVariables'] = null;
                        $constraint['nodesId'] = null;
                        break;
                }
            } else {
                // constraint is not evaluated because at least one relevant constraint is failing
                $constraint['current_state'] = null;
            }
            if($error_msg) {
                break;
            }
        }
        return ['constraints' => $this->constraints, 'error_message' => $error_msg];
    }
    
    public function computeMaxConcentration($spectro) {
        // At this step, spectrumProductionSolution is a dilution of stockSolution
        $stockSolution = $this->getMaterialsByProperty('shortName', 'stockSolution')->first();
        $spectrum = collect($this->spectra[$this->getMaterialPropertyValue($stockSolution, 'spectrumKey')])->first();
        $AMaxSpectro = floatval($this->getMaterialPropertyValue($spectro, 'Amax'));
        $minAcceptableSpectrumWaveLength = floatval($this->getMaterialPropertyValue($stockSolution, 'minAcceptableSpectrumWaveLength'));
        $maxAcceptableSpectrumWaveLength = floatval($this->getMaterialPropertyValue($stockSolution, 'maxAcceptableSpectrumWaveLength'));
        // Assuming user will choose lambdaAmax
        $AMaxSolution = floatval(collect($spectrum->data)->filter(fn($d) => $d->lambda > $minAcceptableSpectrumWaveLength && $d->lambda < $maxAcceptableSpectrumWaveLength)->max('absorbance'));
        $concentrationReference = floatval($spectrum->concentrationReference);
        // Concentration corresponding to the maximum absorbance
        $maxLinearConcentration = $AMaxSpectro / $AMaxSolution * $concentrationReference * $this->alpha;
        // Concentration corresponding to the maximum absorbance acceptable ($maxAcceptableConcentration > $maxLinearConcentration)
        $maxAcceptableConcentration = $maxLinearConcentration * (1 + (1 - $this->alpha) / $this->alpha * log((1 - $this->alpha) / (1 - $this->threshold)));
        // Damping model
        // 0 <= x < $maxLinearConcentration:   f($x) = $AMaxSpectro * $alpha * $x / $maxLinearConcentration = ($AMaxSolution / $concentrationReference) * $alpha * $x
        // $maxLinearConcentration <= x :      f($x) = $AMaxSpectro * (1 - (1 - $alpha) * exp($alpha / (1 - $alpha) * (1 - $x / $maxLinearConcentration)))
        return $maxAcceptableConcentration;
    }
    
    public function measureAbsorbance ($spectrumKey, $concentration, $lambda, $spectro, $whithErrors=true)
    {
        if (!$spectrumKey) {
            return false;
        } else {
            $allSpectrum = collect($this->spectra[$spectrumKey]);
            $spectroMinLambda = floatval($this->getMaterialPropertyValue($spectro, 'minWaveLength'));
            $spectroMaxLambda = floatval($this->getMaterialPropertyValue($spectro, 'maxWaveLength'));
            $spectrumMinLambda = $allSpectrum->pluck('data')->flatten()->min('lambda');
            $spectrumMaxLambda = $allSpectrum->pluck('data')->flatten()->max('lambda');
            $lambda = max(min($lambda + ($whithErrors ? $this->nRand($this->lambdaErrorModel) : 0), $spectroMaxLambda), $spectroMinLambda);
            if ($lambda > $spectrumMaxLambda || $lambda < $spectrumMinLambda) {
                return 0;
            } else {
                $absorbance = $this->computeInterpolateAbsorbance($allSpectrum, $lambda, $concentration);
                $AMaxSpectro = floatval($this->getMaterialPropertyValue($spectro, 'Amax'));
                if ($absorbance >= $AMaxSpectro * $this->alpha) {
                    $maxLinearConcentration = $AMaxSpectro / $absorbance * $concentration * $this->alpha;
                    $absorbance = $AMaxSpectro * (1 - (1 - $this->alpha) * exp($this->alpha / (1 - $this->alpha) * (1 - $concentration / $maxLinearConcentration)));
                }
                if ($lambda > $this->largeLambdaError) {
                    $absorbance = $absorbance + ($whithErrors ? ($this->nRand($this->largeLambdaErrorModel) * ($lambda - $this->largeLambdaError) / ($spectroMaxLambda - $this->largeLambdaError)) : 0);
                }
                return $absorbance + ($whithErrors ? $this->nRand($this->absorbanceErrorModel) : 0);
            }
        }
    }
    
    public function computeInterpolateAbsorbance($allSpectrum, $lambda, $concentration) {
        foreach ($allSpectrum as $spectrum) {
            $spectrum->interpolateAbsorbance = $this->spectrumInterpolate($spectrum->data, $lambda);
        }
        $minReferenceSpectrum = $allSpectrum->sortBy('concentrationReference')->first();
        $maxReferenceSpectrum = $allSpectrum->sortBy('concentrationReference')->last();
        if($concentration > $minReferenceSpectrum->concentrationReference && $concentration < $maxReferenceSpectrum->concentrationReference) {
            // concentration between c_ref_min et c_ref_max => linear interpolation
            $absorbance = ($concentration - $minReferenceSpectrum->concentrationReference)
                / ($maxReferenceSpectrum->concentrationReference - $minReferenceSpectrum->concentrationReference)
                * ($maxReferenceSpectrum->interpolateAbsorbance - $minReferenceSpectrum->interpolateAbsorbance)
                + $minReferenceSpectrum->interpolateAbsorbance;
        } else if( $concentration <= $minReferenceSpectrum->concentrationReference ){
            // concentration lower than c_ref_min => extrapolate from c_ref_min
            $absorbance = $minReferenceSpectrum->interpolateAbsorbance * $concentration / $minReferenceSpectrum->concentrationReference;
        } else {
            // concentration greater than c_ref_max => extrapolate from c_ref_max
            $absorbance = $maxReferenceSpectrum->interpolateAbsorbance * $concentration / $maxReferenceSpectrum->concentrationReference;
        }
        return $absorbance;
    }
    
    public function spectrumInterpolate ($spectrumData, $lambda)
    {
        $deltas = collect($spectrumData)->map(function($d) use($lambda) { $d->delta = $d->lambda - $lambda; return $d; });
        $previousElement = $deltas->filter(fn($elt) => $elt->delta <= 0)->sortBy('delta')->last();
        $nextElement = $deltas->filter(fn($elt) => $elt->delta >= 0)->sortBy('delta')->first();
        // if no record at this lambda we assume absorbance is equal to 0
        $x1 = $previousElement?floatval($previousElement->lambda):0;
        $x2 = $nextElement?floatval($nextElement->lambda):0;
        $y1 = $previousElement?floatval($previousElement->absorbance):0;
        $y2 = $nextElement?floatval($nextElement->absorbance):0;
        if ($x1 === $x2) {
            return $y1;
        } else {
            return ($y2 - $y1) / ($x2 - $x1) * (floatval($lambda) - $x1) + $y1;
        }
    }
    
    public function getSpectrumData($action,$spectro) 
    {
        $spectrumKey = $this->getMaterialPropertyValue($action['spectrumProductionSolution'], 'spectrumKey');
        if(!$spectrumKey) {
            return null;
        } else {
            $newSpectrumData = [];
            $allSpectrum = collect($this->spectra[$spectrumKey]);
            $spectrumMinLambda = $allSpectrum->pluck('data')->flatten()->min('lambda');
            $spectrumMaxLambda = $allSpectrum->pluck('data')->flatten()->max('lambda');
            $concentration = floatval($this->getMaterialPropertyValue($action['spectrumProductionSolution'], 'concentration'));
            $step = 1;
            $lambdaMin = max(min(floatVal($action['spectrumProductionMin']), floatVal($action['spectrumProductionMax'])), $spectrumMinLambda);
            $lambdaMax = max(floatVal($action['spectrumProductionMax']), floatVal($action['spectrumProductionMin']));
            $lambdaMin = ceil($lambdaMin/10)*10;
            $lambdaMax = floor($lambdaMax/10)*10;
            for($lambda = $lambdaMin; $lambda <= $lambdaMax; $lambda+=$step) {
                if($lambda <= $spectrumMaxLambda) {
                    $newSpectrumData[] = ['lambda' => $lambda, 'absorbance' =>  $this->measureAbsorbance($spectrumKey, $concentration, $lambda, $spectro, false)];
                } else {
                    $newSpectrumData[] = ['lambda' => $lambda, 'absorbance' =>  0];
                }
            }
            return $newSpectrumData;
        }
    }
    
    public function getMinAndMaxUserAbsorbance ($absorbanceActions, $spectro)
    {
        $minLinear = floatval($this->getMaterialPropertyValue($spectro, 'UA')) * 5;
        $maxLinear = floatval($this->getMaterialPropertyValue($spectro, 'AmaxLinear'));
        $orderedActions = $absorbanceActions->map(
            fn($d) => array_merge($d, ['absorbance' => $this->measureAbsorbance(
                $this->getMaterialPropertyValue($d['absorbanceMeasurementSolution'],'spectrumKey'),
                floatval($this->getMaterialPropertyValue($d['absorbanceMeasurementSolution'],'concentration')),
                floatval($d['absorbanceMeasurementWaveLength']),
                $spectro)
            ]))->filter(fn($d) => $d['absorbance'] >= $minLinear && $d['absorbance'] <= $maxLinear)
           ->sortBy('absorbance');
        if($orderedActions->count() === 0) {
            return [null,null];
        } else{
            return [floatval($orderedActions->first()['absorbance']), floatval($orderedActions->last()['absorbance'])];
        }
    }
    
    public function getLambdaAMaxRelevant($spectrumKey, $minAcceptableSpectrumWaveLength, $maxAcceptableSpectrumWaveLength) {
        $flattenSpectrumData = collect($this->spectra[$spectrumKey])->pluck('data')->flatten();
        $relevantSpectrumData = $flattenSpectrumData->filter(fn($d) => $d->lambda > $minAcceptableSpectrumWaveLength && $d->lambda < $maxAcceptableSpectrumWaveLength);
        $AMax = $relevantSpectrumData->max('absorbance');
        $lambdaAMaxMin = $relevantSpectrumData->where('absorbance',$AMax)->min('lambda');
        $lambdaAMaxMax = $relevantSpectrumData->where('absorbance',$AMax)->max('lambda');
        return [$lambdaAMaxMin, $lambdaAMaxMax];
    }
    
    public function buildDependencies()
    {
        // All the variables indicated in the constraint logic but not only these: others may be necessary
        $this->constraintsDependencies['oneDilutionShouldExistAtStep1'] = ['dilutionBaseSolution'];
        $this->constraintsDependencies['eachReferenceSolutionIsPreparedWithStockSolutionAtStep1'] = ['dilutionBaseSolution'];
        $this->constraintsDependencies['AtLeast4ReferenceSolutionsAtStep1'] = [];
        $this->constraintsDependencies['smallestReferenceConcentrationShouldMatchSpectroAtStep1'] = ['dilutionSolutionCreated'];
        $this->constraintsDependencies['largestReferenceConcentrationShouldMatchSpectroAtStep1'] = ['dilutionSolutionCreated'];
        $this->constraintsDependencies['referenceConcentrationsShouldBeEvenlyDistributedAtStep1'] = ['dilutionBaseSolutionVolumeAndUnit'];
        $this->constraintsDependencies['oneAbsorbanceMeasureOrSpectrumShouldExist'] = ['absorbanceMeasurementSolution', 'spectrumProductionSolution'];
        $this->constraintsDependencies['oneAbsorbanceMeasureOnStockSolutionOrItsDilutionAtStep3'] = ['absorbanceMeasurementSolution'];
        $this->constraintsDependencies['oneSpectrumShouldExistAtStep2'] = ['spectrumProductionSolution'];
        $this->constraintsDependencies['oneSpectrumOnStockSolutionOrItsDilutionAtStep2'] = ['spectrumProductionSolution'];
        $this->constraintsDependencies['spectrumShouldContainsSpecificMeasureZoneAtStep2'] = ['spectrumProductionMin', 'spectrumProductionMax'];
        $this->constraintsDependencies['oneSpectrumOnStockSolutionDilutionWithRelevantConcentrationAtStep2'] = ['spectrumProductionSolution'];
        $this->constraintsDependencies['firstAbsorbanceMeasurementShouldBeInTheAppropriateRangeAtStep3'] = ['absorbanceMeasurementWaveLength', 'absorbanceMeasurementSolution'];
        $this->constraintsDependencies['oneAbsorbanceMeasureAtStep4'] = ['absorbanceMeasurementSolution'];
        $this->constraintsDependencies['oneAbsorbanceMeasureOnUnknownSolutionOrItsDilutionAtStep4'] = [];
        $this->constraintsDependencies['absorbanceMeasureOnUnknownSolutionOrItsDilutionShouldFallWithinSpectroRangeAtStep4'] = ['absorbanceMeasurementSolution', 'absorbanceMeasurementWaveLength'];
        $this->constraintsDependencies['absorbanceMeasureOnUnknownSolutionOrItsDilutionShouldFallWithinLinearRangeAtStep4'] = ['absorbanceMeasurementSolution', 'absorbanceMeasurementWaveLength'];
        $this->constraintsDependencies['dilutionSolventShouldMatchOriginSolutionSolvent'] = ['dilutionSolvent'];
        $this->constraintsDependencies['dilutionBaseSolutionShouldBeCollectedWithPrecisionMaterial'] = ['dilutionBaseSolutionMeasurer'];
        $this->constraintsDependencies['dilutionShouldBeProcessedWithGraduatedFlask'] = ['dilutionContainer'];
        $this->constraintsDependencies['graduatedFlaskShouldBeFilledToItsMark'] = ['dilutionContainer', 'dilutionSolutionCreatedVolumeAndUnit'];
        $this->constraintsDependencies['aGraduatedFlaskShouldBeConditioned'] = ['conditioningContainer', 'dilutionContainer', 'transferContainer'];
        $this->constraintsDependencies['aGraduatedFlaskShouldBeConditionedWithRelevantSolvent'] = ['conditioningContainer', 'dilutionContainer'];
        $this->constraintsDependencies['betweenTwoDilutionsSolutionShouldBeTransferred'] = ['dilutionSolutionCreated', 'transferSolution'];
        $this->constraintsDependencies['graduatedFlaskSolutionShouldBeTransferredToTube'] = ['transferContainer'];
        $this->constraintsDependencies['noMoreThanXTransfers'] = [];
        $this->constraintsDependencies['spectrophotometerTankShouldBeCorrectlyConditioned'] = ['conditioningContainer', 'absorbanceMeasurementSolution', 'conditioningSolution', 'spectrumProductionSolution'];
        $this->constraintsDependencies['aReferenceMustBeRecordedBeforeTheFirstMeasure'] = ['spectrumProductionSolution', 'absorbanceMeasurementSolution'];
        $this->constraintsDependencies['aReferenceShouldBeRecordedOnlyOnce'] = ['referenceSpectrumRecordingSolution'];
        $this->constraintsDependencies['referenceShouldBeRecordedWithDistilledWater'] = ['referenceSpectrumRecordingSolution'];
        $this->constraintsDependencies['noPipetConditioned'] = ['conditioningContainer'];
        $this->constraintsDependencies['eachAbsorbanceMeasurementShouldBeProcessedAtTheSameWaveLength'] = ['absorbanceMeasurementWaveLength'];
        $this->constraintsDependencies['noMoreThan4ConditioningOrMeasureOnSameDilutedSolution'] = ['dilutionSolutionCreated', 'conditioningSolution'];
    }
    
    public function getConstraintsFromVariable ($variableKeys)
    {
        return collect($this->constraintsDependencies)
            ->filter(function ($constraint) use ($variableKeys) {
                return collect($constraint)->contains(function ($variable) use ($variableKeys) {
                    return in_array($variable, $variableKeys);
                });
            })->keys();
    }

    public function createLabdocResponse ($labdocProcedure)
    {
        $materialResponse = $this->materialList->filter(fn($material) =>
                collect($material['properties'])->filter(fn($p)
                => $p['name'] === 'known' && $p['value'] == '0')->first()
            )->first();
        if ($materialResponse) {
            $properties = collect($materialResponse['properties']);
            $responseProperty = $properties->filter(fn($p) => $p['name'] === 'concentration')->first();
            $responseUnit = collect($this->quantities->filter(fn($q) =>
                    intval($q['id']) === intval($responseProperty['physicalQuantityId'])
                )->first()['unit']
                )->filter(fn($u, $k) =>
                    intval($k) === intval($responseProperty['unitId'])
                )->first()['symbol'];

            $hashedValue = str_split(json_encode($responseProperty['value'] * 100151));
            $letters = ['.' => 'A', '0' => 'Z', '1' => 'C', '2' => 'W', '3' => 'E', '4' => 'V', '5' => 'T', '6' => 'J', '7' => 'R', '8' => 'M', '9' => 'O'];
            $hashedValue = collect($hashedValue)->map(fn($l) => $letters[$l])->join('');
            $unit = $responseUnit[Helper::selectLanguage()];
            $text_data = <<<EOT
                <p>Concentration recherchée : <span contenteditable="true" style="display: inline-block; border: 1px solid grey; width: 100px;" data-simulation="$hashedValue" data-key="verificationOfTheRequiredConcentration"> </span> $unit</p>
            EOT;
            $assignment = <<<EOT
                <p><span>Donnez une réponse avec au moins 3 chiffres significatifs et utilisez le point comme séparateur décimal.</span> <span>La réponse correcte apparaît en vert.</span></p>
            EOT;
            $labdocText = Labdoc::init($labdocProcedure->report, [
                'id_report_part' => $labdocProcedure->id_report_part,
                'id_report' => $labdocProcedure->id_report,
                'type_labdoc' => 'text',
                'name' => __('Réponse à la question'),
                'position' => Labdoc::where('id_report_part', $labdocProcedure->id_report_part)
                        ->whereNull('id_ld_origin')
                        ->whereNull('id_report')
                        ->count() + 1,
                'editable' => 0,
                'editable_name' => 0,
                'deleteable' => 0,
                'draggable' => 0,
                'duplicatable' => 0,
                'assignment' => $assignment
            ]);
            $labdocText->labdoc_data = $text_data;
            $labdocText->save();
            $labdocText->fresh();
            return $labdocText;
        }
        return null;
    }
}

