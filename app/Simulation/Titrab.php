<?php

namespace App\Simulation;

use App\Helper;
use App\Labdoc;
use App\Simulation;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class Titrab extends Simulation
{
    protected array $phErrorModel;
    protected array $volumeErrorModel;
    protected int $irrelevantConstraintsNumber;
    protected array $learningGoalsGonfig;
    public function __construct(Collection $protocol, Collection $materialList, Collection $quantities)
    {
        $this->protocol = $protocol;
        $this->materialList = $materialList;
        $this->quantities = $quantities;
        $this->simulation_code = 'titrab';
        $this->phErrorModel = ['mean' => 0, 'std' => 0.01];
        $this->volumeErrorModel = ['mean' => 0, 'std' => 0.025];
        // Last level is duplicated
        $this->learningGoalsConfig = [
            0 => [5, 6],
            1 => [2, 3, 5, 6],
            2 => [1, 2, 3, 4, 5, 6],
        ];
        $this->irrelevantConstraintsNumber = 0;
    }

    public function getUrl($theoretical_feedback)
    {
        if (!$theoretical_feedback) {
            return "";
        }
        $data = explode(',', $theoretical_feedback);
        if ($data[0] == "notions") {
            $filename = "notions.pdf";
        } else if ($data[0] == "dilution") {
            $filename = "dilution.pdf";
        } else if ($data[0] == "elaborer" || $data[0] == "élaborer") {
            $filename = "elaborer.pdf";
        } else if ($data[0] == "conditionner") {
            $filename = "conditionner.pdf";
        }
        if (count($data) === 1) {
            $data = [$data[0], '1'];
        } elseif (count($data) !== 2) {
            $data = ['notions', '1'];
            $filename = "notions.pdf";
        }
        $url = "/tool_copex/v2_json/titrab/resources/$filename#page=$data[1]";
        return "<a class='lb-a' href='$url' target='_blank' rel='noopener noreferrer' 
            x-on:click.stop=\"proNS.simulation.traceSimulationUserAction('student_open_feedback_link', {url:'$url'})\">Lien vers le cours</a>";
    }

    public function replaceParameter($matches)
    {
        switch ($matches[1]) {
            case 'knownSolutions':
                return $this->getMaterialsByProperty('known',[1])
                    ->map(fn($material) => $material['name'])
                    ->implode(', ');
            case 'strongSolutions':
                return $this->getMaterialsByProperty('nature', ['acid', 'base'])
                    ->filter(function ($material) {
                        return count(collect($material['properties'])->where('name', '=', 'pka1')) === 0;
                    })->map(function ($material) {
                        return $material['name'];
                    })->implode(', ');
            case 'titratorNature':
                $nature = $this->getMaterialPropertyValue($this->getMaterialsByProperty('known',[1])->first(),'nature');
                if ($nature === 'acid') {
                    return __('une base forte');
                } else if ($nature === 'base') {
                    return __('un acide fort');
                }
                break;
            case 'titrandSolution':
                return $this->getMaterialsByProperty('known',[0])->first()['name'];
            case 'minBeakerVolume':
                $beaker = $this->getMaterialsByProperty('shortName', 'beaker')->first();
                $volume_beaker = floatval($this->getMaterialPropertyValue($beaker, 'volume'));
                $titrand_volume = floatval($this->getVariableValue('titrandVolume'));
                return ceil($volume_beaker / 3 - $titrand_volume);
            case 'maxBeakerVolume':
                $beaker = $this->getMaterialsByProperty('shortName', 'beaker')->first();
                return floatval($this->getMaterialPropertyValue($beaker, 'maxVolume'));
            case 'maxVolumeAddedToBeaker':
                $beaker = $this->getMaterialsByProperty('shortName', 'beaker')->first();
                $buret = $this->getMaterialsByProperty('shortName', 'burette')->first();
                $volume_max_beaker = floatval($this->getMaterialPropertyValue($beaker, 'maxVolume'));
                $volume_buret = floatval($this->getMaterialPropertyValue($buret, 'volume'));
                $titrand_volume = floatval($this->getVariableValue('titrandVolume'));
                return $volume_max_beaker - $volume_buret - $titrand_volume;
            case 'buretVolume':
                $buret = $this->getMaterialsByProperty('shortName', 'burette')->first();
                return floatval($this->getMaterialPropertyValue($buret, 'volume'));
            case 'titrandVolume':
                return floatval($this->getVariableValue('titrandVolume'));
            case 'titrandSolventVolume':
                return floatval($this->getVariableValue('titrandSolventVolume'));
        }
    }
    public function getSimulationData($brokenConstraints)
    {
        if($brokenConstraints->count()>0) {
            return null;
        }
        $points = [];
        $phMin = 0;
        $phMax = 14;
        $phStep = 0.01;
        $volumeFromPh = null;

        // Get the necessary actions
        $titrandAction = $this->protocol->first(fn ($a) => $a['structuredAction'] === 'addLiquid'
            && in_array($this->getMaterialPropertyValue($a['addLiquidSolution'], 'nature'), ['acid', 'base']));
        $titratorAction = $this->protocol->first(fn ($a) => $a['structuredAction'] === 'fill'
            && in_array($this->getMaterialPropertyValue($a['fillSolution'], 'nature'), ['acid', 'base']));
        $solventActions = $this->protocol->filter( fn ($a) => $a['structuredAction'] === 'addLiquid'
            && $this->getMaterialPropertyValue($a['addLiquidSolution'], 'shortName') === 'distilledWater' );
        $dropLiquidAction = $this->protocol->first(fn($sa) => $sa['structuredAction'] === 'dropLiquid');
        
        if(!$titrandAction || !$titratorAction || !$dropLiquidAction) {
            return null;
        }
        $beaker = $this->getMaterialsByProperty('shortName', 'burette')->first();
        $maxDropLiquid = floatval($this->getMaterialPropertyValue($beaker, 'volume'));
        
        // Titrand values
        $titrandSolution = $titrandAction['addLiquidSolution'];
        $titrandVolume = floatval($titrandAction['addLiquidTotalVolumeAndUnit'][0]);
        $titrandConcentration = floatval($this->getMaterialPropertyValue($titrandSolution, 'concentration'));
        // Titrator values
        $titratorConcentration = floatval($this->getMaterialPropertyValue($titratorAction['fillSolution'], 'concentration'));
        // Solvent values
        if ($solventActions->count() === 0) {
            $solventVolume = 0;
        } else {
            $solventVolume = $solventActions->sum(fn($sa) => floatval($sa['addLiquidTotalVolumeAndUnit'][0]));
        }
        $indicatorAction = $this->protocol->last(fn($sa) => $sa['structuredAction'] === 'colorize' 
            && array_key_exists('colorizeIndicator', $sa) && $this->getMaterialPropertyValue($sa['colorizeIndicator'], 'nature') === 'indicator');
        $beaker = $this->getMaterialsByProperty('shortName', 'beaker')->first();
        $maxTitratorVolume = min(
            $maxDropLiquid,
                floatval($this->getMaterialPropertyValue($beaker, 'maxVolume')) - $titrandVolume - $solventVolume
        );

        if ($this->getMaterialPropertyValue($titrandSolution, 'nature') === "acid") {
            for ($ph = $phMin; $ph <= $phMax; $ph += $phStep) {
                if (!$this->getMaterialPropertyValue($titrandSolution, 'pka1')) {
                    // Strong acid titrated by a strong base
                    $volumeFromPh = (($titrandVolume + $solventVolume) * (pow(10, -14) / pow(10, -$ph) - pow(10, -$ph))
                            + $titrandConcentration * $titrandVolume) / (pow(10, -$ph) + $titratorConcentration - pow(10, -14) / pow(10, -$ph));
                } elseif (!$this->getMaterialPropertyValue($titrandSolution, 'pka2')) {
                    // Weak acid titrated by a strong base
                    $volumeFromPh = (($titrandVolume + $solventVolume) * (pow(10, -14) / pow(10, -$ph) - pow(10, -$ph))
                            + $titrandConcentration * $titrandVolume / (1 + pow(10, -$ph) / pow(10, -$this->getMaterialPropertyValue($titrandSolution, 'pka1'))))
                        / (pow(10, -$ph) + $titratorConcentration - pow(10, -14) / pow(10, -$ph));
                }
                if ($volumeFromPh >= 0 && $volumeFromPh <= $maxTitratorVolume) {
                    $points[] = [strval($volumeFromPh + $this->nRand($this->volumeErrorModel)), strval($ph + $this->nRand($this->phErrorModel))];
                }
            }
        } elseif ($this->getMaterialPropertyValue($titrandSolution, 'nature') === "base") {
            for ($ph = $phMax; $ph >= $phMin; $ph -= $phStep) {
                if (!$this->getMaterialPropertyValue($titrandSolution, 'pka1')) {
                    // Strong base titrated by a strong acid
                    $volumeFromPh = (($titrandVolume + $solventVolume) * (-pow(10, -14) / pow(10, -$ph) + pow(10, -$ph))
                            + $titrandConcentration * $titrandVolume) / ($titratorConcentration + pow(10, -14) / pow(10, -$ph) - pow(10, -$ph));
                } elseif (!$this->getMaterialPropertyValue($titrandSolution, 'pka2')) {
                    // Weak base titrated by a strong acid
                    $volumeFromPh = (($titrandVolume + $solventVolume) * (-pow(10, -14) / pow(10, -$ph) + pow(10, -$ph))
                            + $titrandConcentration * $titrandVolume / (1 + pow(10, -$this->getMaterialPropertyValue($titrandSolution, 'pka1')) / pow(10, -$ph)))
                        / ($titratorConcentration + pow(10, -14) / pow(10, -$ph) - pow(10, -$ph));
                }
                if ($volumeFromPh >= 0 && $volumeFromPh <= $maxTitratorVolume) {
                    $points[] = [strval($volumeFromPh + $this->nRand($this->volumeErrorModel)), strval($ph + $this->nRand($this->phErrorModel))];
                }
            }
        }

        // Handle the indicator changes
        $indicatorVolume1 = null;
        $indicatorVolume2 = null;
        $indicatorColor1 = null;
        $indicatorColor2 = null;
        $indicatorColor3 = null;

        if ($indicatorAction && count($points) > 0) {
            $indicator = $indicatorAction['colorizeIndicator'];
            $ph1Indicator = floatval($this->getMaterialPropertyValue($indicator, 'ph1'));
            $ph2Indicator = floatval($this->getMaterialPropertyValue($indicator, 'ph2'));
            $ph0 = floatval($points[0][1]);

            for ($i = 0; $i < count($points) - 1; $i++) {
                $phi = floatval($points[$i][1]);
                $vi = floatval($points[$i][0]);
                $phip1 = floatval($points[$i + 1][1]);
                $vip1 = floatval($points[$i + 1][0]);

                if (($phi - $ph1Indicator) * ($phip1 - $ph1Indicator) < 0) {
                    $indicatorVolume1 = ($vi + $vip1) / 2;
                }
                if (($phi - $ph2Indicator) * ($phip1 - $ph2Indicator) < 0) {
                    $indicatorVolume2 = ($vi + $vip1) / 2;
                }
            }

            // Determine indicator colors based on volumes
            if (!$indicatorVolume1 && !$indicatorVolume2) {
                if ($ph0 <= $ph1Indicator) {
                    $indicatorColor3 = $this->getMaterialPropertyValue($indicator, 'color1');
                } elseif ($ph0 <= $ph2Indicator) {
                    $indicatorColor3 = $this->getMaterialPropertyValue($indicator, 'color2');
                } else {
                    $indicatorColor3 = $this->getMaterialPropertyValue($indicator, 'color3');
                }
            } elseif (!$indicatorVolume1) {
                if ($ph0 <= $ph2Indicator) {
                    $indicatorColor2 = $this->getMaterialPropertyValue($indicator, 'color2');
                    $indicatorColor3 = $this->getMaterialPropertyValue($indicator, 'color3');
                } else {
                    $indicatorColor2 = $this->getMaterialPropertyValue($indicator, 'color3');
                    $indicatorColor3 = $this->getMaterialPropertyValue($indicator, 'color2');
                }
            } elseif (!$indicatorVolume2) {
                if ($ph0 <= $ph1Indicator) {
                    $indicatorColor2 = $this->getMaterialPropertyValue($indicator, 'color1');
                    $indicatorColor3 = $this->getMaterialPropertyValue($indicator, 'color2');
                } else {
                    $indicatorColor2 = $this->getMaterialPropertyValue($indicator, 'color2');
                    $indicatorColor3 = $this->getMaterialPropertyValue($indicator, 'color1');
                }
            } else {
                $indicatorColor1 = $this->getMaterialPropertyValue($indicator, 'color1');
                $indicatorColor2 = $this->getMaterialPropertyValue($indicator, 'color2');
                $indicatorColor3 = $this->getMaterialPropertyValue($indicator, 'color3');
            }
        }
        
        // Return data for display
        return [
            'points' => $points,
            'equivalentVolume' => $this->computeVeq(),
            'maxDropLiquid' => $maxDropLiquid,
            'minDropStep' => floatval($dropLiquidAction['dropLiquidMinVolumeAndUnitFraction'][0]),
            'maxDropStep' => floatval($dropLiquidAction['dropLiquidMaxVolumeAndUnitFraction'][0]),
            'indicatorParameters' => [
                'volume1' => $indicatorVolume1,
                'volume2' => $indicatorVolume2,
                'color1' => $indicatorColor1,
                'color2' => $indicatorColor2,
                'color3' => $indicatorColor3
            ],
            'titrandVolume' => $titrandVolume,
            'titrandName' => $titrandSolution['name'],
            'titratorConcentration' => $titratorConcentration,
            'titratorName' => $titratorAction['fillSolution']['name'],
            'solventVolume' => $solventVolume,
            'phMin' => $phMin,
            'phMax' => $phMax
        ];
    }
    
    public function getSimulationTraceInfo($simulation_data) {
        return ['equivalentVolume'=>number_format(floatval($simulation_data['equivalentVolume']), 3, '.', "")];
    } 
    public function checkConstraints($variableList=null)
    {
        $error_msg = '';
        
        $beaker = $this->getMaterialsByProperty('shortName', 'beaker')->first();
        $burette = $this->getMaterialsByProperty('shortName','burette')->first();

        $addLiquidActionsToBeaker = $this->protocol->filter(fn ($a) => $a['structuredAction'] === 'addLiquid');
        $dilutionActions = $this->protocol->filter(fn($sa) => $sa['structuredAction'] === 'dilute');
        $dropLiquidActions = $this->protocol->filter(fn($sa) => $sa['structuredAction'] === 'dropLiquid');
        $fillBuretteActions = $this->protocol->filter(fn ($a) => $a['structuredAction'] === 'fill');
        $measureActions = $this->protocol->filter(fn($a) => $a['structuredAction'] === 'measure');
        
        $isDilutionNecessary = $this->dilutionIsNecessary();

        foreach ($this->constraints as $constraint) {
            $failed_relevance_condition = $this->constraintCheckingIsNotRelevant($constraint);
            if(!$failed_relevance_condition) {
                switch ($constraint->satisfaction_condition_key) {
                    case 'atLeastOneFillBuretteActionInStep1':
                        $fillBuretteActionsInStep1 = $fillBuretteActions
                            ->filter(fn($action) => array_key_exists('stepSimulationVariable', $action))
                            ->where( 'stepSimulationVariable', 'buretteSetup');
                        $constraint['current_state'] = +($fillBuretteActionsInStep1->count() > 0);
                        $constraint['nodesId'] = $fillBuretteActionsInStep1->pluck('nodeId')->all();
                        $constraint['simulationVariables'] = ['fillSolution'];
                        break;
                    case 'atLeastOneAddLiquidBeakerActionInStep2':
                        $addLiquidActionsToBeakerInStep2 = $addLiquidActionsToBeaker
                            ->filter(fn($action) => array_key_exists('stepSimulationVariable', $action))
                            ->where( 'stepSimulationVariable', 'beakerSetup');
                        $constraint['current_state'] = +($addLiquidActionsToBeakerInStep2->count() > 0);
                        $constraint['nodesId'] = $addLiquidActionsToBeakerInStep2->pluck('nodeId')->all();
                        $constraint['simulationVariables'] = ['addLiquidSolution'];
                        break;
                    case 'exactlyOneBuretteFillingAction':
                        $constraint['current_state'] = +($fillBuretteActions->count() === 1);
                        $constraint['nodesId'] = $fillBuretteActions->pluck('nodeId')->all();
                        $constraint['simulationVariables'] = ['fillSolution'];
                        break;
                    case 'titrandSolutionInBeakerOrFillBurette':
                        $titrandInBeakerActions = $addLiquidActionsToBeaker->filter(fn($sa) => $this->getMaterialPropertyValue($sa['addLiquidSolution'], 'known') === "0");
                        $titrandInBuretteActions = $fillBuretteActions->filter(fn($sa) => $this->getMaterialPropertyValue($sa['fillSolution'], 'known') === "0");
                        $constraint['current_state'] = +($titrandInBeakerActions->count() > 0 || $titrandInBuretteActions->count() > 0);
                        $constraint['nodesId'] = $titrandInBeakerActions->concat($titrandInBuretteActions)->pluck('nodeId')->all();
                        $constraint['simulationVariables'] = ['fillSolution', 'addLiquidSolution'];
                        break;
                    case 'buretteLiquidShouldBeOppositeNature':
                        $addLiquidActionToBeaker = $addLiquidActionsToBeaker->first(fn($sa) => in_array($this->getMaterialPropertyValue($sa['addLiquidSolution'], 'nature'), ['acid', 'base']));
                        $fillActionToBurette = $fillBuretteActions->first();
                        $constraint['current_state'] = +($this->getMaterialPropertyValue($addLiquidActionToBeaker['addLiquidSolution'], 'nature')
                            !== $this->getMaterialPropertyValue($fillActionToBurette['fillSolution'], 'nature'));
                        $constraint['nodesId'] = [$addLiquidActionToBeaker['nodeId'], $fillActionToBurette['nodeId']];
                        $constraint['simulationVariables'] = ['addLiquidSolution', 'fillSolution'];
                        break;
                    case 'beakerSolutionConcentrationIsUnknown':
                        $addUnknownSolutionToBeaker = $addLiquidActionsToBeaker->filter(fn($sa) => $this->getMaterialPropertyValue($sa['addLiquidSolution'], 'known') === "0");
                        $constraint['current_state'] = +($addUnknownSolutionToBeaker->count() > 0);
                        $constraint['nodesId'] = $addUnknownSolutionToBeaker->pluck('nodeId')->all();
                        $constraint['simulationVariables'] = ['addLiquidSolution'];
                        break;
                    case 'buretteSolutionShouldBeStrongAcidOrBase':
                        $fillBuretteAction = $fillBuretteActions->first();
                        $constraint['current_state'] = +($this->getMaterialPropertyValue($fillBuretteAction['fillSolution'], 'pka1') === null);
                        $constraint['nodesId'] = [$fillBuretteAction['nodeId']];
                        $constraint['simulationVariables'] = ['fillSolution'];
                        break;
                    case 'beakerShouldOnlyContainOneAcidOrBaseOrWater':
                        $addIrrelevantLiquidToBeaker = $addLiquidActionsToBeaker
                            ->filter(fn ($sa) => (
                                !in_array($this->getMaterialPropertyValue($sa['addLiquidSolution'], 'nature'), ['acid', 'base'])
                                && $this->getMaterialPropertyValue($sa['addLiquidSolution'], 'shortName') !== 'distilledWater'
                            )
                            );
                        $constraint['current_state'] = +(
                            ((int)($addLiquidActionsToBeaker
                                        ->filter(fn($sa) => $this->getMaterialPropertyValue($sa['addLiquidSolution'], 'nature') === 'acid')
                                        ->map(fn($sa) => $sa['addLiquidSolution']['id'])
                                        ->unique()
                                        ->count() === 1)
                                + (int)($addLiquidActionsToBeaker
                                        ->filter(fn($sa) => $this->getMaterialPropertyValue($sa['addLiquidSolution'], 'nature') === 'base')
                                        ->map(fn($sa) => $sa['addLiquidSolution']['id'])
                                        ->unique()
                                        ->count() === 1)
                                === 1) && $addIrrelevantLiquidToBeaker->count() === 0);
                        $constraint['nodesId'] = $addIrrelevantLiquidToBeaker->pluck('nodeId')->all();
                        $constraint['simulationVariables'] = ['addLiquidSolution'];
                        break;
                    case 'addUnknownSolutionToBeakerMeasurerShouldBePipette':
                        $addUnknownSolutionToBeakerWithoutPipette = $addLiquidActionsToBeaker
                            ->filter(fn($sa) =>
                                $this->getMaterialPropertyValue($sa['addLiquidSolution'], 'known') === "0"
                                && $this->getMaterialPropertyValue($sa['addLiquidMeasurer'], 'shortName') !== 'pipette'
                            );
                        $constraint['current_state'] = +($addUnknownSolutionToBeakerWithoutPipette->count() === 0);
                        $constraint['nodesId'] = $addUnknownSolutionToBeakerWithoutPipette->pluck('nodeId')->all();
                        $constraint['simulationVariables'] = ['addLiquidMeasurer'];
                        break;
                    case 'addWaterToBeakerMeasurerShouldBeTestTube':
                        $addWaterToBeakerActions = $addLiquidActionsToBeaker->filter(fn($sa) =>
                                $this->getMaterialPropertyValue($sa['addLiquidSolution'], 'shortName') === "distilledWater");
                        if($addWaterToBeakerActions->count() > 0) {
                            $addWaterToBeakerWithoutTestTubeActions = $addWaterToBeakerActions
                                ->filter(fn($sa) => $this->getMaterialPropertyValue($sa['addLiquidMeasurer'], 'shortName') !== 'eprouvette');
                            $constraint['current_state'] = +($addWaterToBeakerWithoutTestTubeActions->count() === 0);
                            $constraint['nodesId'] = $addWaterToBeakerWithoutTestTubeActions->pluck('nodeId')->all();
                            $constraint['simulationVariables'] = ['addLiquidMeasurer'];
                        } else {
                            $constraint['current_state'] = null;
                            $constraint['nodesId'] = null;
                            $constraint['simulationVariables'] = null; 
                        }
                        break;
                    case 'addLiquidVolumeMustMatchPipetteVolume':
                        $addSolutionWithPipetteWithIrrelevantVolume = $addLiquidActionsToBeaker->filter(fn($sa) => 
                            $this->getMaterialPropertyValue($sa['addLiquidMeasurer'], 'shortName') === 'pipette'
                            && floatval($this->getMaterialPropertyValue($sa['addLiquidMeasurer'], 'volume')) !== floatval($sa['addLiquidTotalVolumeAndUnit'][0])
                        );
                        $constraint['current_state'] = +($addSolutionWithPipetteWithIrrelevantVolume->count() === 0);
                        $constraint['nodesId'] = $addSolutionWithPipetteWithIrrelevantVolume->pluck('nodeId')->all();
                        $constraint['simulationVariables'] = ['addLiquidTotalVolumeAndUnit', 'addLiquidMeasurer'];
                        break;
                    case 'addLiquidVolumeShouldFitBeakerVolume':
                        $irrelevantAddLiquidVolume = $addLiquidActionsToBeaker->filter(fn($sa) =>
                                floatval($this->getMaterialPropertyValue($beaker, 'volume')) < floatval($sa['addLiquidTotalVolumeAndUnit'][0])
                            );
                        $constraint['current_state'] = +($irrelevantAddLiquidVolume->count() === 0);
                        $constraint['nodesId'] = $irrelevantAddLiquidVolume->pluck('nodeId')->all();
                        $constraint['simulationVariables'] = ['addLiquidTotalVolumeAndUnit'];
                        break;
                    case 'atMostOneDilution':
                        if($isDilutionNecessary) {
                            $constraint['current_state'] = +($dilutionActions->count() <= 1);
                            $constraint['nodesId'] = $dilutionActions->pluck('nodeId')->all();
                            $constraint['simulationVariables'] = ["diluteContainer", "diluteVolumeAndUnit", "diluteSolution", "diluteMeasurer", "diluteSolvent", "diluteCreated"];
                        } else {
                            $this->irrelevantConstraintsNumber += 1;
                            $constraint['current_state'] = null;
                            $constraint['nodesId'] = null;
                            $constraint['simulationVariables'] = null;
                        }
                        break;
                    case 'dilutionContainerMustBeGraduatedFlask':
                        if ($dilutionActions->count() > 0) {
                            $irrelevantDilutionActions = $dilutionActions
                                ->filter(fn($sa) => $this->getMaterialPropertyValue($sa['diluteContainer'], 'shortName') !== 'graduatedFlask'
                                );
                            $constraint['current_state'] = +($irrelevantDilutionActions->count() === 0);
                            $constraint['nodesId'] = $irrelevantDilutionActions->pluck('nodeId')->all();
                            $constraint['simulationVariables'] = ['diluteContainer'];
                        } else {
                            $this->irrelevantConstraintsNumber += ($isDilutionNecessary?0:1);
                            $constraint['current_state'] = null;
                            $constraint['nodesId'] = null;
                            $constraint['simulationVariables'] = null;
                        }
                        break;
                    case 'dilutionMeasurerShouldBePipette':
                        if ($dilutionActions->count() >0) {
                            $irrelevantDilutionActions = $dilutionActions
                                ->filter(fn($sa) => $this->getMaterialPropertyValue($sa['diluteMeasurer'], 'shortName') !== 'pipette'
                                );
                            $constraint['current_state'] = +($irrelevantDilutionActions->count() === 0);
                            $constraint['nodesId'] = $irrelevantDilutionActions->pluck('nodeId')->all();
                            $constraint['simulationVariables'] = ['diluteMeasurer'];
                        } else {
                            $this->irrelevantConstraintsNumber += ($isDilutionNecessary?0:1);
                            $constraint['current_state'] = null;
                            $constraint['nodesId'] = null;
                            $constraint['simulationVariables'] = null;
                        }
                        break;
                    case 'dilutionSolventShouldBeDistilledWater':
                        if ($dilutionActions->count() >0) {
                            $irrelevantDilutionActions = $dilutionActions
                                ->filter(fn($sa) => $this->getMaterialPropertyValue($sa['diluteSolvent'], 'shortName') !== 'distilledWater'
                                );
                            $constraint['current_state'] = +($irrelevantDilutionActions->count() === 0);
                            $constraint['nodesId'] = $irrelevantDilutionActions->pluck('nodeId')->all();
                            $constraint['simulationVariables'] = ['diluteMeasurer'];
                        } else {
                            $this->irrelevantConstraintsNumber += ($isDilutionNecessary?0:1);
                            $constraint['current_state'] = null;
                            $constraint['nodesId'] = null;
                            $constraint['simulationVariables'] = null;
                        }
                        break;
                    case 'dilutionBaseSolutionVolumeShouldFitGraduatedFlaskVolume':
                        if ($dilutionActions->count() >0) {
                            $irrelevantDilutionActions = $dilutionActions
                                ->filter(fn($sa) => floatval($this->getMaterialPropertyValue($sa['diluteContainer'], 'volume')) !== floatval($sa['diluteFullVolumeAndUnit'][0])
                                );
                            $constraint['current_state'] = +($irrelevantDilutionActions->count() === 0);
                            $constraint['nodesId'] = $irrelevantDilutionActions->pluck('nodeId')->all();
                            $constraint['simulationVariables'] = ['diluteContainer', 'diluteFullVolumeAndUnit'];
                        } else {
                            $this->irrelevantConstraintsNumber += ($isDilutionNecessary?0:1);
                            $constraint['current_state'] = null;
                            $constraint['nodesId'] = null;
                            $constraint['simulationVariables'] = null;
                        }
                        break;
                    case 'dilutionSolutionVolumeShouldBeSufficient':
                        if ($dilutionActions->count() >0) {
                            foreach ($dilutionActions as $dilutionAction) {
                                $volumePrepared = floatval($dilutionAction['diluteFullVolumeAndUnit'][0]);
                                $idCreatedSolution = intval($dilutionAction['diluteCreated']['id']);
                                $volumeUsed = 0;
                                $usingSolutionActions = $this->protocol->filter(fn($sa) => in_array($sa['structuredAction'], ['conditioning', 'fill', 'addLiquid']));
                                foreach ($usingSolutionActions as $action) {
                                    if($action['structuredAction'] === 'conditioning'
                                        && intval($action['conditioningSolution']['nodeId']) === $idCreatedSolution) {
                                        $volumeUsed += floatval($this->getMaterialPropertyValue($action['conditioningContainer'], 'volume'));
                                    } else if ($action['structuredAction'] === 'fill'
                                        && intval($action['fillSolution']['id']) === $idCreatedSolution) {
                                        $volumeUsed += floatval($this->getMaterialPropertyValue($burette, 'volume'));
                                    } else if($action['structuredAction'] === 'addLiquid'
                                        && intval($action['addLiquidSolution']['id']) === $idCreatedSolution) {
                                        $volumeUsed += floatval($action['addLiquidTotalVolumeAndUnit'][0]);
                                    }
                                }
                                if ($volumeUsed > $volumePrepared) {
                                    $constraint['current_state'] = 0;
                                    $constraint['nodesId'] = [$dilutionAction['nodeId']];
                                    $constraint['simulationVariables'] = ['diluteSolution', 'diluteFullVolumeAndUnit'];
                                    break 2;
                                }
                            }
                            $constraint['current_state'] = 1;
                        } else {
                            $this->irrelevantConstraintsNumber += ($isDilutionNecessary?0:1);
                            $constraint['current_state'] = null;
                        }
                        $constraint['nodesId'] = null;
                        $constraint['simulationVariables'] = null;
                        break;
                    case 'reusableMaterialShouldBeConditioned':
                        $testConditioningRelevant = $this->protocol->filter(fn($sa) => in_array($sa['structuredAction'], ['dilute', 'fill', 'addLiquid']))->count() > 0;
                        $nodeIds = [];
                        $simulationVariables = [];
                        if($testConditioningRelevant) {
                            $reusableMaterials = ['beaker', 'burette', 'eprouvette', 'pipette', 'graduatedFlask'];
                            foreach ($reusableMaterials as $reusableMaterial) {
                                $conditioningAction = null;
                                foreach ($this->protocol as $action) {
                                    if ($action['structuredAction'] === 'conditioning'
                                        && $this->getMaterialPropertyValue($action['conditioningContainer'], 'shortName') === $reusableMaterial) {
                                        $conditioningAction = $action;
                                    }
                                    // Case fill burette action
                                    if($action['structuredAction'] === 'fill' && $reusableMaterial === 'burette') {
                                        if(!$conditioningAction) {
                                            $nodeIds[] = $action['nodeId'];
                                            $simulationVariables[] = 'fillSolution';
                                            break;
                                        } else {
                                            $conditioningAction = null;
                                        }
                                    }
                                    // Case add liquid beaker action
                                    if($action['structuredAction'] === 'addLiquid' && ( $reusableMaterial === 'beaker'
                                            || $this->getMaterialPropertyValue($action['addLiquidMeasurer'], 'shortName') === $reusableMaterial )) {
                                        // Beaker need to be conditioned only once so we keep the conditioning action
                                        if(!$conditioningAction) {
                                            $nodeIds[] = $action['nodeId'];
                                            $simulationVariables[] = 'addLiquidSolution';
                                            break;
                                        }
                                    }
                                    // Case dilution
                                    if($action['structuredAction'] === 'dilute' &&
                                        ( $this->getMaterialPropertyValue($action['diluteContainer'], 'shortName') === $reusableMaterial
                                            || $this->getMaterialPropertyValue($action['diluteMeasurer'], 'shortName') === $reusableMaterial)) {
                                        if( $conditioningAction) {
                                            $conditioningAction = null;
                                        } else {
                                            $nodeIds[] = $action['nodeId'];
                                            $simulationVariables[] = 'diluteContainer';
                                            $simulationVariables[] = 'diluteMeasurer';
                                            break;
                                        }
                                    }
                                }
                            }
                            $constraint['current_state'] = count($nodeIds)>0?0:1;
                            $constraint['nodesId'] = $nodeIds;
                            $constraint['simulationVariables'] = $simulationVariables;
                        } else {
                            $constraint['current_state'] = null;
                            $constraint['nodesId'] = null;
                            $constraint['simulationVariables'] = null;
                        }
                        break;
                    case 'pipetteConditioningSolutionMustBePipetteSolution':
                        // Relevant to test if a pipette is conditioned
                        $testConditioningRelevant = $this->protocol->filter(fn($sa) => 
                                $sa['structuredAction'] === 'conditioning' 
                                && $this->getMaterialPropertyValue($sa['conditioningContainer'], 'shortName') === 'pipette'
                            )->count() > 0;
                        if($testConditioningRelevant) {
                            $conditioningAction = null;
                            foreach ($this->protocol as $action) {
                                if ($action['structuredAction'] === 'conditioning'
                                    && $this->getMaterialPropertyValue($action['conditioningContainer'], 'shortName') === 'pipette') {
                                    $conditioningAction = $action;
                                }
                                if ($action['structuredAction'] === 'dilute'
                                    && ($this->getMaterialPropertyValue($action['diluteContainer'], 'shortName') === 'pipette'
                                        || $this->getMaterialPropertyValue($action['diluteMeasurer'], 'shortName') === 'pipette')) {
                                    if ($conditioningAction 
                                        && intval($action['diluteSolution']['id']) === intval($conditioningAction['conditioningSolution']['id'])) {
                                        $conditioningAction = null;
                                    } else {
                                        $constraint['current_state'] = 0;
                                        $constraint['nodesId'] = [$action['nodeId']];
                                        $constraint['simulationVariables'] = ['diluteContainer', 'diluteMeasurer'];
                                        break 2;
                                    }
                                }
                                if($action['structuredAction'] === 'addLiquid' 
                                    && $this->getMaterialPropertyValue($action['addLiquidMeasurer'], 'shortName') === 'pipette') {
                                    if($conditioningAction 
                                        && intval($action['addLiquidSolution']['id']) === intval($conditioningAction['conditioningSolution']['id'])) {
                                        $conditioningAction = null;
                                    } else {
                                        $constraint['current_state'] = 0;
                                        $constraint['nodesId'] = [$action['nodeId']];
                                        $constraint['simulationVariables'] = ['addLiquidMeasurer'];
                                        break 2;
                                    }
                                }
                            }
                            $constraint['current_state'] = 1;
                        } else {
                            $constraint['current_state'] = null;
                        }
                        $constraint['nodesId'] = null;
                        $constraint['simulationVariables'] = null;
                        break;
                    case 'graduatedFlaskConditioningSolutionMustBeWater':
                        // Relevant to test if a graduated flask is conditioned
                        $testConditioningRelevant = $this->protocol->filter(fn($sa) =>
                                $sa['structuredAction'] === 'conditioning'
                                && $this->getMaterialPropertyValue($sa['conditioningContainer'], 'shortName') === 'graduatedFlask'
                            )->count() > 0;
                        if($testConditioningRelevant) {
                            $conditioningAction = null;
                            foreach ($this->protocol as $action) {
                                if($action['structuredAction'] === 'conditioning'
                                    && $this->getMaterialPropertyValue($action['conditioningContainer'], 'shortName') === 'graduatedFlask'){
                                    $conditioningAction = $action;
                                }
                                if ($action['structuredAction'] === 'dilute'
                                    && ($this->getMaterialPropertyValue($action['diluteContainer'], 'shortName') === 'graduatedFlask'
                                        || $this->getMaterialPropertyValue($action['diluteMeasurer'], 'shortName') === 'graduatedFlask')) {
                                    if ($conditioningAction
                                        && $this->getMaterialPropertyValue($conditioningAction['addLiquidMeasurer'], 'shortName') === 'distilledWater') {
                                        $conditioningAction = null;
                                    } else {
                                        $constraint['current_state'] = 0;
                                        $constraint['nodesId'] = [$action['nodeId']];
                                        $constraint['simulationVariables'] = ['diluteContainer', 'diluteMeasurer'];
                                        break 2;
                                    }
                                }
                                if($action['structuredAction'] === 'addLiquid'
                                    && $this->getMaterialPropertyValue($action['addLiquidMeasurer'], 'shortName') === 'graduatedFlask') {
                                    if($conditioningAction
                                        && $this->getMaterialPropertyValue($conditioningAction['addLiquidMeasurer'], 'shortName') === 'distilledWater') {
                                        $conditioningAction = null;
                                    } else {
                                        $constraint['current_state'] = 0;
                                        $constraint['nodesId'] = [$action['nodeId']];
                                        $constraint['simulationVariables'] = ['addLiquidMeasurer'];
                                        break 2;
                                    }
                                }
                            }
                            $constraint['current_state'] = 1;
                        } else {
                            $this->irrelevantConstraintsNumber += ($isDilutionNecessary?0:1);
                            $constraint['current_state'] = null;
                        }
                        $constraint['nodesId'] = null;
                        $constraint['simulationVariables'] = null;
                        break;
                    case 'beakerConditioningSolutionMustBeWater':
                        // Relevant to test if a beaker is conditioned
                        $testConditioningRelevant = $this->protocol->filter(fn($sa) =>
                                $sa['structuredAction'] === 'conditioning'
                                && $this->getMaterialPropertyValue($sa['conditioningContainer'], 'shortName') === 'beaker'
                            )->count() > 0;
                        if($testConditioningRelevant) {
                            $conditioningAction = null;
                            foreach ($this->protocol as $action) {
                                if($action['structuredAction'] === 'conditioning'
                                    && $this->getMaterialPropertyValue($action['conditioningContainer'], 'shortName') === 'beaker'){
                                    $conditioningAction = $action;
                                }
                                if($action['structuredAction'] === 'addLiquid') {
                                    if(!$conditioningAction
                                        || $this->getMaterialPropertyValue($conditioningAction['conditioningSolution'], 'shortName') !== 'distilledWater'
                                    ) {
                                        // Beaker need to be conditioned only once so we keep the conditioning action
                                        $constraint['current_state'] = 0;
                                        $constraint['nodesId'] = [$action['nodeId']];
                                        $constraint['simulationVariables'] = ['diluteContainer', 'diluteMeasurer'];
                                        break 2;
                                    }
                                }
                            }
                            $constraint['current_state'] = 1;
                        } else {
                            $constraint['current_state'] = null;
                        }
                        $constraint['nodesId'] = null;
                        $constraint['simulationVariables'] = null;
                        break;
                    case 'buretteConditioningSolutionMustFilledIt':
                        // Relevant to test if a burette is conditioned
                        $testConditioningRelevant = $this->protocol->filter(fn($sa) =>
                                $sa['structuredAction'] === 'conditioning'
                                && $this->getMaterialPropertyValue($sa['conditioningContainer'], 'shortName') === 'burette'
                            )->count() > 0;
                        if($testConditioningRelevant) {
                            $conditioningAction = null;
                            foreach ($this->protocol as $action) {
                                if($action['structuredAction'] === 'conditioning'
                                    && $this->getMaterialPropertyValue($action['conditioningContainer'], 'shortName') === 'burette'){
                                    $conditioningAction = $action;
                                }
                                if($action['structuredAction'] === 'fill') {
                                    if($conditioningAction
                                        && intval($action['fillSolution']['id'])
                                        === intval($conditioningAction['conditioningSolution']['id'])
                                    ) {
                                        $conditioningAction = null;
                                    } else {
                                        $constraint['current_state'] = 0;
                                        $constraint['nodesId'] = [$action['nodeId']];
                                        $constraint['simulationVariables'] = ['diluteContainer', 'diluteMeasurer'];
                                        break 2;
                                    }
                                }
                            }
                            $constraint['current_state'] =1;
                        } else {
                            $constraint['current_state'] = null;
                        }
                        $constraint['nodesId'] = null;
                        $constraint['simulationVariables'] = null;
                        break;
                    case 'testTubeConditioningSolutionMustBeWaterOrAppropriateSolution':
                        // Relevant to test if a test tube is conditioned
                        $testConditioningRelevant = $this->protocol->filter(fn($sa) =>
                                $sa['structuredAction'] === 'conditioning'
                                && $this->getMaterialPropertyValue($sa['conditioningContainer'], 'shortName') === 'eprouvette'
                            )->count() > 0;
                        if($testConditioningRelevant) {
                            $conditioningAction = null;
                            foreach ($this->protocol as $action) {
                                if($action['structuredAction'] === 'conditioning'
                                    && $this->getMaterialPropertyValue($action['conditioningContainer'], 'shortName') === 'eprouvette'){
                                    $conditioningAction = $action;
                                }
                                if($action['structuredAction'] === 'addLiquid'
                                    && $this->getMaterialPropertyValue($action['addLiquidMeasurer'], 'shortName') === 'eprouvette') {
                                    if($conditioningAction
                                        && (intval($action['addLiquidSolution']['id']) === intval($conditioningAction['conditioningSolution']['id'])
                                            || $this->getMaterialPropertyValue($conditioningAction['conditioningSolution'], 'shortName') === 'distilledWater')
                                    ) {
                                        $conditioningAction = null;
                                    } else {
                                        $constraint['current_state'] = 0;
                                        $constraint['nodesId'] = [$action['nodeId']];
                                        $constraint['simulationVariables'] = ['diluteContainer', 'diluteMeasurer'];
                                        break 2;
                                    }
                                }
                            }
                            $constraint['current_state'] = 1;
                        } else {
                            
                            $constraint['current_state'] = null;
                        }
                        $constraint['nodesId'] = null;
                        $constraint['simulationVariables'] = null;
                        break;
                    case 'exactlyOneMeasureAction':
                        $constraint['current_state'] = +($measureActions->count()===1);
                        $constraint['nodesId'] = $measureActions->pluck('nodeId')->all();
                        $constraint['simulationVariables'] = ["measureP1", "measureP2", "measureP3", "measureP4"];
                        break;
                    case'measureActionAfterAddLiquidAction':
                        $measureAction = $measureActions->first();
                        $index = $this->protocol->search(fn($a) => intval($a['nodeId']) === intval($measureAction['nodeId']));
                        $slicedProtocol = $this->protocol->slice($index);
                        $constraint['current_state'] = +($slicedProtocol->filter(fn($sa) => $sa['structuredAction'] === 'addLiquid')->count() === 0);
                        $constraint['nodesId'] = [$measureAction['nodeId']];
                        $constraint['simulationVariables'] = ["measureP1", "measureP2", "measureP3", "measureP4"];
                        break;
                    case'measureP1ShouldBeMagnetBar':
                        $measureAction = $measureActions->first();
                        $constraint['current_state'] = +($this->getMaterialPropertyValue($measureAction['measureP1'], 'shortName') === 'magnetBar');
                        $constraint['nodesId'] = [$measureAction['nodeId']];
                        $constraint['simulationVariables'] = ["measureP1"];
                        break;
                    case'measureP2ShouldBeMagneticStirrer':
                        $measureAction = $measureActions->first();
                        $constraint['current_state'] = +($this->getMaterialPropertyValue($measureAction['measureP2'], 'shortName') === 'magneticStirrer');
                        $constraint['nodesId'] = [$measureAction['nodeId']];
                        $constraint['simulationVariables'] = ["measureP2"];
                        break;
                    case'measureP3ShouldBepHElectrode':
                        $measureAction = $measureActions->first();
                        $constraint['current_state'] = +($this->getMaterialPropertyValue($measureAction['measureP3'], 'shortName') === 'pHElectrode');
                        $constraint['nodesId'] = [$measureAction['nodeId']];
                        $constraint['simulationVariables'] = ["measureP3"];
                        break;
                    case'measureP4ShouldBepHMeter':
                        $measureAction = $measureActions->first();
                        $constraint['current_state'] = +($this->getMaterialPropertyValue($measureAction['measureP4'], 'shortName') === 'pHMeter');
                        $constraint['nodesId'] = [$measureAction['nodeId']];
                        $constraint['simulationVariables'] = ["measureP4"];
                        break;
                    case 'beakerShouldBeFilledEnough':
                        $volumeAddedToBeaker = $addLiquidActionsToBeaker
                            ->sum(fn($sa) => floatval($sa['addLiquidTotalVolumeAndUnit'][0]));
                        $constraint['current_state'] = +(
                            floatval($volumeAddedToBeaker) > floatval($this->getMaterialPropertyValue($beaker,'volume')) / 3.0
                        );
                        $constraint['nodesId'] = $addLiquidActionsToBeaker->pluck('nodeId')->all();
                        $constraint['simulationVariables'] = ['addLiquidTotalVolumeAndUnit'];
                        break;
                    case 'onlyOneDropLiquidActionWithBurette':
                        $constraint['current_state'] = +($dropLiquidActions->count() === 1);
                        $constraint['nodesId'] = $dropLiquidActions->pluck('nodeId')->all();
                        $constraint['simulationVariables'] = ["dropLiquidMinVolumeAndUnitFraction", "dropLiquidMaxVolumeAndUnitFraction"];
                        break;
                    case 'dropLiquidActionMustBeLastAction':
                        $dropLiquidAction = $dropLiquidActions->first();
                        $index = $this->protocol->search(fn($a) => intval($a['nodeId']) === intval($dropLiquidAction['nodeId']));
                        $constraint['current_state'] = +($index === $this->protocol->count()-1);
                        $constraint['nodesId'] = [$dropLiquidAction['nodeId']];
                        $constraint['simulationVariables'] = ["dropLiquidMinVolumeAndUnitFraction", "dropLiquidMaxVolumeAndUnitFraction"];
                        break;
                    case 'dropLiquidMinVolumeShouldBeInTheRange':
                        $irrelevantDropLiquidActions = $this->protocol->filter(function($a) {
                            if ($a['structuredAction'] === 'dropLiquid') {
                                $minVolume = floatval($a['dropLiquidMinVolumeAndUnitFraction'][0]);
                                return $minVolume > 0.5 || $minVolume < 0.2;
                            } else {
                                return false;
                            }
                        });
                        $constraint['current_state'] = +($irrelevantDropLiquidActions->count() === 0);
                        $constraint['nodesId'] = $irrelevantDropLiquidActions->pluck('nodeId')->all();
                        $constraint['simulationVariables'] = ['dropLiquidMinVolumeAndUnitFraction'];
                        break;
                    case 'dropLiquidMinVolumeShouldBeLargeEnough':
                        $irrelevantDropLiquidActions = $this->protocol->filter(function($a) {
                            if ($a['structuredAction'] === 'dropLiquid') {
                                $minVolume = floatval($a['dropLiquidMinVolumeAndUnitFraction'][0]);
                                return 0.2 > $minVolume;
                            } else {
                                return false;
                            }
                        });
                        $constraint['current_state'] = +($irrelevantDropLiquidActions->count() === 0);
                        $constraint['nodesId'] = $irrelevantDropLiquidActions->pluck('nodeId')->all();
                        $constraint['simulationVariables'] = ['dropLiquidMinVolumeAndUnitFraction'];
                        break;
                    case 'dropLiquidMaxVolumeShouldBeSmallEnough':
                        $irrelevantDropLiquidActions = $this->protocol->filter(function($a) {
                            if ($a['structuredAction'] === 'dropLiquid') {
                                $maxVolume = floatval($a['dropLiquidMaxVolumeAndUnitFraction'][0]);
                                return $maxVolume > 2;
                            } else {
                                return false;
                            }
                        });
                        $constraint['current_state'] = +($irrelevantDropLiquidActions->count() === 0);
                        $constraint['nodesId'] = $irrelevantDropLiquidActions->pluck('nodeId')->all();
                        $constraint['simulationVariables'] = ['dropLiquidMaxVolumeAndUnitFraction'];
                        break;
                    case 'dropLiquidMaxVolumeShouldBeLargeEnough':
                        $irrelevantDropLiquidActions = $this->protocol->filter(function($a) {
                            if ($a['structuredAction'] === 'dropLiquid') {
                                $maxVolume = floatval($a['dropLiquidMaxVolumeAndUnitFraction'][0]);
                                return 1 > $maxVolume;
                            } else {
                                return false;
                            }
                        });
                        $constraint['current_state'] = +($irrelevantDropLiquidActions->count() === 0);
                        $constraint['nodesId'] = $irrelevantDropLiquidActions->pluck('nodeId')->all();
                        $constraint['simulationVariables'] = ['dropLiquidMaxVolumeAndUnitFraction'];
                        break;
                    case 'beakerSolutionShouldNotOverflowWhileTitrating':
                        $buretteVolume = floatval($this->getMaterialPropertyValue(
                            $this->getMaterialsByProperty('shortName','burette')->first(), 'volume'));
                        $beakerVolume = floatval($this->getMaterialPropertyValue(
                            $this->getMaterialsByProperty('shortName','beaker')->first(), 'volume'));
                        $totalLiquidVolumeInBeaker = $addLiquidActionsToBeaker->sum(fn($a) => floatval($a['addLiquidTotalVolumeAndUnit'][0]));
                        $constraint['current_state'] = +($buretteVolume + $totalLiquidVolumeInBeaker < $beakerVolume);
                        $constraint['nodesId'] = $addLiquidActionsToBeaker->pluck('nodeId')->all();
                        $constraint['simulationVariables'] = ['addLiquidTotalVolumeAndUnit'];
                        break;
                    case 'onlyOneTitrandLiquidAddedInBeaker':
                        $addUnknownSolutionToBeaker = $addLiquidActionsToBeaker->filter(fn($sa) => $this->getMaterialPropertyValue($sa['addLiquidSolution'], 'known') === "0");
                        $constraint['current_state'] = +($addUnknownSolutionToBeaker->count() === 1);
                        $constraint['nodesId'] = $addUnknownSolutionToBeaker->pluck('nodeId')->all();
                        $constraint['simulationVariables'] = ['addLiquidSolution'];
                        break;
                    case 'dilutionRequiredOrDilutionMissing':
                        $constraintSucceeds = $isDilutionNecessary || $dilutionActions->count() === 0;
                        $constraint['current_state'] = +($constraintSucceeds);
                        $constraint['nodesId'] = null;
                        $constraint['simulationVariables'] = null;
                        break;
                    case 'dilutionRequiredOrVeqShouldBeLargeEnough':
                        $constraintSucceeds = $isDilutionNecessary || $this->computeVeq() >= 8;
                        $constraint['current_state'] = +($constraintSucceeds);
                        $constraint['nodesId'] = null;
                        $constraint['simulationVariables'] = null;
                        break;
                    case 'dilutionRequiredOrVeqShouldBeSmallEnough':
                        $constraintSucceeds = $isDilutionNecessary || $this->computeVeq() <= 20;
                        $constraint['current_state'] = +($constraintSucceeds);
                        $constraint['nodesId'] = null;
                        $constraint['simulationVariables'] = null;
                        break;
                        
                    default:
                        $error_msg = __('La contrainte est absente');
                        break;
                }
            } else {
                // constraint is not evaluated because at least one relevant constraint is failing
                $constraint['current_state'] = null;
            }
            if($error_msg) {
                break;
            }
        }
        return ['constraints' => $this->constraints, 'error_message' => $error_msg];
    }
    
    public function dilutionIsNecessary()
    {
        $titrand = $this->getMaterialsByProperty('known', "0")->first(fn($m) =>
            !array_key_exists('createdByProtocol', $m)
        );
        $titrators = $this->getTitratorsFromMaterialList($this->getMaterialPropertyValue($titrand, 'nature'));
        $pipettes = $this->getMaterialsByProperty('shortName', 'pipette');
        $titrandConcentration = floatval($this->getMaterialPropertyValue($titrand, 'concentration'));
        $dilutionRequired = true;
        foreach ($titrators as $titrator) {
            $titratorConcentration = floatval($this->getMaterialPropertyValue($titrator, 'concentration'));
            foreach ($pipettes as $pipette) {
                $pipetteVolume = floatval($this->getMaterialPropertyValue($pipette, 'volume'));
                $vEq = $pipetteVolume * $titrandConcentration /$titratorConcentration;
                if ($vEq >= 8 && $vEq <= 20) {
                    $dilutionRequired = false;
                    break 2;
                }
            }
        }
        return $dilutionRequired;
    }
    
    public function computeVeq()
    {
        $addTitrandToBeakerAction = $this->protocol->first(
            fn($a) => 
                $a['structuredAction'] === 'addLiquid'
                && $this->getMaterialPropertyValue($a['addLiquidSolution'], 'known') === "0"
        );
        $fillTitratorToBuretteAction = $this->protocol->first(
            fn($a) =>
                $a['structuredAction'] === 'fill'
                && $this->getMaterialPropertyValue($a['fillSolution'], 'known') === "1"
                && $this->getMaterialPropertyValue($a['fillSolution'], 'pka1') === null
        );
        if( !$addTitrandToBeakerAction || !$fillTitratorToBuretteAction ) {
            return null;
        }
        $pipetteVolume = floatval($addTitrandToBeakerAction['addLiquidTotalVolumeAndUnit'][0]);
        $titrandConcentration = floatval($this->getMaterialPropertyValue($addTitrandToBeakerAction['addLiquidSolution'], 'concentration'));
        $titratorConcentration = floatval($this->getMaterialPropertyValue($fillTitratorToBuretteAction['fillSolution'], 'concentration'));
        if( !$pipetteVolume || !$titrandConcentration || !$titratorConcentration ) {
            return null;
        }
        return $pipetteVolume * $titrandConcentration / $titratorConcentration;
    }
    
    public function samplingVolumeIsCorrect()
    {
        // Sampling volume : titrand volume added to the beaker
        $addTitrandToBeakerAction = $this->protocol->first(
            fn($a) => 
                $a['structuredAction'] === 'addLiquid'
                && $this->getMaterialPropertyValue($a['addLiquidSolution'], 'known') === "0"
        );
        $titrand = $this->getMaterialsByProperty('known', "0")->first(fn($m) =>
            !array_key_exists('createdByProtocol', $m)
        );
        $titrandVolume = floatval($addTitrandToBeakerAction['addLiquidTotalVolumeAndUnit'][0]);
        $titrandConcentration = floatval($this->getMaterialPropertyValue($titrand, 'concentration'));

        // All possible dilution factors
        $pipettes = $this->getMaterialsByProperty('shortName', 'pipette');
        $graduatedFlasks = $this->getMaterialsByProperty('shortName', 'graduatedFlask');
        $dilutionFactors = collect(1);
        foreach ($graduatedFlasks as $graduatedFlask) {
            foreach ($pipettes as $pipette) {
                $dilutionFactors->push(floatval($this->getMaterialPropertyValue($pipette, 'volume'))
                    / floatval($this->getMaterialPropertyValue($graduatedFlask, 'volume'))
                );
            }
        }
        
        // All possible titrators
        $titrators = $this->getTitratorsFromMaterialList($this->getMaterialPropertyValue($titrand, 'nature'));
        
        $vpeCorrect = false;
        // titrand dilution
        foreach ($dilutionFactors as $dilutionFactor) {
            $dilutedTitrandConcentration = $titrandConcentration * $dilutionFactor;
            foreach ($titrators as $titrator) {
                $titratorConcentration = floatval($this->getMaterialPropertyValue($titrator, 'concentration'));
                $vEq = $titrandVolume * $dilutedTitrandConcentration / $titratorConcentration;
                if ($vEq >= 8 && $vEq <= 20) {
                    $vpeCorrect = true;
                    break 2;
                }
            }
        }
        if (!$vpeCorrect) {
            // titrator dilution
            foreach ($dilutionFactors as $dilutionFactor) {
                foreach ($titrators as $titrator) {
                    $dilutedTitratorConcentration = floatval($this->getMaterialPropertyValue($titrator, 'concentration')) * $dilutionFactor;
                    $vEq = $titrandVolume * $titrandConcentration / $dilutedTitratorConcentration;
                    if ($vEq >= 8 && $vEq <= 20) {
                        $vpeCorrect = true;
                        break 2;
                    }
                }
            }
        }
        return $vpeCorrect;
    }
    
    public function getTitratorsFromMaterialList($titrandNature)
    {
        return $this->materialList->filter(function($item) use ($titrandNature) {
            $isItemConcentrationKnown = count(collect($item['properties'])->where('name', '=', 'known')->where('value', '1')) > 0;
            $isItemAcidOrBase = count(collect($item['properties'])->where('name', '=', 'nature')->whereNotIn('value',[$titrandNature])->whereIn('value', ['acid', 'base'])) > 0;
            $isItemStrong = !array_key_exists('pka1', $item['properties']);
            $isCreatedByProtocol = array_key_exists('createdByProtocol', $item);
            return $isItemConcentrationKnown && $isItemAcidOrBase && $isItemStrong && !$isCreatedByProtocol;
        });
    }
    
    public function createLabdocResponse ($labdocProcedure)
    {
        $materialResponse = $this->materialList->filter(fn($material) => 
            collect($material['properties'])->filter(fn($p) 
                => $p['name'] === 'known' && $p['value'] == '0')->first()
            )->first();
        if ($materialResponse) {
            $properties = collect($materialResponse['properties']);
            $responseProperty = $properties->filter(fn($p) => $p['name'] === 'concentration')->first();
            $responseUnit = collect($this->quantities->filter(fn($q) => 
                    intval($q['id']) === intval($responseProperty['physicalQuantityId'])
                )->first()['unit']
                )->filter(fn($u, $k) => 
                    intval($k) === intval($responseProperty['unitId'])
                )->first()['symbol'];
            
            $hashedValue = str_split(json_encode($responseProperty['value'] * 100151));
            $letters = ['.' => 'A', '0' => 'Z', '1' => 'C', '2' => 'W', '3' => 'E', '4' => 'V', '5' => 'T', '6' => 'J', '7' => 'R', '8' => 'M', '9' => 'O'];
            $hashedValue = collect($hashedValue)->map(fn($l) => $letters[$l])->join('');
            $unit = $responseUnit[Helper::selectLanguage()];
            $text_data = <<<EOT
                <p>Concentration recherchée : <span contenteditable="true" style="display: inline-block; border: 1px solid grey; width: 100px;" data-simulation="$hashedValue" data-key="verificationOfTheRequiredConcentration"> </span>$unit</p>
            EOT;
            $assignment = <<<EOT
                <p><span>Donnez une réponse avec au moins 3 chiffres significatifs et utilisez le point comme séparateur décimal.</span> <span>La réponse correcte apparaît en vert.</span></p>
            EOT;
            $labdocText = Labdoc::init($labdocProcedure->report, [
                'id_report_part' => $labdocProcedure->id_report_part,
                'id_report' => $labdocProcedure->id_report,
                'type_labdoc' => 'text',
                'name' => __('Réponse à la question'),
                'position' => Labdoc::where('id_report_part', $labdocProcedure->id_report_part)
                        ->whereNull('id_ld_origin')
                        ->whereNull('id_report')
                        ->count() + 1,
                'editable' => 0,
                'editable_name' => 0,
                'deleteable' => 0,
                'draggable' => 0,
                'duplicatable' => 0,
                'assignment' => $assignment
            ]);
            $labdocText->labdoc_data = $text_data;
            $labdocText->save();
            $labdocText->fresh();
            return $labdocText;
        }
        return null;
    }

    public function createLabdocData ($simulation_data, $base_units, $labdocProcedure, $error)
    {
        $XML_doc = new \DOMDocument('1.0', 'UTF-8');
        
        $LD = $XML_doc->createElement('LD');
        $LD_type_attr = $XML_doc->createAttribute('type');
        $LD_type_attr->value = 'dataset';
        
        $metadata = $XML_doc->createElement('metadata');
        $metadata_name = $XML_doc->createElement('name', 'default dataset');
        $metadata_author = $XML_doc->createElement('author', 'labnbook');
        $metadata_lastModification = $XML_doc->createElement('lastModification', date('Y-m-d'));
        $metadata->appendChild($metadata_name);
        $metadata->appendChild($metadata_author);
        $metadata->appendChild($metadata_lastModification);
        
        $constants = $XML_doc->createElement('constants');
        $constant = $XML_doc->createElement('constant');
        $constant_name = $XML_doc->createElement('name', 'pi');
        $constant_value = $XML_doc->createElement('value', 3.141593);
        $constant->appendChild($constant_name);
        $constant->appendChild($constant_value);
        $constants->appendChild($constant);
        
        $dataset = $XML_doc->createElement('dataset');
        $dataset_display_attr = $XML_doc->createAttribute('display_formula');
        $dataset_display_attr->value = 'true';
        $dataset->appendChild($dataset_display_attr);
        
        $dataset_header = $XML_doc->createElement('header');
        $dataset_header_column_1 = $XML_doc->createElement('column');
        $dataset_header_column_1_type_attr = $XML_doc->createAttribute('type');
        $dataset_header_column_1_type_attr->value = 'num';
        $dataset_header_column_1_scientific_notation_attr = $XML_doc->createAttribute('scientific_notation');
        $dataset_header_column_1_scientific_notation_attr->value = 'false';
        $dataset_header_column_1_digits_attr = $XML_doc->createAttribute('digits');
        $dataset_header_column_1_digits_attr->value = '';
        $dataset_header_column_1_code = $XML_doc->createElement('code', 'Volume');
        $dataset_header_column_1_name = $XML_doc->createElement('name', 'Volume');
        $dataset_header_column_1_unit = $XML_doc->createElement('unit', $base_units['volume']);
        $dataset_header_column_1_formula = $XML_doc->createElement('formula');
        $dataset_header_column_1->appendChild($dataset_header_column_1_type_attr);
        $dataset_header_column_1->appendChild($dataset_header_column_1_scientific_notation_attr);
        $dataset_header_column_1->appendChild($dataset_header_column_1_digits_attr);
        $dataset_header_column_1->appendChild($dataset_header_column_1_code);
        $dataset_header_column_1->appendChild($dataset_header_column_1_name);
        $dataset_header_column_1->appendChild($dataset_header_column_1_unit);
        $dataset_header_column_1->appendChild($dataset_header_column_1_formula);
        
        $dataset_header_column_2 = $XML_doc->createElement('column');
        $dataset_header_column_2_type_attr = $XML_doc->createAttribute('type');
        $dataset_header_column_2_type_attr->value = 'num';
        $dataset_header_column_2_xmlns_attr = $XML_doc->createAttribute('xmlns');
        $dataset_header_column_2_xmlns_attr->value = 'http://www.w3.org/1999/xhtml';
        $dataset_header_column_2_code = $XML_doc->createElement('code', 'pH');
        $dataset_header_column_2_name = $XML_doc->createElement('name', 'pH');
        $dataset_header_column_2_unit = $XML_doc->createElement('unit');
        $dataset_header_column_2->appendChild($dataset_header_column_2_type_attr);
        $dataset_header_column_2->appendChild($dataset_header_column_2_xmlns_attr);
        $dataset_header_column_2->appendChild($dataset_header_column_2_code);
        $dataset_header_column_2->appendChild($dataset_header_column_2_name);
        $dataset_header_column_2->appendChild($dataset_header_column_2_unit);
        
        $dataset_header->appendChild($dataset_header_column_1);
        $dataset_header->appendChild($dataset_header_column_2);
        
        $dataset->appendChild($dataset_header);
        
        $minTitratorReasonableVolume = floatval($simulation_data['equivalentVolume'] - $this->unitConvertToBaseUnit(0.002, 'L', $base_units));
        $maxTitratorReasonableVolume = floatval($simulation_data['equivalentVolume']  + $this->unitConvertToBaseUnit(0.002, 'L', $base_units));
        $lastIndex = 0;
        foreach ($simulation_data['points'] as $index => $point) {
            $volumeStep = floatval($simulation_data['maxDropStep']);
            if (floatval($point[0]) > $minTitratorReasonableVolume && floatval($point[0]) < $maxTitratorReasonableVolume) {
                $volumeStep = floatval($simulation_data['minDropStep']);
            }
            if ($index === 0 || floatval($point[0]) > floatval($simulation_data['points'][$lastIndex][0]) + $volumeStep) {
                $lastIndex = $index;
                $row = $XML_doc->createElement('row');
                $volume = $XML_doc->createElement('value', number_format(floatval($point[0]), 2, '.', ""));
                $ph = $XML_doc->createElement('value', floatval($point[1]));
                $row->appendChild($volume);
                $row->appendChild($ph);
                $dataset->appendChild($row);
            }
        }
        
        
        $graph = $XML_doc->createElement('graph');
        $graph_y2_attr = $XML_doc->createAttribute('y2');
        $graph_y2_attr->value = "false";
        $graphTitle = __("Titrage de ") 
            . $simulation_data['titrandVolume'] . " " . $base_units['volume'] 
            . __(" de ") . $simulation_data['titrandName']
            . __(" additionnés de ") . $simulation_data['solventVolume'] . " " . $base_units['volume'] . __(' d’eau distillée')
            . __(" par ") . $simulation_data['titratorName'];
        if(!$error) {
            $graphTitle = $graphTitle . __(" - Volume équivalent : ") . round($simulation_data['equivalentVolume'], 2) . " " . $base_units['volume'];
            $graph_name = $XML_doc->createElement('name', $graphTitle);
        } else {
            $graph_name = $XML_doc->createElement('name', $graphTitle);
        }
        $graph_xmin = $XML_doc->createElement('xmin');
        $graph_xmax = $XML_doc->createElement('xmax');
        $graph_ymin = $XML_doc->createElement('ymin');
        $graph_ymax = $XML_doc->createElement('ymax');
        $graph_xlabel = $XML_doc->createElement('xlabel', __('Volume en ') . $base_units['volume'] . __(" de ") . $simulation_data['titratorName']);
        $graph_ylabel = $XML_doc->createElement('ylabel');
        $graph_plot = $XML_doc->createElement('plot');
        $graph_plot_color_attr = $XML_doc->createAttribute('color');
        $graph_plot_color_attr->value = '#8400CD';
        $graph_plot_curve_attr = $XML_doc->createAttribute('curve');
        $graph_plot_curve_attr->value = 'no';
        $graph_plot_axis_attr = $XML_doc->createAttribute('axis');
        $graph_plot_axis_attr->value = 'y';
        $graph_plot_x = $XML_doc->createElement('x', 'Volume');
        $graph_plot_y = $XML_doc->createElement('y', 'pH');
        $graph_plot_ux = $XML_doc->createElement('ux', 'undefined');
        $graph_plot_uy = $XML_doc->createElement('uy', 'undefined');
        $graph_plot->appendChild($graph_plot_color_attr);
        $graph_plot->appendChild($graph_plot_curve_attr);
        $graph_plot->appendChild($graph_plot_axis_attr);
        $graph_plot->appendChild($graph_plot_x);
        $graph_plot->appendChild($graph_plot_y);
        $graph_plot->appendChild($graph_plot_ux);
        $graph_plot->appendChild($graph_plot_uy);

        $graph->appendChild($graph_y2_attr);
        $graph->appendChild($graph_name);
        $graph->appendChild($graph_xmin);
        $graph->appendChild($graph_xmax);
        $graph->appendChild($graph_ymin);
        $graph->appendChild($graph_ymax);
        $graph->appendChild($graph_xlabel);
        $graph->appendChild($graph_ylabel);
        $graph->appendChild($graph_plot);

        $LD->appendChild($LD_type_attr);
        $LD->appendChild($metadata);
        $LD->appendChild($constants);
        $LD->appendChild($dataset);
        $LD->appendChild($graph);

        $XML_doc->appendChild($LD);

        $labdoc_data_XML = $XML_doc->saveXML();

        $labdocDataset = Labdoc::init($labdocProcedure->report, [
            'id_report_part' => $labdocProcedure->id_report_part,
            'id_report' => $labdocProcedure->id_report,
            'type_labdoc' => 'dataset',
            'name' => __('Mesures simulées'),
            'position' => Labdoc::where('id_report_part', $labdocProcedure->id_report_part)
                    ->whereNull('id_ld_origin')
                    ->whereNull('id_report')
                    ->count() + 1,
            'editable' => 1,
            'editable_name' => 0,
            'deleteable' => 1,
            'draggable' => 0,
            'duplicatable' => 1
            ]
        );
        $labdocDataset->labdoc_data = $labdoc_data_XML;
        $labdocDataset->save();
        $labdocDataset->fresh();
        return $labdocDataset;
    }
    
        public function unitConvertToBaseUnit($value, $sourceUnitSymbol, $base_units)
    {
        $sourceQuantity = $this->quantities->first(fn($quantity) => collect($quantity['unit'])->contains(fn($unit) => $unit['symbol']['en'] === $sourceUnitSymbol));
        $baseId = collect($base_units)
            ->filter(function ($baseUnit, $baseQuantity) use ($sourceQuantity) {
                return $baseQuantity === $sourceQuantity['name']['en'];
            })->flatMap(function ($baseUnit, $baseQuantity) use ($sourceQuantity) {
                $unitId = collect($sourceQuantity['unit'])->first(fn($unit) => $unit['symbol']['en'] === $baseUnit)['id'];
                return ['quantityId' => $sourceQuantity['id'], 'unitId' => $unitId];
            });
        $sourceUnit = collect($sourceQuantity['unit'])->first(fn($unit) => $unit['symbol']['en'] === $sourceUnitSymbol);
        $baseUnit = collect($sourceQuantity['unit'])->first(fn($unit) => intval($unit['id']) === intval($baseId['unitId']));
        $sourceFactor = $sourceUnit['factor'];
        $baseFactor = $baseUnit['factor'];
        return floatval($value) * floatval($sourceFactor[0]) / floatval($baseFactor[0]) + (floatval($sourceFactor[1]) - floatval($baseFactor[1])) / floatval($baseFactor[0]);
    }

    public function nRand($errorModel)
    {
        $x = mt_rand() / mt_getrandmax();
        $y = mt_rand() / mt_getrandmax();
        return sqrt(-2 * log($x)) * cos(2 * pi() * $y) * $errorModel['std'] + $errorModel['mean'];
    }
}
