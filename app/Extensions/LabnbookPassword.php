<?php

namespace App\Extensions;

use Closure;
use Illuminate\Auth\Passwords\PasswordBroker as BasePasswordBroker;
use Illuminate\Support\Facades\Mail;
use App\Mail\MarkdownMail;

class LabnbookPassword extends BasePasswordBroker
{
    /**
     * Constant representing the user not managed response
     *
     * @var string
     */
    const USER_NOT_MANAGED = 'passwords.user_cas';

    /**
     * Constant representing an invalid password to be rejected
     *
     * @var string
     */
    const INVALID_PASSWORD = 'passwords.invalid';


    /**
     * Send a password reset link to a user.
     *
     * @param  array  $credentials
     * @return string
     */
    public function sendResetLink(array $credentials, ?Closure $callback = null)
    {
        $users = \App\User::where('email', $credentials['email'])->internal()->get();

        if ($users->isEmpty()) {
            if (\App\User::where('email', $credentials['email'])->exists()) {
                return static::USER_NOT_MANAGED; // There is a CAS user link to this email
            }
            return static::INVALID_USER;
        }
        $tokens = [];

        foreach ($users as $user) {
            // Generate token
            $tokens[$user->login] = $this->tokens->create($user);
        }
        // Send mail
        Mail::to($users->first()->email)
            ->send(new MarkdownMail('emails.passwords.reset', "LabNBook : informations de connexion à la plateforme", $tokens));
        return static::RESET_LINK_SENT;
    }

    /**
     * Validate a password reset for the given credentials.
     *
     * @param  array  $credentials
     * @return \Illuminate\Contracts\Auth\CanResetPassword|string
     */
    protected function validateReset(array $credentials)
    {
        $users = \App\User::Where('login', $credentials['login'])->get();
        if ($users->isEmpty()) {
            return static::INVALID_USER;
        }

        $user = null;
        foreach ($users as $u) {
            if ($this->tokens->exists($u, $credentials['token'])) {
                // User with right login and token
                $user = $u;
                break;
            }
        }

        if ($user === null) {
            return static::INVALID_TOKEN;
        }

        $password = $credentials['password'];
        $confirmation = $credentials['password_confirmation'];
        if ($password != $confirmation
            || ! \App\User::validPassword($password)
            || ! $user->isUniqueLoginPass($credentials['login'], $password)
        ) {
            return static::INVALID_PASSWORD;
        }
        return $user;
    }
}
