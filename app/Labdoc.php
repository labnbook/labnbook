<?php

namespace App;

use \App\Helper;
use \App\ImagesExtractor;
use App\Processes\LabdocDiff;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\QueryException;

/**
 * App\Labdoc
 *
 * @property int $id_labdoc
 * @property int|null $id_report_part
 * @property int|null $id_report
 * @property int|null $id_ld_origin
 * @property string $type_labdoc
 * @property string|null $name
 * @property int|null $position
 * @property int $editable
 * @property int $editable_name
 * @property int $duplicatable
 * @property int $deleteable
 * @property int $draggable
 * @property int|null $locked
 * @property int|null $last_editor
 * @property int|null $last_edition
 * @property int|null $last_id_version
 * @property string $labdoc_data
 * @property int $draft
 * @property string|null $deleted
 * @property string $creation_time
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Comment[] $comments
 * @property-read int|null $comments_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Message[] $messages
 * @property-read int|null $messages_count
 * @property-read \App\Mission|null $mission
 * @property-read \App\Labdoc|null $parentLD
 * @property-read \App\Report|null $report
 * @property-read \App\ReportPart|null $reportPart
 * @property-read \App\User|null $user_last_editor
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Labdoc newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Labdoc newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Labdoc query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Labdoc whereCreationTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Labdoc whereDeleteable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Labdoc whereDeleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Labdoc whereDraft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Labdoc whereDraggable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Labdoc whereDuplicatable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Labdoc whereEditable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Labdoc whereEditableName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Labdoc whereIdLabdoc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Labdoc whereIdLdOrigin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Labdoc whereIdReport($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Labdoc whereIdReportPart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Labdoc whereLabdocData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Labdoc whereLastEdition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Labdoc whereLastEditor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Labdoc whereLocked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Labdoc whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Labdoc wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Labdoc whereTypeLabdoc($value)
 * @mixin \Eloquent
 */
class Labdoc extends Model
{
    protected $table = 'labdoc';
    protected $primaryKey = 'id_labdoc';
    
    const CREATED_AT = 'creation_time';
    const UPDATED_AT = 'last_edition';
    
    // TODO Need to be used in whole code
    public const TEXT = "text";
    public const DRAWING = "drawing";
    public const PROCEDURE = "procedure";
    public const DATASET = "dataset";
    public const CODE = "code";
    
    public $timestamps = false;

    /**
     * Constants used in synchronzize
     */
    const FREE = "free_ld";
    const LOCKED = "locked_ld";
    const MODIFIED = "modified_ld";

    /**
     * Constants for labdoc content validation
     */
    const CONTENT_ERR_NONE = 0;
    const CONTENT_ERR_RECOVERABLE = 1;
    const CONTENT_ERR_FATAL = 2;

    /**
     * Boot function to handle not standard timestamp fields
     */
    public static function boot()
    {
        parent::boot();

        static::updating(function ($model) {
            $field = $model::UPDATED_AT;
            $model->$field = time();
        });
    }

    public $fillable = [
        'id_report_part', 'id_report', 'id_team_config', 'id_ld_origin', 'type_labdoc', 'name', 'position',
        'locked', 'last_editor', 'last_id_version', 'labdoc_data', 'editable', 'editable_name',
        'duplicatable', 'deleteable', 'draggable', 'shared', 'assignment'
    ];

    /**
     * Returns the report of the labdoc
     * @return \App\Report
     */
    public function report()
    {
        return $this->belongsTo('App\Report', 'id_report');
    }

    /**
     * Get the labdocs report for a user
     */
    public function getSharedLabdocReport($user)
    {
        if ($this->id_team_config && $this->shared) {
            $tc = TeamConfig::find($this->id_team_config);
            if ($tc) {
                foreach ($tc->reports as $r) {
                    if ($r->users()->where('user.id_user', $user->id_user)->exists()) {
                        return $r;
                    }
                }
            }
        }
        return null;
    }

    /**
     * Returns the mission of the labdoc through the report part
     * @return \Illuminate\Database\Eloquent\Relations\HasOneThrough
     */
    private function _missionFromRP(): \Illuminate\Database\Eloquent\Relations\HasOneThrough
    {
        return $this->hasOneThrough(
            'App\Mission',
            'App\ReportPart',
            'id_report_part', // fk on report_part
            'id_mission', // lk on mission
            'id_report_part', //lk on labdoc
            'id_mission' //lk on mission
        );
    }

    /**
     * Returns the mission of the labdoc through the report
     * @return \Illuminate\Database\Eloquent\Relations\HasOneThrough
     */
    private function _missionFromReport(): \Illuminate\Database\Eloquent\Relations\HasOneThrough
    {
        return $this->hasOneThrough(
            'App\Mission',
            'App\Report',
            'id_report', // fk on report_part
            'id_mission', // lk on mission
            'id_report', //lk on labdoc
            'id_mission' //lk on mission
        );
    }

    /**
     * Returns the mission of the labdoc
     * @return \Illuminate\Database\Eloquent\Relations\HasOneThrough
     */
    public function mission(): \Illuminate\Database\Eloquent\Relations\HasOneThrough
    {
        if ($this->id_report_part) {
            return $this->_missionFromRP();
        }
        return $this->_missionFromReport();
    }

    /**
     * Return the user who last edited the labdoc
     * @return \App\User
     */
    public function user_last_editor()
    {
        return $this->belongsTo('App\User', 'last_editor');
    }

    /**
     * Returns the report_part of the labdoc
     * @return \App\ReportPart
     */
    public function reportPart()
    {
        return $this->belongsTo('App\ReportPart', 'id_report_part');
    }

    /**
     * Return the labdoc from with it was copied
     * @return \App\Labdoc
     */
    public function parentLD()
    {
        return $this->hasOne('App\Labdoc', 'id_labdoc', 'id_ld_origin');
    }

    /**
     * Returns the comments of the labdoc
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function comments()
    {
        return $this->hasMany('App\Comment', 'id_labdoc');
    }

    /**
     * Returns the unread comments of the labdoc for a user
     * @param int id_labdoc
     * @param int id_user
     * @return \Illuminate\Support\Collection|\App\Comment[]
     */
    public static function unreadComments($id_labdoc, $id_user)
    {
        return DB::table('comment')
            ->join("labdoc_status", "comment.id_labdoc", "=", "labdoc_status.id_labdoc")
            ->where("comment.id_labdoc", "=", $id_labdoc)
            ->where("labdoc_status.id_user", "=", $id_user)
            ->where("comment.id_user", "<>", $id_user)
            ->where(function ($query) {
                $query->whereRaw("comment_time > com_viewed")
                    ->orWhereNull("com_viewed");
            });
    }

    /**
     * Returns the messages containing the labdoc
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function messages()
    {
        return $this->hasMany('App\Message', 'id_labdoc');
    }

    /**
     * Accessor to auto_fit option
     * @return boolean
     */
    public function getAutoFitAttribute(): bool
    {
        try {
            $autofit = $this->mission->auto_fit;
        }  catch (\ErrorException $e) {
            $autofit = $this->report->mission->auto_fit;
        }
        return $autofit;
    }

    /**
     * Retourne position max des labdoc dans une partie de rapport, pour une mission ou un report
     * @param int(10) $id_rp : id de la partie de rapport
     * @param int(10) $id : id du rapport ou du team config
     * @param string  $type : 'report'|'team_config'
     * @return int : position la plus haute du labdoc
     */
    public static function maxPos($id_rp, $id, $type = 'report') {
        if (!in_array($type, ['report', 'team_config'])) {
            throw new \Exception('maxPos called on unknown object');
        }
        $args = [':id_rp' => (int)$id_rp];
        $id_name = 'id_' . $type;
        $query = Labdoc::where('id_report_part', $id_rp);
        if ($id) {
            $query = $query->where($id_name, $id);
        } else {
            $query = $query->whereNull('id_report');
        }
        $max = $query->max('position');
        return $max != null ? (int)$max : 0;
    }

    /**
     * Creates a Labdoc from fillable arguments
     * @param \App\Report $report
     * @param array $args
     * @return \App\Labdoc
     */
    public static function init($report, $args):Labdoc
    {
        $ld = self::create($args);
        $ld->initLabdocStatus($report->users, -1, 1);
        $ld->initLabdocStatus($report->teachers(true)->get(), -1, 1);
        return $ld;
    }

    /**
     * Initialize the labdoc status table for a newly created labdoc
     * @param \Illuminate\Support\Collection|\App\user[]|\App\Teacher[] $users
     * @param int $mod_icon (-1 for auto, 0 or 1 for manual set)
     * @param int $extend
     */
    public function initLabdocStatus($users, $mod_icon, $extend)
    {
        // mise à jour de la table labdoc status
        $inserts = [];
        foreach ($users as $user) {
            $id_user = $user->id_user ? $user->id_user : $user->id_teacher;
            $state = $id_user == Auth::id() ? 0 : 1;
            $inserts[] = [
                'id_labdoc' => $this->id_labdoc,
                'id_user' => $id_user,
                'modified' => $state,
                'moved' => $state,
                'mod_icon' => $mod_icon >=0 ? $mod_icon : $state,
                'extend' => $extend,
            ];
        }
        DB::table('labdoc_status')->insertOrIgnore($inserts);
    }

    /**
     * Duplicates a labdoc from a template (mission labdoc)
     * @param \App\Labdoc $ld_origin
     * @param int $id_report the report in which we ae copying
     * @param int $pos new position
     * @param boolean $forceReport force copy into report even if LD is shared
     * @return \App\Labdoc
     */
    public function copyIntoReport($id, $pos, $forceReport=false)
    {
        if ($this->shared && !$forceReport) {
            $id_name = 'id_team_config';
        } else {
            $id_name = 'id_report';
        }
        try {
            $ld = self::create([
            'id_report_part' => $this->id_report_part,
            $id_name => $id,
            'id_ld_origin' => $this->id_labdoc,
            'type_labdoc' => $this->type_labdoc,
            'name' => $this->name,
            'position' => $pos,
            'editable' => (int)$this->editable,
            'editable_name' => $this->shared ? 0 : (int)$this->editable_name,
            'duplicatable' => (int)$this->duplicatable,
            'deleteable' => $this->shared ? 0 : (int)$this->deleteable,
            'draggable' => $this->shared ? 0 : (int)$this->draggable,
            'labdoc_data' => $this->labdoc_data,
            'shared' => $this->shared && !$forceReport,
            'assignment' => $this->assignment
            ]);
        } catch (QueryException $e) {
            $errorCode = $e->errorInfo[1];
            if ($errorCode == 1062) {
                // The labdoc has already been created by another student initializing
                // the same report or another report from the same team_config if this
                // is a shared labdoc
                $ld = self::where('id_ld_origin', $this->id_labdoc)
                    ->where($id_name, $id)
                    ->first();
            }
        }
        return $ld;
    }

    /**
     * Add the created default LD to the students' reports ( only if the LD doesn't exist )
     */
    public function spreadToReports()
    {
        if (!$this->shared) {
            $this->_doSpreadToReports('reports', false);
        } else {
            $this->_doSpreadToReports('teamconfigs', false);
            $this->_doSpreadToReports('reports', true);
        }
    }

    /**
     * Internal method that does the inner work of spreading labdocs.
     *
     * @param string  $spreadType             'reports'|'teamconfigs'
     * @param boolean $onlyForSolution restrict to status SOLUTION and TEST only with 'reports'
     */
    private function _doSpreadToReports($spreadType, $onlyForSolution)
    {
        $posType = $spreadType === 'teamconfigs' ? 'team_config' : 'report';

        $query = $this->mission->$spreadType();
        if ($onlyForSolution) {
            $query = $query->where('report.status', Report::SOLUTION);
        }

        foreach ($query->get() as $dest) {
            $id = (int) $dest->getKey();
            $pos_child = self::maxPos($this->id_report_part, $id, $posType) + 1;
            $new_ld = false;
            if ($dest->labdocs()->where('id_ld_origin', $this->id_labdoc)->exists() === false) {
                if (($spreadType == 'reports' && $dest->initialized == 1) || $spreadType != 'reports') {
                    $new_ld = $this->copyIntoReport($id, $pos_child, $onlyForSolution);
                }
                if ($new_ld) {
                    if ($spreadType === 'reports') {
                        $learners = $dest->users;
                        $teachers = $dest->teachers(true)->get();
                    } else {
                        $learners = $dest->users();
                        $teachers = $dest->teachers()->get();
                    }
                    $new_ld->initLabdocStatus($learners, 1, 1);
                    $new_ld->initLabdocStatus($teachers, 0, 0);
                }
            }
        }
    }

    /**
     * Returns the next name for a copy of this ld
     * @param int $id_report
     * @param string $incr_name
     * @return string
     */
    public function getNextCopyName($id_report, $incr_name):string
    {
        $name = $this->name;
        if ($incr_name) {
            $name = $name . " - copie(";
            $last_copy = 0;
            // $res2 = Legacy\DB::query("SELECT name FROM labdoc WHERE name LIKE ? AND id_report = ?", ["$name%", $id_report]);
            $query = DB::table('labdoc')
                ->where('id_report', $id_report)
                ->where('name', 'LIKE', "$name%");
            foreach ($query->get() as $copy_name) {
                $num = substr(str_replace($name, '', $copy_name->name), 0, -1);
                if ((int) $num > (int) $last_copy) {
                    $last_copy = $num;
                }
            }
            $last_copy++;
            $name .= $last_copy . ")";
        }
        return $name;
    }

    /**
     * Check if the ld is locked for a given user
     * @param int $id_user
     * @return boolean
     */
    public function isLocked($id_user)
    {
        $lock_time = config('labnbook.locker_validity');
        return ($this->locked && time() - $this->locked < $lock_time && $this->last_editor != $id_user);
    }

    /**
     * Return the usability status
     * @param int id_user
     * @return string 'deleted'|'locked'|'free'
     */
    public function getUsabilityStatus($id_user)
    {
        if ($this->deleted) {
            return 'deleted';
        }
        if ($this->isLocked($id_user)) {
            return 'locked';
        }
        return 'free';
    }

    /**
     * Lock the labdoc
     * @param int $id_user
     */
    public function setLock($id_user)
    {
        $this->locked = time();
        $this->last_editor = $id_user;
        $this->save();
    }

    /**
     * Lock the labdoc
     * @param int $id_user
     */
    public function unSetLock($id_user)
    {
        $this->locked = null;
        $this->last_editor = $id_user;
        $this->save();
        $this->updateStatus(['modified' => 1, 'mod_icon' =>1], $id_user, false);
    }

    /**
     * Returns true if the lock is obsolete and should be removed.
     *
     * @param int $id_user
     * @return boolean
     */
    public function unsetObsoleteLock($id_user)
    {
        if ($this->locked === null) { // not locked
            return false;
        }
        $lockAge = time() - $this->locked;
        // old lock by another user
        if ($lockAge > config('labnbook.locker_validity')
            && (int) $this->last_editor !== $id_user) {
            $this->locked = null;
            $this->save();
            return true;
        }
        return false;
    }

    /**
     * Deletes the ld
     * @param int $id_user
     * @return boolean was the labdoc deleted
     */
    public function remove($id_user)
    {
        $deleted = false;
        $position = $this->position;
        $r = $this->id_report;
        $rp = $this->id_report_part;
        $changePositions = false;
        if ($r) {
            // Deleting from report
            $this->deleted = $this->freshTimestamp();
            $this->position = null;
            if ($this->save()) {
                // Maj du statut du labdoc pour qu'il soit supprimé lors du synchronize
                $this->updateStatus(['deleted' => 1], $id_user, false);
                $changePositions = $rp && $r && $position !== null;
                $deleted = true;
            }
        } elseif ($rp) {
            // Deleting from mission
            if ($this->delete()) {
                $changePositions = $position != null;
                $deleted = true;
            }
        }
        if ($changePositions) {
            //Maj de la position du Labdoc et de la position des labdocs qui suivent dans le report part
            DB::table('labdoc')
                ->where('id_report', $r)
                ->where('id_report_part', $rp)
                ->where('position', '>', $position)
                ->decrement('position');
        }
        return $deleted;
    }

    /** Restores the ld
     * @param int $id_user
     * @param int $id_report_part
     */
    public function restore($id_user, $id_report_part)
    {
        $position = DB::table('labdoc')
            ->where('id_report_part', $id_report_part)
            ->orderBy('position', 'DESC')
            ->pluck('position')
            ->first();
        $this->position = $position + 1;
        $this->deleted = null;
        $this->last_editor = $id_user;
        $this->save();

        $this->updateStatus(['deleted' => 0, 'modified' => 1, 'mod_icon' => 1], $id_user, false);
        $this->updateStatus(['extend' => 1], $id_user, true);
    }

    /**
     * Duplicates a labdoc
     * @param int $id_ld the labodc id's to duplicate
     * @param int $id_report the report id's or 0
     * @return \App\Labdoc
     */
    public static function duplicate($id_ld, $id_report)
    {
        $labdoc = self::find($id_ld);
        $duplicatable = $id_report ? 1 : $labdoc->duplicatable;
        if (!$id_report) { // on attache un LD à un message
            $id_report = $labdoc->id_report;
            $id_rp = null;
        } else {
            $id_rp = 0 ;  // sauvegarde d'un LD dans une mission
        }
        $new_ld = self::create([
            'id_report_part' => $id_rp,
            'id_report' => $id_report,
            'type_labdoc' => $labdoc->type_labdoc,
            'name' => $labdoc->name,
            'labdoc_data' => $labdoc->labdoc_data,
            'duplicatable' => $duplicatable,
        ]);
        return $new_ld;
    }

    /**
     * Fills a labdoc with data and updates it without setting timestamps
     * @return int 1 if an update actually happenned
     */
    public function updateNoTimestamps($data)
    {
        // Direct DB update to avoid setting last_etition timestamp
        return DB::table('labdoc')->where('id_labdoc', $this->id_labdoc)->update($data);
    }

    /**
     * Update labdoc status table
     * @param array $updates
     * @param int $id_user
     * @param boolean user_with_id should we update user with this id or with different id (true)
     */
    public function updateStatus($updates, $id_user, $user_with_id = true)
    {
        $opt = $user_with_id ? '=' : '<>';
        DB::table('labdoc_status')
            ->where('id_labdoc', $this->id_labdoc)
            ->where('id_user', $opt, $id_user)
            ->update($updates);
    }

    /**
     * Returns the decoded zwibbler data
     * @param string $data
     */
    public static function decodeZwibblerData($data)
    {
        return json_decode(str_replace("zwibbler3.", "", $data));
    }
    
    /**
     * Returns the encoded zwibbler data
     * @param string $data
     */
    public static function encodeZwibblerData($data)
    {
        return 'zwibbler3.' . json_encode($data);
    }

    private function validate_and_clean($content, $ldFormat, $log, &$err_level, $id_mission, $id_labdoc)
    {
        $validators = [
            'procedure' => 'xml||json',
            'dataset' => 'xml',
            'drawing' => 'pseudo-json',
            'text' => 'html',
            'code' => 'json'
        ];

        $err_level = $this->validateContent($content, $validators[$ldFormat], $log);
        if ($err_level != self::CONTENT_ERR_NONE) {
            if ($log) {
                Helper::logIntoFile("id_labdoc = $id_labdoc - Error level $err_level\n$content\n\n", "invalid_content.log");
            }
            if ($err_level === self::CONTENT_ERR_FATAL) {
                return -1;
            }
        }
        try {
            // TODO for LD text and image. On the first saving img is stored with the path "/storage/" ... On the second the path becom ../storage.
            // So a useless modify_ld is logged. Need to be changed on files refactoring.
            $images = new ImagesExtractor($id_mission, $id_labdoc, $content);
            if ($images->getCount() > 0) {
                $content = $images->getContent();
            }
        } catch (\Exception $ex) {
            Log::Error(sprintf("labdoc %d, mission %d : ERREUR de données ? %s\n", $id_labdoc, $id_mission, $ex->getMessage()));
        }
        if ($ldFormat === 'text') {
            // HTML input: remove invalid characters, then filter the HTML for safety.
            $cleanUtf8 = iconv('utf-8//TRANSLIT//IGNORE', 'utf-8', $content);
            $content = \App\HtmlFilter::clean($cleanUtf8);
        }
        return $content;
    }

    /**
     * Validates then save in MySQL the labdoc data.
     *
     * @param int $id_user
     * @param $id_mission
     * @param string $content
     * @param string| null $name (opt) If given, the LD name will be set
     * @param int $mod_icon
     * @return array
     */
    public function updateinDB($id_user, $id_mission, $content, $name = null, $mod_icon = 1)
    {
        $id_labdoc = $this->id_labdoc;
        if ($content === "NULL") {
            // déjà arrivé lors de MaJ
            return [
                'internal' => "already saved through a regular update",
                'error' => self::CONTENT_ERR_NONE,
                'name_has_changed' => false,
                'content_has_changed' => false
            ];
        }

        $ldFormat = $this->type_labdoc;
        $log = $mod_icon != 1; // TODO maybe use a separate parameter
        $content = $this->validate_and_clean($content, $ldFormat, $log, $err_level, $id_mission, $id_labdoc);
        if ($content === -1) {
            return [
                'internal' => "Fatal error labdoc will not be saved",
                'error' => $err_level,
                'name_has_changed' => false,
                'content_has_changed' => false,
                'assignment_has_changed' => false
                ];
        }
        $assignment = $this->validate_and_clean($this->assignment, 'text', $log, $err_level, $id_mission, $id_labdoc);
        if ($assignment === -1) {
            return [
                'internal' => "Fatal error labdoc will not be saved",
                'error' => $err_level,
                'name_has_changed' => false,
                'content_has_changed' => false,
                'assignment_has_changed' => false
                ];
        }

        // Check if LD content changed.
        $contentHasChanged= md5($this->labdoc_data) != md5($content);
        $assignmentHasChanged= md5($this->assignment) != md5($assignment);
        $nameHasChanged = $name !=  $this->name;
        
        $result = [
            'error' => $err_level,
            'name_has_changed' => $nameHasChanged,
            'content_has_changed' => $contentHasChanged,
            'assignment_has_changed' => $assignmentHasChanged
        ];
        // Update in DB only if name or content have change
        if ($nameHasChanged|| $contentHasChanged || $assignmentHasChanged) { // do save only if something has changed
            $this->labdoc_data = $content;
            $this->assignment = empty($assignment) ? null : $assignment;
            $this->last_editor = $id_user;
            $this->last_edition = time();
            if ($name) {
                $this->name = $name;
            }
            $this->save();
            $this->updateStatus(['modified' => 1, 'mod_icon' =>$mod_icon], $id_user, false);
            $result['internal'] = "saved";
           
        } else {
            $result['internal'] = "not saved";
        }
        return $result;
    }

    /**
     *
     * @param string $content
     * @param string $format
     * @param boolean $log should we log errors
     * @return int CONTENT_ERR_NONE|CONTENT_ERR_RECOVERABLE|CONTENT_ERR_FATAL
     */
    public function validateContent($content, $format, $log)
    {
        if (!$content) {
            return self::CONTENT_ERR_NONE;
        }
        if ($format === 'xml||json') {
            $format = (str_starts_with(trim($content), '<?xml')) ? 'xml' : 'json';
        }
        try {
            if ($format === 'pseudo-json') {
                $json = substr($content, 1 + strpos($content, '.'));
                return json_decode($json) !== null ? self::CONTENT_ERR_NONE : self::CONTENT_ERR_FATAL;
            } elseif ($format === 'json') {
                return json_decode($content) !== null ? self::CONTENT_ERR_NONE : self::CONTENT_ERR_FATAL;
            } elseif ($format === 'xml') {
                return $this->hasXmlErrors($content, false, $log);
            } elseif ($format === 'html') {
                return $this->hasXmlErrors($content, true, $log);
            }
            return self::CONTENT_ERR_NONE;
        } catch (Exception $ex) {
            error_log("validateContent() Exception: " . $ex->getMessage());
            return self::CONTENT_ERR_FATAL;
        }
    }

    /**
     * Check if the HTML/XML text has major errors.
     *
     * @staticvar boolean $init
     * @param string $content
     * @param boolean $html If false, XML
     * @param boolean $log should we log errors
     * @return int LIBXML_ERR_NONE|LIBXML_ERR_WARNING|LIBXML_ERR_ERROR|LIBXML_ERR_FATAL higher error level
     * @return int CONTENT_ERR_NONE|CONTENT_ERR_RECOVERABLE|CONTENT_ERR_FATAL
     */
    protected function hasXmlErrors($content, $html, $log)
    {
        static $init = false;
        if (!$init) {
            libxml_use_internal_errors(true);
            $init = true;
        }
        libxml_clear_errors();
        if ($content === '' || $content === null) {
            return self::CONTENT_ERR_NONE;
        }
        $dom = new \DOMDocument();
        $load = $html ? $dom->loadHTML($content) : $dom->loadXML($content);
        if (!$load || libxml_get_last_error()) {
            // Get the higher error level
            $errors = libxml_get_errors();
            $max = array_reduce($errors, function ($max, $e) {
                return max($max, $e->level);
            });
            if ($max < LIBXML_ERR_ERROR) {
                return self::CONTENT_ERR_NONE; // Ignore warnings
            }
            if ($log) {
                Helper::logIntoFile("XML ERRORS \n" . JSON_ENCODE($errors, JSON_PRETTY_PRINT) . "\n", "invalid_content.log");
            }
            if ($max === LIBXML_ERR_ERROR) {
                return self::CONTENT_ERR_RECOVERABLE;
            }
            return self::CONTENT_ERR_FATAL;
        }
        return self::CONTENT_ERR_NONE;
    }

    /**
     * Modifie le statut draft du LD si possible
     * @param int $id_user
     * @param int $draft
     */
    public function updateDraftStatus($id_user, $draft)
    {
        $this->draft = $draft;
        $this->save();
        $new_status = ['drafted' => 1];
        if (!$draft) {
            $new_status['mod_icon'] = 1;
        }
        $this->updateStatus($new_status, $id_user, false);
    }

    /**
     * Modifie la position des labdocs d'un report_part après un déplacement
     *
     * @param int $id_user
     * @param int $id_rp
     * @param int $position
     * @param array $ld_in_local
     * @return boolan
     */
    public function move($id_user, $id_rp, $position, $ld_in_local)
    {
        $ans = [];
        if ($id_rp <= 0 || $position <= 0) {
            Helper::logIntoFile(
                "moveLD() called with bad parameters: "
                . json_encode(["id_ld" => $this->id_labdoc, "id_rp" => $id_rp, "position" => $position])
            );
            return false;
        }

        $ld_status = $this->getUsabilityStatus($id_user);
        if ($ld_status == "free") {
            $id_r = $this->id_report;
            $old_position = $this->position;
            $old_rp = $this->id_report_part;
            // Check if the structure of the destination RP is coherent with the user state
            $do_move = true;
            if ($position > 1) {
                $i = 0;
                $lds = \App\Labdoc::where('id_report', $id_r)
                    ->where('id_report_part', $id_rp)
                    ->whereNull('deleted')
                    ->orderBy('position')
                    ->select('id_labdoc')
                    ->get();
                // TODO : rewrite
                foreach ($lds as $lds_in_db) {
                    $ld_in_db = $lds_in_db->id_labdoc;
                    // Helper::logIntoFile($i." pos " . $position . " sql ". $ld_in_db->id_labdoc . " local " . $ld_in_local[$i]);
                    if ($ld_in_db != $this->id_labdoc && $ld_in_db != $ld_in_local[$i]) {
                        $ans['status'] = 'fail';
                        $do_move = false;
                        break;
                    }
                    if ($ld_in_db != $this->id_labdoc) {
                        $i++;
                    }
                    if ($i == $position-1) {
                        break;
                    }
                }
            }

            if ($do_move) {
                // Les mises à jour sont effecturées directement en BD pour éviter de toucher les timestamps
                // Maj des positions des labdocs dans l'ancien report part
                DB::table('labdoc')
                    ->where('id_report', $id_r)
                    ->where('id_report_part', $old_rp)
                    ->where('position', '>', $old_position)
                    ->decrement('position');
                // Maj des positions des autres labdoc présents dans le report part de destination
                DB::table('labdoc')
                    ->where('id_report', $id_r)
                    ->where('id_report_part', $id_rp)
                    ->where('position', '>=', $position)
                    ->increment('position');
                // Maj de la nouvelle position du labdoc déplacé
                DB::table('labdoc')
                    ->where('id_labdoc', $this->id_labdoc)
                    ->update(['position' => $position, 'id_report_part' => $id_rp]);
                // modifie le statut du LD pour les autres utilisateurs : moved
                $this->updateStatus(['moved' =>1], $id_user, false);
                $ans['status'] = 'ok';
                $ans['ld_position'] = intval($position);
                $new_rp = \App\ReportPart::find($id_rp);
                $ans['rp_position'] = $new_rp?$new_rp->position:0;
            }
        } else {
            $ans['status'] = 'locked';
        }
        return $ans;
    }

    /**
     * Remove a status from a LD
     * @param int $id_user
     * @param string $status
     * @return boolean
     */
    public function removedStatus($id_user, $status)
    {
        if ($status != 'deleted' && $status != 'moved') {
            return false;
        }
        return DB::table('labdoc_status')
            ->where('id_labdoc', $this->id_labdoc)
            ->where('id_user', $id_user)
            ->update([$status => 0]);
    }

    /**
     * Returns the list of Labdoc to import in a specified report part
     *
     * @param int $id_user
     * @param int $id_r id report
     * @param int $id_rp id report part
     * @param int $id_m id mission
     * @param int $ld_txt activate labdoc constraint text
     * @param int $ld_drw activate labdoc constraint drawing
     * @param int $ld_pro activate labdoc constraint procedure
     * @param int $ld_ds activate labdoc constraint dataset
     * @param int $ld_cde activate labdoc constraint code
     * @param string role 'learner' or 'teacher'
     * @return \Illuminate\Support\Collection|\App\Labdoc[]
     *
     */
    public static function fetchLDToImport($id_user, $id_r, $id_rp, $id_m, $ld_txt, $ld_drw, $ld_pro, $ld_ds, $ld_cde, $role)
    {
        if ($role == 'teacher') {
            return DB::table('mission')
                ->join('link_mission_teacher','mission.id_mission','=','link_mission_teacher.id_mission')
                ->join('report_part','report_part.id_mission', '=', 'link_mission_teacher.id_mission')
                ->join('labdoc','report_part.id_report_part','=','labdoc.id_report_part')
                ->where('id_teacher', $id_user)
                ->where('labdoc.id_report_part', '<>', $id_rp)
                ->whereNull('id_report')
                ->whereNull('labdoc.deleted')
                ->whereNotNull('labdoc.id_report_part')
                ->groupBy('labdoc.id_labdoc')
                ->selectRaw('mission.code, mission.id_mission, labdoc.id_labdoc, labdoc.name, type_labdoc')
                ->orderByRaw('mission.code, report_part.position, labdoc.position ASC')
                ->get();
        } else if ($role == 'learner') {
            $authorized_types = array();
            if($ld_txt) {array_push($authorized_types, 'text');}
            if($ld_drw) {array_push($authorized_types, 'drawing');}
            if($ld_pro) {array_push($authorized_types, 'procedure');}
            if($ld_ds) {array_push($authorized_types, 'dataset');}
            if($ld_cde) {array_push($authorized_types, 'code');}
            $lds_from_my_reports = DB::table('link_report_learner')
                ->join('report','link_report_learner.id_report','=','report.id_report')
                ->join('labdoc','report.id_report','=','labdoc.id_report')
                ->join('mission','report.id_mission','=','mission.id_mission')
                ->join('report_part','report_part.id_mission','=','mission.id_mission')
                ->where('id_user', $id_user)
                ->where('report.id_report', '<>', $id_r)
                ->where('report.status', '<>', 'test')
                ->when($id_m>0,function ($q) use ($id_m) { return $q->where('mission.id_mission', $id_m); })
                ->whereNull('report.delete_time')
                ->whereNull('labdoc.deleted')
                ->whereNotNull('labdoc.id_report_part')
                ->whereIn('type_labdoc', $authorized_types)
                ->groupBy('labdoc.id_labdoc')
                ->selectRaw('mission.code, mission.id_mission, labdoc.id_labdoc, labdoc.name, type_labdoc')
                ->orderByRaw('mission.code, report_part.position, labdoc.position ASC');
            return DB::table('link_report_learner')
                ->join('report','link_report_learner.id_report','=','report.id_report')
                ->join('labdoc','report.id_team_config','=','labdoc.id_team_config')
                ->join('mission','report.id_mission','=','mission.id_mission')
                ->join('report_part','report_part.id_mission','=','mission.id_mission')
                ->where('id_user', $id_user)
                ->where('report.id_report', '<>', $id_r)
                ->where('report.status', '<>', 'test')
                ->when($id_m>0,function ($q) use ($id_m) { return $q->where('mission.id_mission', $id_m); })
                ->whereNull('report.delete_time')
                ->whereNull('labdoc.deleted')
                ->whereNotNull('labdoc.id_team_config')
                ->whereNotNull('labdoc.id_report_part')
                ->whereIn('type_labdoc', $authorized_types)
                ->selectRaw('mission.code, mission.id_mission, labdoc.id_labdoc, labdoc.name, type_labdoc')
                ->orderByRaw('mission.code, report_part.position, labdoc.position ASC')
                ->groupBy('labdoc.id_labdoc')
                ->union($lds_from_my_reports)
                ->get();
        } else if($role === 'corrector') {
            // All labdocs from the user from all mission
            $lds_current_mission = DB::table('mission')
                ->join('link_mission_teacher','mission.id_mission','=','link_mission_teacher.id_mission')
                ->join('report_part','report_part.id_mission', '=', 'link_mission_teacher.id_mission')
                ->join('labdoc','report_part.id_report_part','=','labdoc.id_report_part')
                ->where('id_teacher', $id_user)
                ->whereNull('id_report')
                ->whereNull('labdoc.deleted')
                ->whereNotNull('labdoc.id_report_part')
                ->whereNotNull('labdoc.name')
                ->groupBy('labdoc.id_labdoc')
                ->selectRaw('mission.code as code, mission.id_mission, "mission" as origin_type, labdoc.id_labdoc, labdoc.name, type_labdoc, report_part.position as rp_pos, labdoc.position as ld_pos');
            // All labdocs from all corrected reports of the user except the current report
            $lds_all_solution_report = DB::table('report')
                ->join('link_report_learner','report.id_report','=','link_report_learner.id_report')
                ->join('labdoc','report.id_report','=','labdoc.id_report')
                ->join('report_part','report_part.id_report_part', '=', 'labdoc.id_report_part')
                ->join('mission','report.id_mission','=','mission.id_mission')
                ->where('id_user', $id_user)
                ->where('report.id_report', '<>', $id_r)
                ->where('report.status','=',Report::SOLUTION)
                ->whereNull('labdoc.deleted')
                ->whereNotNull('labdoc.id_report_part')
                ->whereNotNull('labdoc.name')
                ->union($lds_current_mission)
                ->groupBy('labdoc.id_labdoc')
                ->selectRaw('mission.code as code, mission.id_mission, "solution" as origin_type, labdoc.id_labdoc, labdoc.name, type_labdoc, report_part.position as rp_pos, labdoc.position as ld_pos');
            // All labdocs from all reports where the teacher is learner
            return DB::table('report')
                ->join('link_report_learner','report.id_report','=','link_report_learner.id_report')
                ->join('labdoc','report.id_report','=','labdoc.id_report')
                ->join('report_part','report_part.id_report_part', '=', 'labdoc.id_report_part')
                ->join('mission','report.id_mission','=','mission.id_mission')
                ->where('id_user', $id_user)
                ->whereNotIn('report.status', [Report::TEST, Report::SOLUTION])
                ->whereNull('labdoc.deleted')
                ->whereNotNull('labdoc.id_report_part')
                ->whereNotNull('labdoc.name')
                ->union($lds_all_solution_report)
                ->groupBy('labdoc.id_labdoc')
                ->orderByRaw('origin_type, code, rp_pos, ld_pos ASC')
                ->selectRaw('mission.code as code, mission.id_mission, "student" as origin_type, labdoc.id_labdoc, labdoc.name, type_labdoc, report_part.position as rp_pos, labdoc.position as ld_pos')
                ->get();
        }
    }

    /**
     * Retrieve the last id of a version reviewed by a user in DB
     * @param \App\User $user
     * @return int id_trace of a validate_ld or null
     */
    public function getLastIdVersionReviewed($user)
    {
        return DB::table('labdoc_status')
            ->where('id_labdoc', $this->id_labdoc)
            ->where('id_user', $user->id_user)
            ->value('last_id_version_reviewed');
    }

    /**
     * Retrieve the mod_icon status in DB
     * @param \App\User $user
     * @return int 0 or 1
     */
    public function getModIconStatus($user)
    {
        return DB::table('labdoc_status')
            ->where('id_labdoc', $this->id_labdoc)
            ->where('id_user', $user->id_user)
            ->value('mod_icon');
    }

    /**
     * Store a version (id_trace) of a labdoc in labdoc_status table
     * @param int $id_user
     * @return void
     */
    public function storeLastIdVersionReviewed($last_id_version_reviewed, $id_user)
    {
        if (
            (is_numeric($last_id_version_reviewed))
            && (intval($last_id_version_reviewed) > 0)
        ){
            $this->updateStatus(['last_id_version_reviewed' => intval($last_id_version_reviewed)], $id_user);
        }
    }

    /**
     * Returns the display data that should be put in global_tab_ld
     *
     * @return array
     */
    public function getDisplayData()
    {
        $data = [
            "ld_history" => [$this->labdoc_data],
            "ld_assignment_history" => [$this->assignment],
            "ld_type" => $this->type_labdoc,
            "current_i" => 0,
            "send_in_db" => false,
            "recovering" => false,
        ];
        if ($this->ld_type === "dataset") {
            $data["auto_fit"] = $this->auto_fit;
        }
        return $data;
    }

    public function getTypeNameTranslated() {
        switch ($this->type_labdoc) {
            case self::TEXT:
                return __("texte");
            case self::PROCEDURE:
                return __("protocole");
            case self::CODE:
                return __("code");
            case self::DRAWING:
                return __("dessin");
            case self::DATASET:
                return __("jeu de données");
        }
    }
}
