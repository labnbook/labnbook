<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ReportPart
 *
 * @property int $id_report_part
 * @property int $id_mission
 * @property int $position
 * @property string|null $title
 * @property int $text
 * @property int $drawing
 * @property int $dataset
 * @property int $procedure
 * @property int $code
 * @property string|null $assignment
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Labdoc[] $labdocs
 * @property-read int|null $labdocs_count
 * @property-read \App\Mission $mission
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Report[] $reports
 * @property-read int|null $reports_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReportPart newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReportPart newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReportPart query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReportPart whereAssignment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReportPart whereDataset($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReportPart whereDrawing($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReportPart whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReportPart whereIdMission($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReportPart whereIdReportPart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReportPart wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReportPart whereProcedure($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReportPart whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReportPart whereTitle($value)
 * @mixin \Eloquent
 */
class ReportPart extends Model
{
    public $timestamps = false;
    protected $table = 'report_part';
    protected $primaryKey = 'id_report_part';

    public $fillable = ['title', 'text', 'drawing', 'code', 'dataset', 'procedure', 'assignment', 'id_mission', 'position'];

    /**
     * Returns the labdocs of the reportPart
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function labdocs()
    {
        return $this->hasMany('App\Labdoc', 'id_report_part');
    }
    /**

     * Returns the labdocs of the reportPart
     * @return \app\Report
     */
    public function reports()
    {
        return $this->hasManyThrough('App\Report', 'App\Mission', 'id_mission', 'id_mission', 'id_mission');
    }

    /**
     * Returns the labdocs of the reportPart
     * @return \App\Mission
     */
    public function mission()
    {
        return $this->belongsTo('App\Mission', 'id_mission');
    }

    /**
     * Does the rp has at least one labdoc with one assignment ?
     *
     * @return bool
     */
    public function hasLdAssignment()
    {
        return $this->labdocs()
                    ->whereNotNull('assignment')
                    ->where('assignment', '<>', '')
                    ->exists();
    }
}
