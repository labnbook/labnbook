<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Comment
 *
 * @property int $id_comment
 * @property int $id_user
 * @property int $id_labdoc
 * @property string|null $content
 * @property string $comment_time
 * @property-read \App\Labdoc $labdoc
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereCommentTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereIdComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereIdLabdoc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereIdUser($value)
 * @mixin \Eloquent
 */
class Comment extends Model
{
    protected $table = 'comment';
    protected $primaryKey = 'id_comment';
    const CREATED_AT = 'comment_time';

    public $fillable = [ 'id_labdoc', 'id_user', 'content' ];

    public $timestamps = false;

    /**
     * Get the Labdoc that owns the comment.
     * @return \App\Labdoc
     */
    public function labdoc()
    {
        return $this->belongsTo('App\Labdoc', 'id_labdoc');
    }
}
