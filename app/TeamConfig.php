<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * App\TeamConfig
 *
 * @property int $id_team_config
 * @property int $id_mission
 * @property int $id_class
 * @property int $method 0=undef 1=student-choice 2=teacher-choice 3=random
 * @property int|null $size_opt
 * @property int|null $size_max
 * @property int|null $size_min
 * @property int|null $teams_max
 * @property string|null $start_datetime
 * @property string|null $end_datetime
 * @property int $allow_msg_teacher authorize messages to the class teachers (boolean)
 * @property int $allow_msg_team authorize messages to the other team members (boolean)
 * @property int|null $allow_msg_id_class authorize messages to class: NULL=none other=id_class
 * @property int|null $allow_msg_id_mission authorize messages to mission students: NULL=none other=id_mission
 * @property int $allow_attach_ld authorize attaching LD to outgoing messages (boolean)
 * @property int $allow_save_ld authorize saving into the report the LD received in messages (boolean)
 * @property int $allow_import Allow import labdoc.  If true, may be bound to a mission with allow_import_id_mission
 * @property int|null $allow_import_id_mission Limit LD import to a single mission
 * @property string $name_prefix
 * @property int $email_on_create send an email to members when a team is created (boolean)
 * @property string $creation_time
 * @property string|null $update_time
 * @property-read \App\Classe $classe
 * @property-read \App\Mission $mission
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Report[] $reports
 * @property-read int|null $reports_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeamConfig newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeamConfig newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeamConfig query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeamConfig whereAllowAttachLd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeamConfig whereAllowImport($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeamConfig whereAllowImportIdMission($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeamConfig whereAllowMsgIdClass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeamConfig whereAllowMsgIdMission($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeamConfig whereAllowMsgTeacher($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeamConfig whereAllowMsgTeam($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeamConfig whereAllowSaveLd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeamConfig whereCreationTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeamConfig whereEmailOnCreate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeamConfig whereEndDatetime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeamConfig whereIdClass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeamConfig whereIdMission($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeamConfig whereIdTeamConfig($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeamConfig whereMethod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeamConfig whereNamePrefix($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeamConfig whereSizeMax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeamConfig whereSizeMin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeamConfig whereSizeOpt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeamConfig whereStartDatetime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeamConfig whereTeamsMax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TeamConfig whereUpdateTime($value)
 * @mixin \Eloquent
*/

class TeamConfig extends Model
{
    const CREATED_AT = 'creation_time';
    const UPDATED_AT = 'update_time';

    const NO_METHOD = 0;
    const METHOD_STUDENTCHOICE = 1;
    const METHOD_TEACHERCHOICE = 2;
    const METHOD_RANDOM = 3;
    const METHOD_COPY = 4;

    protected $table = 'team_config';
    protected $primaryKey = 'id_team_config';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_class',
        'id_mission',
        'method',
        'size_opt',
        'size_max',
        'size_min',
        'teams_max',
        'start_datetime',
        'end_datetime',
        'allow_msg_teacher',
        'allow_msg_team',
        'allow_msg_id_class',
        'allow_msg_id_mission',
        'allow_attach_id',
        'allow_save_id',
        'allow_import',
        'allow_import_id_mission',
        'allow_import',
        'name_prefix',
        'email_on_create',
    ];

    /**
     * Cast fields that are not correctly recognized by Laravel
     */
    protected $casts = ['method' => 'int'];

    /**
     * Returns the team config's mission
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo|\App\Mission
     */
    public function mission()
    {
        return $this->belongsTo('App\Mission', 'id_mission');
    }


    /**
     * Returns the labdocs of the team_config
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function labdocs()
    {
        return $this->hasMany('App\Labdoc', 'id_team_config');
    }


    /**
     * Returns the team config's classe
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo|\App\Classe
     */
    public function classe()
    {
        return $this->belongsTo('App\Classe', 'id_class');
    }

    /**
     * Returns the reports of the team_config
     * @return \Illuminate\Database\Eloquent\Relations\hasMany|\App\Report[]
     */
    public function reports()
    {
        return $this->hasMany('App\Report', 'id_team_config');
    }


    /**
     * Get started reports
     * @return \Illuminate\Database\Eloquent\Relations\hasMany|\App\Report[]
     */
    public function startedReports()
    {
        return $this->reports()->where('initialized', 1);
    }

    /**
     * Get submitted reports
     * @return \Illuminate\Database\Eloquent\Relations\hasMany|\App\Report[]
     */
    public function submittedReports()
    {
        return $this->reports()->where('status', Report::WAIT);
    }

    /**
     * checks if the teamconfig has started reports
     * @return bool
     */
    public function hasStartedReports()
    {
        return $this->startedReports->isNotEmpty();
    }

    /**
     * Is the team_config from an external institution ?
     * @return boolean
     */
    public function isExternal() {
        return $this->classe->isExternal();
    }

    /**
     * Returns the number of reports of a team_config by id
     * @param int $id_team_config
     * @return int
     */
    public static function countReports($id_team_config)
    {
        return \App\Report::WhereHas('teamConfig', function ($query) use ($id_team_config) {
            $query->Where('team_config.id_team_config', $id_team_config);
        })->count();
    }

    /**
     * Returns the students linked to the teamconfig
     * @retun \Illuminate\Database\Eloquent\Collection|\App\User[]
     */
    public function users()
    {
        return $this->reports->flatMap(function ($r) {
            return $r->users;
        });
    }


    /**
     * Returns the teachers linked to the teamconfig
     * @retun \Illuminate\Database\Eloquent\Collection|\App\Teacher[]
     */
    public function teachers()
    {
        $mission_teachers_query = $this->mission
            ->teachers()
            ->select('teacher.*')
            ->getQuery();
        $class_teachers_query = $this->classe
            ->teachers()
            ->select('teacher.*')
            ->getQuery();
        return $mission_teachers_query->union($class_teachers_query);
    }

    /**
     * Returns the teachers of the mission and the class
     * @return array
     */
    public function getTeachersList()
    {
        $teachers = \App\User::whereIn('id_user', $this->mission->teachers()->pluck('teacher.id_teacher'))
            ->whereIn('id_user', $this->classe->teachers()->pluck('teacher.id_teacher'))
            ->orderBy('user.user_name')
            ->get();
        $result = [];
        foreach ($teachers as $teacher) {
            $result[] = $teacher->first_name . " " . $teacher->user_name;
        }
        return $result;
    }


    /**
     * Returns the size of the smallest report
     * @return int
     */
    public function getMinTeamSize()
    {
        return DB::Select(
            DB::raw(
                "SELECT MIN(size) FROM (
                    SELECT count(*) AS size FROM report r JOIN link_report_learner lrl USING(id_report)
                    WHERE r.id_team_config = :id_team_config
                    GROUP BY r.id_report
            ) AS t"
            ),
            [':id_team_config' => $this->id_team_config]
        );
    }

    /**
     * Deletes the model if possible
     * @return bool|null
     */
    public function delete()
    {
        if (!$this->hasStartedReports()) {
            $this->reports()->delete();
            return parent::delete();
        }
        return false;
    }

    /**
     * @param int $id_mission
     * @param int $id_class
     * @param int $id_team_config
     * @return object Record from the table team_config
     */
    public static function get($id_mission, $id_class, $id_team_config = null)
    {
        if ($id_team_config) {
            $record = \App\TeamConfig::find($id_team_config);
        } else {
            $record = \App\TeamConfig::where('id_mission', $id_mission)
                ->where('id_class', $id_class)->first();
        }
        if (!$record) {
            return null;
        }
        foreach (['id_team_config', 'id_mission', 'id_class', 'method', 'size_opt', 'size_max', 'size_min', 'teams_max', 'allow_msg_id_class', 'allow_msg_id_mission', 'allow_import_id_mission'] as $f) {
            if (isset($record->$f)) {
                $record->$f = (int) $record->$f;
            }
        }
        foreach (['allow_msg_teacher', 'allow_msg_team', 'allow_attach_ld', 'allow_save_ld', 'allow_import','email_on_create'] as $f) {
            if (isset($record->$f)) {
                $record->$f = (boolean) $record->$f;
            }
        }
        $record->external = $record->isExternal();
        return $record;
    }

    /**
     * Insert or update a team_config record.
     * @param \StdClass $teamSettings
     * @return \App\TeamConfig|null
     */
    public static function saveOrUpdate($teamSettings)
    {
        // Unicity per (class, mission)
        $teamConfig = TeamConfig::where('id_mission', $teamSettings->id_mission)
            ->where('id_class', $teamSettings->id_class)
            ->first();
        if (!$teamConfig) {
            $teamConfig = new \App\TeamConfig();
            $isCreated = true;
        } else {
            if (!$teamSettings->id_team_config) {
                // The teacher did not know that there was a teamconfig => abort
                return [
                    "teamConfig" =>null,
                    "traceAddingTeamConfig" => false,
                    "traceEditingTemConfig" => false
                ];
                
            }
            $isCreated = false;
        }
        // $teamConfig is either a new teamConfig or the existing one
        // Prepare fields
        if (empty($teamSettings->name_prefix)) {
            $teamSettings->name_prefix = __('Equipe_'); 
        }

        // merge date and time into datetime, for start and end
        if (empty($teamSettings->start_datetime) && preg_match('/^\d{4}-\d\d-\d\d \d\d:\d\d:\d\d$/', $teamSettings->start_datetime)) {
            if (!empty($teamSettings->start_date) && preg_match('/^\d{4}-\d\d-\d\d$/', $teamSettings->start_date)) {
                $teamSettings->start_datetime = $teamSettings->start_date . " " . (empty($teamSettings->start_time) ? '00:00:00' : $teamSettings->start_time . ':00');
            } else {
                $teamSettings->start_datetime = null;
            }
        }
        if (isset($teamSettings->start_date)) {
            unset($teamSettings->start_date);
        }
        if (empty($teamSettings->end_datetime)) {
            if (!empty($teamSettings->end_date) && preg_match('/^\d{4}-\d\d-\d\d$/', $teamSettings->end_date)) {
                $teamSettings->end_datetime = $teamSettings->end_date . " " . (empty($teamSettings->end_time) ? '23:59:59' : $teamSettings->end_time . ':59');
            } else {
                $teamSettings->end_datetime = null;
            }
        }
        if (isset($teamSettings->end_date)) {
            unset($teamSettings->end_date);
        }

        $stringFields = [
            'start_datetime', 'end_datetime', 'name_prefix',
        ];
        foreach ($stringFields as $f) {
            if (isset($teamSettings->$f)) {
                $teamConfig->$f = $teamSettings->$f;
            }
        }
        $nullableFields = ["allow_msg_id_class", "allow_msg_id_mission", "allow_import_id_mission"];
        $boolFields = [
            'allow_msg_teacher', 'allow_msg_team', 'allow_attach_ld', 'allow_save_ld', 'allow_import','email_on_create',
        ];
        foreach ($boolFields as $f) {
            if (!property_exists($teamSettings, $f)) {
                continue;
            }
            $teamConfig->$f = $teamSettings->$f;
        }
        $intFields = [
            'id_team_config', 'id_mission', 'id_class', 'method', 'size_opt', 'size_max', 'size_min', 'teams_max',
            'allow_msg_id_class', 'allow_msg_id_mission', 'allow_import_id_mission',
        ];
        foreach ($intFields as $f) {
            if (!property_exists($teamSettings, $f)) {
                continue;
            }
            if (in_array($f, $nullableFields) && ($teamSettings->$f === null || $teamSettings->$f === 0)) {
                $teamConfig->$f = null;
            } else if ($teamSettings->$f !== null) {
                $teamConfig->$f = (int)$teamSettings->$f;
            }
        }
      
        $isUpdated= $teamConfig->getDirty();
        $teamConfig->save();
        
        return [
            "teamConfig" =>$teamConfig->fresh(),
            "traceAddingTeamConfig" => $isCreated,
            "traceEditingTemConfig" => $isUpdated
            ];
    }

    private function deleteOldNewTeams($teams) {
        $ids = array_filter(array_map(
            function($t) { return isset($t->id_report) ? (int) $t->id_report : null; },
            $teams
        ));
        
        return $this->reports()
            ->where('status', Report::NEW)
            ->whereNotIn('id_report', $ids)
            ->where('initialized', 0)
            ->delete();
        
    
    }

    /**
     * Save all the teams.
     *
     * This function asserts that each student can only be in a single team for this teamConfig.
     *
     * @param array $teamConfig Assoc array
     * @param array $teams List of assoc arrays
     * @param bool $retroactive Apply config changes to previous teams
     * @return integer|null Number of teams where membership has changed.
     */
    public function saveTeams(array $teams, $retroactive = false) {
        $this->deleteOldNewTeams($teams);

        $teamRegistrations = [];
        $nonEmptyTeams = 0;
        $teams =  array_map(function($t) { return (object) $t; }, $teams);
        foreach ($teams as $t) {
            if (empty($t->members)) {
                if (!empty($t->id_report)) {
                    Log::warning("The report (ID = {$t->id_report} is stripped of its members.");
                    DB::table('link_report_learner')->where('id_report', $t->id_report)->delete();
                }
                continue;
            }
            $nonEmptyTeams++;
            if (empty($t->id_report)) {
                $report = \App\Report::createFromTeamconfig($this);
                $report->team_name = empty($t->name) ? "" : $t->name;
                $report->save();
                $t->id_report = (int) $report->id_report;
            } else {
                $report = \App\Report::withTrashed()->find($t->id_report);
                if ($report->team_name !== $t->name) {
                    $report->team_name = $t->name;
                    $report->save();
                }
            }
            $membersIds = array_map(function($x) { return $x['id_user']; }, $t->members);
            if ($report->setMembers($membersIds)) {
                $teamRegistrations[] = $report->id_report;
            }
        }
        if ($nonEmptyTeams) {
            // If retroactive, all the existing reports are updated.
            // If not, only those whose status is 'new'
            $ids = array_filter(array_map(
                function($t) use($retroactive) {
                    if (empty($t->id_report)) {
                        return null;
                    }
                    if ($retroactive || $t->status === 'new') {
                        return $t->id_report;
                    }
                },
                $teams
            ));
            if ($ids) {
                \App\Report::updateAllFromTeamconfig($this, $ids);
            }
            return count(array_unique($teamRegistrations));
        } else {
            return 0;
        }
    }

    /**
     * Returns the string of the method
     * @param int $method METHOD_COPY|METHOD_RANDOM|METHOD_STUDENTCHOICE|METHOD_TEACHERCHOICE
     * @return string
     */
    public static function methodString($method)
    {
        switch ($method) {
            case self::METHOD_COPY:
                return __("copie d'une mise en équipe");
            case self::METHOD_RANDOM:
                return __("aléatoire");
            case self::METHOD_STUDENTCHOICE:
                return __("au choix des étudiants");
            case self::METHOD_TEACHERCHOICE:
                return __("au choix de l'enseignant");
        }
    }

    /**
     * Prints the teamconfig infos
     * @return string
     */
    public function infos()
    {
        $start = $this->start_datetime ? $this->start_datetime : __("indefini");
        $end = $this->end_datetime ? $this->end_datetime : __("indefini");
        $method = __("méthode").": ".self::methodString($this->method);
        return $this->id_team_config." (".$this->mission->code." ".__("du")." ".$start." ".__("au")." ".$end." ".$method.")";
    }

    /**
     * Returns the teams with there members
     * @return array
     */
    public function getTeams()
    {
        $teams = [];
        foreach ($this->reports as $r) {
            $teams [] = (object) [
                "name" => $r->team_name,
                "id_report" => $r->id_report,
                "members" => $r->users()
                               ->select(['first_name', 'user.id_user', 'user_name'])
                               ->orderBy('user_name')
                               ->orderBy('first_name')
                               ->get()
            ];
        }
        return $teams;
    }
}
