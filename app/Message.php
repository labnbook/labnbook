<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Message
 *
 * @property int $id_message
 * @property string|null $msg_content
 * @property string $msg_time
 * @property int|null $id_user_sender
 * @property int|null $id_labdoc
 * @property int $id_conversation
 * @property-read \App\Conversation $conversation
 * @property-read \App\Labdoc|null $labdoc
 * @property-read \App\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Message newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Message newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Message query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Message whereIdConversation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Message whereIdLabdoc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Message whereIdMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Message whereIdUserSender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Message whereMsgContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Message whereMsgTime($value)
 * @mixin \Eloquent
 */
class Message extends Model
{
    protected $table = 'message';
    protected $primaryKey = 'id_message';
    const CREATED_AT = 'msg_time';

    public $timestamps = false;

    /**
     * Get the Labdoc that owns the comment.
     * @return \App\Labdoc
     */
    public function labdoc()
    {
        return $this->belongsTo('App\Labdoc', 'id_labdoc');
    }

    /**
     * Get the conversation that owns the comment.
     * @return \App\Conversation
     */
    public function conversation()
    {
        return $this->belongsTo('App\Conversation', 'id_conversation');
    }

    /**
     * Get the user that owns the comment.
     * @return \App\User
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'id_user_sender');
    }

    /**
     * @param string $msg
     * @return string snapshot
     */
    public static function getSnapshotFromMsg($msg)
    {
        if (strpos($msg, "<br")) {
            $length = strpos($msg, "<br");
            $msg = substr($msg, 0, $length)."..." ;
        }
        return $msg;
    }
}
