<?php

namespace App\Http\Controllers;

use App\Helper;
use App\Labdoc;
use App\LabdocVersioning;
use App\Processes\LabdocDiff;
use App\Report;
use App\ReportPart;
use App\Trace;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LabdocController extends Controller
{
    /**
     * Display a labdoc
     *
     * @param  \App\Labdoc  $labdoc
     * @return \Illuminate\Http\Response
     */
    public function show(Labdoc $labdoc)
    {
        $user = Auth::user();
        abort_if($user->cannot('view', $labdoc), 403);
        return view('labdoc/ldStandalone')->with(['ld' => $labdoc]);
    }

    /**
     * Returns a labdoc as JSON
     *
     * @param  \App\Labdoc  $labdoc
     * @return \Illuminate\Http\Response
     */
    public function getData(Labdoc $labdoc)
    {
        $user = Auth::user();
        abort_if($user->cannot('view', $labdoc), 403);
        if ($labdoc->type_labdoc === 'dataset'){
            return response()->json($labdoc->append('auto_fit'));
        }
        return response()->json($labdoc);
    }
    /**
     * Returns a copex blade template
     *
     * @param \Illuminate\Http\Request $request
     * @param  \App\Labdoc  $labdoc
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function getCopexTemplate(Request $request, Labdoc $labdoc)
    {
        $user = Auth::user();
        abort_if($user->cannot('view', $labdoc), 403);
        $data = $request->validate(['edition' => 'string']);
        if($data['edition'] === 'true' || $data['edition'] === '1'){
            $data['edition'] = 1;
        } else {
            $data['edition'] = 0;
        }
        $quantities_data = file_get_contents(public_path('/tool_copex/v2_json/quantities.json'));
        return view('labdoc.copex.copex')->with([
            'ld'=> $labdoc,
            'edition'=>$data['edition'], 
            'quantities_data'=>$quantities_data,
            'is_teacher' => (isset($labdoc->id_report) || isset($labdoc->id_team_config)) ? 0 : 1, 
            'is_super_manager' => $user->canEditSimulation($labdoc->mission->simulation_code) ? 1 : 0
        ]);
    }

    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Labdoc  $labdoc
     * @return \Illuminate\Http\Response
     */
    public function extend(Request $request, Labdoc $labdoc)
    {
        $user = Auth::user();
        abort_if($user->cannot('view', $labdoc), 403);
        $labdoc->updateStatus(['extend'=>(int)$request->input('extend')], $user->id_user);
    }

    /**
     * Hide a labdoc's star
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Labdoc  $labdoc
     * @return \Illuminate\Http\Response
     */
    public function hideStar(Request $request, Labdoc $labdoc)
    {
        $user = Auth::user();
        abort_if($user->cannot('view', $labdoc), 403);
        $data = $request->validate(['last_id_version_reviewed' => 'sometimes|nullable|int']);
        if (array_key_exists('last_id_version_reviewed', $data)){
            $last_id_version_reviewed = $data['last_id_version_reviewed'];
        } else {
            $last_id_version_reviewed = null;
        }
        $labdoc->storeLastIdVersionReviewed($last_id_version_reviewed, $user->id_user);
        if ($labdoc->last_id_version == $labdoc->getLastIdVersionReviewed($user)){
            $labdoc->updateStatus(['mod_icon' => 0], $user->id_user);
        }
    }

    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Labdoc  $labdoc
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Labdoc $labdoc)
    {
        $user = Auth::user();
        abort_if($user->cannot('update', $labdoc), 403);
        $content = (string)$request->input('content');
        $name = (string)$request->input('name');
        $trace = (int)$request->input('trace');
        $old_name = $labdoc->name;
        
        if ($labdoc->report) {
            $id_mission = $labdoc->report->id_mission;
        } else {
            $id_mission =  $labdoc->mission->id_mission;
        }

        // Update in DB and check if the logging of traces is needed 
        $result = $labdoc->updateinDB($user->id_user, $id_mission, $content, $name);
        if ($result['error'] === Labdoc::CONTENT_ERR_FATAL) {
            abort(422, __("Le contenu du labdoc n'a pu être sauvé"));
        }

        // Add trace if laddoc is in report (aka not in mission), the report is not in status on or tuto
        if ($labdoc->report && $labdoc->report->shouldTrace() && $trace) { // ne pas tracer en mode test ni en mode high_frequency_synchro
            if ($result['content_has_changed']) {
                Trace::logAction(Trace::MODIFY_LD, ['id_report' => $labdoc->report->id_report, 'id_mission' => $labdoc->report->id_mission, 'id_labdoc' => $labdoc->id_labdoc]);
            }
            if ($result['name_has_changed']) {
                Trace::logAction(Trace::UPDATE_LD_NAME, ['id_report' => $labdoc->report->id_report, 'id_mission' => $labdoc->report->id_mission, 'id_labdoc' => $labdoc->id_labdoc, 'new' => $name, 'previous' => $old_name]);
            }
        }
        
        return response()->json($result);
    }

    /**
     * Send the header of the LD
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Labdoc  $labdoc
     * @return \Illuminate\Http\Response
     */
    public function getLDHeader(Request $request, Labdoc $labdoc)
    {
        $user = Auth::user();
        $edition = (int)$request->input('edition');
        $scope = (string)$request->input('scope');
        if ($edition) {
            abort_if($user->cannot('update', $labdoc), 403);
        } else {
            abort_if($user->cannot('view', $labdoc), 403);
        }
        $ldv = \App\Views\Labdoc::fromLd($labdoc, $user, $scope, $edition);
        return view('labdoc/_ld')->with(['ldv' =>$ldv]);
    }

    /**
     * modifie le statut draft du LD si possible
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Labdoc  $labdoc
     * @return \Illuminate\Http\Response
     */
    public function toggleDraftStatus(Request $request, Labdoc $labdoc)
    {
        $user = Auth::user();
        abort_if($user->cannot('changeDraftStatus', $labdoc), 403);

        $id_report = (int) $request->input('id_report');
        $draft = (int) $request->input('draft');

        $labdoc->updateDraftStatus($user->id_user, $draft);

        $report = \App\Report::find($id_report);
        if ($report->shouldTrace()) {
            $st = $draft ? "draft" : "visible";
            Trace::logAction(Trace::TOGGLE_LD_STATUS, ['id_report' => $id_report, 'id_mission' => $report->id_mission, 'id_labdoc' => $labdoc->id_labdoc, 'status' => $st]);
        }
    }


    /**
     * Retrieve differences of all labdocs of the labdoc
     * @param \Illuminate\Http\Request $request
     * @param \App\Labdoc $labdoc
     * @return \App\Processes\Array|array[]
     */
    public function getDiff(Request $request, Labdoc $labdoc)
    {
        $user = Auth::user();
        abort_if($user->cannot('view', $labdoc), 403);
        $parameters = $request->validate([
            'ld_to_diff.last_ld_id_version_reviewed' => 'sometimes|nullable|int',
            'ld_to_diff.current_ld_id_version' => 'sometimes|nullable|int'
        ]);
        return LabdocDiff::getDiffs($labdoc, $user, $parameters);
    }

    /**
     * Log display diff trace
     * @param \Illuminate\Http\Request $request
     * @param \App\Labdoc $labdoc
     */
    public function logDisplayDiffsTrace(Request $request, Labdoc $labdoc)
    {
        $user = Auth::user();
        abort_if($user->cannot('view', $labdoc),Helper::pullPolicyErrorCode(), Helper::pullPolicyErrorMessage());
        Trace::logAction(Trace::TEACHER_DISPLAY_DIFFS, ['id_labdoc'=>$labdoc->id_labdoc]);
    }

    /**
     * Move a labdoc.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Labdoc  $labdoc
     * @return \Illuminate\Http\Response
     */
    public function move(Request $request, Labdoc $labdoc)
    {
        $user = Auth::user();
        $data = $request->validate([
            'id_rp' => 'int|required',
            'position' => 'int|required',
            'ld_in_rp' => 'array|required',
            'ld_in_rp.*' => 'int',
        ]);
        $ans = null;
        abort_if($user->cannot('update', $labdoc), 403);
        if ($labdoc->id_report_part != $data['id_rp']) {
            $rp = ReportPart::find($data['id_rp']);
            if (!$rp || $rp->id_mission != $labdoc->report->id_mission) {
                $ans = [
                    'status' => 'fail',
                    'message' => __("Partie de rapport erronée")
                ];
            }
            $type = $labdoc->type_labdoc;
            if (!$rp->$type) {
                $ans = [
                    'status' => 'fail',
                    'message' => __(
                        "Impossible de déplacer un labdoc :type dans cette partie de rapport",
                        ['type' => $labdoc->getTypeNameTranslated()]
                    )
                ];
            }
        }
        if (!$ans) {
            $ans = $labdoc->move($user->id_user, $data['id_rp'], $data['position'], $data['ld_in_rp']);
        }
        return response()->json($ans);
    }

    /**
     * Remove a status from the labdoc labdoc.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Labdoc  $labdoc
     * @return \Illuminate\Http\Response
     */
    public function removedStatus(Request $request, Labdoc $labdoc)
    {
        $user = Auth::user();
        abort_if($user->cannot('view', $labdoc), 403);
        $status = $request->input('status');
        $removed = $labdoc->removedStatus($user->id_user, $status);
        return response()->json($removed);
    }
}
