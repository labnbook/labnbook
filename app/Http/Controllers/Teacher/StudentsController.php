<?php

namespace App\Http\Controllers\Teacher;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class StudentsController extends Controller
{
    /**
     * Display a list of existing and potential reports for a student.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = $request->validate([
            'id_team_config' => 'int|nullable',
        ]);
        $user = Auth::user();
        $page = $request->path();
        \App\Teacher::enterInterface($user->id_user, $page);
        $id_class = null;
        if (isset($data['id_team_config'])) {
            $tc = \App\TeamConfig::find($data['id_team_config']);
            if ($tc) {
                $id_class = $tc->id_class;
                $classe = \App\Classe::find($id_class);
                abort_if($user->cannot('view', $classe), 403);
            }
        }

        return view('teacher/students/index')->with([
            'user' => $user,
            'id_class' => $id_class
        ]);
    }

    /**
     * affiche un json contenant toutes les informations de l'utilisateur  :
     * - les institutions de la classe (sélectionnée par défaut) et de l'enseignant
     * - les informations de l'utilisateur si $id_user != 0
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function initEditLearnerData(Request $request)
    {
        $data = $request->validate([
            'id_class' => 'int|required',
            'id_learner' => 'int|nullable',
        ]);

        if ($data['id_learner']) { // modification d'un étudiant
            $out = DB::table('user')->where('id_user', $data['id_learner'])
                                    ->selectRaw('id_user, user_name, first_name, login, email, inst_number, pwd is NULL as cas')
                                    ->first();
        } else {
            $out = new \StdClass();
            $out->cas = false;
        }

        // récupérations des de l'institution de la classe
        $cl_inst = \App\Classe::find($data['id_class'])->institution()->select('institution.id_inst', 'institution.name')->first();
        $out->institutionsHTML = '<input type="hidden" value="' . $cl_inst->id_inst . '">' . htmlspecialchars($cl_inst->name);

        return response()->json($out);
    }

    /**
     * Get the user classes
     * @return array [ {id_class, id_inst, class_name, class_code, inst_name, nb_learners, teachers, teamMissions} ]
     */
    public function getClasses(Request $request)
    {
        $teacher = Auth::user()->teacher;
        return response()->json(['data' => $teacher->getClasses()]);
    }

    /**
     * Return a list of students, similar to the CSV format.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function findAllByStudentNumbers(Request $request)
    {
        $data = $request->validate([
            'institute' => 'int',
            'studentNumbers' => 'string',
        ]);

        try {
            return response()->json(
                \App\Processes\AddCSVLearners::findAllByStudentNumbers(
                    $data['institute'],
                    $data['studentNumbers']
                )
            );
        } catch (\Exception $ex) {
            return response()->json(
                ['error' => __("Erreur") . __("&nbsp;: ") . $ex->getMessage()],
                503
            );
        }
    }

    /**
     * Return a list of students, similar to the CSV format.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getAddStudentsView(Request $request)
    {
        $user = Auth::user();
        $data = $request->validate([
            'method' => 'in:csv,form,number,class,code',
            'id_class' => 'int|nullable',
        ]);
        $params = [];
        $id_inst = \App\Classe::find($data['id_class'])->id_inst;
        if ($data['method'] == 'form') {
                $params['prefix'] = 'add-students-';
        } elseif ($data['method'] == 'class') {
                $params['classes'] = $user->teacher
                                          ->classes()
                                          ->where('class.id_class', '<>', $data['id_class'])
                                          ->where('class.id_inst', $id_inst)
                                          ->withLearners()
                                          ->groupBy('id_class')
                                          ->orderBy('class_name')
                                          ->get();
        } else if($data['method'] == 'code') {
            $params['class_code'] = \App\Classe::find($data['id_class'])->class_code;
        }
        $view = 'teacher/students/_addStudentBy'.ucfirst($data['method']);
        return view($view)->with($params);
    }

    /**
     * Return a list of students, similar to the CSV format.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getLearnerNameDataList(Request $request)
    {
        $user = Auth::user();
        $adminIds = "(".implode(',', \App\Teacher::getAdminIds()).")";
        $sql = <<<EOSQL
SELECT id_user, user_name, first_name, GROUP_CONCAT(loc SEPARATOR ', ') AS loc
FROM (
    (
        SELECT u.id_user, u.user_name, u.first_name, class_name AS loc
        FROM user u
            JOIN link_class_learner as lcl ON lcl.id_user = u.id_user
            JOIN class ON lcl.id_class = class.id_class
            JOIN link_class_teacher lct ON lct.id_class = lcl.id_class
        WHERE
            class.status = 'normal'
            AND lct.id_teacher = :id_user
            AND u.id_user NOT IN $adminIds
    ) UNION (
        SELECT u.id_user, u.user_name, u.first_name, CONCAT("ens. ",i.name) AS loc
        FROM user u
            JOIN link_inst_teacher lit1 ON lit1.id_teacher = u.id_user
            JOIN link_inst_teacher lit2 ON lit1.id_inst = lit2.id_inst
            JOIN institution i ON lit1.id_inst = i.id_inst
        WHERE
            lit2.id_teacher = :id_user
            AND u.id_user NOT IN $adminIds
    )
) AS tmp
GROUP BY id_user
ORDER BY user_name
EOSQL;
        $args = [':id_user' => $user->id_user];
        $results = DB::select($sql, $args);
        $return = '';
        foreach ($results as $row) {
            $return .= sprintf(
                '<option data-id-learner="%d" value="%s (%s)" />',
                $row->id_user,
                htmlspecialchars($row->user_name . " " . $row->first_name),
                htmlspecialchars($row->loc)
            );
        }
        return response()->json($return);
    }
}
