<?php

namespace App\Http\Controllers\Teacher;


use App\Http\Controllers\Controller;

class TeacherHelper extends Controller
{
    public static function parseDateTime($input, $name)
    {
        if (isset($input[$name . '_date']) && preg_match('/^\d{4}-\d\d-\d\d$/', $input[$name . '_date'])) {
            return $input[$name . '_date'] . " " . ($input[$name . '_time'] ? $input[$name . '_time'] . ':00' : '00:00:00');
        }
        return null;
    }

    /**
     * Parse an associative array and return an array whose keys are columns of the table report.
     *
     * @param array $input POST data
     * @return array
     */
    public static function parseTeamInput($input) {
        $team = [];
        foreach (['id_mission', 'id_report', 'allow_msg_teacher', 'allow_msg_team', 'allow_msg_id_class', 'allow_msg_id_mission', 'allow_attach_ld', 'allow_save_ld', 'allow_import', 'allow_import_id_mission'] as $k) {
            $team[$k] = isset($input[$k]) && ($input[$k] !== '')  ? (int) $input[$k] : null;
        }
        foreach (['status', 'start_datetime', 'end_datetime', 'start_datetimes_are_unified', 'end_datetimes_are_unified'] as $k) {
            $team[$k] = isset($input[$k]) && ($input[$k] !== '') ? (string) $input[$k] : null;
        }
        foreach (['team_name'] as $k) {
            $team[$k] = isset($input[$k]) && ($input[$k] !== '') ? (string) $input[$k] : '';
        }
        foreach (['start', 'end'] as $name) {
            if (empty($team[$name . '_datetime'])) {
                $team[$name . '_datetime'] = self::parseDateTime($input, $name);
            }
        }
        return $team;
    }

    /**
     * @param array $serialized
     * @return array
     */
    public static function serializedToAssoc($serialized) {
        $assoc = [];
        foreach ($serialized as $s) {
            $assoc[$s['name']] = $s['value'];
        }
        return $assoc;
    }
    
    /**
     * Keep start and end datetimes only if they have been unified.
     * @param array $updates
     * @return array
     */
    public static function dateTimeUpdate($updates) {
        foreach (array('start', 'end') as $val) {
            $key = $val . '_datetimes_are_unified';
            if (isset($updates[$key]) 
                && (int)$updates[$key] === 0) {
                unset($updates[$val . '_datetime']);
            }
            unset($updates[$key]);
        }
        return $updates;
    }
}
