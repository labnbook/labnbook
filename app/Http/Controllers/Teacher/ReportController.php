<?php

namespace App\Http\Controllers\Teacher;

use App\Classe;
use App\Http\Controllers\Controller;
use App\Helper;
use App\Report;
use App\Indicators;
use App\Scoring\Rubric;
use App\TeamConfig;
use App\Trace;
use App\Mission;
use App\Scoring\Assessment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ReportController extends Controller
{
    /**
     * Checks that the user is teacher of all the given reports
     * @param \Illuminate\Database\Eloquent\Collection|\App\Report[] $reports
     * @param boolean $delete is this a check for deletion
     */
    private function checkPermsTeacherReports($reports, $delete = false)
    {
        $user = Auth::user();
        $action = $delete ? 'delete' : 'manage';
        foreach ($reports as $r) {
            abort_if($user->cannot($action, $r), 403, __("Vous n'avez pas la permission de gérer un des rapports"));
        }
    }
    
    public function getMissionsForFilters(Request $request)
    {
        $data = $request->validate([
            'search' => 'string|nullable',
            'page' => 'int|nullable',
            'pageSize' => 'int|nullable',
            'id_class' => 'int|nullable',
        ]);

        $user = Auth::user();
        $search = '';
        if (isset($data['search'])) {
            $search = $data['search'];
        }
        $id_class = 0;
        if(isset($data['id_class'])) {
            $id_class = $data['id_class'];
        }
        $missions = Mission::getMissionsForFilters($user, $id_class, $search, $data['page'], $data['pageSize']);
        
        return response()->json(['missions' => $missions->items(), 'searchTerm' => $search,
            'pagination' => ['page' => $missions->currentPage(), 'more' => $missions->hasMorePages()]]);
    }
    
    public function getClassesForFilters(Request $request)
    {
        $data = $request->validate([
            'search' => 'string|nullable',
            'page' => 'int|nullable',
            'pageSize' => 'int|nullable',
            'id_mission' => 'int|nullable',
        ]);
        $user = Auth::user();
        $search = '';
        if (isset($data['search'])) {
            $search = $data['search'];
        }
        $id_mission = 0;
        if(isset($data['id_mission'])) {
            $id_mission = $data['id_mission'];
        }
        $classes = Classe::getClassesForFilters($user, $id_mission, $search, $data['page'], $data['pageSize']);
        
        return response()->json(['classes' => $classes->items(), 'searchTerm' => $search,
            'pagination' => ['page' => $classes->currentPage(), 'more' => $classes->hasMorePages()]]);
    }

    /**
     * Display a list of existing and potential reports for a student.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        $page = $request->path();
        \App\Teacher::enterInterface($user->id_user, $page);
        
        if ($request->has('team_config')) {
            $tc = \App\TeamConfig::find((int)$request->input('team_config'));
            // To use teamconfig select we should be a teacher of the mission and the class
            abort_if($user->cannot('update', $tc), 403);
        }

        return view('teacher/report/reports')->with([
            'page_name' => $page,
            'user' => $user,
            'missions' => [],
            'classes' => []
        ]);
    }

    /**
     * Return the report and a HTML view of its learners
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function getReportAndLearners(Request $request, Report $report)
    {
        $user = Auth::user();
        abort_if($user->cannot('manage', $report), 403);

        // Add an id_inst to the report
        $report->id_inst = $report->teamConfig->classe->id_inst;
        return response()->json([
            'report' => $report,
            'learners' => view('teacher._usersList', ['users' => $report->users])->render(),
        ]);
    }

    
    /**
     * Return the report's structure
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function getStructure(Request $request, Report $report)
    {
        $user = Auth::user();
        abort_if($user->cannot('manage', $report), 403);

        return response()->json($report->getStructure());
    }
    
    /**
     * Validate that the request contains report filters and returns them
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    private function validateReportFilters($request)
    {
        $data = $request->validate([
            'filters' => 'array',
            'filters.id_mission' => 'integer|nullable',
            'filters.mission_code' => 'string|nullable',
            'filters.id_class' => 'integer|nullable',
            'filters.class_name' => 'string|nullable',
            'filters.teaming' => 'boolean',
            'filters.status' => 'array',
        ]);
        return $data;
    }

    /**
     * Reports array to be used in the "Reports" table
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getReports(Request $request)
    {
        $user = Auth::user();
        $data = $request->validate([
            'draw' => 'integer|nullable',
            'columns' => 'array|nullable',
            'order' => 'array|nullable',
            'start' => 'integer|nullable',
            'length' => 'integer|nullable',
            'search' => 'array|nullable',
        ]);
        $data += $this->validateReportFilters($request);
        // Add the reports
        $out ['dataTable'] = $user->teacher->filterReports($data);
        return response()->json($out['dataTable']);
    }

    /**************************************************************************
     *
     *                  Indicators
     *
     *************************************************************************/

    /**
    * Return reports indactors
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
    */
    public function getIndicators(Request $request)
    {
        $data = $request->validate(['report' => 'integer']);
        if (isset($data['report'])) {
            $report = Report::find($data['report']);
        }
        // Abort if $report is null. 
        if(!$report){
            Log::error("Issue #467 Report null on getIndicators, data: ".json_encode($data));
            abort(404, "Report not found");
          
        }
        $user = Auth::user();
        abort_if($user->cannot('manage', $report), 403);
        return response()->json(Indicators::getIndicators($report));
    }

    /**
    * Return reports indactors
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
    */
    public function getInfoMission(Request $request)
    {
        $data = $request->validate(['mission' => 'integer|required']);
        $mission = Mission::find($data['mission']);
        $user = Auth::user();
        abort_if($user->cannot('view', $mission), 403);
        return response()->json(Indicators::getAverageFromObject($mission));
    }

    /**
    * Return reports indactors
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
    */
    public function getInfoClass(Request $request)
    {
        $data = $request->validate(['id_team_config' => 'integer|required']);
        $teamConfig = TeamConfig::find($data['id_team_config']);

        $user = Auth::user();
        abort_if($user->cannot('view', $teamConfig->classe), 403);
        return response()->json(Indicators::getAverageFromObject($teamConfig));
    }

    /**
    * Return reports indactors
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
    */
    public function getInfoAverageReports(Request $request)
    {
        // Decode JSON before validation
        $json = json_decode($request->input('reports'));
        $request->replace(['reports' => $json]);
        $data = $request->validate(
            [
                'reports' => 'array|required',
                'reports.*' => 'integer',
            ]
        );
        $reports = \App\Report::find($data['reports']);

        $this->checkPermsTeacherReports($reports);
        
        return response()->json(Indicators::getAverageByReports($reports));
    }

    /**
     * Add a teacher select report trace
     * @param  \Illuminate\Http\Request  $request
     * @param \App\Report $report
     * @return \Illuminate\Http\Response
     */
    public function traceSelectReport(Request $request, Report $report)
    {
        $user = Auth::user();
        abort_if($user->cannot('manage', $report), 403);
        Trace::logAction(Trace::TEACHER_SELECT_REPORT, ['id_report' => (int)$report->id_report, 'id_mission' => (int)$report->id_mission]);
    }

    /**
     * Updates a report status
     * @param  \Illuminate\Http\Request  $request
     * @param \App\Report $report
     * @return \Illuminate\Http\Response
     */
    public function updateStatus(Request $request, Report $report)
    {
        $user = Auth::user();
        abort_if($user->cannot('manage', $report), 403);

        $status = $request->input('status');
        // check that the date is in the future
        if ($status === "on" || $status === "new") {
            $end_date = $report->end_datetime;
            if ($end_date && strtotime($end_date) < strtotime(date("Y-m-d 23:59:59"))) {
                return response()->json(__("Le rapport n'a pas pu être désarchivé car sa date de rendu est antérieure à aujourd'hui. Pour modifier le rapport, utilisez l'interface de modification d'un rapport."));
            }
        }

        // authorizations for changing report.status are managed in javascript only
        $report->status = $status;
        $report->save();
        if($report->status==REPORT::ARC){
            Trace::logAction(Trace::TEACHER_ARCHIVE_REPORT, ['id_report' => $report->id_report]);
        }else {
            Trace::logAction(Trace::TEACHER_EDIT_REPORT, ['id_report' => $report->id_report]);
        }
    }

    /**
     * Batch updates reports
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function batchUpdate(Request $request)
    {
        $data = $request->validate([
            'reports' => 'required|array',
            'reports.*' => 'numeric',
            'updates' => 'required|array',
        ]);

        $ids = $data['reports'];
        $updates = TeacherHelper::dateTimeUpdate(
            TeacherHelper::parseTeamInput(TeacherHelper::serializedToAssoc($data['updates']))
        );

        $reports = \App\Report::find($ids);

        if ($reports->isEmpty()) {
            return response()->json([
                'message' => __("Aucun rapport sélectionné."),
            ]);
        }
        $this->checkPermsTeacherReports($reports);
        $existingIds = $reports->keyBy('id_report')->keys();

        $response = [
            'selected' => count($existingIds),
            'updated' => 0,
            'statuses' => 0
        ];

        // Datetime fields are processed separately because a null value is understood as a deletion order
        $allowedDatetimeFields = ['start_datetime', 'end_datetime'];
        $allowedFields = array_merge($allowedDatetimeFields, ['allow_msg_teacher', 'allow_msg_team', 'allow_msg_id_class', 'allow_msg_id_mission', 'allow_attach_ld', 'allow_save_ld', 'allow_import', 'allow_import_id_mission', 'status']);
        $updates = array_filter($updates, function($value, $key) use ($allowedFields, $allowedDatetimeFields) {
            if (in_array($key, $allowedDatetimeFields, true)){
                return $value !== ''; 
            } else {
                return in_array($key, $allowedFields, true) && $value !== null && $value !== '';
            }
        }, \ARRAY_FILTER_USE_BOTH);


        foreach($existingIds as $reportID ){
            $report = Report::find($reportID);
            $updateResult = $report->updateReport($updates);
            $response["updated"] +=  $updateResult ["updated"];
            $response["statuses"] +=  $updateResult ["statuses"];
            if($updateResult["updated"]){
                Trace::logAction(Trace::TEACHER_EDIT_REPORT, ['id_report' => $report->id_report]);
            }
        }
        
        if (!$response) {
            http_response_code(403);
            return response()->json(['message' => __("Vous n'êtes pas autorisé à modifier ces rapports.")], 403);
        }
        return response()->json($response);
    }


    /**
     * Deletes a report
     * @param  \Illuminate\Http\Request  $request
     * @param \App\Report $report
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, Report $report)
    {
        $user = Auth::user();
        abort_if($user->cannot('delete', $report), 403);

        Trace::logAction(Trace::TEACHER_DELETE_REPORT, ['id_report' => $report->id_report, 'id_mission' => $report->id_mission]);
        $report->delete();
        return response()->json(true);
    }

    /**
     * Batch restore reports
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function batchRestore(Request $request)
    {
        $data = $request->validate([
            'reports' => 'required|array',
            'reports.*' => 'numeric',
        ]);
        $user = Auth::user();
        abort_if($user->cannot('restore', Report::class), 403);

        $ids = $data['reports'];

        $restored = \App\Report::onlyTrashed()->whereIn('id_report', $ids)->restore();

        return response()->json($restored);
    }

    /**
     * Deletes several reports 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function batchDelete(Request $request)
    {
        $user = Auth::user();
        $reports = $this->getReportsFromPost($request);

        $nbReportsDeleted = 0;
        foreach($reports as $r){
            abort_if($user->cannot('delete', $r), 403);
            $r->delete();
            Trace::logAction(Trace::TEACHER_DELETE_REPORT, ['id_report' => $r->id_report]);
            $nbReportsDeleted++;
        }
        
        return response()->json([
            'nb_reports_deleted' => $nbReportsDeleted]);
    }

    /**
     * Retrieve reports from list of ids in post
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|\App\Report[]
     */
    private function getReportsFromPost(Request $request)
    {
        $data = $request->validate([
            'id_reports' => 'array',
            'id_reports.*' => 'int',
        ]);
        return \App\Report::find($data['id_reports']);

    }

    /**
     * Archive several reports
     * 2 modes : onlyArchive or toggleArchive
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function batchArchive(Request $request)
    {
        $reports = $this->getReportsFromPost($request);
        $data = $request->validate([
            'new_status' => 'array'
            ]);
        $this->checkPermsTeacherReports($reports);
        
        if(array_key_exists("new_status", $data) && count($data["new_status"])){
            // Toggle archive status
            $numberOfReportArchived = 0;
            $numberOfReportUnarchived = 0;
            foreach ($data["new_status"] as $new_report_status) {
                $report = $reports->find($new_report_status["id_report"]);
                $report->status = $new_report_status["new_status"];
                if($report->status === 'arc') {
                    $numberOfReportArchived++;
                    Trace::logAction(Trace::TEACHER_ARCHIVE_REPORT, ['id_report' => $report->id_report]);
                }else {
                    $numberOfReportUnarchived++;
                }
                $report->save();
            }
            $message = "";
            if ($numberOfReportArchived) {
                $message .= __(":count rapports :status", ["count"=> $numberOfReportArchived, "status"=> __("archivé(s)")]);
            }
            if ($numberOfReportArchived && $numberOfReportUnarchived) {
                $message .= "<br />";
            }
            if ($numberOfReportUnarchived) {
                $message .= __(":count rapports :status", ["count"=> $numberOfReportUnarchived, "status"=> __("desarchivé(s)")]);
            }
        } else {
            // only archive
            foreach ($reports as $r) {
                $r->status='arc';
                $r->save();
                Trace::logAction(Trace::TEACHER_ARCHIVE_REPORT, ['id_report' => $r->id_report]);
            }
            $message = __(":count rapports :status", ["count"=> count($reports), "status"=> __("archivé(s)")]);
        }
        
        return response()->json(['msg' => $message]);
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Report $report)
    {
        $data = $request->validate([
            'team_name' => 'string|nullable',
            'start_date' => 'string|nullable',
            'start_time' => 'string|nullable',
            'end_date' => 'string|nullable',
            'end_time' => 'string|nullable',
            'allow_import' => 'int|nullable',
            'allow_import_id_mission' => 'int|nullable',
            'allow_msg_teacher' => 'int|nullable',
            'allow_msg_team' => 'int|nullable',
            'allow_msg_id_class' => 'int|nullable',
            'allow_msg_id_mission' => 'int|nullable',
            'allow_attach_ld' => 'int|nullable',
            'allow_save_ld' => 'int|nullable',
            'status' => 'in:new,on,wait,arc|required',
            'learners_list' => 'array|required',
            'learners_list.*' => 'int',
            ]
        );
        abort_if(Auth::user()->cannot('manage', $report), 403);

        $errors = [];
        // Update the list of learners
        $learners_changes = $report->updateLearners($data['learners_list']);
        unset($data['learners_list']);


        foreach (['start', 'end'] as $name) {
            $data[$name . '_datetime'] = TeacherHelper::parseDateTime($data, $name);
        }
        $report_changes = $report->updateReport($data);
        if ($report_changes["updated"]) {
            Trace::logAction(Trace::TEACHER_EDIT_REPORT, ['id_report' => $report->id_report]);
        }
        if ($report->status != $data['status']) {
            $errors []= __(
                "Impossible de changer ce rapport du statut ':from' vers le statut ':to'",
                [
                    'from' => Report::statusName($report->status),
                    'to' => Report::statusName($data['status']),
                ]
            );
        }

        return response()->json([
            'errors' => $errors,
            'changes' => [
                'report' => $report_changes,
                'learners_changes' => $learners_changes,
            ]
        ]);
    }
}
