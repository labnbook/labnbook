<?php

namespace App\Http\Controllers\Teacher;

use App\Classe;
use App\Http\Controllers\Controller;
use App\Mission;
use App\Report;
use App\TeamConfig;
use App\Trace;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TracesController extends Controller
{
    /**
     * Select traces given a mission and a class in order to plot graphs
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function selectDataToPlotHeatmap(Request $request){
        $data = $request->validate([
            'id_mission' => 'integer|required',
            'id_class' => 'integer|required',
            ]);

        $mission = Mission::find($data['id_mission']);
        $class = Classe::find($data['id_class']);
        abort_if(Auth::user()->cannot('view', $mission), 403);
        abort_if(Auth::user()->cannot('view', $class), 403);
        
        return Trace::getStudentTraces($mission, $class);
    }

    /**
     * Select traces given a mission and a class in order to plot collab dashboard
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function selectDataToPlotDashboardCollab(Request $request){
        $data = $request->validate([
            'id_mission' => 'integer|required',
            'id_report' => 'integer|required',
            'id_team_config' => 'integer|required',
        ]);

        $mission = Mission::find($data['id_mission']);
        $report = Report::find($data['id_report']);
        $team_config = TeamConfig::find($data['id_team_config']);

        abort_if($report->id_mission != $mission->id_mission || $team_config->id_mission != $mission->id_mission , 400);
        abort_if(Auth::user()->cannot('manage', $report), 403);

        Trace::logAction(Trace::TEACHER_OPEN_COLLABORATIVE_DASHBOARD, ['id_report' => $report->id_report, 'id_mission' => $report->id_mission]);
        
        return Trace::getStudentCollabTraces($mission, $report, $team_config);
    }
}
