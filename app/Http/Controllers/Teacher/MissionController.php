<?php

namespace App\Http\Controllers\Teacher;

use App\Helper;
use App\Labdoc;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Mission;
use App\Trace;
use App\Processes\ExportLabdocs;

class MissionController extends Controller
{
    /**
     * Checks that the labdoc exists, and belongs to the mission and the user
     * can edit / view the mission
     * Return the labdoc or abort if the access is not legal
     * @param \App\Mission $mission
     * @param int $id_labdoc
     * @param \App\User $user
     * @param string $action view|update
     * @return \App\Labdoc
     */
    private function checkLabdocAccess($mission, $id_labdoc, $user, $action)
    {
        // Can we act on the mission ?
        abort_if($user->cannot($action, $mission), 403);

        // Does the LD exists
        $labdoc = \App\Labdoc::find($id_labdoc);
        abort_if($labdoc == null, 404, __("Le Labdoc n'existe pas"));

        // Is the ld part of the mission ?
        abort_if($labdoc->reportPart->id_mission != $mission->id_mission, 409, __("Le Labdoc ne fait pas partie de la mission courante"));
        return $labdoc;
    }

    /**
     * Display a list of existing and potential reports for a student.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        $page = $request->path();
        \App\Teacher::enterInterface($user->id_user, $page);

        return view('teacher/missions/index')->with([
            'user' => $user,
        ]);
    }

    /**
     * Archives the mission
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mission $mission
     * @return \Illuminate\Http\Response
     */
    public function archive(Request $request, Mission $mission)
    {
        abort_if(Auth::user()->cannot('update', $mission), 403);

        $code = 200;
        if (!$mission->archive()) {
            $mission->fresh();
            $code = 500;
        }
        return response()->json($mission->status, $code);
    }

    /**
     * Creates a mission
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = Auth::user();
        abort_if($user->cannot('list', Mission::class), 403);

        $data = $request->validate([
            'code' => 'string',
            'name' => 'string',
        ]);

        $name = trim($data['name']);
        $code = trim($data['code']);

        if (mb_strlen($code) < 3 || mb_strlen($code) > 16) {
            return response()->json(__("Le code doit faire entre 3 et 16 caractères."), 400);
        }
        if (mb_strlen($name) < 3 || mb_strlen($name) > 64) {
            return response()->json(__("Le nom doit faire entre 3 et 64 caractères."), 400);
        }
        // verifie que la valeur de code n'existe pas deja
        if (\App\Mission::where('code', $code)->exists()) {
            return response()->json(__("Ce code est déjà attribué."), 409);
        }

        $mission = \App\Mission::create([
            'name' => $name,
            'code' => $code,
            Mission::CREATED_AT => date("Y-m-d"),
            Mission::UPDATED_AT => date("Y-m-d"),
        ]);
        $mission->linkTeacher($user->id_user, 'designer');
        Trace::logAction(Trace::TEACHER_ADD_MISSION, ['id_mission' => $mission->id_mission]);

        return response()->json($mission->id_mission);
    }

    /**
     * Deletes the mission
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mission $mission
     */
    public function delete(Request $request, Mission $mission)
    {
        $user = Auth::user();
        abort_if($user->cannot('update', $mission), 403);
        $mission->delete();
    }
    
    /**
     * Unsubscribe the user from the mission
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mission $mission
     */
    public function unsubscribe(Request $request, Mission $mission)
    {
        $user = Auth::user();
        abort_if($user->cannot('view', $mission), 403);
        if($mission->designers()->count() > 1) {
            $mission->teachers()->detach($user->id_user);
            return response()->json(['message' => 'success'], 200);
        } else {
            return response()->json(['message' => __('Au moins un manager doit être associé à cette mission')], 400);
        }
    }


    /**
     * Duplicates the mission
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mission $mission
     * @return \Illuminate\Http\JsonResponse
     */
    public function duplicate(Request $request, Mission $mission)
    {
        $user = Auth::user();
        abort_if($user->cannot('list', Mission::class), 403);

        $duplicate_mission = $mission->duplicate($user->id_user);
        $solution_report = $mission->getSolutionReport();
        // Duplicate solution report
        $duplicate_solution_report = $solution_report->duplicate($duplicate_mission, $user);
        return response()->json($duplicate_mission->id_mission);
    }

    /**
     * Creates a corrected report
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mission $mission
     * @return \Illuminate\Http\JsonResponse
     */
    public function correct(Request $request, Mission $mission)
    {
        $user = Auth::user();
        abort_if($user->cannot('correct', $mission), 403, __("Vous ne pouvez pas corriger cette mission"));
        $solution_report = $mission->getSolutionReport();
        return response()->json(['id_report'=>$solution_report->id_report]);
    }

    /**
     * Gets the public missions
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getPublic(Request $request)
    {
        $user = Auth::user();
        abort_if($user->cannot('list', Mission::class), 403);
        // No public mission for admins
        $data = $user->isAdmin() ? []: Mission::getPublic($user->id_user);
        return response()->json(['data' => $data ]);
    }

    /**
     * Get the user's missions
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getMine(Request $request)
    {
        $user = Auth::user();
        abort_if($user->cannot('list', Mission::class), 403);
        // No public mission for admins
        return response()->json(['data' => Mission::getTeacherMissions($user)]);
    }

    /**
     * Gets the user's missions for teamCopy
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getMineForTeamCopy(Request $request)
    {
        $data = $request->validate([
            'id_class' => 'int'
        ]);

        $user = Auth::user();
        abort_if($user->cannot('list', Mission::class), 403);

        $missions = $user->teacher
                         ->missions()
                         ->join('team_config', function ($join) use ($data) {
                             $join->on('team_config.id_mission', '=', 'mission.id_mission')
                                  ->where('team_config.id_class', '=', $data['id_class']);
                         })
                         ->groupBy('mission.id_mission')
                         ->orderBy('mission.code')
                         ->get();

        return response()->json($missions);
    }

    /**
     * Edits a mission
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mission $mission
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Mission $mission)
    {
        $user = Auth::user();
        abort_if($user->cannot('view', $mission), 403, __("Vous ne pouvez pas voir cette mission"));
        if ($user->cannot('update', $mission)) {
            // Do not throw a 403 here, it will interferre with middleware AddPermissionIfAllowedByAPI
            Helper::addAlert('danger', __('Vous ne pouvez pas modifier la mission car vous êtes simplement "tuteur" de cette mission sur LabNBook. Pour devenir "concepteur" demandez à un concepteur de la mission de vous attribuer ce statut.'));
            return redirect('/teacher/missions');
        }

        return view('teacher/missions/edit')->with([
            'current_mission' => $mission,
            'user' => $user,
        ]);
    }

    /**
     * Updates a given mission field
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mission $mission
     * @return \Illuminate\Http\Response
     */
    public function updateField(Request $request, Mission $mission)
    {
        abort_if(Auth::user()->cannot('update', $mission), 403);
        $data = $request->validate([
            'field' => 'string|required',
            'value' => 'string|nullable',
        ]);
        $value = $data['value'];
        $field = $data['field'];
        if ($field == 'code') {
            if (\App\Mission::where('code', $value)->exists()) {
                return response()->json(__("Ce code de mission est utilisé par une autre mission. Veuillez en choisir un nouveau."));
            }
        } elseif ($field === 'description' or $field === 'assignment') {
            $value = \App\HtmlFilter::clean($value);
        } elseif (!preg_match('/^\w+$/', $field)) {
                return response()->json(__("Champ incorrect"), 409);
        }
        $mission->$field = $value;
        $mission->traceUpdate();
    }

    /**
     * Handles the modification of links associated with a teacher
     * 
     * @param $mission
     * @param $user
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    private function teacherLinkModified($mission, $user) {
        $mission->traceUpdate();
        return view('teacher/missions/_linkedTeachers')->with([
            'teachers' => $mission->getLinkedTeachers(),
            'id_teacher' => $user->id_user,
            'mission' => $mission,
        ]);
    }

    /**
     * Links a teacher to a mission by name
     * displays nothing if the teacher's name is not found
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mission $mission
     * @return \Illuminate\Http\Response
     */
    public function linkTeacher(Request $request, Mission $mission)
    {
        $user = Auth::user();
        abort_if($user->cannot('update', $mission), 403);
        $data = $request->validate([
            'id_teacher' => 'int|required'
        ]);

        $teacher = \App\Teacher::find($data['id_teacher']);
        abort_if (!$teacher, 404);
        // TODO check that $teacher is linked in some way to $user;
        if (!$mission->teachers()->where('teacher.id_teacher', $teacher->id_teacher)->exists()) {
            if ($mission->linkTeacher($teacher->id_teacher, 'teacher')) {
                return $this->teacherLinkModified($mission, $user);
            }
        }
    }

    /**
     * Links a teacher to a mission by ID
     * displays nothing if the teacher's name is not found
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mission $mission
     * @return \Illuminate\Http\Response
     */
    public function linkTeacherTeamFromId(Request $request, Mission $mission)
    {
        $user = Auth::user();
        abort_if($user->cannot('update', $mission), 403);

        $data = $request->validate([
            'id_teacher_team' => 'int|required'
        ]);

        $tteam = \App\TeacherTeam::find($data["id_teacher_team"]);
        abort_if($user->cannot('view', $tteam), 403);

        $mission->linkTeacherTeam($tteam->id_teacher_team);

        return $this->teacherLinkModified($mission, $user);
    }

    /**
     * Updates associated teacher type
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mission $mission
     * @return \Illuminate\Http\Response
     */
    public function updateLinkedTeacher(Request $request, Mission $mission)
    {
        $user = Auth::user();
        abort_if($user->cannot('update', $mission), 403);

        $data = $request->validate([
            'id_teacher' => 'int|required',
            'teacher_type' => 'string|required',
        ]);
        if ($mission->countActualDesigners() <= 1 && $data['teacher_type'] != 'designer') {
            abort(409, __('Vous ne pouvez pas supprimer le dernier concepteur'));
        }

        if ($mission->linkTeacher($data['id_teacher'], $data['teacher_type'], 'update')) {
            return $this->teacherLinkModified($mission, $user);
        }
    }

    /**
     * Removes the association of a teacher with the given mission
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mission $mission
     * @return \Illuminate\Http\Response
     */
    public function removeLinkedTeacher(Request $request, Mission $mission)
    {
        $user = Auth::user();
        abort_if($user->cannot('update', $mission), 403);

        $id_teacher = (int)$request->input('id_teacher');

        // Checks that ther is still at list 1 designer other than admin
        if ($mission->countActualDesigners() == 1 && $mission->designers[0]->id_teacher == $id_teacher) {
            abort(409, __('Vous ne pouvez pas supprimer le dernier concepteur'));
        }
        // Do unlink
        if ($mission->linkTeacher($id_teacher, '', 'delete')) {
            return $this->teacherLinkModified($mission, $user);
        }
    }

    /**
     * Updates all position fields in the target table
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mission $mission
     * @return \Illuminate\Http\Response
     */
    public function updateAllItemPos(Request $request, Mission $mission)
    {
        $user = Auth::user();
        abort_if($user->cannot('update', $mission), 403);

        $data = $request->validate([
            'table' => 'in:ressource,report_part,labdoc|required',
            'items_array' => 'array|required',
            'items_array.*.id' => 'int',
            'items_array.*.idRp' => 'int',
            'items_array.*.pos' => 'int',
        ]);
        foreach ($data['items_array'] as $item) {
            if(key_exists('idRp',$item)) {
                $mission->updateLDPos($item['id'], 'id_report_part', $item['idRp']);
            }
            $mission->setItemPos($data['table'], $item['pos'], $item['id']);
        }
        $mission->traceUpdate();
    }

    /**
     * Updates the position field of the target table
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mission $mission
     * @return \Illuminate\Http\Response
     */
    public function setItemPos(Request $request, Mission $mission)
    {
        $user = Auth::user();
        abort_if($user->cannot('update', $mission), 403);

        $data = $request->validate([
            'table' => 'in:ressource,report_part,labdoc|required',
            'id1' => 'int|required',
            'pos1' => 'int|required',
            'id2' => 'int|nullable',
            'pos2' => 'int|nullable',
        ]);
        $mission->setItemPos($data['table'], $data['pos1'], $data['id1']);
        if ($data['id2'] != null) {
            $mission->setItemPos($data['table'], $data['pos2'], $data['id2']);
        }
        $mission->traceUpdate();
    }

    /**
     * Adds a web link resource to the mission
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mission $mission
     * @return \Illuminate\Http\Response
     */
    public function addURLRD(Request $request, Mission $mission)
    {
        $user = Auth::user();
        abort_if($user->cannot('update', $mission), 403);
        $position = $mission->nextRDPos();

        $res_path = strip_tags($request->input('res_path'));
        $name = strip_tags($request->input('name'));
        $id_resource = \App\Resource::addDoc(null, $res_path, $name, "url", $mission->id_mission, $position);
        if ($id_resource) {
            $resource = \App\Resource::find($id_resource);
            Trace::logAction(Trace::TEACHER_ADD_RESOURCE, ['id_mission' => $mission->id_mission, 'url' => $res_path]);
            $mission->traceUpdate();
            return view('teacher/missions/_RDLine')->with([
                'ressource' => $resource,
                'path' => "",
                'ressource_type' =>  'link'
            ]);
        }
    }

    /**
     * Updates a report part
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mission $mission
     * @return \Illuminate\Http\Response
     */
    public function editRP(Request $request, Mission $mission)
    {
        $data = $request->validate([
            'title' => 'string|nullable',
            'text' => 'int|required',
            'drawing' => 'int|required',
            'dataset' => 'int|required',
            'procedure' => 'int|required',
            'code' => 'int|required',
            'id_rp' => 'int|nullable',
            'assignment' => 'string|nullable',
        ]);
        $data['title'] = strip_tags($data['title']);


        $user = Auth::user();
        abort_if($user->cannot('update', $mission), 403);

        $id_rp = $data['id_rp'];
        unset($data['id_rp']);

        if ($id_rp > 0) { // modif
            $rp = \App\ReportPart::find($id_rp);
            if ($mission->id_mission != $rp->id_mission) {
                abort(409, __("La partie de rapport n'appartient pas à la mission courante"));
            }
            $rp->fill($data);
            $rp->save();
            Trace::logAction(Trace::TEACHER_MODIFY_REPORTPART, ['id_mission' => $mission->id_mission, 'id_rp' => $id_rp]);
            $position = $rp->position;
            $mission->traceUpdate();
        } else { // ajout
            $data['id_mission'] = $mission->id_mission;
            $data['position'] = $mission->nextRPPos();
            $rp = \App\ReportPart::create($data);
            Trace::logAction(Trace::TEACHER_ADD_REPORTPART, ['id_mission' => $mission->id_mission, 'id_rp' => $id_rp]);
            $rp->text = (int)$rp->text;
            $rp->drawing = (int)$rp->drawing;
            $rp->dataset = (int)$rp->dataset;
            $rp->code = (int)$rp->code;
            $rp->procedure = (int)$rp->procedure;
            $mission->traceUpdate();
            return view('teacher/missions/_RP', ['rp' => $rp]);
        }
    }

    /**
     * Deletes a report part
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mission $mission
     * @return \Illuminate\Http\Response
     */
    public function removeRP(Request $request, Mission $mission)
    {
        $data = $request->validate([
            'id_rp' => 'int|required',
        ]);

        $user = Auth::user();
        abort_if($user->cannot('update', $mission), 403);
        $mission->deleteRP($data['id_rp']);
        $mission->traceUpdate();
    }

    /**
     * Adds a labdoc
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mission $mission
     * @return \Illuminate\Http\Response
     */
    public function validateAddLD(Request $request, Mission $mission)
    {
        $data = $request->validate([
            'id_rp' => 'int|required',
            'type_labdoc' => 'string',
            'name' => 'nullable|string',
            'position' => 'string',
        ]);

        $user = Auth::user();
        abort_if($user->cannot('update', $mission), 403);

        $ld = $mission->addLD($data['id_rp'], $data['type_labdoc'], $data['name'], $data['position'], $user->id_user);
        if ($ld) {
            Trace::logAction(Trace::TEACHER_ADD_LABDOC, ['id_mission' => $mission->id_mission, 'id_labdoc' => $ld->id_labdoc, 'type' => $data['type_labdoc']]);
            $mission->traceUpdate();
            $ld->makeHidden('labdoc_data'); // Return only headers
            $ld->is_new_ld = 1;
            return response()->JSON(['ld' => $ld]);
        }
    }
    
    public function refreshLDView(Request $request, Mission $mission)
    {
        $data = $request->validate(['id_ld' => 'int|required']);
        $user = Auth::user();
        abort_if($user->cannot('update', $mission), 403);
        return view('teacher/missions/_LD')->with(['ld' => Labdoc::find($data['id_ld']), 'is_new_ld' => 0]);
    }

    /**
     * Termine l'edition d'un labdoc
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mission $mission
     * @return \Illuminate\Http\Response
     */
    public function validateEditLD(Request $request, Mission $mission)
    {
        //validateEditLD((int) $_POST["id_labdoc"], strip_tags($_POST["name"]), (int) $_POST["editable"], (int) $_POST["editable_name"], (int) $_POST["duplicatable"], (int) $_POST["deleteable"], (int) $_POST["draggable"], (int) $_POST["last_editor"], $_POST["labdoc_data"], (int) $_POST["id_mission"]);
        $data = $request->validate([
            'id_labdoc' => 'int|required',
            'name' => 'string|nullable',
            'editable' => 'int',
            'editable_name' => 'int',
            'duplicatable' => 'int',
            'deleteable' => 'int',
            'draggable' => 'int',
            'shared' => 'int',
            'last_editor' => 'int',
            'labdoc_data' => 'string|nullable',
            'assignment' => 'string|nullable',
        ]);

        $data['name'] = strip_tags($data['name']);
        $id_labdoc = $data['id_labdoc'];

        $user = Auth::user();
        $ld = $this->checkLabdocAccess($mission, $data['id_labdoc'], $user, 'update');
        unset($data['id_labdoc']);
        
        // Simulation case
        if (isset($ld->report)) {
            $mission = $ld->report->mission;
        } else {
            $mission = $ld->mission;
        }

        if ($data['shared']) {
            $data['deleteable'] = 0;
            $data['draggable'] = 0;
            $data['editable_name'] = 0;
            $ld->shared = 1; // For set before update for spreadToReports
        } else {
            $ld->shared = 0; // For set before update for spreadToReports
        }

        $ld->spreadToReports();

        return response()->json(['nb_modified_reports' => $mission->updateLD($data, $ld, $user->id_user)]);
    }

    /**
     * duplique un labdoc
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mission $mission
     * @return \Illuminate\Http\Response
     */
    public function duplicateLD(Request $request, Mission $mission)
    {
        $data = $request->validate([
            'id_labdoc' => 'int|required',
            'id_rp' => 'int|required',
        ]);

        $id_rp = $data['id_rp'];
        $user = Auth::user();
        abort_if($user->cannot('update', $mission), 403);
        abort_if(!$mission->reportParts()->where('id_report_part', $id_rp)->exists(), 403);

        $orig_ld = \App\Labdoc::find($data['id_labdoc']);
        abort_if($orig_ld === null, 404, __("Le Labdoc n'existe pas"));
        $type = $orig_ld->shared ? 'team_config' : 'report';
        $position = $orig_ld->position === null ? null : \App\Labdoc::maxPos($id_rp, 0, $type) + 1;

        $new_ld = \App\Labdoc::create([
            'id_report_part' => $id_rp,
            'type_labdoc' => $orig_ld->type_labdoc,
            'name' => $orig_ld->name." (copie)",
            'position' => $position,
            'editable' => $orig_ld->editable,
            'editable_name' => $orig_ld->editable_name,
            'duplicatable' => $orig_ld->duplicatable,
            'deleteable' => $orig_ld->deleteable,
            'draggable' => $orig_ld->draggable,
            'locked' => time(),
            'last_editor' => $user->id_user,
            'labdoc_data' => $orig_ld->labdoc_data,
            'shared' => $orig_ld->shared,
        ]);
        $mission->traceUpdate();
        $new_ld->shared_in_db = $orig_ld->shared;
        $new_ld->is_new_ld = 1;
        return response()->json($new_ld);
    }

    /**
     * supprime un labdoc
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mission $mission
     * @return \Illuminate\Http\Response
     */
    public function removeLD(Request $request, Mission $mission)
    {
        $user = Auth::user();
        $data = $request->validate([
            'id_labdoc' => 'int|required',
        ]);
        $labdoc = \App\Labdoc::find($data['id_labdoc']);
        abort_if($user->cannot('delete', $labdoc), Helper::pullPolicyErrorCode(), Helper::pullPolicyErrorMessage());

        return response()->json($mission->deleteLD($labdoc, $user->id_user));
    }

    /**
     * Retourne le contenu d un Labdoc XML
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mission $mission
     * @return \Illuminate\Http\Response
     */
    public function getLDXMLCode(Request $request, Mission $mission)
    {
        $id_labdoc = (int) $request->input('id_labdoc');

        $user = Auth::user();
        $labdoc = $this->checkLabdocAccess($mission, $id_labdoc, $user, 'view');
        if (@simplexml_load_string($labdoc->labdoc_data)){ // XML valide
            return response($labdoc->labdoc_data);
        }
       return response()->json(false);
    }

    /**
     * Met à jour le contenu d un Labdoc XML
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mission $mission
     * @return \Illuminate\Http\Response
     */
    public function updateLDXMLCode(Request $request, Mission $mission)
    {
        $data = $request->validate([
            'id_labdoc' => 'int|required',
            'labdoc_data' => 'string|required',
        ]);

        $user = Auth::user();
        $labdoc = $this->checkLabdocAccess($mission, $data['id_labdoc'], $user, 'view');

        $labdoc_data = $data['labdoc_data'];
        libxml_use_internal_errors(true);
        $labdoc_data_valid = simplexml_load_string($labdoc_data);
        if (!$labdoc_data_valid) {
            $message = "Failed loading XML\n";
            foreach (libxml_get_errors() as $error) {
                $message .= "\t". $error->message;
            }
            return response()->json($message, 409);
        } else {
            $labdoc->labdoc_data = $labdoc_data;
            if ($labdoc->save()) {
                $mission->traceUpdate();
            }
        }
    }

    /**
     *
     * Création d'un report et d'une équipe pour tester une mission ;
     * suppression de l'équipe existant déjà si la mission a été testée
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mission $mission
     * @return \Illuminate\Http\Response
     */
    public function createTestReport(Request $request, Mission $mission)
    {
        $user = Auth::user();
        abort_if($user->cannot('view', $mission), 403);

        $team_name = $user->id_user . '_' . $mission->id_mission . '_test';
        $report = \App\Report::create([
            'id_mission' => $mission->id_mission,
            'team_name' => $team_name,
            'status' => 'test',
        ]);
        $report->users()->attach($user);
        return response()->json($report->id_report);
    }

    /**
     *
     * Export de labdocs
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mission $mission
     * @return \Illuminate\Http\Response
     */
    public function exportLabdocs(Request $request)
    {
        $user = auth()->user();
        $teacher = $user->teacher;
        $id_mission = (int) $request->input('mission');
        $id_labdoc = (int) $request->input('labdoc');

        $missions = $teacher->missions()->orderBy('name')->get();

        if ($id_mission) {
            abort_if(!$missions->contains($id_mission), 403);
            $mission = $missions->only($id_mission)[0];
            $teacherLabdocs = ExportLabdocs::getTeacherLabdocs($mission->id_mission, $user->id_user);
        } else {
            $mission = null;
            $teacherLabdocs = Collect(); #empty collection
        }

        if ($id_labdoc && $teacherLabdocs->contains($id_labdoc)) {
            $selectedLabdoc = $teacherLabdocs->where('id_labdoc', $id_labdoc)->first();
            $studentLabdocs = ExportLabdocs::getStudentLabdocs($selectedLabdoc->id_labdoc);
        } else {
            $selectedLabdoc = null;
            $studentLabdocs = [];
        }

        $zipfile = $studentLabdocs ? ExportLabdocs::zipLabdocs($studentLabdocs) : null;

        if ($zipfile && file_exists($zipfile)) {
            return response()->stream(function () use ($zipfile, $mission, $selectedLabdoc) {
                readfile($zipfile);
            },200, [
                "Content-Type" => "application/zip", 
                "Content-Transfer-Encoding" => "binary",
                "Content-Disposition" => "attachment; filename=\"labnbook_labdoc-dataset_mission-{$mission->code}_{$selectedLabdoc->id_labdoc}.zip" 
            ]);
        } else {
            return view('teacher/missions/exportLabdocs')->with([
                'missions' => $missions,
                'mission' => $mission,
                'teacherLabdocs' => $teacherLabdocs,
            ]);
        }

    }


    /**
     * Returns the html to import labdocs in the mission given report part
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mission $mission
     * @param  \App\ReportPart $reportPart
     * @return \Illuminate\Http\Response
     */
    public function getImportLDHTML(Request $request, Mission $mission, \App\ReportPart $reportPart)
    {
        $user = Auth::user();
        abort_if($user->cannot('update', $mission), 403);
        $lds = \App\Labdoc::fetchLDToImport($user->id_user, '', $reportPart->id_report_part, '', 1, 1, 1, 1, 1, 'teacher');
        return view('teacher/missions/_LDsToImport')->with(['lds' => $lds, 'rp' => $reportPart])->render();
    }
}
