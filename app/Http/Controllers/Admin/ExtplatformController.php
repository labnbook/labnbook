<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use \App\Extplatform;

class ExtplatformController
{
    /**
     * Returns the list of extplatform for data-table
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $extplatforms = Extplatform::get();
        $out = [];
        $api_url = url('api/');
        foreach ($extplatforms as $e) {
            $out [] = [
                'name' => $e->name,
                'id' => $e->id_extplatform,
                'url' => $e->url,
                'inst' => $e->institution ? $e->institution->name : null,
                'instId' => $e->id_inst,
                'plugin_version' => $e->plugin_version,
                'platform_version' => $e->platform_version,
                'contacts' => $e->contacts,
                'secret' => $e->secret,
                'description' => view('admin._extplatform', ['extplatform' => $e, 'api_url' => $api_url])->render()
            ];
        }
        return response()->json(['data' => $out]);
    }
    
    /**
     * Creates an extplatform
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $data = $request->validate([
            'name' => 'string|required',
            'url' => 'string|required',
            'id_inst' => 'int|required',
            'contacts' => 'string|required'
        ]);

        $extp = new Extplatform();
        $extp->fill($data);
        $extp->secret = Str::random(64);
        if (!$extp->save()) {
            return response()->json('Unable to save extplatform, duplicate name or url ?', 409);
        }

        return response()->json($extp->id_extplatform);
    }

    /**
     * updates an extplatform
     * @param  \illuminate\http\request  $request
     * @param \App\Extplatform $extplatform
     * @return \illuminate\http\response
     */
    public function update(request $request, Extplatform $extplatform)
    {

        $data = $request->validate([
            'name' => 'string|required',
            'url' => 'string|required',
            'id_inst' => 'int|required',
            'secret' => 'string|required',
            'contacts' => 'string|required'
        ]);

        $extplatform->fill($data);
        try {
            $extplatform->save();
        } catch (\Exception $e) {
            return response()->json('Unable to save extplatform, duplicate name or url ?', 409);
        }

        return response()->json($extplatform->id_extplatform);
    }

    /**
     * deletes an extplatform
     * @param  \illuminate\http\request  $request
     * @param \App\Institution $institution
     * @return \illuminate\http\response
     */
    public function delete(request $request, Extplatform $extplatform)
    {
        return response()->json($extplatform->delete());
    }

}
