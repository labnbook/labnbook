<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use \App\Institution;
use \App\Classe;

class InstitutionController
{
    /**
     * Returns the list of institution for data-table
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();

        $institutions = Institution::get();
        $out = [];
        foreach ($institutions as $inst) {
            $out [] = [
                'name' => $inst->name,
                'id' => $inst->id_inst,
                'classe' => $inst->teacherClasse ? $inst->teacherClasse->class_name : null,
                'id_class_teacher' => $inst->id_class_teacher,
                'cas' => $inst->cas,
                'description' => __("Id institution : :id_inst,  id classe pour l'inscription des enseignants : :id_classe", ['id_inst' => $inst->id_inst, 'id_classe' => $inst->id_class_teacher])
            ];
        }
        return response()->json(['data' => $out]);
    }

    /**
     * Creates an instituion with a teacher class associated
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = Auth::user();
        $data = $request->validate([
            'name' => 'string|required',
            'cas' => 'string|required'
        ]);
        
        $inst = Institution::createInstWithTeacherClass($data['name'], $data['cas'] === 'true', $user->teacher);
        if (!$inst) {
            return response()->json('Unable to save institution, duplicate name ?', 409);
        }

        return response()->json($inst->id_inst);
    }

    /**
     * Get the classes for a given institution
     * @param  \illuminate\http\request  $request
     * @param \App\Institution $institution
     * @return \illuminate\http\response
     */
    public function getClasses(request $request, Institution $institution)
    {
        $user = auth::user();

        $classes = Classe::where('id_inst', $institution->id_inst)->get();

        $out = [];

        foreach ($classes as $cl) {
            $out[$cl->id_class] = $cl->class_name;
        }

        return response()->json($out);
    }

    /**
     * updates an instituion
     * @param  \illuminate\http\request  $request
     * @param \App\Institution $institution
     * @return \illuminate\http\response
     */
    public function update(request $request, Institution $institution)
    {
        $user = auth::user();

        $data = $request->validate([
            'name' => 'string|required',
            'cas' => 'int|required',
            'classe' => 'int|nullable'
        ]);

        $institution->name = $data['name'];
        $institution->cas = $data['cas'];
        $institution->id_class_teacher = $data['classe'];
        $msg = $data['classe'] ? null : __("Il est nécessaire d'associer une classe à l'institution pour l'ajout d'enseignants");

        return response()->json(["msg" => $msg, "status" => $institution->save()]);
    }

    /**
     * deletes an instituion
     * @param  \illuminate\http\request  $request
     * @param \App\Institution $institution
     * @return \illuminate\http\response
     */
    public function delete(request $request, Institution $institution)
    {
        $user = auth::user();

        if ($institution->countUsers() > 0) {
            return response()->json(["msg" => __("Impossible de supprimer une institution contenant des utilisateurs")], 409);
        }

        return response()->json($institution->delete());
    }
}
