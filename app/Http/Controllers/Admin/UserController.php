<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

use \App\User;

class UserController
{
    /**
     * Returns the list of users in institution for data-table
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = $request->validate([
            'id_inst' => 'int|required',
        ]);
        $users = \App\User::where('id_inst', $data['id_inst'])->get();
        $out = [];
        foreach ($users as $u) {
            $role = $u->getDetailedRole();
            $out [] = [
                'id' => $u->id_user,
                'user_name' => $u->user_name,
                'first_name' => $u->first_name,
                'email' => $u->email,
                'login' => $u->login,
                'roleName' => $u->roleName($role),
                'role' => $role,
                'login_type' => $u->isInternal() ? config('app.name') : __("Institutionel (:cas_name)", ["cas_name" => config('labnbook.cas_name')])
            ];
        }
        return response()->json(['data' => $out]);
    }

    /**
     * Get user description for dataTable
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function getDescr(Request $request, User $user)
    {
        $teacherMissions = [];
        $teacherClasses = [];
        $classes = [];
        $reports = [];
        if ($user->isTeacher()) {
            foreach ($user->teacher->missions as $m) {
                $role = $m->designers->contains($user->teacher) ? __('Concepteur') : __('Tuteur');
                $teacherMissions[] = [
                    'id' => $m->id_mission,
                    __('nom') => $m->name,
                    __('état') => $m->statusName($m->status),
                    __('role') => $role,
                ];
            }
            foreach ($user->teacher->classes as $c) {
                $tcs = "";
                foreach ($c->teamConfigs as $t) {
                    $tcs .= $t->infos().", ";
                }
                $teacherClasses[] = [
                    'id' => $c->id_class,
                    __('nom') => $c->class_name,
                    __('état') => __($c->status),
                    __('mises en équipes') => trim($tcs, ", "),
                ];
            }
        }

        foreach ($user->classes as $c) {
            $tcs = "";
            foreach ($c->teamConfigs as $t) {
                $tcs .= $t->infos().", ";
            }
            $classes[] = [
                'id' => $c->id_class,
                __('nom') => $c->class_name,
                __('état') => __($c->status),
                __('mises en équipes') => $tcs,
            ];
        }
        foreach ($user->reports()->withTrashed()->get() as $r) {
            $reports[] = [
                'id' => $r->id_report,
                __('état') => $r->delete_time ? __('supprimé') : __($r->statusName($r->status)),
                __('mission') => $r->id_mission." (".$r->mission->code.")",
                __('état de la mission') => __($r->mission->statusName($r->mission->status))
            ];
        }

        return view(
            'admin/_userDescription',
            [
                'teacherMissions' => $teacherMissions,
                'teacherClasses' => $teacherClasses,
                'classes' => $classes,
                'reports' => $reports,
            ]
        );
    }

    /**
     * updates a user
     * @param  \illuminate\http\request  $request
     * @param \App\User $user
     * @return \illuminate\http\response
     */
    public function update(request $request, User $user)
    {
        $data = $request->validate([
            'first_name' => 'string|required',
            'user_name' => 'string|required',
            'role' => 'string|required',
            'email' => 'string|nullable',
            'login' => 'string|required',
        ]);
        $msg = "";
        if ($user->getDetailedRole() != $data['role']) {
            if ($data['role'] === 'learner') {
                // User was not a learner so they had a teacher
                $user->teacher->delete();
            } else {
                if (!$user->isTeacher()) {
                    $user->grantTeacher([$user->id_inst], "Teacher right given by admin");
                }
                $user->teacher->changeRole($data['role']);
            }
        }

        foreach (['first_name', 'user_name', 'email', 'login'] as $field) {
            $user->$field = $data[$field];
        }

        return response()->json(["msg" => $msg, "status" => $user->save()]);
    }

    /**
     * impersonnate a user
     * @param  \illuminate\http\request  $request
     * @param \App\User $user
     * @return \illuminate\http\response
     */
    public function impersonate(request $request, User $user)
    {
        Log::Debug("Starting impersonnation of user ".$user->id_user);
        Auth::user()->impersonate($user);
        Log::Debug("Impersonnating user ".$user->id_user);
        Log::Debug("User is now " . auth()->id());
        \App\Helper::AddAlert("warning", __("Usurpation de l'identité de :login en cours", ['login' => $user->login]));
    }

    /**
     * deletes a user
     * @param  \illuminate\http\request  $request
     * @param \App\User $user
     * @return \illuminate\http\response
     */
    public function delete(request $request, User $user)
    {
        return response()->json($user->delete());
    }

    /**
     * Anonymize a user
     * @param  \illuminate\http\request  $request
     * @param \App\User $user
     * @return \illuminate\http\response
     */
    public function anonymize(request $request, User $user)
    {
        return response()->json($user->anonymize());
    }

    /**
     * Get the RGPD dump for a user
     * @param  \illuminate\http\request  $request
     * @param \App\User $user
     * @return \illuminate\http\response
     */
    public function GDPRDump(request $request, User $user)
    {
        $gdpr = new \App\Processes\GDPR($user, 'zip');
        $zipfile = $gdpr->generateDump();
        if ($zipfile && file_exists($zipfile)) {
            return response()->stream(function () use ($zipfile, $user) {
                readfile($zipfile);
            }, 200, [
                "Content-Type" => "application/zip",
                "Content-Transfer-Encoding" => "binary",
                "Content-Disposition" => "attachment; filename=\"rgpd_dump_{$user->id_user}.zip"
            ]);
        }
        abort(500, __("Impossible de générer le dump RGPD"));
    }
}
