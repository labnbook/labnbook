<?php

namespace App\Http\Controllers;

use \App\Helper;
use App\Report;
use \App\Resource;
use \App\Trace;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ResourceController extends Controller
{
    /**
     * Update the contents of report's resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateLearnerDocs(Request $request)
    {
        $user = Auth::user();
        $report = \App\Report::find((int)$request->input('id_report'));
        abort_if($user->cannot('view', $report), Helper::pullPolicyErrorCode());
        $displayNewDoc = $report->displayNewRessources($user);
        return view('components.widgets.resource._learnerDocs')->with(['resources' => $report->resources,'displayNewDoc'=>$displayNewDoc]);
    }

    /**
     * Add an Url to the report's resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addUrl(Request $request)
    {
        $user = Auth::user();
        $report = \App\Report::find((int)$request->input('id_report'));
        abort_if($user->cannot('manageOrUpdate', $report), Helper::pullPolicyErrorCode());
        $url = $request->input('url');
        $name = $request->input('name');
        $id_ressource = Resource::addDoc($report->id_report, $url, $name, 'url');
        Resource::createOrUpdateLinkToUser($id_ressource, $user->id_user);
        Trace::logAction(Trace::ADD_RESOURCE, ['id_report' => $report->id_report, 'url' => $url]);
        return response()->json();
    }

    /**
     * Deletes a document to the report's resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Resource  $resource
     * @return \Illuminate\Http\Response
     */
    public function delDoc(Request $request, Resource $resource)
    {
        $user = Auth::user();
        abort_if($user->cannot('update', $resource->report), Helper::pullPolicyErrorCode());
        $resource->delete();
        return response()->json();
    }

    /**
     * Rename a document to the report's resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Resource  $resource
     * @return \Illuminate\Http\Response
     */
    public function renameDoc(Request $request, Resource $resource)
    {
        $user = Auth::user();
        abort_if($user->cannot('update', $resource->report), Helper::pullPolicyErrorCode());
        $resource->name = $request->input('name');
        $resource->save();
        return response()->json();
    }

    /**
     * Deletes a document from the mission's resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Resource  $resource
     * @return \Illuminate\Http\Response
     */
    public function removeFromMission(Request $request, Resource $resource)
    {
        $user = Auth::user();
        $mission = $resource->mission;
        abort_if($user->cannot('update', $mission), Helper::pullPolicyErrorCode());

        $position = $resource->position;
        $type = $resource->type;

        if ($resource->delete()) {
            $mission->traceUpdate();
        }
        if ($type != 'assignment') {
            $mission->updateResourcesPosition((int)$position);
        }
    }

    /**
     * Update's a missions document
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Resource  $resource
     * @return \Illuminate\Http\Response
     */
    public function updateFromMission(Request $request, Resource $resource)
    {
        $user = Auth::user();
        $mission = $resource->mission;
        abort_if($user->cannot('update', $mission), Helper::pullPolicyErrorCode());
        $data = $request->validate([
            'name' => 'string|required',
            'url' => 'string|nullable',
        ]);

        $resource->name = strip_tags($data['name']);
        if ($resource->res_type === Resource::RES_TYPE_URL) {
            abort_if(!isset($data['url']), 409, __('Le champ "Adresse" doit être renseigné'));
            $resource->res_path = filter_var($data['url'], FILTER_VALIDATE_URL);
            abort_if(!$resource->res_path, 409, __('Le champ "Adresse" doit être renseigné une url  valide'));
        }
        Log::Debug($resource);

        if ($resource->save()) {
            $mission->traceUpdate();
        }
        return response()->json($resource);
    }
    /**
     * Control data type and user access
     * @param Request $request
     * @return void
     */
    public function updateOrInsertLinkResourceUser(Request $request, Resource $resource): void
    {
        $user = Auth::user();
        if ($resource->report) {
            abort_if($user->cannot('view', $resource->report), Helper::pullPolicyErrorCode());
        } else {
            if ($user->cannot('view', $resource->mission)) {
                abort_if(!$resource->mission->hasReportForUser($user), 403);
            }
        }
        Resource::createOrUpdateLinkToUser($resource->id_ressource, $user->id_user);
    }
}
