<?php

namespace App\Http\Controllers;

use App\Trace;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TrashController extends Controller
{
    const NUMTRASHDAYS = 90;
    /**
     * Permanently deletes a labdoc.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function deleteLabdoc(Request $request)
    {
        $labdoc =  \App\Labdoc::find($request->input('id_labdoc'));
        if ($labdoc) {
            $parameters = ['id_labdoc' => $labdoc->id_labdoc, 
                'id_mission' => $labdoc->mission ? $labdoc->mission->id_mission : null, 
                'id_report' => $labdoc->id_report];
            \App\Labdoc::destroy($labdoc->id_labdoc);
        
            Trace::logAction(Trace::HARD_DELETE_LD,  $parameters);
        }
        
        return $this->listDeletedLabdoc($request);
    }

    /**
     * Permanently deletes a labdoc.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function restoreLabdoc(Request $request)
    {
        $data = $request->validate([
            'id_labdoc' => 'required|int',
            'id_report_part' => 'required|int',
            'id_report' => 'required|int',
        ]);
        $ld = \App\Labdoc::find($data['id_labdoc']);
        $ld->restore(Auth::id(), $data['id_report_part']);
    }

    /**
     * Permanently deletes a labdoc.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function listDeletedLabdoc(Request $request)
    {
        $deletedLds =  DB::select(
            DB::raw(
                "SELECT ld.id_report_part, ld.id_labdoc, ld.type_labdoc, ld.name, rp.title, rp.position, "
                ."UNIX_TIMESTAMP(deleted) as deleted "
                ."FROM labdoc ld JOIN report_part rp USING (id_report_part) "
                ."WHERE id_report = ? AND DATEDIFF(NOW(), deleted) < ? "
                ."ORDER BY rp.position, ld.name"
            ),
            [(int) $request->input('id_report'), self::NUMTRASHDAYS]
        );
        return view('components.widgets.trash._deleted')->with([ 'id_report' => (int) $request->input('id_report'), 'deleted' => $deletedLds ]);
    }
}
