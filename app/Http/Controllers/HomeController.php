<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\User;

class HomeController extends Controller
{
    /**
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        Log::Debug("Home page requested for user ".$user->id_user);
        switch ($user->getRole()) {
            case 'admin':
            case 'teacher':
                $url = $user->teacher->getInterfaceUrl();
                break;
            case 'learner':
                $url = '/reports';
                break;
        }
        Log::Debug("Redirecting to ".session()->get('url.intended', $url));
        return redirect()->intended($url);
    }

    /**
     * Redirect to a web route after appending payload to the session.
     * See API\V1\RedirectController::index().
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function redirect(Request $request)
    {
        $rawPayload = $request->input('payload');
        if (empty($rawPayload)) {
            abort(402, "The 'payload' parameter is missing or empty.");
        }
        $payload = decrypt($rawPayload);

        // authenticate
        if (!empty($payload->authentication->id_user)) {
            // authenticate this user
            $user = User::find($payload->authentication->id_user);
            if (!$user) {
                abort(403, "Unknown user");
            }
            if (!auth()->check() || auth()->id() != $user->id_user) {
                auth()->login($user);
            }
        }

        // fill-in the user session
        foreach ($payload->session as $key => $value) {
            session()->put($key, $value);
        }
        session()->put('redirecting', true);

        return redirect($payload->path);
    }
}
