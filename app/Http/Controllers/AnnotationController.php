<?php

namespace App\Http\Controllers;

use \App\Helper;
use \App\Trace;
use \App\Annotation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AnnotationController extends Controller
{

    /**
     * Get annotations by report or reportPart.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function get(Request $request)
    {
        $user = Auth::user();
        if ($request->has('id_report')) {
            $query = Annotation::selectRaw("*, ".\App\User::selectInitialsSql())
                ->join('user', 'annotation.id_teacher', '=', 'user.id_user')
                ->where('id_report', (int)$request->input('id_report'));
        } else {
            $query = Annotation::where('id_report_part', (int)$request->input('id_report_part'))
                ->where('id_teacher', (int)$request->input('id_teacher'))
                ->whereNotNull('popularity')
                ->orderBy('popularity', 'DESC');
        }
        $annotations = $query->orderBy('annotation.update_time', 'DESC')->get();
 
        if ($annotations->isEmpty()) {
            return response()->json([]);
        }
        abort_if($user->cannot('view', $annotations->first()), Helper::pullPolicyErrorCode());
        return response()->json($annotations);
    }

    /**
     * Store an annotation.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'id_teacher' => 'int|required',
            'id_report' => 'int|required',
            'id_labdoc' => 'int|nullable',
            'id_report_part' => 'int|required',
            'json' => 'json|required',
            'type' => 'string|required',
        ]);
        $user = Auth::user();
        $report = \App\Report::find($data['id_report']);
        abort_if(!$user->isReportTeacher($report), 403);
        $annotation = new Annotation();
        $data = $data;
        $annotation->fill($data);
        $annotation->save();

        $report->notifyAnnotation(true);

        $parameters = array_map(
            'intval',
            $annotation->only('id_annotation', 'id_report', 'id_labdoc')
        );
        $parameters['id_mission'] = $annotation->report->id_mission;
        Trace::logAction(Trace::TEACHER_ANNOTATE_REPORT, $parameters);

        return response()->json($annotation->id_annotation);
    }

    /**
     * Update the contents of the annotation.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param \App\Annotation $annotation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Annotation $annotation)
    {
        $data = $request->validate([
            'json' => 'json|required',
        ]);
        $user = Auth::user();
        abort_if($user->cannot('update', $annotation), Helper::pullPolicyErrorCode());
        $annotation->json = $request->input('json');
        $annotation->save();

        $report = \App\Report::find($annotation->id_report);
        $report->notifyAnnotation(true);

        return response()->json(1);
    }

    /**
     * Delete the contents of the annotation.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param \App\Annotation $annotation
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, Annotation $annotation)
    {
        $user = Auth::user();
        abort_if($user->cannot('update', $annotation), Helper::pullPolicyErrorCode());
        $annotation->delete();

        $report = \App\Report::find($annotation->id_report);
        $report->notifyAnnotation(true);

        return response()->json(1);
    }

    /**
     * Trace the reading of the annotation.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param \App\Annotation $annotation
     * @return \Illuminate\Http\Response
     */
    public function trace(Request $request, Annotation $annotation)
    {
        $user = Auth::user();

        $parameters = array_map(
            'intval',
            $annotation->only('id_annotation', 'id_report', 'id_labdoc')
        );
        $parameters['id_mission'] = $annotation->report->id_mission;
        if ($request->has('duration')) {
            $parameters['duration'] = (int) $request->input('duration');
        }
        abort_if($user->cannot('view', $annotation), Helper::pullPolicyErrorCode());
        Trace::logAction(Trace::CHECK_ANNOTATION, $parameters);
        $annotation->saveReadingByUser($user->id_user);
        return response()->json([]);
    }

    /**
     * Mets à jour la popularité de l'annotation.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param \App\Annotation $annotation
     * @return \Illuminate\Http\Response
     */
    public function updatePopularity(Request $request, Annotation $annotation)
    {

        $data = $request->validate([
            'reset'        => 'boolean|required'
        ]);

        // Vérification si l'utilisateur peut modifier la popularité
        $user = Auth::user();
        abort_if($user->cannot('updatePopularity', $annotation), Helper::pullPolicyErrorCode());

        $annotation->updatePopularity($data['reset']);

        return response()->json($annotation);
    }
}
