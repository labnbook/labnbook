<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Trace;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CommentController extends Controller
{
    /**
     * Prints the HTML for all the comments of a LD.
     *
     * @param int $id_ld
     * @return int number of comments displayed
     */
    public function displayComments(Request $request)
    {
        $data = $request->validate([
            'id_ld' => 'int|required',
        ]);
        $labdoc = \App\Labdoc::find($data['id_ld']);
        abort_if($labdoc == null, 404);
        $user = Auth::user();
        abort_if($user->cannot('viewComments', $labdoc), 403);
        if ($labdoc->comments->count()) {
            Trace::logAction(
                Trace::OPEN_COMMENT,
                ['id_report' => $labdoc->id_report, 'id_labdoc' => $labdoc->id_labdoc]
            );
        }
        // mise a jour de la derniere fois où l'utilisateur a consulté le fil de commentaires
        $raw = DB::raw("UPDATE labdoc_status SET com_viewed = NOW()"
            ." WHERE id_labdoc = :id_labdoc AND id_user = :id_user");
        DB::update($raw, ['id_labdoc' => $labdoc->id_labdoc, 'id_user' => $user->id_user]);
        return view('components.widgets.comments._ldComments')->with([
                'labdoc' => $labdoc,
                'user' => $user,
            ]);
    }

    /*
     * Add a new comment and then display the list of comments
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addComment(Request $request)
    {
        $data = $request->validate([
            'id_ld' => 'int|required',
            'comment' => 'string|required',
        ]);
        $user = Auth::User();
        $labdoc = \App\Labdoc::find($data['id_ld']);
        abort_if($labdoc == null, 404);
        abort_if($user->cannot('addComment', $labdoc), 403);
        $content = $data['comment'];
        if ($content != "") {
            $comment = Comment::Create([
                'id_labdoc' => $labdoc->id_labdoc,
                'id_user' => $user->id_user,
                'content' => nl2br(strip_tags($content)),
            ]);
        }
        Trace::logAction(
            Trace::ADD_COMMENT,
            ['id_report' => $labdoc->id_report, 'id_labdoc' => $labdoc->id_labdoc]
        );
        return view('components.widgets.comments._ldComments')->with([
                'labdoc' => $labdoc,
                'user' => $user,
            ]);
    }

    /**
     * Delete a comment and then display the list of comments
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, Comment $comment)
    {
        $labdoc = $comment->labdoc;
        $user = Auth::user();
        abort_if($user->cannot('delete', $comment), 403);
        $comment->delete();
        return view('components.widgets.comments._ldComments')->with([
                'labdoc' => $labdoc,
                'user' => $user,
            ]);
    }
}
