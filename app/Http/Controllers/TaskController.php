<?php

namespace App\Http\Controllers;

use \App\Task;
use \App\Report;
use App\Trace;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;


class TaskController extends Controller
{

    /**
     * Returns all tasks assigned to the users id $id_report <= 0, all the
     * reports tasks else
     *
     * @param \App\User $user user for which we should retrieve tasks
     * @param int $id_report if > 0 returns the tasks of the id_report
     * @param boolean $allreports only if $id_report < 0, should we get task from
     * all reports instead of personnals
     *
     * @return \Illuminate\Database\Eloquent\Collection|\App\Task[]
     */
    private function _getUserTasks($user, $id_report)
    {
        if ((int)$id_report > 0) {
            $report = Report::find($id_report);
            abort_if(!$report, 404);
            abort_if(!$report->users->contains($user), 403);
            $query = $report->tasks();
        } else {
            $query = $user->tasks()
                          ->select(['task.*', DB::Raw('"Tâches personnelles" as report_name')])
                          ->union(
                              Task::join('link_report_learner', function($join) use ($user) {
                                  $join->where('link_report_learner.id_user', '=', $user->id_user)
                                       ->on('link_report_learner.id_report', '=', 'task.id_report');
                              })->join('report', 'report.id_report', 'task.id_report')
                                ->join('mission', 'mission.id_mission', 'report.id_mission')
                                ->join('link_task_user', 'task.id_task', '=', 'link_task_user.id_task')
                                ->where('link_task_user.id_user', '=', $user->id_user)
                                ->whereIn('report.status', [Report::ON, Report::NEW])
                                ->select(['task.*', 'mission.name as report_name'])
                                ->orderBy('id_report')
            );
        }
        $tasks = $query->orderBy('position')
            ->with('assignees')
            ->get();
        return ['tasks' => $tasks];
    }

    /**
     * Updates the list of assignees if set in data
     *
     * @param \App\Task $task task to update
     * @param array $data Data sent in request and validated
     */
    protected function updateAssignees($task, $data)
    {
        $changes = 0;
        if (isset($data['assignees'])) {
            $newAssignees = array_map(
                function ($a) {
                    return $a['value'];
                },
                $data['assignees']
            );
            // Remove unassigned
            foreach ($task->assignees as $a) {
                if (!in_array($a->id_user, $newAssignees)) {
                    $changes++;
                    $task->assignees()->detach($a);
                }
            }
            // Add assigned
            foreach ($newAssignees as $a) {
                if (!$task->assignees->contains($a)) {
                    $changes++;
                    $task->assignees()->attach($a);
                }
            }
        }
        if ($changes > 0) {
            Trace::logAction(Trace::TASK_ASSIGN,
                [
                    'id_task' => $task->id_task,
                    'assignees' => $task->assignees()->pluck('link_task_user.id_user'),
                ]
            );
        }
    }

    /**
     * Display a list of existing and potential tasks for a student.
     *
     * @param \Illuminate\Http\Request $request the http request
     *
     * @return \Illuminate\Http\Response json
     */
    public function index(Request $request)
    {
        $data = $request->validate(
            [
                'idu' => 'int|nullable',
                'idr' => 'int|nullable',
                'format' => 'json|vtodo',
                'key' => 'string|nullable', // Key for retrieving tasks without being logged in
            ]
        );
        if (!auth()->check()) {
            // TODO use $data['key'] and $data['idu']
            return response()->json('Not implemented', 501);
        }
        $user = auth()->user();
        if (!isset($data['idr'])) {
            $data['idr'] = -1;
        }
        $tasks = $this->_getUserTasks($user, $data['idr']);
        if (isset($data['format']) && $data['format'] == 'vtodo') {
            // TODO not implemented return VTODO format
            return response()->json('Not implemented', 501);
        }
        return response()->json($tasks);
    }

    /**
     * Trace an action on a task.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function trace(Request $request)
    {
        $trace_names = array_merge(
            Trace::getTracesByNamePrefix("TASK"),
            Trace::getTracesByNamePrefix("CALENDAR"),
        );
        $sort_fields = ['due_date', 'priority', 'estimated_time'];
        $sort_names = array_map(fn($val)  => $val . '_asc' . ',' . $val . '_desc', $sort_fields);
        $data = $request->validate(
            [
                'trace' => 'required|in:'.implode(',', array_keys($trace_names)),
                'id_task' => 'int|nullable',
                'id_parent_task' => 'int|nullable',
                'id_report' => 'int|nullable',
                'criteria' => 'nullable|in:'.implode(',', $sort_names),
                'reports' => 'array|nullable',
                'reports.*' => 'int',
                'filter' => 'string|nullable',
                'view' => 'string|nullable|in:dayGridMonth,listDay,listWeek',
            ]
        );
        $params = [];
        $KEYS = ['id_report', 'id_parent_task', 'criteria', 'reports', 'asignees', 'field', 'filter', 'view', 'id_task'];
        foreach ($KEYS as $key) {
            if (isset($data[$key])) {
                $params[$key] = $data[$key];
            }
        }
        Trace::logAction($trace_names[$data['trace']], $params);
        return response()->json('');
    }

    /**
     * Creates a task.
     *
     * @param \Illuminate\Http\Request $request the http request
     *
     * @return \Illuminate\Http\Response json
     */
    public function create(Request $request)
    {
        $data = $request->validate([
            'title' => 'string|required',
            'state' => 'in:' . Task::STATE_IN_PROGRESS . ',' . Task::STATE_DONE . '|required',
            'priority' => 'in:' . Task::PRIORITY_LOW . ',' . Task::PRIORITY_MEDIUM . ',' . Task::PRIORITY_HIGH . '|nullable',
            'estimated_duration' => 'int|nullable',
            'due_date' => 'date|nullable',
            'id_parent_task' => 'int|nullable',
            'position' => 'int|required',
            'id_report' => 'int|nullable',
            'assignees' => 'array|nullable',
            'assignees.*' => 'array|nullable',
            'assignees.*.value' => 'int|nullable',
            'hasSubtasksUnfolded' => 'boolean|nullable'
        ]);
        $id_report = isset($data['id_report']) ? $data['id_report'] : -1;
        $user = Auth::user();
        $data['id_user_creator'] = $user->id_user;
        $data['id_user_last_editor'] = $user->id_user;
        if ($id_report > 0) {
            $report = Report::find($id_report);
            abort_if(!$report || !$report->users->contains($user), 403);
        } else {
            $report = null;
        }
        $t = new Task();
        $t->fill($data);
        $t->save();
        Trace::logAction(Trace::TASK_CREATE,
            [
                'id_report' => $report ? (int)$report->id_report : null,
                'id_mission' => $report ? (int)$report->id_mission : null,
                'id_task' => $t->id_task,
            ]
        );
        if (isset($data['assignees']) && !empty($data['assignees'])) {
            $this->updateAssignees($t, $data);
        }

        return response()->json($this->_getUserTasks($user, $id_report));
    }

    /**
     * Updates a task.
     *
     * @param \Illuminate\Http\Request $request the http request
     * @param \App\Task $task the task to destroy
     *
     * @return \Illuminate\Http\Response json
     */
    public function update(Request $request, Task $task)
    {

        $data = $request->validate([
            'title' => 'string|nullable',
            'state' => 'in:' . Task::STATE_IN_PROGRESS . ',' . Task::STATE_DONE . '|nullable',
            'priority' => 'in:' . Task::PRIORITY_LOW . ',' . Task::PRIORITY_MEDIUM . ',' . Task::PRIORITY_HIGH . '|nullable',
            'estimated_duration' => 'int|nullable',
            'due_date' => 'date|nullable',
            'id_parent_task' => 'int|nullable',
            'position' => 'int|nullable',
            'id_report' => 'int|nullable',
            'assignees' => 'array|nullable',
            'assignees.*' => 'array|nullable',
            'assignees.*.value' => 'int|nullable',
            'hasSubtasksUnfolded' => 'boolean|nullable'
        ]);
        $user = Auth::user();
        abort_if($user->cannot('update', $task), 403);
        $data['id_user_last_editor'] = $user->id_user;

        // If the task is completed, trace the action
        if(isset($data['state']) && $data['state'] != $task->state && $data['state'] == Task::STATE_DONE) {
            Trace::logAction(Trace::TASK_COMPLETED,
                [
                    'id_report' => $task->id_report,
                    'id_mission' => $task->id_report ? (int)$task->report->id_mission : null,
                    'id_task' => $task->id_task,
                ]
            );
            $task->state = $data['state'];
            $task->save();
            // TODO mark completed
        }

        $task->fill($data);
        if ($task->isDirty()) {
            Trace::logAction(Trace::TASK_EDIT,
                [
                    'id_report' => $task->id_report,
                    'id_mission' => $task->id_report ? (int)$task->report->id_mission : null,
                    'id_task' => (int)$task->id_task,
                ]
            );
            $task->save();
        }
        $this->updateAssignees($task, $data);
        return response()->json($this->_getUserTasks($user, $task->id_report));
    }

    /**
     * Updates a set of tasks positions.
     *
     * @param \Illuminate\Http\Request $request the http request
     *
     * @return \Illuminate\Http\Response json
     */
    public function updatePositions(Request $request)
    {
        $data = $request->validate(
            [
                'tasks' => 'array|required',
                'tasks.*.id_task' => 'int|required',
                'tasks.*.position' => 'int|required',
                'tasks.*.id_parent_task' => 'int|nullable'
            ]
        );
        $user = auth()->user();
        $ids = array_map(fn($e) => $e['id_task'], $data['tasks']);
        // First pass : check perms
        $tasks = Task::find($ids);
        $id_report = null;
        foreach ($tasks as $t) {
            abort_if($user->cannot('update', $t), 403);
            if ($t->id_report) {
                $id_report = $t->id_report;
            }
        }
        foreach ($tasks as $t) {
            $dataTask = $data['tasks'][$t->id_task];
            $t->position = $dataTask['position'];

            if(isset($dataTask['id_parent_task']) && $dataTask['id_parent_task'] != $t->id_parent_task) {
                Trace::logAction(Trace::TASK_PAIRING,
                    [
                        'id_report' => $t->id_report,
                        'id_mission' => $t->id_report ? $t->report->id_mission : null,
                        'id_task' => $t->id_task,
                        'id_parent_task' => $dataTask['id_parent_task'],
                    ]
                );
            }
            $t->id_parent_task = $dataTask['id_parent_task'];
            if ($t->id_parent_task == $t->id_task) {
                // Avoid impossible case
                $t->id_parent_task = null;
            }
            $t->update();
        }
        Trace::logAction(Trace::TASK_MOVE,
            [
                'id_report' => $id_report,
                'id_mission' => $id_report ? (int)$tasks[0]->report->id_mission : null,
            ]
        );
        return $this->_getUserTasks($user, $id_report);
    }

    /**
     * Destroys a task.
     *
     * @param \Illuminate\Http\Request $request the http request
     * @param \App\Task $task the task to destroy
     *
     * @return \Illuminate\Http\Response json
     */
    public function destroy(Request $request, Task $task)
    {
        $user = auth()->user();
        abort_if($user->cannot('delete', $task), 403);
        $r = $task->report;
        $task->delete();
        $id_report = $r ? $r->id_report : null;
        $id_mission = $r ? $r->id_mission : null;
        Trace::logAction(Trace::TASK_DELETE,
            [
                'id_report' => $id_report,
                'id_mission' => $id_mission,
                'id_task' => $task->id_task,
            ]
        );
        return $this->_getUserTasks($user, $id_report);
    }
}
