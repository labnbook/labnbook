<?php

namespace App\Http\Controllers;

use App\Helper;
use App\Labdoc;
use App\Report;
use App\TeamConfig;
use App\Trace;
use App\Views\ReportEnter;
use App\Views\ReportIndex;
use App\Processes\LabdocDiff;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class ReportController extends Controller
{
    /**
     * Returns the reportView for thegiven couple request / report
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Report  $report
     * @param   string  $scope
     * @return \App\Views\Report
     */
    private function getReportView(Request $request, Report $report, $scope = null)
    {
        return new \App\Views\Report(
            $report,
            $scope == null ? $request->input('sc') : $scope,
            $this->extractLdChosen($request),
            $request->input('choice_ld'),
            (bool) $request->input('print_empty_rp', false),
            (bool) $request->input('print_assignment', false),
            (bool) $request->input('assignment', false),
            (bool) $request->input('puppeteer', false)
        );
    }

    /**
     * Check update rights on a labdoc of a report
     * aborts if the user does not have the right or returns the labdoc
     * @param \App\User $user
     * @param \App\Report $report
     * @param int $id_labdoc
     * @param string action
     * @return \App\Labdoc
     */
    private function checkRightsAndGetLabdoc($user, $report, $id_labdoc, $action)
    {
        $labdoc = \App\Labdoc::find($id_labdoc);
        if ($user->cannot($action, $report) && $action === 'update') {
            if (!($labdoc->shared && $user->can('manage', $report))) {
                // Teachers are not allowed to edit report, but they can edit
                // shared labdocs from a report
                abort(Helper::pullPolicyErrorCode(), Helper::pullPolicyErrorMessage());
            }
        }
        abort_if($labdoc == null, 404);
        abort_if(!$labdoc->shared && $labdoc->id_report != $report->id_report, 403);
        abort_if($labdoc->shared && $labdoc->id_team_config != $report->id_team_config, 403);
        abort_if($user->cannot($action, $labdoc), 403, __("Action interdite"));
        return $labdoc;
    }

    /**
     * Display a list of existing and potential reports for a student.
     *
     * @return View
     */
    public function index()
    {
        $user = Auth::user();
        ReportIndex::traceReportLeave($user->id_user);
        

        return view('report.index')->with(
            [
                'user' => $user,
                'missionsRecords' => ReportIndex::getUserMissions($user),
            ]
        );
    }
    /**
     * Display the selected report description
     * @param \Illuminate\Http\Request $request
     * @return View
     */
    public function getRecordDescription(Request $request)
    {
        $user = Auth::user();
        $data = $request->validate([
            'id_report' => 'int|nullable',
            'id_team_config' => 'int|nullable',
            'id_mission' => 'int'
        ]);
        return view('report._mission')->with(['r' => ReportIndex::getSelectedMission($user, $data['id_report'], $data['id_team_config'], $data['id_mission'])]);
    }

    /**
     * Enter a tutorial mission
     *
     * @return \Illuminate\Http\Response
     */
    public function enterTutorial($missionId)
    {
        $user = Auth::user();
        $mission = \App\Mission::find($missionId);
        if (!$mission) {
            abort(404, __("Rapport découverte non trouvé"));
        }

        abort_if($user->cannot('startTutorial', $mission), 403, __("Vous n'avez pas le droit d'acceder à ce rapport"));

        $report = $user->reports()->where('id_mission', $mission->id_mission)->first();
        if (!$report) {
            $report = new \App\Report();
            $report->id_mission = $mission->id_mission;
            $report->status = "tuto";
            $report->initialized = false;
            $report->team_name = __('Equipe');
            $report->allow_msg_teacher = false;
            $report->allow_msg_team = false;
            $report->allow_msg_id_class = false;
            $report->allow_msg_id_mission = false;
            $report->allow_attach_ld = false;
            $report->allow_save_ld = false;
            $report->allow_import = false;
            $report->allow_import_id_mission = false;
            $report->save();
            $report->users()->attach($user);
        }
        return redirect("/report/{$report->id_report}");
    }

    /**
     * Enter a mission through its id_team_config.
     *
     * 3 actions are possible:
     * - redirect to the student's report
     * - add the student to a (new|existing) team, then redirect to it
     * - redirect to team choice page
     *
     * @return \Illuminate\Http\Response
     */
    public function enter($teamconfigId)
    {
        $user = Auth::user();
        $teamconfig = TeamConfig::find($teamconfigId);
        if (!$teamconfig) {
            abort(404, __("Mise en équipe inexistante"));
        }

        $existingReportId = ReportEnter::findReportId($user, $teamconfig);
        if ($existingReportId) {
            return redirect()->action('ReportController@show', ['report' => $existingReportId]);
        }

        switch ($teamconfig->method) {
            case TeamConfig::METHOD_RANDOM:
                $teamconfig->classe->addLearner($user->id_user);
                $newReportId = ReportEnter::assignReport($user, $teamconfig);
                if ($newReportId) {
                    return redirect()->action('ReportController@show', ['report' => $newReportId]);
                }
                abort(500, "Error while randomly assigning a report");
            case TeamConfig::METHOD_STUDENTCHOICE:
                return redirect()->away('/teamconfig/' . $teamconfig->id_team_config.'/select');
            case TeamConfig::METHOD_TEACHERCHOICE:
                $teamconfig->classe->addLearner($user->id_user);
                $teachers = "";
                foreach ($teamconfig->classe->teachers as $teach) {
                    $teachers .= substr($teach->user->first_name, 0, 1).'. '.$teach->user->user_name.',';
                }
                $params = [
                    'mission' => $teamconfig->mission->code,
                    'teachers' => trim($teachers, ',')
                ];
                $message = __(
                    "Nous n'avons pas trouvé le rapport (mission :mission) que vous essayez d'ouvrir.<br />
S'il n'est pas accessible dans le menu de gauche de cette page, demandez à vos enseignants (:teachers) de vous affecter à un rapport puis rechargez cette page (touche F5).",
                    $params
                );
                \App\Helper::AddAlert('danger', $message);
                return redirect('/reports');
        }
        abort(400, 'Invalid input: no action for this parameter.');
    }
 
    /**
     * Set the report status to 'wait' or print an error message.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function submit(Request $request, Report $report)
    {
        $user = Auth::user();
        abort_if($user->cannot('update', $report), Helper::pullPolicyErrorCode(), Helper::pullPolicyErrorMessage());

        return response()->json($report->submit());
    }

    /**
     * Extract ld_chosen list from request
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    private function extractLdChosen($request)
    {
        // ld choices for menu
        $ld_chosen = [];
        foreach ($request->input() as $key => $value) {
            if (preg_match('/^ld_chosen/', $key)) {
                $ld_chosen[$key] = (int) $value;
            }
        }
        return $ld_chosen;
    }

    /**
     * prints the report.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function printPDF(Request $request, Report $report)
    {
        abort_if(Auth::user()->cannot('view', $report), 403, __("Vous n'avez pas le droit de convertir de rapport en PDF"));

        $view = $this->show($request, $report)->render();
        return $view;
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Report $report)
    {
        abort_if(!Auth::check(), 401, __("Vous devez vous authentifier pour accéder à cette page"));
        // Prepare view
        if ($request->has('sc')) {
            $scope = $request->query('sc');
        } else {
            switch ($report->status) {
                case 'test':
                    $scope = 'test';
                    break;
                case 'solution':
                    $scope = $report->mission->designers()->whereKey(Auth::user()->id_user)->exists()?'default':'view';
                    break;
                case 'on':
                case 'new':
                case 'tuto':
                    $scope = 'default';
                    break;
                default:
                    $scope = 'view';
                    break;
            }
        }

        $user = Auth::user();
        $reportView = $this->getReportView($request, $report, $scope);
        // Last security check
        abort_if($user->cannot('view', $reportView), Helper::pullPolicyErrorCode(), Helper::pullPolicyErrorMessage());
        
        if($reportView->is_follow){
            if($request->has('freezeLds') && $request->has('openScoring')) {
                $reportView->freeze_lds_on_init = $request->query('freezeLds') === 'true';
                $reportView->open_scoring_on_init = $request->query('openScoring') === 'true';
            }
        }

        $reportView->assignment = $report->init($user);

        // This must be done after the security check thus not in reportView initialization
        if ($reportView->is_pdf && !$reportView->puppeteer) {
            session([$reportView->filepath => true]);
        }

        $report->insertMissingLdStatuses($user);
        // suppress all tags for modifications that are synchronized at opening
        DB::update(DB::raw("UPDATE labdoc_status ls SET ls.deleted = 0, ls.moved = 0, ls.drafted = 0, ls.modified = 0 WHERE ls.id_labdoc IN (SELECT id_labdoc FROM labdoc ld WHERE ld.id_report = ? ) AND ls.id_user = ?"), [$report->id_report, $user->id_user]);
        DB::update(DB::raw("UPDATE link_report_learner lrl SET lrl.reload_annotations = 0 WHERE lrl.id_report = ? AND lrl.id_user = ?"), [$report->id_report, $user->id_user]);

        // mise à jour du current_report de l'utilisateur
        if ($reportView->is_default && $report->status !== 'solution' && $user->current_report != $report->id_report) {
            $user->current_report = $report->id_report;
            $user->save();
        }

        // mise à jour de l'interface du teacher
        if ($reportView->is_teacher) {
            $reportView->teacherAddress = $user->teacher->getInterfaceUrl();
        }

        if (!$report->isUserInReport($user->id_user, $reportView->is_follow)) {
            $reportView->traceReportEnter($user);
        }
        $report->setLastVisit($user, $scope);
        $displayNewDoc = $report->displayNewRessources($user); //get unread resources
        return view('report.report')->with(['rv'=> $reportView,'displayNewDoc'=>$displayNewDoc]);
    }

    /**
     * Open a temporary test report
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function test(Request $request)
    {
        $user = \App\User::getTestUser();
        if (!$user) {
            \App\Helper::addAlert("warning", __("Le mode test n'est pas activé, veuillez vous connecter"));
            return redirect("/");
        }
        Auth::login($user);
        $report = Report::getTestReport();
        if (!$report) {
            abort(500, __("Erreur lors de la création du rapport de test."));
        }
        return $this->show($request, $report);
    }

    /**
     * Returns the list of labdoc to print
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function listLdToPrint(Request $request, Report $report)
    {
        $reportView = $this->getReportView($request, $report);
        abort_if(Auth::user()->cannot('view', $reportView), Helper::pullPolicyErrorCode(), Helper::pullPolicyErrorMessage());
        return view('report._listLdToPrint')->with('rv', $reportView);
    }

    /**
     * return the view to import Lds
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function displayLDToImport(Request $request, Report $report)
    {
        $data = $request->validate([
            'id_rp' => 'int',
            'id_m' => 'int',
            'ld_txt' => 'int',
            'ld_drw' => 'int',
            'ld_pro' => 'int',
            'ld_ds' => 'int',
            'ld_cde' => 'int',
            'role' => 'in:learner,teacher',
        ]);
        $user = Auth::User();
        abort_if($user->cannot('importLabdoc', $report), Helper::pullPolicyErrorCode(), Helper::pullPolicyErrorMessage());
        $rp = (int) $request->input('id_rp');
        $reportPart = \App\ReportPart::find($rp);
        abort_if($reportPart->id_mission != $report->id_mission, 409, __("Tentative d'import dans un autre rapport"));
        if (!isset($data['role'])) {
            // Retro compatibility in case of old JS file
            $data['role'] = $data['id_m'] === -1 ? 'teacher' : 'learner';
        }
        if ($data['role'] === 'teacher') {
            abort_if($user->cannot('manage', $report), 403, __("Vous n'êtes pas enseignant de ce rapport"));
        } else {
            $data['id_m'] = $report->status === 'solution' ? $report->id_mission:$report->allow_import_id_mission;
            $data['ld_txt'] = $reportPart->text;
            $data['ld_drw'] = $reportPart->drawing;
            $data['ld_pro'] = $reportPart->procedure;
            $data['ld_ds'] = $reportPart->dataset;
            $data['ld_cde'] = $reportPart->code;
        }
        $role = $report->status === 'solution' ? 'corrector' : 'learner';
        // Retrieve list of lds
        $lds = \App\Labdoc::fetchLDToImport(
            $user->id_user,
            $report->id_report,
            $rp,
            $data['id_m'],
            $data['ld_txt'],
            $data['ld_drw'],
            $data['ld_pro'],
            $data['ld_ds'],
            $data['ld_cde'],
            $role
        );
        return view('report/_listLdToImport')->with(['lds' => $lds, 'id_rp' => $rp]);
    }

    /**
     * duplique le LD en fin de rp.
     * creates the new name of the duplicata and then call addLD()
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Report  $report
     * @return \Illuminate\Http\JsonResponse
     */
    public function duplicateLD(Request $request, Report $report)
    {
        $user = Auth::user();
        abort_if($user->cannot('manageOrUpdate', $report), Helper::pullPolicyErrorCode(), Helper::pullPolicyErrorMessage());
        $data = $request->validate([
            'id_rp' => 'int|nullable',
            'id_ld' => 'int',
            'edition' => 'boolean',
            'incr_name' => 'boolean',
            'scope' => 'string|in:default,view,pdf,test,follow',
        ]);
        $labdoc = \App\Labdoc::find($data['id_ld']);
        abort_if($user->cannot('view', $labdoc), Helper::pullPolicyErrorCode(), Helper::pullPolicyErrorMessage());
        $sameReport = false;
        if ((!$labdoc->shared && $labdoc->id_report == $report->id_report) ||
                ($labdoc->shared && $labdoc->id_team_config == $report->id_team_config)
        ){
            // Duplication in the same report
            abort_if($user->cannot('duplicate', $labdoc), Helper::pullPolicyErrorCode(), Helper::pullPolicyErrorMessage());
            $sameReport = true;
        }
        if ($labdoc->messages()->count() > 0 ) {
            // Import from message
            abort_if($user->cannot('importLabdocFromMessage', $report), Helper::pullPolicyErrorCode(), Helper::pullPolicyErrorMessage());
        } else {
            if (!$sameReport) {
                // Import from different report
                abort_if($user->cannot('importLabdoc', $report), Helper::pullPolicyErrorCode(), Helper::pullPolicyErrorMessage());
                if ($data['scope'] !== 'follow' && $report->allow_import_id_mission && $labdoc->mission->id_mission != $report->allow_import_id_mission) {
                    abort(403, __("Vous n'avez pas les droits d'importer ce labdoc"));
                }
            }
        }

        return response()->json($report->duplicateLD(
            $labdoc,
            $data['edition'],
            $data['id_rp'],
            $data['incr_name'],
            $user->id_user
        ));
    }

    /**
     * Ajoute un LD en fin de rp
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function addLD(Request $request, Report $report)
    {
        $data = $request->validate([
            'id_rp' => 'int',
            'type' => 'in:procedure,dataset,text,drawing,code',
        ]);
        abort_if(Auth::user()->cannot('update', $report), Helper::pullPolicyErrorCode(), Helper::pullPolicyErrorMessage());
        return response()->json($report->addLD(
            '',
            $data['type'],
            0,
            1,
            $data['id_rp'],
            Auth::id()
        ));
    }

    /**
     * Demarre le mode édition d'un Labdoc
     * @param \App\User $user
     * @param \App\Report $report
     * @param \App\Labdoc $labdoc
     */
    private function startEditionMode($user, $report, $labdoc)
    {
        $ans = [];
        $labdoc->updateStatus(['mod_icon'=>0], $user->id_user);
        // si l'édition n'est pas possible
        if (!$labdoc->editable) {
            $ans['status'] = 'forbidden';
            $ans['message'] = __("Vous ne pouvez pas éditer ce labdoc");
        } else {
            $ans['status'] = $labdoc->getUsabilityStatus($user->id_user);
            if ($ans['status'] == "locked") {
                $ans['message'] = __("Ce labdoc ne peut pas être modifié, car il est actuellement modifié par un autre membre de l'équipe.");
            } elseif ($ans['status'] == "deleted") {
                $ans['message'] = __("Ce labdoc a été supprimé par un autre membre de l'équipe.");
            } else {
                $ans['status'] = 'ok';
                $labdoc->setLock($user->id_user);
            }
        }
        return $ans;
    }

    /**
     * Ouvre le versioning du ld (si possible)
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function openLDversioning(Request $request, Report $report)
    {
        $user = Auth::user();
        $id_ld = (int) $request->input('id_ld');
        $labdoc = $this->checkRightsAndGetLabdoc($user, $report, $id_ld, 'update');
        $ans = $this->startEditionMode($user, $report, $labdoc);
        if ($ans['status'] != 'deleted') {
            $ldv = \App\Views\Labdoc::fromLd($labdoc, $user, 'default', 0, $ans['status'] == 'ok');
            if ($ans['status'] == 'locked') {
                $ans['html'] = view('labdoc/_ld')->with(['ldv' => $ldv])->render();
            } else {
                // Retrieve versions
                if ($labdoc->shared) {
                    $id_mission = \App\TeamConfig::find($labdoc->id_team_config)->id_mission;
                } else {
                    $id_mission = $labdoc->report ? $labdoc->report->mission->id_mission : $labdoc->report_part->mission->id_mission;
                }
                $labdocVersioning = new \App\LabdocVersioning($id_mission);
                $versions = $labdocVersioning->load($labdoc->id_labdoc);
                if (!$versions) {
                    $versions = new \StdClass();
                    $versions->contents = [];
                } else {
                    $versions->contents = array_reverse($versions->contents);
                }

                $labdoc->last_edition_format = \App\Helper::datetimeToFr(date('Y-m-d G:i.s', $labdoc->last_edition));
                $labdoc->id_version = 0;
                foreach ($versions->contents as $index => $vers) {
                    $vers->user = DB::table('user')
                         ->where('id_user', $vers->id_user)
                         ->pluck(DB::raw('concat(first_name, " ", user_name)'))
                         ->first();
                    // Is it the current version ?
                    if (!$labdoc->id_version && $vers->data === $labdoc->labdoc_data) {
                        $labdoc->id_version = $vers->id_trace;
                        $vers->current = true;
                    } else {
                        $vers->current = false;
                    }
                    $vers->date_format = \App\Helper::datetimeToFr(date('Y-m-d G:i.s', $vers->ts));
                }

                // Current version is not in history, add it for consistency, this is due to #738
                if (!$labdoc->id_version) {
                    $cur_v = new \StdClass();
                    $cur_v->data = $labdoc->labdoc_data;
                    $cur_v->id_trace = 1;
                    $cur_v->current = true;
                    if ($labdoc->user_last_editor) {
                        $cur_v->user = $labdoc->user_last_editor->first_name." ".$labdoc->user_last_editor->user_name;
                    } else {
                        $cur_v->user = __("inconnu");
                    }
                    $cur_v->name = $labdoc->name;
                    $cur_v->ts = $labdoc->last_edition;
                    $cur_v->date_format = $labdoc->last_edition_format;
                    array_unshift($versions->contents, $cur_v);
                    $labdoc->id_version = $cur_v->id_trace;
                }
                // Add an initial version in case id_ld_origin is not null
                if ($labdoc->id_ld_origin !== null && count($versions->contents) > 0) {
                    $labdoc_init = \App\Labdoc::find($labdoc->id_ld_origin);
                    $init_v = new \StdClass();
                    $init_v->data = $labdoc_init->labdoc_data;
                    $init_v->id_trace = null;
                    $init_v->current = false;
                    if ($labdoc_init->user_last_editor) {
                        $init_v->user = $labdoc_init->user_last_editor->first_name." ".$labdoc_init->user_last_editor->user_name;
                    } else {
                        $init_v->user = __("inconnu");
                    }
                    $init_v->name = $labdoc_init->name;
                    $init_v->ts = $labdoc_init->last_edition;
                    $init_v->date_format = \App\Helper::datetimeToFr(date('Y-m-d G:i.s', $labdoc_init->last_edition));
                    array_push($versions->contents, $init_v);
                }
                Trace::logAction(Trace::OPEN_LD_VERSIONING, ['id_report' => $report->id_report, 'id_mission' => $report->id_mission, 'id_labdoc' => $id_ld]);
                $ans['html'] = view('labdoc/_versions')->with(['ldv' => $ldv, 'versions' => $versions, 'user' => $user])->render();
            }
        }
        return response()->json($ans);
    }

    /**
     * Ferme le versioning du ld
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function closeLDversioning(Request $request, Report $report)
    {
        $user = Auth::user();
        $id_ld = (int) $request->input('id_ld');
        $labdoc = $this->checkRightsAndGetLabdoc($user, $report, $id_ld, 'update');
        $labdoc->unsetLock($user->id_user);
        $ldv = \App\Views\Labdoc::fromLd($labdoc, $user, 'default', 0);
        return response()->json([
            'html' => view('labdoc/_ld')->with(['ldv' => $ldv])->render(),
        ]);
    }



    /**
     * appelle un LD en mode edition (si possible)
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function editLD(Request $request, Report $report)
    {
        $user = Auth::user();
        $id_ld = (int) $request->input('id_ld');
        $labdoc = $this->checkRightsAndGetLabdoc($user, $report, $id_ld, 'update');
        $ans = $this->startEditionMode($user, $report, $labdoc);
        if ($ans['status'] == "ok") {
            // trace
            if ($report->status === 'on') { // ne pas tracer en mode test
                Trace::logAction(Trace::EDIT_LD, ['id_report' => $report->id_report, 'id_mission' => $report->id_mission, 'id_labdoc' => $id_ld, 'type' => $labdoc->type_labdoc]);
            }
        }
        if ($ans['status'] != 'deleted') {
            $edition = $ans['status'] =='ok';
            $ldv = \App\Views\Labdoc::fromLd($labdoc, $user, 'default', $edition);
            $ans['html'] = view('labdoc/_ld')->with(['ldv' => $ldv])->render();
        }
        return response()->json($ans);
    }

    /**
     *
     * Termine l'édition du LD.
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function validateLD(Request $request, Report $report)
    {
        $user = Auth::user();
        $id_ld = (int) $request->input('id_ld');
        $ld = $this->checkRightsAndGetLabdoc($user, $report, $id_ld, 'update');
        if (!$ld->editable) {
            return response()->json(['message' => __("Vous ne pouvez pas éditer ce labdoc")], 403);
        }
        $data = $request->validate([
            'name' => 'string|nullable',
            'content' => 'string|nullable',
            'recovering' => 'in:0,1,true,false,"true","false","True","False"',
            'new_version' => 'int',
            'recovering_id_version' => 'string|nullable'
        ]);
        
        $name = $ld->editable_name ? $data['name'] : $ld->name;
        $content = $data['content'];
        // Ugly retro compatibility for #738
        if (!isset($data['recovering'])) {
            $recovering = 0;
        } else {
            $recovering = $data['recovering'];
            if (in_array($recovering, ["true","True"])) {
                $recovering = 1;
            } elseif (in_array($recovering, ["false","False"])) {
                $recovering = 0;
            }
        }
        $new_version = isset($data['new_version']) ?$data['new_version'] : 1;
        $recovering_id_version = isset($data['recovering_id_version']) ? intval($data['recovering_id_version']) : 0;

        if ($ld == null) {
            return response()->json(['message' => __('Labdoc non trouvé')], 404);
        }

        if ((int) $ld->last_editor !== (int) $user->id_user) {
            Log::info("validateLD() by user={$user->id_user} is invalid: id_labdoc={$id_ld} locked={$ld->locked} last_editor={$ld->last_editor}");
            return response()->json(['message' => __("Vous n'êtes pas reconnu comme éditeur du labdoc.")], 400);
        }
        if (!$ld->locked) { // the LD id unlocked! (probably because of a network problem) => the lock is set again
            $ld->setLock($user->id_user);
        }
        
        $old_name=$ld->name;
       
        // Update in DB and check if the logging of traces is needed 
        $result = $ld->updateInDB($user->id_user, $report->id_mission, $content, $name, 0);
        if ($result['error'] === \App\Labdoc::CONTENT_ERR_FATAL) {
            abort(422, __("Le contenu du labdoc n'a pu être sauvé"));
        }

        $ld->unsetLock($user->id_user);
        $ld->updateStatus([ 'extend' => 1 ], $user->id_user);

        // logTracks
        $traceId = 0;
        if ($report->shouldTrace()) { // ne pas tracer en mode test
            if ($recovering) {
                $version = date("Y-m-d H:i:s", $ld->last_edition);
                Trace::logAction(Trace::RECOVER_LD, ['id_report' => $report->id_report, 'id_mission' => $report->id_mission, 'id_labdoc' => $id_ld, 'version'=> $version]);
            } else {
                if ($result['content_has_changed']) {
                    Trace::logAction(Trace::MODIFY_LD, ['id_report' => $report->id_report, 'id_mission' => $report->id_mission, 'id_labdoc' => $id_ld]);
                }
                if ($result['name_has_changed']) {
                    Trace::logAction(Trace::UPDATE_LD_NAME, ['id_report' => $report->id_report, 'id_mission' => $report->id_mission, 'id_labdoc' => $id_ld, 'new' => $name, 'previous' => $old_name]);
                }
                $traceId = Trace::logAction(Trace::VALIDATE_LD, ['id_report' => $report->id_report, 'id_mission' => $report->id_mission, 'id_labdoc' => $id_ld]);
            }
        }
        // Versioning
        if ($new_version && $report->shouldTrace()) {
            try {
                $versioning = new \App\LabdocVersioning($report->id_mission);
                $store_version = true;
                // We store a new version for a labdoc text only if the name is different or the content is different
                if($ld->type_labdoc === Labdoc::TEXT) {
                    $last_stored_version_ld_data = $versioning->lastVersionContentData($ld->id_labdoc);
                    $store_version = $old_name !== $name || $content !== $last_stored_version_ld_data;
                }
                if($store_version) {
                    $versioning->save($ld, $traceId, $user->id_user);
                    $ld->last_id_version = $traceId;
                    $ld->save();
                    $ld->storeLastIdVersionReviewed($ld->last_id_version, $user->id_user);
                }
            } catch (\Exception $e) {
                \App\Helper::reportError(
                    new \Exception(
                        "Erreur lors du versioning : (traceID=".$traceId.", labdocdId=".$ld->id_labdoc.", missionId="
                        .\App\Report::find($ld->id_report)->id_mission.", userId=".$user->id_user."). Error Message:".$e->getMessage()
                    )
                );
            }
        }
        // Update last_id_version in case of recovering
        if ($recovering && $recovering_id_version > 0) {
            $ld->last_id_version = $recovering_id_version;
            $ld->save();
            $ld->storeLastIdVersionReviewed($ld->last_id_version, $user->id_user);
        }
        // MaJ du LD en mode affichage
        $ldv = \App\Views\Labdoc::fromLd($ld, $user, 'default', 0);
        return response()->json([
            'error' => $result['error'],
            'html' => view('labdoc/_ld')->with(['ldv' => $ldv])->render()
        ]);
    }

    /**
     * Supprime un LD
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ReportPart  $report
     * @return \Illuminate\Http\Response
     */
    public function deleteLD(Request $request, Report $report)
    {
        $user = Auth::user();
        $data = $request->validate([
            'id_ld' => 'int|required',
        ]);
        $labdoc = \App\Labdoc::find($data['id_ld']);
        abort_if($user->cannot('delete', $labdoc), Helper::pullPolicyErrorCode(), Helper::pullPolicyErrorMessage());
        return response()->json($report->deleteLD(
            $labdoc->id_labdoc,
            $user->id_user
        ));
    }

    /**
     * Synchronizes a report.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ReportPart  $report
     * @return \Illuminate\Http\Response
     */
    public function synchronize(Request $request, Report $report)
    {
        $user = Auth::user();
        $scope = $request->input('scope');
        $perm = in_array($scope, ['view', 'follow']) ? 'view' : 'update';
        abort_if($user->cannot($perm, $report), Helper::pullPolicyErrorCode(), Helper::pullPolicyErrorMessage());
        if ("follow" === $scope) {
            abort_if($user->cannot('manage', $report), Helper::pullPolicyErrorCode(), Helper::pullPolicyErrorMessage());
        }
        $edited_ld = (int) $request->input('edited_ld');
        $message = (int)$request->input('message');
        return response($report->synchronize($user, $scope, $edited_ld, $message))->header('Content-Type', 'application/xml');

    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function trace(Request $request, Report $report)
    {
        $trace = $request->input('trace');
        $data = ['id_report' => (int) $report->id_report, 'id_mission' => (int) $report->id_mission];
        if ($trace === "viewPartAssignment") {
            $data['id_rp'] = (int) $request->input('id_report_part');
            $trace = Trace::OPEN_RP_ASSIGNMENT;
        } elseif ($trace === "viewAssignment") {
            $trace = Trace::OPEN_ASSIGNMENT;
        } elseif ($trace === "viewDetailedAssignment") {
            $trace = Trace::OPEN_DETAILED_ASSIGNMENT;
        } elseif ($trace === "viewDoc") {
            $data['url'] = $request->input('url');
            $trace = Trace::OPEN_RESOURCE;
        } elseif ($trace === "LeaveReport") {
            $trace = Trace::LEAVE_REPORT;
        } 
        Trace::logAction($trace, $data);

        return response()->json('');
    }

    /**
     * Shows the reports pdf.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function displayPDF(Request $request, Report $report)
    {
        $filepath = urldecode((string)$request->input('filepath'));
        $path = session()->get($filepath, null);
        if ($path == null) {
            abort(403, __("Le fichier demandé n'existe pas"));
        }
        if (file_exists($filepath)) {
            header("Content-Type: application/pdf");
            return readfile($filepath);
        }
    }

    /**
     * Toggles all LDs of the report
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function toggleAllLD(Request $request, Report $report)
    {
        $user = Auth::user();
        abort_if($user->cannot('view', $report), Helper::pullPolicyErrorCode(), Helper::pullPolicyErrorMessage());
        $report->toggleAllLD($user->id_user, (int) $request->input('extend'));
    }

    /**
     * Freeze report trace
     * @param \Illuminate\Http\Request $request
     * @param \App\Report $report
     */
    public function  freezeReportTrace(Request $request, Report $report)
    {
        $user = Auth::user();
        abort_if($user->cannot('manage', $report),Helper::pullPolicyErrorCode(), Helper::pullPolicyErrorMessage());
        Trace::logAction(Trace::TEACHER_FREEZE_REPORT, ['id_report' => $report->id_report, 'id_mission' => $report->id_mission]);
    }
}
