<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helper;

class LogController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function jserror(Request $request)
    {
        Helper::logJsError(
            $request->input('category').": ".$request->input('message'),
            $request->input('time'),
            $request->input('trace', null)
        );
        if (config('app.env') === 'test') {
            // Tell the JS error handler that it should abort because we
            // are in test mode and thus we do not tolerate errors
            return response()->json('abort');
        }
        return response()->json('');
    }
}
