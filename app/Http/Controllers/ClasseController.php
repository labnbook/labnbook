<?php

namespace App\Http\Controllers;

use \App\Classe;
use Hoa\Compiler\Llk\Rule\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ClasseController extends Controller
{
    /* Check if a class code is valid
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
    */
    public function joinByCode(Request $request)
    {
        $data = $request->validate([
            'code' => 'string|required',
        ]);

        $code = $data['code'];
        $classe = Classe::fromCode($code)->first();
        if (!$classe) {
            \App\Helper::AddAlert('danger', __("Le code fourni n'existe pas ou a expiré"));
            return redirect('/');
        }
        if (auth()->check()) {
            session()->put('user_code', $code);
            return self::join($request, $classe);
        }

        $user = new \App\User();
        $user->id_inst = $classe->id_inst;
        session()->put('user', $user);
        session()->put('authType', 'code');
        session()->put('bindUrl', '/classe/'.$classe->id_class.'/join');
        session()->put('confirmUrl', '/user/create');
        session()->put('user_code', $classe->class_code);
        session()->pull('url.intended');
        return \App\Http\Controllers\Auth\LoginController::bindOrCreateAccount($request);
    }

    /*
     * Joins a class
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Classe  $classe
     * @return \Illuminate\Http\Response
    */
    public function join(Request $request, Classe $classe)
    {
        $code = session()->pull('user_code');
        abort_if($classe->class_code != $code, '403');
        $user = Auth::user();
        if (!$classe->users->contains($user)) {
            $classe->addLearner($user->id_user);
        }
        \App\Helper::AddAlert('success',  __("l'utilisation du code :code a permis de vous ajouter à la classe de votre enseignant", ["code" => $code]));
        return redirect()->intended('');
    }

    /* Check if classe is external
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Classe  $classe
     * @return \Illuminate\Http\Response
    */
    public function isExternal(Request $request, Classe $classe)
    {
        abort_if(Auth::user()->cannot('view', $classe), 403);
        return response()->json(['external' => $classe->isExternal()]);
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addOrUpdate(Request $request)
    {
        $data = $request->validate([
            'inst' => 'int|required',
            'name' => 'string|required',
            'code' => 'string|nullable',
            'teachers' => 'JSON',
            'teacherTeams' => 'JSON',
            'teachers.*' => 'int',
            'id_class' => 'nullable|int',
        ]);

        try {
            return response()->json(Classe::addOrUpdate(
                $data["name"],
                $data["code"],
                $data["inst"],
                json_decode($data["teachers"]),
                $data["id_class"],
                json_decode($data["teacherTeams"])
            ));
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /** Deletes a class
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Classe  $classe
     * @return \Illuminate\Http\Response
    */
    public function delete(Request $request, Classe $classe)
    {
        abort_if(Auth::user()->cannot('update', $classe), 403);
        abort_if($classe->isExternal() && $classe->teamConfigs()->count() > 0, 409, __("Impossible de supprimer cette classe car elle est liée à des activités Moodle"));
        $classe->delete();
        return response()->json(true);
    }

    /**
     * @param Request $request
     * @param Classe $classe
     * @return \Illuminate\Http\JsonResponse
     */
    public function unsubscribe(Request $request, Classe $classe)
    {
        $user = Auth::user();
        abort_if($user->cannot('update', $classe), 403);
        if($classe->teachers()->count() > 1) {
            $classe->teachers()->detach($user->id_user);
            return response()->json(['message' => 'success'], 200);
        } else {
            return response()->json(['message' => __('Au moins un enseignant doit être associé à cette classe')], 400);
        }
    }

    /* Adds a student to a class
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Classe  $classe
     * @return \Illuminate\Http\Response
    */
    public function addLearner(Request $request, Classe $classe)
    {
        abort_if(Auth::user()->cannot('update', $classe), 403);
        $data = $request->validate(['id_user' => 'int|required']);

        $msg = $classe->addLearner($data['id_user'], 'assign') ? '' :
            __("L'étudiant était déjà dans la classe.");
        return response()->json(__($msg));
    }

    /* Adds several students to a class
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Classe  $classe
     * @return \Illuminate\Http\Response
    */
    public function addLearners(Request $request, Classe $classe)
    {
        abort_if(Auth::user()->cannot('update', $classe), 403);
        $data = $request->validate([
            'learners' => 'array|required',
            'learners.*' => 'int'
        ]);

        $out = [
            'failed' => [],
            'success' => [],
        ];

        foreach ($data['learners'] as $id_user) {
            $ans = $classe->addLearner($id_user, 'assign');
            $cat = $ans ? 'success' : 'failed';
            array_push($out[$cat], $id_user);
        }
        if (!empty($out['failed'])) {
            $out['message'] = __("Des étudiants étaient déjà dans la classe.");
        }
        return response()->json($out);
    }
   
    /**
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Classe  $classe
     * @return \Illuminate\Http\Response
     */ 
    function updateClassCode(Request $request, Classe $classe) {
        abort_if(Auth::user()->cannot('update', $classe), 403);
        $data = $request->validate([
            'class_code' => ['string',
                'nullable',
                \Illuminate\Validation\Rule::unique('class')->ignore($classe)
            ]
        ]);
        $classe->class_code = $data['class_code'];
        $classe->save();
        return response()->json(['new_code'=>$data['class_code']]);
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Classe  $classe
     * @return \Illuminate\Http\Response
     */
    public function addOrUpdateUser(Request $request, Classe $classe)
    {
        abort_if(Auth::user()->cannot('update', $classe), 403);
        $data = $request->validate([
            'id_user' => 'int',
            'user_name' => 'string',
            'first_name' => 'string',
            'id_inst' => 'int|nullable',
            'inst_number' => 'string|nullable',
            'email' => 'string|nullable',
            'login' => 'string',
            'password' => 'string|nullable',
            'send_mail' => 'in:true,false',
        ]);
        $data['email'] = $data['email'] ?? "";
        $id_inst = $data['id_inst'] ?? $classe->id_inst;
        return response()->json(\App\User::addOrUpdate(
            $data['id_user'],
            $classe,
            $data['user_name'],
            $data['first_name'],
            $id_inst,
            $data['inst_number'],
            $data['email'],
            $data['login'],
            $data['password'],
            $data['send_mail']
        ));
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Classe  $classe
     * @return \Illuminate\Http\Response
     */
    public function checkAndAddCSVLearners(Request $request, Classe $classe)
    {
        abort_if(Auth::user()->cannot('update', $classe), 403);
        $data = $request->validate([
            'json_array' => 'array|required',
            'save' => 'in:true,false',
            'send_mail' => 'in:true,false',
            'genPasswords' => 'in:true,false|nullable',
        ]);
        $genPasswords = $data['genPasswords'] ?? true;

        // Handle CSV
        $out = \App\Processes\AddCSVLearners::checkAndAddCSVLearners(
            $data['json_array'],
            $classe,
            $data['save'],
            $data['send_mail'],
            $genPasswords
        );

        // Prepare view
        $out['html'] = view('teacher/students/_csvLearners')->with([
            'learners' => array_key_exists('data', $out) ? $out['data'] : [],
            'warnings' => array_key_exists('warnings', $out) ? $out['warnings'] : [],
            'errors' => array_key_exists('errors', $out) ? $out['errors'] : [],
            'nb_warnings' => array_key_exists('nb_warnings', $out) ? $out['nb_warnings'] : 0,
            'nb_errors' => array_key_exists('nb_errors', $out) ? $out['nb_errors'] : 0,
        ])->render();

        // Cleanup
        unset($out['warnings']);
        unset($out['nb_warnings']);
        unset($out['errors']);
        unset($out['nb_errors']);

        return response()->json($out);
    }

    /* Archive the classe
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Classe  $classe
     * @return \Illuminate\Http\Response
    */
    public function archive(Request $request, Classe $classe)
    {
        abort_if(Auth::user()->cannot('update', $classe), 403);
        return response()->json($classe->archive());
    }

    /* Unpack the classe
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Classe  $classe
     * @return \Illuminate\Http\Response
    */
    public function unpack(Request $request, Classe $classe)
    {
        abort_if(Auth::user()->cannot('update', $classe), 403);
        return response()->json($classe->unpack());
    }

    /* Unpack the classe
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Classe  $classe
     * @return \Illuminate\Http\Response
    */
    public function hasLdap(Request $request, Classe $classe)
    {
        abort_if(Auth::user()->cannot('update', $classe), 403);
        return response()->json([
            'ldap' => \App\Processes\Ldap\Ldap::classHasLdap($classe->id_class)
        ]);
    }

    /*
     * Get the students of the classe
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Classe  $classe
     * @return \Illuminate\Http\Response
    */
    public function getStudents(Request $request, Classe $classe)
    {
        abort_if(Auth::user()->cannot('view', $classe), 403);
        return response()->json(['students' => $classe->getStudentsNameAndId()]);
    }

    /*
     * Get the students of the classe formatted for datatable
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
    */
    public function getLearnersData(Request $request)
    {
        $id_class = $request->input('id_class', null);
        if ($id_class) {
            abort_if(Auth::user()->cannot('view', \App\Classe::find($id_class)), 403);
        }

        // Get Learners with their classes
        $results = DB::select(
            DB::raw(
                "SELECT lcl.id_user, c.class_name"
                . " FROM link_class_learner lcl JOIN link_class_learner lcl1 USING(id_user) JOIN class c ON lcl1.id_class = c.id_class"
                . " WHERE lcl.id_class = ?"
                . " ORDER BY c.class_name"
            ),
            [$id_class]
        );
        $members = [];
        foreach ($results as $m) {
            $members[$m->id_user][] = $m->class_name;
        }

        // Get users
        $users = \App\User::Hydrate(
            DB::select(
                DB::raw(
                    "SELECT u.id_user, u.login, u.first_name, u.user_name, u.email, u.inst_number, u.last_synchro, u.pwd, GROUP_CONCAT(m.code ORDER BY r.id_report DESC SEPARATOR ' ; ') AS missions"
                    . " FROM user u"
                    . " JOIN link_class_learner lcl ON u.id_user = lcl.id_user"
                    . " LEFT JOIN link_report_learner lrl ON u.id_user = lrl.id_user"
                    . " LEFT JOIN report r ON lrl.id_report = r.id_report AND r.delete_time is NULL AND r.status IN ('new', 'on', 'wait', 'arc')"
                    . " LEFT JOIN mission m ON r.id_mission = m.id_mission"
                    . " WHERE lcl.id_class = ?"
                    . " GROUP BY u.id_user"
                    . " ORDER BY u.user_name"
                ),
                [$id_class]
            )
        );

        $learners = [];

        foreach ($users as $u) {
            $u->id_user = (int) $u->id_user;
            if (!$u->isInternal()) {
                $u->login = $u->login . " (".__("compte institutionnel").")";
            }
            $u->last_synchro = (int) $u->last_synchro;
            $u->classes_num = count($members[$u->id_user]);
            $u->classes_names = join(", ", $members[$u->id_user]);
            $learners[] = $u;
        }

        return response()->json(['data' => $learners]);
    }

    /*
     * Get the view for creating / editing class
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
    */
    public function initEdit(Request $request)
    {
        $user = Auth::user();
        $teach = $user->teacher;
        $id_class = (int)$request->input('id_class');
        if ($id_class) {
            $classe = \App\Classe::find($id_class);
            abort_if($user->cannot('update', $classe), 403);

            $institutions = [$classe->institution];
            $teachers = $classe->teachers;
            $id_inst = $classe->id_inst;
        } else {
            // We are creating a class
            $classe = new \App\Classe();
            $institutions = $teach->institutions;
            $id_inst = $institutions[0]->id_inst;
            $teachers = Collect([$user->teacher]);
        }

        $instHTML = view('teacher/students/_institutionOptions')
            ->with(['institutions' => $institutions])->render();

        $data = [
            "class_name" => $classe->class_name,
            "institutionsHTML" => $instHTML,
            "class_code" => $classe->class_code,
            "teachers" => $teachers->map(
                function ($t) {
                    return [
                        'id_teacher' => $t->id_teacher,
                        'name' => $t->user->longDisplayName(false)
                    ];
                }
            ),
            "teacherTeamsAssociated" => $classe->teacherTeams()->get()
        ];

        return response()->json($data);
    }
}
