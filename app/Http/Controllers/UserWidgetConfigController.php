<?php

namespace App\Http\Controllers;

use App\Report;
use App\UserWidgetConfig;
use Illuminate\Http\Request;
use App\Views\Widgets\Widget;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class UserWidgetConfigController extends Controller
{
    /**
     * Save a widget config.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveUserWidgetConfig(Request $request)
    {
        $data = $request->validate([
            'id_report' => 'required|integer',
            'widget_type' => 'string|nullable',
            'widget_reduced' => 'required|boolean',
            'widget_position' => 'required|string',
            'widget_size_ratio' => 'int',
        ]);
        $user = Auth::user();
        abort_if($user->cannot('view', Report::find($data['id_report'])), 403);

        if (!array_key_exists('widget_type', $data)) {
            $data['widget_type'] = null;
        }
        
        $user->widgetConfig()->updateOrCreate([], [
            'widget_type' => $data['widget_type'],
            'widget_reduced' => $data['widget_reduced'],
            'widget_position' => $data['widget_position'],
            'widget_size_ratio' => $data['widget_size_ratio']
        ]);
        
        return response()->json(['message' => 'config saved successfully.']);
    }
}
