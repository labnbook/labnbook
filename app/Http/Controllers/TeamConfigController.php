<?php

namespace App\Http\Controllers;

use App\Trace;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\TeamConfig;
use App\TeamChoice;

class TeamConfigController extends Controller
{
    /**
     * Show select team config page
     *
     * @param  \App\TeamConfig  $teamconfig
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function select(TeamConfig $teamconfig, Request $request)
    {
        $user = Auth::user();
        abort_if($user->cannot('select', $teamconfig), 403);
        if (empty($teamconfig)) {
            \App\Helper::addAlert('danger', __("Paramètre id_team_config manquant ou non-valide."));
            return redirect('/report/');
        }
        $existing_report = $user->reports()->where('id_team_config', $teamconfig->id_team_config)->first();
        if ($existing_report) {
            return redirect('/report/' . $existing_report->id_report);
        }
        $teamconfig->classe->addLearner($user->id_user);
        $teamChoice = new TeamChoice($user);
 
        return view('teamconfig/select')->with($teamChoice->actionGetTeams($teamconfig));
    }

    /**
     * Returns the teams
     *
     * @param  \App\TeamConfig  $teamconfig
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getTeams(TeamConfig $teamconfig, Request $request)
    {
        $user = Auth::user();
        abort_if($user->cannot('view', $teamconfig), 403);
        $teamChoice = new TeamChoice($user);
        return response()->json($teamChoice->actionGetTeams($teamconfig));
    }

    /**
     * Select a team
     *
     * @param  \App\TeamConfig  $teamconfig
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function selectTeam(TeamConfig $teamconfig, Request $request)
    {
        $user = Auth::user();
        abort_if($user->cannot('select', $teamconfig), 403);
        $teamconfig->classe->addLearner($user->id_user);
        $teamChoice = new TeamChoice($user);
        $json = $teamChoice->actionSelectTeam((int) $request->input('id_report'), $teamconfig);
        $code = $json['status'] == 'success' ? 200 : 409;
        return response()->json($json, $code);
    }

    /**
     * Get team config + teams from class and id_mission + mission reusable
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getConfigAndTeamsFromClasseMission(Request $request)
    {
        $user = Auth::user();
        $data = $request->validate([
            'id_mission' => 'int',
            'id_class' => 'int',
            'reuse'=> 'boolean'
        ]);
        
        $teamConfig = \App\TeamConfig::get($data['id_mission'], $data['id_class']);
        if($teamConfig){
            abort_if($user->cannot('update', $teamConfig), 403);
            $teamChoice = new TeamChoice($user);
            $reuse = $data['reuse']==='1';
            return response()->json($teamChoice->actionGetFullTeams($teamConfig, $reuse));
        }else{
            return response()->json([ 'missionsToCopy' => \Auth::getUser()->teacher->missionsToCopyForAClass($data["id_class"])]);
        }
        
     }
    
    
    /**
     * Returns the full teams
     *
     * @param  \App\TeamConfig  $teamconfig
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getFullTeams(TeamConfig $teamconfig, Request $request)
    {
        $user = Auth::user();
        abort_if($user->cannot('view', $teamconfig), 403);
        $teamChoice = new TeamChoice($user);
        return response()->json($teamChoice->actionGetFullTeams($teamconfig));
    }

    /**
     * Save the teamconfig settings
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function saveTeamSettings(Request $request)
    {
        $user = Auth::user();

        $teamSettings = $request->validate([
            'id_team_config' => 'int|nullable',
            'id_mission' => 'int|required',
            'id_class' => 'int|required',
            'method' => 'in:0,1,2,3',
            'size_opt' => 'int|nullable',
            'size_max' => 'int|nullable',
            'size_min' => 'int|nullable',
            'teams_max' => 'int|nullable',
            'start_datetime' => 'string|nullable',
            'end_datetime' => 'string|nullable',
            'allow_msg_teacher' => 'boolean',
            'allow_msg_team' => 'boolean',
            'allow_msg_id_class' => 'int|nullable',
            'allow_msg_id_mission' => 'int|nullable',
            'allow_attach_ld' => 'boolean',
            'allow_save_ld' => 'boolean',
            'allow_import' => 'boolean',
            'allow_import_id_mission' => 'int|nullable',
            'name_prefix' => 'string|nullable',
            'email_on_create' => 'boolean',
            'teams' => 'array',
            'teams.*' => 'array',
            'retroactive' => 'boolean|nullable',
            
        ]);
        if ($teamSettings["id_team_config"]) {
            $tc = \App\TeamConfig::find($teamSettings["id_team_config"]);
            abort_if($user->cannot('update', $tc), 403);
        } else {
            $classe = \App\Classe::find($teamSettings["id_class"]);
            abort_if($user->cannot('update', $classe), 403);
        }

        try {
            $teamConfigSaved = TeamConfig::saveOrUpdate((object) $teamSettings);
        } catch (\Exception $e) {
            $out = [
                'message' => __("Erreur en enregistrant les instructions pour les équipes ") . print_r(DB::getPdo()->errorInfo(), true),
                'status' => 'danger',
            ];
            return response()->json($out);
        }

        if (!$teamConfigSaved["teamConfig"]) {
            $out = [
                'message' => __("Une mise en équipe existe déjà, veuillez rafraichir la page."),
                'status' => 'danger',
            ];
            return response()->json($out);
        }


        if (!isset($teamSettings['teams'])) {
            // I do not now why this happens
            $out = [
                'message' => __("Configuration enregistrée, aucune équipe crée."),
                'status' => 'success',
            ];
            Log::Error("Save teamSettings without argument teams");
            return response()->json($out);
        }

        $teams =  array_map(function($t) { return (object) $t; }, $teamSettings['teams']);

        $changes = $teamConfigSaved["teamConfig"]->saveTeams($teams, $teamSettings["retroactive"]);

        if ($changes === null) {
            $out =[
                'message' => _("Erreur en enregistrant les équipes: ") . print_r(DB::getPdo()->errorInfo(), true),
                'status' => 'danger',
            ];
        } elseif ($changes === 0) {
            $out =[
                'message' => __("Configuration enregistrée."),
                'status' => 'success',
            ];
        }  elseif ($changes === 1) {
           $out =[
                'message' => __("Équipes enregistrées : 1 inscription modifiée" ),
                'status' => 'success',
                ];
            }
        else {
            $out =[
                'message' => __("Équipes enregistrées : :changes inscriptions modifiées.",[ 'changes' => $changes] ),
                'status' => 'success',
            ];
        }
        
        if($teamConfigSaved["traceAddingTeamConfig"]){
            Trace::logAction(Trace::TEACHER_ADD_TEAMING, ['id_mission' => (int) $teamConfigSaved["teamConfig"]->id_mission, 'id_class' => (int) $teamConfigSaved["teamConfig"]->id_class]);
        } else {
            if($changes>0 || $teamConfigSaved["traceEditingTemConfig"] ){
                Trace::logAction(Trace::TEACHER_EDIT_TEAMING, ['id_mission' => (int) $teamConfigSaved["teamConfig"]->id_mission, 'id_class' => (int) $teamConfigSaved["teamConfig"]->id_class]);
            }
        }
        
        return response()->json($out);
    }

    /**
     * Update teamconfig teams
     *
     * @param  \App\TeamConfig  $teamconfig
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // Depreceated
    public function saveTeams(TeamConfig $teamconfig, Request $request)
    {
        $user = Auth::user();
        abort_if($user->cannot('update', $teamconfig), 403);
        $data = $request->validate([
            'teams' => 'array',
            'retroactive' => 'boolean|nullable',
        ]);

        $teams = array_map(function($t) { return (object) $t; }, $data['teams']);
        $changes = $teamconfig->saveTeams($teams, $data['retroactive']);

        if ($changes === null) {
            $out =[
                'message' => "Erreur en enregistrant les équipes: " . print_r(DB::getPdo()->errorInfo(), true),
                'status' => 'danger',
            ];
        } elseif ($changes === 0) {
            $out =[
                'message' => "Configuration enregistrée.",
                'status' => 'success',
            ];
        } else {
            $out =[
                'message' => "Equipes enregistrées. $changes inscriptions modifiées.",
                'status' => 'success',
            ];
        }
        return response()->json($out);
    }

    /**
     * 
     * TODO delete when refactoring Team Config
     * Get a teamconfig from class and id_mission
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getFromClasseMission(Request $request)
    {
        $user = Auth::user();
        $data = $request->validate([
            'id_mission' => 'int',
            'id_class' => 'int',
        ]);

        $tc = \App\TeamConfig::get($data['id_mission'], $data['id_class']);
        if ($tc) {
            abort_if($user->cannot('view', $tc), 403);
            $tc = $tc->withoutRelations();
        }
        return response()->json(['teamConfig' => $tc]);
    }

    /**
     * Returns the ids of the started reports
     *
     * @param  \App\TeamConfig  $teamconfig
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function startedReports(TeamConfig $teamconfig, Request $request)
    {
        abort_if(Auth::user()->cannot('view', $teamconfig), 403);

        $ids = $teamconfig->reports()->where('initialized', 1)->pluck('id_report');
        return response()->json($ids);
    }

    /**
     * Returns the ids of the started reports
     *
     * @param  \App\TeamConfig  $teamconfig
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function delete(TeamConfig $teamconfig, Request $request)
    {
        abort_if(Auth::user()->cannot('update', $teamconfig), 403);

        if (!$teamconfig->isExternal()) {
            $teamconfig->delete();
        }
        return response('');
    }

    public function getReportsViews(TeamConfig $teamconfig, Request $request)
    {
        $teamsStarted=[];
        $teamsNotStarted=[];
        $reportsStartdedHTML = view('teacher/teamConfig/_reportStartedFieldset')
            ->with(['teams' => $teamsStarted])->render();

        $reportsNotStartdedHTML = view('teacher/teamConfig/_reportNotStartedFieldset')
            ->with(['teams' => $teamsNotStarted])->render();

        $data = [
            "reportsStartedHTML" => $reportsStartdedHTML,
            "reportsNotStartedHTML" => $reportsNotStartdedHTML
        ];
        return response()->json($data);
    }


}
