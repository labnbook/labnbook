<?php

namespace App\Http\Controllers\Scoring;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use \App\Scoring\Processes\UpdateRubric;
use \App\Scoring\Rubric;
use \App\Trace;

class RubricController
{
    use PermissionTrait;

    /**
     * Retrieves an activity and check that the currently logged user is allowed
     * to act on it
     * @param int $id_activity
     * @param string $verb action that the user must be allowed
     * @return Activty
     */
    private function getActivityAndcheckPerms($id_activity, $verb)
    {
        $user = Auth::user();
        $activity = Rubric::getLinkedModelById('activity', $id_activity);
        abort_if($user->cannot($verb, $activity), 403);
        return $activity;
    }

    /**
     * Get the rubric for the activity
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sections(Request $request, int $id_activity)
    {
        $activity = $this->getActivityAndcheckPerms($id_activity, 'view');
        $sectionClass = config('scoring.linked_model_classes.section');
        $sectionClassPath = explode('\\', $sectionClass);
        $sectionName = end($sectionClassPath) . 's';
        $idName = strtolower('id_' . preg_replace('/([a-z])([A-Z])/', '$1_$2', end($sectionClassPath)));
        $sections = $activity->$sectionName->map(function ($e) use ($idName) {
            return ['id_section' => $e->$idName, 'title' => $e->title];
        });
        return response()->json($sections);
    }

    /**
     * Get the activities linked to the current user and with a rubric
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function getMyLinkedActivities(Request $request)
    {
        $user = Auth::user();
        abort_if($user->cannot('list', Rubric::getLinkedModelClass('activity')), 403);
        return response()->json(Rubric::getUserActivitiesWithRubrics($user));
    }

    /**
     * Get the rubric for the activity
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function get(Request $request, int $id_activity)
    {
        $rubric = $this->getFromLocalLinkAndCheckPerms(Rubric::class, $id_activity, 'view');
        // If we don't disable the following relationship, it will replace the 'assessments' attribute defined by
        // keepOnlyStudentsAssessments (the laravel serialization gives priority to relations over attributes)
        $rubric->unsetRelation('assessments');
        $rubric->keepOnlyStudentsAssessments();
        return response()->json($rubric);
    }

    /**
     * Create or update the rubric of the activity
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id_activity)
    {
        $data = $request->validate([
            'rubric' => 'array',
            'rubric.criteria' => 'array',
            'rubric.criteria_group' => 'array',
            'rubric.id_rubric' => 'int|nullable',
            'rubric.id_activity' => 'int',
            'rubric.grading' => 'numeric',
            'rubric.min_bonus' => 'numeric|max:0',
            'rubric.max_bonus' => 'numeric|min:0',
            'rubric.min_grade' => 'numeric',
            'rubric.max_grade' => 'numeric',
            'rubric.name' => 'string',
            'rubric.description' => 'string|nullable',
            'deletes' => 'array',
            'deletes.descrobs' => 'array',
            'deletes.criteria' => 'array',
            'deletes.criteria_group' => 'array',
            'propagate' => 'boolean',
        ]);
        if($data['rubric']['description'] !== null) {
            $data['rubric']['description'] = \App\HtmlFilter::clean($data['rubric']['description']);
        }
        $criteria = $data['rubric']['criteria'];
        unset($data['rubric']['criteria']);
        $groups = $data['rubric']['criteria_group'];
        unset($data['rubric']['criteria_group']);
        $this->getActivityAndcheckPerms($id_activity, 'update');
        $updateRubric = new UpdateRubric(
            $data['rubric'],
            $groups,
            $criteria,
            $data['deletes'],
            $data['propagate'] ? 1 : 0
        );
        $out = $updateRubric->run();
        $parameters = ['id_mission' => $out['rubric']['id_activity']];
        Trace::logAction(Trace::SCORING_SAVE_GRID, $parameters);
        $out['rubric']->unsetRelation('assessments');
        $out['rubric']->keepOnlyStudentsAssessments();
        return response()->json($out);
    }

    /**
     * delete the rubric by id
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id_rubric)
    {
        $rubric = Rubric::find($id_rubric);
        abort_if(!$rubric, 404);
        abort_if(Auth::user()->cannot('update', $rubric), 403);
        $rubric->keepOnlyStudentsAssessments();
        abort_if(
            !$rubric->assessments->isEmpty(),
            409,
            __('Impossible de supprimer la grille critériée : il existe déjà des évaluations')
        );
        return response()->json($rubric->delete());
    }
}
