<?php

namespace App\Http\Controllers\Scoring;

use \App\Scoring\Assessment;
use \App\Scoring\Rubric;
use \App\Scoring\Processes\UpdateAssessment;
use \App\Scoring\Processes\ComputeGradesStats;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use \App\Trace;

class AssessmentController
{
    use PermissionTrait;

    /**
     * Get the assessment for the production
     * @param Request $request
     * @return JsonResponse
     */
    public function get(Request $request, int $id_production): JsonResponse
    {
        $data = $request->validate([
            'view_as_student' => 'boolean|required',
        ]);
        $assessment = Assessment::fromLocalLink($id_production);
        $id_activity = Assessment::getIdActivity($id_production);
        if (!$assessment) {
            // No assessment, we can create one if and only if we are allowed
            // to view the related production
            $rubric = Rubric::fromLocalLink($id_activity);
            if ($rubric && auth()->user()->can('view', Assessment::getProduction($id_production))) {
                $assessment = Assessment::fromRubric($rubric, $id_production);
            }
        }
        // Check perms will return 404 if $assessment is null
        $this->checkPerms($assessment, 'view');
        $assessment->FetchStudents();
        // Return a cleaned clone of an assessment (ie without any correction) if the user is:
        // - a real student (and the mission designer allowed to broadcast the rubric before the assessment publication)
        // - or a student/teacher who is in view_as_student mode
        $activity = Rubric::getActivity($id_activity);
        $act_as_student = (auth()->user()->cannot('update', $assessment) || $data['view_as_student']);
        if (
            $act_as_student
            && !is_null($activity)
            && $activity->rubric_broadcast
            && !$assessment->published
        ) {
            return response()->json($assessment->cleanSelectionsAndComments());
        } elseif ($act_as_student && !$assessment->published) {
            return response()->json(['message' => __('L’évaluation n’est pas encore publiée')], 404);
        }
        return response()->json($assessment);
    }

    /**
     * Get several assessments
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getGrades(Request $request)
    {
        $data = $request->validate([
            'productions' => 'array',
            'productions.*' => 'int'
        ]);
        $assessments = [];
        $code = null;
        foreach ($data['productions'] as $id_production) {
            $a = Assessment::fromLocalLink($id_production);
            if ($a) {
                $this->checkPerms($a, 'view');
                $assessments[] = $a;
                if ($code === null) {
                    $code = $a->rubric->activity->code;
                }
            }
        }
        $process = new ComputeGradesStats($assessments);
        return view('teacher/report/assessments', ['grades' => $process->run(), 'code' => $code]);
    }


    /**
     * Publish several assessments
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function publishBatch(Request $request)
    {
        $data = $request->validate([
            'productions' => 'array',
            'productions.*' => 'int'
        ]);
        foreach ($data['productions'] as $id_production) {
            $a = Assessment::fromLocalLink($id_production);
            if ($a) {
                $this->checkPerms($a, 'update');
                if (!$a->published) {
                    $a->published = true;
                    $a->save();
                    $parameters = ['id_report' => $a->id_production];
                    Trace::logAction(Trace::SCORING_PUBLISH_EVAL, $parameters);
                }
            }
        }
        return response()->json("OK");
    }

    /**
     * Create or update the assessment of the production
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id_production)
    {
        $data = $request->validate([
            'assessment' => 'array',
            'assessment.bonus' => 'numeric|nullable',
            'assessment.bonus_comment' => 'string|nullable',
            'assessment.score' => 'numeric|nullable',
            'assessment.score_comment' => 'string|nullable',
            'assessment.published' => 'boolean',
            'assessment.criteria' => 'array',
            'assessment.criteria.*.id_criterion' => 'numeric|required',
            'assessment.criteria.*.comment' => 'string|nullable',
            'assessment.criteria.*.activated' => 'boolean',
            'assessment.criteria.*.descrobs' => 'array',
            'assessment.criteria.*.descrobs.*.selected' => 'boolean',
            'assessment.criteria.*.descrobs.*.id_descrob' => 'numeric|required',
            'assessment.assessed_students' => 'array',
            'assessment.assessed_students.*.id_assessed_student' => 'int|required',
            'assessment.assessed_students.*.id_assessment' => 'int|required',
            'assessment.assessed_students.*.id_student' => 'int|required',
            'assessment.assessed_students.*.missing' => 'boolean|required',
            'assessment.assessed_students.*.bonus' => 'numeric|required',
            'assessment.assessed_students.*.bonus_comment' => 'string|nullable',
        ]);


        $students = $data['assessment']['assessed_students'];
        unset($data['assessment']['assessed_students']);

        // Get assessment from id_production and check perms
        $a = $this->getFromLocalLinkAndCheckPerms(Assessment::class, $id_production, 'update');
        $publishing = !$a->published && $data['assessment']['published'];

        // TODO for #1038 only a teacher should be allowed to add an individual bonus
        $updateAssessment = new UpdateAssessment(
            $a,
            $data['assessment'],
            $students,
        );
        $out = $updateAssessment->run();
        $parameters = ['id_report' => $out['assessment']['id_production'], 'score' => $out['assessment']['score']];
        Trace::logAction(Trace::SCORING_SAVE_EVAL, $parameters);
        if ($publishing) {
            $parameters = ['id_report' => $out['assessment']['id_production']];
            Trace::logAction(Trace::SCORING_PUBLISH_EVAL, $parameters);
        }
        return response()->json($out);
    }


    /**
     * Log the fact that a user view the assessment of the production
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function traceView(Request $request, int $id_production)
    {
        $data = $request->validate([
            'who' => 'in:student,corrector'
        ]);
        $a = Assessment::fromLocalLink($id_production);
        if ($a) {
            $user = Auth::user();
            abort_if($user->cannot('view', $a), 403);
            Trace::logAction(Trace::SCORING_VIEW_EVAL, ['id_report' => $id_production]);
        }
    }

    /**
     * Log the fact that a user edit a published the assessment of the production
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function traceEdit(Request $request, int $id_production)
    {
        $a = Assessment::fromLocalLink($id_production);
        if ($a) {
            $user = Auth::user();
            abort_if($user->cannot('update', $a), 403);
            Trace::logAction(Trace::SCORING_EDIT_EVAL, ['id_report' => $id_production]);
        }
    }

    /**
     * Deletes the assessment of the production
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, int $id_production)
    {
        $a = Assessment::fromLocalLink($id_production);
        if ($a) {
            $this->checkPerms($a, 'update');
            $a->published = true;
            $a->delete();
        }
    }
}
