<?php

namespace App\Http\Controllers\Scoring;

trait PermissionTrait
{
    /**
     * Retrieve a Model of the given class from it's local bound object
     * and checks that the current user has the permission of the given verb
     * aborts 404 or 403 on error
     * @param CLASS $CLS
     * @param int $id_local
     * @param string $verb
     * @return CLASS|null
     */
    protected function getFromLocalLinkAndCheckPerms($CLS, $id_local, $verb)
    {
        $obj = $CLS::fromLocalLink($id_local);
        $this->checkPerms($obj, $verb);
        return $obj;
    }

    /**
     * Checks that the given object is not null and the current user can act on
     * it according to $verb
     * aborts with 404 if the object is null, 403 if the user does not have the
     * requested permission
     * @param StdClass $obj
     * @param string $verb
     */
    protected function checkPerms($obj, $verb)
    {
        abort_if(!$obj, 404);
        abort_if(auth()->user()->cannot($verb, $obj), 403);
    }
}
