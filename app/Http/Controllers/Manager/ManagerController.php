<?php

namespace App\Http\Controllers\Manager;

use App\Classe;
use App\Http\Controllers\Controller;
use App\Institution;
use App\Teacher;
use App\TeacherTeam;
use App\Trace;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class ManagerController extends Controller
{

    /*********************************************************************************
     *         Teacher team
     *********************************************************************************/

    /**
     * Display the list of the team of the user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        $page = $request->path();
        \App\Teacher::enterInterface($user->id_user, $page);

        abort_if($user->cannot('list', TeacherTeam::class), 403);

        return view('manager/teacherTeam/index')->with([
            'user' => $user,
            'inst' => $user->institution,
        ]);
    }

    /**
     * Get the teacher teams manage by user
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getTeacherTeams(Request $request)
    {
        $user = Auth::user();
        abort_if($user->cannot('list', TeacherTeam::class), 403);
        
        return response()->json(['data' => $user->teacher->getTeacherTeamWithNbTeacher()]);
    }
  
    /**
     * Get the view for creating / editing teacherTeam
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
    */
    public function initEdit(Request $request)
    {
        $user = Auth::user();
        $teacher = $user->teacher;
        $id_teacher_team = (int)$request->input('id_teacher_team');
    
        if ($id_teacher_team) {
            // Updating Team
            $teacher_team = \App\TeacherTeam::find($id_teacher_team);
            abort_if($user->cannot('update', $teacher_team), 403);

            $institutions = [$teacher_team->institution];
            $teachers = $teacher_team->teachers()
                ->join('user', 'user.id_user', 'teacher.id_teacher')
                ->select('user.id_user', 'user.first_name', 'user.user_name')
                ->get();
            $id_inst = $teacher_team->id_inst;
        } else {
            // Creating a teacher team
            abort_if($user->cannot('create', TeacherTeam::class), 403);
            $teacher_team =new \App\TeacherTeam();
            $institutions = $teacher->institutions;
            $id_inst = $institutions[0]->id_inst;
            $teachers = [$user];
        }

        $instHTML = view('manager/teacherTeam/_institutionOptions')
            ->with(['institutions' => $institutions])->render();

        $classesHTML = view('manager/teacherTeam/_classesSpan')
            ->with([
                'classes' => $teacher_team->classes,
            ])->render();
        $missionsHTML = view('manager/teacherTeam/_missionsSpan')
            ->with([
                'missions' => $teacher_team->missions,
            ])->render();

        $teachers_options = [];
        $teachers_ids = [];
        foreach ($teachers as $u) {
            $teachers_ids [] = $u->id_user;
        }

        $data = [
            "teacher_team_name" => $teacher_team->name,
            "institutionsHTML" => $instHTML,
            "classesHTML" => $classesHTML,
            "missionsHTML" => $missionsHTML,
            "teachers" => $teachers_ids,
            "currentUser" => $user,
        ];

        return response()->json($data);
    }

    /**
     * addOrUpdate a teacher team
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addOrUpdate(Request $request)
    {
        $data = $request->validate([
            'inst' => 'int|required',
            'name' => 'string|required',
            'newTeachers' => 'JSON',
            'newTeachers.*' => 'int',
            'deletedTeachers' => 'JSON',
            'deletedTeachers.*' => 'int',
            'id_teacher_team' => 'nullable|int',
            'updateLink' => 'in:true,false'
        ]);
        
        $user = Auth::user();
        if( $data["id_teacher_team"]){
            $teacher_team = \App\TeacherTeam::find($data["id_teacher_team"]);
            abort_if($user->cannot('update', $teacher_team), 403);
        } else {
            abort_if($user->cannot('create', TeacherTeam::class), 403);
        }

        try {
            return response()->json(TeacherTeam::addOrUpdate(
                $data["name"],
                $data["inst"],
                Auth::user()->teacher,
                $data["id_teacher_team"],
                json_decode($data["newTeachers"]),
                json_decode($data["deletedTeachers"]),
                $data["updateLink"]
            ));
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }
    
    /**
     * return a teacher from his ldap login or email
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getUserFromLdap(Request $request)
    {
        $data = $request->validate([
            'query' => 'string|required',
        ]);
        abort_if(Auth::user()->cannot('grantTeacher', TeacherTeam::class), 403);

        try {
            $ldap = new \App\Processes\Ldap\Ldap();
            $user = $ldap->getUserByUsername($data['query']);
        } catch (\Exception $e) {
            return response()->json($e->getMessage());
        }
        if (!$user) {
            $user = $ldap->getUserByEmail($data['query']);
        }
        if ($user) {
            return response()->json($ldap->mapLdapUserToUser($user));
        }
        return response()->json(null);
    }

    /**
     * Creates a teacher from its Ldap login
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addTeacherFromLdap(Request $request)
    {
        $data = $request->validate([
            'login' => 'string|required',
            'sendMail' => 'string|required',
            'id_inst' => 'int|nullable',
        ]);

        $logged_user = Auth::user();
        abort_if($logged_user->cannot('grantTeacher', TeacherTeam::class), 403);
        $id_inst = $data['id_inst'] ?? $logged_user->id_inst;
        abort_if($id_inst != $logged_user->id_inst && !$logged_user->isAdmin(), 403);

        // First look for user locally
        $user = \App\User::Where('login', $data['login'])->internal(false)->first();
        $create = false;
        if (!$user) {
            // The user does not exists, look in ldap
            $ldap = new \App\Processes\Ldap\Ldap();
            $create = true;
            $ldapUser = $ldap->getUserByUsername($data['login']);
            if (!$ldapUser) {
                // Login not in Ldap, should not happen except if the user changed 'login' since the last ajax
                return response()->json(__("Impossible de trouver l'utilisateur :user dans l'annuaire", ['user' => $data['login']]), 409);
            }
            $user = $ldap->mapLdapUserToUser($ldapUser);
            if (!$user->save()) {
                abort(500, __("Impossible d'enregistrer l'utilisateur. Veuillez contacter l'administrateur."));
            }
            if ($data['sendMail'] !== 'false') {
                \App\User::sendInscriptionEmail($user->email, $user->login, null);
            }
        }
        // Now $user is in the database, we can grant him teacher rights
        $comment = __("Inscription manuelle par un manager");
        $user->grantTeacher([], $comment);
        $message = "";
        if ($user->id_inst != $id_inst) {
            $message = __(
                "L'enseignant possède déjà un compte dans l'institution :add_inst il n'apparaitra pas l'instituion :our_inst",
                [
                    "add_inst" => \App\Institution::find($user->id_inst)->name,
                    "our_inst" => \App\Institution::find($id_inst)->name
                ]
            );
        }
        return response()->json(["create" => $create, "msg" => $message]);
    }

    /**
     * Return the teachers from the teacherTeam
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getTeachersFromTeacherTeam(Request $request)
    {
        $data = $request->validate([
            'id_teacher_team' => 'int|required'
        ]);
        
        $teacher_team = \App\TeacherTeam::find($data["id_teacher_team"]);
        abort_if(Auth::user()->cannot('list', $teacher_team), 403);
        
        return response()->json($teacher_team->teachers->map(
            function ($t) {
                return [
                    'id_teacher' => $t->id_teacher,
                    'name' => $t->user->longDisplayName(false),
                ];
            }
        ));
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, TeacherTeam $teacherTeam)
    {
        abort_if(Auth::user()->cannot('delete', $teacherTeam), 403);
        $teacherTeam->delete();
        return response()->json(true);
    }

    public function duplicate(Request $request, TeacherTeam $teacherTeam)
    {
        abort_if(Auth::user()->cannot('create', $teacherTeam), 403);
        $teacherTeam->duplicate();
        return response()->json(true);
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function archive(Request $request, TeacherTeam $teacherTeam)
    {
        abort_if(Auth::user()->cannot('update', $teacherTeam), 403);
        return response()->json($teacherTeam->archive());
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function unarchive(Request $request, TeacherTeam $teacherTeam)
    {
        abort_if(Auth::user()->cannot('update', $teacherTeam), 403);
        return response()->json($teacherTeam->unarchive());
    }

    /*********************************************************************************
     *         Teacher 
     *********************************************************************************/
    
    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * Grant the teacher rights for id_user. Do nothing if user 
     */
    public function grantTeacherRights(Request $request)
    {
        $data = $request->validate([
            'id_user' => 'int|required'
        ]);

        $user = \App\User::find($data["id_user"]);
        abort_if(Auth::user()->cannot('grantTeacher', TeacherTeam::class), 403);

        try {
            $result=$user->grantTeacher(array(Auth::user()->id_inst));
            if($result){
                Trace::logAction(Trace::MANAGER_ADD_TEACHER_STATUS, ['id_teacher' => $data["id_user"] ]);
            }
            return response()->json($result);
        } catch (Exception $e){
            return __("Erreur lors de l'ajout du statut enseignant.") ;
        }
    }
    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * Grant the teacher rights for id_user. Do nothing if user
     */
    public function removeTeacherRights(Request $request)
    {
        $data = $request->validate([
            'id_teacher' => 'int|required'
        ]);

        abort_if(Auth::user()->cannot('removeTeacher', TeacherTeam::class), 403);
        $teacher = Teacher::find($data["id_teacher"]);
        abort_if($teacher->missions()->count()==0 &&  $teacher->classes()->count(), 403, __("L'enseignant est lié à au moins une classe ou une mission"));
      
        try {
            $return =  Teacher::find($data["id_teacher"])->delete();
            if($return){
                Trace::logAction(Trace::MANAGER_REMOVE_TEACHER_STATUS, ['id_teacher' => $data["id_teacher"] ]);
            }
            return response()->json($return);
        } catch (\Exception $e) {
            return __("Erreur lors de la suppression du statut enseignant.") ;
        }
      
    }

    public function addTeacher(Request $request)
    {
        abort_if(Auth::user()->cannot('addTeacher', TeacherTeam::class), 403);
        $data = $request->validate([
            'user_name' => 'string',
            'first_name' => 'string',
            'email' => 'string|nullable',
            'login' => 'string',
            'password' => 'string|nullable',
            'send_mail' => 'in:true,false',
            'id_inst' => 'int|nullable',
        ]);
        $data['email'] = $data['email'] ?? "";
        $user = Auth::user();
        $id_inst = $data['id_inst'] ?? $user->id_inst;
        abort_if($id_inst != $user->id_inst && !$user->isAdmin(), 403);

        $institution = Institution::find($id_inst);

        $classe = $institution->teacherClasse;
        if (!$classe) {
            abort(409, __("Il est nécessaire d'associer une classe à l'institution pour l'ajout d'enseignants"));
        }
        
        $id_new_user = \App\User::addOrUpdate(
            0,
            $classe,
            $data['user_name'],
            $data['first_name'],
            $id_inst,
            null,
            $data['email'],
            $data['login'],
            $data['password'],
            $data['send_mail']
        );
        if (is_numeric($id_new_user)){
            $user = User::find($id_new_user);
            $user->grantTeacher();
            Trace::logAction(Trace::MANAGER_ADD_TEACHER_STATUS, ['id_teacher' => $id_new_user ]);
        }
       

        return response()->json($id_new_user);
    }
    
    public function getTeacherWithInstAndLinkBy(Request $request)
    {
        abort_if(Auth::user()->cannot('list', TeacherTeam::class), 403);
        $data = $request->validate([
            'id_inst' => 'int|required',
            'search' => 'string|nullable',
        ]);
        return response()->json(Teacher::getTeacherWithInstAndLinkBy( $data['id_inst'], $data['search']));

    }
    
}
