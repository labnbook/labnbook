<?php

namespace App\Http\Controllers;

use App\Classe;
use App\Conversation;
use App\Report;
use App\Trace;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;

class ConversationController extends Controller
{
    /**
     * Display the conversation list
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        $filter = $request->input('filter');
        $id_report_context = (int)$request->input('id_report_context');
        $conversations = Conversation::getConversationList($filter, $id_report_context, $user);
        return view('components.widgets.message._conversations')->with([
            'conversations' => $conversations,
            'id_report_context' => $id_report_context,
            'user' => $user,
        ]);
    }

    /**
     * Display a list of labdoc to send
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function displayLD2Send(Request $request)
    {
        $id_report_context = (int)$request->input('id_report_context');
        $report = \App\Report::find($id_report_context);
        $lds = null;
        if ($report) { // select the ld of the report
            $lds = $report->labdocs()
                ->join('report_part', 'labdoc.id_report_part', '=', 'report_part.id_report_part')
                ->whereNull('deleted')
                ->orderBy('report_part.position')
                ->orderBy('labdoc.position')
                ->get();
            $shared_lds = $report->sharedLabdocs()
                ->join('report_part', 'labdoc.id_report_part', '=', 'report_part.id_report_part')
                ->whereNull('deleted')
                ->orderBy('report_part.position')
                ->orderBy('labdoc.position')
                ->get();
            $lds = $lds->concat($shared_lds);
        } 
        return view('components.widgets.message._ld2Send')->with(['lds' => $lds]);
    }

    /**
     * Should we add the save ld buttons in the conversation
     * @param \App\User $user
     * @param \App\Conversation $conversation
     * @param string $scope
     * @param int $id_report_context
     */
    private function allowSaveLd($user, $conversation, $scope, $id_report_context)
    {
        $allow_save_ld = 0;
        $report = \App\Report::find($id_report_context);
        if ($report) {
            if ($scope === "follow") {
                $allow_save_ld = $user->can('manage', $report) ? 1 :0;
            } elseif ($scope === "default" && $id_report_context) { // check if the user can save LD in the context of his report
                $allow_save_ld = $user->can('update', $report) ? $report->allow_save_ld : 0;
            }
        }
        return $allow_save_ld;
    }

    /**
     * Display a specific conversation
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $user = Auth::User();
        /** @var string $title */
        $title = $request->input('title');
        /** @var string $new_part_ids Participants: list of id_user separated by "," */
        $new_part_ids = $request->input('new_part_ids');
        /**
         * @param int $id_report_context report.id_report if in a report contect OR 0 if in a teacher context
         */
        $id_report_context = (int) $request->input('id_report_context');

        /** @var \App\Conversation $conversation */
        $conversation = Conversation::find((int) $request->input('id_conv'));

        $id_report_conv = (int) $request->input('id_report_conv');
        $scope = $request->input('scope');

        // INSERTS AND UPDATES
        if ($new_part_ids) {
            $new_participants = \App\User::distinct()->whereIn('id_user', str_replace("'", "", explode(",", $new_part_ids)))->where('id_user', '<>', $user->id_user)->get();
            if($conversation) {
                // Remove participants that are already in conversation from the new list
                $new_participants = $new_participants->diff($conversation->users);
            }
        } // new participants collection

        $trace = Trace::OPEN_CONVERSATION;
        if (!$conversation) { // the conversation must be created + get the new id_conv
            // creator of the conversation
            $conversation = Conversation::init($user, $id_report_conv ? $id_report_conv : $id_report_context);
            $trace = Trace::ADD_CONVERSATION;
        } elseif ($new_part_ids) { // the conversation already exists : creation of welcome messages
            $conversation->welcomeUsers($new_participants);
        }

        if ($new_part_ids) { // Update the participants
            $conversation->addUsers($new_participants);
        }
        if ($title) { // Update the title
            $conversation->title = $title;
            $conversation->save();
        } else {
            $title = $conversation->title ;
        }
        if ($title) { $title = $title." - "; }

        // GET INFO
        if ($conversation->report && $conversation->report->mission) { // conversation initiated in a mission context
            $mission_code = $conversation->report->mission->code;
            $mission_tag = $conversation->missionTag();
            $id_report_conv = $conversation->report->id_report ;
        } else { // conversation initiated without mission context
            $mission_code = NULL;
            $mission_tag = "" ;
            $id_report_conv = NULL ;
        }


        // traces
        $attributes= [
            'id_conversation' => $conversation->id_conv,
            'conv_mission_code' => $mission_code,
            // with someone who is a teacher
            'teachers' => (boolean) $conversation->users()
                                                 ->join('teacher', 'user.id_user', '=', 'teacher.id_teacher')
                                                 ->exists(),
            // with someone who works on the same report as the current report (if existing)
            'team_mates' => $id_report_context ?
            (boolean) $conversation->users()
                                   ->join('link_report_learner as lrl', 'user.id_user', '=', 'lrl.id_user')
                                   ->where('lrl.id_report', $id_report_context)
                                   ->exists()
            : false,
            // with someone who is not a teacher, and does not work on the current report (if existing)
            'other_students' => $id_report_context ?
            (boolean)
            $conversation->users()
                         ->leftJoin('link_report_learner as lrl', function ($join) use ($id_report_context) {
                             $join->on('link_conversation_user.id_user', '=', 'lrl.id_user')
                                  ->where('lrl.id_report', $id_report_context);
                         })->leftJoin('teacher as t', function ($join) {
                             $join->on('link_conversation_user.id_user', '=', 't.id_teacher')
                                  ->whereNull('lrl.id_user')
                                  ->whereNull('t.id_teacher');
                         })->exists()
                             : (boolean) $conversation->users()
                                                      ->leftJoin('teacher as t', 'link_conversation_user.id_user', '=', 't.id_teacher')
                                                      ->whereNull('t.id_teacher')
                                                      ->exists(),
            'id_report' => $id_report_context,
        ];
        Trace::logAction($trace, $attributes);
        $conversation->updateLastSeen($user->id_user);

        return view('components.widgets.message._conversation')->with([
            'conv' => $conversation,
            'id_report_context' => $id_report_context,
            'mission_tag' => $conversation->missionTag(),
            'mission_code' => $mission_code,
            'id_report_conv' => $id_report_conv,
            'user' => $user,
            'scope' => $scope,
            'allow_save_ld' => $this->allowSaveLd($user, $conversation, $scope, $id_report_context)
        ]);
    }

    /**
     * Edit the participants of a conversation
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function editParticipants(Request $request)
    {
        $id_conv = (int) $request->input('id_conv');
        $id_report_conv = (int) $request->input('id_report_conv');
        $title = $request->input('title');
        $id_report_context = (int) $request->input('id_report_context');
        $scope = $request->input('scope');
        $user = Auth::user();

        if ($id_conv) { // existing conversation
            $conv = Conversation::find($id_conv);
            $id_report_conv = $conv->id_report;
            if (!$id_report_conv) {
                $id_report_conv = 0;
            }
        } else { // conversation creation
            $conv = null;
            if (!$id_report_conv && $scope && $id_report_context) { // in the scope of a report
                $id_report_conv = $id_report_context;
            } elseif (!$id_report_conv) { // creation of a generic conversation by a teacher
                $id_report_conv = 0;
            } // else : creation of a contextualized conversation by a teacher (through the Reports table)
        }

        // Adding participants
        $can_add = false;
        $teachers = false;
        $team = false;
        $allow_msg_id_class = null;

        if ($id_report_context && $id_report_conv) { // The widget message is used in a report and the conversation is not generic
            $can_add = true;
            $msg_opt = \App\Report::find($id_report_conv);
            if ($msg_opt) {
                // Teachers' list
                $teachers = $scope == 'follow' || $msg_opt->allow_msg_teacher;
                $team = $scope == 'follow' || $msg_opt->allow_msg_team;
                $allow_msg_id_class = ($scope == 'follow' || $msg_opt->allow_msg_id_class)?$msg_opt->allow_msg_id_class:0;
                $mission = ($scope == 'follow' || $msg_opt->allow_msg_id_mission);
            }
        } elseif ($user->getRole() != 'learner') { // The widget message is used by a TEACHER
            $params = [':id_teacher' => (int)$user->id_user];
            $can_add = true;
        }
        return view('components.widgets.message._addParticipantsList')->with([
            'id_conv' => $id_conv,
            'title' => $title,
            'conv' => $conv,
            'scope' => $scope,
            'id_report_conv' => $id_report_conv,
            'user' => $user,
            'can_add' => $can_add,
            'teachers' => $teachers,
            'team' => $team,
            'allow_msg_id_class' => $allow_msg_id_class
        ]);
    }
    
    /**
     * Get a list of user by role in ('team', 'report')
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function getParticipantsByRole(Request $request)
    {
        $data = $request->validate([
            'id_report' => 'nullable|int',
            'id_class' => 'nullable|int',
            'id_conversation' => 'nullable|int',
            'role' => 'required|string',
            'for_teacher' => 'required|boolean',
        ]);
        $userConversationIds = [];
        if (array_key_exists('id_conversation', $data)) {
            $conversation = Conversation::find($data['id_conversation']);
            if($conversation) {
                $userConversationIds = $conversation->users()->pluck('user.id_user')->toArray();
            }
        }
        $userList = [];
        $raw_select_name = $data['for_teacher']?
            'CONCAT(user.user_name, " ", user.first_name) as name':'CONCAT(user.first_name, " ", user.user_name) as name';
        if (array_key_exists('id_report', $data) && in_array($data['role'], ['team', 'teacher'])) {
            $report = Report::find($data['id_report']);
            if ($report) {
                if ($data['role'] === 'team') {
                    $userList = $report->users()
                        ->whereNotIn('user.id_user', $userConversationIds)
                        ->select(['user.id_user',
                            DB::raw($raw_select_name),
                            DB::raw('0 as connected'),
                        ])->orderBy('name')->get();
                } else if($data['role'] === 'teacher') {
                    $userList = $report->teachers()
                        ->join('user', 'user.id_user', '=', 'teacher.id_teacher')
                        ->whereNotIn('user.id_user', $userConversationIds)
                        ->select(['user.id_user',
                            DB::raw($raw_select_name),
                            DB::raw('0 as connected'),
                        ])->orderBy('name')->get();
                }
            }
        } else if(array_key_exists('id_class', $data) && $data['role'] === 'class') {
            $class = Classe::find($data['id_class']);
            if($class) {
                $userList = $class->users()
                    ->whereNotIn('user.id_user', $userConversationIds)
                    ->select(['user.id_user',
                        DB::raw($raw_select_name),
                        DB::raw('0 as connected'),
                    ])->orderBy('name')->get();
            }
        }
        return response()->json(['userList' => $userList]);
    }

    /**
     * Get the paginated list of teacher classes
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getClasses(Request $request)
    {
        $data = $request->validate([
            'id_conv' => 'required|int',
            'search' => 'string|nullable',
            'page' => 'int|nullable',
            'pageSize' => 'int|nullable',
        ]);
        $user = Auth::user();
        if($data['id_conv'] !== '0') {
            // Case teacher modify a conversation 
            abort_if(!$user->conversations()->pluck('conversation.id_conversation')->contains($data['id_conv']),403);
        }
        $searchTerm = $data['search']??'';
        $paginatedClasses = $user
            ->teacher
            ->classes()
            ->select(['class.id_class', 'class.id_class as id', 'class.class_name', 'class.class_name as text', 'class.status'])
            ->where('class.class_name', 'like', '%' . $searchTerm . '%')
            ->orderBy('class.class_name')
            ->paginate($data['pageSize'], ['*'], 'users', $data['page']);
        return response()->json(['classes' => $paginatedClasses->items(), 'searchTerm' => $searchTerm,
            'pagination' => ['page' => $paginatedClasses->currentPage(), 'more' => $paginatedClasses->hasMorePages()]]);
    }

    /**
     * Adds a message to a conversation
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Conversation  $conversation
     * @return \Illuminate\Http\Response
     */
    public function addMessage(Request $request, Conversation $conversation)
    {
        /** @var int $id_report_context used to know if a LD can be SAVED - null if the msg widget is opened in a teacher context */
        $id_report_context = $request->input('id_report_context');
        /** @var string $msgText texte du message : chaine vide si il n'y a pas de nouveau message ou si un labdoc est envoyé */
        $msgText = $request->input('new_msg');
        /**
         * @param int $id_ld nouveau LD attache : 0 si il n'y a pas de LD attaché
         */
        $id_ld = (int) $request->input('id_ld');

        $scope = $request->input('scope');
        $user = Auth::user();
        // Add a new message
        if ($msgText) {
            $conversation->addMessage($user->id_user, $msgText);
        }

        // Add a new attached LD
        if ($id_ld) {
            $new_ld = \App\Labdoc::duplicate((int) $id_ld, null) ; // NULL car on crée le LD attaché à un message
            $conversation->addMessage($user->id_user, $new_ld->name, $new_ld->id_labdoc);
        }
 

        // mise a jour de la derniere fois où l'utilisateur a consulté la conversation
        $conversation->updateLastSeen($user->id_user);

        if ($msgText || $id_ld) { // TRACE
            // get the context mission of the conversation
            $mission = $conversation->report ? $conversation->report->mission : null;
            if ($id_report_context) {
                $id_report_context = (int) $id_report_context;
            } elseif ($mission) {
                $id_report_context = (int) $conversation->report->id_report; // for teachers, there is no report_context : the context is given by the conversation_context
            } else {
                $id_report_context = 0;
            }
            $attributes = [
                'id_conversation' => (int) $conversation->id_conversation,
                'conv_mission_code' => $mission ? $mission->code : null,
                'attached_ld' => $id_ld ? (int) $id_ld : null,
                'id_report' => $id_report_context,
            ];
        } else {
            $attributes= [];
        }
        Trace::logAction(Trace::ADD_MESSAGE, $attributes);

        return view('components.widgets.message._messages')->with([
            'conv' => $conversation,
            'id_report_context' => $id_report_context,
            'user' => $user,
            'scope' => $scope,
            'allow_save_ld' => $this->allowSaveLd($user, $conversation, $scope, $id_report_context)
        ]);
    }

     /**
     * Remove a conversation
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Conversation  $conversation
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, Conversation $conversation)
    {
        $conversation->remove(Auth::user());
        return $this->index($request);
    }

    /**
     * Retrieve the new messages for the current user
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function fetchMessages(Request $request)
    {
        $user = Auth::user();
        $user->last_synchro = time();
        $user->save();

        $XML_doc = new \App\XMLDoc();
        $XML_doc->loadXML("<modification><message></message></modification>");
        $result = \App\Conversation::getConvWithNewMsg($user->id_user);
        foreach ($result as $conv) {
            $snapshot = \App\Message::getSnapshotFromMsg($conv->msg_content);
            $XML_doc->addXMLNode("message", "conv", $conv->id_conversation, $snapshot);
        }
        return response($XML_doc->saveXML());
    }
}
