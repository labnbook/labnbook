<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Views\Widgets\Widget;
use Illuminate\Support\Facades\Auth;

class WidgetController extends Controller
{
    /**
     * Save a widget params.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function saveParams(Request $request)
    {
        Widget::saveParams(
            Auth::Id(),
            $request->input('widg'),
            (int) $request->input('x'),
            (int) $request->input('y'),
            (int) $request->input('h'),
            (int) $request->input('w'),
            (int) $request->input('vis'),
        );
    }
}
