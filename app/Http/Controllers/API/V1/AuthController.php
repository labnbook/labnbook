<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Log;

class AuthController extends Controller
{
    /**
     * Get a JWT for requested user.
     * The auth api is not set for this method
     * The jwtserver middleware MUST be called before using this method
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $ext_user = $request->input('id_user_ext');
        $ext_provider = (int) $request->input('id_extplatform');
        $user = User::fromExternal($ext_user, $ext_provider);
        if(!$user){
            return response()->json(['message' => 'User not linked to external platform'], 403);
        }
        // Manual login as method login is not provided by api auth Guard
        auth('api')->setUser($user);
        return response()->json();
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        // Ask JWTServer middleware to logout
        return response()->json(['logout'=>true,'message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return response()->json();
    }

    /**
     * Test if LabNBook is running
     * @return \Illuminate\Http\JsonResponse
     */
    public function status()
    {
        $status = "FAIL";
        try {
            if (\App\User::exists()) {
                $status = "OK";
            }
        } catch (\Exception $e) {
            Log::alert($e);
        }
        return response()->json($status);
    }
}
