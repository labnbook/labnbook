<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use \App\Extplatform;

class ExtplatformController
{
    /**
     * Update the extplatform infos
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $data = $request->validate([
            'id_extplatform' => 'int|required',
            'plugin_version' => 'string|required',
            'platform_version' => 'string|required',
        ]);
        $extplatform = Extplatform::find($data['id_extplatform']);
        $extplatform->fill($data);
        $extplatform->save();
    }
}
