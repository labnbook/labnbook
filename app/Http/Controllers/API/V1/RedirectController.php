<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class RedirectController extends Controller
{
    /**
     * TODO Init this value from the configuration
     * @var string The root URL used for redirecting. No trailing slash.
     */
    protected static $webRootUrl = '';

    /**
     * Redirect to a web route forwarding authentication and the contents of 'forward'
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $path = $request->input('path');
        if (empty($path)) {
            abort(402, "The 'path' parameter is missing or empty.");
        }

        // forward data
        $session = (array) $request->query('forward', []);
        $session['id_extplatform'] = (int) $request->input('id_extplatform');
        $session['id_user_ext'] = $request->input('id_user_ext');
        $session['allowed_by_api'] = str_replace('/?', '?', $path);
        $authentication = new \Stdclass;
        if (auth('api')->check()) {
            $ext_user = (int) $request->input('id_user_ext');
            $ext_provider = $request->input('id_extplatform');
            $user = User::fromExternal($ext_user, $ext_provider);
            $authentication->id_user = $user->id_user;
        }
        $payload = encrypt((object) [
                'path' => $path,
                'authentication' => $authentication,
                'session' => $session,
        ]);

        return redirect(self::$webRootUrl . "/redirect?payload=$payload");
    }
}
