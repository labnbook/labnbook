<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\TeamConfig;

class TeamconfigController extends Controller
{
    /**
     * Get a teamconfig.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Teamconfig  $teamconfig
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Teamconfig $teamconfig)
    {

        $user = Auth::user();
        if ($user->cannot('view', $teamconfig)) {
            return response()->json(['message' => 'Permission denied'], 403);
        }
        return response()->json($teamconfig);
    }

    /**
     * Get a teamconfig.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Teamconfig  $teamconfig
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, Teamconfig $teamconfig)
    {
        if ($teamconfig->startedReports()->count() === 0) {
            return response()->json($teamconfig->delete());
        }
        return response()->json(false);
    }

    /**
     * Returns the number of started reports for a teamconfig
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Teamconfig  $teamconfig
     * @return \Illuminate\Http\Response
     */
    public function countStartedReports(Request $request, TeamConfig $teamconfig)
    {
        if (Auth::user()->cannot('view', $teamconfig)) {
            return response()->json(['message' => 'Permission denied'], 403);
        }
        return response()->json($teamconfig->startedReports()->count());
    }

    /**
     * Returns the number of submitted reports for a teamconfig
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Teamconfig  $teamconfig
     * @return \Illuminate\Http\Response
     */
    public function countSubmittedReports(Request $request, TeamConfig $teamconfig)
    {
        if (Auth::user()->cannot('view', $teamconfig)) {
            return response()->json(['message' => 'Permission denied'], 403);
        }
        return response()->json($teamconfig->SubmittedReports()->count());
    }
}
