<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use App\User;

class UserController extends Controller
{
    /**
     * Return list of user with the desired email address
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $users = User::where('email', $request->input('email'))->get();
        return response()->json($users);
    }

    /**
     * Return a GDPR dump in json format for each user
     *
     * @param \Illuminate\Http\Request $request the request
     *
     * @return \Illuminate\Http\Response
     */
    public function exportData(Request $request)
    {
        $data = $request->validate(
            [
            'id_extplatform' => 'int|required',
            'user' => 'int|required',
            ]
        );
        $user = User::fromExternal($data['user'], $data['id_extplatform']);
        if (!$user) {
            return response()->json(['message' => 'User not linked to external platform'], 403);
        }
        $gdpr = new \App\Processes\GDPR($user, 'raw');
        return response()->json($gdpr->generateDump());
    }

    /**
     * Deletes user data for teamconfigs
     *
     * @param \Illuminate\Http\Request $request the request
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteData(Request $request)
    {
        $data = $request->validate(
            [
            'id_extplatform' => 'int|required',
            'user' => 'int|required',
            'teamconfigids' => 'array|required',
            'teamconfigids.*' => 'int|required',
            ]
        );
        $user = User::fromExternal($data['user'], $data['id_extplatform']);
        if (!$user) {
            return response()->json(['message' => 'User not linked to external platform'], 403);
        }
        // Get all user reports for TC
        $reports = $user->reports()
                        ->where('id_team_config', $data['teamconfigids'])
                        ->get();
        foreach ($reports as $r) {
            // Remove conversations messages
            foreach ($r->conversations as $c) {
                $c->messages()
                  ->where('id_user_sender', $user->id_user)
                  ->update(['msg_content' => __("Message supprimé")]);
            }
            // Remove labdocs comments
            foreach ($r->labdocs as $l) {
                $l->comments()
                  ->where('id_user', $user->id_user)
                  ->update(['content' => __("Message supprimé")]);
            }
            // Remove user from classe
            $r->teamConfig->classe->users()->detach($user);
            // Remove user from report
            $r->users()->detach($user);
        }
        // Unbind user
        $user->unbind($data['id_extplatform']);
        return response()->json(true);
    }

    /**
     * Get all linked users for teamconfigs
     *
     * @param \Illuminate\Http\Request $request the request
     *
     * @return \Illuminate\Http\Response
     */
    public function fromTeamConfigIds(Request $request)
    {
        $data = $request->validate(
            [
            'id_extplatform' => 'int|required',
            'teamconfigids' => 'array|required',
            'teamconfigids.*' => 'int|required',
            ]
        );
        $query = Extplatform::find($data['id_extplatform'])
            ->users()
            ->select('link_extplatform_user.id_user_ext');
        //   1. Teachers linked to tc mission and linked to extplatform
        $ClTeachers = $query->clone
                            ->join('link_class_teacher', 'link_class_teacher.id_teacher', 'user.id_user')
                            ->join('team_config', 'team_config.id_class', 'link_class_teacher.id_class')
                            ->whereIn('team_config.id_team_config', $data['teamconfigids'])
                            ->distinct()
                            ->get();
        //   2. Teachers linked to tc class and linked to extplatform
        $MiTeachers = $query->clone
                            ->join('link_mission_teacher', 'link_mission_teacher.id_teacher', 'user.id_user')
                            ->join('team_config', 'team_config.id_mission', 'link_mission_teacher.id_mission')
                            ->whereIn('team_config.id_team_config', $data['teamconfigids'])
                            ->distinct()
                            ->get();
        //   3. Students linked to tc reports and linked to extplatform
        $students = $query->clone
                          ->join('link_report_learner', 'link_report_learner.id_user', 'user.id_user')
                          ->join('report', 'report.id_report', 'link_report_learner.id_report')
                          ->whereIn('report.id_team_config', $data['teamconfigids'])
                          ->distinct()
                          ->get();
        //   Return id_user_ext from link_extplatform user
        $external_ids = $ClTeachers->union($MiTeachers)->union($students)->unique();
        return response()->json($external_ids);
    }


    /**
     * Make a user teacher
     */
    public static function grantteacher(Request $request)
    {
        $user = Auth::user();
        $id_extplatform = (int) request()->input('id_extplatform');
        $extplatform = $user->extplatforms()->find($id_extplatform);
        if (!$extplatform) {
            return response()->json(['message' => 'Not allowed'], 403);
        }
        $user->grantTeacher([$extplatform->id_inst], 'auto-inscription API');
        return response()->json(['message' => 'ok']);
    }
}
