<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Classe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ClasseController extends Controller
{
    /**
     * Updates the participants of a classe
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateParticipants(Request $request)
    {
        if (Auth::user()->cannot('listExt', Classe::class)) {
            return response()->json(['message' => 'Not allowed'], 403);
        }
        $data = $request->validate([
            'id_extplatform' => 'int|required',
             'courseid' => 'int|required',
             'groupid' => 'int|required',
             'participants' => 'array|nullable',
             'participants.*' => 'int',
        ]);
        $classe = Classe::fromExternal($data['courseid'], $data['id_extplatform'], $data['groupid']);
        $participants = (array)$data['participants'];
        if (Auth::user()->cannot('update', $classe)) {
            return response()->json(['message' => 'Not allowed'], 403);
        }
        $ret = $classe->updateExternalParticipants($participants, $data['id_extplatform']);
        return response()->json($ret);
    }
}
