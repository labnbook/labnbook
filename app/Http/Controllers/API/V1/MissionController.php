<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Mission;
use App\Classe;
use App\TeamConfig;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class MissionController extends Controller
{
    /**
     * List the missions of a teacher.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        if ($user->cannot('list', Mission::class)) {
            return response()->json(['message' => 'Permission denied'], 403);
        }
        $missions = $user->teacher->missions()
                         ->select(['mission.id_mission','mission.code',
                             'mission.name','mission.description'])
                         ->where('status', '<>', 'archive')
                         ->orderBy('mission.code', 'ASC')
                         ->get()
                         ->toArray();
        $missions = array_map(
            function ($m) {
                $m['description'] = \App\Helper::makeUrlsAbsolute($m['description']);
                return $m;
            },
            $missions
        );
        return response()->json($missions);
    }

    /**
     * List the missions of a teacher.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mission  $mission
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Mission $mission)
    {
        if(Auth::user()->cannot('view', $mission)){
            return response()->json(['message' => 'Permission denied'],403);
        }
        return response()->json($mission);
    }

    public function createTeamConfig(Request $request, Mission $mission)
    {
        if (Auth::user()->cannot('view', $mission)) {
            return response()->json(['message' => 'Not allowed'], 403);
        }
        $data = $request->validate([
            'courseid' => 'int|required',
            'coursename' => 'string|required',
            'participants' => '',
            'team_config' => 'required',
            'groupid' => 'int|nullable',
            'id_extplatform' => 'int',
        ]);
        if (!isset($data['participants'])) {
            $data['participants'] = [];
        }else {
            $data['participants'] = (array) $data['participants'];
        }
        if (isset($data['team_config']['id_team_config'])) {
            // Moodle only asks to restore a TC, if it still exists, we return it
            $tc = TeamConfig::find($data['team_config']['id_team_config']);
            if ($tc) {
                return response()->json(['team_config' => $tc]);
            }
        }
        $classe = Classe::fetchOrCreateFromExternal(
            $data['courseid'],
            $data['id_extplatform'],
            $data['groupid'],
            $data['coursename'],
            $data['participants'],
            Auth::user()
        );
        $tcdata = (array)$data['team_config'];
        $tcdata['id_class'] = $classe->id_class;
        $tcdata['id_mission'] = $mission->id_mission;
        $tcdata['allow_import'] = 0;
        $tcdata['allow_save_ld'] = 0;
        $tcdata['allow_msg_teacher'] = 1;
        $tcdata['allow_msg_team'] = 1;
        if (!isset($tcdata['name_prefix'])) {
            $tcdata['name_prefix'] = '';
        }
        if (!isset($tcdata['method'])) {
            $tcdata['method'] = TeamConfig::NO_METHOD;
        }
        try {
            $tc = TeamConfig::create($tcdata);
        } catch (\Illuminate\Database\QueryException $e) {
            $code = $e->errorInfo[1];
            if ($code == 1062) {
                $code = 409;
                //$message = "Mission déjà utilisée pour cette classe";
                $message = __("Rapport déjà utilisé pour cette classe");
            } else {
                $message = $e->getMessage();
                Log::error("API error while creating teamconfig : ".$message ."( ".$code." )");
                $code = 500;
            }
            return response()->json(['message' => $message], $code);
        }
        return response()->json(['team_config' => $tc]);
    }

    public function updateTeamConfig(Request $request, Mission $mission)
    {
        if (Auth::user()->cannot('view', $mission)) {
            return response()->json(['message' => 'Not allowed'], 403);
        }
        // Designer on the mission
        $data = $request->validate([
            'courseid' => 'int|required',
            'groupid' => 'int|nullable',
            'coursename' => 'string|nullable',
            'id_extplatform' => 'int',
            'team_config' => 'required',
        ]);
        $classe = Classe::fromExternal($data['courseid'], $data['id_extplatform'], $data['groupid']);
        if (!$classe) {
            return response()->json(['message' => 'No such class'], 403);
        }
        $tc = $classe->teamConfigs()->where('id_mission', $mission->id_mission)->first();
        if (!$tc) {
            return response()->json(['message' => 'No such team_config'], 403);
        }
        $tc->fill((array)$data['team_config']);
        $tc->save();
        return response()->json(['team_config' => $tc]);
    }

    /**
     * The API plugin cannot give permission to teamconfig anymore, we keep
     * this method for retro compatibility.
     */
    public function grant(Request $request)
    {
        return  UserController::grantteacher($request);
    }
}
