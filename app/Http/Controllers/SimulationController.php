<?php

namespace App\Http\Controllers;

use App\Labdoc;
use App\Report;
use App\ReportPart;
use App\Simulation;
use App\Simulation\DosageSpectro;
use App\Simulation\Titrab;
use App\SimulationConstraint;
use App\SimulationTaskType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SimulationController extends Controller
{
    public function checkProtocolAndGetFeedback(Request $request)
    {
        $user = Auth::user();
        $data = $request->validate([
            'id_labdoc' => 'int|required',
            'isLdInEdition' => 'boolean',
            'onlySimulation' => 'boolean',
            'protocol' => 'json|nullable',
            'states' => 'array|nullable',
            'materialList' => 'json|nullable',
            'previous_error' => 'int|nullable',
            'base_unit' => 'array|nullable',
            'scope' => 'string|nullable',
            'quantities' => 'json|nullable',
            'variableList' => 'array|nullable',
            'replaceLdResponse' => 'boolean|nullable',
            'replaceLdData' => 'boolean|nullable',
        ]);
        $data['protocol'] = json_decode($data['protocol'], true);
        $data['materialList'] = json_decode($data['materialList'], true);
        $data['quantities'] = json_decode($data['quantities'], true);
        if (!array_key_exists('protocol', $data)) {
            return response()->json(['message' => __('Le protocole n’a pas été envoyé')], 404);
        } 
        if (!array_key_exists('states', $data)) {
            $data['states'] = [];
        }
        if (!array_key_exists('previous_error', $data)) {
            $data['previous_error'] = null;
        }
        if (!array_key_exists('base_unit', $data)) {
            $data['base_unit'] = null;
        }
        if (!array_key_exists('variableList', $data)) {
            $data['variableList'] = null;
        }
        if (!array_key_exists('replaceLdResponse', $data)) {
            $data['replaceLdResponse'] = null;
        }
        if (!array_key_exists('replaceLdData', $data)) {
            $data['replaceLdData'] = null;
        }

        $labdoc = Labdoc::find($data['id_labdoc']);
        abort_if($user->cannot('view', $labdoc), 403);

        $simulation_code = $labdoc->mission->simulation_code;
        if (!$simulation_code) {
            return response()->json(['message' => __('Le labdoc n’est pas rattaché à une simulation')], 404);
        } else {
            // Trace user only in scope default
            $traceAllowed = $data['scope'] === 'default' && $labdoc->report->status !== Report::SOLUTION && $data['isLdInEdition'];
            // conversion
            $data['materialList'] = self::_convertMaterialQuantitiesToBaseUnits($data['materialList'], $data['base_unit'], $data['quantities']);
            
            // Trace protocol and materialList before checking constraint
            $attributes = json_encode(array_filter(
                $data,
                fn($value, $key) => in_array($key, ['materialList', 'protocol']) && $value,
                ARRAY_FILTER_USE_BOTH
            ), JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);

            if($traceAllowed) {
                DB::table('simulation_trace')->insert([
                    'simulation_code' => $simulation_code,
                    'trace_action' => 'student_check_protocol',
                    'id_user' => Auth::id(),
                    'id_mission' => $labdoc->mission->id_mission,
                    'id_report' => $labdoc->report->id_report,
                    'id_labdoc' => $labdoc->id_labdoc,
                    'attributes' => $attributes
                ]);
            }

            $previous_states = $data['states'];
            if ($simulation_code === 'titrab') {
                $simulator = new Titrab(collect($data['protocol']), collect($data['materialList']), collect($data['quantities']));
            } else if ($simulation_code === 'dosage-spectro') {
                $simulator = new DosageSpectro(collect($data['protocol']), collect($data['materialList']), collect($data['quantities']));
            } else {
                return response()->json(['message' => __('No simulator was found')],403);
            }
            $previous_error = SimulationConstraint::find($data['previous_error']);
            $simulator->removeDatasetAndResponseLabdoc($labdoc, $data['replaceLdResponse'], $data['replaceLdData']);
            $simulator->prepareConstraints($previous_states, $previous_error);
            $evaluated_constraints = $simulator->checkConstraints($data['variableList']);
            $current_error = $evaluated_constraints['constraints']->whereNotNull('current_state')->where('current_state', 0)->first();
            $first_blocking_error = $evaluated_constraints['constraints']->whereNotNull('current_state')->where('current_state', 0)->where('simulation_enabled', 0)->first();

            $feedback = null;
            $globalKnowledgeEvolutions = [];
            $skillsChanges = [];
            $progression = null; 
            $newUserSkills = null;
            if($data['isLdInEdition']) {
                $oldUserSkills = $simulator->getCurrentSkills($user);
                $oldGlobalKnowledges = Simulation::getGlobalKnowledges($user, $simulation_code);
                // Decrease feedback level if needed
                $simulator->decrementFeedbackLevel($user, $evaluated_constraints['constraints'], $previous_error, $current_error);
                if(!$data['onlySimulation']) {
                    // Compute the feedback with the new feedback level
                    $feedback = $simulator->generateFeedback($user, $current_error);
                    // Increase of 1 feedback level for current error for next time
                    if($current_error) {
                        $simulator->updateUserFeedbackLevel($user, $current_error, $current_error->taskType, 1);
                    }
                }
                // Update skills  
                $newUserSkills = $simulator->updateSkills($user, $evaluated_constraints['constraints'], $previous_error);
                $skillsChanges = $simulator->computeSkillsChanges($oldUserSkills, $newUserSkills);

                // Compute the evolution of the K group or if no error get the new K group
                $globalKnowledgeEvolutions = $simulator->getGlobalKnowledgeEvolutions($user, $oldGlobalKnowledges, $current_error!==null);

                $progression = $simulator->getProgression($evaluated_constraints['constraints']);
            }

            // Case where the current error allows simulation but another error in the protocol blocks it
            $feedbackNextBlocking = ($current_error && $first_blocking_error && $current_error->id_constraint !== $first_blocking_error->id_constraint);
            $brokenConstraints = $evaluated_constraints['constraints']->filter(fn($c) => $c['current_state'] === 0 && $c['simulation_enabled'] === 0);
            $simulation_data = $simulator->getSimulationData($brokenConstraints);
            
            $constraints_states = collect($evaluated_constraints['constraints'])->map(function($c) {
                return [
                    'id_constraint' => $c->id_constraint,
                    'previous_state' => $c->previous_state,
                    'current_state' => $c->current_state,
                    'last_not_null_state' => $c->last_not_null_state,
                    'last_with_feedback' => $c->last_with_feedback,
                    'description' => $c->description,
                    'constraintKey' => $c->satisfaction_condition_key,
                    'position' => $c->position,
                ];
            });
            
            if($traceAllowed) {
                // Trace current constraints states
                DB::table('simulation_trace')->insert([
                    'simulation_code' => $simulation_code,
                    'trace_action' => 'student_constraints_state',
                    'id_user' => Auth::id(),
                    'id_mission' => $labdoc->mission->id_mission,
                    'id_report' => $labdoc->report->id_report,
                    'id_labdoc' => $labdoc->id_labdoc,
                    'attributes' => json_encode($constraints_states,JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES)
                ]);
                // Trace feedback
                if($feedback) {
                    DB::table('simulation_trace')->insert([
                        'simulation_code' => $simulation_code,
                        'trace_action' => 'student_get_a_feedback',
                        'id_user' => Auth::id(),
                        'id_mission' => $labdoc->mission->id_mission,
                        'id_report' => $labdoc->report->id_report,
                        'id_labdoc' => $labdoc->id_labdoc,
                        'attributes' => json_encode($feedback,JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES)
                    ]);
                }
                // Trace knowledge
                if($newUserSkills) {
                    DB::table('simulation_trace')->insert([
                        'simulation_code' => $simulation_code,
                        'trace_action' => 'student_update_knowledge',
                        'id_user' => Auth::id(),
                        'id_mission' => $labdoc->mission->id_mission,
                        'id_report' => $labdoc->report->id_report,
                        'id_labdoc' => $labdoc->id_labdoc,
                        'attributes' => json_encode($newUserSkills,JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES)
                    ]);
                }
                if($progression !== null) {
                    // Trace current progression
                    DB::table('simulation_trace')->insert([
                        'simulation_code' => $simulation_code,
                        'trace_action' => 'student_progression',
                        'id_user' => Auth::id(),
                        'id_mission' => $labdoc->mission->id_mission,
                        'id_report' => $labdoc->report->id_report,
                        'id_labdoc' => $labdoc->id_labdoc,
                        'attributes' => json_encode(['progression' => $progression],JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES)
                    ]);
                }
            }
            
            // Trace launch simulation
            $labdocDataset = null;
            $labdocResponse = null;
            if((!$first_blocking_error && $simulation_data !== null && $data['onlySimulation']) || !$current_error) {
                if($traceAllowed) {
                    if($current_error || $data['variableList']) {
                        $traceAction = 'student_launch_' . ($data['variableList'] ? 'partial_' : '') . 'simulation';
                    } else {
                        $traceAction = 'student_launch_simulation_and_succeed';
                    }
                    DB::table('simulation_trace')->insert([
                        'simulation_code' => $simulation_code,
                        'trace_action' => $traceAction,
                        'id_user' => Auth::id(),
                        'id_mission' => $labdoc->mission->id_mission,
                        'id_report' => $labdoc->report->id_report,
                        'id_labdoc' => $labdoc->id_labdoc,
                        'attributes' => json_encode(['simulate' => $simulation_code, 'simulation_info' => $simulator->getSimulationTraceInfo($simulation_data)],JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES)
                    ]);
                }
                if (!$data['variableList']) {
                    if ($data['replaceLdResponse']) {
                        $labdocResponse = $simulator->createLabdocResponse($labdoc);
                        $responseViewObject = \App\Views\Labdoc::fromLd($labdocResponse, $user, 'default', 0);
                        $responseView = view('labdoc/_ld')->with(['ldv' => $responseViewObject])->render();
                    }
                    if ($data['replaceLdData']) {
                        $labdocDataset = $simulator->createLabdocData($simulation_data, $data['base_unit'], $labdoc, $current_error);
                        $datasetViewObject = \App\Views\Labdoc::fromLd($labdocDataset, $user, 'default', 0);
                        $datasetView = view('labdoc/_ld')->with(['ldv' => $datasetViewObject])->render();
                    }
                }
            }
            return response()->json([
                'previous_error' => $current_error,
                'simulation_enabled' => ($simulation_data === null) ? 0 : 1,
                'constraintStates' => $constraints_states,
                'feedback' => $feedback,
                'globalKnowledges' => $globalKnowledgeEvolutions,
                'progression' => $progression,
                'feedbackNextBlocking' => $feedbackNextBlocking,
                'simulation_data' => $simulation_data,
                'idReportPart' => $labdoc->id_report_part,
                'datasetView' => $labdocDataset ? $datasetView : null,
                'datasetAssignment' => $labdocDataset ? $labdocDataset->assignment : null,
                'responseView' => $labdocResponse ? $responseView : null,
                'responseAssignment' => $labdocResponse ? $labdocResponse->assignment : null,
                'datasetData' => $labdocDataset ? $labdocDataset->labdoc_data : null,
                'responseData' => $labdocResponse ? $labdocResponse->labdoc_data : null,
                'idDataset' => $labdocDataset ? $labdocDataset->id_labdoc : null,
                'idResponse' => $labdocResponse ? $labdocResponse->id_labdoc : null,
                'skillsChanges' => $skillsChanges
            ]);
        }
    }
    
    public function resetUserSkills(Request $request) {
        $user = Auth::user();
        $data = $request->validate([
            'id_labdoc' => 'int|required'
        ]);
        $labdoc = \App\Labdoc::find($data['id_labdoc']);
        abort_if($user->cannot('view', $labdoc), 403);
        $simulation_code = $labdoc->mission->simulation_code;
        SimulationTaskType::resetSkills($simulation_code, $user);
        return response()->json(['updated'=>true]);
    }
    
    private function _convertMaterialQuantitiesToBaseUnits($materialList, $base_units, $quantities)
    {
        $base_units = collect($base_units)->map(function ($baseUnit, $baseQuantity)  use ($quantities) {
            $quantityId = collect($quantities)->first(function ($quantity) use ($baseQuantity) {
                return $quantity['name']['en'] === $baseQuantity;
            })['id'];
            $unitId = collect(collect($quantities)->firstWhere('id', $quantityId)['unit'])->first(function ($unit) use ($baseUnit) {
                return $unit['symbol']['en'] === $baseUnit;
            })['id'];
            return ['quantityId' => $quantityId, 'unitId' => $unitId];
        });
        foreach ($materialList as $materialKey => $material) {
            foreach ($material['properties'] as $propertyIdx => $property) {
                $baseUnitQuantity = collect($base_units)->firstWhere('quantityId', $property['physicalQuantityId']);
                if ($baseUnitQuantity) {
                    $sourceFactor = collect(collect($quantities)->firstWhere('id', $property['physicalQuantityId'])['unit'])->firstWhere('id', $property['unitId'])['factor'];
                    $targetFactor = collect(collect($quantities)->firstWhere('id', $baseUnitQuantity['quantityId'])['unit'])->firstWhere('id', $baseUnitQuantity['unitId'])['factor'];
                    $materialList[$materialKey]['properties'][$propertyIdx]['value'] = floatval($property['value']) * floatval($sourceFactor[0]) / floatval($targetFactor[0]) + (floatval($sourceFactor[1]) - floatval($targetFactor[1])) / floatval($targetFactor[0]);
                    $materialList[$materialKey]['properties'][$propertyIdx]['unitId'] = $baseUnitQuantity['unitId'];
                }
            }
        }
        return $materialList;
    }

    public function getDataJsonMigration (Request $request) {
        $user = Auth::user();
        $data = $request->validate([
            'simulation_code' => 'string|required'
        ]);
        abort_if(!$user->canEditSimulation($data['simulation_code']), 403);
        
        $jsonData = Simulation::_getSimulationTableInJson($data['simulation_code']);
        
        return response()->json(json_encode($jsonData));
    }
    
    public function getSynchronizeData(Request $request) {
        $user = Auth::user();
        $data = $request->validate([
            'simulationCode' => 'string|required',
            'idLd' => 'int|required'
        ]);
        $labdoc = Labdoc::find($data['idLd']);
        abort_if($user->cannot('view', $labdoc), 403);
        return Simulation::_getSynchronizeData($labdoc, $data['simulationCode']);
    }
    
    public function updateDataMigration(Request $request)
    {
        $user = Auth::user();
        $data = $request->validate([
            'simulation_code' => 'string|required',
            'data_migration' => 'json|required'
        ]);
        abort_if(!$user->canEditSimulation($data['simulation_code']), 403);

        Simulation::_updateSimulationTableInBd(json_decode($data['data_migration'], true), $data['simulation_code']);
        return true;
    }
    
    public function traceSimulationNodeAction (Request $request) 
    {
        $data = $request->validate([
            'idLabdoc' => 'int|required',
            'traceAction' => 'string|nullable',
            'simulationVariable' => 'string|nullable',
            'path' => 'array|nullable',
            'position' => 'int|required',
            'action' => 'array|nullable',
            'step' => 'array|nullable',
        ]);
        $labdoc = Labdoc::find($data['idLabdoc']);
        abort_if(Auth::user()->cannot('update', $labdoc), 403);
        $simulationCode = $labdoc->mission->simulation_code;
        abort_if(!$simulationCode, 404);

        $idReport = $labdoc->id_report;
        $idMission = $labdoc->mission->id_mission;
        
        if (!array_key_exists('path', $data)) {
            $data['path'] = ['root'];
        }

        $attributes = json_encode(array_filter(
            $data, 
            fn($value, $key) => in_array($key, ['path', 'position', 'simulationVariable', 'iterationVariables', 'action', 'step']) && $value!==null,
            ARRAY_FILTER_USE_BOTH
        ), JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
        
        if($labdoc->report->status !== Report::SOLUTION) {
            DB::table('simulation_trace')->insertGetId([
                'simulation_code' => $simulationCode,
                'trace_action' => $data['traceAction'],
                'id_user' => Auth::id(),
                'id_mission' => $idMission,
                'id_report' => $idReport,
                'id_labdoc' => $labdoc->id_labdoc,
                'attributes' => $attributes
            ]);
        }
        return null;
    }
    
    public function traceSimulationUserAction(Request $request)
    {
        $data = $request->validate([
            'idLabdoc' => 'int|required',
            'traceAction' => 'string|nullable',
            'attributes' => 'json|nullable',
        ]);
        $labdoc = Labdoc::find($data['idLabdoc']);
        abort_if(Auth::user()->cannot('update', $labdoc), 403);
        $simulationCode = $labdoc->mission->simulation_code;
        abort_if(!$simulationCode, 404);

        $idReport = $labdoc->id_report;
        $idMission = $labdoc->mission->id_mission;
        if($labdoc->report->status !== Report::SOLUTION) {
            DB::table('simulation_trace')->insertGetId([
                'simulation_code' => $simulationCode,
                'trace_action' => $data['traceAction'],
                'id_user' => Auth::id(),
                'id_mission' => $idMission,
                'id_report' => $idReport,
                'id_labdoc' => $labdoc->id_labdoc,
                'attributes' => $data['attributes'],
            ]);
        }
        return null;
    }
    
    public function traceSimulationInit (Request $request) 
    {
        $data = $request->validate([
            'idLabdoc' => 'int|required',
            'traceAction' => 'string|nullable',
            'materialList' => 'json|nullable',
            'protocol' => 'json|nullable'
        ]);
        $data['protocol'] = json_decode($data['protocol'], true);
        $data['materialList'] = json_decode($data['materialList'], true);
        
        $labdoc = Labdoc::find($data['idLabdoc']);
        abort_if(Auth::user()->cannot('update', $labdoc), 403);
        $simulationCode = $labdoc->mission->simulation_code;
        abort_if(!$simulationCode, 404);

        $idReport = $labdoc->id_report;
        $idMission = $labdoc->mission->id_mission;
        
        $attributes = json_encode(array_filter(
            $data, 
            fn($value, $key) => in_array($key, ['materialList', 'protocol']) && $value,
            ARRAY_FILTER_USE_BOTH
        ), JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);

        if($labdoc->report->status !== Report::SOLUTION) {
            DB::table('simulation_trace')->insertGetId([
                'simulation_code' => $simulationCode,
                'trace_action' => $data['traceAction'],
                'id_user' => Auth::id(),
                'id_mission' => $idMission,
                'id_report' => $idReport,
                'id_labdoc' => $labdoc->id_labdoc,
                'attributes' => $attributes
            ]);
        }
        return null;
    }
    
    public function traceSimulationCorrectAnswer(Request $request)
    {
        $data = $request->validate([
            'idLabdoc' => 'int|required',
        ]);

        $labdoc = Labdoc::find($data['idLabdoc']);
        
        abort_if(Auth::user()->cannot('view', $labdoc), 403);
        
        $simulationCode = $labdoc->mission->simulation_code;
        abort_if(!$simulationCode, 404);
        
        $idReport = $labdoc->id_report;
        $idMission = $labdoc->mission->id_mission;
        
        if($labdoc->report->status !== Report::SOLUTION) {
            DB::table('simulation_trace')->insertGetId([
                'simulation_code' => $simulationCode,
                'trace_action' => 'student_correct_answer',
                'id_user' => Auth::id(),
                'id_mission' => $idMission,
                'id_report' => $idReport,
                'id_labdoc' => $labdoc->id_labdoc,
                'attributes' => '[]'
            ]);
        }
        return null;
    }
    
    public function editSimulation(Request $request)
    {
        $user = Auth::user();
        abort_if(!$user->isSimulationManager(), 403);
        return view('teacher.simulation.edit');
    }
    
    public function getReportSimulationDashboard(Request $request)
    {
        $data = $request->validate([
            'simulationCode' => 'string|required',
            'idReport' => 'int|required',
        ]);
        $report = \App\Report::find($data['idReport']);
        abort_if(Auth::user()->cannot('view', $report), 403);
        $dashboardData = Simulation::_getReportDashboardData($data['simulationCode'], $report);
        return view('teacher.report._simulationReportDashboard', ['dashboardData' => $dashboardData]);
    }
    
    public function getClassSimulationDashboard(Request $request)
    {
        $data = $request->validate([
            'idMission' => 'int|required',
            'idClass' => 'int|required',
        ]);
        $user = Auth::user();
        $mission = \App\Mission::find($data['idMission']);
        $class = \App\Classe::find($data['idClass']);
        abort_if($user->cannot('view', $mission), 403);
        abort_if($user->cannot('view', $class), 403);
        abort_if(!$mission->simulation_code,200);
        
        $dashboardData = Simulation::_getClassDashboardData($mission, $class);
        return view('teacher.report._simulationClassDashboard', ['dashboardData' => $dashboardData]);
    }
}
