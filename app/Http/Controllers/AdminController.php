<?php

namespace App\Http\Controllers;

use App\TeacherTeam;
use App\Processes\AdminTests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\MarkdownMail;

class AdminController
{
    /**
     * Display a list of existing and potential reports for a student.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();

        $tz = __("Fuseau horaire : :tz", ["tz" => date_default_timezone_get()]);

        $output = "";
        exec('echo -n "uptime"; uptime; free -h; df -h storage', $output);

        return view('admin/index')->with([
            'user' => $user,
            'vers' => \App\Helper::getVersion(),
            'tz' => $tz,
            'syst' => trim(implode('<br />', $output))
        ]);
    }

    /**
     * Run instance test
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function selfTest(Request $request)
    {
        $adminTests = new AdminTests();
        $messages = $adminTests->runTests();
        return response()->json($messages);
    }
    
    /**
     * Send test email
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sendTestMail(Request $request)
    {
        $user = Auth::user();
        if (!$user->email) {
            return response()->json(__("Impossible d'envoyer un mail de test : votre adressse email n'est pas définie"));
        }
        Mail::to($user->email)->send(
            new MarkdownMail(
                'emails/test',
                __("LabNBook : email de test"),
                []
            )
        );
        $message = __(
            "Email de test envoyé à :mail, pensez à vérifier votre dossier indésirable",
            ['mail' => $user->email]
        );
        return response()->json($message);
    }


    /**
     * Creates an instituion with a teacher class associated
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function changeRoleTeacher(Request $request)
    {
        $user = Auth::user();
        abort_if($user->cannot('changeRole', TeacherTeam::class), 403);

        $data = $request->validate([
            'id_user' => 'int|required',
            'role' => 'string|required',
        ]);

        $teacher = \App\Teacher::find($data["id_user"]);
        $teacher->changeRole($data["role"]);

        return response()->json($teacher);
    }
}
