<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use \App\Mission;
use \App\Report;
use \App\Resource;
use \App\Trace;

class StorageController
{
    /**
     * @param string $ressourceType
     * @param string $fileExtension
     * @param string $fileMimeType
     * @param boolean $isTeacher
     * @return array ['valid' => boolean, 'message' => string]
     */
    private function validateFileType($ressourceType, $fileExtension, $fileMimeType, $is_teacher)
    {
        if ($ressourceType === "assignment" || $ressourceType === "tmce_pdf") {
            if (! ($fileExtension === "pdf" && $fileMimeType === "application/pdf")) {
                return [
                    'valid' => false,
                    'message' => __("Vous ne pouvez uploader que des fichiers pdf.")
                ];
            }
        } elseif ($ressourceType === "tmce_img" || $ressourceType == 'res_doc' && !$is_teacher) {
            $allowedExts  = [
                "pdf",
                "gif", "jpeg", "jpg", "png", // image
                "csv", 
                "txt",
            ];
            $allowedMimeTypes = [
                "application/pdf",
                "image/gif", "image/jpeg", "image/jpg", "image/pjpeg", "image/x-png", "image/png",
                "text/csv", "text/plain",
            ];
            if (!in_array($fileMimeType, $allowedMimeTypes) || !in_array(strtolower($fileExtension), $allowedExts)) {
                return [
                    'valid' => false,
                    'message' => __("Vous ne pouvez uploader que des fichiers pdf, csv, texte ou image (jpg, gif, png).")
                ];
            }
        }
        return ['valid' => true];
    }

    /**
     * @param int $bytes
     * @return int
     */
    private function toMegabytes($bytes)
    {
        return round($bytes / 1024 / 1024);
    }

    /**
     * @param  int  $error
     * @return \Illuminate\Http\Response
     */
    private function handleFileError($error)
    {
        switch ($error) {
            case 1: // UPLOAD_ERR_INI_SIZE
                header("Content-type: application/json", true, 413);
                $max = $this->toMegaBytes(min(
                    array_map(['\App\Helper', 'parseHumanSize'], [ini_get('post_max_size'), ini_get('upload_max_filesize')])
                ));
                $out = __("La taille maximale autorisée par le serveur est :max Mo.", ['max' => $max]);
                break;
            case 2: // UPLOAD_ERR_FORM_SIZE
                $out = __("Erreur : le fichier dépasse la limite autorisée (html form)");
                break;
            case 3: // UPLOAD_ERR_PARTIAL
                $out = __("Erreur : l'envoi du fichier a été interrompu pendant le transfert");
                break;
            case 4: // UPLOAD_ERR_NO_FILE
                $out = __("Erreur : le fichier que vous avez envoyé a une taille nulle");
                break;
            default:
                $out = __("Erreur inconnue lors de l'envoi");
                break;
        }
        return response()->json($out, 409);
    }

    /**
     * Returns a uniq filename for a user with a given suffix
     * @param int id_user
     * @param string $suffix
     * @return string
     */
    private function getUniqueName($id_user, $suffix)
    {
        $now = new \DateTime("now");
        $time = $now->format('Y-m-d_H-i-s');
        return $id_user.'_'.$time.$suffix ;
    }

    /**
     * Sanitize a fileName
     * @param string name
     * @return string
     */
    private function sanitize($name)
    {
            $fileid = Str::ascii(trim($name));
            $fileid = preg_replace("/[ _]+/", "-", $fileid); // remplacement des espaces et "_" par un seul tiret
            return preg_replace("/[^a-z0-9-\.]/i", "", $fileid); // suppression des caracteres speciaux
    }

    /**
     * uploads a file.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request)
    {
        $data = $request->validate([
            'id_context' => 'int|required',
            'context' => 'in:mission,report|required',
            'type' => 'string|required',
            'name' => 'string|nullable',
            'id_resource' => 'int|nullable',
        ]);

        $file = $request->file('file_document');
        if ($file == null) {
            return response()->json(__("Upload impossible - fichier non reçu"), 409);
        }
        if (!$file->isValid()) {
            return $this->handleFileError($file->getError());
        }

        $extension = $file->getClientOriginalExtension();
        $mime_type = $file->getMimeType();
        $is_teacher = $data['context'] == 'mission';
        $user = Auth::user();
        if ($data['context'] == 'report') {
            // The user is a teacher not linked as a student to the report
            // The fact that the user is linked as a teacher will be checked later by the abort_if($user->cannot('update', $object)) test
            $is_teacher = $user->isTeacher() && !$user->reports()->where('report.id_report', $data['id_context'])->exists();
        }
        $status = $this->validateFileType($data['type'], $extension, $mime_type, $is_teacher);
        if (!$status['valid']) {
            unlink($file->path()) ;
            return response()->json($status['message'], 409);
        }

        if ($data['context'] == 'report') {
            // Report resource
            $object = Report::find($data['id_context']);
            $base_dir = 'reports/'.$object->id_report;

            // création du nom de fichier (pour eviter des doublons malheureux)
            $fileid = $this->getUniqueName($user->id_user, ".".$extension);
            $name = strip_tags($data['name']);
        } else {
            $object = Mission::find($data['id_context']);
            $base_dir = "/missions/". $object->id_mission;
            $fileid = $this->sanitize($file->getClientOriginalName());
            if ($data['name'] != null) {
                $name = strip_tags($data['name']);
            } else {
                $name = '';
            }
            if ($data['type'] === "tmce_img" || $data['type'] === "tmce_pdf") {
                // Mission tinymce
                $base_dir .= "/tinymce";
                $name = $fileid;
                $fileid = $this->getUniqueName($user->id_user, "_".$fileid);
            } else {
                // Mission resource
                $base_dir .= "/resources";
            }
        }
        $url = "/storage/$base_dir/$fileid";
        $dir_dest = storage_path('app/public/'.$base_dir);

        // Last security check
        abort_if($user->cannot('update', $object), 403);

        if (!is_dir($dir_dest)) {
            @mkdir($dir_dest, 0777, true);
        }

        // copie du fichier dans le dossier
        if (!move_uploaded_file($file->path(), $dir_dest.'/'.$fileid)) {
            return response()->json(__("Impossible de copier le fichier dans le dossier de destination."), 500);
        }
        chmod($dir_dest.'/'.$fileid, 0777);
        // Copie reussie

        if ($data['context'] == 'report') {
            // Report resource
            $id_ressource = Resource::addDoc($object->id_report, $fileid, $name, 'file');
            Resource::createOrUpdateLinkToUser($id_ressource,$user->id_user);
            Trace::logAction(Trace::ADD_RESOURCE, ['id_report' => $object->id_report, 'url' => $url]);
        } else {
            // Mission
            // Gestion des resources
            $id_resource = $data['id_resource'];
            $last_rd_position = "" ;
            if ($data['type'] === "assignment") {
                $resource = Resource::where('id_mission', $object->id_mission)
                    ->where('res_type', $data['type'])
                    ->first();
                if ($resource) { // remplacement d'un assignment
                    $resource->replaceFile($name, $fileid);
                } else {
                    $id_resource = Resource::addDoc(null, $fileid, $name, "assignment", $object->id_mission);
                    Trace::logAction(Trace::TEACHER_ADD_RESOURCE, ['id_mission' => $object->id_mission, 'name' => $fileid]);
                }
            } elseif ($data['type'] == "res_doc") { // traitement des ressources
                if ($id_resource) { // on est dans le cas d'une modification
                    // suppression de l'ancien fichier
                    $resource = Resource::find($id_resource);
                    $resource->replaceFile($name, $fileid);
                } else { // on est dans le cas d'un ajout
                    $last_rd_position = $object->nextRDPos() ;
                    $id_resource = Resource::addDoc(null, $fileid, $name, "file", $object->id_mission, $last_rd_position);
                    Trace::logAction(Trace::TEACHER_ADD_RESOURCE, ['id_mission' => $object->id_mission, 'name' => $name]);
                }
            }

            if ($id_resource) { // modification du nom de la ressource pour éviter les doublons
                $new_path = $id_resource."-".$fileid;
                if (rename($dir_dest."/".$fileid, $dir_dest."/".$new_path)) {
                    $resource = Resource::find($id_resource);
                    $resource->res_path = $new_path;
                    $resource->save();
                    // Update fileid and url
                    $fileid = $new_path;
                    $url = "/storage/$base_dir/$fileid";
                }
            }
            
            if($data['type'] === "res_doc") {
                // TO DO from 1070 need to be checked during refactor (591)  
                $resource = Resource::find($id_resource);
                $file_ext = strtolower(pathinfo($resource->res_path, PATHINFO_EXTENSION));
                if ($file_ext == "pdf") {
                    $resource_type = "pdf";
                } else if ($file_ext == "png" || $file_ext == "jpg" || $file_ext == "jpeg" || $file_ext == "gif") {
                    $resource_type = "image";
                } else {
                    $resource_type = "file";
                }
                return view('teacher/missions/_RDLine')->with([
                    'ressource' => $resource,
                    'path' => "/storage/missions/".$data['id_context']."/resources/",
                    'ressource_type' =>  $resource_type,
                ]);
            }
            
            return response()->json([
                "res_type" => $data['type'],
                "file_name" => $fileid,
                "name" => $name,
                "pos" => $last_rd_position,
                "id_res" => $id_resource,
                "file_ext" => $extension,
                "res_path" => $url,
            ]);
        }
    }
}
