<?php

namespace App\Http\Controllers\Auth;

use App\Helper;
use App\User;
use App\Http\Controllers\Controller;
use App\Processes\Ldap\LdapUser;
use App\Processes\Ldap\Ldap;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CasController extends Controller
{
    protected $ldap = null;
    /**
     * Switch an connected password user to CAS authentication.
     *
     * @param Request $request
     */
    public function confirm(Request $request)
    {
        if (auth()->check() && !auth()->user()->isInternal()) {
            // Nothing to do there
            return redirect()->intended('/');
        }
        // We are logged in as a non cas user and want to switch to CAS
        $ldapUser = $this->authenticateCasAndgetLdapUser();

        if (\App\User::internal(false)->where('login', $ldapUser->uid)->exists()) {
            // Cas account
            // We inject a form to merge accounts in the error message
            $url=route('merge-cas');
            $token = csrf_token();
            $title = __("Fusionner mes comptes");
            $text = __("En cochant cette case vous acceptez que vos deux comptes soient fusionnés (opération recommandée).");
            $form = <<<EOJS
            <form method='POST' action='$url' id="merge-users">
             <input type='hidden' name='_token' value='$token'/>
             </form>
<script type="text/javascript">
    $().ready(function(){
        global_confirmation_popup.openConfirmationPopup( 
          "$title",
          "$text",
          function(){
            $('#merge-users').submit();
          }
        );
    });
</script>
EOJS;
            Helper::addAlert(
                "info",
                __("Vous avez déjà un compte LabNBook accessible depuis l'annuaire institutionnel (:name). Vous pouvez choisir de fusionner vos deux comptes pour utiliser uniquement l'annuaire institutionnel. :form", ['name' => config('labnbook.cas_name'), 'form' => $form])
            );
        } else {
            $updated = $this->getLdap()->switchToCasAuth(auth()->user(), $ldapUser->uid);
            Helper::addAlert(
                "info",
                __("La connexion à votre compte LabNBook se fera désormais par l'annuaire institutionnel :name.", ['name' => config('labnbook.cas_name')])
                . ($updated ? __("Vos informations personnelles ont été mises à jour depuis l'annuaire.") : "")
            );
        }
        if (session()->has('bindUrl')) {
            Helper::cleanBindSession();
        }

        return redirect()->intended('/');
    }

    /**
     * Authenticate with CAS (using subfission/cas which wraps jasig/phpcas)
     *
     * @param Request $request
     */
    public function login(Request $request)
    {
        $ldapError = null;
        try {
            $ldapUser = $this->authenticateCasAndgetLdapUser(false);
        } catch (\Exception $e) {
            $ldapError = $e;
            $ldapUser = new \StdClass();
            $ldapUser->uid = cas()->user();
        }
        $user = User::internal(false)->where('login', $ldapUser->uid)->first();

        if ($user) {
            return self::connect($user);
        } elseif (!$ldapError) {
            // in this block, CAS account, but no LabNBook account
            $confirmUrl =  session('confirmUrl', '');
            if ($confirmUrl) {
                // We are an account via a bind we can directly processeed to creation
                if (strpos($confirmUrl, 'create-teacher') !== false ) {
                    return self::createTeacher($request);
                }
                return self::createStudent($request);
            }
            if ($ldapUser->isTeacher()) {
                // this is a teacher that do not have a LnB account yet
                // Show the create teacher view
                $user = $this->getLdap()->mapLdapUserToUser($ldapUser);
                if (\App\User::getHomonymes($user)->isEmpty()) {
                    // No homonymes, we create a teacher account
                    return self::createTeacher($request, true);
                }
                return redirect('/auth/cas/create-teacher');
            }
            // CAS Student without local account : forbidden
            Helper::addAlert(
                "danger",
                    "<p>".__("Votre compte institutionnel :name n'est pas reconnu par LabNBook. Il peut y avoir deux raisons à cela", ['name' => htmlspecialchars($ldapUser->uid)]) . __("&nbsp;:") .
                    "</p> <ul> <li>".
                    __("Votre compte LabNBook est différent de votre compte institutionnel : utilisez la connexion 'avec un compte LabNBook'.").
                    "</li> <li>".
                    __("Vous n'avez pas encore été inscrit à LabNBook : contactez votre enseignant.").
                    " </li> </ul>"
            );
            return redirect()->intended('');
        }
        throw $ldapError;
    }

    /**
     * Authenticate with CAS (i.e. subfission/cas i.e. jasig/phpcas)
     *
     * @param Request $request
     */
    public function logout(Request $request)
    {
        $request->session()->remove('auth_method');
        Helper::addAlert(
            'info',
            __("Vous êtes déconnecté de :name.", ['name' => config('labnbook.cas_name')])
        );
        if (!config("cas.cas_masquerade")) {
            try {
                if (cas()->checkAuthentication()) {
                    cas()->logout();
                }
            } catch( \CAS_AuthenticationException $e) {
                // If ticket is invalid here, it is not a problem
                Log::Warning($e->getMessage());
            }
        }
        return redirect()->route('home');
    }

    /**
     * Create a student account if the CAS + LDAP respond.
     *
     * @param Request $request
     */
    public function createStudent(Request $request)
    {
        if (auth()->check()) {
            abort(403, __("Impossible de créer un compte depuis une session authentifiée."));
        }

        if (session('bindUrl', null) === null) {
            abort(403, __("Creation de compte étudiant non autorisée"));
        }

        $ldapUser = $this->authenticateCasAndgetLdapUser();
        $user = User::internal(false)->where('login', $ldapUser->uid)->first();
        if (!$user) {
            // user not in DB, create from LDAP
            $user = $this->getLdap()->createStudent($ldapUser->uid, $ldapUser->getInstitutionId(), $ldapUser->getEmail());
        }
        if (!$user) {
            // User was not found internally and create from LDAP failed
            Helper::addAlert(
                'danger',
                __("Aucun utilisateur ayant cet identifiant n'a été trouvé dans l'annuaire. Contactez les administrateurs de la plateforme.")
            );
            return redirect()->intended('');
        }
        return self::connect($user);
    }

    /**
     * Create a teacher account if the session contains the right data.
     *
     * @param Bool $forceCreate create account even if method is not post
     * @param Request $request
     */
    public function createTeacher(Request $request, $forceCreate = false)
    {
        $ldapUser = $this->authenticateCasAndgetLdapUser();
        if ($request->isMethod('post') || $forceCreate) {
            Helper::cleanBindSession();
            $user = \App\User::Where('login', $ldapUser->uid)->internal(false)->first();
            if ($user) {
                return self::connect($user);
            }
            $user = $this->getLdap()->mapLdapUserToUser($ldapUser);
            if ('on' === $request->input('accept_cgu')) {
                $user->cgu_accept_time = now();
            }
            if (!$user->save()) {
                abort(500, __("Impossible d'enregistrer l'utilisateur. Veuillez contacter l'administrateur."));
            }
            $comment = __('auto-inscription :name : ', ['name' => config('labnbook.cas_name')]);
            $comment .= $ldapUser->getInstitutionName();
            $user->grantTeacher([], $comment);

            Helper::addAlert("success", __("Vous avez été inscrit à LabNBook en tant qu'enseignant avec votre compte institutionnel (:name).", ['name' => config('labnbook.cas_name')])
            . "<br>" . __("N'hésitez pas à consulter les ressources d'aide dans le menu ci-dessus") . '&nbsp;&nbsp;&nbsp;<i class="fa fa-bars"></i>');
            return self::connect($user);
        } // else
        $user = $this->getLdap()->mapLdapUserToUser($ldapUser);
        session()->put('user', $user);
        session()->put('authType', 'Cas');
        session()->put('bindUrl', '/auth/cas/confirm');
        session()->put('confirmUrl', url()->current());
        return \App\Http\Controllers\Auth\LoginController::bindOrCreateAccount($request);
    }

    /**
     * Login the user then redirects.
     *
     * @param \App\Http\Controllers\Auth\User $user
     * @return \Illuminate\Http\Response
     */
    private static function connect(User $user)
    {
        auth()->login($user);
        session()->put('auth_method', 'cas');
        $bindUrl = session()->get('bindUrl', null);
        if ($bindUrl !== null) {
            return redirect($bindUrl);
        }
        return redirect()->intended('');
    }

    /**
     * Get the ldap representation of the cas authenticated user
     * @param boolean isMandatory
     */
    protected function authenticateCasAndgetLdapUser($sendReportOnError = true)
    {
        try {
            cas()->authenticate();
        } catch (\CAS_AuthenticationException $e) {
            abort(403, __("Le service d'authentification CAS a retourné une erreur, impossible de continuer"));
        }
        $username = cas()->user();
        if (empty($username)) {
            abort(500, __("Le service d'authentification CAS n'a pas fourni d'identifiant, impossible de continuer."));
        }
        return $this->getLdap($sendReportOnError)->getUserByUsername($username, true);
    }

    /**
     * Get the Ldap
     */
    protected function getLdap($sendReportOnError = true)
    {
        if (!$this->ldap) {
            $this->ldap = new Ldap(null, $sendReportOnError);
        }
        return $this->ldap;
    }


    /**
     * Merge the currently logger user with the CAS logged account
     *
     * @param Request $request
     */
    public function merge(Request $request)
    {
        $ldapUser = $this->authenticateCasAndgetLdapUser();
        $source = Auth()->user();
        $target = \App\User::internal(false)->where('login', $ldapUser->uid)->first();
        if (!$source) {
            throw new \Exception("No user logged in");
        }
        if (!$target) {
            throw new \Exception("No target user for merge process");
        }
        if ($source->id_user == $target->id_user) {
            \App\Helper::addAlert('danger', __("Les comptes ont déjà été fusionnés"));
            return redirect()->back();
        }
        Log::Info("Merging account {$source->id_user} into {$target->id_user}");
        $process = new \App\Processes\MergeUsers($source, $target, false /*this is not a dry run*/);
        if ($process->run()) {
            auth()->logout();
            \App\Helper::addAlert('info', __("La connexion se fera désormais par le compte institutionnel CAS (:name).", ['name' => config('labnbook.cas_name')]));
            return $this->connect($target);
        }
        \App\Helper::addAlert('danger', __("Impossible de fusionner les comptes, rien n'a été modifié, veuillez contacter les administrateurs"));
        return redirect()->back();
    }
}
