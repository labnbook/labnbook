<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\Extensions\LabnbookPassword;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
 
    /**
     * Get the response for password reset link.
     *
     * @param  string  $message
     * @param  boolean  $success
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    private function sendResponse($message, $success)
    {
        return response()->json([
            'message'=>$message,
            'category'=>$success?'success':'danger',
        ]);
    }

    /**
     * Get the response for a successful password reset link.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetLinkResponse(Request $request, $response)
    {
        $message=__("Un e-mail vous a été envoyé. A partir de cet e-mail, vous pouvez modifier le mot de passe de votre compte.") . "\n" . __("Pensez à regarder dans votre boite de spams...");
        return $this->sendResponse($message, true);
    }

    /**
     * Get the response for a failed password reset link.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetLinkFailedResponse(Request $request, $response)
    {
        $email = htmlspecialchars($request->input('email'));
        if ($response == LabnbookPassword::USER_NOT_MANAGED) {
            $message = __("Le compte correspondant à l'adresse :email est un compte institutionnel : nous ne pouvons pas vous aider à retrouver vos informations de connexion.", ['email' => $email]);
        } else {
            $message=__("L'e-mail :email n'existe pas dans la base LabNBook", ['email' => $email]);
        }
        return $this->sendResponse($message, false);
    }
}
