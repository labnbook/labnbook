<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use App\Helper;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $manager = app('impersonate');
        $user = Auth::user();
        if ($manager->isImpersonating()) {
            $user->leaveImpersonation();
            \App\Helper::AddAlert('warning', __("Vous êtes de nouveau connecté en tant que ':login'", ["login" => Auth::user()->login]));
            return redirect(config('laravel-impersonate.leave_redirect_to'));
        }
        $isCas = ($request->session()->get('auth_method') === 'cas');

        $this->guard()->logout();
        $request->session()->invalidate();
        // Logout with a message
        $message = $request->get('message', "");
        if (!empty($message)) {
            \App\Helper::AddAlert("danger", $message);
        }

        if ($isCas) {
            $request->session()->put('auth_method', 'cas');
        }
        redirect()->setIntendedUrl('/');

        return $this->loggedOut($request) ?: redirect('/login');
    }

    public function username()
    {
        return 'login';
    }

    public function showLoginForm(Request $request)
    {
        // Redirect to previous if no intended
        if (!session()->has('url.intended')) {
            session(['url.intended' => url()->previous()]);
        }
        /**
         * This check is broken since the session was deleted on the logout page.
         * @todo When the user logs out, if he was using CAS, put the marker in the new session.
         */
        try {
            if (!empty(config('cas.cas_hostname'))
                && session()->get('auth_method') == 'cas'
                && cas()->checkAuthentication()) {
                return redirect('/auth/cas/logout');
            }
        } catch( \CAS_AuthenticationException $e) {
            // If ticket is invalid here, it is not a problem
            Log::Warning($e->getMessage());
        }
        // Reset session
        Helper::cleanBindSession();
        return view('auth.login')->with(['bind' => true]);
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        $message = "<strong>".__('Connexion refusée')."</strong>";
        $message .= "<ul><li>".__('vérifiez vos informations de connexion (compte et mot de passe)').'</li>';
        $message .= "<li>".__('ou utilisez votre compte institutionnel.').'</li></ul>';

        if (session()->has('loginFailUrl')) {
            Helper::addAlert('warning', $message);
            return redirect(session()->get('loginFailUrl')[0]);
        }
        throw ValidationException::withMessages([
            $this->username() => [$message],
        ]);
    }

    /**
     * Get the post register / login redirect path.
     *
     * @return string
     */
    public function redirectTo()
    {
        $next = session('bindUrl', '/');
        if ('/' !== $next) {
            // Next is stronger than intended
            session()->forget('url.intended');
        }
        return $next;
    }

    /**
     * Check if the user authenticated by an external mechanism (CAS / API)
     * has homonyms and handle account binding if required
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public static function bindOrCreateAccount(Request $request)
    {
        $user = session()->get('user');
        $authType = session()->get('authType');
        $confirmUrl = session()->get('confirmUrl');
        $bindUrl = session()->get('bindUrl');
        $loginFailUrl = session()->get('loginFailUrl');

        $hasCas = $user->id_inst && \App\Institution::find($user->id_inst)->cas;

        return view(
            'auth.create-account',
            [
                'homonymes' => \App\User::getHomonymes($user),
                'hasCas' => $hasCas,
                'id_inst' => $user->id_inst,
                'confirmUrl' => $confirmUrl,
                'bindUrl' => $bindUrl,
                'authType' => $authType,
                'class_code' => session()->get('user_code'),
                'user' => $user,
            ]
        );
    }
}
