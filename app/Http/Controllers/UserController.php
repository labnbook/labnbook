<?php

namespace App\Http\Controllers;

use App\Teacher;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use App\Helper;
use App\Institution;
use App\TeacherTeam;

class UserController extends Controller
{
    /**
     * get a list of users
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public static function index(Request $request)
    {
        $data = $request->validate([
            'type' => 'in:teacher,student,both,follow|required',
            'include_my_teams' => 'boolean|required',
            'exclude_ids' => 'array|nullable',
            'exclude_ids.*' => 'int',
            'id_inst' => 'int|required',
            'id_report' => 'int|nullable',
            'search' => 'string|nullable',
            'page' => 'int|nullable',
            'pageSize' => 'int|nullable',
        ]);
        if (!isset($data['exclude_ids'])) {
            $data['exclude_ids'] = [];
        }
        if (!isset($data['search'])) {
            $search = '';
        } else {
            $search = $data['search'];
        }
        
        $user = Auth::user();
        $team_list = [];
        $display_all_inst = false;
        
        $report = null;
        $report_learners_ids = [];
        $mission_learners_ids = [];
        $class_learners_ids = [];
        $report_teacher_team_ids = [];
        $users_allowed_ids = [];
        if($data['id_report']) {
            $report = \App\Report::find($data['id_report']);
            $report_learners_ids = $report?$report->users()->pluck('user.id_user')->toArray():[];
            $report_teacher_team_ids = $report?$report->teachers()->pluck('id_teacher')->toArray():[];
            $mission = $report->allow_msg_id_mission?\App\Mission::find($report->allow_msg_id_mission):$report->mission;
            $mission_learners_ids = $mission?$mission->getLinkedLearners()->pluck('id_user')->toArray():[];
            $class = $report->allow_msg_id_class?\App\Classe::find($report->allow_msg_id_class):null;
            $class_learners_ids = $class?$class->users()->pluck('user.id_user')->toArray():[];
        }
        // Ensure current user is excluded
        if (!in_array($user->id_user, $data['exclude_ids'])) {
            $data['exclude_ids'][] = $user->id_user;
        }
        // Main users query
        $query_users = User::whereNotIn('user.id_user', $data['exclude_ids'])
            ->where(DB::raw("CONCAT(LOWER(user.user_name), ' ', LOWER(user.first_name))"), 'like', '%' . strtolower($search) . '%');
        
        $for_teacher = $user->hasTeacher() && !in_array($data['type'], ['follow', 'student']);
        
        if ($for_teacher) {
            // Teacher ask for users list
            $teacher = $user->teacher;
            $id_institution = null;
            // Get only the user from the defined institution
            if (intval($data['id_inst']) > 0) {
                abort_if(!$teacher->institutions->contains($data['id_inst']), 403);
                $id_institution = intval($data['id_inst']);
            } else if (intval($data['id_inst']) === 0) {
                $id_institution = $user->institution->id_inst;
            } else {
                $display_all_inst = true;
            }
            $inst_learners_ids = [];
            $inst_teachers_ids = [];
            if($id_institution) {
                // teachers from institution
                $inst_learners_ids = Institution::find($id_institution)->learners()->pluck('user.id_user')->toArray();
                // learners from institution
                $inst_teachers_ids = Institution::find($id_institution)->teachers()->pluck('teacher.id_teacher')->toArray();
            } else {
                // all teachers from all institution
                $inst_teachers_ids = Teacher::pluck('teacher.id_teacher')->toArray();
                // all learners from all institution
                $inst_learners_ids = User::whereNotIn('user.id_user', $inst_teachers_ids);
            }
            if (in_array($data['type'], ['teacher', 'both'])) {
                // Add teachers from my institution
                $users_allowed_ids = array_merge($users_allowed_ids, $inst_teachers_ids);
                // Get teacher teams
                if ($data['include_my_teams']) {
                    foreach ($teacher->teacherTeamsManaged() as $team) {
                        if (
                            $team->status !== TeacherTeam::STATUS_ARCHIVE &&
                            (
                                intval($data['id_inst']) < 0 ||
                                $team->id_inst === intval($data['id_inst'])
                            )
                        ) {
                            if(str_contains(strtolower($team->name), strtolower($search))) {
                                $team_list[] = [
                                    'id' => $team->id_teacher_team,
                                    'text' => $team->name,
                                    'type' => 'teacher_team'
                                ];
                            }
                        }
                    }
                }
            }
            if (in_array($data['type'], ['student', 'both'])) {
                // Add learners from selected institutions
                $users_allowed_ids = array_merge($users_allowed_ids, $inst_learners_ids);
            }
        } else {
            // Learner ask for users list
            // Learners can only see user linked to a conversation via a report
            abort_if(!$data['id_report'], 403);
            // User is not part of this report
            abort_if($user->cannot('view', $report), 403);
            // Get only the users from the learners institution
            $query_users = $query_users->where('id_inst', $user->institution->id_inst);
            // Get users ids from the report team
            if($report->allow_msg_team || $data['type'] === 'follow'){
                $users_allowed_ids = array_merge($users_allowed_ids, $report_learners_ids);
            }
            // Get users ids from the class
            if ($report->allow_msg_id_class || $data['type'] === 'follow') {
                $users_allowed_ids = array_merge($users_allowed_ids, $class_learners_ids);
            }
            // Get users ids from the mission
            if ($report->allow_msg_id_mission || $data['type'] === 'follow') {
                $users_allowed_ids = array_merge($users_allowed_ids, $mission_learners_ids);
            }
            // Add teachers ids from report
            if ($report->allow_msg_teacher || $data['type'] === 'follow') {
                $users_allowed_ids = array_merge($users_allowed_ids, $report_teacher_team_ids);
            }
        }
        // Get all users
        $paginatedUsers = $query_users
            ->whereIn('user.id_user', $users_allowed_ids)
            ->select(['user.*'])
            ->distinct('user.id_user')
            ->orderBy('user.user_name')
            ->paginate($data['pageSize'], ['*'], 'users', $data['page']);
        
        $user_list = collect($paginatedUsers->items())
            ->map(function ($u) use($display_all_inst, $for_teacher) {
                $user = User::find($u->id_user);
                return [
                    'id' => $user->id_user,
                    'firstName' => $user->first_name,
                    'userName' => $user->user_name,
                    'text' => $user->longDisplayName($display_all_inst, $for_teacher),
                    'type' => 'user'
                ];
            });
        
        return response()->json(['users' => $user_list, 'teams' => $team_list, 'searchTerm' => $search,
            'pagination' => ['page' => $paginatedUsers->currentPage(), 'more' => $paginatedUsers->hasMorePages()]]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        abort_if(Auth::user()->cannot('update', $user), 403);
        return response()->json([
                'success' => 1,
                'html' => view('user._edit')->with([
                    'user' => $user,
                    'inst_login' => !$user->isInternal(),
                    'inst_name' => $user->institution->name,
                    'ldap_cnx' => $user->hasLdapCnx(),
                ])->render(),
            ]);
    }

    /**
     * Creates a user
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public static function create(Request $request)
    {
        $data = $request->validate([
            'code' => 'string|nullable',
            'first_name' => 'string|nullable',
            'user_name' => 'string|nullable',
            'inst_number' => 'string|nullable',
            'email' => 'string|nullable',
            'login' => 'string|required',
            'password' => 'string|required',
            'password2' => 'string|required',
            'accept_cgu' => 'string|nullable',
            'id_inst' => 'int|nullable',
        ]);
 
        abort_if(!session('bindUrl', false), 403, __("Creation de compte non autorisée"));
        // Verify code
        if (isset($data['code']) && $data['code'] && 
            !\App\Classe::fromCode($data['code'])->exists()) {
            Helper::AddAlert(__("Le code fourni n'existe pas ou a expiré"));
            return redirect()->back();
        }
        $user = new \App\User();
        $user->fill($data);
        $user->cgu_accept_time = array_key_exists('accept_cgu', $data) && "on" === $data['accept_cgu'] ? now() : null;
 
        $error = $user->autoRegister(
            'lnb',
            $data['password'],
            $data['password2'],
            $data['code'] ?? null
        );
        if (!empty($error)) {
            Helper::AddAlert('danger', $error);
            return redirect()->back()->withInput($request->except('password'));
        }
        auth()->login($user);
        Helper::AddAlert('success', __("Votre compte LabNBook a été créé."));
        $bindUrl = session('bindUrl', false);
        if ($bindUrl) {
            session()->forget('url.intended');
            return redirect($bindUrl);
        }
        return redirect()->intended();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user) {
        abort_if(Auth::user()->cannot('update', $user), 403);

        $data = $request->validate([
            'user_name' => 'string|required',
            'first_name' => 'string|required',
            'email' => 'string|nullable',
            'login_edition' => 'boolean|required',
            'login' => 'required_if:login_edition,true|string|nullable',
            'password_edition' => 'boolean|required',
            'password' => 'required_if:login_edition,true|required_if:password_edition,true|string|nullable',
            'new_password' => 'required_if:password_edition,true|string|nullable',
            'new_confirm_password' => 'required_if:password_edition,true|same:new_password|string|nullable',
            'code' => 'string|nullable',
            'lang' => 'string|nullable',
        ]);
        $changes = [];
        $errors = [];
        $lang = \App\Helper::selectLanguage();

        if ($user->lang !== $data['lang']) {
            $user->lang = $data['lang'];
            $changes[] = __("langue");
            App::setLocale($data['lang']);
            $lang = $data['lang'];
        }

        $data['email'] = $data['email'] ?? '';
        
        if ($user->email !== $data['email']) {
            $u = false;
            if ($data['email'] !== '') {
                $u = User::where('email', $data['email'])->first();
            }
            if ($u) {
                $errors[] = __("l'e-mail fourni est déjà utilisé par un autre compte");
            } else {
                $changes[] = __("e-mail");
                $user->email = $data['email'];
            }
        }
        
        if (!$user->save()) {
            $errors = array_merge($errors, $changes);
            $changes = [];
        }
        
        if ($data['login_edition'] || $data['password_edition']) {
            $oldLogin = $user->login;
            $login = $data['login'];
            $new_password = $data['new_password'] ?? null;
            $modified = $user->updatePasswordLogin($new_password, $data['password'], $login);
            if ($login !== $oldLogin) {
                $message = __("identifiant");
                if ($modified['login']) {
                   $changes[] = $message;
                } else {
                    $errors[] = $message;
                }
            }
            if ($new_password) {
                $message = __("mot de passe");
                if ($modified['password']) {
                    $changes[] = $message;
                } else {
                    $errors[] = $message;
                }
            }
        }
        // Add to class
        $code = $data['code'] ?? null;
        if ($code) {
            $class = \App\Classe::FromCode($code)->first();
            if ($class) {
                $class->addLearner($user->id_user);
                $changes[] = __("l'utilisation du code :code a permis de vous ajouter à la classe de votre enseignant", ["code" => $code]);
            } else {
                $errors[] = __("le code :code n'est pas valide : vous n'avez pas été ajouté à la classe de votre enseignant", ["code" => $code]);
            }
        }

        return response(json_encode([
            "success"=>join('<br> - ', $changes),
            "error" => join('<br> - ', $errors),
            "lang" => $lang,
        ]), 200);
    }

    /**
     * Link a user to an external institution
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function bind(Request $request)
    {
        $data = session()->all();
        Helper::cleanBindSession();
        foreach (['id_user_ext', 'id_extplatform', 'role_ext', 'return_to'] as $field) {
            if (!array_key_exists($field, $data)) {
                Helper::AddAlert('warning', __("La page demandée n'est plus disponible, vous avez été redirigé."));
                return redirect('/');
            }
        }

        $user = Auth::User();
        $linked_user = User::fromExternal($data['id_user_ext'], $data['id_extplatform']);
        if ($linked_user && $linked_user->id_user != $user->id_user) {
            Auth::logout();
            Helper::AddAlert('danger', __("Cet utilisateur externe est déjà lié à un autre compte, nous vous avons déconnecté par sécurité, veuillez vous reconnecter en passant par Moodle"));
            return redirect()->back();
        }
        if ($user->bind($data['id_user_ext'], $data['id_extplatform'], $data['role_ext'])) {
            if ($user->hasTeacher()) {
                $message = __("Votre compte Moodle a maintenant été lié à un compte LabNBook. Vous pouvez retourner dans Moodle en changeant d'onglet dans votre navigateur.");
            } else {
                $message = __("Votre compte Moodle a maintenant été lié à un compte LabNBook. Bienvenue dans votre nouvelle plateforme.");
            }
            Helper::AddAlert('success', $message);
            return redirect()->away($data['return_to']);
        }
        if (!empty(config('cas.cas_hostname')) && config('cas.cas_hostname')!='none' && cas()->checkAuthentication()) {
            // Bind has failed because CAS session is openned
            // Mettre un message :
            // Vous essayez de connecter le compte Moodle (<login>) avec un comtpe <CASNAME> qui est déjà lié à ce Moodle.
            // Peut être est-ce due à une session <CASNAME> non fermée, dans ce cas vous pouvez [Vous déconnecter](bouton logout)
            $disconnectbtn = '<button class="lb_btn" onclick="$(\'#frm-logout\').submit()">' . __('vous déconnecter') . '</button>';
            $msgparams = [
                'login' => $data['login'],
                'casname' => config('labnbook.cas_institution_name'),
                'caslogin' => cas()->user(),
                'disconnectbtn' => $disconnectbtn
            ];
            $message = __("Vous essayez de lier le compte Moodle (:login) avec le compte :casname (:caslogin) qui est déjà lié à un autre compte Moodle.<br />Peut être est-ce due à une session :casname non fermée, dans ce cas vous pouvez :disconnectbtn puis réessayer.", $msgparams);

            // //cas()->logout();
        } else {
            $message = __("Echec du lien utilisateur");
        }
        Helper::AddAlert('danger', $message);
        return redirect()->back();
    }

    /**
     * Link a user to an external institution
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function preBind(Request $request)
    {
        $data = session()->all();
        foreach (['login', 'id_extplatform', 'user_name', 'first_name', 'email', 'role_ext', 'return_to'] as $field) {
            if (!array_key_exists($field, $data)) {
                Helper::AddAlert('warning', __("La page demandée n'est plus disponible, vous avez été redirigé."));
                return redirect('/');
            }
        }
        $user = User::fromExternal($data['id_user_ext'], $data['id_extplatform']);
        if ($user) {
            // There is already a user connected to this extenal user, skip binding
            auth()->login($user);
            return redirect()->away($data['return_to']);
        }
        $extplatform = \App\Extplatform::find($data['id_extplatform']);
        $user = new \App\User();
        $inst_number = isset($data['inst_number']) ? $data['inst_number'] : null;
        $user->fill([
            'login' => $data['login'],
            'user_name' => $data['user_name'],
            'first_name' => $data['first_name'],
            'email' => $data['email'],
            'inst_number' => $inst_number,
            'id_inst' => $extplatform->id_inst,
        ]);
        auth()->logout();
        // Logout has cleaned the sessions, restore data required for bind or create
        session()->put([
            'id_extplatform' => $data['id_extplatform'],
            'id_user_ext' => $data['id_user_ext'],
            'login' => $data['login'],
            'first_name' => $data['first_name'],
            'user_name' => $data['user_name'],
            'email' => $data['email'],
            'inst_number' => $inst_number,
            'role_ext' => $data['role_ext'],
            'return_to' => $data['return_to'],
            'user' => $user,
            'authType' => 'Moodle',
            'confirmUrl' => '/user/create',
            'bindUrl' => '/user/bind/',
            'createAllowed' => true,
            // We need to forward the xsrf token to avoid errors during the binding process
            '_token' => $data['_token'],
            'auth_method' => isset($data['auth_method']) ? $data['auth_method']  : null,
            '_old_input' => isset($data['_old_input']) ? $data['_old_input'] : null,
        ]);
        if (array_key_exists('alerts', $data)) {
            session()->put('alerts', $data['alerts']);
        }
        return \App\Http\Controllers\Auth\LoginController::bindOrCreateAccount($request);
    }

    /**
     * Retourne les initiales de l'utilisateur demandé
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getInitials(Request $request)
    {
        $data = $request->validate([
            'id_user'   => 'int|required'
        ]);

        return response()->json(User::getInitials($data['id_user']));
    }

    /* Check if a user can be soft deleted
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
    */
    public function canSoftDeleteLearner(Request $request, User $user)
    {
        abort_if(Auth::user()->cannot('manage', $user), 403);
        return response()->json($user->canSoftDeleteLearner());
    }

    /*
    * désinscrit un étudiant de la classe et le supprime si il n'est plus isncrit nul part
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
    */
    public function deleteLearnerFromClasse(Request $request, User $user)
    {
        $data = $request->validate(['id_class' => 'int|required']);
        abort_if(Auth::user()->cannot('manage', $user), 403);
        return response()->json($user->deleteLearnerFromClasse($data['id_class']));
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
    */
    public function getUserWithInstBy(Request $request)
    {
        // TODO Policy ?
        $data = $request->validate([
            'id_inst' => 'int|required',
            'what' => 'string|required',
            'var1' => 'string|nullable',
            'var2' => 'string|nullable',
            'type' => 'string|nullable',
        ]);
        abort_if(!$data['var1'] && ! $data['var2'], 422);
        // TODO add search by type  
        return response()->json(User::getUserWithInstBy($data['what'], $data['id_inst'], $data['var1'], $data['var2']));
    }

    /**
     * Saves the fact that the user just accepted the conditions
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
    */
    public function acceptConditions(Request $request)
    {
        $user = Auth::user();
        $user->cgu_accept_time = now();
        return response()->json($user->save());
    }

    /**
     * Get potential homonymes
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
    */
    public function getHomonymes(Request $request)
    {
        $data = $request->validate([
            'id_inst' => 'int|required',
            'first_name' => 'string|nullable',
            'user_name' => 'string|nullable',
            'email' => 'string|nullable',
            'inst_number' => 'int|nullable',
            'login' => 'string|nullable',
        ]);
        return response()->json(User::getHomonymes((object)$data));
    }
}
