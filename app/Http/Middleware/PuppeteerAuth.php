<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;

class PuppeteerAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $ips = array_merge(['127.0.0.1', '::1'], config('labnbook.ips'));

        $token = $request->input('puppeteer');
        if (empty($token) || !in_array($request->ip(), $ips)) {
            // This route is only for puppeteer for authorized IPS
            Log::Error("PuppeteerAuth failed empty token or bad IP, token '{$token}' ip {$request->ip()}");
            abort(401);
        }

        $id_user = (int)$request->input('id_user');
        $user = \App\User::find($id_user);
        if (!$user || !$user->validateRememberToken($token)) {
            Log::Error("PuppeteerAuth failed user not found or bad remembertoken, id_user '{$id_user}'");
            abort(401);
        }
        auth()->login($user);

        return $next($request);
    }
}
