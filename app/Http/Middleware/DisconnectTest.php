<?php

namespace App\Http\Middleware;

use Closure;

class DisconnectTest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check() && auth()->id() == config('labnbook.id_user_test')) {
            auth()->logout();
            \App\Helper::addAlert("warning", __("La page demandée n'est pas accessible en mode test, veuillez vous connecter."));
            return redirect('/');
        }
        return $next($request);
    }
}
