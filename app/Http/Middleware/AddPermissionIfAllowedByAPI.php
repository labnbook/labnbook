<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Throwable;
use \App\TeamConfig;
use \App\Mission;

class AddPermissionIfAllowedByAPI
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $path = session()->pull('allowed_by_api', null);
        $permission_infos = session()->pull('api_ask_permission_infos', []);
        $user = Auth::user();
        $full = $request->FullUrl();
        $root = $request->root();
        $requestpath = str_replace($root, '', $full);
        if (!empty($permission_infos)) {
            if ((int)$request->input('ask_permission') === 1) {
                switch ($permission_infos['type']) {
                    case 'mission':
                        $mission = Mission::find($permission_infos['id']);
                        $mission->linkTeacher($user->id_user);
                        break;
                    case 'team_config':
                        $user->makeTutor([$permission_infos['id']]);
                        break;
                    case 'classe':
                        $classe = TeamConfig::find($permission_infos['id'])->classe;
                        $classe->linkTeacher($user);
                        break;
                }
            } elseif ($permission_infos['expires'] === 1) {
                // Allow one redirection with this information in session not more
                $permission_infos['expires'] = 0;
                session()->put('api_ask_permission_infos', $permission_infos);
            }
        }
        if ($path && ($path ==  $requestpath || $requestpath == preg_replace('/#teamconfig=[0-9]*$/', '', $path))) {
            if (preg_match('/#teamconfig=([0-9]*)$/', $path, $matches)) {
                // Inject id_team_config as a query to trigger 403 error if required
                $request->merge(['id_team_config' => $matches[1]]);
            }
            try {
                session()->flash('dont_render_403_rethrow', true);
                return $next($request);
            } catch (Throwable $exception) {
                // Exception are handled by exception handler, if we are here, it means that it rethrowed it to us
                // 2. set in sessios 'api_ask_permission' => ['type' => 'team_config|mission|classe', 'id' => 'id']
                $type = null;
                $connector = '?';
                $message = __("Vous n'êtes pas enseignant de la classe et/ou tuteur de la mission LabNBook associées à cette activité Moodle. Voulez-vous le devenir ?");
                $anchor = '';
                if (preg_match('@^' . $root . '/teacher/reports\?team_config=([0-9]*)@', $full, $matches)) {
                    $connector = '&';
                    $type = 'team_config';
                } elseif (preg_match('@^/teacher/students#teamconfig=([0-9]*)@', $path, $matches)) {
                    // In this specific case we use path to retrive the data as it is not accessible from the request
                    $id = $matches[1];
                    if ($id == $request->input('id_team_config', -1)) {
                        // Ensure that the requested id is the same as the one in the path
                        $type = 'team_config';
                        $anchor = '#teamconfig=' . $id;
                    }
                } elseif (preg_match('@^' . $root . '/teacher/students\?id_team_config=([0-9]*)@', $full, $matches)) {
                    $type = 'classe';
                    $connector = '&';
                    $message = __("Vous n'êtes pas enseignant de la classe LabNBook associée à cette activité Moodle. Voulez-vous le devenir ?");
                } elseif (preg_match('@^' . $root . '/teacher/mission/([0-9]*)@', $full, $matches)) {
                    $id = $matches[1];
                    $type = 'mission';
                    $message = __("Vous n'êtes pas tuteur de la mission LabNBook associée à cette activité Moodle. Voulez-vous le devenir ?");
                }
                if ($type != null) {
                    $infos = [
                        'type' => $type,
                        'id' => $matches[1],
                        'valid_url' => $request->FullUrlWithQuery(['ask_permission' => 1]) . $anchor,
                        'message' => $message,
                        'expires' => 1
                    ];
                    session()->put('api_ask_permission_infos', $infos);
                }
            }
        }
        return $next($request);
    }
}
