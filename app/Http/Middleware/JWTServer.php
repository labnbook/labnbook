<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use \Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use App\Extplatform;
use App\User;
use Lcobucci\JWT\Validation\Constraint\SignedWith;
use Lcobucci\JWT\Validation\Constraint\ValidAt;
use Lcobucci\Clock\FrozenClock;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Key\InMemory;
use Lcobucci\JWT\Signer\Hmac\Sha256;

class JWTServer
{

    /** @var array fields that MUST be in all jwt */
    private $requiredFields = ['iss'=>0, 'iat'=>0, 'sub'=>0, 'dest'=>0, 'orig'=>0];

    /** @var \App\User */
    private $user = null;

    /** @var int */
    private $extplatform = null;

    /** @var int ttl seconds */
    private $ttl;

    /** @var int leeway seconds (clock skew)*/
    private $leeway;


    public function  __construct(){
        $this->ttl = config('labnbook.token_ttl', 600);
        $this->leeway = config('labnbook.token_leeway', 20);
    }

    /**
     * Send a json error message
     * @param int code
     * @param string message
     */
    private function fail($code, $message){
        return response()->json(['message'=>$message],$code);
    }

    /**
     * Retrieve extplatform secret from jwt payload
     * @param array $payload the jwt payload
     * @return string|null secret
     */
    private function getExtplatformSecret($payload){
        $extplatform = Extplatform::find($payload['iss']);
        if(!$extplatform){
            // Iss is not a valid id_extplatform
            return null;
        }
        $secret=$extplatform->secret;
        if(!$secret){
            // Iss is an extplatform without secret i.e not an external provider
            return null;
        }
        return $secret;
    }

    /**
     * Find the DB user matching the JWT payload
     *
     * @param array $payload
     * @return User|null
     */
    private static function findUser(array $payload) : ?User
    {
        return User::fromExternal($payload['sub'], $payload['iss']);
    }

    /**
     * Retrieve user secret from jwt payload
     *
     * @param \App\User $user
     * @param array $payload the jwt payload
     * @return string secret
     */
    private function getUserSecret(User $user, array $payload) : string {
        if(!array_key_exists('token_exp', $payload)){
            throw new JWTException('Missing token_exp');
        }
        $now = \Carbon\Carbon::now()->getTimestamp();
        if($payload['token_exp'] <= $now){
            throw new JWTException('User token expired');
        }
        return $this->userToken(
            $user,
            $payload['token_exp'],
            $payload['iss']
        )['token'];
    }

    /**
     * Generate a sharable token from a private api token and a timestamp
     * @param \App\User $user
     * @param int|null timestamp
     * @param int extplatform
     * @return array
     */
    public function userToken($user, $timestamp, $extplatform){
        if($timestamp == null){
            $timestamp =\Carbon\Carbon::now()->addSeconds($this->ttl)->getTimestamp();
        }
        $key = \hash('sha256', $user->login."_".$user->email);
        return [
            'token' => \hash('sha256', "$key.$timestamp.$extplatform"),
            'token_exp' => $timestamp
        ];
    }

    /**
     * Configures the JWT parser
     */
    private function getJWTConfiguration($key)
    {
        return Configuration::forSymmetricSigner(
            new Sha256(),
            InMemory::plainText($key)
        );
    }

    /**
     * Validate JWT payload and returns it
     * @return array tuple [array JWT payload, ?User]
     */
    private function validateJWT($token, $path){
        # First parser we do not know who is talking yet so we do not
        # know which key to use, we provide 'nokey' argument to avoid errors
        $config = $this->getJWTConfiguration('nokey');
        try {
            $jwt = $config->parser()->parse($token);
        } catch (\Throwable $e) {
            throw new JWTException("Missing or corrupted token");
        }
        $payload = $jwt->claims()->all();
        if(array_key_exists('data', $payload)){
            $payload['data'] = (array)$payload['data'];
        }else{
            $payload['data'] = [];
        }

        // Check all required fields are present
        $missings = array_diff_key($this->requiredFields, $payload);
        if(!empty($missings)){
            throw new JWTException("Missing required field(s) ".
                json_encode(array_keys($missings)));
        }

        $now =\Carbon\Carbon::now()->getTimestamp();
        // Checkh that token is not expired
        $iat = new \Carbon\Carbon($payload['iat']);
        $expireTms = $iat->addSeconds($this->ttl)->getTimestamp();
        if ($now > $expireTms) {
            throw new JWTException('Token expired');
        }
        $iat = new \Carbon\Carbon($payload['iat']);
        if ($iat->getTimestamp() > $now) {
            throw new JWTException('Token emmited in the future');
        }


        // Check payload path is allowed
        if (ltrim($payload['dest'], '/') !== ltrim($path, '/')) {
            throw new JWTException("Token not authorized for the path $path");
        }

        // Retrieve secret
        if($payload['orig'] == 'inst'){
            $secret = $this->getExtplatformSecret($payload);
            $user = null;
        }else{
            $user = self::findUser($payload);
            if (!$user) {
                throw new JWTException('User not found');
            }
            $secret = $this->getUserSecret($user, $payload);
        }
        if(!$secret){
            throw new JWTException('Cannot retrieve secret key for '.$payload['orig']);
        }

        // Verify signature
        $config = $this->getJWTConfiguration($secret);
        $jwt = $config->parser()->parse($token);
        $config->setValidationConstraints(
            new SignedWith($config->signer(), $config->verificationKey()),
        );
        $constraints = $config->validationConstraints();
        if (!$config->validator()->validate($jwt, ...$constraints)) {
            throw new JWTException('Bad signature');
        }

        return [$payload, $user];
    }

    /**
     * Authenticate this request for a given User, using the auth guard.
     *
     * @param Request $request
     * @param User $user
     */
    private static function authenticate(Request $request, User $user) {
        // Set "token" property expected by the auth guard 'jwtserver'
        $request->token = $user->id_user;
    }

    /**
     * Handle a request :
     *  1. Validate JWT
     *      1. Authenticate source server
     *      2. Authenticate user (if any)
     *      3. Validate data signature
     *  2. Replace request's data with trusted JWT payload
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return  mixed
     */
    private function handleRequest($request, Closure $next)
    {
        // Validate and retrieve JWT Payload
        try {
            $token = $request->bearerToken();
            if ($token == null) {
                $token = $request->input('token', '');
            }
            $apiPath = preg_replace('/^api/', '', $request->path());
            list ($payload, $user) = $this->validateJWT($token, $apiPath);
            if ($user) {
                self::authenticate($request, $user);
            }
        } catch (JWTException $e) {
            Log::Warning('JWT Exception while authenticating via API: "'.$e->getMessage().'"');
            return $this->fail(401, $e->getMessage());
        } catch (\InvalidArgumentException $e) {
            Log::Error('Error validating token while authenticating via API: "'.$e->getMessage().'"');
            return $this->fail(401, 'Missing or corrupted token');
        }

        $this->extplatform = (int) $payload['iss'];

        // Replace request input by trusted data
        $payload['data']['id_extplatform'] = $payload['iss'];
        $payload['data']['id_user_ext'] = $payload['sub'];
        $request->replace($payload['data']);

        return $next($request);
    }

    /**
     * Handle a response :
     *  1. Logout if required
     *  2. Add auth token if required
     * @param  \Illuminate\Http\JsonResponse  $response
     * @return  \Illuminate\Http\JsonResponse
     */
    public function handleResponse(JsonResponse $response) {
        $payload = ['data' => $response->getData(true)];
        if (!empty($payload['logout'])) {
            unset($payload['data']['logout']);
        } else if (auth('api')->check()) {
            // authenticated as user, so send a new secret for JWT
            $payload['auth'] = $this->userToken(auth('api')->user(), null, $this->extplatform);
        }
        $response->setData($payload);
        return $response;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $this->handleRequest($request, $next);
        if ($response instanceof JsonResponse) {
            return $this->handleResponse($response);
        } else {
            // HTML response does not have getData method
            return $response;
        }
    }
}

class JWTException extends \Exception {};
