<?php

namespace App\Http\Middleware;

use App\Helper;
use Closure;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Support\Facades\Log;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
    ];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     *
     * @throws \Illuminate\Session\TokenMismatchException
     */
    public function handle($request, Closure $next)
    {
        try {
            return parent::handle($request, $next);
        } catch (TokenMismatchException $e) {
            $url = $request->path();
            switch (config('labnbook.csrf_level')) {
                case 'log':
                    if ($request->ajax()) {
                        Log::Warning("Missing or expired CSRF on ajax query to /$url");
                    }
                    // Log then ignore
                case 'ignore':
                    // Mark the path as ignored and restart kernel
                    $this->except[] = $url;
                    return parent::handle($request, $next);
                default:
                    if ($request->getRequestUri() === '/logout') {
                        /* When the session has expired we send a POST with and expired CSRF
                        * to '/logout' thus Laravel throws a 419 instead of loging the user
                        * out and redirecting to '/'
                        * See https://laracasts.com/discuss/channels/laravel/the-page-has-expired-due-to-inactivity-when-logout
                        */
                        auth()->logout();
                        Helper::AddAlert('warning', __('Votre session a expiré, veuillez vous reconnecter'));
                        return redirect('/');
                    } elseif (in_array('auth', $request->route()->gatherMiddleware())
                        && !auth()->id()) {
                        /*
                         * Laravel checks CSRF before auth but if the CSRF mismatch is
                         * due to an auth expiration, we'd rather return a 401 than a 419
                         */
                        return $request->ajax() ? response()->json("", 401) : redirect('/');
                    }
                    throw $e;
            }
        }
    }
}
