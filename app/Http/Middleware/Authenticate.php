<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    {
        if (!$request->expectsJson()) {
            if (!in_array($request->path(), ['/', '/home', '/login'])) {
                \App\Helper::addAlert('danger', __("Vous devez vous connecter pour accéder à cette page"));
            }
            return route('login');
        }
    }
}
