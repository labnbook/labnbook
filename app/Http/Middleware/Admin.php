<?php

namespace App\Http\Middleware;

use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!auth()->user()->isAdmin()) {
            if (!request()->ajax()) {
                echo redirect('/');
            }
            abort(401, __("Vous n'avez pas l'autorisation de voir cette page"));
        }
        return $next($request);
    }
}
