<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\QueryException;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Helper;
use App\Mail\MarkdownMail;

/**
 * App\User
 *
 * @property int $id_user
 * @property string $user_name
 * @property string $first_name
 * @property string $login
 * @property mixed|null $pwd
 * @property string $email
 * @property string|null $lang
 * @property int $id_inst
 * @property string|null $inst_number
 * @property int|null $current_report
 * @property int|null $last_synchro
 * @property string|null $reset_pwd
 * @property string $creation_time
 * @property string|null $update_time
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Classe[] $classes
 * @property-read int|null $classes_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Conversation[] $conversations
 * @property-read int|null $conversations_count
 * @property-read \App\Institution $institution
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Message[] $messages
 * @property-read int|null $messages_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Report[] $reports
 * @property-read int|null $reports_count
 * @property-read \App\Teacher|null $teacher
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Extplatform[] $extplatforms
 * @property-read int|null $extplatforms_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User connected($report = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User internal($internal = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreationTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCurrentReport($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereIdInst($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereInstNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereLang($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereLastSynchro($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereLogin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePwd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereResetPwd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUserName($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User admin($admin = true)
 * @property string|null $cgu_accept_time
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCguAcceptTime($value)
 */
class User extends Authenticatable
{
    use Notifiable;
    use \Lab404\Impersonate\Models\Impersonate;

    protected $table = 'user';
    protected $primaryKey = 'id_user';
    public $timestamps = false;

    const CREATED_AT = 'creation_time';
    const UPDATED_AT = 'update_time';

    const PASSWORD_NOT_VALID = -1;
    const PASSWORD_COUPLE_NOT_UNIQUE = -2;
    const SAVE_ERROR = -3;
    const PASSWORD_CHANGED = 1;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'login', 'first_name', 'user_name', 'email', 'id_inst', 'inst_number', 'cgu_accept_time'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'pwd',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * User role 'admin', 'teacher', or 'learner'
     *
     * @var string
     */
    protected $role = null;

    /**
     * The column name of the "remember me" token.
     *
     * @var string
     */
    protected $rememberTokenName = 'reset_pwd';

    /**
     * Returns the hashed password
     *
     * @return string : the hashed password
     */
    public function getAuthPassword()
    {
        return $this->pwd;
    }

    /**
     * Returns the field used to store the password
     *
     * @return string : the password field name
     */
    public static function getAuthPasswordName()
    {
        return 'pwd';
    }

    /**
     * Returns the reports linked to the user
     *
     * @return BelongsToMany
     */
    public function reports()
    {
        return $this->belongsToMany('App\Report', 'link_report_learner', 'id_user', 'id_report');
    }

    /**
     * Returns the conversations linked to the user
     *
     * @return BelongsToMany
     */
    public function conversations()
    {
        return $this->belongsToMany('App\Conversation', 'link_conversation_user', 'id_user', 'id_conversation');
    }
    
    /**
     * Returns the ressources linked to the user
     *
     * @return BelongsToMany
     */
    public function ressources()
    {
        return $this->belongsToMany('App\Resource', 'link_ressource_user', 'id_user', 'id_ressource');
    }


    /**
     * Returns the messages send by the user
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function messages()
    {
        return $this->hasMany('App\Message', 'id_user_sender');
    }

    /**
     * Returns the personal task of the user
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function tasks()
    {
        return $this->hasMany('App\Task', 'id_user_creator', 'id_user')->whereNull('id_report');
    }


    /**
     * Returns the messages send by the user
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function comments()
    {
        return $this->hasMany('App\Comment', 'id_user');
    }

    /**
     * Returns the user widgets configuration 
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function widgetConfig()
    {
        return $this->hasOne('App\UserWidgetConfig', 'id_user', 'id_user');
    }

    /**
     * Returns the teacher linked to the user if any
     * @return \App\Teacher
     */
    public function teacher(){
        return $this->hasOne('App\Teacher', 'id_teacher', 'id_user');;
    }

    /**
     * Returns the role of the user
     * @return string 'admin'|'teacher'|'learner'
     */
    public function getRole()
    {
        if ($this->role == null) {
            $this->role = ($this->teacher ? ($this->teacher->role == 'admin' ? 'admin' : 'teacher') : 'learner');
        }
        return $this->role;
    }

    /**
     * Returns the translate string for a role
     * @param string $role
     * @return string
     */
    public static function roleName($role)
    {
        switch ($role) {
            case 'admin':
                return __('admin');
            case 'manager':
                return __('gestionnaire');
            case 'teacher':
                return __('enseignant');
            default:
                return __('étudiant');
        }
    }

    public function getDetailedRole()
    {
        $role = $this->getRole();
        if ($role == 'teacher' && $this->teacher->isManager()) {
            return 'manager';
        }
        return $role;
    }


    /**
     * Returns true if the user is a learner
     * @return boolean
     */
    public function isLearner(){
        return $this->getRole() == 'learner';
    }

    /**
     * Returns true if the user is a teacher
     * @return boolean
     */
    public function hasTeacher(){
        return $this->teacher != null;
    }

    /**
     * Returns true if the user is a teacher
     * @return boolean
     */
    public function isTeacher(){
        return $this->getRole() == 'teacher';
    }

    /**
     * Determines if the user is a teacher for a given report
     * @param \App\Report $report
     * @return boolean
     */
    public function isReportTeacher($report)
    {
        return $this->hasTeacher() && $this->teacher->isReportTeacher($report);
    }
    
    /**
     * Determines if the user is a teacher for a given report
     * @param \App\Report $report
     * @return boolean
     */
    public function isReportLearner($report)
    {
        return $report->users()->whereKey($this->id_user)->exists();
    }
    
    /**
     * Determines if the user is a super manager for the specific simulation code
     * @return boolean
     */
    public function isSimulationManager()
    {
        return DB::table('simulation_link_super_manager')
            ->where('id_super_manager', $this->id_user)->exists();
    }

    /**
     * Get all related simulation code to the user
     * @return \Illuminate\Support\Collection
     */
    public function getSimulationsRelated()
    {
        return DB::table('simulation_link_super_manager')
            ->where('id_super_manager', $this->id_user)->get('simulation_code');
    }

    /**
     * Return true if a user can edit the simulation
     * @param $simulation_code
     * @return bool
     */
    public function canEditSimulation($simulation_code)
    {
        return DB::table('simulation_link_super_manager')
            ->where('id_super_manager', $this->id_user)
            ->where('simulation_code', $simulation_code)->exists();
    }
    
    /**
     * Can the user impersonnate another user
     * @return bool
     */
    public function canImpersonate()
    {
        return $this->isAdmin();
    }

    /**
     * Return true if the user is an admin
     * @return boolean
     */
    public function isAdmin()
    {
        return $this->getRole() == 'admin';
    }

    /**
     *
     * Returns the classes in which the user belongs
     * @return BelongsToMany
     */
    public function classes(){
        return $this->belongsToMany('App\Classe', 'link_class_learner', 'id_user', 'id_class');
    }

    /*
     * Returns the user's institution
     * @return \App\Institution
     */
    public function institution(){
        return $this->belongsTo('App\Institution', 'id_inst');
    }

    /**
     * @return BelongsToMany
     */
    public function simulationTaskTypes(): BelongsToMany
    {
        return $this->belongsToMany('App\SimulationTaskType', 'simulation_link_learner_task', 'id_learner', 'id_task_type')->withPivot('feedback_level', 'knowledge', 'update_number');
    }

    /*
     * Returns the user's external platform
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function extplatforms()
    {
        return $this->belongsToMany('App\Extplatform', 'link_extplatform_user', 'id_user', 'id_extplatform');
    }

    /**
     * Scope a query to only admin users.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param bool $admin=true
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAdmin($query, $admin = true)
    {
        // TODO fixme
        $op = $admin ? '=' : '<>';
        return $query->where('id_user', $op, 1);
    }

    /**
     * Scope a query to only include internal users.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param bool $internal=true
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeInternal($query, $internal = true)
    {
        return $query->whereNull($this->getAuthPasswordName(), 'and', $internal);
    }

    /**
     * Return true if user uses institution login
     * @return boolean
     */
    public function isInternal()
    {
        $field = $this->getAuthPasswordName();
        return $this->$field != null;
    }

    /**
     * Scope to query only connected users
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param bool $report = false should we test on link_report_learner
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeConnected($query, $report = false)
    {
        return $query->whereRaw($this->connectedSqlString($report));
    }

    /**
     * Return the sql string to test if a user is connected
     * @param bool $report = false should we test on link_report_learner
     * @param bool $short = false should we use short table name ('u' and 'lrl')
     */
    public static function connectedSqlString($report = false, $short = false)
    {
        $table = $report ? ($short ? 'lrl' : 'link_report_learner') : ($short ? 'u' : 'user');
        return time() . ' - '.$table.'.last_synchro <= '. 1.4*config('labnbook.refresh_period');
    }

    /**
     * Return true if user has ldap connection
     * @return boolean
     */
    public function hasLdapCnx()
    {
        return \App\Processes\Ldap\Ldap::hasInstitute($this->id_inst);
    }

    /**
     * Return the internal user for an external user id and provider
     * @param $ids string|array external user identifier
     * @param $ext_platform int external provider id
     * @return \Illuminate\Database\Eloquent\Collection|\App\User[]|\App\User|null
     */
    public static function fromExternal($ids, $ext_platform)
    {
        $getter = 'get';
        if (!is_array($ids)) {
            // Retro compatibility
            $ids = [$ids];
            $getter = 'first';
        }
        return self::select('user.*')
            ->join('link_extplatform_user as leu', 'leu.id_user', 'user.id_user')
            ->where('id_extplatform', $ext_platform)
            ->whereIn('leu.id_user_ext', $ids)
            ->$getter();
    }

    /**
     * Should I accept the CGU ?
     */
    public function mustAcceptCGU()
    {
        return config('labnbook.cgu_url') && $this->cgu_accept_time == null;
    }

    /**
     * Link the current user to an external provider
     * @param $ext_user string external user identifier
     * @param $ext_provider int external provider id
     * @param $role string 'learner'|'teacher'
     * @return bool
     */
    public function bind($ext_user, $ext_provider, $role)
    {
        $now = \Carbon\Carbon::now();
        // Add link to external
        try {
            $ret = DB::table('link_extplatform_user')->insert([
                'id_user_ext' => $ext_user,
                'id_user' => $this->id_user,
                'id_extplatform' => $ext_provider,
                'created_at' => $now,
                'updated_at' => $now,
            ]);
        } catch (QueryException $e) {
            return false;
        }
        if ($role == 'teacher') {
            $id_inst = \App\Extplatform::find($ext_provider)->id_inst;
            $this->grantTeacher([$id_inst], 'auto-inscription API');
        }
        return true;
    }


    /**
     * Remove the link between the current user and an external provider
     * @param $ext_user string external user identifier
     * @param $ext_provider int external provider id
     * @param $role string 'learner'|'teacher'
     * @return bool
     */
    public function unbind($ext_provider)
    {
        return DB::table('link_extplatform_user')
            ->where('id_user', $this->id_user)
            ->where('id_extplatform', $ext_provider)
            ->delete();
    }


    /**
     * make the user an external user updating the given fields
     * @param array $fields
     * @return bool
     */
    public function switchToCas($fields)
    {
        $fields[$this->getAuthPasswordName()] = null;
        foreach ($fields as $field => $value) {
            $this->$field = $value;
        }
        return $this->save();
    }

    /**
     * Make user tutor of the givent missions
     * @param array $id_teamconfigs
     * @return boolean
     */
    public function makeTutor($id_teamconfigs)
    {
        $teamconfigs = \App\TeamConfig::find($id_teamconfigs);
        $inserts = [
            'link_mission_teacher' => [],
            'link_class_teacher' => [],
        ];
        $insert_classes = [];
        DB::beginTransaction();
        foreach ($teamconfigs as $tc) {
            if (!$this->teacher->missions()->whereKey($tc->id_mission)->exists()) {
                $key = $tc->id_mission . '_' . $this->id_user;
                $inserts['link_mission_teacher'][$key] = [
                    'id_mission' => $tc->id_mission,
                    'id_teacher' => $this->id_user,
                    'teacher_type' => 'teacher',
                ];
            }
            if (!$this->teacher->classes()->whereKey($tc->id_class)->exists()
                && !in_array($tc->id_class, $insert_classes)) {
                $key = $tc->id_class . '_' . $this->id_user;
                $inserts['link_class_teacher'][$key] = [
                    'id_teacher' => $this->id_user,
                    'id_class' => $tc->id_class,
                ];
                $insert_classes[] = $tc->id_class;
            }
        }
        foreach ($inserts as $table => $values) {
            if (!DB::table($table)->insert($values)) {
                DB::rollback();
                return false;
            }
        }
        DB::commit();
        return true;
    }

    /**
     * Make this user a teacher in the given institutions.
     *
     * @param array $institutionsIds
     * @param string|null $comment
     * @return boolean was there a change in DB
     */
    public function grantTeacher(array $institutionsIds = [], ?string $comment = null)
    {
        $instIds = empty($institutionsIds) ? [$this->id_inst] : $institutionsIds;
        $changes = false;
        // Create teacher if required
        if (!$this->hasTeacher()) {
            $this->teacher()->save(new \App\Teacher(["teacher_comment" => $comment]));
            // Load required because teacher has already been accessed
            $this->load('teacher');
            $changes = true;
        }
        // Link teacher to institutions
        $institutions = \App\Institution::find($instIds);
        foreach (\App\Institution::find($instIds) as $inst) {
            if (!$this->teacher->institutions()->whereKey($inst->id_inst)->exists()) {
                // Add the teacher to the institution
                $this->teacher->addInstitution((int)$inst->id_inst);
                $changes = true;
            }
            if ($inst->teacherClasse != null
                && !$inst->teacherClasse->users()->whereKey($this->id_user)->exists()) {
                // The institution has an auto registration class and we are not part of it
                $inst->teacherClasse->users()->attach($this);
                $changes = true;
            }
        }
        return $changes;
    }


    /**
     * Return true if the user is designer of the mission
     * @param \App\Mission $mission
     * @return boolean
     */
    public function isDesigner($mission)
    {
        return $mission->designers()->whereKey($this->id_user)->first() != null;
    }

    /**
     * Return true if the user is currently connected
     * @param \App\Report $report
     * @return boolean
     */
    public function isConnected($report = null)
    {
        $last = $report ? $report->getLastVisit($this->id_user) : $this->last_synchro;
        return time() - $last <= 2 * config('labnbook.refresh_period');
    }

    /**
     * Returns true if the user is a teacher in external inst
     * @param int $id_extplatform external platform
     * @return bool
     */
    public function hasTeacherOfExtplatform($id_extplatform)
    {
        return $this->hasTeacher()
            && $this->extplatforms()->where('extplatform.id_extplatform', $id_extplatform)->exists();
    }

    /**
     * Returns the test user
     * @return \App\User
     */
    public static function getTestUser()
    {
        $id = config('labnbook.id_user_test');
        if (!$id) {
            return;
        }
        $user = User::find($id);
        if (!$user) {
            $user = new User();
            $user->id_user = $id;
            $user->first_name = 'étudiant';
            $user->user_name = 'test';
            $user->login = 'etest';
            $field = self::getAuthPasswordName();
            $user->$field = '43acfd580043d89babe2e2f973ad3c6937c0024360b5c6afd3a52fbc94dcd408';
            $user->id_inst = 1;
            $user->save();
        }
        return $user;
    }

    /*************************************************************************/
    /*                      Password management                              */
    /*************************************************************************/

    /**
     * Returns the legacy hash for a plain password
     *
     * @param  String   plain the password to hash
     * @return String   the legacy hash
     */
    protected function legacyHash($plain)
    {
        return hash("sha256", str_replace(["'", '"'], ["\\'", '\\"'], $plain));
    }

    /**
     * génère une chaine de caractère aléatoire pour la création d'un mot de passe (entre autre)
     *
     * @param int $length (opt) taille de la chaine de caractère à générer
     * @return string une chaine de caractère aléatoire de taille fixée
     */
    public static function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $pass = '';
        while (!$pass || !self::validPassword($pass)) {
            // Le mot de passe pourrait ne pas contenir tous les types de caractères demandés du premier coup
            $pass = substr(str_shuffle($characters), 0, $length);
        }
        return $pass;
    }

    /*
     * Returns true if $plain is the user password
     * Side effects : Reshashes the password if required
     * @param string $Plain
     */
    public function checkPassword($plain)
    {
        $pwd = $this->getAuthPassword();
        if (Hash::needsRehash($pwd)) {
            // Password has not been hashed by current hasher
            if ($this->legacyHash($plain) === $pwd) {
                // We need a rehash and are having the right plain password
                $this->resetPassword($plain);
                return true;
            } else {
                return false;
            }
        }
        return Hash::check($plain, $pwd);
    }

    /**
     * Try to update the password and login at the same time
     * Return false is password is not acceptable
     * @param string $new plain password
     * @param string $old plain password
     * @param string $login
     * @return array list of parameters modified
     */
    public function updatePasswordLogin($new, $old, $login)
    {
        $ret = [
            'password' => false,
            'login' => false,
        ];
        $field=$this->getAuthPasswordName();
        if (!Hash::check($old, $this->$field)) {
            // Bad passsword : nothing to do
            return $ret;
        }
        if ($login && $this->isUniqueLoginPass($login, $new?$new:$old)) {
            // Update login
            $this->login = $login;
            $ret['login'] = true;
        }

        if (!Hash::check($new, $this->$field)
            && $this::validPassword($new)
            && $this->isUniqueLoginPass($this->login, $new)) {
            // Update password
            $this->$field = Hash::make($new);
            $ret['password'] = true;
        }

        if (!$this->save()) {
            // DB save failed
            $ret['login'] = false;
            $ret['password'] = false;
        }
        return $ret;
    }

    /**
     * Set the user password
     * @param string $plain
     * @return int code
     */
    public function setPassword($plain)
    {
        if (!$this::validPassword($plain)) {
            return self::PASSWORD_NOT_VALID;
        }
        if (!$this->isUniqueLoginPass($this->login, $plain)) {
            return self::PASSWORD_COUPLE_NOT_UNIQUE;
        }
        $field=$this->getAuthPasswordName();
        $this->$field = Hash::make($plain);
        return $this->save() ? self::PASSWORD_CHANGED : self::SAVE_ERROR;
    }

    /**
     * Reset the user passsword and token
     * Checks must have been done before
     * @param string $plain
     */
    public function resetPassword($plain)
    {
        $field = $this->getAuthPasswordName();
        $this->$field = Hash::make($plain);
        $this->generateRememberToken();
    }

    /**
     * Generates a token and place it in reset_pwd field
     * @return string $token
     */
    public function generateRememberToken()
    {
        $token = Str::random(32);
        $this->setRememberToken($token);
        $this->save();
        return $token;
    }

    /**
     * Validates a token and reset it if valid
     * @return boolean if the token was correct
     */
    public function validateRememberToken($token)
    {
        $ok = $this->getRememberToken() == $token;
        if ($ok) {
            $this->generateRememberToken(); // Reset token
        }
        return $ok;
    }

    /**
     * Return true if $plain is avalid password
     * @param string $plain
     * @return boolean
     */
    public static function validPassword($plain)
    {
        // Validation rule
        $field=self::getAuthPasswordName();
        if (strlen($plain) < 8
            || strlen($plain) > 20
            || $plain === strtoupper($plain)
            || $plain === strtolower($plain)) {
            return false;
        }
        return true;
    }

    /**
     * Return true if the couple login / pass is unique to the given user
     * @param string login
     * @param string plain
     */
    public function isUniqueLoginPass($login, $plain)
    {
        return $this->loginPassCoupleFree($login, $plain, $this);
    }
 
    /**
     * Return true if the couple login / passe is free
     * If a user is given, this users is excluded from the search
     * @param string login
     * @param string plain
     * @param \App\User|null $user
     */
    public static function loginPassCoupleFree($login, $plain, $user = null)
    {
        // Login - Password unicity
         $query = self::where('login', $login);
        if ($user) {
            $query = $query->where('id_user', '<>', $user->id_user);
        }
        $usersWithSameLogin = $query->get();
        $field=self::getAuthPasswordName();
        foreach ($usersWithSameLogin as $u) {
            if (Hash::check($plain, $u->$field)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Creates a user
     * This method is used when a user creates his own account
     * @param string $method
     * @param string $pwd
     * @param string $pwdConfirm
     * @param string $code
     * @return string message empty means success
     */
    public function autoRegister($method, $pwd, $pwdConfirm, $code)
    {
        $passfield = $this->getAuthPasswordName();
        if ($method === 'cas') {
            if (self::where('login', $this->login)->whereNull('pwd')->exists()) {
                return __("Un compte authentifié par l'annuaire existe déjà pour cet identifiant.");
            }
            $this->$passfield = null;
            if (!$this->save()) {
                return __("Impossible d'enregistrer l'utilisateur dans la base de donnée");
            }
            return "";
        }

        // Standard user registering either by code or by Moodle
        if ($this->user_name == ""
            || $this->first_name == ""
            || $this->login == ""
            || $pwd == ""
            || $pwdConfirm == "") {
            return __("Informations manquantes. Le compte n'a pas pu être créé.") ;
        }
        // Cas particulier sur l'auto inscription
        $link = "&nbsp;<button class='lb_btn btn' onclick='prelogin(%s, \"%s\", true)'>";
        $link .= __("Utiliser ce compte");
        $link .= "</button>";
        $u = self::where('login', $this->login)->first();
        if ($u) {
            return __("Cet identifiant n'est pas disponible").sprintf($link, $u->isInternal()?"true":"false", $u->login);
        }
        $u = self::where('email', $this->email)->first();
        if ($u) {
            return __("Il y a deja un compte avec cet email").sprintf($link, $u->isInternal()?"true":"false", $u->login);
        }
        if ($pwd !== $pwdConfirm || !self::validPassword($pwd)) {
            return __("Le format du mot de passe pose problème. Le compte n'a pas pu être créé.") ;
        }
        $this->$passfield = Hash::make($pwd);
        if ($code) {
            $class = \App\Classe::FromCode($code)->first();
            if (!$class) {
                return __("La classe correspondant au code fourni n'a pas été retrouvée. Le compte n'a pas pu être créé.") ;
            }
            $this->id_inst = $class->id_inst;
        } else {
            // We are created from an extplatform
            $class=null;
            Log::Debug(session()->all());
            $extplatform = \App\Extplatform::find(session('id_extplatform'));
            if (!$extplatform) {
                return __("Le moodle n'a pas été trouvé, le compte n'a pas pu être créé.") ;
            }
            $this->id_inst = $extplatform->id_inst;
        }
        //vérification de la validité de l'e-mail s'il est renseigé : on évite d'enregistrer des mails invalides
        if ($this->email && \filter_var($this->email, FILTER_VALIDATE_EMAIL) === false) {
            return __("Le format de l'e-mail est incorrect") ;
        }
        // Vérification que le N° d'étudiant est unique
        if ($this->inst_number) {
            $msg =  __("L'utilisateur correspondant à ce numéro d'étudiant existe déjà dans la base.");
            if (self::where('id_inst', $this->id_inst)->where('inst_number', $this->inst_number)->exists()) {
                return $msg . __("\nVous pouvez récupérer vos informations de connexion par l'interface 'Oubli du mot de passe'.") ;
            }
        } else {
            $this->inst_number = null;
        }

        if (!$this->email) {
            $this->email = "";
        }
        // tout va bien : on fait les insertions
        $this->save();
        $this->refresh();
        if ($class) {
            $class->addLearner($this->id_user);
        }

        if ($this->email) {
            self::sendInscriptionEmail($this->email, $this->login, $method === 'cas' ? null : '');
        }
        return "";
    }

    /**
     * Return the sql raw string to select user initials
     * @return string
     */
    public static function selectInitialsSql()
    {
        return "CONCAT(UPPER(SUBSTRING(first_name, 1, 1)),UPPER(SUBSTRING(user_name, 1, 1))) as initial";
    }

    /**
     * Select a users initials
     * @param int $id_user
     * @return string initials
     */
    public static function getInitials($id_user)
    {
        return self::selectRaw(self::selectInitialsSql())
            ->where('id_user', $id_user)
            ->first();
    }

    /**
     * Return the user information anonymized
     * @return string
     */
    public function getAnonymizedInfos()
    {
        $ret = $this->first_name.' '.$this->user_name;
        if (!empty($this->email) && strpos('@', $this->email)) {
            $ret.= ' - '.Helper::anonymizeString($this->email, true);
        }
        if ($this->inst_number) {
            $ret .= ' - '.__('n° étudiant').' : '.Helper::anonymizeString($this->inst_number);
        }
        $ret .= ' - '.__('institution').' : '.\App\Institution::find($this->id_inst)->name;
        $ret .= ' - '.__('compte').' : '.Helper::anonymizeString($this->login);
        return $ret;
    }

    /**
     * Anonymize the user
     * This cannot be reverted
     */
    public function anonymize()
    {
        $this->first_name = 'Ano';
        $this->user_name = 'Nymous';
        $this->email = null;
        $this->login = self::generateRandomString();
        $this->setPassword(self::generateRandomString());
        $this->removeMessagesAndComments();
        return $this->save();
    }

    /**
     * Removes a user messages and comments
     */
    public function removeMessagesAndComments()
    {
        $this->messages()->update(['msg_content' => __("Message supprimé")]);
        $this->comments()->update(['content' => __("Message supprimé")]);
    }

    /**
     * Search for homonymes for the potential user using fields :
     * first_name, user_name, login, email, inst_number and id_inst
     * @param StdClass User
     * @return \Database\Eloquent\Collection|\App\User[]|null
     */
    public static function getHomonymes($user)
    {
        $ignoredClasses = "[[:punct:][:space:][:digit:]]";
        $query = User::Where(
            function ($query) use ($user, $ignoredClasses) {
                $query = $query->where('login', $user->login)
                            // Normalize by removing all non alpha chars so "d'ham", "d ham" "d-ham" and "dham" is the same
                               ->orWhereRaw(
                                   '(REGEXP_REPLACE(first_name, "'.$ignoredClasses.'", "") LIKE ? AND REGEXP_REPLACE(user_name, "'.$ignoredClasses.'", "") LIKE ?)',
                                   [preg_replace('/'.$ignoredClasses.'/', '', $user->first_name), preg_replace('/'.$ignoredClasses.'/', '', $user->user_name)]
                               );
                if ($user->email) {
                    $query = $query->orWhere('email', 'LIKE', $user->email);
                }
                if ($user->inst_number) {
                    $query = $query->orWhere('inst_number', $user->inst_number);
                }
            }
        );
        if ($user->id_inst) {
            $query = $query->Where('id_inst', $user->id_inst);
        }
        return $query->get();
    }

    /**
     * fonction canSoftDeleteLearner
     * teste si  la suppression de l'etudiant d'une classe peut se faire
     * sans supprimer le compte
     * @return bool
     */
    public function canSoftDeleteLearner() {
        return $this->classes()->count() > 1 || $this->hasTeacher();
    }

    /**
     * fonction deleteLearnerFromClass
     * désinscrit un étudiant de la classe et le supprime si il n'est plus isncrit nul part
     * @param int $id_class
     */
    public function deleteLearnerFromClasse($id_class)
    {
        if ($this->canSoftDeleteLearner()) {
            $this->classes()->detach($id_class);
        } else {
            $id_user = $this->id_user;
            $this->delete();
            Trace::logAction(Trace::TEACHER_DELETE_STUDENT, ['id_student' => $id_user, 'id_class' => $id_class]);
        }
    }

    /**
     * fonction getUserWithInstBy
     * recherche et renvoie les données JSON des utilisateurs correspondats aux paramètres fournis
     * @param what = name (prénom et nom) number (numéro d'étudiant et id_inst) email (email)
     * var1 et var2 : cf. ci-dessus
     * id_inst : number. if 0 all the inst
     * @return StdClass
     */
    public static function getUserWithInstBy($what, $id_inst, $var1, $var2) {
        $query = DB::table('user')->join('institution', 'user.id_inst', 'institution.id_inst')
                                        ->select('id_user', 'user_name', 'first_name', 'login', 'email', 'user.id_inst', 'inst_number', 'institution.name')
                                        ->where('user.id_inst', $id_inst);
        
        if ($what === "name") {
            $query = $query->where('first_name', 'like', $var1."%")
                           ->where('user_name', 'like', $var2."%");
        } else if ($what === "email") {
            $query = $query->where('email', 'like', $var1);
        } else if ($what === "name-email") {
            $query = $query->where('email', 'like', $var1)
                            ->orWhere('first_name', 'like', $var1."%")
                            ->orWhere('user_name', 'like', $var1."%")
                            ->orderBy('user_name', 'asc')
                            ->orderBy('first_name', 'asc');
        } else if ($what === "number") {
            $query = $query->where('inst_number', $var1);
        }
        return $query->get();
    }

    /**
     * Create the user account if $id_user == 0, else update it.
     * This method is used sor creation or update triggered by another user
     *
     * @param int $id_user
     * @param \App\Classe $classe
     * @param string $user_name
     * @param string $first_name
     * @param int $id_inst
     * @param string $inst_number
     * @param string $email
     * @param string $login
     * @param string $password
     * @param boolean $send_mail Pseudo boolean
     * @return string|int Error message or user.id_user
     */
    public static function addOrUpdate($id_user, $classe, $user_name, $first_name, $id_inst, $inst_number, $email, $login, $password, $send_mail)
    {
        $error_msg =  __("La mise à jour n'a pas pu être faite") . __("&nbsp;:");
        if ($id_user == 0) {
            if (\App\User::where('login', $login)->exists()) {
                return $error_msg." ".__("Il existe déjà un utilisateur ayant cet intitulé de compte (login)")."\n";
            }
            $user = new \App\User();
            if ($password === null) {
                $password = self::generateRandomString();
            }
        } else {
            $user = \App\User::find($id_user);
        }

        // Set fields that should be updated
        $user->fill([
            'id_inst' => $id_inst,
            'inst_number' => $inst_number,
            'user_name' => $user_name,
            'first_name' => $first_name,
            'email' => $email,
            'login' => $login,
        ]);

        //  id_inst + inst_number (si ils sont définis)
        if ($id_inst && $inst_number) {
            $instNumberExists = \App\User::where('id_inst', $id_inst)
                ->where('inst_number', $inst_number)
                ->where('id_user', '<>', $id_user)
                ->exists();
            if ($instNumberExists) {
                return __("La mise à jour n'a pas pu être faite : le numéro d'étudiant existe déjà pour cette institution dans la base\n");
            }
        }

        if ($password) {
            $ret = $user->setPassword($password); // saves user to db
            if ($ret != $user::PASSWORD_CHANGED) {
                switch ($ret) {
                    case $user::PASSWORD_NOT_VALID:
                        $message = __("le mot de passe doit comporter au moins 8 caractères dont une majuscule et une minuscule");
                        break;
                    case $user::PASSWORD_COUPLE_NOT_UNIQUE:
                        $message = __("Il existe déjà un utilisateur ayant cet intitulé de compte (login)");
                        break;
                    default:
                        $message = __("probleme d'ecriture en base de donnée");
                        break;
                }
                return $error_msg." ".$message."\n";
            }
        } else {
            $user->save();
        }

        if ($id_user == 0) { // cas d'un ajout
            Trace::logAction(Trace::TEACHER_CREATE_USER, ['count' => 1]);
            // on ajoute l'étudiant dans la classe
            $classe->addLearner($user->id_user, 'form');
            // envoi du mail
            if ($user->id_user && $send_mail === 'true' && $user->email) {
                self::sendInscriptionEmail($user->email, $user->login, $password);
            }
        }
        return $user->id_user;
    }


    /**
     * Sends the register email to the concerned user
     * @param string $email
     * @param string $login
     * @param string|null $password
     * If password is empty than a message will say "The password you provided"
     * If password is NULL the email will tell the user to login via CAS
     * If a plain password is provided **it will be sent** to the user, use
     * this if and only if the account is created by another user
     */
    public static function sendInscriptionEmail($email, $login, $password) {
        Mail::to($email)->send(
            new MarkdownMail(
                'emails/inscription',
                __("LabNBook : informations de connexion à la plateforme"),
                [
                    'login' => $login,
                    'password' => $password,
                ])
        );
    }

    /**
     * Update the users information from a LdapUser
     * @param \App\Process\Ldap\LdapUser $ldapUser
     * @param Boolean $syncTeacherStatus whether to synchronize or not the teacher status with lnb
     * @return boolean have we changed something
     */
    public function updateFromLdapUser($ldapUser, $syncTeacherStatus=false)
    {
        if ($syncTeacherStatus && !$this->hasTeacher() && $ldapUser->isTeacher()) {
            // grants teacher status in case the student has been promoted
            if ($this->grantTeacher([$ldapUser->getInstitutionId()], "Teacher rights granted by the system during a login")) {
                Helper::addAlert("success", "<p>".__("Votre compte institutionnel a été reconnu comme un compte enseignant. Vous disposez désormais des droits d’enseignant sur LabNBook.")."</p>");
            }
        }
        if (!$ldapUser->isTeacher()) {
            try {
                $this->update(["inst_number" => $ldapUser->getApogeeId()]);
            } catch(QueryException $e) {
                Log::Warning($e->getMessage());
            }
        }
        if (empty($this->email)) {
                $this->update(["email" => $ldapUser->getEmail()]);
        }
    }

    /**
     * Handles the result of a LDAP user query :
     * Checks if there is a local account for the found ldapUser but with a different student number
     * @param \App\Process\Ldap\LdapUser $ldapUser
     * @param Boolean $syncTeacherStatus whether to synchronize or not the teacher status with lnb
     */
    public static function handleLdapLookupResponse($ldapUser, $syncTeacherStatus=false)
    {
        $query = self::where('id_inst', $ldapUser->getInstitutionId())
            ->where('login', $ldapUser->uid);
        // First we try to find an external acount.
        $internal = false;
        // We clone query to reuse later.
        $user = $query->clone()->internal(false)->first();
        if (!$user) {
            $user = $query->first();
            $internal = true; // If we found a user, it's an internal one not a CAS user
        }
        if (!$user) {
            // No match, nothing to do
            return;
        }
        $numbers = $user->inst_number ? [$user->inst_number] : [];
        $inst_number = $ldapUser->getApogeeId($numbers);
        if (!$user->hasTeacher() && $user->inst_number) {
            if ($user->inst_number == $inst_number) {
                $user->updateFromLdapUser($ldapUser, $syncTeacherStatus);
            } else {
                $user->archiveLdapAccount();
            }
        }
    }

    /**
     * Archive a CAS user by adding .N where N is the smallest avaiable number after login 
     * so the user cannot login anymore
     * Also empties the user email to avoid future errors
     */
    protected function archiveLdapAccount()
    {
        // The account informations does not match and the local user should login by CAS
        // This probably means that the ldap login has been reused for another person
        // Change user login for archiving purposes
        $count = 1;
        do {
            $login = $this->login . '.' . $count;
            $count++;
        } while (self::where('login', $login)->exists());
        $this->login = $login;
        $this->email = '';
        $this->save();
    }

    /**
     * Returns a long display name like
     * first last ( login - institution )
     * @param boolean $showInst should we show the user institution
     * @return string
     */
    public function longDisplayName($showInst, $forTeacher=true)
    {
        $displayname = ($forTeacher)?($this->user_name . ' ' . $this->first_name):($this->first_name . ' ' . $this->user_name);
        if ($showInst) {
            $displayname .=  ' (' . $this->institution->name . ')';
        }
        return $displayname;
    }
}
