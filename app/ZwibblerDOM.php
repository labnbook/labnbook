<?php

namespace App;
use Illuminate\Support\Facades\Log;

class ZwibblerDom
{
    public function __construct($data)
    {
        $this->data = Labdoc::decodeZwibblerData($data);
        $this->elements = new ZwibblerCollection(
            array_map(
            function ($e) {
                return new ZwibblerElem($e);
            },
            $this->data
            )
        );
    }

    /**
     * Returns an array of ZwibblerElems of the given type
     * return ZwibblerElem[]|null
     */
    public function getElementsByTagName($tag)
    {
        return new ZwibblerCollection(
            array_filter(
                (array) $this->elements,
                function ($e) use ($tag) {
                    return $e->getAttribute('type') === $tag;
                },
            )
        );
    }

    /**
     * Simulates a XML data save
     * @return string saved data
     */
    public function saveXML()
    {
        $this->data = array_map(
            function ($e) {
                return $e->obj;
            },
            (array) $this->elements
        );
        return Labdoc::encodeZwibblerData($this->data);
    }
}

class ZwibblerElem
{
    public function __construct($object)
    {
        $this->obj = $object;
    }
    /**
     * Updates the given attribute
     * @param string $attribute
     * @param string $value
     */
    public function setAttribute($attribute, $value)
    {
        $this->obj->$attribute = $value;
    }

    /**
     * Returns the attribute value
     * @return string
     */
    public function getAttribute($attribute)
    {
        return $this->obj->$attribute;
    }
}

class ZwibblerCollection extends \ArrayObject
{
    public function __construct($elements, $flags = 0, $iteratorClass = \ArrayIterator::Class)
    {
        parent::__construct($elements, $flags, $iteratorClass);
        $this->length = parent::count();
    }
}
