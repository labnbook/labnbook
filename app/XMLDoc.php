<?php

namespace App;

use \DomDocument;

class XmlDoc
{
    /** @var $domDoc DomDocument */
    private $domDoc;
    
    public function __construct() {
        $this->domDoc = new DomDocument();
    }

    /**
     * @param string $XML_parent_type
     * @param string $node_content
     * @param int $id_ld
     * @param int $id_rp
     */
    public function addXMLLD($XML_parent_type, $node_content, $id_ld, $id_rp, $shared)
    {
        $xml_ld = $this->domDoc->createElement("ld");
        $xml_ld->setAttribute("id_ld", $id_ld);
        $xml_ld->setAttribute("shared", $shared);
        if ($id_rp) {
            $xml_ld->setAttribute("id_rp", $id_rp);
        }
        if ($node_content) {
            $xml_content = $this->domDoc->createTextNode($node_content);
            $xml_ld->appendChild($xml_content);
        }
        $xml_parent = $this->domDoc->getElementsByTagName($XML_parent_type)->item(0);
        $xml_parent->appendChild($xml_ld);
    }

    /**
     * @param int $id_ld
     * @param int $id_rp
     * @param string $status
     * @param string $lastEditorFullName
     * @param string $last_edition
     * @param string $last_id_version_before_freeze
     * Add information in XML ld_list for Report synchronize method
     */
    public function addXMLInfoStatusLD($id_ld, $id_rp, $status, $lastEditorFullName="", $last_edition="", $last_id_version_before_freeze=""){
        $xml_ld = $this->domDoc->createElement("ld");
        $xml_ld->setAttribute("id_ld", $id_ld);
        $xml_ld->setAttribute("id_rp", $id_rp);

        $xml_status = $this->domDoc->createElement("status");
        $xml_status_content = $this->domDoc->createTextNode($status);
        $xml_status->appendChild($xml_status_content);
        $xml_ld->appendChild($xml_status);

        $xml_last_id_version_before_freeze = $this->domDoc->createElement("last_id_version_before_freeze");
        $xml_last_id_version_before_freeze_content = $this->domDoc->createTextNode($last_id_version_before_freeze);
        $xml_last_id_version_before_freeze->appendChild($xml_last_id_version_before_freeze_content);
        $xml_ld->appendChild($xml_last_id_version_before_freeze);
        
        if($status===Labdoc::LOCKED || $status===Labdoc::MODIFIED){
            $xml_editor = $this->domDoc->createElement("editor");
            $xml_editor_content = $this->domDoc->createTextNode($lastEditorFullName);
            $xml_editor->appendChild($xml_editor_content);
            $xml_ld->appendChild($xml_editor);

            if($status===Labdoc::MODIFIED){
                $xml_last_edition = $this->domDoc->createElement("last_edition");
                $xml_last_edition_content = $this->domDoc->createTextNode(date('d/m/y - H:i',$last_edition));
                $xml_last_edition->appendChild($xml_last_edition_content);
                $xml_ld->appendChild($xml_last_edition);
            }
        }

        $xml_parent = $this->domDoc->getElementsByTagName("ld_list");
        $xml_parent->item(0)->appendChild($xml_ld);
    }

    /**
     * @param int $id_ld
     * @param int $id_rp
     * @param string $name
     * @param string $content
     */
    public function addXMLModifiedLD($id_ld, $id_rp, $name, $content, $shared)
    {
        $xml_ld = $this->domDoc->createElement("ld");
        $xml_ld->setAttribute("id_ld", $id_ld);
        $xml_ld->setAttribute("id_rp", $id_rp);
        $xml_ld->setAttribute("shared", $shared);

        $xml_name = $this->domDoc->createElement("name");
        $xml_name_content = $this->domDoc->createTextNode($name);
        $xml_name->appendChild($xml_name_content);
        $xml_ld->appendChild($xml_name);

        $xml_content = $this->domDoc->createElement("content");
        $xml_content_content = $this->domDoc->createTextNode($content);
        $xml_content->appendChild($xml_content_content);
        $xml_ld->appendChild($xml_content);
        
        $xml_parent = $this->domDoc->getElementsByTagName("modified");
        $xml_parent->item(0)->appendChild($xml_ld);
    }

    /**
     * @param string $XML_parent_type
     * @param string $node_type
     * @param string $node_content
     * @return DomDocument()
     */
    public function addXMLNode($XML_parent_type, $node_type, $node_id, $node_content){
        $xml_element = $this->domDoc->createElement($node_type);
        $xml_element->setAttribute("id_".$node_type, $node_id);
        if ($node_content) {
            $xml_content = $this->domDoc->createTextNode($node_content);
            $xml_element->appendChild($xml_content);
        }
        $xml_parent = $this->domDoc->getElementsByTagName($XML_parent_type)->item(0);
        $xml_parent->appendChild($xml_element);
    }

    /**
     * Loads XML in the domDoc
     * @inherit
     * {@inheritDoc}
     */
    public function loadXML($xml) {
        return $this->domDoc->loadXML($xml);
    }

    /**
     * Saves the domDoc xml
     * @inherit
     * {@inheritDoc}
     */
    public function saveXML() {
        return $this->domDoc->saveXML();
    }

    /**
     * Return the domDocument childNodes
     * @inherit
     * {@inheritDoc}
     */
    public function childNodes() {
        return $this->domDoc->childNodes;
    }

    /**
     * Creates an element
     * @inherit
     * {@inheritDoc}
     */
    public function createElement($name, $value) {
        return $this->domDoc->createElement($name, $value);
    }
}
