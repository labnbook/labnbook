<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\belongsToMany;

class SimulationConstraint extends Model
{
    protected $table = 'simulation_constraint';
    protected $primaryKey = 'id_constraint';
    public $fillable = ['simulation_code', 'id_task_type', 'description', 'position', 'satisfaction_condition_key', 'constraint_message',
        'theoretical_feedback', 'technical_feedback', 'correction_feedback', 'simulation_enabled'];

    /**
     * Returns the task_type related to a constraint
     * @return BelongsTo
     */
    public function taskType(): BelongsTo
    {
        return $this->belongsTo('App\SimulationTaskType', 'id_task_type');
    }

    /**
     * Returns the relevance constraints related to a constraint
     * @return belongsToMany
     */
    public function relevanceConstraints(): belongsToMany
    {
        return $this->belongsToMany('App\SimulationConstraint', 'simulation_link_relevance_constraint', 'id_constraint', 'id_relevance_constraint');
    }

    public function allRelevanceConstraints()
    {
        $relevantConstraints = $this->relevanceConstraints;
        if (empty($relevantConstraints))
            return $relevantConstraints;

        foreach ($relevantConstraints as $rc)
        {
            $rc->load('relevanceConstraints');
            $relevantConstraints = $relevantConstraints->merge($rc->allRelevanceConstraints());
        }

        return $relevantConstraints;
    }
    
}
