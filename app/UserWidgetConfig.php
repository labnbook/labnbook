<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserWidgetConfig extends Model
{
    protected $table = 'user_widget_config';
    protected $primaryKey = 'id_user';
    protected $fillable = ['widget_reduced', 'widget_type', 'widget_position', 'widget_size_ratio'];
    public $timestamps = false;
    public $incrementing = false;
    protected bool $widget_reduced;
    protected String $widget_type;
    protected int $widget_position;
    protected int $widget_size_ratio;

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'id_user', 'id_user');
    }

}
