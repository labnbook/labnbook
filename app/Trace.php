<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Trace extends Model
{
    protected $table = 'trace';
    protected $primaryKey = 'id_trace';
    
    const CONNECT_LNB = 1;
    const DISCONNECT_LNB = 2;
    const ENTER_REPORT = 3;
    const LEAVE_REPORT = 4;
    const SUBMIT_REPORT = 5;
    const ADD_LD = 6;
    const DUPLICATE_LD = 7;
    const EDIT_LD = 8;
    const MODIFY_LD = 9;
    const VALIDATE_LD = 10;
    const TOGGLE_LD_STATUS = 11;
    const UPDATE_LD_NAME = 12;
    const DELETE_LD = 13;
    const CHECK_ANNOTATION = 14;
    const OPEN_RP_ASSIGNMENT = 15;
    const OPEN_ASSIGNMENT = 16;
    const OPEN_DETAILED_ASSIGNMENT = 17;
    const OPEN_RESOURCE = 18;
    const ADD_RESOURCE = 19;
    const OPEN_COMMENT = 20;
    const ADD_COMMENT = 21;
    const OPEN_CONVERSATION = 22;
    const ADD_MESSAGE = 23;
    const ADD_CONVERSATION = 24;
    const IMPORT_LD = 25;
    const SAVE_ATTACHED_LD = 26;
    const RECOVER_LD = 27;
    const OPEN_LD_VERSIONING = 28;
    const HARD_DELETE_LD = 29;

    // 31+
    const TEACHER_ADD_CLASS = 40;
    const TEACHER_ADD_LABDOC = 38;
    const TEACHER_ADD_MISSION = 34;
    const TEACHER_ADD_REPORT = 44;
    const TEACHER_ADD_REPORTPART = 39;
    
    const TEACHER_MODIFY_REPORTPART = 63;
    const TEACHER_ADD_RESOURCE = 37;
    const TEACHER_ADD_STUDENT = 42; // add to a class
    const TEACHER_ADD_TEAMING = 48;
    const TEACHER_ANNOTATE_REPORT = 47;
    const TEACHER_CREATE_USER = 49;
    const TEACHER_DELETE_CLASS = 41;
    const TEACHER_DELETE_MISSION = 36;
    const TEACHER_DELETE_REPORT = 46;
    const TEACHER_DELETE_STUDENT = 43;
    const TEACHER_DUPLICATE_MISSION = 35;
    const TEACHER_EDIT_REPORT = 45;
    const TEACHER_FOLLOW_REPORT = 31; // AKA follow_report
    const TEACHER_MODIFY_MISSION = 33;
    const TEACHER_TEST_MISSION = 32;
    const TEACHER_SELECT_REPORT = 50;
    const MANAGER_ADD_TEACHER_STATUS = 51;
    const MANAGER_REMOVE_TEACHER_STATUS = 52;
    const TEACHER_EDIT_TEAMING = 53;
    const TEACHER_ARCHIVE_REPORT = 54; // TODO Migration
    const TEACHER_OPEN_COLLABORATIVE_DASHBOARD = 55;
    const TEACHER_DISPLAY_DIFFS = 56;
    const USER_MERGED = 57;
    const TEACHER_FREEZE_REPORT = 64;

    // Scoring
    const SCORING_SAVE_GRID = 58;
    const SCORING_SAVE_EVAL = 59;
    const SCORING_PUBLISH_EVAL = 60;
    const SCORING_EDIT_EVAL = 61;
    const SCORING_VIEW_EVAL = 62;
    
    // Task
    const TASK_OPEN_LIST = 65;
    const TASK_CLOSE_LIST = 66;
    const TASK_CREATE = 67;
    const TASK_EDIT = 68;
    const TASK_DELETE = 69;
    const TASK_COMPLETED = 70;
    const TASK_PAIRING = 71;
    const TASK_SORT = 72;
    const TASK_FILTER_REPORTS = 73;
    const TASK_MOVE = 74;
    const TASK_FILTER = 75;
    const TASK_ASSIGN = 76;
    const CALENDAR_OPEN_WIDGET = 77;
    const CALENDAR_CLOSE_WIDGET = 78;
    const CALENDAR_CHANGE_VIEW = 79;
    const CALENDAR_OPEN_TASK = 80;
    

    /**
     * Log an action with its parameters.
     *
     * Keys "id_mission", "id_report", "id_labdoc" are extracted from attributes, if present.
     *
     * @param int|string $action
     * @param array $attributes Will be serialized to JSON, or set to NULL if not set or too big.
     * @param in $force_id_user force the id_user leaving the trace instead of the connected user
     * @return int trace ID
     */
    public static function logAction($action, $attributes = [], $force_id_user=null)
    {
        if (!Auth::check()) {
            // session expired, so abort tracing
            return;
        }
        if (is_int($action) || ctype_digit($action)) {
            $id_action = (int) $action;
        } elseif (defined('Trace::' . strtoupper($action))) {
            $id_action = constant('Trace::' . strtoupper($action));
        } else {
            error_log("Trace: unknown action '$action'");
            return;
        }
        list ($id_report, $id_mission, $id_labdoc, $strAttributes) = self::parseAttributes($attributes);
        if ($id_report && !$id_mission) {
            $id_mission = DB::table('report')
                ->where('id_report', $id_report)
                ->pluck('id_mission')
                ->first();
        }
        
        if( config('app.debug')){
            $userId = Auth::id();
            
            $trace_action_name = DB::table('trace_action')
                ->select('action')
                ->where('id_action', $id_action)->get()->first()->action;

            
            if($id_action=== self::CONNECT_LNB){
                error_log("*********** Start traces for User $userId ********\n", 3, storage_path("logs/trace.log"));
            }
            $DateAndTime = date('m-d-Y h:i:s a', time());
            $message="$DateAndTime - Trace " . str_pad($id_action,2,"0",STR_PAD_LEFT) . " ($trace_action_name) | User $userId Report $id_report, Mission $id_mission, Labdoc $id_labdoc\n";
            error_log($message, 3, storage_path("logs/trace.log"));

            if($id_action=== self::DISCONNECT_LNB){
                error_log("*********** End traces for User $userId ********\n", 3, storage_path("logs/trace.log"));
            }
         
        }
    
        
        return DB::table('trace')->insertGetId([
            'id_action' => (int) $id_action,
            'id_user' => $force_id_user ? $force_id_user : (int) Auth::id(),
            'id_mission' => !$id_mission ? null : (int) $id_mission,
            'id_report' => !$id_report ? null : (int) $id_report,
            'id_labdoc' => !$id_labdoc ? null : (int) $id_labdoc,
            'attributes' => $strAttributes === null || strlen($strAttributes) > 255 ? null : $strAttributes
        ]);
    }

    private static function parseAttributes($attributes)
    {
        if (isset($attributes['id_report'])) {
            $id_report = (int) $attributes['id_report'];
            unset($attributes['id_report']);
        } else {
            $id_report = null;
        }

        if (isset($attributes['id_mission'])) {
            $id_mission = (int) $attributes['id_mission'];
            unset($attributes['id_mission']);
        } else {
            $id_mission = null;
        }

        if (isset($attributes['id_labdoc'])) {
            $id_labdoc = (int) $attributes['id_labdoc'];
            unset($attributes['id_labdoc']);
        } else {
            $id_labdoc = null;
        }

        $strAttributes = array_filter($attributes)
            ? (is_string($attributes) ? $attributes : json_encode(array_filter($attributes)))
            : null;

        return [$id_report, $id_mission, $id_labdoc, $strAttributes];
    }

    public static function getLastTraceAction($id_user)
    {
        return DB::table('trace')
            ->where('id_user', $id_user)
            ->orderBy('id_trace', 'DESC')
            ->pluck('id_action')
            ->first();
    }


    /**
     * Collect student traces in order to plot the heatmap
     * @param \App\Mission $mission
     * @param \App\Class $class
     * @return array
     */
    public static function getStudentTraces($mission, $class)
    {
        $missions = $mission->teachers()->pluck('teacher.id_teacher');
        $classes = $class->teachers()->pluck('teacher.id_teacher');
        $teachers = $missions->concat($classes)->unique();

        return self::computeProdTime(Trace::select('trace.id_report', 'report.team_name', 'trace.action_time')
            ->join('report', 'trace.id_report', '=', 'report.id_report')
            ->join('team_config', 'team_config.id_team_config', '=', 'report.id_team_config')
            ->where('team_config.id_mission', '=', $mission->id_mission)
            ->where('team_config.id_class', '=', $class->id_class)
            ->where('trace.id_action', '=', 9)
            ->whereNotIn('id_user', $teachers)
            ->orderBy('trace.id_report')
            ->orderBy('trace.id_trace')
            ->get());
    }


    /**
     * Add a production time in minutes according to the corresponding date of the action_time
     * @param \Illuminate\Database\Eloquent\Collection $collectionOfTraces
     * @param \App\Report $report
     * @return array
     */
    public static function computeProdTime($collectionOfTraces)
    {
        $OLD_REFRESH_PERIOD = config('labnbook.old_refresh_period');
        ksort($OLD_REFRESH_PERIOD);
        $REFRESH_PERIOD = config('labnbook.refresh_period');
        return $collectionOfTraces
            ->mapWithKeys(function($item, $key) use($OLD_REFRESH_PERIOD, $REFRESH_PERIOD){
                $item['production'] = self::getProdTimeFromDate($item['action_time'], $OLD_REFRESH_PERIOD, $REFRESH_PERIOD);
                return [$key => $item];
            });
    }


    /**
     * Returns a production time in minutes according to a date
     * @param String $dateString
     * @param Object $OLD_REFRESH_PERIOD
     * @param Integer $REFRESH_PERIOD
     * @return Integer
     */
    public static function getProdTimeFromDate($dateString, $OLD_REFRESH_PERIOD, $REFRESH_PERIOD)
    {
        $prodtime = intval($REFRESH_PERIOD);
        foreach ($OLD_REFRESH_PERIOD as $date => $value) {
            if (strtotime($dateString) < strtotime($date)){
                $prodtime = intval($value);
                break;
            }
        };
        return floatval($prodtime / 60);
    }


    /**
     * Collect student traces in order to plot the collab dashboard
     * @param \App\Mission $mission
     * @param \App\Report $report
     * @param \App\TeamConfig $team_config
     * @return array
     */
    public static function getStudentCollabTraces($mission, $report, $team_config)
    {
        $mission_teachers = $mission->teachers()->pluck('teacher.id_teacher');
        $class_teachers = $team_config->classe->teachers()->pluck('teacher.id_teacher');
        $teachers = $mission_teachers->concat($class_teachers)->unique();

        return self::computeProdTime(
            self::UserIdToUserIndex(
                Trace::select(
                    'trace.id_user', 
                    'trace.action_time', 
                    'labdoc.id_labdoc', 
                    'labdoc.type_labdoc', 
                    'labdoc.position AS labdoc_position',
                    'labdoc.name AS labdoc_name',
                    DB::raw('COALESCE(report_part.position, 0) AS report_part_position'),
                    DB::raw('COALESCE(report_part.title, "...") AS report_part_title')
                )
                ->join('labdoc', 'labdoc.id_labdoc', '=', 'trace.id_labdoc')
                ->leftJoin('report_part', 'report_part.id_report_part', '=', 'labdoc.id_report_part')
                ->where('trace.id_report', '=', $report->id_report)
                ->where('trace.id_action', '=', Trace::MODIFY_LD)
                ->whereNull('labdoc.deleted')
                ->whereNotNull('labdoc.position')
                ->whereNotIn('id_user', $teachers)
                ->orderBy('id_user')
                ->orderBy('labdoc.id_labdoc')
                ->orderBy('trace.action_time')
                ->get(), $report)
        );
    }


    /**
     * Transform id_user to index of id_user (pseudonymization)
     * @param \Illuminate\Database\Eloquent\Collection $collectionOfTraces
     * @param \App\Report $report
     * @return array
     */
    public static function UserIdToUserIndex($collectionOfTraces, $report)
    {
        $orderedUsersArray = $report->users->modelKeys();
        sort($orderedUsersArray);
        $orderedUsers = collect($orderedUsersArray);
        return $collectionOfTraces
            ->mapWithKeys(function($item, $key) use($orderedUsers){
                $item['id_user'] = $orderedUsers->search(intval($item['id_user'])) + 1;
                return [$key => $item];
            });
    }

    public static function getTracesByNamePrefix ($prefix)
    {
        $consts = (new \ReflectionClass(self::class))->getConstants();
        $traces = [];
        foreach ($consts as $key => $val) {
            if (strpos($key, $prefix) === 0) {
                $traces[$key] = $val;
            }
        }
        return $traces;
    }
}
