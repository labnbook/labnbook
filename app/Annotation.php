<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Annotation
 *
 * @property int $id_annotation
 * @property int|null $id_teacher
 * @property int $id_report
 * @property int|null $id_labdoc
 * @property int|null $id_report_part
 * @property string|null $json
 * @property string|null $type
 * @property string $creation_time
 * @property string $update_time
 * @property-read \App\Report $report
 * @property-read \App\ReportPart|null $reportPart
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Annotation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Annotation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Annotation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Annotation whereCreationTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Annotation whereIdAnnotation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Annotation whereIdLabdoc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Annotation whereIdReport($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Annotation whereIdReportPart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Annotation whereIdTeacher($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Annotation whereJson($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Annotation whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Annotation whereUpdateTime($value)
 * @mixin \Eloquent
 * @property int|null $popularity
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Annotation wherePopularity($value)
 */
class Annotation extends Model
{
    protected $table = 'annotation';
    protected $primaryKey = 'id_annotation';

    public $timestamps = true;

    const CREATED_AT = 'creation_time';
    const UPDATED_AT = 'update_time';

    public $fillable = ['id_teacher', 'id_report', 'id_labdoc', 'id_report_part', 'json', 'type'];


    /**
     * Returns the report of the conversation
     * @return \App\Report
     */
    public function report()
    {
        return $this->belongsTo('App\Report', 'id_report');
    }

    /**
     * Returns the report of the conversation
     * @return \App\ReportPart
     */
    public function reportPart()
    {
        return $this->belongsTo('App\ReportPart', 'id_report_part');
    }

    /**
     * Update Popularity of annotation without update "update_time"
     * @param boolean $reset should we reset or increment the popularity
     */
    public function updatePopularity($reset)
    {
        // Si la popularité n'est pas renseigné alors on incremente
        if (!$reset) {
            $this->popularity++;
        } else {
            $this->popularity = 0;
        }

        // Save without updating timestamps
        $this->timestamps = false;
        $this->save();
        $this->timestamps = true;
    }

    /**
     * Save reading of the annotation by user
     * @param $id_user
     */
    public function saveReadingByUser($id_user)
    {
        DB::table('link_annotation_learner')->insertOrIgnore(
            [
                'id_user' => $id_user,
                'id_annotation' =>  $this->id_annotation,
            ]
        );
    }


}
