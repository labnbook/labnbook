<?php

namespace App;

use Illuminate\Support\Facades\DB;

class Indicators
{
    const USER_INDICATORS   = ['Info', 'Com', 'Cnx_time', 'Active_time'];
    const REPORT_INDICATORS = ['Info', 'Com', 'Cnx_time', 'Active_time', 'Work_distr'];
    const NA = "NA"; // Value not accurate

    public static function getUsers($report)
    {
        return $report->users->modelKeys();
    }

    /**
     * @param \App\Report $report
     * @param boolean $singleReport
     * @return array indicators info,com,active_time,cnx_time,work_distr and method for report
     */
    public static function getIndicators($report, $singleReport = true)
    {
        $id_report = $report->id_report;
        $indicateurs = [];

        foreach (self::USER_INDICATORS as $field) {
            // The methods getInfo, getCom, getCnx_time, getActive_time and get Work_distr are standardized
            // They all return an integer value corresponding the indicator for the given report and the given user if specified or "NA" if the value is not accurate
            $method = "get$field";
            if ($singleReport) {
                $indicateurs[$field] = [];
                // Compute separate indicators foreach users of the report
                $users = self::getUsers($report);
                sort($users);
                foreach ($users as $id_user) {
                    $indicateurs[$field][] = self::$method($id_report, $id_user);
                }
            } else { // Compute indicators for the report
                $indicateurs[$field] = self::$method($id_report);
            }
        }
        $indicateurs['Work_distr'] = self::getWork_distr($id_report);
        if ($singleReport) {
            // getAnnotation is not standardized : it does not take into account single user and it returns an array with two values
            // Total Annotations and Total annotations read
            $indicateurs['Annotation'] = self::getAnnotation($report);
        }

        return $indicateurs;
    }

    /**
     * Computes statistics on a serie of values
     * All returned values can be NAs except Count
     * @param array values
     * @return ['Median' => xxx, 'Min' => xxx, 'Count' => xxx, 'Sum' => XXX, 'Max' => xxx]
     */
    public static function computeStatistics($values)
    {
        // Sort to ease min max and median
        sort($values, SORT_NUMERIC);
        $count = count($values);
        if ($count > 0) {
            $sum = array_sum($values);
            $max = $values[$count -1];
            $min = $values[0];
            // Compute Median
            $middle = (int)($count/2);
            $median = $count &1 ? $values[$middle] : ($values[$middle] + $values[$middle -1])/2;
        } else {
            $sum = self::NA;
            $max = self::NA;
            $min = self::NA;
            $median = self::NA;
        }
        return ['Median' => $median, 'Min' => $min, 'Max' => $max, 'Count' => $count, 'Sum' => $sum];
    }

    /**
     * @param \Illuminate\Database\Eloquent\Collection|\App\Report[] $reports
     * @return null|array average of indicators info,com,active_time,cnx_time,work_distr and method for all $reports
     */
    public static function getAverageByReports($reports)
    {
        //get the indicators for each report
        foreach ($reports as $report) {
            $indicateurs[] = self::getIndicators($report, false);
        }

        $statsByIndicators = [];
        // addition of the indicators
        foreach ($indicateurs as $tab_indics) {
            foreach (self::REPORT_INDICATORS as $field) {
                $values[$field][] = $tab_indics[$field];
            }
        }

        foreach (self::USER_INDICATORS as $field) {
            $statsByIndicators[$field] = self::computeStatistics($values[$field]);
        }

        // Remove NAs
        $values['Work_distr'] = array_filter($values['Work_distr'], function ($e) { return $e != self::NA; }); 
        $statsByIndicators['Work_distr'] = self::computeStatistics($values['Work_distr']);
        return $statsByIndicators;
    }

    /**
     * @param $object \App\TeamConfig|\App\Mission
     * @return null|array average of indicators info,com,active_time,cnx_time,work_distr and method for all $reports
     */
    public static function getAverageFromObject($object)
    {
        $reports = $object->reports()->select('id_report')->whereIn('status', ['on', 'wait', 'arc'])->get();
        return self::getAverageByReports($reports);
    }

    /**
     * @param array $actions actions to filter
     * @param int $id_report
     * @param int|null $id_user
     * @param int|null $before count only traces before this date
     * @param int|null $after count only traces after this date
     *
     * @return int Number of matching traces
     */
    public static function countTraces($actions, $id_report, $id_user=null, $before=null, $after=null)
    {
        $query = DB::table('trace')
            ->where('id_report', $id_report)
            ->whereIn('id_action', $actions);
        if ($id_user) {
            $query = $query->where('id_user', $id_user);
        }
        if ($before) {
            $query = $query->where('action_time', '<', $before);
        }
        if ($after) {
            $query = $query->where('action_time', '>=', $after);
        }
        return (int) $query->count();
    }

    /**
     * @param int $id_report
     * @param int $id_user
     * @return int Number of read assignments and resources for the report and the user if it's not null
     */
    public static function getInfo($id_report, $id_user = null)
    {
        $actions = [
            Trace::OPEN_RP_ASSIGNMENT,
            Trace::OPEN_ASSIGNMENT,
            Trace::OPEN_DETAILED_ASSIGNMENT,
            Trace::OPEN_RESOURCE
        ];
        return self::countTraces($actions, $id_report, $id_user);
    }

    /**
     * @param int $id_report
     * @param int $id_user user id
     * @return int Number of messages for the report the user if it's not null
     */
    public static function getCom($id_report, $id_user = null)
    {
        $actions = [
            Trace::ADD_COMMENT,
            Trace::ADD_MESSAGE
        ];
        return self::countTraces($actions, $id_report, $id_user);
    }

    /**
     * Return the connexion time for the given user to the report
     * @param int $id_report
     * @param int $id_user
     * @return int $cnx_time (seconds)
     */
    public static function getCnx_time(int $id_report, $id_user = null)
    {
        $req = <<<EOL
        SELECT t.id_user, t.id_report, UNIX_TIMESTAMP(action_time) AS action_time, id_action
        FROM   trace t
        JOIN   link_report_learner lrl ON t.id_report = lrl.id_report AND t.id_user = lrl.id_user
        WHERE  t.id_report = ?
EOL;
        $params = [$id_report];
        if ($id_user != null) {
            $req .= " AND t.id_user = ? ";
            $params[] = $id_user;
        }
        $req .= " ORDER BY id_user, id_trace";
        $table = DB::select(DB::raw($req), $params);

        $cnx_time = 0;
        for ($i = 0; $i < count($table) - 1; $i++) {
            $j = $i + 1;
            $interval = $table[$j]->action_time - $table[$i]->action_time;
            if ($table[$i]->id_user == $table[$j]->id_user       // don't use the interval when change of user
                and $table[$i]->id_action != Trace::LEAVE_REPORT // don't use an interval after leaving a report
                and $table[$j]->id_action != Trace::ENTER_REPORT // don't use an interval before entering a report
                and $interval <= (15 * 60)                       // don't use the interval if > 15 min
            ) {
                $cnx_time += $interval;
            }
        }
        return $cnx_time;
    }

    /**
     * @param int $id_report
     * @param int $id_user user id
     * @return int Production time in timestamp format for the report and the user if it's not null
     */
    public static function getActive_time($id_report, $id_user = null)
    {
        $OLD_REFRESH_PERIOD = config('labnbook.old_refresh_period');
        $before = null;
        $after = null;
        $count = 0;
        foreach ($OLD_REFRESH_PERIOD as $date => $value) {
            $before = $date;
            // Count all trace before the REFRESH_PERIOD change ad $date and after the previous one (if any)
            $count += $value * self::countTraces([Trace::MODIFY_LD], $id_report, $id_user, $before, $after);
            $after = $before;
        }
        // $after will be set only if OLD_REFRESH_PERIOD was set in this case traces before $after where counted in the previous loop
        return $count + config('labnbook.refresh_period') * self::countTraces([Trace::MODIFY_LD], $id_report, $id_user, null, $after);
    }

    /**
     * @param int $id_report
     * @return int average of interwinding for all the labdocs from report
     */
    public static function getWork_distr($id_report)
    {
        $req = <<<EOL
        SELECT id_labdoc, t.id_user
        FROM trace t
        JOIN labdoc USING(id_labdoc)
        JOIN  link_report_learner lrl ON t.id_report = lrl.id_report AND t.id_user = lrl.id_user
        WHERE 
            t.id_action = :id_action    AND 
            t.id_report = :id_report    AND
            position    IS NOT NULL     AND
            deleted     IS NULL
        ORDER BY id_labdoc, id_trace
EOL;
        $traces = DB::select(DB::raw($req), ["id_action" => Trace::MODIFY_LD, "id_report" => $id_report]);

        $nb_ld = 0;
        $nb_change = 0;
        $last_id_ld = 0;
        $last_id_u = 0;
        foreach ($traces as $trace) {
            if ($last_id_ld != $trace->id_labdoc) { // another LD
                $nb_ld++;
                $last_id_ld = $trace->id_labdoc;
                $last_id_u = $trace->id_user;
            } else if ($last_id_u != $trace->id_user) { // same LD, different user
                $nb_change++;
                $last_id_u = $trace->id_user;
            }
        }
        if ($nb_ld == 0) {
            return self::NA;
        }
        return $nb_change / $nb_ld;
    }

    /**
     * @param \App\Report $report
     * @return array with
     * total_annotation = total number of annotation in report (not deleted)
     * total_annotation_read = number of annotation read at least one time by students
     */
    public static function getAnnotation($report)
    {
        // Get number of annotation read at least once
        $sql = "SELECT id_annotation, COUNT(id_user) AS nbReading 
                FROM link_annotation_learner JOIN annotation USING (id_annotation) 
                WHERE id_report = ? GROUP by id_annotation";
        $nb_annotations_read = count(DB::select(DB::raw($sql), [$report->id_report]));

        // Get total number of annotation in report
        $nb_annotations = DB::table('annotation')
            ->where('id_report', $report->id_report)
            ->count();

        // Send array with number of annotation read at least one time by students and total number of annotation in report (not deleted)
        return array(
            "total_annotation" => $nb_annotations,
            "total_annotation_read" => $nb_annotations_read
        );
    }
}
