<?php

namespace App\Policies;

use App\User;
use App\TeamConfig;
use Illuminate\Auth\Access\HandlesAuthorization;

class TeamConfigPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the team config.
     *
     * @param  \App\User  $user
     * @param  \App\TeamConfig  $teamConfig
     * @return mixed
     */
    public function select(User $user, TeamConfig $teamConfig)
    {
        // To select a teamconfig we must be a user of the class or of the external institution
        return $teamConfig->classe->contains($user->id_user, 'learner')
            || ( $teamConfig->classe->isExternal()
            &&   $teamConfig->classe->extplatform->hasUser($user->id_user));
    }

    /**
     * Determine whether the user can view the team config.
     *
     * @param  \App\User  $user
     * @param  \App\TeamConfig  $teamConfig
     * @return mixed
     */
    public function view(User $user, TeamConfig $teamConfig)
    {
        return $this->select($user, $teamConfig)
            || $teamConfig->classe->contains($user->id_user, 'teacher');
    }

    /**
     * Determine whether the user can update the team config.
     *
     * @param  \App\User  $user
     * @param  \App\TeamConfig  $teamConfig
     * @return mixed
     */
    public function update(User $user, TeamConfig $teamConfig)
    {
        return $teamConfig->classe->teachers()->whereKey($user->id_user)->exists()
            &&   $teamConfig->mission->teachers()->whereKey($user->id_user)->exists();
    }
}
