<?php

namespace App\Policies;

use App\Teacher;
use App\User;
use App\TeacherTeam;
use Illuminate\Auth\Access\HandlesAuthorization;

class TeacherTeamPolicy
{
    use HandlesAuthorization;

    public function view(User $user, TeacherTeam $teacherTeam)
    {
        return $teacherTeam != null && $teacherTeam->managers->contains($user->id_user);
    }

    public function update(User $user, TeacherTeam $teacherTeam)
    {
        return $this->view($user, $teacherTeam);
    }

    public function delete(User $user, TeacherTeam $teacherTeam)
    {
        return $this->view($user, $teacherTeam);
    }

    public function create(User $user)
    {
        return $user->teacher->isManager();
    }

    public function list(User $user)
    {
        return $user->teacher->isManager();
    }

    public function getTeachers(User $user)
    {
        return $user->teacher->isManager();
    }

    public function getMembers(User $user)
    {
        return $user->teacher->isManager();
    }

    // TODO Check if used
    public function addManager(User $user)
    {
        return $user->teacher->isManager();
    }

    public function grantTeacher(User $user)
    {
        return $user->teacher->isManager();
    }

    public function removeTeacher(User $user)
    {
        return $user->teacher->isManager();
    }


    public function addTeacher(User $user)
    {
        return $user->teacher->isManager();
    }
}
