<?php

namespace App\Policies;

use App\User;
use App\Teacher;
use Illuminate\Auth\Access\HandlesAuthorization;

class TeacherPolicy
{
    use HandlesAuthorization;

    public function view(User $user, Teacher $teacher)
    {
        return $user->isAdmin();
    }

    public function changeRole(User $user, Teacher $teacher)
    {
        return $user->isAdmin();
    }
}
