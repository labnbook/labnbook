<?php

namespace App\Policies;

use App\Helper;
use App\User;
use App\Report;
use Illuminate\Auth\Access\HandlesAuthorization;

class ReportPolicy {

    use HandlesAuthorization;

    /**
     * Determine whether the user can view the report.
     *
     * @param  \App\User  $user
     * @param  \App\Report  $report
     * @return mixed
     */
    public function view(User $user, Report $report)
    {
        /* To view a report, the user must be either
         * one of the report's users
         * one of the report's missions' users
         */
        return $report != null && (
            $report->users()->whereKey($user->id_user)->exists()
            || $report->teachers()->get()->contains($user->id_user)
        );
    }

    /**
     * Determine whether the user can either update or manage the report.
     *
     * @param  \App\User  $user
     * @param  \App\Report  $report
     * @return mixed
     */
    public function manageOrUpdate(User $user, Report $report)
    {
        return $user->can('update', $report) || $user->can('manage', $report);
    }

    /**
     * Determine whether the user can update the report.
     *
     * @param  \App\User  $user
     * @param  \App\Report  $report
     * @return mixed
     */
    public function update(User $user, Report $report)
    {
        $message = "Ce rapport n'existe pas ou vous n'êtes pas membre de l'équipe qui y travaille.";
        if (!$report) {
            // No report given
            Helper::setPolicyError(403, $message);
            return false;
        }
        if (!$user->isReportLearner($report)) {
            // Student not linked to the report
            Helper::setPolicyError(403, $message);
            return false;
        }
        // Last check : can a student update the report
        $status = $report->updateAllowed();
        if (!$status['allowed']) {
            Helper::setPolicyError(409, $status['msg']);
        }
        return $status['allowed'];
    }

    /**
     * Determine wether the user can manage a report
     */
    public function manage(User $user, Report $report)
    {
        return $user->isReportTeacher($report);
    }

    /**
     * Determine whether the user can restore a report.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function restore(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can delete a report.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function delete(User $user,  Report $report)
    {
        return $user->can('manage', $report);
    }

    /**
     * Determine whether the user can import a labdoc in the report
     * @param \App\User $user
     * @param \App\Report $report
     * @return mixed
     */
    public function importLabdoc(User $user, Report $report)
    {
        if (!$user->can('manageOrUpdate', $report)) {
            return false;
        }
        if (!$report->allow_import && !$user->can('manage', $report)) {
            Helper::setPolicyError(403, __("Vous n'avez pas le droit d'importer des labdocs dans ce rapport"));
            return false;
        }
        return true;
    }

    /**
     * Determine whether the user can import a labdoc in the report
     * @param \App\User $user
     * @param \App\Report $report
     * @return mixed
     */
    public function importLabdocFromMessage(User $user, Report $report)
    {
        if (!$report->allow_save_ld) {
            Helper::setPolicyError(403, __("Vous n'avez pas le droit d'enregistrer des labdocs de message dans ce rapport"));
            return false;
        }
        return true;
    }
}
