<?php

namespace App\Policies;

use \App\User;
use \App\Annotation;

class AnnotationPolicy
{
    /**
     * Determine whether the user can view the annotation
     *
     * @param  \App\User  $user
     * @param  \App\Annotation  $annotation
     * @return mixed
     */
    public function view(User $user, Annotation $annotation)
    {
        return $annotation->report && $user->can('view', $annotation->report)
            || $user->can('view', $annotation->reportPart->mission);
    }

    /**
     * Determine whether the user can update the annotation
     *
     * @param  \App\User  $user
     * @param  \App\Annotation  $annotation
     * @return mixed
     */
    public function update(User $user, Annotation $annotation)
    {
        return $user->id_user == $annotation->id_teacher;
    }

    /**
     * Determine whether the user can update popularity the annotation
     *
     * @param  \App\User  $user
     * @param  \App\Annotation  $annotation
     * @return mixed
     */
    public function updatePopularity(User $user, Annotation $annotation)
    {
        if ($annotation->id_teacher == $user->id_user) {
            return true;
        }
        if ($annotation->report && $user->isReportTeacher($annotation->report)) {
            return true;
        }
        return $user->teacher->missions->contains($annotation->reportPart->id_mission);
    }
}
