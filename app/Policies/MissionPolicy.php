<?php

namespace App\Policies;

use App\User;
use App\Mission;
use Illuminate\Auth\Access\HandlesAuthorization;

class MissionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can list missions.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function list(User $user)
    {
        return $user->hasTeacher();
    }

    /**
     * Determine whether the user can view the mission.
     *
     * @param  \App\User  $user
     * @param  \App\Mission  $mission
     * @return mixed
     */
    public function view(User $user, Mission $mission)
    {
        return  $mission->teachers()->whereKey($user->id_user)->exists() ||
            in_array($mission->status, ['public', 'tutorial']) && $user->hasTeacher() ;
    }

    /**
     * Determine whether the user can update the mission.
     *
     * @param  \App\User  $user
     * @param  \App\Mission  $mission
     * @return mixed
     */
    public function update(User $user, Mission $mission)
    {
        return $mission->designers()->whereKey($user->id_user)->exists();
    }

    /**
     * Determine whether the user can correct the mission.
     *
     * @param  \App\User  $user
     * @param  \App\Mission  $mission
     * @return mixed
     */
    public function correct(User $user, Mission $mission)
    {
        return $mission->teachers()->whereKey($user->id_user)->exists();
    }

    /**
     * Can the user starts this mission as a tutorial
     * @param \App\User #user
     * @param \App\Mission $mission
     * @return mixed
     */
    public function startTutorial(User $user, Mission $mission)
    {
        return $mission->status == 'tutorial';
    }
}
