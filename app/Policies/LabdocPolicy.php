<?php

namespace App\Policies;

use App\Report;
use App\User;
use App\Labdoc;
use App\Helper;
use Illuminate\Auth\Access\HandlesAuthorization;

class LabdocPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the labdoc's comments.
     *
     * @param  \App\User  $user
     * @param  \App\Labdoc  $labdoc
     * @return mixed
     */
    public function viewComments(User $user, Labdoc $labdoc)
    {
        return $this->view($user, $labdoc);
    }

    /**
     * Determine whether the user can add comments to the labdoc.
     *
     * @param  \App\User  $user
     * @param  \App\Labdoc  $labdoc
     * @return mixed
     */
    public function addComment(User $user, Labdoc $labdoc)
    {
        return $this->viewComments($user, $labdoc);
    }

    /**
     * Determine whether the user can view the labdoc.
     *
     * @param  \App\User  $user
     * @param  \App\Labdoc  $labdoc
     * @return mixed
     */
    public function view(User $user, Labdoc $labdoc)
    {
        if ($labdoc->messages()->exists()) {
            foreach ($labdoc->messages as $m) {
                if ($m->conversation->users->contains($user)) {
                    return true;
                }
            }
        }
        if ($labdoc->report) {
            return $user->can('view', $labdoc->report);
        }
        if ($labdoc->shared) {
            if ($labdoc->mission->teachers->contains($user->id_user)) {
                return true;
            }
            return $user->reports()->where('id_team_config', $labdoc->id_team_config)->exists();
        }
        if ($labdoc->reportPart) {
            return $user->can('view', $labdoc->reportPart->mission);
        }
        return false;
        // Maybe? return $user->can('view', $labdoc->parentLD);
    }

    /**
     * Determine whether the user can view the labdoc.
     *
     * @param  \App\User  $user
     * @param  \App\Labdoc  $labdoc
     * @return mixed
     */
    public function update(User $user, Labdoc $labdoc)
    {
        if ($labdoc->shared) {
            if ($labdoc->mission->teachers->contains($user->id_user)) {
                return true;
            }
            $reports = $user->reports()->where('id_team_config', $labdoc->id_team_config)->get();
            foreach ($reports as $r) {
                if ($user->can('update', $r)) {
                    return true;
                }
            }
        }
        if ($labdoc->report) {
            return $labdoc->report->users->contains($user) && $user->can('update', $labdoc->report);
        }
        if ($labdoc->reportPart) {
            return $user->can('update', $labdoc->reportPart->mission);
        }
        return false;
    }

    /**
     * Determine whether the change the labdoc draft status.
     *
     * @param  \App\User  $user
     * @param  \App\Labdoc  $labdoc
     * @return mixed
     */
    public function changeDraftStatus(User $user, Labdoc $labdoc)
    {
        return $labdoc->report != null && $labdoc->report->users()->whereKey($user->id_user)->exists();
    }

    /**
     * Determine whether user can duplicate the labdoc.
     *
     * @param  \App\User  $user
     * @param  \App\Labdoc  $labdoc
     * @return mixed
     */
    public function duplicate(User $user, Labdoc $labdoc)
    {
        if (!$labdoc->duplicatable && !($labdoc->report && $labdoc->report->status === Report::SOLUTION)) {
            Helper::setPolicyError(403, __("Vous n'avez pas le droit de dupliquer ce labdoc"));
            return false;
        }
        return true;
    }

    /**
     * Determine whether user can duplicate the labdoc.
     *
     * @param  \App\User  $user
     * @param  \App\Labdoc  $labdoc
     * @return mixed
     */
    public function delete(User $user, Labdoc $labdoc)
    {
        if ((($labdoc->report && $labdoc->report->status !== Report::SOLUTION)|| $labdoc->id_team_config) && !$labdoc->deleteable) {
            Helper::setPolicyError(403, __("Vous n'avez pas le droit de supprimer ce labdoc"));
            return false;
        }
        if ($labdoc->report) {
            return $user->can('update', $labdoc->report);
        } elseif ($labdoc->id_team_config) {
            return $user->can('update', $labdoc->getSharedLabdocReport($user));
        }
        return $user->can('update', $labdoc->mission);
    }
}
