<?php

namespace App\Policies;

use App\User;
use App\Classe;
use Illuminate\Auth\Access\HandlesAuthorization;

class ClassePolicy
{
    use HandlesAuthorization;

    /**
     * Determine wether the user can list classe from external institution
     */
    public function listExt(User $user)
    {
        return $user->hasTeacherOfExtplatform(request()->input('id_extplatform'));
    }

    public function view(User $user, Classe $classe)
    {
        return $classe->teachers->contains($user->id_user);
    }

    public function update(User $user, Classe $classe)
    {
        return $this->view($user, $classe);
    }
}
