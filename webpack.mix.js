const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.copy('node_modules/i18next/i18next.js', 'public/libraries/i18next/i18next.js');
mix.copy('node_modules/i18next-xhr-backend/i18nextXHRBackend.js', 'public/libraries/i18next-xhr-backend/i18nextXHRBackend.js');
mix.copy('node_modules/bowser/es5.js', 'public/libraries/bowser.min.js');
mix.copy('node_modules/select2/dist/js/select2.min.js', 'public/libraries/select2.min.js');
mix.copy('node_modules/select2/dist/css/select2.min.css', 'public/libraries/select2.min.css');
mix.copy('node_modules/sortablejs/Sortable.min.js', 'public/libraries/sortable/Sortable.min.js');
mix.copy('node_modules/alpinejs/dist/cdn.min.js', 'public/libraries/alpine/alpinejs.js');
mix.copy('node_modules//@alpinejs/mask/dist/cdn.min.js', 'public/libraries/alpine/mask/mask.js');
mix.copy('node_modules/mathlive/', 'public/libraries/tinymce/plugins/mathlive-equation-editor/editor/modules/mathlive/');
mix.copy('node_modules/@cortex-js/compute-engine/', 'public/libraries/tinymce/plugins/mathlive-equation-editor/editor/modules/@cortex-js/compute-engine/');
mix.copy('node_modules/decimal.js/', 'public/libraries/tinymce/plugins/mathlive-equation-editor/editor/modules/decimal.js/');
mix.copy('node_modules/complex.js/', 'public/libraries/tinymce/plugins/mathlive-equation-editor/editor/modules/complex.js/');
mix.copy('node_modules/katex/dist/', 'public/libraries/katex/');
mix.copy('node_modules/fullcalendar/index.global.min.js', 'public/libraries/fullcalendar/index.min.js');
mix.copy('node_modules/@fullcalendar/core/locales-all.global.min.js', 'public/libraries/fullcalendar/locales.min.js');
