<?php

use App\Processes\Ldap\Ldap;

class LdapMasqueradeTest extends \Codeception\Test\Unit
{
    /**
     *
     * @var type @var \App\Processes\Ldap\Ldap
     */
    protected $ldap;

    public function setUp(): void
    {
        if (isset($this->ldap)) {
            return;
        }
        $this->ldap = new Ldap([
            'masquerade' => true,
            'masquerade_users' => [
                [
                    'uid' => 'user-CAS', // login
                    'cn' => "Masako CAS user", // full name
                    'givenName' => "Masako", // first name
                    'sn' => "CAS user", // last name
                    'mail' => 'user-cas@example.org',
                    'o' => 'u123', // organisation
                    'employeeType' => ['E'],
                    // AGALAN fields
                    'aglnOrganizationUid' => ['u123_apo-12345678'], // student ID
                    'aglnPrimaryOrganizationName' => 'u123',
                    'aglnPersonStatus' => '',
                    'aglnMailStatus' => 'normal',
                    'aglnMailEffectiveAddr' => [
                       'user-cas@etu.example.org,user-cas@example.org'
                    ],
                ],
                [
                    'uid' => 'nouveau', // login
                    'cn' => "Petit Nouveau", // full name
                    'givenName' => "Petit", // first name
                    'sn' => "Nouveau", // last name
                    'mail' => 'petit.nouveau@example.org',
                    'o' => 'inpg', // organisation
                    'employeeType' => ['E'],
                    // AGALAN fields
                    'aglnOrganizationUid' => ['udg_apo-987654321'], // student ID
                    'aglnPrimaryOrganizationName' => 'inpg',
                    'aglnPersonStatus' => '',
                    'aglnMailStatus' => 'normal',
                    'aglnMailEffectiveAddr' => [
                       'petit.nouveau@example.org'
                    ],
                ],
            ],
        ]);
    }

    public function testGetUserByUsername()
    {
        $user = $this->ldap->getUserByUsername('user-CAS');
        $this->assertInstanceOf('\App\Processes\Ldap\LdapUser', $user);
        $this->assertEquals('Masako', $user->givenName);

        $this->assertNull($this->ldap->getUserByUsername(''));
        $this->assertNull($this->ldap->getUserByUsername('user'));
        $this->assertNull($this->ldap->getUserByUsername('user cas'));
    }

    public function testGetStudentByNumber()
    {
        // UGA
        $user = $this->ldap->getStudentByNumber('12345678');
        $this->assertInstanceOf('\App\Processes\Ldap\LdapUser', $user);
        $this->assertEquals('Masako', $user->givenName);

        // G-INP
        $phd = $this->ldap->getStudentByNumber('987654321');
        $this->assertInstanceOf('\App\Processes\Ldap\LdapUser', $phd);
        $this->assertEquals('Petit', $phd->givenName);

        // Students not in Ldap
        $this->assertNull($this->ldap->getStudentByNumber('123456'));
        $this->assertNull($this->ldap->getStudentByNumber(''));
    }

    public function testGetStudentsByNumbers()
    {
        $this->assertCount(2, $this->ldap->getStudentsByNumbers(['12345678', '987654321', '213']));
        $this->assertCount(2, $this->ldap->getStudentsByNumbers(['12345678', '987654321', '213']));
    }
}
