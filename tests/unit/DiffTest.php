<?php

use App\User;
use App\Labdoc;
use App\Processes\LabdocDiff;
use Illuminate\Support\Facades\Log;


class DiffTest extends \Codeception\Test\Unit
{
	public function testLdText()
	{
		$user = new User();
		$labdoc = new Labdoc();
		$labdoc->type_labdoc = 'text';
		$labdoc->labdoc_data = null;
		$labdocDiff = new LabdocDiff($user, $labdoc, -1, -1);

		$json = file_get_contents('tests/_data/diff_tests.json');
		$json_data = json_decode($json,true);
		
		foreach($json_data['tests'] as $test) {
			$labdocDiff->setLastLdContent($test['oldText']);
			$labdocDiff->setCurrentLdContent($test['newText']);
			$diffShouldBe = $test['expectedPhpDiff'];
			$diff = $labdocDiff->computeDiff();
			$this->assertEquals($diff, $diffShouldBe);
		}
	}
}
