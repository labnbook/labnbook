<?php
use Codeception\Util\HttpCode;

class UserCest
{
	private $known_users = [
		'teacher1' => [
				'id_user' => 2,
				'user_name' => 'Teacher1',
				'first_name' => 'Yukiko',
				'login' => 'teacher1',
				'email' => 'teacher1@example.org',
			],
	];

	public function searchUser(ApiTester $I){
		$payload = [
			'sub' => 'null',
			'data' => [
				'email' => 'teacher1@example.org',
			]
		];
		$I->send('user/search', 'GET', $payload);
		$I->seeResponseCodeIs(HttpCode::OK);
		$I->seeResponseIsJson();
		$I->seeResponseContainsJson(['data' => $this->known_users['teacher1']]);
		$I->amNotAuthenticated();
	}

	public function searchInexistingUser(ApiTester $I){
		$payload = [
			'sub' => 'null',
			'data' => [
				'email' => 'foobar@example.org',
			]
		];
		$I->send('user/search', 'GET', $payload);
		$I->seeResponseCodeIs(HttpCode::OK);
		$I->seeResponseIsJson();
		$I->seeResponseContainsJson(['data' => []]);
		$I->amNotAuthenticated();
	}

	public function login(ApiTester $I){
		$auth = $I->login($I->id_teach_ext);
		$I->send('auth/me', 'POST', $auth['payload'], $auth['token']);
		$I->seeResponseCodeIs(HttpCode::OK); // 200
		$I->seeResponseIsJson();
		$I->seeResponseContainsJson(['data' => $this->known_users['teacher1']]);
		$I->amAuthenticated();
	}

	public function loginBadUser(ApiTester $I){
		$payload = [ 'sub' => 'plop' ];
		$I->send('auth/login', 'POST', $payload);
		$I->expectError('User not linked to external platform',HttpCode::FORBIDDEN);
		$I->amNotAuthenticated();
	}
}
