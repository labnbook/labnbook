<?php
use Codeception\Util\HttpCode;

class MissionCest
{
	private $missions = [
		[
			"id_mission"=> 1,
			"code"=> "mission1",
			"name"=> "Mission première",
			"description"=> "<p>Ceci est la première mission</p>",
		],
		[
			"id_mission"=> 3,
			"code"=> "mission3",
			"name"=> "mission 3 vide",
			"description"=> "<p>La troisième mission est vide</p>",
		],
	];

	private $courseId = 4;
	private $groupId = 2;

	private $team_config = [
		'method' => 3, //random
		'size_opt' => 3,
		'size_max' => 4,
		'size_min' => 2,
		'teams_max' => 21,
		'name_prefix' => 'foo',
	];

	public function getMissionsUnauthorized(ApiTester $I){
		$payload = [
			'sub' => $I->id_teach_ext+10,
		];
		$I->send('mission', 'GET', $payload);
		$I->expectError('Authentication required',HttpCode::UNAUTHORIZED);
	}

	public function getGoodMissions(ApiTester $I){
		$auth = $I->login($I->id_teach_ext);
		$I->send('mission', 'GET', $auth['payload'], $auth['token']);
		$I->seeResponseCodeIs(HttpCode::OK);
		$I->seeResponseIsJson();
		$I->seeResponseContainsJson(['data' => $this->missions ]);
		$I->amAuthenticated();
	}

	public function getGoodMission(ApiTester $I){
		$auth = $I->login($I->id_teach_ext);
		$I->send('mission/3', 'GET', $auth['payload'], $auth['token']);
		$I->seeResponseCodeIs(HttpCode::OK);
		$I->seeResponseIsJson();
		$I->seeResponseContainsJson(['data' => $this->missions[1]]);
		$I->amAuthenticated();
	}

	public function getBadMission(ApiTester $I){
		$auth = $I->login($I->id_teach_ext);
		$I->send('mission/4', 'GET', $auth['payload'], $auth['token']);
		$I->expectError('Permission denied',HttpCode::FORBIDDEN);
		$I->amAuthenticated();
	}

	public function grant(ApiTester $I){
		$auth = $I->login($I->id_teach_ext);
		$payload = $auth['payload'];
		$I->wantTo('Create two teamconfigs');
		$tc = [ 
			'method' => 3,
			'teams_max' => 10,
			'size_opt' => 3,
		];
		foreach ([3,4] as $g) {
			$payload['data'] = [
				'courseid' => 34,
				'groupid' => $g,
				'coursename' => 'plop',
				'team_config' => $tc,
			];
			$I->send('mission/1/use', 'POST', $payload, $auth['token']);
			$I->seeResponseCodeIs(HttpCode::OK);
			$I->seeResponseIsJson();
			$I->seeResponseContainsJson(['data' => ['team_config' => $tc]]);
			$I->amAuthenticated();
			$auth = $I->getAuthFromResponse($I->id_teach_ext);
			$payload = $auth['payload'];
		}
		$I->wantTo('Grant teamconfigs');
		$payload['data'] = [
			'id_team_configs' => [1, 2],
		];
		$I->send('mission/grant', 'POST', $payload, $auth['token']);
		$I->seeResponseCodeIs(HttpCode::OK);
		$I->seeResponseIsJson();
		$I->seeResponseContainsJson(['data' => ['message' => 'ok']]);
		$I->amAuthenticated();
	}

	private function useMission(ApiTester $I, $id_mission, $group=null) {
		$team_config = $this->team_config;
		if ($group === null) {
			$group = $this->groupId;
		}

		$team_config['start_datetime'] = \Carbon\Carbon::now()->toDateTimeString();
		$team_config['end_datetime'] = \Carbon\Carbon::now()->addDays(30)->toDateTimeString();

		$auth = $I->login($I->id_teach_ext);
		$payload = $auth['payload'];
		$payload['data'] = [
			'coursename' => 'plop',
			'courseid' => $this->courseId,
			'groupid' => $group,
			'team_config' => $team_config,
		];
		$I->wantTo('Create teamconfig');
		$I->send("mission/$id_mission/use", 'POST', $payload, $auth['token']);
		return [$team_config, $auth];
	}

	public function useUpdate(ApiTester $I) {
		$id_mission = $this->missions[0]['id_mission'];
		list($team_config, $auth) = $this->useMission($I, $id_mission);
		$I->seeResponseCodeIs(HttpCode::OK);
		$I->seeResponseIsJson();
		$I->seeResponseContainsJson(['data' => ['team_config' => $team_config]]);
		$I->amAuthenticated();
		
		$I->wantTo('get teamconfig');
		$idtc = $I->grabDataFromResponseByJsonPath('$.data.team_config.id_team_config')[0];
		$auth = $I->getAuthFromResponse($I->id_teach_ext);
		$payload = $auth['payload'];
		$I->send('teamconfig/'.$idtc, 'GET', $payload, $auth['token']);
		$I->seeResponseCodeIs(HttpCode::OK);
		$I->seeResponseIsJson();
		$I->seeResponseContainsJson(['data' => $team_config]);
		$I->amAuthenticated();


		$I->wantTo('Update team_config');
		$team_config['size_max'] = 3;
		$team_config['size_min'] = 1;
		$auth = $I->getAuthFromResponse($I->id_teach_ext);
		$payload = $auth['payload'];
		$payload['data'] = [
			'courseid' => $this->courseId,
			'groupid' => $this->groupId,
			'team_config' => $team_config,
		];
		$I->send("mission/$id_mission/update", 'POST', $payload, $auth['token']);
		$I->seeResponseCodeIs(HttpCode::OK);
		$I->seeResponseIsJson();
		$I->seeResponseContainsJson(['data' => ['team_config' => $team_config]]);
		$I->amAuthenticated();

		$I->wantTo('update not allowed');
		$auth = $I->getAuthFromResponse($I->id_teach_ext);
		$payload = $auth['payload'];
		$I->send("mission/4/update", 'POST', $payload, $auth['token']);
		$I->seeResponseCodeIs(HttpCode::FORBIDDEN);
		$I->seeResponseIsJson();
		$I->seeResponseContainsJson(['data' => ['message' => 'Not allowed']]);
		$I->amAuthenticated();

		$I->wantTo('update class not found');
		$auth = $I->getAuthFromResponse($I->id_teach_ext);
		$payload = $auth['payload'];
		$payload['data'] = [
			'courseid' => 42,
			'groupid' => $this->groupId,
			'team_config' => $team_config,
		];
		$I->send("mission/$id_mission/update", 'POST', $payload, $auth['token']);
		$I->seeResponseCodeIs(403);
		$I->seeResponseIsJson();
		$I->seeResponseContainsJson(['data' => ['message' => 'No such class']]);
		$I->amAuthenticated();

		$I->wantTo('use not allowed');
		list($team_config, $auth) = $this->useMission($I, 4);
		$I->seeResponseCodeIs(HttpCode::FORBIDDEN);
		$I->seeResponseIsJson();
		$I->seeResponseContainsJson(['data' => ['message' => 'Not allowed']]);
		$I->amAuthenticated();
	}

	public function deleteTeamConfig(ApiTester $I) {
		$id_mission = $this->missions[0]['id_mission'];
		list($team_config, $auth) = $this->useMission($I, $id_mission);
		$I->seeResponseCodeIs(HttpCode::OK);
		$I->seeResponseIsJson();
		$I->seeResponseContainsJson(['data' => ['team_config' => $team_config]]);
		$I->amAuthenticated();

		$I->wantTo('delete teamconfig');
		$idtc = $I->grabDataFromResponseByJsonPath('$.data.team_config.id_team_config')[0];
		$payload = ['sub' => $I->id_teach_ext];
		$I->send('teamconfig/'.$idtc.'/delete', 'POST', $payload);
		$I->seeResponseCodeIs(HttpCode::OK);
		$I->seeResponseIsJson();
	}
}
