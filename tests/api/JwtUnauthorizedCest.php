<?php
use Codeception\Util\HttpCode;

class JwtUnauthorizedCest
{
	public function badServerKey(ApiTester $I){
		$payload = [
			'sub' => $I->id_teach_ext,
			'dest' => "$I->prefix/auth/login",
			'iat' =>  \Carbon\CarbonImmutable::now(),
			'iss' =>  $I->id_inst+1,
			'orig' => 'inst',
		];
		$I->send('auth/login', 'POST', $payload, $I->key, false);
		$I->expectError('Cannot retrieve secret key for inst', HttpCode::UNAUTHORIZED);
	}

	public function userTokenExpired(ApiTester $I){
		$ans = $I->login($I->id_teach_ext);
		sleep(6); // Wait for token expiration
		$I->send('auth/me', 'POST', $ans['payload'], $ans['token']);
		$I->expectError('User token expired', HttpCode::UNAUTHORIZED);
	}

	public function missingTokenExp(ApiTester $I){
		$ans = $I->login($I->id_teach_ext);
		unset($ans['payload']['token_exp']);
		$I->send('auth/me', 'POST', $ans['payload'], $ans['token']);
		$I->expectError('Missing token_exp', HttpCode::UNAUTHORIZED);
	}

	public function badUser(ApiTester $I){
		$ans = $I->login($I->id_teach_ext);
		$ans['payload']['sub'] = 'foobar';
		$I->send('auth/me', 'POST', $ans['payload'], $ans['token']);
		$I->expectError('User not found', HttpCode::UNAUTHORIZED);
	}



	public function missingTokenFields(ApiTester $I){
		$mandatory = ['sub', 'iss', 'iat', 'orig', 'dest'];
		foreach($mandatory as $field){
			$payload = [
				'sub' => $I->id_teach_ext,
				'dest' => "$I->prefix/auth/login",
				'iat' =>  \Carbon\CarbonImmutable::now(),
				'iss' =>  $I->id_inst+1,
				'orig' => 'inst',
			];
			unset($payload[$field]);
			$I->send('auth/login', 'POST', $payload, $I->key, false);
			$I->expectError("Missing required field(s) ".json_encode([$field]), HttpCode::UNAUTHORIZED);
		}
	}

	public function expiredInstToken(ApiTester $I){
		$payload = [
			'sub' => $I->id_teach_ext,
			'dest' => "$I->prefix/auth/login",
			'iat' =>  \Carbon\Carbon::now()->subSeconds(30)->toImmutable(),
			'iss' =>  $I->id_inst,
			'orig' => 'inst',
		];
		$I->send('auth/login', 'POST', $payload, $I->key, false);

		$I->expectError('Token expired', HttpCode::UNAUTHORIZED);

		$payload['iat'] = \Carbon\Carbon::now()->addSeconds(30)->toImmutable();

		$I->send('auth/login', 'POST', $payload, $I->key, false);

		$I->expectError('Token emmited in the future', HttpCode::UNAUTHORIZED);
	}

	public function badDest(ApiTester $I){
		$payload = [
			'sub' => $I->id_teach_ext,
			'dest' => "$I->prefix/foo/bar",
			'iat' =>  \Carbon\CarbonImmutable::now(),
			'iss' =>  $I->id_inst,
			'orig' => 'inst',
		];
		$I->send('auth/login', 'POST', $payload, $I->key, false);

		$I->expectError("Token not authorized for the path /v1/auth/login", HttpCode::UNAUTHORIZED);
	}

	public function badSignature(ApiTester $I){
		$payload = [
			'sub' => $I->id_teach_ext,
			'dest' => "$I->prefix/auth/login",
			'iat' =>  \Carbon\CarbonImmutable::now(),
			'iss' =>  $I->id_inst,
			'orig' => 'inst',
		];
		$I->send('auth/login', 'POST', $payload, 'jvJKrPMIUjXWhn9lzfRzOK46gRTGYB3E', false);
		$I->expectError('Bad signature', HttpCode::UNAUTHORIZED);
	}

	public function missingToken(ApiTester $I){
		$I->haveHttpHeader('accept', 'application/json');
		$I->haveHttpHeader('content-type', 'application/json');
		$I->sendPOST("$I->prefix/auth/login");
		$I->expectError('Missing or corrupted token', HttpCode::UNAUTHORIZED);
	}
}
