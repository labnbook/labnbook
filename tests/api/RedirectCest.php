<?php

/**
 * @license http://www.gnu.org/licenses/gpl-3.0.html  GNU GPL v3
 */

/**
 * Description of RedirectCest
 *
 * @author David Beniamine <David.Beniamine@tetras-libre.fr>
 */
class RedirectCest
{

	public function redirectAuthenticated(ApiTester $I){
		$I->redirectWithAuth($I->id_teach_ext, '/', 'teacher');
		$I->seeCurrentUrlEquals($I->home['teacher']);
		$I->seeResponseCodeIs(200);
	}

	private function bind(ApiTester $I, $username, $role, $redirurl, $localLogin, $cas, $homonymesString){
		// Bind
		$payload = [
			'sub' => $username,
			'data' => [
				'path' => '/user/preBind',
				'forward' => [
					'role_ext' => $role,
					'return_to' => $redirurl,
					'login' => $username,
					'email' => "$username@example.com",
					'first_name' => $username,
					'user_name' => $username,
				]
			]
		];
		$I->setCasMode($cas);
		$I->sendRedirect($payload);
		$I->makeHtmlSnapshot();
		// We cannot interact with JS here, so we directly submit the login form
		if ($localLogin === 'create') {
			$I->createLocalUser($username, $role, $cas);
		} else if ($localLogin === 'cas') {
			$I->bindCasUser($username, $role, $cas, $username, $homonymesString);
		} else {
			// localLogin is a username
			$I->bindLocalUser($username, $role, $cas, $localLogin, $homonymesString);
		}
	}

	public function bindStudentNoCasNoHomonymes(ApiTester $I){
		$this->bind($I, 'student', 'learner', '/', 'student1', false, '');
	}

	public function bindStudentNoCasHomonymes(ApiTester $I){
		$homonyme = 'Kōtaro Student1 - compte : student1';
		$this->bind($I, 'student1', 'learner', '/', 'student1', false, $homonyme);
	}

	public function bindTeacherNoCasNoHomonymes(ApiTester $I){
		$this->bind($I, 'teacher', 'teacher', '/', 'teacher1', false, '');
	}

	public function bindTeacherCas(ApiTester $I){
		$this->bind($I, 'teacher', 'teacher', '/', 'cas', true, '');
	}

	public function bindTeacherCasHomonymes(ApiTester $I){
		$homonyme = 'Yukiko Teacher1 - compte : teacher1';
		$I->updateInDatabase('user', ['pwd' => 'a'], ['login' => 'user-CAS']);
		$I->updateInDatabase('user', ['pwd' => null], ['login' => 'teacher1']);
		$this->bind($I, 'teacher1', 'teacher', '/', 'cas', true, $homonyme);
	}

	public function createTeacherCasInst(ApiTester $I){
		$this->bind($I, 'teacher', 'teacher', '/', 'create', true, '');
	}
	public function createTeacherNoCasInst(ApiTester $I){
		$this->bind($I, 'teacher', 'teacher', '/', 'create', false, '');
	}

	public function createLearnerCasInst(ApiTester $I){
		$this->bind($I, 'student', 'learner', '/', 'create', true, '');
	}

	public function createLearnerNoCasInst(ApiTester $I){
		$this->bind($I, 'student', 'learner', '/', 'create', false, '');
	}
}
