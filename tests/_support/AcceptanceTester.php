<?php

use Facebook\WebDriver\WebDriverKeys;
use \App\Trace;
use \App\Helper;

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
*/
class AcceptanceTester extends \Codeception\Actor
{
    use _generated\AcceptanceTesterActions;

    use \Codeception\Lib\Actor\Shared\Retry;

	// Enable haveFriend().
	// See https://codeception.com/docs/03-AcceptanceTests.html#multi-session-testing
	use \Codeception\Lib\Actor\Shared\Friend;

	public function loginAs($username, $password)
	{
		$this->amOnPage('/');
		$this->fillField('login', $username);
		$this->fillField('password', $password);
		$this->click("Se connecter", "#login-lnb");
		$this->seeILeftTrace(Trace::CONNECT_LNB);
	}

	public function logout($withtrace=true){
		$this->submitForm('#frm-logout', []);
		// TODO Disable this not so usefull check to decrease execution time (except when impersonate) ?
		if ($withtrace) { // We dont leave trace when we logout from impersonate
			$this->seeILeftTrace(Trace::DISCONNECT_LNB);
		}
	}

	public function clearText($selector)
	{
		$this->pressKey($selector, WebDriverKeys::END);
		$this->pressKey($selector, ['shift', WebDriverKeys::HOME]);
		$this->pressKey($selector, WebDriverKeys::BACKSPACE);
	}

	public function modifyLabdoc($labdocId) {
		$this->scrollTo("#labdoc_{$labdocId}", 0, -50);
		// TODO? open the labdoc if necessary
		$this->waitForElementNotVisible('.labdoc-content-loading', 2);
		//$this->moveMouseOver("#ld_name_actions_{$labdocId} .lb_ld_menu_btn > a");
		//$this->click("#ld_name_actions_{$labdocId} .lb_ld_menu > tbody > tr:first-child"); // "Modifier"
		$this->executeJS("editLD($labdocId);");
		$this->waitForElementVisible("input#ld_name_$labdocId", 2);
	}

	public function typeIn($selector, $text)
	{
		$this->pressKey($selector, str_split($text));
	}

	public function setHtmlContent($selector, $text)
	{
		$jsSelector = json_encode($selector);
		$jsText = json_encode($text);
		$this->executeJS(<<<EOL
document.querySelector($jsSelector).innerHTML = $jsText;
EOL
		);
	}

	public function highlightLabdocTitleAndClick($labdocId)
	{
		$this->moveMouseOver("#labdoc_{$labdocId} .labdoc_name");
		$this->executeJS(<<<EOJS
// highlight the labdoc title, then click on it
let target = document.querySelector("#labdoc_{$labdocId} .labdoc_name");
$(target).trigger('mousedown');
$("#page-alerts").text("Selecting text: " + $(target).text());
let range = document.createRange();
range.selectNodeContents(target.firstChild);
range.setEnd(target.firstChild, $(target).text().length);
let s = window.getSelection();
s.removeAllRanges();
s.addRange(range);
$(target).trigger('mouseup');
$("#page-alerts").text("");
EOJS
		);
	}

	/**
	 * Wait for a field to be filled with val
	 * @param string $selector
	 * @param string $val
	 */
	public function waitForFieldFilled(string $selector, string $val){
		$this->waitForJS("return $('$selector').val() == '$val';", 3);
	}


	/**
	 * Wait for an selector to have an attribute equals to val
	 * @param string $selector
	 * @param string $attr
	 * @param string $val
	 */
	public function waitForAttr(string $selector, string $attr, string $val){
		$this->waitForJS("return $('$selector').attr('$attr') == '$val';", 3);
	}


	/**
	 * Checks that there are no html special chars such as &nbsp; visible
	 */
	public function dontSeeSpecials()
	{
		$this->dontSee("&nbsp;");
	}

	/**
	 * Checks that the given trace is on the database
	 * @param $trace int|array id of traces to be Left in this order
	 * @param $optionnals array of id of optionnals values in previous array
	 */
	public function seeILeftTrace($traces, $optionnals=[])
	{
		$matches=0;
		if (!is_array($traces)) {
			$traces = [$traces];
		}
		// Get the last id
		$last = (int)$this->grabFromDatabase('trace', 'MAX(id_trace)');
		$num = count($traces);
		$i= $num-1;
		while($i >= 0) {
			$id_trace = $last-$num+1+$i;
			$id_action = $this->grabFromDatabase(
				'trace',
				'id_action',
				['id_trace' => $id_trace]
			);
			error_log("Id action " . $id_action . " - Id trace " . $id_trace . "\n",3, "trace.log" );
			if (in_array($i, $optionnals) && (int)$id_action != $traces[$i]) {
				$i--;
				$num--;
			}
			$this->assertEquals((int)$id_action, $traces[$i], "Unexpected trace");
			$matches++;
			$i--;
		}
		return $matches;
	}

	/**
	 * Checks that the good number of trace is on the database
	 * @param $login login of tested user
	 * @param $expected_number_of_trace
	 */
	public function seeILeftGoodNumberOfTracesSinceLastLogin($login, $expected_number_of_trace)
	{
		// TODO Is this one avoidable to get user id ?
		$id_user = (int)$this->grabFromDatabase('user', 'id_user', ['login' => $login]);
		
		// TODO Can be great to do in one request
		// Ge the last id of login trace
		$first_trace_id = (int)$this->grabFromDatabase('trace', 'MAX(id_trace)', ['id_user' => $id_user, 'id_action' => Trace::CONNECT_LNB]);
		// Get the number of trace
		$real_number_of_trace =  (int)$this->grabFromDatabase('trace', 'COUNT(id_trace)', ['id_user' => $id_user, 'id_trace >=' => $first_trace_id]);
		$this->assertEquals($expected_number_of_trace, $real_number_of_trace, "Wrong number of trace left");
	}

	public function clickAndCancelPopup($selector)
	{
		return $this->runAndActOnPopup('click', [$selector], false);
	}

	public function clickAndAcceptPopup($selector)
	{
		return $this->runAndActOnPopup('click', [$selector], true);
	}

	public function seeInPreviousPopup($text)
	{
		$text = htmlspecialchars($text);
		return $this->executeJS("window.confirmtext.includes('{$text}')");
	}

	public function dontSeeInPreviousPopup($text)
	{
		return !$this->seeInPreviousPopup($text);
	}

	public function runAndActOnPopup($method, $args, $accept)
	{
		$this->executeJS('
				var realConfirm=window.confirm;
				var realAlert=window.alert;
				window.confirm=function(data){
					window.confirm=realConfirm;
					window.confirmtext=data
					return '. ($accept ? 'true' : 'false') . ';
				};
				window.alert=function(data){
					window.confirmtext=data
				};
			');
		call_user_func_array([$this, $method], $args);
	}
	
	public function disableAlertPopup() 
	{
		$this->executeJS('
				var realAlert=window.alert;
				window.alert = x => console.log(x);
			');
	}
	
	public function restoreAlertPopup()
	{
		$this->executeJS('
				window.alert=realAlert;
			');
	}

	/**
	 * Fills a Select2 and click on the first matching element
	 * @param string $selector the id of the select element
	 * @param string $query the query to type in selector
	 * @param string $expeccted the expected text to see, by default the query
	 */
	public function fillSelect2($selector, $query, $expected = '')
	{
		// Based on https://stackoverflow.com/a/69160282
		if ($expected === '') {
			$expected = $query;
		}
		$selector_name = str_replace('#', '', $selector);
		$container_selector = '#select2-' . $selector_name . '-container';
		$input_selector = 'input[aria-controls=select2-' . $selector_name . '-results]';
		$this->waitForElementVisible($container_selector);
		// Click using JS to avoid error click is intercepted
		$this->click($container_selector);
		// Fill your dynamic tag with the option you want to search
		$this->fillField($input_selector, $query);
		// Wait the searching process
		$this->waitForElementNotVisible('.select2-results__option[aria-disabled=true]');
		// Then, wait for the option is loaded
		$this->waitForText($expected, 3);
		// Finally, use WebDriverKeys to select the option
		$this->pressKey($input_selector, \Facebook\WebDriver\WebDriverKeys::ENTER);
	}

	/**
	 * Writes on tiny mce iframe then return back to the caller window
	 * @param AcceptanceTester $I
	 * @param $tmce_selector
	 * @param $text
	 * @return void
	 */
	public function writeInTinyMceThenSwitchBack($tmce_selector, $text): void
	{
		$this->waitForELement($tmce_selector, 8);
		$this->switchToIFrame($tmce_selector);
		$this->pressKey('#tinymce', $text);
		$this->switchToWindow();
	}
}
