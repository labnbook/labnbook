<?php

declare(strict_types=1);

namespace Helper;

use App\LabdocVersioning;

class VersioningHelper extends \Codeception\Module
{
	/**
	 * Returns suite test is complete
	 * @return void
	 */
	public function _afterSuite()
	{
		$versioningDirectory = codecept_root_dir('storage/app/versioning_test');
		if (is_dir($versioningDirectory)) {
			$this->debug("Cleaning test versioning versioningDirectory : $versioningDirectory");
			$this->deleteDirectory($versioningDirectory);
		}
	}

	/**
	 * Deletes a directory
	 * @param $dir
	 * @return void
	 */
	private function deleteDirectory($dir)
	{
		if (!is_dir($dir)) {
			return;
		}

		$files = array_diff(scandir($dir), ['.', '..']);
		foreach ($files as $file) {
			$path = "$dir/$file";
			is_dir($path) ? $this->deleteDirectory($path) : unlink($path);
		}

		rmdir($dir);
	}
}
