<?php
use Lcobucci\JWT\Signer\Key\InMemory;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Codeception\Util\HttpCode;

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
 */
class ApiTester extends \Codeception\Actor
{
    use _generated\ApiTesterActions;
	/**
	 * @var string key used for jwt signing
	 */
	public $key = 'TV8b<Nx@h3DtVf2=TvXqrPD=JBR|tzLL';

	/**
	 * @var $string api url prefix
	 */
	public $prefix = 'v1';

	/**
	 * @var $int institution id
	 */
	public $id_inst = 3;

	/**
	 * @var int id of existing external teacher
	 */
	public $id_teach_ext = 45;
	
	public $home = [
		'learner' => '/reports',
		'teacher' => '/teacher/missions',
	];

	protected $registered_claims = [
		'iat' => 'issuedAt',
		'iss' => 'issuedBy',
		'sub' => 'relatedTo',
	];


	/**
	 * Returns a JWT with containing payloard
	 * @param array $payload
	 * @param string $key
	 * @return string
	 */
	public function payloadToJWT($payload, $key=null){
		if (!$key) {
			$key = $this->key;
		}
		$configuration = Configuration::forSymmetricSigner(
			new Sha256(),
			InMemory::plainText($key)
		);
		$time = time();
		$token = $configuration->builder();
		if(!$key){
			$key = $this->key;
		}
		foreach($payload as $claim => $value){
			if (isset($this->registered_claims[$claim])) {
				$method = $this->registered_claims[$claim];
				$token = $token->$method($value);
			} else {
				$token = $token->withClaim($claim, $value);
			}
		}
		return $token->getToken($configuration->signer(), $configuration->signingKey())->toString();
	}

	/**
	 * Send a request to the api
	 * @param string $path
	 * @param string $method GET|POST|PUT
	 * @param array $payload
	 * @param string $key
	 * @param string boolean should we add required field to payload
	 */
	public function send($path, $method, $payload, $key=null, $preparePayload=true){
		// Prepare payload
		$path = "$this->prefix/$path";
		if($preparePayload){
			$payload['dest'] = "$path";
			$payload['iat'] = \Carbon\CarbonImmutable::now();
			if(!$key){
				$payload['orig'] = 'inst';
				$key = $this->key;
			}else{
				$payload['orig'] = 'user';
			}
			$payload['iss'] = $this->id_inst;
		}

		// Retrieve JWT
		$jwt = $this->payloadToJWT($payload, $key);

		// set headers
		$this->amBearerAuthenticated($jwt);
		$this->haveHttpHeader('accept', 'application/json');
		$this->haveHttpHeader('content-type', 'application/json');

		// Do send the request
		$func = "send$method";
		$this->$func($path);
		$this->deleteHeader('Authorization');
		$this->deleteHeader('accept');
		$this->deleteHeader('content-type');
	}

	/**
	 * Return token and payload for authenticating from response
	 * @param int $id_user_ext
	 * @return array
	 */
	public function getAuthFromResponse($id_user_ext){
		$this->seeResponseIsJson();
		$auth=$this->grabDataFromResponseByJsonPath('$.auth')[0];
		return [
			'token' => $auth['token'],
			'payload' => [
				'sub' => $id_user_ext,
				'token_exp' => $auth['token_exp'],
			],
		];
	}

	/**
	 * Login as user, return usable payload for authenticated queries
	 * @param string $id_user_ext
	 * @return array
	 */
	public function login($id_user_ext){
		$payload = [ 'sub' => $id_user_ext ];
		$this->send('auth/login', 'POST', $payload);
		$this->seeResponseCodeIs(HttpCode::OK); // 200
		return $this->getAuthFromResponse($id_user_ext);
	}
	

	public function logout(){
		$this->submitForm('#frm-logout', []);
	}

	/**
	 * Check that the server answer with the correct erro
	 * @param string message
	 * @param int code
	 */
	public function expectError($message, $code){
		$this->seeResponseCodeIs($code);
		$this->seeResponseIsJson();
		$this->seeResponseContainsJson(['data' => ['message' => $message]]);
	}

	/**
	* helper to check if there is an auth token or not
	* @param boolean present should the auth token be present or not
	 */
	private function haveAuthToken($present){
		$this->seeResponseIsJson();
		if($present){
			$func = "seeResponseMatchesJsonType";
		}else{
			$func = "dontSeeResponseMatchesJsonType";
		}
		$this->$func([
			'auth' => [
				'token' => 'string',
				'token_exp' => 'integer',
			]
		]);
	}

	/**
	 * Check that the respond not contains auth
	 */
	public function amAuthenticated(){
		$this->haveAuthToken(true);
	}

	/**
	 * Check that the respond does not contains auth
	 */
	public function amNotAuthenticated(){
		$this->haveAuthToken(false);
	}

	public function redirectWithAuth($username, $path, $role){
		$auth=$this->login($username);
		$payload = [
			'sub' => $username,
			'token_exp' => $auth['payload']['token_exp'],
			'data' => [
				'forward' => [],
				'path' => $path,
			]
		];
		$this->sendRedirect($payload, $auth['token']);
		if($role === 'teacher') {
			$this->amOnPage($path);
		}
	}


	/**
	 * Changes the instituion cas mode
	 * @param boolean $cas
	 */
	public function setCasMode($cas) {
		if (!$cas) {
			$this->updateInDatabase('institution', ['cas' => 0], ['id_inst' => 2]);
		}
	}

	/**
	 * Verifies that the bind pages shows the right initial data
	 */
	public function verifyBindPage($cas) {
		if ($cas) {
			$this->waitForElementVisible("#login-CAS", 3);
			$this->dontSee("#user_login");
		} else {
			$this->waitForElementVisible("#user_login", 3);
			$this->dontSee("Compte institutionnel");
		}
		$this->dontSee("#user_login_create");
	}

	public function validateBind($sub, $role) {
		if ($role === 'learner') {
			$this->waitForElementVisible("#lb_btn_logout");
			$this->click("#lb_btn_logout");
		} else {
			$this->amOnPage('/');
			$this->logout();
		}
		$auth=$this->login($sub);
		$this->amAuthenticated();
		// Check learner / teacher
		$this->redirectWithAuth($sub, '/', $role);
		$this->makeHtmlSnapshot();
		$this->seeCurrentUrlEquals($this->home[$role]);
	}

	/**
	 * Does the user creation on the bind page
	 */
	public function createLocalUser($sub, $role, $cas) {
		$this->dontSee("#login-CAS");
		$this->dontSee("#login_login");
		$this->dontSee("#user_login_create");
		$this->click('créez un nouveau compte');
		$this->waitForElementVisible("#user_login_create", 3);
		$this->dontSee("#user_login");
		$this->dontSee("avec un compte institutionnel");
		$this->fillField('#create-account-pwd', 'P@ssword12234');
		$this->fillField('#create-account-pwd2', 'P@ssword12234');
		$this->click('input[name=accept_cgu]');
		$this->click('#user_create_validate');
		$this->makeHtmlSnapshot();
		$this->validateBind($sub, $role);
	}

	/**
	 * Does the user binding with a CAS user
	 */
	public function bindCasUser($sub, $role, $cas, $localUsername, $homonymesString) {
		$this->click('connectez-vous');
		$this->verifyBindPage($cas);
		if (!empty($homonymesString)) {
			$this->see("Nous avons détecté des comptes LabNBook qui pourraient vous appartenir.");
			$this->see($homonymesString);
			$this->click($localUsername);
		} else {
			$this->dontSee("Nous avons détecté des comptes LabNBook qui pourraient vous appartenir.");
			$this->click("#frm-login-cas");
		}
		$this->makeHtmlSnapshot();
		$this->validateBind($sub, $role);
	}

	/**
	 * Does the user binding with a Local user
	 */
	public function bindLocalUser($sub, $role, $cas, $localUsername, $homonymesString) {
		$this->click('connectez-vous');
		$this->verifyBindPage($cas);
		if (!empty($homonymesString)) {
			$this->see("Si un de ceux-ci vous appartient");
			$this->see($homonymesString);
			$this->click($localUsername);
		} else {
			$this->dontSee("Si un de ceux-ci vous appartient");
			$this->fillField('#user_login', $localUsername);
		}
		$this->fillField('#user_pwd', 'mdp');
		$this->click('Se connecter');
		$this->makeHtmlSnapshot();
		$this->validateBind($sub, $role);
	}

	public function sendRedirect($payload, $key=null){
		$path = $this->prefix.'/redirect';
		$payload['dest'] = $path;
		$payload['iat'] = \Carbon\CarbonImmutable::now();
		if(!$key){
			$payload['orig'] = 'inst';
		}else{
			$payload['orig'] = 'user';
		}
		$payload['iss'] = 3;
		$token = $this->payloadToJWT($payload, $key);
		$this->amOnPage("/api/$path?token=$token");
	}


}
