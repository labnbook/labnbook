SET NAMES 'utf8' COLLATE 'utf8_general_ci';
SET FOREIGN_KEY_CHECKS=0;

--
-- Table structure for table `annotation`
--

DROP TABLE IF EXISTS `annotation`;
CREATE TABLE `annotation` (
  `id_annotation` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_teacher` int(10) unsigned DEFAULT NULL,
  `id_report` int(10) unsigned NOT NULL,
  `id_labdoc` int(10) unsigned DEFAULT NULL,
  `id_report_part` int(10) unsigned DEFAULT NULL,
  `json` text DEFAULT NULL,
  `type` varchar(30) DEFAULT NULL,
  `creation_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `popularity` smallint(6) DEFAULT 0,
  PRIMARY KEY (`id_annotation`),
  KEY `id_teacher` (`id_teacher`),
  KEY `id_report` (`id_report`),
  KEY `id_report_part` (`id_report_part`),
  KEY `id_labdoc` (`id_labdoc`),
  CONSTRAINT `annotation_ibfk_1` FOREIGN KEY (`id_teacher`) REFERENCES `teacher` (`id_teacher`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `annotation_ibfk_2` FOREIGN KEY (`id_report`) REFERENCES `report` (`id_report`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `annotation_ibfk_3` FOREIGN KEY (`id_report_part`) REFERENCES `report_part` (`id_report_part`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `annotation_ibfk_4` FOREIGN KEY (`id_labdoc`) REFERENCES `labdoc` (`id_labdoc`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1000002 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Table structure for table `extplatform`
--

DROP TABLE IF EXISTS `extplatform`;
CREATE TABLE `extplatform` (
  `id_extplatform` int(11) NOT NULL AUTO_INCREMENT,
  `id_inst` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `secret` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contacts` text DEFAULT NULL,
  `plugin_version` varchar(255) DEFAULT NULL,
  `platform_version` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_extplatform`),
  UNIQUE KEY `extplatformname` (`name`),
  KEY `extplatform_ibfk_1` (`id_inst`),
  CONSTRAINT `extplatform_ibfk_1` FOREIGN KEY (`id_inst`) REFERENCES `institution` (`id_inst`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
--
-- Table structure for table `class`
--

DROP TABLE IF EXISTS `class`;
CREATE TABLE `class` (
  `id_class` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_inst` int(11) DEFAULT NULL,
  `class_name` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class_code` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `creation_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` enum('normal','archive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'normal',
  `ext_code` varchar(255) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL COMMENT 'Class identifier in external institution',
  `id_extplatform` int(11) DEFAULT NULL,
  `num_ext_participants` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_class`),
  UNIQUE KEY `class_code` (`class_code`),
  KEY `id_inst` (`id_inst`),
  CONSTRAINT `class_ibfk_1` FOREIGN KEY (`id_inst`) REFERENCES `institution` (`id_inst`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `class_ibfk_2` FOREIGN KEY (`id_extplatform`) REFERENCES `extplatform` (`id_extplatform`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `id_comment` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(10) unsigned NOT NULL DEFAULT 1,
  `id_labdoc` int(10) unsigned NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment_time` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id_comment`),
  KEY `id_user` (`id_user`),
  KEY `id_labdoc` (`id_labdoc`),
  CONSTRAINT `com_id_ld` FOREIGN KEY (`id_labdoc`) REFERENCES `labdoc` (`id_labdoc`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `com_id_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table User widget config
--
DROP TABLE IF EXISTS `user_widget_config`;
CREATE TABLE `user_widget_config` (
   `id_user` int(10) unsigned NOT NULL,
   `widget_type` varchar(100) DEFAULT NULL,
   `widget_reduced` BOOLEAN DEFAULT TRUE,
   `widget_position` enum('bottom', 'right') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'right',
   `widget_size_ratio` smallint(6) unsigned DEFAULT NULL,
   PRIMARY KEY (`id_user`),
   CONSTRAINT `user_widget_config_id_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `conversation`
--

DROP TABLE IF EXISTS `conversation`;
CREATE TABLE `conversation` (
  `id_conversation` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_report` int(10) unsigned DEFAULT NULL,
  `title` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id_conversation`),
  KEY `Conversation_FKIndex1` (`id_report`),
  CONSTRAINT `conv_id_rep` FOREIGN KEY (`id_report`) REFERENCES `report` (`id_report`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `institution`
--

DROP TABLE IF EXISTS `institution`;
CREATE TABLE `institution` (
  `id_inst` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_class_teacher` int(10) unsigned DEFAULT NULL,
  `cas` BOOLEAN DEFAULT FALSE,
  PRIMARY KEY (`id_inst`),
  UNIQUE KEY `institutionname` (`name`),
  CONSTRAINT `institution_ibfk_1` FOREIGN KEY (`id_class_teacher`) REFERENCES `class` (`id_class`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `labdoc`
--

DROP TABLE IF EXISTS `labdoc`;
CREATE TABLE `labdoc` (
  `id_labdoc` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_report_part` int(10) unsigned DEFAULT NULL,
  `id_report` int(10) unsigned DEFAULT NULL,
  `id_ld_origin` int(10) unsigned DEFAULT NULL,
  `id_team_config` int(10) unsigned DEFAULT NULL,
  `type_labdoc` enum('text','drawing','dataset','procedure','code') COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` int(10) unsigned DEFAULT NULL,
  `editable` tinyint(1) NOT NULL DEFAULT 1,
  `editable_name` int(1) NOT NULL DEFAULT 1,
  `duplicatable` tinyint(1) NOT NULL DEFAULT 1,
  `deleteable` int(1) NOT NULL DEFAULT 1,
  `draggable` int(1) NOT NULL DEFAULT 1,
  `shared` int(1) DEFAULT 0,
  `locked` int(11) DEFAULT NULL,
  `last_editor` int(10) unsigned DEFAULT NULL,
  `last_edition` int(11) DEFAULT NULL,
  `last_id_version` bigint(20) DEFAULT NULL,
  `labdoc_data` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `assignment` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `draft` tinyint(1) NOT NULL DEFAULT 0,
  `deleted` timestamp NULL DEFAULT NULL,
  `creation_time` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id_labdoc`),
  UNIQUE KEY `labdoc_id_ld_origin_id_team_config_unique` (`id_ld_origin`,`id_team_config`),
  UNIQUE KEY `labdoc_id_ld_origin_id_report_unique` (`id_ld_origin`,`id_report`),
  KEY `id_report_part` (`id_report_part`),
  KEY `id_mission_conf` (`id_report`),
  KEY `id_ld_origin` (`id_ld_origin`),
  KEY `last_editor` (`last_editor`),
  CONSTRAINT `labdoc_ibfk_1` FOREIGN KEY (`id_report_part`) REFERENCES `report_part` (`id_report_part`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `labdoc_ibfk_2` FOREIGN KEY (`id_ld_origin`) REFERENCES `labdoc` (`id_labdoc`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `labdoc_ibfk_3` FOREIGN KEY (`last_editor`) REFERENCES `user` (`id_user`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `ld_id_rep` FOREIGN KEY (`id_report`) REFERENCES `report` (`id_report`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `labdoc_status`
--

DROP TABLE IF EXISTS `labdoc_status`;
CREATE TABLE `labdoc_status` (
  `id_labdoc` int(10) unsigned NOT NULL,
  `id_user` int(10) unsigned NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `moved` tinyint(1) NOT NULL DEFAULT 0,
  `drafted` tinyint(1) NOT NULL DEFAULT 0,
  `modified` int(1) NOT NULL DEFAULT 1,
  `mod_icon` int(1) NOT NULL DEFAULT 1,
  `extend` int(1) NOT NULL DEFAULT 1,
  `com_viewed` timestamp NULL DEFAULT NULL,
  `last_id_version_reviewed` BIGINT(20) UNSIGNED DEFAULT NULL
	  COMMENT 'Last id_trace, corresponding to a labdoc version, that matches last user review',
  PRIMARY KEY (`id_labdoc`,`id_user`),
  KEY `lds_id_user` (`id_user`),
  CONSTRAINT `lds_id_labdoc` FOREIGN KEY (`id_labdoc`) REFERENCES `labdoc` (`id_labdoc`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `lds_id_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `labdoc_stdalone`
--

DROP TABLE IF EXISTS `labdoc_stdalone`;
CREATE TABLE `labdoc_stdalone` (
  `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_labdoc` enum('text','drawing','dataset','procedure','code') COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_modif` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `labdoc_data` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `labdocfile`
--

DROP TABLE IF EXISTS `labdocfile`;
CREATE TABLE `labdocfile` (
  `filehash` binary(20) NOT NULL,
  `id_labdoc` int(10) unsigned NOT NULL,
  `id_mission` int(10) unsigned NOT NULL,
  `uploadtime` bigint(20) NOT NULL,
  PRIMARY KEY (`filehash`,`id_labdoc`),
  KEY `id_labdoc` (`id_labdoc`),
  KEY `id_mission` (`id_mission`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `link_class_learner`
--

DROP TABLE IF EXISTS `link_class_learner`;
CREATE TABLE `link_class_learner` (
  `id_class` int(10) unsigned NOT NULL,
  `id_user` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_class`,`id_user`),
  KEY `lcl_id_user` (`id_user`),
  CONSTRAINT `lcl_id_class` FOREIGN KEY (`id_class`) REFERENCES `class` (`id_class`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `lcl_id_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `link_class_teacher`
--

DROP TABLE IF EXISTS `link_class_teacher`;
CREATE TABLE `link_class_teacher` (
  `id_teacher` int(10) unsigned NOT NULL,
  `id_class` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_teacher`,`id_class`),
  KEY `lct_id_class` (`id_class`),
  CONSTRAINT `lct_id_class` FOREIGN KEY (`id_class`) REFERENCES `class` (`id_class`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `lct_id_teacher` FOREIGN KEY (`id_teacher`) REFERENCES `teacher` (`id_teacher`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `link_conversation_user`
--

DROP TABLE IF EXISTS `link_conversation_user`;
CREATE TABLE `link_conversation_user` (
  `id_conversation` int(10) unsigned NOT NULL,
  `id_user` int(10) unsigned NOT NULL,
  `last_seen` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_conversation`,`id_user`),
  KEY `lcu_id_user` (`id_user`),
  CONSTRAINT `lcu_id_conv` FOREIGN KEY (`id_conversation`) REFERENCES `conversation` (`id_conversation`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `lcu_id_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `link_ressource_user`
--
DROP TABLE IF EXISTS `link_ressource_user`;
CREATE TABLE `link_ressource_user` (
    `id_ressource` int(10) unsigned NOT NULL,
    `id_user` int(10) unsigned NOT NULL,
    `already_seen` timestamp NULL DEFAULT NULL,
    KEY `id_ressource` (`id_ressource`),
    KEY `id_user` (`id_user`),
	CONSTRAINT `lru_id_resource` FOREIGN KEY (`id_ressource`) REFERENCES `ressource` (`id_ressource`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT `lru_id_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `link_inst_teacher`
--

DROP TABLE IF EXISTS `link_inst_teacher`;
CREATE TABLE `link_inst_teacher` (
  `id_teacher` int(11) unsigned NOT NULL,
  `id_inst` int(11) NOT NULL,
  PRIMARY KEY (`id_teacher`,`id_inst`),
  KEY `id_inst` (`id_inst`),
  CONSTRAINT `link_inst_teacher_ibfk_1` FOREIGN KEY (`id_teacher`) REFERENCES `teacher` (`id_teacher`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `link_inst_teacher_ibfk_2` FOREIGN KEY (`id_inst`) REFERENCES `institution` (`id_inst`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `link_mission_teacher`
--

DROP TABLE IF EXISTS `link_mission_teacher`;
CREATE TABLE `link_mission_teacher` (
  `id_mission` int(10) unsigned NOT NULL,
  `id_teacher` int(10) unsigned NOT NULL,
  `teacher_type` enum('teacher','designer') COLLATE utf8mb4_unicode_ci DEFAULT 'designer',
  PRIMARY KEY (`id_mission`,`id_teacher`),
  KEY `lmt_id_teacher` (`id_teacher`),
  CONSTRAINT `lmt_id_mission` FOREIGN KEY (`id_mission`) REFERENCES `mission` (`id_mission`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `lmt_id_teacher` FOREIGN KEY (`id_teacher`) REFERENCES `teacher` (`id_teacher`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `teacher_report_status`
--

DROP TABLE IF EXISTS `teacher_report_status`;
CREATE TABLE `teacher_report_status` (
  `id_report` int(10) unsigned NOT NULL,
  `id_teacher` int(10) unsigned NOT NULL,
  `last_synchro` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_report`,`id_teacher`),
  KEY `Teacher_Report_Status_FKIndex1` (`id_report`),
  KEY `Teacher_Report_Status_FKIndex2` (`id_teacher`),
  CONSTRAINT `trs_ibfk_1` FOREIGN KEY (`id_report`) REFERENCES `report` (`id_report`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `trs_ibfk_2` FOREIGN KEY (`id_teacher`) REFERENCES `teacher` (`id_teacher`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `link_report_learner`
--

DROP TABLE IF EXISTS `link_report_learner`;
CREATE TABLE `link_report_learner` (
  `id_report` int(10) unsigned NOT NULL,
  `id_user` int(10) unsigned NOT NULL,
  `last_synchro` int(11) DEFAULT NULL,
  `reload_annotations` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_report`,`id_user`),
  KEY `Link_Report_Learner_FKIndex1` (`id_report`),
  KEY `Link_Report_Learner_FKIndex2` (`id_user`),
  CONSTRAINT `link_report_learner_ibfk_1` FOREIGN KEY (`id_report`) REFERENCES `report` (`id_report`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `link_report_learner_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
CREATE TABLE `message` (
  `id_message` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `msg_content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `msg_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `id_user_sender` int(10) unsigned DEFAULT 1,
  `id_labdoc` int(10) unsigned DEFAULT NULL,
  `id_conversation` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_message`),
  KEY `id_user` (`id_user_sender`),
  KEY `id_conv` (`id_conversation`),
  KEY `id_labdoc` (`id_labdoc`),
  CONSTRAINT `message_ibfk_1` FOREIGN KEY (`id_labdoc`) REFERENCES `labdoc` (`id_labdoc`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `message_ibfk_2` FOREIGN KEY (`id_user_sender`) REFERENCES `user` (`id_user`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `msg_id_conv` FOREIGN KEY (`id_conversation`) REFERENCES `conversation` (`id_conversation`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `message_var`
--

DROP TABLE IF EXISTS `message_var`;
CREATE TABLE `message_var` (
  `id_user` int(10) unsigned NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT 1,
  `pos_x` smallint(6) unsigned NOT NULL,
  `pos_y` smallint(6) unsigned NOT NULL,
  `width` smallint(6) unsigned NOT NULL,
  `height` smallint(6) unsigned NOT NULL,
  PRIMARY KEY (`id_user`),
  CONSTRAINT `mv_id_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
CREATE TABLE `migration` (
  `id` binary(20) NOT NULL,
  `filename` varbinary(255) NOT NULL,
  `filehash` binary(20) NOT NULL,
  `filetime` bigint(20) NOT NULL,
  `appliedtime` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `migrations` (laravel migrations
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `mission`
--

DROP TABLE IF EXISTS `mission`;
CREATE TABLE `mission` (
  `id_mission` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(16) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `assignment` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `auto_fit` tinyint(1) NOT NULL DEFAULT 0,
  `status` enum('public','private','archive') CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT 'private',
  `simulation_code` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `creation_date` date DEFAULT NULL,
  `modif_date` date DEFAULT NULL,
  PRIMARY KEY (`id_mission`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `report`
--

DROP TABLE IF EXISTS `report`;
CREATE TABLE `report` (
  `id_report` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_mission` int(10) unsigned NOT NULL,
  `id_team_config` int(10) unsigned DEFAULT NULL,
  `start_datetime` datetime DEFAULT NULL,
  `end_datetime` datetime DEFAULT NULL,
  `status` enum('new','on','wait','arc','test', 'solution') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'new',
  `initialized` tinyint(1) NOT NULL DEFAULT 0,
  `team_name` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `send_teacher` tinyint(1) NOT NULL DEFAULT 1,
  `send_team` tinyint(1) NOT NULL DEFAULT 1,
  `send_class` int(10) NOT NULL DEFAULT 0,
  `send_mission` int(10) NOT NULL DEFAULT 0,
  `send_ld` tinyint(1) NOT NULL DEFAULT 0,
  `save_ld` tinyint(1) NOT NULL DEFAULT 0,
  `import_ld` int(10) NOT NULL DEFAULT -1,
  `creation_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `delete_time` timestamp NULL DEFAULT NULL COMMENT 'deletion time',
  PRIMARY KEY (`id_report`),
  KEY `Mission_Conf_FKIndex2` (`id_mission`),
  KEY `id_team_config` (`id_team_config`),
  CONSTRAINT `mc_id_mission` FOREIGN KEY (`id_mission`) REFERENCES `mission` (`id_mission`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `report_ibfk_1` FOREIGN KEY (`id_team_config`) REFERENCES `team_config` (`id_team_config`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `report_part`
--

DROP TABLE IF EXISTS `report_part`;
CREATE TABLE `report_part` (
  `id_report_part` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_mission` int(10) unsigned NOT NULL,
  `position` int(10) unsigned NOT NULL,
  `title` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` tinyint(3) unsigned NOT NULL DEFAULT 1,
  `drawing` tinyint(3) unsigned NOT NULL DEFAULT 1,
  `dataset` tinyint(3) unsigned NOT NULL DEFAULT 1,
  `procedure` tinyint(3) unsigned NOT NULL DEFAULT 1,
  `code` tinyint(3) unsigned NOT NULL DEFAULT 1,
  `assignment` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_report_part`),
  KEY `my_unique` (`id_mission`,`position`),
  CONSTRAINT `rp_id_mission` FOREIGN KEY (`id_mission`) REFERENCES `mission` (`id_mission`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `ressource`
--

DROP TABLE IF EXISTS `ressource`;
CREATE TABLE `ressource` (
  `id_ressource` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_mission` int(10) unsigned DEFAULT NULL,
  `id_report` int(10) unsigned DEFAULT NULL,
  `name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `res_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `res_type` enum('assignment','file','url') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'file',
  `position` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id_ressource`),
  KEY `id_mission` (`id_mission`),
  KEY `id_mission_conf` (`id_report`),
  CONSTRAINT `r_id_mission` FOREIGN KEY (`id_mission`) REFERENCES `mission` (`id_mission`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `r_id_report` FOREIGN KEY (`id_report`) REFERENCES `report` (`id_report`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `teacher`
--

DROP TABLE IF EXISTS `teacher`;
CREATE TABLE `teacher` (
  `id_teacher` int(10) unsigned NOT NULL,
  `teacher_domain` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `teacher_level` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `teacher_comment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_interface` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` ENUM('teacher','manager', 'admin') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'teacher',
  PRIMARY KEY (`id_teacher`),
  CONSTRAINT `t_id_teacher` FOREIGN KEY (`id_teacher`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `team_config`
--

DROP TABLE IF EXISTS `team_config`;
CREATE TABLE `team_config` (
  `id_team_config` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_mission` int(10) unsigned NOT NULL,
  `id_class` int(10) unsigned NOT NULL,
  `method` tinyint(4) unsigned NOT NULL DEFAULT 0 COMMENT '0=undef 1=student-choice 2=teacher-choice 3=random',
  `size_opt` smallint(5) unsigned DEFAULT NULL,
  `size_max` smallint(5) unsigned DEFAULT NULL,
  `size_min` smallint(5) unsigned DEFAULT NULL,
  `teams_max` smallint(5) unsigned DEFAULT NULL,
  `start_datetime` datetime DEFAULT NULL,
  `end_datetime` datetime DEFAULT NULL,
  `allow_msg_teacher` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'authorize messages to the class teachers (boolean)',
  `allow_msg_team` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'authorize messages to the other team members (boolean)',
  `allow_msg_id_class` int(10) unsigned DEFAULT NULL COMMENT 'authorize messages to class: NULL=none other=id_class',
  `allow_msg_id_mission` int(10) unsigned DEFAULT NULL COMMENT 'authorize messages to mission students: NULL=none other=id_mission',
  `allow_attach_ld` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'authorize attaching LD to outgoing messages (boolean)',
  `allow_save_ld` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'authorize saving into the report the LD received in messages (boolean)',
  `allow_import` tinyint(1) NOT NULL COMMENT 'Allow the import of labdocs. If true, may be bound to a mission with allow_import_id_mission',
  `allow_import_id_mission` int(10) unsigned DEFAULT NULL COMMENT 'Limit LD import to a single mission',
  `name_prefix` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_on_create` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'send an email to members when a team is created (boolean)',
  `creation_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_team_config`),
  UNIQUE KEY `unique_class_mission` (`id_class`,`id_mission`),
  KEY `id_mission` (`id_mission`),
  KEY `allow_import_id_mission` (`allow_import_id_mission`),
  KEY `allow_msg_id_class` (`allow_msg_id_class`),
  KEY `allow_msg_id_mission` (`allow_msg_id_mission`),
  CONSTRAINT `team_config_ibfk_1` FOREIGN KEY (`id_mission`) REFERENCES `mission` (`id_mission`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `team_config_ibfk_2` FOREIGN KEY (`id_class`) REFERENCES `class` (`id_class`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `team_config_ibfk_3` FOREIGN KEY (`allow_import_id_mission`) REFERENCES `mission` (`id_mission`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `team_config_ibfk_4` FOREIGN KEY (`allow_msg_id_class`) REFERENCES `class` (`id_class`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `team_config_ibfk_5` FOREIGN KEY (`allow_msg_id_mission`) REFERENCES `mission` (`id_mission`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `trace`
--

DROP TABLE IF EXISTS `trace`;
CREATE TABLE `trace` (
  `id_trace` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_action` smallint(6) NOT NULL,
  `action_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `id_user` int(10) unsigned NOT NULL,
  `id_mission` int(10) unsigned DEFAULT NULL,
  `id_report` int(10) unsigned DEFAULT NULL,
  `id_labdoc` int(10) unsigned DEFAULT NULL,
  `attributes` varbinary(255) DEFAULT NULL,
  PRIMARY KEY (`id_trace`),
  KEY `key_id_report` (`id_report`),
  KEY `key_id_action` (`id_action`),
  CONSTRAINT `trace_ibfk_1` FOREIGN KEY (`id_action`) REFERENCES `trace_action` (`id_action`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `trace_action`
--

DROP TABLE IF EXISTS `trace_action`;
CREATE TABLE `trace_action` (
  `id_action` smallint(6) NOT NULL,
  `action` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action_type` enum('administration','production','information','communication','t_mission','t_student','t_report') CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
  PRIMARY KEY (`id_action`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id_user` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `login` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pwd` varbinary(64) DEFAULT NULL,
  `email` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `lang` varchar(3) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `id_inst` int(11) NOT NULL,
  `inst_number` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_report` int(10) unsigned DEFAULT NULL,
  `last_synchro` int(11) DEFAULT NULL,
  `reset_pwd` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `creation_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_time` timestamp NULL DEFAULT NULL,
  `cgu_accept_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_user`),
  UNIQUE KEY `login` (`login`,`pwd`),
  UNIQUE KEY `reset_pwd` (`reset_pwd`),
  UNIQUE KEY `instid_instnumber` (`id_inst`,`inst_number`),
  KEY `current_report` (`current_report`),
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`current_report`) REFERENCES `report` (`id_report`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `user_ibfk_2` FOREIGN KEY (`id_inst`) REFERENCES `institution` (`id_inst`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `link_extplatform_user`
--
DROP TABLE IF EXISTS `link_extplatform_user`;
CREATE TABLE `link_extplatform_user` (
  `id_extplatform` int(11) NOT NULL,
  `id_user_ext` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_user` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_extplatform`,`id_user_ext`),
  KEY `link_ext_user_inst_id_user_foreign` (`id_user`),
  CONSTRAINT `link_extplatform_user_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE,
  CONSTRAINT `link_extplatform_user_ibfk_2` FOREIGN KEY (`id_extplatform`) REFERENCES `extplatform` (`id_extplatform`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `link_annotation_learner`
--

DROP TABLE IF EXISTS `link_annotation_learner`;
CREATE TABLE `link_annotation_learner` (
  `id_annotation` int(10) unsigned NOT NULL,
  `id_user` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_annotation`,`id_user`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `link_annotation_learner_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `link_annotation_learner_ibfk_2` FOREIGN KEY (`id_annotation`) REFERENCES `annotation` (`id_annotation`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
SET FOREIGN_KEY_CHECKS=1;


--
-- Table structure for table `teacher_team`
--
DROP TABLE IF EXISTS `teacher_team`;
CREATE TABLE teacher_team (
      `id_teacher_team` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
      `id_inst` int(11),
      `name` varchar(64) NOT NULL,
      `status` enum('normal','archive') CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT 'normal',
      `update_time` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
      `creation_time` timestamp NULL DEFAULT NULL,
      PRIMARY KEY (`id_teacher_team`),
      FOREIGN KEY (`id_inst`) REFERENCES `institution` (`id_inst`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `link_mission_tteam`
--
DROP TABLE IF EXISTS `link_mission_tteam`;
CREATE TABLE link_mission_tteam (
    `id_mission` int (10) UNSIGNED NOT NULL,
    `id_teacher_team` int (10) UNSIGNED NOT NULL,
    PRIMARY KEY ( `id_mission`,`id_teacher_team` ),
    FOREIGN KEY (`id_mission`) REFERENCES `mission` (`id_mission`) ON DELETE CASCADE ON UPDATE CASCADE,
                                    FOREIGN KEY (`id_teacher_team`) REFERENCES `teacher_team` (`id_teacher_team`) ON DELETE CASCADE ON UPDATE CASCADE
)  ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `link_class_tteam`
--
DROP TABLE IF EXISTS `link_class_tteam`;
CREATE TABLE link_class_tteam (
    `id_class` int (10) UNSIGNED NOT NULL,
    `id_teacher_team` int (10) UNSIGNED NOT NULL,
    PRIMARY KEY ( `id_class`,`id_teacher_team` ),
    CONSTRAINT `lctt_id_class` FOREIGN KEY (`id_class`) REFERENCES `class` (`id_class`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `lctt_id_teacher_team` FOREIGN KEY (`id_teacher_team`) REFERENCES `teacher_team` (`id_teacher_team`) ON DELETE CASCADE ON UPDATE CASCADE
)  ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `link_teacher_tteam`
--
DROP TABLE IF EXISTS `link_teacher_tteam`;
CREATE TABLE link_teacher_tteam (
    `id_teacher` int (10) UNSIGNED NOT NULL,
    `id_teacher_team` int (10) UNSIGNED NOT NULL,
    `status` enum('member','manager') CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT 'member',
    PRIMARY KEY ( `id_teacher`,`id_teacher_team`, `status` ),
    CONSTRAINT `lttt_id_teacher` FOREIGN KEY (`id_teacher`) REFERENCES `teacher` (`id_teacher`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `lttt_id_teacher_team` FOREIGN KEY (`id_teacher_team`) REFERENCES `teacher_team` (`id_teacher_team`) ON DELETE CASCADE ON UPDATE CASCADE
)  ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Table structure for table `maillog`
--
DROP TABLE IF EXISTS `maillog`;
CREATE TABLE `maillog` (
  `to` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hash` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sent_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `rubric`;
CREATE TABLE `rubric` (
  `id_rubric` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_activity` int(10) unsigned DEFAULT NULL,
  `name` char(255) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `grading` double(8,2) DEFAULT NULL,
  `min_grade` double(8,2) DEFAULT NULL,
  `max_grade` double(8,2) DEFAULT NULL,
  `min_bonus` double(8,2) DEFAULT NULL,
  `max_bonus` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_rubric`),
  KEY `rubric_ibfk1` (`id_activity`),
  CONSTRAINT `rubric_ibfk1` FOREIGN KEY (`id_activity`) REFERENCES `mission` (`id_mission`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `assessment`;
CREATE TABLE `assessment` (
  `id_assessment` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_production` int(10) unsigned NOT NULL,
  `id_rubric_origin` int(10) unsigned DEFAULT NULL,
  `bonus` double(8,2) DEFAULT NULL,
  `bonus_comment` longtext DEFAULT NULL,
  `score` double(8,2) DEFAULT NULL,
  `score_comment` longtext DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT 0,
  `grading` double(8,2) DEFAULT NULL,
  `min_grade` double(8,2) DEFAULT NULL,
  `max_grade` double(8,2) DEFAULT NULL,
  `min_bonus` double(8,2) DEFAULT NULL,
  `max_bonus` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_assessment`),
  KEY `assessment_ibfk1` (`id_production`),
  CONSTRAINT `assessment_ibfk1` FOREIGN KEY (`id_production`) REFERENCES `report` (`id_report`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `criteria_group`;
CREATE TABLE `criteria_group` (
  `id_criteria_group` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_origin` int(10) unsigned DEFAULT NULL,
  `position` smallint(6) NOT NULL DEFAULT 0,
  `description` longtext DEFAULT NULL,
  `relative_weight` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id_rubric` int(10) unsigned DEFAULT NULL,
  `id_assessment` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_criteria_group`),
  KEY `criteria_group_id_origin_foreign` (`id_origin`),
  KEY `criteria_group_ibfk2` (`id_rubric`),
  KEY `criteria_group_ibfk3` (`id_assessment`),
  CONSTRAINT `criteria_group_ibfk1` FOREIGN KEY (`id_origin`) REFERENCES `criteria_group` (`id_criteria_group`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `criteria_group_ibfk2` FOREIGN KEY (`id_rubric`) REFERENCES `rubric` (`id_rubric`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `criteria_group_ibfk3` FOREIGN KEY (`id_assessment`) REFERENCES `assessment` (`id_assessment`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `criterion`;
CREATE TABLE `criterion` (
  `id_criterion` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_origin` int(10) unsigned DEFAULT NULL,
  `id_rubric` int(10) unsigned DEFAULT NULL,
  `id_assessment` int(10) unsigned DEFAULT NULL,
  `id_section` int(10) unsigned DEFAULT NULL COMMENT 'Not used yet',
  `id_criteria_group` int(10) unsigned DEFAULT NULL,
  `type` enum('descr','ob') NOT NULL DEFAULT 'descr',
  `position` smallint(6) NOT NULL DEFAULT 0,
  `description` longtext DEFAULT NULL,
  `relative_weight` double(8,2) DEFAULT NULL,
  `comment` longtext DEFAULT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_criterion`),
  KEY `criterion_id_rubric_foreign` (`id_rubric`),
  KEY `criterion_id_assessment_foreign` (`id_assessment`),
  KEY `criterion_id_criteria_group_foreign` (`id_criteria_group`),
  KEY `criterion_id_origin_foreign` (`id_origin`),
  CONSTRAINT `criterion_ibfk1` FOREIGN KEY (`id_origin`) REFERENCES `criterion` (`id_criterion`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `criterion_ibfk2` FOREIGN KEY (`id_criteria_group`) REFERENCES `criteria_group` (`id_criteria_group`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `criterion_ibfk3` FOREIGN KEY (`id_rubric`) REFERENCES `rubric` (`id_rubric`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `criterion_ibfk4` FOREIGN KEY (`id_assessment`) REFERENCES `assessment` (`id_assessment`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `descrob`;
 CREATE TABLE `descrob` (
  `id_descrob` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_origin` int(10) unsigned DEFAULT NULL,
  `id_criterion` int(10) unsigned DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `value` double(8,2) DEFAULT NULL,
  `position` smallint(6) NOT NULL DEFAULT 0,
  `selected` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_descrob`),
  KEY `descrob_id_criterion_foreign` (`id_criterion`),
  KEY `descrob_id_origin_foreign` (`id_origin`),
  CONSTRAINT `descrob_ibfk1` FOREIGN KEY (`id_criterion`) REFERENCES `criterion` (`id_criterion`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `descrob_ibfk2` FOREIGN KEY (`id_origin`) REFERENCES `descrob` (`id_descrob`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1165 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `assessed_student`;
CREATE TABLE `assessed_student` (
  `id_assessed_student` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_assessment` int(10) unsigned NOT NULL,
  `id_student` int(10) unsigned NOT NULL,
  `missing` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `bonus` int(11) NOT NULL DEFAULT 0,
  `bonus_comment` longtext DEFAULT NULL,
  PRIMARY KEY (`id_assessed_student`),
  UNIQUE KEY `assessed_student_id_student_id_assessment_unique` (`id_student`,`id_assessment`),
  KEY `assessed_student_id_assessment_foreign` (`id_assessment`),
  CONSTRAINT `assessed_ibfk1` FOREIGN KEY (`id_assessment`) REFERENCES `assessment` (`id_assessment`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `assessed_ibfk2` FOREIGN KEY (`id_student`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- labnbook.simulation_global_knowledge definition

CREATE TABLE `simulation_global_knowledge` (
	`id_global_knowledge` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`simulation_code` varchar(32) NOT NULL,
	`description` longtext DEFAULT NULL,
	`position` smallint(6) DEFAULT NULL,
	`created_at` timestamp NULL DEFAULT NULL,
	`updated_at` timestamp NULL DEFAULT NULL,
	PRIMARY KEY (`id_global_knowledge`),
	UNIQUE KEY `simulation_constraint_simulation_code_id_constraint_unique` (`simulation_code`,`id_global_knowledge`)
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- labnbook.simulation_task_type definition

CREATE TABLE `simulation_task_type` (
	`id_task_type` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`id_overtask_type` int(10) unsigned DEFAULT NULL,
	`id_global_knowledge` int(10) unsigned DEFAULT NULL,
	`simulation_code` varchar(32) NOT NULL,
	`description` longtext DEFAULT NULL,
	`technological_feedback` longtext DEFAULT NULL,
	`learning_goal` smallint(6) DEFAULT NULL,
	`created_at` timestamp NULL DEFAULT NULL,
	`updated_at` timestamp NULL DEFAULT NULL,
	`position` smallint(6) DEFAULT NULL,
	PRIMARY KEY (`id_task_type`),
	UNIQUE KEY `simulation_task_type_simulation_code_id_task_type_unique` (`simulation_code`,`id_task_type`),
	KEY `simulation_task_type_id_overtask_type_foreign` (`id_overtask_type`),
	KEY `simulation_task_type_id_global_knowledge_foreign` (`id_global_knowledge`),
	CONSTRAINT `simulation_task_type_id_overtask_type_foreign` FOREIGN KEY (`id_overtask_type`) REFERENCES `simulation_task_type` (`id_task_type`) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT `simulation_task_type_id_global_knowledge_foreign` FOREIGN KEY (`id_global_knowledge`) REFERENCES `simulation_global_knowledge` (`id_global_knowledge`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- labnbook.simulation_constraint definition

CREATE TABLE `simulation_constraint` (
  `id_constraint` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `simulation_code` varchar(32) NOT NULL,
  `id_task_type` int(10) unsigned DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `position` smallint(6) DEFAULT NULL,
  `satisfaction_condition_key` text DEFAULT NULL,
  `constraint_message` longtext DEFAULT NULL,
  `theoretical_feedback` longtext DEFAULT NULL,
  `technical_feedback` longtext DEFAULT NULL,
  `correction_feedback` longtext DEFAULT NULL,
  `simulation_enabled` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_constraint`),
  UNIQUE KEY `simulation_constraint_simulation_code_id_constraint_unique` (`simulation_code`,`id_constraint`),
  KEY `simulation_constraint_id_task_type_foreign` (`id_task_type`),
  CONSTRAINT `simulation_constraint_id_task_type_foreign` FOREIGN KEY (`id_task_type`) REFERENCES `simulation_task_type` (`id_task_type`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- labnbook.simulation_trace definition

CREATE TABLE `simulation_trace` (
  `id_simulation_trace` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `simulation_code` varchar(32) NOT NULL,
  `trace_action` varchar(32) NOT NULL,
  `action_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `id_user` int(10) unsigned NOT NULL,
  `id_mission` int(10) unsigned DEFAULT NULL,
  `id_report` int(10) unsigned DEFAULT NULL,
  `id_labdoc` int(10) unsigned NOT NULL,
  `attributes` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`attributes`)),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_simulation_trace`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- labnbook.simulation_link_learner_task definition

CREATE TABLE `simulation_link_learner_task` (
  `simulation_code` varchar(32) NOT NULL,
  `id_learner` int(10) unsigned NOT NULL,
  `id_task_type` int(10) unsigned NOT NULL,
  `feedback_level` smallint(6) DEFAULT NULL,
  `knowledge` double(10,9) DEFAULT NULL,
  PRIMARY KEY (`simulation_code`,`id_learner`,`id_task_type`),
  KEY `simulation_link_learner_task_id_learner_foreign` (`id_learner`),
  KEY `simulation_link_learner_task_id_task_type_foreign` (`id_task_type`),
  CONSTRAINT `simulation_link_learner_task_id_learner_foreign` FOREIGN KEY (`id_learner`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `simulation_link_learner_task_id_task_type_foreign` FOREIGN KEY (`id_task_type`) REFERENCES `simulation_task_type` (`id_task_type`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- labnbook.simulation_link_relevance_constraint definition

CREATE TABLE `simulation_link_relevance_constraint` (
  `simulation_code` varchar(32) NOT NULL,
  `id_constraint` int(10) unsigned NOT NULL,
  `id_relevance_constraint` int(10) unsigned NOT NULL,
  PRIMARY KEY (`simulation_code`,`id_constraint`,`id_relevance_constraint`),
  KEY `sim_constraint_foreign` (`id_constraint`),
  KEY `sim_relevance_constraint_foreign` (`id_relevance_constraint`),
  CONSTRAINT `sim_constraint_foreign` FOREIGN KEY (`id_constraint`) REFERENCES `simulation_constraint` (`id_constraint`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sim_relevance_constraint_foreign` FOREIGN KEY (`id_relevance_constraint`) REFERENCES `simulation_constraint` (`id_constraint`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- labnbook.simulation_link_super_manager definition

CREATE TABLE `simulation_link_super_manager` (
  `simulation_code` varchar(32) NOT NULL,
  `id_super_manager` int(10) unsigned NOT NULL,
  PRIMARY KEY (`simulation_code`,`id_super_manager`),
  KEY `simulation_link_super_manager_id_super_manager_foreign` (`id_super_manager`),
  CONSTRAINT `simulation_link_super_manager_id_super_manager_foreign` FOREIGN KEY (`id_super_manager`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


--
-- data
--

INSERT INTO `migration` VALUES (UNHEX('874D53D0123B7C32AD7063D27BE88064C59D78B2'), '2018-03-19_1910_trace_add_conv.sql', UNHEX('0E54F2E3279B4F686AF0D3C83711828E3D4A9E36'), 1528338405, 20181102142908);
INSERT INTO `migration` VALUES (UNHEX('31647331D90110D48044CBE49F19AD997A1CCB39'), '2018-01-22_1300_report-msg.sql', UNHEX('3E41DE27C5A2E80F6AA96E6E42A0AFF14ED49488'), 1522418594, 20181102142908);
INSERT INTO `migration` VALUES (UNHEX('CB160B29D7E313CCCC1F9A24F3749CC4FDE13627'), '2018-04-10_1210_fix-annotation-data.sql', UNHEX('E8B1F2DF8E36CC1BBEA5EC0D7585C880FBD627C3'), 1528338405, 20181102142908);
INSERT INTO `migration` VALUES (UNHEX('7BA69705BD9000A3038D296C3B650386840A9A09'), '2018-03-22_1005_unique_inst-number.sql', UNHEX('03182E9992A9109779CE7730F7DB158CD1C9845B'), 1528338405, 20181102142908);
INSERT INTO `migration` VALUES (UNHEX('3D49FA28DA3C332BD4BB2152CC74AB6A6AB1F3CA'), '2017-12-28_1200_commentaires.sql', UNHEX('E97630E4AC94CCB0697BEA17BAAB15F5612E0F66'), 1522418594, 20181102142908);
INSERT INTO `migration` VALUES (UNHEX('BA839DE76322C4EBED1C3BDA062A19E66A2FA5B3'), '2018-03-02_0847_trace.sql', UNHEX('2D5D6EB7A1DBD646E1F16432EE9113A8AC3C5FE4'), 1528338405, 20181102142908);
INSERT INTO `migration` VALUES (UNHEX('4BF4502950421976BB765F23F9847E9644ABBA14'), '2018-04-20_1144_remove-server-name-from-URLs.sql', UNHEX('6F5AD1E0AB1AC82586F31D782436F2A63DFD1DF7'), 1528338405, 20181102142908);
INSERT INTO `migration` VALUES (UNHEX('AD0AB41B51D318AE014E1E3A923FC1835BDD42E1'), '2018-03-16_0924_traces-extended.sql', UNHEX('87F6FB37838212C84385560932EAD0908088B7E6'), 1528338405, 20181102142908);
INSERT INTO `migration` VALUES (UNHEX('4FF5815AF73B254676C3A55ED3B6DB153011DF84'), '2018-02-08_1019_missing-fk.sql', UNHEX('126A88B4C558BD159E9470DFE2B9D24A3DEAC8DF'), 1522418594, 20181102142908);
INSERT INTO `migration` VALUES (UNHEX('CA5FE11FCB255017BE87C6130A90B69FD6F5641D'), '2018-04-06_1138_traces-fix-actions.sql', UNHEX('BF6C0B386EBAB4CF7CEB140EBFF04A8042355A55'), 1528338405, 20181102142908);
INSERT INTO `migration` VALUES (UNHEX('2798370267F702988DCE802D96D26AD3550BF26A'), '2018-01-23_1200_debug-comment.sql', UNHEX('52D8A202847598CC627BBD65FA097B7C1522F58A'), 1522418594, 20181102142908);
INSERT INTO `migration` VALUES (UNHEX('E6E68A91F9A4DB2EC142192BCE2E85B87D25386F'), '2018-04-20_1748_teams.sql', UNHEX('5BCFCAE6647816F8A78133BDB7CEC04810E35975'), 1538145041, 20181102142908);
INSERT INTO `migration` VALUES (UNHEX('319999CF019FBB1E1A0DCBB4B2C243D78A202466'), '2018-04-19_1240_null-password-for-CAS.sql', UNHEX('993EE703F5EEA3F70A42D5FD009CB28E2B0A75A1'), 1530282861, 20181102142908);
INSERT INTO `migration` VALUES (UNHEX('07600B0031591AFF1E911464076696210F587366'), '2019-04-02_1725_team-config.sql', UNHEX('7C9283918816394C5320A45A7A16D1A5B53AC6C7'), 1538145041, 20181102142908);
INSERT INTO `migration` VALUES (UNHEX('1DADA0D18C37106A7C2448B81180A46E07ED4B9C'), '2018-01-11_1200_widget-msg.sql', UNHEX('A52460AB894D21CE900CF33827ECC8C33EF56D49'), 1522418594, 20181102142908);
INSERT INTO `migration` VALUES (UNHEX('0FC3530C8BC981368FF0A1A7E3F94AF060616194'), '2018-01-22_1200_conversation-msg.sql', UNHEX('5660C049E905D9551827D0B1F45F15051C6EC937'), 1522418594, 20181102142908);
INSERT INTO `migration` VALUES (UNHEX('14CD969090B9E8EB8FB08B74A588F088F51AB54F'), '2018-03-01_1656_labdocfile.sql', UNHEX('B75D8903EDD6357E64A04277DB045698AD01F352'), 1528338405, 20181102142908);
INSERT INTO `migration` VALUES (UNHEX('610ADEB57874A6CB6290679DC25307A16D5F1310'), '2018-08-30_0820_fitex-column-code.php', UNHEX('82706417F5930CFA04775E4AAD87FED4C1B53CCF'), 1538145041, 20181102143625);
INSERT INTO `migration` VALUES (UNHEX('B6E37343A633580F763FCB94A95415665A3F17A0'), '2018-07-16_import-ld-from-report.sql', UNHEX('7AD4B31B3D7FFEA346F324D15BF17380BDCAE98F'), 1538145041, 20181102143625);
INSERT INTO `migration` VALUES (UNHEX('9FA78D13E52322BF1AF09D5F621FC2218C4FEA95'), '2018-08-11_import-ld-from-report.sql', UNHEX('1F2BDB2AE5B9483E683506BFC87820215279BC97'), 1538145041, 20181102143625);
INSERT INTO `migration` VALUES (UNHEX('DF76DD4928AE79625BB17CA820D9CCFEF4E51AC3'), '2018-09-11_0951_team_config_name-prefix.sql', UNHEX('9F55F66A695C284058992768DC965D6DEF617BE2'), 1538398118, 20181102143654);
INSERT INTO `migration` VALUES (UNHEX('43FB2C400127656A4B94D9CA7CD49F35ABD76329'), '2018-09-07_1700_team_config-import_ld.sql', UNHEX('8857F667E97C4EE1530B89136985141499EFB553'), 1538398118, 20181102143654);
INSERT INTO `migration` VALUES (UNHEX('3F7DCC67848983754D19B3E0A42DA004D369D4DD'), '2018-09-25_1732_update_time.sql', UNHEX('7B008C1747BCD2580844C8515960572C19A479FD'), 1538398118, 20181102143654);
INSERT INTO `migration` VALUES (UNHEX('2D6CC144FACD5E0C07443BDAB733573053154C1E'), '2018-09-27_1732_remove-empty-graphs.sql', UNHEX('9B8C6C868EF7BC51873E24458E60DD333D574DC7'), 1538398118, 20181127153131);
INSERT INTO `migration` VALUES (UNHEX('8B4B454536B9DD0DB3F1CF4182DF761150165F45'), '2018-10-23_1412_report_initialized.sql', UNHEX('2F28944B39A93998B8A5BA7011F3F00F38738A23'), 1543306762, 20181127153131);
INSERT INTO `migration` VALUES (UNHEX('0D366C6B4CDBC2FBDB0AEC85CD2D8C5FA34CF7A4'), '2018-10-02_0942_demo-requisites.sql', UNHEX('D15F65B5E1E10AF66BCD1E79AF6D5CE5D872277D'), 1543307296, 20181127153131);
INSERT INTO `migration` VALUES (UNHEX('3546B8B62EBC418E85BA300089F9DD0F9E095BCE'), '2018-10-16_1242_trace_teacher-view-report.sql', UNHEX('AAEF973FC800F193328E232B050E98BA209756BD'), 1543306762, 20181127153131);
INSERT INTO `migration` VALUES (UNHEX('2EAE06A13853F20AEAC7A90560207002519A59AE'), '2018-11-22_1357_drop_trace_userrole.sql', UNHEX('F99C13D02BD7FBFC63120A726C4134494C202062'), 1543307296, 20181127153132);
INSERT INTO `migration` VALUES (UNHEX('43BCB34295B80FD38144040916C031BC2FF8FA09'), '2018-11-23_1232_fix-truncated-annotations.sql', UNHEX('A0D5889B327D5C512DFE237CA97A03C13A6F2331'), 1543307296, 20181127153132);
INSERT INTO `migration` VALUES (UNHEX('3560F6A5F33C0E61A97665C2AD2C7EBD710F1E48'), '2018-11-27_1549_alter-trace-actions.sql', UNHEX('A1909542D2A1EDC383A8532E279F45378F02B887'), 1543330241, 20181127155358);
INSERT INTO `migration` VALUES (UNHEX('E63FE74EF079BBC12C56730124193881BE407BC7'), '2018-11-23_1545_rename_conv-user_last-seen.sql', UNHEX('4A8D7A0F21AB5C7A9714DC38F8D8E1C977D92839'), 1544170080, 20190205141748);
INSERT INTO `migration` VALUES (UNHEX('636FFDF1E2A2FF8D1449EC6EC08E90021A81F6C6'), '2018-12-04_1525_traces_teacher.sql', UNHEX('3FEEAEDC1C37E609AFFF50D081F4C86312BDE736'), 1548424981, 20190205141748);
INSERT INTO `migration` VALUES (UNHEX('39140BE471333CC2DD9763D5F69638AC3D1B3026'), '2019-01-11_1055_https-wrapper.php', UNHEX('7F57038B81178DEB8E7B748E56CAF88F327E2536'), 1547569428, 20190205141749);
INSERT INTO `migration` VALUES (UNHEX('81AFF569DB672E0967D89750AB66D9D8EFD67EA3'), '2018-12-11_0852_report-dates-time.sql', UNHEX('E770834131608505EC45B62BAFAEA215956392CE'), 1548424981, 20190205141749);
INSERT INTO `migration` VALUES (UNHEX('050418A41FE85F9AE5DC369742EDF465F14768E3'), '2019-01-25_11746_drafted.sql', UNHEX('2E26B4BFCCB9114F4A158DD8651E4A09B7247044'), 1548752838, 20190205141749);
INSERT INTO `migration` VALUES (UNHEX('19904C80AB438046FA49F63B57426597E90F4C8F'), '2019-01-08_1449_https.sql', UNHEX('52A89C6F22ED51E4521F76256660B04D6FD3CFB4'), 1547713508, 20190205141749);
INSERT INTO `migration` VALUES (UNHEX('9DD0EC35E76892BECEC7BCF2D88CCC857D400567'), '2019-05-03_1636_trace_save-attached-ld.sql', UNHEX('EE876E5679629A61320F4B3D9B7EFDE54B5A9075'), 1559897312, 20190607104844);
INSERT INTO `migration` VALUES (UNHEX('1D208D91FB6CBEFCDDF23E4BDB269FA193D43165'), '2019-02-01_ld_backup.sql', UNHEX('C46F1C2334B400D5E451CAB4D9CE1A92DC65DE28'), 1550565598, 20190607104844);
INSERT INTO `migration` VALUES (UNHEX('5C8EFDA3295FF5D8936A1A5BDE1F2E19F81B860C'), '2019-05-03_1656_split_trace_duplicate-ld.php', UNHEX('3B748E4EF9533AED5376B9E365E23BC8C07D6A8F'), 1559897312, 20190607104844);
INSERT INTO `migration` VALUES (UNHEX('B507AC60C5D84E6C6E86377E6288F6D3204C30CF'), '2019-03-08_trace_action.sql', UNHEX('799045B9EBDB1900AD28CB9260984DE86A0FD101'), 1554382133, 20190607104844);
INSERT INTO `migration` VALUES (UNHEX('F6533FF273708E5411A2F5C03D017EA464F4E86F'), '2019-06-12_1558_trash_widg.sql', UNHEX('ED5B731C2B93EC42CB1DAE98B4BABA58CD18EEA2'), 1591283730, 20200612104339);
INSERT INTO `migration` VALUES (UNHEX('A73370E4CD92A7673B1F1F0F80CC5F637FFD007F'), '2019-12-11_1253_301-fixed-mixed-up-traces.sql', UNHEX('F281ACDCAC379F5275B50272D7126650F7B33A18'), 1591283730, 20200612104339);
INSERT INTO `migration` VALUES (UNHEX('FA9D94B3FE1F9BF400B8293BD650B0619579FC9F'), '2019-08-14_0825_new.sql', UNHEX('DA39A3EE5E6B4B0D3255BFEF95601890AFD80709'), 1591283730, 20200612104339);
INSERT INTO `migration` VALUES (UNHEX('79733C23F2C87BF7AE232E3B66B82B10742C0084'), '2020-02-20_0843_fix-report-status.sql', UNHEX('302CF600E89229CDE6879E3E6980D9D2105ED18E'), 1591283730, 20200612104339);
INSERT INTO `migration` VALUES (UNHEX('43C9C53838C57BBE784658A4FA32F852ED6F13F8'), '2019-08-20_1053_mission-archive.sql', UNHEX('BCC73DFBF43BD00578B08C3E52319EEFEA69D8A6'), 1591283730, 20200612104339);
INSERT INTO `migration` VALUES (UNHEX('4CC5E36AC5A5A445B2CC17851EFF73765CAC2C91'), '2019-08-29_1133_new.sql', UNHEX('DA39A3EE5E6B4B0D3255BFEF95601890AFD80709'), 1591283730, 20200612104339);
INSERT INTO `migration` VALUES (UNHEX('7854ECABD9A02DB2A07F23EFB2064B4E4E61A4E4'), '2020-06-11_1650_table_annotation_remove_attributes_column_update_time.sql', UNHEX('247FFEE913BB034652DFC7E0E5BE53CB1CC232FB'), 1591951067, 20200612104339);
INSERT INTO `migration` VALUES (UNHEX('6065D15341D40A3DEDA279EA16CF34602616D4D5'), '2019-06-06_0953_create_index_trace.sql', UNHEX('5EACF51D4C663654F2662EB23FA71AB4A51979E0'), 1591283730, 20200612104339);
INSERT INTO `migration` VALUES (UNHEX('6B15F2099C463D98FDE0D1452AF5C6418D2B8613'), '2019-05-31_1550_labdoc_deleted_type_update.sql', UNHEX('FEB9108EDE141CB8D1F33FF28374697CCB47853D'), 1591283730, 20200612104339);
INSERT INTO `migration` VALUES (UNHEX('6CE82828CAB7BD28244306ECC8B1B6A02847319A'), '2019-09-17_1553_old_student_teaming.sql', UNHEX('62DCC2550E1EB3E35BBE73873EEA986A43F527A4'), 1591283730, 20200612104339);
INSERT INTO `migration` VALUES (UNHEX('6D725D4FFCD333F875F068DDFF409590B1817B7F'), '2019-12-11_1646_302-duplicate-traces.sql', UNHEX('9E28F5F8DC716631D2EBE08668063884FF291AFF'), 1591283730, 20200612104339);
INSERT INTO `migration` VALUES (UNHEX('6DD4D23230233947E3F8CE3F55E0F3A3319070CB'), '2019-08-28_0816_new.sql', UNHEX('DA39A3EE5E6B4B0D3255BFEF95601890AFD80709'), 1591283730, 20200612104339);
INSERT INTO `migration` VALUES (UNHEX('0F7F78C05761E351AEC1220E0623191CB3D84BB6'), '2020-03-29_1748_fix_pb_timestamp.sql', UNHEX('C6ADBA595362D73F83A0EAB89E6D0E9137E83A7C'), 1591283730, 20200612105827);
INSERT INTO `migration` VALUES (UNHEX('A6D75DB31A7C7EFFE8353BF4357060623A2E8F5A'), '2020-06-11_1650_annotation_remove_current_timestamp.sql', UNHEX('45E702FEB61B3E8438ECE2F4BCC1FF4B0B0EA375'), 1591959460, 20200612105827);
INSERT INTO `migration` VALUES (UNHEX('3578CAFB978DC0304DA031CEAC9F18C4DB32B14B'), '2019-05-14_1752_user_language.sql', UNHEX('B7DCEAA8959D47E81564CD68D524CFCB67281B8D'), 1593014225, 20200624155708);
INSERT INTO `migration` VALUES (UNHEX('C9100C2BD3212E093D299A4343877BD54E46A4A0'), '2019-06-12_0950_update_class.sql', UNHEX('B7DCEAA8959D47E81564CD68D524CFCB67281B8D'), 1593014282, 20200624155805);
INSERT INTO `migration` VALUES (UNHEX('FAA57687C5CA0D5FA040343D998ED70C6CBFFB9E'), '2019-09-12_1347_teamconfig_unicity.sql', UNHEX('D517C4E5CEC18D1B9172FDF50BBBD69AF4875236'), 1593014325, 20200624155848);
INSERT INTO `migration` VALUES (UNHEX('7FDE4D7C976EF24D48C6A4DDA0CEE8FA13D51608'), '2020-01-14_1753_reformat-lb_tracks.sql', UNHEX('D517C4E5CEC18D1B9172FDF50BBBD69AF4875236'), 1593014357, 20200624155920);
INSERT INTO `migration` VALUES (UNHEX('428618F27F2565D1BAF009D5AB5B6783514D584C'), '2020-02-17_1843_class_remove_enddate.sql', UNHEX('D517C4E5CEC18D1B9172FDF50BBBD69AF4875236'), 1593014382, 20200624155946);
INSERT INTO `migration` VALUES (UNHEX('39501ECB65DFBBC528889E685E8316195EB5A300'), '2020-03-22_1114_harmonize-timestamps.sql', UNHEX('E255E9A0525461EB520C52811CAF38214677A41C'), 1593014417, 20200624160022);
INSERT INTO `migration` VALUES (UNHEX('AE359B9F8F942DA8F2090A75FA94EA29A61B81CB'), '2020-06-24_1754_drop_jobs.sql', UNHEX('041DC838403C91BD66CB476A41075E498F2FCB3A'), 1593014086, 20200624160042);
INSERT INTO `migration` VALUES (UNHEX('16DBEAC738EB31193D47AE3535046F940011D8E9'), '2020-03-31_1220_remove_researcher_status.sql', UNHEX('E255E9A0525461EB520C52811CAF38214677A41C'), 1593014439, 20200624160042);
INSERT INTO `migration` VALUES (UNHEX('3FDEED1B61E3F44A9324FC78CAF8C66050F462C1'), '2020-06-24_1800_new.sql', UNHEX('DA39A3EE5E6B4B0D3255BFEF95601890AFD80709'), 1593014444, 20200624160839);
INSERT INTO `migration` VALUES (UNHEX('D84C57B29AB0F6C38F7EC4F02270BB81D7C4DCFD'), '2020-09-09_1726_add_action_TEACHER_SELECT_REPORT.sql', UNHEX('7FB6CBFE2906914B6259100850778E53AE6EDC16'), 1599665320, 20200909152914);
INSERT INTO `migration` VALUES (UNHEX('241D6A3B6DB4954C2F71485DAA8B797207917AD1'), '2020-09-16_1343_move_ext_inst_to_ext_platform.sql', UNHEX('8A7A16CC677E3B21FFBD898F611D503CA774B8B0'), 1600270878, 20200916154249);
INSERT INTO `migration` VALUES (UNHEX('2E59E0DAEA69142A6EB2792370772EF4895FEF16'), '2020-09-17_13439_add_extplatform_url.sql', UNHEX('368917BA77D76A452A443B0AA3FE02CA297FDB45'), 1600342787, 20200917114020);
INSERT INTO `migration` VALUES (UNHEX('44D4EC65BB3B330666E659A358CE79BA195ED438'), '2020-10-02_1148_remove_ON_UPDATE_CURRENT_TIMESTAMP.sql', UNHEX('511BD34466107B8EB965FA50A2705DC957EB6C5D'), 1601633310, 20201002100849);

INSERT INTO `migrations` VALUES (1, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (2, '2019_10_08_124640_add_jwt_fields', 2);
INSERT INTO `migrations` VALUES (3, '2019_11_05_141239_class_external_code', 2);
INSERT INTO `migrations` VALUES (4, '2020_03_12_150108_add_maillog', 2);
INSERT INTO `migrations` VALUES (11, '2020_05_16_153307_create_jobs_table', 3);
INSERT INTO `migrations` VALUES (12, '2020_05_18_143550_fix_zwibler_urls', 3);
INSERT INTO `migrations` VALUES (15, '2020_06_04_134521_report_soft_delete', 5);
INSERT INTO `migrations` VALUES (16, '2020_06_08_110755_add_popularity_annotation', 6);
INSERT INTO `migrations` VALUES (18, '2020_05_18_154916_fix_tinymce_img_src', 7);
INSERT INTO `migrations` VALUES (19, '2020_06_24_172217_fix_teacher_images', 8);

INSERT INTO trace_action VALUES (1,'connect_lnb','administration');
INSERT INTO trace_action VALUES (2,'disconnect_lnb','administration');
INSERT INTO trace_action VALUES (3,'enter_report','administration');
INSERT INTO trace_action VALUES (4,'leave_report','administration');
INSERT INTO trace_action VALUES (5,'submit_report','administration');
INSERT INTO trace_action VALUES (6,'add_ld','production');
INSERT INTO trace_action VALUES (7,'duplicate_ld','production');
INSERT INTO trace_action VALUES (8,'edit_ld','production');
INSERT INTO trace_action VALUES (9,'modify_ld','production');
INSERT INTO trace_action VALUES (10,'validate_ld','production');
INSERT INTO trace_action VALUES (11,'toggle_ld_status','production');
INSERT INTO trace_action VALUES (12,'update_ld_name','production');
INSERT INTO trace_action VALUES (13,'delete_ld','production');
INSERT INTO trace_action VALUES (14,'open_annotation','information');
INSERT INTO trace_action VALUES (15,'open_rp_assignment','information');
INSERT INTO trace_action VALUES (16,'open_assignment','information');
INSERT INTO trace_action VALUES (17,'open_detailed_assignment','information');
INSERT INTO trace_action VALUES (18,'open_resource','information');
INSERT INTO trace_action VALUES (19,'add_resource','communication');
INSERT INTO trace_action VALUES (20,'open_comment','communication');
INSERT INTO trace_action VALUES (21,'add_comment','communication');
INSERT INTO trace_action VALUES (22,'open_conversation','communication');
INSERT INTO trace_action VALUES (23,'add_message','communication');
INSERT INTO trace_action VALUES (24,'add_conversation','communication');
INSERT INTO trace_action VALUES (25,'import_ld','production');
INSERT INTO trace_action VALUES (26,'save_attached_ld','production');
INSERT INTO trace_action VALUES (27,'recover_ld', 'production');
INSERT INTO trace_action VALUES (28,'open_ld_versioning', 'production');
INSERT INTO trace_action VALUES (29, 'hard_delete_ld', 'production');
INSERT INTO trace_action VALUES (31,'follow_report','t_report');
INSERT INTO trace_action VALUES (32,'test_mission','t_mission');
INSERT INTO trace_action VALUES (33,'modify_mission','t_mission');
INSERT INTO trace_action VALUES (34,'add_mission','t_mission');
INSERT INTO trace_action VALUES (35,'duplicate_mission','t_mission');
INSERT INTO trace_action VALUES (36,'delete_mission','t_mission');
INSERT INTO trace_action VALUES (37,'add_resource','t_mission');
INSERT INTO trace_action VALUES (38,'add_ld','t_mission');
INSERT INTO trace_action VALUES (39,'add_rp','t_mission');
INSERT INTO trace_action VALUES (40,'add_class','t_student');
INSERT INTO trace_action VALUES (41,'delete_class','t_student');
INSERT INTO trace_action VALUES (42,'add_student','t_student');
INSERT INTO trace_action VALUES (43,'delete_student','t_student');
INSERT INTO trace_action VALUES (44,'add_report','t_student');
INSERT INTO trace_action VALUES (45,'edit_report','t_report');
INSERT INTO trace_action VALUES (46,'delete_report','t_report');
INSERT INTO trace_action VALUES (47,'annotate_report','t_report');
INSERT INTO trace_action VALUES (48,'add_teaming','t_student');
INSERT INTO trace_action VALUES (49,'create_user','t_student');
INSERT INTO trace_action VALUES (50,'select_report', 't_report');
INSERT INTO trace_action VALUES (57,'user_merged', 'administration');
INSERT INTO trace_action VALUES (58, 'scoring_save_grid', 't_mission');
INSERT INTO trace_action VALUES (59, 'scoring_save_eval', 't_report');
INSERT INTO trace_action VALUES (60, 'scoring_publish_eval', 't_report');
INSERT INTO trace_action VALUES (61, 'scoring_edit_eval', 't_report');
INSERT INTO trace_action VALUES (62, 'scoring_view_eval', 'information');
INSERT INTO trace_action VALUES (63, 'modify_rp', 't_mission');
INSERT INTO trace_action VALUES (65, 'task_open_widget', 'information');
INSERT INTO trace_action VALUES (67, 'task_create', 'information');
INSERT INTO trace_action VALUES (68, 'task_edit', 'information');
INSERT INTO trace_action VALUES (69, 'task_delete', 'information');
INSERT INTO trace_action VALUES (70, 'task_completed', 'information');
INSERT INTO trace_action VALUES (71, 'task_pairing', 'information');
INSERT INTO trace_action VALUES (72, 'task_sort', 'information');
INSERT INTO trace_action VALUES (74, 'task_move', 'information');

ALTER TABLE `trace_action` CHANGE `action_type` `action_type` ENUM('administration','production','information','communication','t_mission','t_student','t_report','m_teacher') CHARACTER SET ascii COLLATE ascii_bin NOT NULL;

INSERT INTO trace_action (id_action,action,action_type) VALUES (51,'add_teacher_status','m_teacher');
INSERT INTO trace_action (id_action,action,action_type) VALUES (52,'remove_teacher_status','m_teacher');

ALTER TABLE `trace_action` CHANGE `action_type` `action_type` ENUM('administration','production','information','communication','t_mission','t_student','t_report','m_teacher') CHARACTER SET ascii COLLATE ascii_bin NOT NULL;

INSERT INTO institution VALUES (1,'Tōdai',NULL, False);

INSERT INTO user VALUES (1,'Admin','','admin','f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17','someone@example.org',NULL,1,NULL,NULL,NULL,NULL,'2018-01-01 00:00:00',NULL,'2018-01-01 00:00:00');
INSERT INTO teacher VALUES (1,'Admin','univ','',NULL, 'admin');

-- Parts of the code have hardcoded behavior on "UGA"
INSERT INTO institution VALUES (2, 'UGA', NULL, True);
-- Parts of the code have hardcoded behavior on "UGA"
INSERT INTO extplatform VALUES (3, 2, 'API', 'TV8b<Nx@h3DtVf2=TvXqrPD=JBR|tzLL', 'http://labnbook-moodle.fr', 'Alice (alice@example.org)', null, null);

-- 8 teachers
INSERT INTO user VALUES (2,'Teacher1','Yukiko','teacher1','f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17','teacher1@example.org',NULL,2,NULL,NULL,NULL,NULL,'2018-01-01 00:00:00',NULL,'2018-01-01 00:00:00');
INSERT INTO teacher VALUES (2,'Biologie','univ','',NULL,'teacher');
INSERT INTO user VALUES (11,'Teacher2','Alain','teacher2','f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17','teacher2@example.org',NULL,2,NULL,NULL,NULL,NULL,'2018-01-01 00:00:00',NULL,'2018-01-01 00:00:00');
INSERT INTO teacher VALUES (11,'Physique','univ','',NULL,'manager');
INSERT INTO user VALUES (12,'Teacher3','Pierre','teacher3','f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17','teacher3@example.org',NULL,2,NULL,NULL,NULL,NULL,'2018-01-01 00:00:00',NULL,'2018-01-01 00:00:00');
INSERT INTO teacher VALUES (12,'Philosophie','univ','',NULL,'manager');
INSERT INTO user VALUES (13,'Teacher4','Paul','teacher4','f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17','teacher4@example.org',NULL,2,NULL,NULL,NULL,NULL,'2018-01-01 00:00:00',NULL,'2018-01-01 00:00:00');
INSERT INTO teacher VALUES (13,'Économie','univ','',NULL,'teacher');
INSERT INTO user VALUES (14,'Teacher5','Jacques','teacher5','f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17','teacher5@example.org',NULL,1,NULL,NULL,NULL,NULL,'2018-01-01 00:00:00',NULL,'2018-01-01 00:00:00');
INSERT INTO teacher VALUES (14,'Logistique','univ','',NULL,'teacher');
INSERT INTO user VALUES (15,'Teacher6','Louis','teacher6','f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17','teacher6@example.org',NULL,1,NULL,NULL,NULL,NULL,'2018-01-01 00:00:00',NULL,'2018-01-01 00:00:00');
INSERT INTO teacher VALUES (15,'Économie','univ','',NULL,'teacher');
INSERT INTO user VALUES (16,'Teacher7','Michel','teacher7','f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17','teacher7@example.org',NULL,1,NULL,NULL,NULL,NULL,'2018-01-01 00:00:00',NULL,'2018-01-01 00:00:00');
INSERT INTO teacher VALUES (16,'Logistique','univ','',NULL,'teacher');
INSERT INTO user VALUES (17,'Teacher8','Henri','teacher8','f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17','teacher8@example.org',NULL,2,NULL,NULL,NULL,NULL,'2018-01-01 00:00:00',NULL,'2018-01-01 00:00:00');
INSERT INTO teacher VALUES (17,'Économie','univ','',NULL,'teacher');
INSERT INTO user VALUES (18,'Teacher9','Simulateur','teacher9','f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17','teacher9@example.org',NULL,2,NULL,NULL,NULL,NULL,'2018-01-01 00:00:00',NULL,'2018-01-01 00:00:00');
INSERT INTO teacher VALUES (18,'Chimie','univ','',NULL,'teacher');
INSERT INTO simulation_link_super_manager VALUES ('simulation-test',18);

INSERT INTO link_inst_teacher VALUES
(1, 2),
(2, 2),
(11, 2),
(12, 2),
(13, 2),
(14, 1),
(15, 1),
(16, 1),
(17, 2);

-- students
INSERT INTO user VALUES (3,'CAS user','Masako','user-CAS',null,'user-cas@example.org',NULL,2,'12345679',NULL,NULL,NULL,'2018-01-01 00:00:00',NULL,'2018-01-01 00:00:00');
INSERT INTO user VALUES (4,'Student1','Kōtaro','student1','f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17','student1@example.org',NULL,2,NULL,NULL,NULL,NULL,'2019-01-01 12:00:00',NULL,'2018-01-01 00:00:00');
INSERT INTO user VALUES (5,'Student2','Shinji','student2','f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17','student2@example.org',NULL,2,NULL,NULL,NULL,NULL,'2019-01-01 12:00:00',NULL,'2018-01-01 00:00:00');
INSERT INTO user VALUES (61, 'Student3','Masato','student3','f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17','student3@example.org',NULL,2,NULL,NULL,NULL,NULL,'2019-01-01 12:00:00',NULL,'2018-01-01 00:00:00');
INSERT INTO user VALUES (7,'Student4','Évariste','student4','f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17','student4@example.org',NULL,2,NULL,NULL,NULL,NULL,'2019-01-01 12:00:00',NULL,'2018-01-01 00:00:00');
INSERT INTO user VALUES (8,'Student5','N''ji','student5','f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17','student5@example.org',NULL,2,NULL,NULL,NULL,NULL,'2019-01-01 12:00:00',NULL,'2018-01-01 00:00:00');
INSERT INTO user VALUES (9,'Student6','Sans-classe','student6','f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17','student6@example.org',NULL,2,NULL,NULL,NULL,NULL,'2019-01-01 12:00:00',NULL,'2018-01-01 00:00:00');
INSERT INTO user VALUES (10,'Uuga','Uuga','uuga','f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17','student-uga@example.org',NULL,2,NULL,NULL,NULL,NULL,'2019-01-01 12:00:00',NULL,'2018-01-01 00:00:00');

-- mission with little content
INSERT INTO `mission` VALUES (1,'mission1','Mission première','<p>Ceci est la première mission</p>','<p>Réussir !</p>', 0, 'private', null,'2019-01-01','2019-01-02');
INSERT INTO `link_mission_teacher` VALUES (1,2,'designer');
INSERT INTO `report_part` VALUES (1,1,1,'Global',1,1,1,1,0,'<p>Tout doit s''inscrire ci-dessous.</p>');
INSERT INTO `ressource` VALUES (1,1,NULL,'consigne_longue','1-LabNbook.pdf','assignment',NULL);

INSERT INTO `mission` VALUES (3,'mission3','mission 3 vide','<p>La troisième mission est vide</p>','', 1, 'private',null,'2019-01-03','2019-01-03');
INSERT INTO `link_mission_teacher` VALUES (3,2,'designer');
INSERT INTO `mission` VALUES (4,'mission4','mission 4 vide','<p>La quatrième mission est vide</p>','', 0, 'private',null,'2019-01-03','2019-01-03');
INSERT INTO `link_mission_teacher` VALUES (4,1,'designer');
INSERT INTO `mission` VALUES (5,'mission5','mission physique','<p>La mission physique est vide</p>','', 1, 'private',null,'2019-01-03','2019-01-03');
INSERT INTO `link_mission_teacher` VALUES (5,11,'designer');
INSERT INTO `mission` VALUES (6,'mission6philo','mission philosophie','<p>La mission philosophie est vide</p>', '', 0,'private',null,'2019-01-03','2019-01-03');
INSERT INTO `link_mission_teacher` VALUES (6,11,'designer');
INSERT INTO `mission` VALUES (7,'mission7philo','mission philosophie2','<p>La mission philosophie est vide</p>', '', 1, 'private',null,'2019-01-03','2019-01-03');
INSERT INTO `link_mission_teacher` VALUES (7,11,'designer');

-- class
INSERT INTO `class` VALUES (1,2,'classe 1','code_classe1','2019-01-01 12:00:00','2019-01-01 12:00:00','normal', null, null, null);
INSERT INTO `link_class_learner` VALUES (1,4);
INSERT INTO `link_class_learner` VALUES (1,5);
INSERT INTO `link_class_learner` VALUES (1,61);
INSERT INTO `link_class_learner` VALUES (1,7);
INSERT INTO `link_class_learner` VALUES (1,8);
INSERT INTO `link_class_teacher` VALUES (2,1);
INSERT INTO `class` VALUES (2,2,'classe 2',NULL,'2019-01-01 12:00:00','2019-01-01 12:00:00','normal', null, null, null);
INSERT INTO `link_class_learner` VALUES (2,4);
INSERT INTO `link_class_teacher` VALUES (2,2);
UPDATE institution SET id_class_teacher = 2 WHERE id_inst = 2;
INSERT INTO `class` VALUES (3,2,'classe 3',NULL,'2019-01-01 12:00:00','2019-01-01 12:00:00','normal', null, null, null);
INSERT INTO `link_class_teacher` VALUES (2,3);
INSERT INTO `class` VALUES (4,2,'classe 4',NULL,'2019-01-01 12:00:00','2019-01-01 12:00:00','normal', null, null, null);
INSERT INTO `link_class_teacher` VALUES (2,4);
INSERT INTO `link_class_learner` VALUES (2,8);
INSERT INTO `class` VALUES (5,2,'classe LDAP',NULL,'2019-01-01 12:00:00','2019-01-01 12:00:00','normal', null, null, null);
INSERT INTO `link_class_teacher` VALUES (2,5);
INSERT INTO `class` VALUES (6,2,'classe archivée',NULL,'2019-01-01 12:00:00','2019-01-01 12:00:00','archive', null, null, null);
INSERT INTO `link_class_teacher` VALUES (2,6);
INSERT INTO `class` VALUES (7,1,'classe institution autre','code_classe7','2019-01-01 12:00:00','2019-01-01 12:00:00','normal', null, null, null);
INSERT INTO `link_class_teacher` VALUES (2,7);
INSERT INTO `class` VALUES (8,2,'classe philosophie UGA','code_classe8','2019-01-01 12:00:00','2019-01-01 12:00:00','normal', null, null, null);
INSERT INTO `link_class_teacher` VALUES (11,8);
INSERT INTO `class` VALUES (9,2,'classe maths UGA','code_classe9','2019-01-01 12:00:00','2019-01-01 12:00:00','normal', null, null, null);
INSERT INTO `link_class_teacher` VALUES (11,9);

-- students have reports
INSERT INTO `team_config` VALUES (1,1,1,3,1,1,1,9,NULL,NULL,1,1,1,NULL,0,1,1,3,'Equipe_',0,'2019-06-13 09:29:22',NULL);
INSERT INTO `report` VALUES (1,1,1,NULL,NULL,'new',0,'Equipe_01',1,1,0,0,0,0,-1,'2019-06-01 09:09:09',NULL, NULL);
INSERT INTO `report` VALUES (2,1,1,NULL,NULL,'on',1,'Equipe_02',1,1,0,0,0,0,-1,'2019-06-01 09:09:09','2019-06-01 09:09:09', NULL);
INSERT INTO `report` VALUES (3,1,1,NULL,NULL,'on',1,'Equipe_a_deux',1,1,0,0,0,0,-1,'2019-06-01 09:09:09','2019-06-01 09:09:09', NULL);
INSERT INTO `report` VALUES (4,1,1,NULL,NULL,'wait',1,'fini',1,1,0,0,0,0,-1,'2019-06-01 09:09:09','2019-06-01 09:09:09', NULL);
INSERT INTO `report` VALUES (5,1,1,NULL,NULL,'on',1,'soft delete',1,1,0,0,0,0,-1,'2019-06-01 09:09:09','2019-06-01 09:09:09', '2021-12-15 11:22:22');
INSERT INTO `link_report_learner` VALUES (1,5,NULL,0);
INSERT INTO `link_report_learner` VALUES (2,4,NULL,0);
INSERT INTO `link_report_learner` VALUES (4,8,NULL,0);
INSERT INTO `link_report_learner` VALUES (5,8,NULL,0);
-- student3 and student4 share a report
INSERT INTO `link_report_learner` VALUES (3,61,NULL,0);
INSERT INTO `link_report_learner` VALUES (3,7,NULL,0);


-- student1 has started working on its report
INSERT INTO `labdoc` VALUES (1,1,2,NULL, NULL,'text','labdoc texte 1',1,1,1,1,1,1,0,NULL,4,1560418774,NULL,'<p>Soit \\( f : \\mathbb{R}\\mapsto\\mathbb{C} \\) une fonction telle que \\[ \\forall x \\in [0,+\\infty[, f(ix)=f(x) \\]</p>','Consigne de labdoc',0,NULL,'2019-06-13 09:35:59');
INSERT INTO `labdoc_status` VALUES (1,4,0,0,0,0,0,1,NULL,NULL);

-- report 3 has some content
INSERT INTO `labdoc` VALUES (2,1,3,NULL,NULL,'text','labdoc texte',1,1,1,1,1,1,0,NULL,7,1560436174,NULL,'<p>Mon texte à moi : $$ \\sum_{0\\leq k\\leq n} \\frac{1}{k^2} $$</p>',NULL,0,NULL,'2019-06-10 10:10:10');
INSERT INTO `labdoc` VALUES (3,1,3,NULL,NULL,'drawing','labdoc dessin',2,1,1,1,1,1,0,NULL,7,1560436192,NULL,'zwibbler3.[{\"id\":0,\"type\":\"GroupNode\",\"fillStyle\":\"#cccccc\",\"strokeStyle\":\"#000000\",\"lineWidth\":2,\"shadow\":false,\"matrix\":[1,0,0,1,0,0],\"layer\":\"default\"},{\"id\":1,\"type\":\"PageNode\",\"parent\":0,\"fillStyle\":\"#cccccc\",\"strokeStyle\":\"#000000\",\"lineWidth\":2,\"shadow\":false,\"matrix\":[1,0,0,1,0,0],\"layer\":\"default\"},{\"id\":3,\"type\":\"PathNode\",\"parent\":1,\"fillStyle\":\"#e0e0e0\",\"strokeStyle\":\"#000000\",\"lineWidth\":2,\"shadow\":false,\"matrix\":[1,0,0,1,171,70],\"layer\":\"default\",\"textFillStyle\":\"#000000\",\"fontName\":\"Arial\",\"fontSize\":14,\"dashes\":\"\",\"shapeWidth\":0,\"smoothness\":0.3,\"sloppiness\":0,\"closed\":true,\"arrowSize\":0,\"arrowStyle\":\"simple\",\"doubleArrow\":false,\"text\":\"\",\"roundRadius\":0,\"wrap\":true,\"angleArcs\":0,\"commands\":[0,0,-50,6,50,0,50,-50,6,0,50,50,50,6,-50,0,-50,50,6,0,-50,-50,-50,7],\"seed\":48809}]',NULL,0,NULL,'2019-06-10 10:10:10');
INSERT INTO `labdoc` VALUES (4,1,3,NULL,NULL,'procedure','labdoc protocole',3,1,1,1,1,1,0,NULL,7,1560436205,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?><LD type=\"protocol\"><metadata><name>Default protocol</name><author></author><lastModification></lastModification></metadata><QHP edit=\"true\" type=\"question\"><label>Question de recherche ou objectif</label><defaultContent>Décrivez l\"objectif de votre expérimentation : la question à laquelle vous voulez répondre et/ou les objets que vous voulez produire.</defaultContent><content>Ma question</content><comment>Mon commentaire</comment></QHP><QHP edit=\"true\" type=\"hypothesis\"><label>Hypothèses ou résultats attendus</label><defaultContent>Lister les hypothèses que vous souhaitez tester au cours de votre expérimentation et/ou les résultats que vous pensez obtenir.</defaultContent><content></content><comment></comment></QHP><QHP edit=\"true\" type=\"principle\"><label>Principe de la manipulation</label><defaultContent>Décrivez rapidement la stratégie, les moyens que vous allez mettre en place. Le principe de manipulation ressemble à un mode opératoire succinct ne contenant pas les paramètres de la manipulation.</defaultContent><content></content><comment></comment></QHP><materialList select=\"true\" add=\"true\" copexGeneralMaterial=\"true\"><label>Liste du matériel</label></materialList><actionList copexGeneralActions=\"true\"><action structured=\"none\"><name>Action libre</name><content/><comment/></action></actionList><procedure><label>Mode opératoire</label><ul xmlns=\"http://www.w3.org/1999/xhtml\"><li rel=\"step\" edit=\"true\" suppr=\"true\" copy=\"true\" move=\"true\" modify=\"true\" iteration=\"1\" parameters=\"1\"><a><content>Étape \"1''</content><comment></comment><table></table></a></li></ul></procedure></LD>',NULL,0,NULL,'2019-06-01 10:10:10');
INSERT INTO `labdoc` VALUES (5,1,3,NULL,NULL,'dataset','labdoc tableur',4,1,1,1,1,1,0,NULL,7,1560436302,NULL,'<?xml version=\"1.0\" encoding=\"UTF-8\"?><LD type=\"dataset\"><metadata><name>default dataset</name><author>fitex</author><lastModification>2016-03-03</lastModification></metadata><constants><constant><name>pi</name><value>3.141593</value></constant></constants><dataset display_formula=\"true\"><header><column type=\"double\"><code>data1</code><name>data1</name><unit/></column><column type=\"double\"><code>data2</code><name>data2</name><unit/></column><column type=\"double\"><code>data3</code><name>data3</name><unit/></column><column type=\"double\"><code>data4</code><name>data4</name><unit/></column></header><row><value>1</value><value>0</value><value/><value/></row><row><value>2</value><value>5</value><value/><value/></row><row><value>2.5</value><value>8</value><value/><value/></row><row><value>2.8</value><value>-3</value><value/><value/></row><row><value/><value/><value/><value/></row></dataset><graph><name>mon graphe</name><xmin/><xmax/><ymin/><ymax/><plot color=\"Red\"><x>data1</x><y>data2</y><ux/><uy/></plot></graph></LD>',NULL,0,NULL,'2019-06-10 10:10:10');
INSERT INTO `labdoc_status` VALUES (2,61,0,1,0,1,1,1,NULL,NULL);
INSERT INTO `labdoc_status` VALUES (2,7,0,0,0,0,0,1,NULL,NULL);
INSERT INTO `labdoc_status` VALUES (3,61,0,1,0,1,1,1,NULL,NULL);
INSERT INTO `labdoc_status` VALUES (3,7,0,0,0,0,0,1,NULL,NULL);
INSERT INTO `labdoc_status` VALUES (4,61,0,1,0,1,1,1,NULL,NULL);
INSERT INTO `labdoc_status` VALUES (4,7,0,0,0,0,0,1,NULL,NULL);
INSERT INTO `labdoc_status` VALUES (5,61,0,1,0,1,1,1,NULL,NULL);
INSERT INTO `labdoc_status` VALUES (5,7,0,0,0,0,0,1,NULL,NULL);

-- Link user 2 to external institution
INSERT INTO `link_extplatform_user` VALUES (3, 45, 2, '2019-06-01 09:09:09','2019-06-01 09:09:09');

-- TeacherTeams 
INSERT INTO `teacher_team` (`id_teacher_team`, `id_inst`, `name`, `status`, `update_time`, `creation_time`) VALUES 
(1, 2, 'Equipe pédago chimie UGA', 'normal', '2021-04-21 10:10:48', '2021-04-21 10:10:48');
INSERT INTO `link_teacher_tteam` (`id_teacher`, `id_teacher_team`, `status`) VALUES
(11, 1, 'member'),
(11, 1, 'manager');

INSERT INTO `teacher_team` (`id_teacher_team`, `id_inst`, `name`, `status`, `update_time`, `creation_time`) VALUES
(2, 2, 'Equipe pédago physique UGA', 'normal', '2021-04-21 10:10:48', '2021-04-21 10:10:48');
INSERT INTO `link_teacher_tteam` (`id_teacher`, `id_teacher_team`, `status`) VALUES
(11, 2, 'member'),
(11, 2, 'manager'),
(2, 2, 'member');                                                                                 

INSERT INTO `teacher_team` (`id_teacher_team`, `id_inst`, `name`, `status`, `update_time`, `creation_time`) VALUES
(3, 1, 'Equipe pédago maths Todai', 'archive', '2021-04-21 10:10:48', '2021-04-21 10:10:48');
INSERT INTO `link_teacher_tteam` (`id_teacher`, `id_teacher_team`, `status`) VALUES
(16, 3, 'member'),
(16, 3, 'manager');


INSERT INTO `teacher_team` (`id_teacher_team`, `id_inst`, `name`, `status`, `update_time`, `creation_time`) VALUES
(4, 2, 'Equipe pédago philosophie UGA', 'normal', '2021-04-21 10:10:48', '2021-04-21 10:10:48');
INSERT INTO `link_teacher_tteam` (`id_teacher`, `id_teacher_team`, `status`) VALUES
(11, 4, 'member'),
(11, 4, 'manager'),
(12, 4, 'member'),
(13, 4, 'member');
INSERT INTO `link_mission_tteam` VALUES (6,4);
INSERT INTO `link_class_tteam` VALUES (8,4);

INSERT INTO `teacher_team` (`id_teacher_team`, `id_inst`, `name`, `status`, `update_time`, `creation_time`) VALUES
(5, 1, 'Equipe pédago archéologie Todai', 'normal', '2021-04-21 10:10:48', '2021-04-21 10:10:48');
INSERT INTO `link_teacher_tteam` (`id_teacher`, `id_teacher_team`, `status`) VALUES
(14, 5, 'member'),
(14, 5, 'manager'),
(15, 5, 'member');

INSERT INTO `teacher_team` (`id_teacher_team`, `id_inst`, `name`, `status`, `update_time`, `creation_time`) VALUES
(6, 1, 'Equipe pédago économie Todai', 'normal', '2021-04-21 10:10:48', '2021-04-21 10:10:48');
INSERT INTO `link_teacher_tteam` (`id_teacher`, `id_teacher_team`, `status`) VALUES
(14, 6, 'member'),
(14, 6, 'manager'),
(15, 6, 'member'),
(16, 6, 'member');

INSERT INTO `teacher_team` (`id_teacher_team`, `id_inst`, `name`, `status`, `update_time`, `creation_time`) VALUES
(7, 1, 'Equipe pédago maths UGA', 'archive', '2021-04-21 10:10:48', '2021-04-21 10:10:48');
INSERT INTO `link_teacher_tteam` (`id_teacher`, `id_teacher_team`, `status`) VALUES
(11, 7, 'member'),
(11, 7, 'manager');

-- Default lang
UPDATE `user` SET `lang` = "fr";

ALTER TABLE report
	CHANGE `send_teacher` `allow_msg_teacher` TINYINT(1) NOT NULL DEFAULT '1',
	CHANGE `send_team` `allow_msg_team` TINYINT(1) NOT NULL DEFAULT '1',
	CHANGE `send_class` `allow_msg_id_class` INT(10) NOT NULL DEFAULT '0',
	CHANGE `send_mission` `allow_msg_id_mission` INT(10) NOT NULL DEFAULT '0',
	CHANGE `send_ld` `allow_attach_ld` TINYINT(1) NOT NULL DEFAULT '0',
	CHANGE `save_ld` `allow_save_ld` TINYINT(1) NOT NULL DEFAULT '0',
	CHANGE `import_ld` `allow_import_id_mission` INT(10) NOT NULL DEFAULT '-1';

INSERT INTO `trace_action` (`id_action`, `action`, `action_type`) VALUES ('53', 'edit_teaming', 't_student');

ALTER TABLE `report` ADD `allow_import` TINYINT NOT NULL DEFAULT '0' AFTER `allow_save_ld`;
ALTER TABLE `report` CHANGE `allow_import_id_mission` `allow_import_id_mission` INT(10) NULL DEFAULT '-1';

UPDATE `report`
SET `allow_import`= 1
WHERE `allow_import_id_mission` > 0;

UPDATE `report`
SET `allow_import`= 1, `allow_import_id_mission` = NULL
WHERE `allow_import_id_mission` = -1;

ALTER TABLE `report` CHANGE `allow_import_id_mission` `allow_import_id_mission` INT(10) UNSIGNED NULL DEFAULT '0';
-- ALTER TABLE `report` ADD CONSTRAINT `report_ibfk_1` FOREIGN KEY (`allow_import_id_mission`) REFERENCES `mission` (`id_mission`) ON DELETE SET NULL ON UPDATE CASCADE;

-- New teaming to test reuse
INSERT INTO `mission` VALUES (8,'mission8','mission 8 vide','<p>La huitième mission est vide</p>','', 0,'private',null,'2019-01-03','2019-01-03');
INSERT INTO `link_mission_teacher` VALUES (8,2,'designer');
INSERT INTO `team_config` VALUES (2,8,1,3,1,1,1,9,NULL,NULL,1,1,1,NULL,0,1,1,3,'Equipe_',0,'2019-06-13 09:29:22',NULL);

INSERT INTO `report` VALUES (6,8,2,NULL,NULL,'new',0,'Equipe_01',1,1,0,0,0,0,1,null,'2019-06-01 09:09:09',NULL, NULL);

INSERT INTO `link_report_learner` VALUES (6,5,NULL,0);
INSERT INTO `link_report_learner` VALUES (6,4,NULL,0);

-- second team config
INSERT INTO `team_config` VALUES (3, 3, 4, 2, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, 0, 0, 0, NULL, 'Equipe_', 0, '2022-05-11 13:26:43', '2022-05-11 13:26:43');
INSERT INTO `report` VALUES	(7,3,3,NULL,NULL,'new',0,'Equipe_01',1,1,0,0,0,0,0,null, '2022-05-11 13:26:43', NULL, NULL);
INSERT INTO `link_report_learner` VALUES (7,8,NULL,0);
INSERT INTO `trace_action` (`id_action`, `action`, `action_type`) VALUES ('54', 'archive_report', 't_report');
INSERT INTO `trace_action` (`id_action`, `action`, `action_type`) VALUES ('55', 'open_collaborative_dashboard', 't_report');
INSERT INTO `trace_action` (`id_action`, `action`, `action_type`) VALUES ('56', 'display_diffs', 't_report');
INSERT INTO `trace_action` (`id_action`, `action`, `action_type`) VALUES ('64', 'freeze_report', 't_report');


-- Update to test tasks

-- labnbook.task definition
DROP TABLE IF EXISTS `task`;
CREATE TABLE `task` (
						`id_task` int(10) unsigned NOT NULL AUTO_INCREMENT,
						`title` varchar(200) NOT NULL,
						`state` enum('inprogress','done') NOT NULL,
						`estimated_duration` int(11) DEFAULT NULL,
						`due_date` date DEFAULT NULL,
						`priority` enum('low','medium','high') DEFAULT NULL,
						`id_parent_task` int(10) unsigned DEFAULT NULL,
						`id_report` int(10) unsigned DEFAULT NULL,
						`position` int(10) unsigned NOT NULL DEFAULT 1,
						`created_at` datetime NOT NULL DEFAULT current_timestamp(),
						`id_user_creator` int(10) unsigned NOT NULL,
						`last_modified_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
						`id_user_last_editor` int(10) unsigned NOT NULL,
						`hasSubtasksUnfolded` tinyint(1) NOT NULL DEFAULT 1,
						PRIMARY KEY (`id_task`),
						KEY `task_creator_constraint` (`id_user_creator`),
						KEY `task_last_editor_constraint` (`id_user_last_editor`),
						CONSTRAINT `id_report_constraint` FOREIGN KEY (`id_report`) REFERENCES `report` (`id_report`) ON DELETE CASCADE ON UPDATE NO ACTION,
						CONSTRAINT `task_creator_constraint` FOREIGN KEY (`id_user_creator`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE NO ACTION,
						CONSTRAINT `task_last_editor_constraint` FOREIGN KEY (`id_user_last_editor`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- labnbook.link_task_user definition
DROP TABLE IF EXISTS `link_task_user`;
CREATE TABLE `link_task_user` (
								  `id_task` int(10) unsigned NOT NULL,
								  `id_user` int(10) unsigned NOT NULL,
								  PRIMARY KEY (`id_task`,`id_user`),
								  KEY `link_task_user_user_constraint` (`id_user`),
								  CONSTRAINT `link_task_user_task_constraint` FOREIGN KEY (`id_task`) REFERENCES `task` (`id_task`) ON DELETE CASCADE,
								  CONSTRAINT `link_task_user_user_constraint` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO task (id_task,title,state,estimated_duration,due_date,priority,id_parent_task,id_report,`position`,created_at,id_user_creator,last_modified_at,id_user_last_editor,hasSubtasksUnfolded) VALUES
	 (293,'Tache 1','inprogress',NULL,NULL,'high',NULL,2,0,'2024-09-12 21:16:53',4,'2024-09-12 21:19:29',4,1),
	 (294,'Tache 2','inprogress',NULL,NULL,'medium',NULL,2,1,'2024-09-12 21:17:08',4,'2024-09-12 21:17:25',4,1),
	 (295,'Tache 3','inprogress',NULL,NULL,'low',NULL,2,2,'2024-09-12 21:17:14',4,'2024-09-12 21:19:34',4,1),
	 (296,'Sous tache 2.1','inprogress',30,'2024-09-05','low',294,2,0,'2024-09-12 21:17:22',4,'2024-09-12 21:19:48',4,1),
	 (297,'Sous tache 2.2','inprogress',NULL,NULL,'medium',294,2,1,'2024-09-12 21:17:30',4,'2024-09-12 21:17:33',4,1);
