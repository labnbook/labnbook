<?php

use App\Trace;

/**
 * @group teaming
 */
class TeamingRandomCest
{
	const ID_CLASS = 1;

	public function testProcess(AcceptanceTester $I) {
		$I->loginAs('teacher1', 'mdp');
		$I->amOnPage("/teacher/students");
		$this->createTeamConfig($I);
		$I->seeILeftTrace(Trace::TEACHER_ADD_TEAMING); // BUg in code
	
		$this->modifyTeamConfig($I);
		// TODO At the time i'm writing this tests, Trace 53 is not available
		$I->seeILeftTrace(Trace::TEACHER_EDIT_TEAMING);
		
		$I->logout();
		$I->seeILeftTrace(Trace::DISCONNECT_LNB);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin('teacher1', 4);
		
		$this->studentOpensReport($I);
		$I->seeILeftTrace(Trace::ENTER_REPORT);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin('student1', 2);
		
		$this->newStudentOpensReport($I);
		$I->seeILeftTrace(Trace::ENTER_REPORT);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin('student6', 2);
	}

	private function createTeamConfig(AcceptanceTester $I) {
		// Todo duplicate code between teamingRandom Student Teacher
		$I->wantTo("create a randomized team config");
		$I->waitForElementVisible("tr#class-" . self::ID_CLASS . " td");
		$I->click(['css' => "tr#class-" . self::ID_CLASS . " td"]);
		$I->click(['css' => '[data-action="openTeamConfig"]']);
		$I->waitForElementVisible("#tc-popup");

		$I->waitForElementVisible('#tc-mission option[value="3"]');
		$I->selectOption("#tc-mission", ['value' => "3"]);
		
		// No panels are visible
		$I->dontSeeElement('#tc-report-options');
		$I->dontSeeElement('#tc-future-students-options');
		$I->dontSeeElement('#tc-students-options');
		
		$I->click(['css' => '#tc-toggle-reports-options']);
		$I->click(['css' => '#tc-toggle-future-students-options']);
		$I->click(['css' => '#tc-toggle-students-options']);
		
		$I->selectOption("#tc-teaming-method", ['text' => "de façon aléatoire"]);
		
		$I->seeCheckboxIsChecked('#tc-email-on-create');
		$I->checkOption("#tc-email-on-create"); // "Prévenir les étudiants par email de la création de l'équipe"
		$I->fillField('#tc-report-prefix', "Eq"); // "Préfixe des noms"
		//$I->fillField("#tc-future-max-team-number", 3);
		$I->fillField("#tc-future-team-size-optimal", 2);
		$I->fillField("#tc-future-team-size-maximal", 3);
		$I->fillField("#tc-future-team-size-minimal", 2);
		$I->click(["css" => "#tc-random-assignment"]);
		
		$I->selectOption('#tc-import-ld', '1'); // "Import de labdocs" := "mission1 - Mission première"
		
		$I->selectOption('[name=tc-msg-teacher]', "0");
		$I->selectOption('#tc-msg-class', "classe 3"); // "aux étudiants de la classe"
		
		$I->seeNumberOfElements(".without-team", 0);
		$I->seeNumberOfElements(".with-team", 5);
		$I->seeNumberOfElements(".tc-reports", 3); //  2 non-empty teams + 1 one empty

		// modify the first team by removing its first user
		$I->moveMouseOver(['css' => '.with-team .tc-user']);
		$I->click(['css' => "button.tc-user-close"]);
		$I->seeNumberOfElements(".without-team", 1);
		$I->seeNumberOfElements(".with-team", 4);

		$I->clickAndAcceptPopup("#tc-popup-submit"); // submit
		$I->click("#tc-popup-submit"); // submit
		$I->waitForText("mission3", 3, ".class-team-missions");
	}

	private function modifyTeamConfig(AcceptanceTester $I) {
		$I->wantTo("modify a randomized team config");
		$I->click("mission3");
		$I->waitForElementVisible("#tc-popup");
		$I->seeOptionIsSelected("#tc-mission", "mission3");
		// TODO Checkbox
		//$I->seeCheckboxIsChecked('[name=email_on_create][value="1"]');
		$I->click("#tc-toggle-future-students-options");
		$I->seeInField("#tc-future-team-size-optimal", "2");
		$I->fillField("#tc-future-team-size-maximal", 2);
		$I->fillField("#tc-future-team-size-minimal", 1);

		$I->seeNumberOfElements(".tc-reports", 3); // 2 non-empty teams + 1 one empty
		$I->seeNumberOfElements(".without-team", 0);
		$I->seeNumberOfElements(".not-draggable", 0); //// student has been assigned a new team

		$I->click(["css" => "#tc-random-assignment"]);

		$I->seeNumberOfElements(".tc-reports", 4); // One student moved in a separate team

		$I->click("#tc-popup-submit");  // submit
	}

	private function studentOpensReport(AcceptanceTester $I) {
		$I->wantTo("test student access to randomized team");
		$friendP = $I->haveFriend('student1');
		$friendP->does(function(AcceptanceTester $I) {
			$I->loginAs('student1', 'mdp');
			$I->waitForText("mission3");
			$I->click("mission3");
			$I->waitForElementVisible("#cadre_mission");
			$I->see("mission 3 vide", '.mission_name');
			$I->click("Commencer");

			$I->seeCurrentUrlMatches('/\/report/');
		});
    }

	private function newStudentOpensReport(AcceptanceTester $I) {
		$I->wantTo("test a new student is added into a random team");
		$friendP = $I->haveFriend('student6');
		$friendP->does(function(AcceptanceTester $I) {
			$I->loginAs('student6', 'mdp');
			$I->dontSee("mission3");

			// add this student to the class
			$I->execSQL("INSERT INTO `link_class_learner` VALUES (1,9)");

			$I->reloadPage();
			$I->waitForElementVisible('#nouvelles');
			$I->click("mission3");
			$I->waitForElementVisible("#cadre_mission");
			$I->see("mission 3 vide", '.mission_name');
			$I->see("Student6", '.team-member');
			$I->click(".lb-section-orange .lb-mission-btn"); // Button can be Commencer or Continuer

			$I->seeCurrentUrlMatches('/\/report/');
		});
    }
}
