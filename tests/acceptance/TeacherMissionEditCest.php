<?php

use App\Trace;

class TeacherMissionEditCest
{
	public function _before(AcceptanceTester $I)
	{
		$I->loginAs('teacher1', 'mdp');
		$I->amOnPage("/teacher/missions");
		$I->dontSeeSpecials();
		$I->seeElement('#mission-3');
	}

	public function renameMission(AcceptanceTester $I)
	{
		$I->click('#my-missions tr[role=row] td');
		$I->click('.fas.fa-pencil-alt');
		$I->see("Paramètres généraux de la mission", '.edit-mission-title');
		$I->click('#at_rep_param .edit-mission-title');
		$I->dontSeeSpecials();

		$I->clearText("#m_name", "");
		$I->typeIn("#m_name", "Text' <b>\"dangereux<script>alert('a')");
		//$I->fillField("#m_name", "Text' <b>\"dangereux<script>alert('a')");
		$I->click('#m_code'); // focus out of m_name to save it
		//$I->executeJS('updateInput("name","nom",3,65);'); // another tentative hack
		//$I->executeJS('$("#m_name").trigger("change");'); // another tentative hack
		$I->click('.on .fa.fa-check');
		$I->dontSee("Paramètres généraux de la mission", '.edit-mission-title');
		$I->see("dangereux", '#my-missions td');
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("teacher1", 2);
	}

	public function AddLabdoc(AcceptanceTester $I)
	{
		$I->typeIn('#private-missions .search-input', "mission1");
		$I->click('#my-missions tr[role=row] td');
		$I->click('.fas.fa-pencil-alt');

		$I->click('#at_rep_struct .edit-mission-title');
		$I->scrollTo('#rp_1');
		$I->click('#rp_text_1');
		$I->fillField("#ld_name_6", "Text1' <b>dang\"<script>alert('a')");
		$I->click('#ld_edit_validate_6');

		// TODO add dangerous content to TinyMCE
		$I->seeElement('#page-alerts');
		$I->see("3 rapport(s) d'étudiants sur 3 ont été mis à jour", "#page-alerts");
		$I->click(".alert-close");

		$I->click('#authoring_menu .on .fa.fa-check');
		$I->dontSee("Paramètres généraux de la mission", '.edit-mission-title');
		$I->see("mission première", 'tr > td');
		$I->seeILeftTrace([Trace::TEACHER_ADD_LABDOC, Trace::TEACHER_MODIFY_MISSION, Trace::TEACHER_MODIFY_MISSION]);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("teacher1", 4);
	}

	public function AddAndModifyReportPart(AcceptanceTester $I)
	{
		$I->typeIn('#private-missions .search-input', "mission1");
		$I->click('#my-missions tr[role=row] td');
		$I->click('.fas.fa-pencil-alt');

		$I->click('#at_rep_struct .edit-mission-title');
		$I->scrollTo('#rp_1');
		$I->click(['css' => '#rp_add .fa.fa-plus']);
		$I->dontSeeSpecials();
		$I->fillField('#rp_edit_title_0', "Text' <b>dang\"<script>alert('a')");
		$I->click('#rp_edit_validate_');
		$I->seeILeftTrace([Trace::TEACHER_ADD_REPORTPART, Trace::TEACHER_MODIFY_MISSION]);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("teacher1", 3);

		$I->waitForElement("#rp_1");
		$I->moveMouseOver('#rp_1 .context_menu');
		$I->click('#rp_1 .menu_edition_item');
		$I->dontSeeSpecials();
		$I->see("Types de labdocs");
		$I->fillField('#rp_edit_title_1', "Text' <b>dang\"<script>alert('a')");
		$I->click('#rp_edit_validate_');
		$I->seeILeftTrace([Trace::TEACHER_MODIFY_REPORTPART, Trace::TEACHER_MODIFY_MISSION]);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("teacher1", 5);
	}

	public function AddLabdocWithInstructions(AcceptanceTester $I)
	{
		$instructions_default_text = "Ajouter une consigne de labdoc";
		$I->typeIn('#private-missions .search-input', "mission1");
		$I->click('#my-missions tr[role=row] td');
		$I->click('.fas.fa-pencil-alt');

		// Add a Text labdoc
		$I->click('#at_rep_struct .edit-mission-title');
		$I->scrollTo('#rp_1');
		$I->click('#rp_text_1');
		// Add some text "bla"
		$I->fillField("#ld_name_6", "bla");

		// Switch to instruction
		$I->see($instructions_default_text, 'button.lb_btn');
		$I->click($instructions_default_text);
		$I->dontSee($instructions_default_text);
		// Do not add instruction
		$I->writeInTinyMceThenSwitchBack("#ld_txt_6_ifr", 'blo');
		// Validate
		$I->click('#ld_edit_validate_6');

		// Switch to a report
		$I->click('#authoring_menu .fa-file');
		$I->switchToNextTab();
		$I->click('#lb_toggle_all_ld');
		$I->click('#lb_toggle_all_ld');
		// See "bla blo"
		// dont see instruction
		$I->see('bla');
		$I->see('blo');
		$I->dontSee('.labdoc_assignment');
		// Open LD edit
		$I->moveMouseOver('.lb_ld_menu_btn i');
		$I->click('tr[title="Modifier le labdoc"]');
		$I->dontSee('.labdoc_assignment');
		$I->click('.labdoc .fa-check');
		// dont see instruction
		// Switch to mission
		$I->switchToNextTab();
		$I->click('#ld_6 .fa-ellipsis-v');
		// Edit LD
		$I->see($instructions_default_text, '.lb_btn');
		// Add labdoc assignment
		$I->writeInTinyMceThenSwitchBack("#ld_txt_6_ifr", 'coucou');
		$I->click($instructions_default_text);
		// Modify content
		$I->writeInTinyMceThenSwitchBack('#ld_txt_6_assignment_ifr', 'consigne');
		// Save
		$I->click('#ld_edit_validate_6');
		// Go back to report
		$I->switchToNextTab();
		// Force sync
		$I->dontSee('.labdoc_assignment');
		$I->moveMouseOver('.lb_ld_menu_btn i');
		$I->click('tr[title="Modifier le labdoc"]');
		// See assignment
		$I->see('consigne', '.labdoc_assignment');
		$I->click('.labdoc_assignment .fa-times');
		$I->dontsee('consigne', '.labdoc_assignment');
		$I->click('.fa-check');
		$I->dontsee('consigne', '.labdoc_assignment');
		$I->click('.lb_show_assignment');
		$I->see('consigne', '.labdoc_assignment');
	}

	public function EveryModificationShouldForceUpdateModificationDate(AcceptanceTester $I)
	{
		$firstRowSelector = 'table.table-missions tr[id^="mission-"]:first-child';
		$iconEdit = '#private-missions-actions a[data-action="edit"]';
		$tableToCheck = 'mission';
		$columnToCheck = 'modif_date';
		$inputMissionName = '#m_name';
		
		$I->waitForElement($firstRowSelector, 3);
		$I->click($firstRowSelector);
		$elementId = $I->grabAttributeFrom($firstRowSelector, 'id');
		$recordId = $this->retrieveMissionIDFromDataAttribute($elementId);
		$requestModificationDateFromDB = $I->grabFromDatabase($tableToCheck, $columnToCheck, ['id_mission' => $recordId]);
		$initialModificationDateRecord = $requestModificationDateFromDB;
		$I->waitForElement($iconEdit, 3);
		$I->click($iconEdit);
		$I->see("Paramètres généraux de la mission", '.edit-mission-title');
		$I->click('#at_rep_param .edit-mission-title');
		self::selectMissionVisibility($I, 'public');
		$I->waitForElement($inputMissionName, 3);
		$I->clearText($inputMissionName);
		$I->fillField($inputMissionName, "Nouveau nom de mission");
		$I->wait(1);
		$I->click('.on .fa.fa-check');
		$newModificationDateRecord = $I->grabFromDatabase($tableToCheck, $columnToCheck, ['id_mission' => $recordId]);
		$I->assertNotEquals($initialModificationDateRecord, $newModificationDateRecord, 'La colonne "modif_date" a bien été mise à jour.');
	}

	private function retrieveMissionIDFromDataAttribute(string $dataAttribute): int
	{
		if (preg_match('/mission-(\d+)/', $dataAttribute, $matches)) {
			return (int) $matches[1];
		}

		return 0;
	}

	private function selectMissionVisibility(AcceptanceTester $I, string $value): void
	{
		$selectorRef = '#m_status';
		$I->waitForElement($selectorRef, 2);
		$I->selectOption($selectorRef, ['value' => $value]);
		$I->seeOptionIsSelected($selectorRef, $value);
	}
}
