<?php

use App\Trace;

class ReportCest
{

	private StructureSelectors $structure;
	private int $latestLadocId;

	public function toggleLabdoc(AcceptanceTester $I)
	{
		$labdocId = 1;
		$I->wantTo("toggle the visibility of a labdoc");
		$I->loginAs('student1', 'mdp');
		$I->see("mission1", ".menu_mission_item");
		$I->click("#encours .menu_mission_item a:first-child"); // cannot click() on text, because the HTML is misleading
		$I->waitForElementVisible("#btn_form_submit");
		$I->click("Continuer");
		$I->waitForElement('#report_header');
		$I->seeILeftTrace(Trace::ENTER_REPORT);
		$I->dontSeeSpecials();
		$I->see("Mission première", '#lb_mission_name');
		$I->see("Global");
		$I->see("une fonction telle que", "#labdoc_$labdocId");
		$I->click("#labdoc_$labdocId .lb_show_hide_ld");
		$I->dontSee("une fonction telle que", "#labdoc_$labdocId");
		$I->click("#labdoc_$labdocId .lb_show_hide_ld");
		$I->see("une fonction telle que", "#labdoc_$labdocId");
		$I->seeILeftGoodNumberOfTracesSinceLastLogin('student1', 2);
		$I->click('#change-report-btn a');
		// TODO this one is not working. Must be the Chromium browser the coupable
		//$I->seeILeftTrace(Trace::LEAVE_REPORT);
		$I->logout(); // logout

	}

	public function testReportPart(AcceptanceTester $I)
	{
		$I->loginAs('student1', 'mdp');
		$I->click("#encours .menu_mission_item"); // cannot click() on text, because the HTML is misleading
		$I->waitForElementVisible("#btn_form_submit");
		$I->click("Continuer");
		$I->seeILeftTrace(Trace::ENTER_REPORT);
		$I->waitForText("Tout doit s'inscrire ci-dessous.");
		$I->click(".lb_show_assignment");
		$I->dontSee("Tout doit s'inscrire ci-dessous.");
		$I->seeILeftGoodNumberOfTracesSinceLastLogin('student1', 2);
		$I->logout(); // logout

	}

	// TODO improve this test with opening real ressource (file not url) to fire OPEN_RESOURCE
	public function testResourceWidget(AcceptanceTester $I)
	{
		$I->wantTo("toggle the 'Ressource' widget");
		$I->loginAs('student2', 'mdp');
		$I->click(".menu_mission_item"); // cannot click() on text, because the HTML is misleading
		$I->waitForElementVisible("#btn_form_submit");
		$I->click("Commencer");
		$I->seeCurrentUrlMatches('#/report/#');
		$I->seeILeftTrace(Trace::ENTER_REPORT);
		$I->dontSeeSpecials();
		$I->click('#lb_menubar_ressource .lnb-widget-menu-btn');
		$I->seeElement('#ressource');
		$I->see("Consigne détaillée", '#ressource a');
		$I->see("Ajouter un document (lien, pdf ou image)");

		$I->seeILeftGoodNumberOfTracesSinceLastLogin('student2', 2);
		$I->logout(); // logout
	}

	public function labdocLifecycle(AcceptanceTester $I)
	{
		$structure = $this->retrieveStructureElement(false);
		
		$I->wantTo("Test a labdoc lifecycle");
		$I->loginAs('student2', 'mdp');
		$I->click(".menu_mission_item");
		$I->waitForElementVisible("#btn_form_submit");
		$I->click("Commencer");
		$I->seeCurrentUrlMatches('#/report/#');
		$I->seeILeftTrace(Trace::ENTER_REPORT);
		$I->seeNumberOfElements($structure['labdoc'], 0);

		$I->comment("Add LD");
		$this->actionAddALabdoc($I);
		
		$I->comment("Duplicate LD");
		$this->actionDuplicateALabdoc($I, expectedLabdocCountAfterProcessing: 2);
		
		$I->comment("Draft a LD (hide/unhide to teacher)");
		// Arrange
		$I->moveMouseOver("#ld_name_actions_{$this->latestLadocId} .lb_ld_menu_btn i");
		$I->clickAndAcceptPopup("#ld_name_actions_{$this->latestLadocId} tr[title=\"Cacher le labdoc à votre enseignant\"]");
		$I->seeILeftTrace(Trace::TOGGLE_LD_STATUS);
		
		// TODO Check if Teacher cant view anything
		$I->moveMouseOver("#ld_name_actions_{$this->latestLadocId} .lb_ld_menu_btn i");
		$I->click("#ld_name_actions_{$this->latestLadocId} tr[title=\"Montrer le labdoc à votre enseignant\"]");
		$I->seeILeftTrace(Trace::TOGGLE_LD_STATUS);

		$I->comment("Delete LD");
		$this->actionDeleteALabdoc($I);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("student2", 11);
		$I->logout(); // logout
	}

	/**
	 * Retrieves a report part ID by his position on structure
	 *
	 * @param AcceptanceTester $I
	 * @param int $reportPartPosition
	 * @param bool $isTeacher
	 * @return mixed|string
	 */
	private function retrieveReportIdPartByPosition(AcceptanceTester $I, int $reportPartPosition = 1, bool $isTeacher = false)
	{
		$structure = $this->retrieveStructureElement($isTeacher);
		$expectedReportPart = $structure['report_part'] . ":nth-of-type(" . $reportPartPosition . ")";
		$I->waitForElementVisible($expectedReportPart);
		$reportPartIDAttribute = $I->grabAttributeFrom($expectedReportPart, $structure['data_id_report_part']);
		if (!$reportPartIDAttribute) {
			return $I->fail("Report part " . $reportPartPosition . " not found");
		}

		return $reportPartIDAttribute;
	}

	/**
	 * Retrieves the labdoc ID by his position on report part list
	 *
	 * @param AcceptanceTester $I
	 * @param int $reportPartID
	 * @param int $labdocPosition
	 * @param bool $isTeacher
	 * @return mixed|string
	 */
	private function retrieveLabdocIdByPosition(AcceptanceTester $I, int $reportPartID = 1, int $labdocPosition = 1, bool $isTeacher = false): mixed
	{
		$structure = $this->retrieveStructureElement($isTeacher);
		$expectedLabdoc = $structure['report_part_id'] . $reportPartID . " " . $structure['labdoc'] . ":nth-of-type(" . $labdocPosition . ")";
		$I->waitForElementVisible($expectedLabdoc);
		$labdocIdAttribute = $I->grabAttributeFrom($expectedLabdoc, $structure['data_id_labdoc']);

		if (!$labdocIdAttribute) {
			return $I->fail("Labdoc at position " . $labdocPosition . " on report part " . $reportPartID . " not found");
		}

		return $labdocIdAttribute;
	}

	/**
	 * Retrieves the shared structure for elements
	 * 
	 * @param bool $isTeacher
	 * @return string[]
	 */
	private function retrieveStructureElement(bool $isTeacher = false): array
	{
		return [
			'report_part_id'      => $isTeacher ? "#rp_" : "#report_part_",
			'report_part'         => $isTeacher ? "#rp_list .lba_rp" : "#lb_report .lb_report_part",
			'labdoc_id'           => $isTeacher ? "#ld_" : "#labdoc_",
			'labdoc'              => $isTeacher ? ".ld_item_draggable_report" : ".labdoc",
			'data_id_report_part' => $isTeacher ? "data-id-rp" : "data-id_rp",
			'data_id_labdoc'      => $isTeacher ? "data-id-ld" : "data-id_ld",
			'validate_btn'        => $isTeacher ? ".lba_line .edit_menu .fa-check" : "div[title='Enregistrer une nouvelle version du labdoc'] i",
		];
	}

	/**
	 * Adds a labdoc
	 *
	 * @param AcceptanceTester $I
	 * @param int $reportPartPosition
	 * @param int $labdocPosition
	 * @param string $title
	 * @param string $content
	 * @param bool $isTeacher
	 * @return void
	 */
	private function actionAddALabdoc(
		AcceptanceTester $I,
		int              $reportPartPosition = 1,
		int              $labdocPosition = 1,
		string           $title = "Mon labdoc",
		string           $content = "<p>Some Text Here!</p>",
		bool             $isTeacher = false): void
	{
		$structure = $this->retrieveStructureElement($isTeacher);
		$reportPartID = $this->retrieveReportIdPartByPosition($I, reportPartPosition: $reportPartPosition, isTeacher: $isTeacher);
		$reportPartDOMSelector = $structure['report_part_id'];
		$labdocClassname = $structure['labdoc'];
		$labdocPartDOMSelector = $reportPartDOMSelector . $reportPartID . " " . $labdocClassname;

		$I->click('img[title="Ajouter un labdoc texte"]', $reportPartDOMSelector . $reportPartID);
		$I->waitForElement($labdocPartDOMSelector . ":nth-of-type(" . $labdocPosition . ")");

		$labdocID = $this->retrieveLabdocIdByPosition($I, $reportPartPosition, $labdocPosition, isTeacher: $isTeacher);

		if ($isTeacher) {
			$I->seeILeftTrace([Trace::TEACHER_ADD_LABDOC, Trace::TEACHER_MODIFY_MISSION]);
		} else {
			$I->seeILeftTrace(Trace::ADD_LD);
		}

		$I->seeNumberOfElements($labdocPartDOMSelector, $labdocPosition);
		$I->waitForElement("#ld_name_" . $labdocID);
		$I->fillField("#ld_name_" . $labdocID, $title);
		$I->waitForElementVisible("#ld_txt_" . $labdocID . "_ifr");
		$I->switchToIFrame("#ld_txt_" . $labdocID . "_ifr");
		$I->executeJS('document.getElementById("tinymce").innerHTML = "' . $content . '";');
		$I->switchToIFrame();
		$I->click($structure['validate_btn'], $reportPartDOMSelector . $reportPartID);
		$I->dontSee("Paragraphe");

		if (!$isTeacher) {
			$I->seeILeftTrace([Trace::MODIFY_LD, Trace::UPDATE_LD_NAME, Trace::VALIDATE_LD]);
		}
	}

	/**
	 * Edits an existing labdoc
	 *
	 * @param AcceptanceTester $I
	 * @param int $reportPartPosition
	 * @param int $labdocPosition
	 * @param string $text
	 * @param bool $isTeacher
	 * @return void
	 */
	private function actionEditALabdoc(
		AcceptanceTester $I,
		int              $reportPartPosition = 1,
		int              $labdocPosition = 2,
		string           $text = "ajout de texte",
		bool             $isTeacher = false): void
	{
		$I->comment("Edit an existing labdoc");
		$reportPartID = $this->retrieveReportIdPartByPosition($I, $reportPartPosition);
		$labdocID = $this->retrieveLabdocIdByPosition($I, $reportPartID, $labdocPosition);
		$structure = $this->retrieveStructureElement($isTeacher);
		$labdocExpected = $structure['labdoc_id'] . $labdocID;
		
		$I->seeElement($labdocExpected);
		$I->seeElement($labdocExpected . ' .lb_ld_menu_btn');
		$I->moveMouseOver($labdocExpected . ' .lb_ld_menu_btn');
		$I->click('.lb_ld_menu .fa-pencil-alt', $labdocExpected);
		$I->writeInTinyMceThenSwitchBack("#ld_txt_" . $labdocID . "_ifr", $text);
		// Validate
		$I->waitForElement('div[title="Enregistrer une nouvelle version du labdoc"] i');
		$I->click('div[title="Enregistrer une nouvelle version du labdoc"] i');
	}

	/**
	 * Shows versions history of a labdoc
	 *
	 * @param AcceptanceTester $I
	 * @param int $labdocID
	 * @param bool $isTeacher
	 * @return void
	 */
	private function actionShowHistory(AcceptanceTester $I, int $labdocID = 6, bool $isTeacher = false): void
	{
		$structure = $this->retrieveStructureElement($isTeacher);
		$expectedLabdoc = $structure['labdoc_id'] . $labdocID;
		$I->seeElement($expectedLabdoc);
		$I->seeElement($expectedLabdoc . ' .lb_ld_menu_btn');
		$I->moveMouseOver($expectedLabdoc . ' .lb_ld_menu_btn');
		$I->click('.lb_ld_menu .fa-history', $expectedLabdoc);
		$I->waitForElementVisible($expectedLabdoc . ' #ld_versions');
	}

	/**
	 * Duplicates an exiting labdoc
	 *
	 * @param AcceptanceTester $I
	 * @param int $reportPartPosition
	 * @param int $labdocPosition
	 * @param bool $isTeacher
	 * @param int|null $expectedLabdocCountAfterProcessing
	 * @return void
	 */
	private function actionDuplicateALabdoc(AcceptanceTester $I, int $reportPartPosition = 1, int $labdocPosition = 1,  bool $isTeacher = false, ?int $expectedLabdocCountAfterProcessing = null): void
	{
		// Arrange
		$structure = $this->retrieveStructureElement($isTeacher);
		$reportPartID = $this->retrieveReportIdPartByPosition($I, $reportPartPosition);
		$expectedReportPart = $structure['report_part_id'] . $reportPartID;
		$labdocID = $this->retrieveLabdocIdByPosition($I, $reportPartID, $labdocPosition);
		$labdocReference = $structure['labdoc_id'] . $labdocID;
		
		$I->comment("Duplicate a Labdoc");
		// Act
		$I->moveMouseOver($labdocReference . ' .lb_ld_menu_btn i');
		$I->click('tr[title="Dupliquer le labdoc"]', $labdocReference);
		$I->moveMouseOver($labdocReference);
		
		$lastLabdocElement = $expectedReportPart . " " .  $structure['labdoc'].":last-child";
		$labdocIDAttribute = $I->grabAttributeFrom($lastLabdocElement, $structure['data_id_labdoc']);
		$this->latestLadocId = $labdocIDAttribute;
		$labdocExpected = $structure['labdoc_id'] . $labdocIDAttribute;
		
		if (!$labdocIDAttribute) {
			$I->fail("Labdoc " . $labdocIDAttribute . " not found");
			return;
		}
		// Assert
		$I->waitForElement($labdocExpected);
		$I->see("Paragraphe");
		$I->click('div[title="Enregistrer une nouvelle version du labdoc"] i', $labdocExpected);
		$I->see("Mon labdoc - copie(1)", $labdocExpected);
		
		if ($expectedLabdocCountAfterProcessing) {
			$I->seeNumberOfElements($expectedReportPart . " " . $structure['labdoc'], $expectedLabdocCountAfterProcessing);
		}
		
		$I->seeILeftTrace([Trace::DUPLICATE_LD, Trace::VALIDATE_LD]);
	}

	/**
	 * Deletes an existing labdoc
	 *
	 * @param AcceptanceTester $I
	 * @param int $labdocID
	 * @param bool $isTeacher
	 * @return void
	 */
	private function actionDeleteALabdoc(AcceptanceTester $I, int $labdocID = 6, bool $isTeacher = false)
	{
		$structure = $this->retrieveStructureElement($isTeacher);
		$labdocExpected = $structure['labdoc_id'] . $labdocID;
		$I->seeElement($labdocExpected);
		$I->moveMouseOver($labdocExpected . ' .lb_ld_menu_btn i');
		$I->click('tr[title="Supprimer le labdoc"]');
		$I->seeNumberOfElements($structure['labdoc'], 1);
		$I->seeILeftTrace(Trace::DELETE_LD);
	}

	/**
	 * Browses labdoc history versions and ensures not contains version with empty text
	 * 
	 * @param AcceptanceTester $I
	 * @param mixed $labdocID
	 * @param string $labdocExpected
	 * @param bool $isTeacher
	 * @return void
	 */
	private function actionLabdocHistoryVersions(AcceptanceTester $I, int $labdocID, string $labdocExpected, bool $isTeacher = false): void
	{
		$I->comment("Verify that history version not contains version empty");
		$this->actionShowHistory($I, $labdocID, $isTeacher);
		$I->waitForElementVisible($labdocExpected . ' #ld_versions li');

		$I->comment("Scroll into versions list to ensure no empty text found");
		$versions = $I->grabMultiple($labdocExpected . " #ld_versions > li", 'text');

		if (empty($versions)) {
			$I->fail('Empty versions list for labdoc' . $labdocID);
		}
		
		foreach ($versions as $index => $version) {
			$versionElement = $labdocExpected . " #ld_versions li:nth-child(" . $index + 1 . ")";
			$versionContentElement = $labdocExpected . " .ld_versions_content";
			$I->waitForElement($versionElement);
			$I->waitForElement($versionContentElement);
			$I->click($versionElement);
			$versionContent = $I->grabTextFrom($versionContentElement);
			$I->assertStringNotContainsString("Le texte est vide, cliquez sur modifier pour ajouter du texte", $versionContent);
		}
	}

	/**
	 * Ensures versioning changes on list are good
	 *
	 * @param AcceptanceTester $I
	 * @return void
	 */
	public function labdocWithVersioningChangesList(AcceptanceTester $I)
	{
		$I->comment("List labdoc history versions");

		$student = $I->haveFriend("student");
		$student->does(function (AcceptanceTester $I) {
			// Arrange
			$structure = $this->retrieveStructureElement(false);
			$I->loginAs('student2', 'mdp');
			$I->click(".menu_mission_item");
			$I->waitForElementVisible("#btn_form_submit");
			$I->click("Commencer");
			$I->seeCurrentUrlMatches('#/report/#');
			$I->seeILeftTrace(Trace::ENTER_REPORT);
			$I->seeNumberOfElements($structure['labdoc'], 0);
			
			$this->actionAddALabdoc($I);
			$this->actionAddALabdoc($I, 1, 2, "Labdoc numéro 2", "<p>Contenu spécifique</p>");
			$this->actionAddALabdoc($I, 1, 3, "Labdoc numéro 3", "<p>Nouveau contenu</p>");
			$this->actionEditALabdoc($I, 1, 2, ' modifications version 2', false);
			$this->actionEditALabdoc($I, 1, 2, ' et encore des ajustements pour une version 3', false);

			$labdocID = $this->retrieveLabdocIdByPosition($I, 1, 2);
			$labdocExpected = $structure['labdoc_id'] . $labdocID;

			$this->actionLabdocHistoryVersions($I, $labdocID, $labdocExpected, false);
			$I->logout(); // logout
		});
	}

	/**
	 * Ensures versioning changes on original labdoc created by teacher are good
	 *
	 * @param AcceptanceTester $I
	 * @return void
	 */
	public function labdocVersioningWithOriginalVersion(AcceptanceTester $I)
	{
		$teacher = $I->haveFriend("teacher");
		$teacher->does(function (AcceptanceTester $I) {
			$structure = $this->retrieveStructureElement(true);
			$reportPartID = 1;
			$expectedReportPart = $structure['report_part_id'] . $reportPartID;
			$I->loginAs('teacher1', 'mdp');
			// Act
			$I->amOnPage("/teacher/missions");
			$I->waitForElementVisible("#private-missions .search-input");

			$I->typeIn('#private-missions .search-input', "mission1");
			$I->waitForElementVisible('#my-missions tr#mission-1');
			$I->click('#my-missions tr#mission-1 td');
			$I->waitForElement('#private-missions-actions .fas.fa-pencil-alt');
			$I->click('.fas.fa-pencil-alt');

			$I->waitForElementVisible('#at_rep_struct .edit-mission-title');
			$I->click('#at_rep_struct .edit-mission-title');
			$I->scrollTo($expectedReportPart);

			$this->actionAddALabdoc(
				$I,
				reportPartPosition: 1,
				title: "Labdoc par défaut de l'enseignant",
				content: "<p>Contenu intial du labdoc produit par l'enseignant</p>",
				isTeacher: true);

			$I->logout(); // logout
		});

		$student = $I->haveFriend("student");
		$student->does(function (AcceptanceTester $I) {
			$I->loginAs('student2', 'mdp');
			$structure = $this->retrieveStructureElement(false);
			$I->click(".menu_mission_item");
			$I->waitForElementVisible("#btn_form_submit");
			$I->click("Commencer");
			$I->seeCurrentUrlMatches('#/report/#');
			$I->seeILeftTrace(Trace::ENTER_REPORT);
			$I->seeNumberOfElements($structure['labdoc'], 1);
			$labdocID = $this->retrieveLabdocIdByPosition($I, 1, 1);
			$labdocExpected = $structure['labdoc_id'] . $labdocID;
			$I->waitForElementVisible($labdocExpected);
			$this->actionEditALabdoc($I, 1, 1, " modifié par l'étudiant", false);
			$this->actionEditALabdoc($I, 1, 1, " et à nouveau complété", false);

			$this->actionLabdocHistoryVersions($I, $labdocID, $labdocExpected, false);
			$I->see("version initiale du labdoc", $labdocExpected);

			$I->logout(); // logout
		});
	}
	
	public function labdocVersioningDuplicated(AcceptanceTester $I) {
		$I->wantTo("List labdoc history versions after duplication");

		// Arrange
		$structure = $this->retrieveStructureElement(false);
		$I->loginAs('student2', 'mdp');
		$I->click(".menu_mission_item");
		$I->waitForElementVisible("#btn_form_submit");
		$I->click("Commencer");
		$I->seeCurrentUrlMatches('#/report/#');
		$I->seeILeftTrace(Trace::ENTER_REPORT);
		$I->seeNumberOfElements($structure['labdoc'], 0);

		$this->actionAddALabdoc($I);
		$this->actionEditALabdoc($I, 1, 1, ' modifications version 2', false);
		$this->actionEditALabdoc($I, 1, 1, ' et encore des ajustements pour une version 3', false);
		
		$this->actionDuplicateALabdoc($I, 1, 1);
		$labdocID = $this->retrieveLabdocIdByPosition($I, 1, 2);
		$labdocExpected = $structure['labdoc_id'] . $labdocID;

		$this->actionLabdocHistoryVersions($I, $labdocID, $labdocExpected, false);
		$I->logout(); // logout
	}
	
	public function studentsOnlyShouldSeeHelpButton(AcceptanceTester $I)
	{
		$I->wantTo("As a student with default language as FRENCH, I should see a help button on reports page header menu");
		// Arrange
		$helpButtonElement = "#header-content #lb_btn_help";
		$studentResourceURLFrench = "https://labnbook.fr/ressources-etudiants/";
		$studentResourceURLEnglish = "https://labnbook.fr/en/student-resources/";
		$I->loginAs('student1', 'mdp');
		$I->seeInCurrentUrl('/reports');
		// Act
		$I->waitForElementVisible($helpButtonElement);
		// Assert
		$I->seeLink("Aide", $studentResourceURLFrench);
		$I->click($helpButtonElement);
		$I->switchToNextTab();
		$I->seeInCurrentUrl("/ressources-etudiants");
		$I->switchToPreviousTab();
		
		$I->wantTo("As a student with default language as ENGLISH, I should see a help button on reports page header menu with dedicated link");
		$studentHasEnglishLang = $I->haveFriend('studentHasEnglishLang');
		$studentHasEnglishLang->does(function(AcceptanceTester $I) use ($studentResourceURLEnglish, $studentResourceURLFrench, $helpButtonElement) {
			$studentLogin = 'student6';
			$I->updateInDatabase('user', ['lang' => 'en'], ['login' => $studentLogin]);
			$I->seeInDatabase('user', ['login' => $studentLogin, 'lang' => 'en']);
			$I->loginAs($studentLogin, 'mdp');
			$I->seeInCurrentUrl('/reports');
			// Act
			$I->waitForElementVisible($helpButtonElement);
			// Assert
			$I->dontSeeLink("Aide", $studentResourceURLFrench);
			$I->seeLink("Help", $studentResourceURLEnglish);
			$I->click($helpButtonElement);
			$I->switchToNextTab();
			$I->seeInCurrentUrl("/en/student-resources");
			$I->switchToPreviousTab();
		});
		
		$teacher = $I->haveFriend('teacher');
		$teacher->does(function(AcceptanceTester $I) use ($studentResourceURLEnglish, $studentResourceURLFrench) {
			$I->wantTo("As a teacher I should not see a help button on reports page header menu");
			// Arrange
			$I->loginAs('teacher1', 'mdp');
			$I->amOnPage('/reports');
			// Assert
			$I->seeInCurrentUrl('/reports');
			$I->dontSeeLink("Aide", $studentResourceURLFrench);
			$I->dontSeeLink("Help", $studentResourceURLEnglish);
		});
	}
}
