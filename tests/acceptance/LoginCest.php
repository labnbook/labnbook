<?php

class LoginCest
{
	public function testPasswordLogin(AcceptanceTester $I)
    {
		$I->wantTo("Log in with a password");
		$I->amOnPage('/');
		$I->dontSeeSpecials();
		$I->loginAs("admin", "mdp");
		$I->dontSee("Créer ses rapports scientifiques en ligne");
		$I->dontSeeSpecials();
		$I->seeElement("#page-missions");
    }

	public function testCasLogin(AcceptanceTester $I)
    {
		$I->wantTo("Log in with CAS");
		$I->amOnPage('/login');
		$I->click("Connexion CAS-AGALAN");
		// fake CAS is configured by: CAS_MASQUERADE=user-CAS
		// Switch to page reports
		$I->amOnPage('/reports');
		$I->waitForElementVisible("#header-content");
		$I->see("Mon compte", '#header');
		$I->see("Bienvenue Masako", 'h2');

        $I->logout(true);
		$I->dontSeeSpecials();
        $I->seeElement('.alert-info');
        $I->see("Vous êtes déconnecté de AGALAN", '.alert-info');
		$I->dontSeeSpecials();
	}

	private function loginUnknownTeacherCas(AcceptanceTester $I, $keepHomonymes)
	{
		$I->wantTo("Log in with CAS, unknown teacher");
		// remove the CAS user from the DB, so that LnB creates a new account
		$data = ['pwd' => 'f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17'];
		if (!$keepHomonymes) {
			$data['email'] = 'x@b.org';
			$data['login'] = 'anything';
			$data['user_name'] = 'plop';
			$data['first_name'] = 'ploum';
		}
		$I->updateInDatabase('user', $data, ['login' => 'user-CAS']);

        $I->amOnPage('/login');
        $I->click("Connexion CAS-AGALAN");
	}

	private function seeHomonymesPage(AcceptanceTester $I)
	{
		$I->waitForText("Créer un accès LabNBook avec votre compte institutionnel");
        $I->seeCurrentUrlMatches('#/auth/cas/create-teacher$#');
		$I->waitForText("Nous avons détecté des comptes LabNBook qui pourraient vous appartenir");
	}
	

	/* -------------------------- CAS Teacher -------------------------------*/
	private function seeHomonymesPageCAS(AcceptanceTester $I)
	{
		$this->seeHomonymesPage($I);
		$I->see('Masako CAS user (compte user-CAS)');
		$I->see('Ne pas reprendre de compte ci-dessus');
	}

	private function seeNewTeacherCasAccountCreated(AcceptanceTester $I)
	{
		$I->see("Vous avez été inscrit à LabNBook en tant qu'enseignant avec votre compte institutionnel", '.alert-success');
        $I->seeCurrentUrlMatches('#/teacher/missions$#');
	}

	public function testUnknownTeacherCasLoginWithoutHomonymes(AcceptanceTester $I)
	{
		$this->loginUnknownTeacherCas($I, false);
		$this->seeNewTeacherCasAccountCreated($I);
	}

	public function testUnknownTeacherCasLoginWithHomonymesCreate(AcceptanceTester $I) 
	{
		$this->loginUnknownTeacherCas($I, true);
		$this->seeHomonymesPageCAS($I);
		$I->click('Créer l\'accès');
		$I->waitForText('Veuillez selectionner une option', 3, '.alert-danger');
		$I->click("input.account-checkbox[value=none]");
		$I->click('Créer l\'accès');
		$this->seeNewTeacherCasAccountCreated($I);
	}

	public function testUnknownTeacherCasLoginWithHomonymesConnect(AcceptanceTester $I)
    {
		$this->loginUnknownTeacherCas($I, true);
		$this->seeHomonymesPageCAS($I);
		$I->click("input.account-checkbox[value=user-CAS]");
		$I->seeElement('input[name="password"]');
		$I->fillField('input[name="password"]', 'mdp');
		$I->click('Créer l\'accès');
		$I->see("Vos informations personnelles ont été mises à jour depuis l'annuaire", '.alert-info');
		$I->see("La connexion à votre compte LabNBook se fera désormais par l'annuaire institutionnel", '.alert-info');
		$I->dontSeeSpecials();
	}
}
