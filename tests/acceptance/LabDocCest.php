<?php

use App\Trace;

class LabDocCest {
	/* TODO Tests traces 
	    - recover
	    - open ld versioning 
	    - hard Delete
	Some tests for labdoc are in report and can be moved here
	*/

	public function comment (AcceptanceTester $I){
		$I->wantTo("Leave a comment");
		$I->loginAs('student1', 'mdp');

		$I->click("#encours > .menu_mission_item"); // cannot click() on text, because the HTML is misleading
		$I->waitForElementVisible('#cadre_mission');
		$I->click("Continuer");
		$I->seeCurrentUrlMatches('#/report/#');
		$I->seeILeftTrace(Trace::ENTER_REPORT);
		$I->moveMouseOver(['css' => '#ld_name_actions_1 .ld_name_actions_header']);
		$I->click("#comment_icon_1 .icon-with-badge .fa-layers");
		$I->assertEquals(1, $I->ExecuteJS('return Alpine.store("commentWidgetIdLd")'));
		$I->fillField("#text_comment_send", "LabNBook c'est super");
		$I->click("i[title='Envoyer le commentaire']");
		$I->see("Moi : LabNBook c'est super");
	}
}
