<?php

use App\Trace;

class AdminCest
{
	CONST NUM_INSTITUTIONS = 2;
	CONST NUM_USERS_INST = [
		1 => 4,
		2 => 14,
		3 => 1, // Aucune donnée
	];
	const ROLESNAME = [
		'teacher' => 'enseignant',
		'learner' => 'étudiant',
		'manager' => 'gestionnaire',
		'admin' => 'admin'
	];

	private function waitSelfTest(AcceptanceTester $I) {
		$I->waitForElementNotVisible('#adminstatus i.wait', 3);
	}

	private function selectInstiution($I, $id_inst, $added=0) {
		$I->click('#institution-'.$id_inst);
		$I->waitForElementVisible('#u-table');
		$I->seeNumberOfElements("#u-table tbody tr", self::NUM_USERS_INST[$id_inst] + $added);
	}

	public function _before(AcceptanceTester $I) {
		$I->loginAs('admin', 'mdp');
		$I->amOnPage("/admin");
		$I->dontSeeSpecials();
		$this->waitSelfTest($I);
	}

	public function selfTest(AcceptanceTester $I) {
		// Missing teacher class for Todai
		$I->see("Il y a 1 institutions sans classes enseignant.", "li.err");
		// Migration usually not run on test DB
		$I->see("Des migrations n'ont pas été executées, veuillez executer la commande suivante :", "li.err");
		// User without classes
		$I->see("Il y a 13 utilisateurs qui ne sont membre d'aucune classe.", "li.err");
		// Storage is OK
		$I->see("Le dossier", "li.warn");
		$I->dontSee("Dossier", "li.err");
		// No CAS errors
		$I->dontSee("Le CAS", "li.err");
		$I->dontSee("Plus d'une institution", "li.err");
		// No DB config error
		$I->dontSee("Date PHP", "li.err");
		// Extensions
		$I->dontSee("Extension php manquante(s) ", "li.err");
		// Node
		$I->dontSee("Version node trop ancienne", "li.err");
		$I->seeILeftGoodNumberOfTracesSinceLastLogin('admin', 1);
		// Max upload not configured properly in test mode
		$I->see('upload_max_filesize', 'li.warn');
	}

	public function institutions(AcceptanceTester $I) {
		$I->dontSee('#u-table');
		$I->seeNumberOfElements("#i-table tbody tr", self::NUM_INSTITUTIONS);
		$this->selectInstiution($I, 2);
		$this->selectInstiution($I, 1);
		$I->comment("Add teacher class for todai");
		$I->click('#i-edit');
		$I->waitForElementVisible('#in-edit-cas');
		$I->click('#in-edit-cas');
		$I->selectOption('#in-edit-class', 7);
		$I->click('#modal-inst-edit .popup-submit');
		$this->waitSelfTest($I);
		$I->see("Plus d'une institution", 'li.err');
		$I->click('#i-edit');
		$I->waitForElementVisible('#in-edit-cas');
		$I->click('#in-edit-cas');
		$I->click('#modal-inst-edit .popup-submit');
		$this->waitSelfTest($I);
		$I->dontSee("Plus d'une institution", 'li.err');

		$I->comment("Add a teacher for Todai");
		$I->click('#add-user-btn');
		$I->waitForElementVisible('#t-add-first-name');
		$I->fillField('#t-add-first-name', 'first');
		$I->fillField('#t-add-name', 'last');
		$I->fillField('#t-add-email', 'first.last@example.org');
		$I->click('#t-add-send-email');
		$I->seeInField('#t-add-login', 'lastf');
		$I->click('#t-popup-valid');
		$I->waitForText("L'utilisateur a bien été ajouté en tant qu'enseignant.");
		$I->click("#institution-2");
		$this->selectInstiution($I, 1, 1);
		// TODO finish here fix css selector
		$I->click('#u-table>tbody>tr:nth-child(2)');
		$I->click("#u-delete");
		$I->waitForElementVisible("#confirmation-popup");;
		$I->see("vous confirmez la suppression définitive de l'utilisateur lastf");
		$I->click("#confirmation-popup-confirm-ok");
		$I->click("#confirmation-popup .popup-submit");

		$I->comment("Add an instiution");
		$I->click('#add-inst-btn');
		$I->waitForElementVisible('#in-add-name');
		$I->fillField('#in-add-name', 'inst-toto');
		$I->click('#modal-inst-add .popup-submit');
		$I->waitForText('inst-toto');
		$I->seeNumberOfElements("#i-table tbody tr", self::NUM_INSTITUTIONS+2/* toto and description line*/);

		$I->comment("Delete an instiution");
		$I->click('#i-delete');
		$inst = 3;
		$I->waitForElementNotVisible('#institution-'.$inst);
		$I->seeNumberOfElements("#i-table tbody tr", self::NUM_INSTITUTIONS);
		$this->selectInstiution($I, 1);

		$I->click('#i-delete');
		$I->waitForText('Impossible de supprimer une institution contenant des utilisateurs');

		$I->comment("Remove all CAS connexions");
		// remove cas from UGA and todai and see error
		$this->selectInstiution($I, 2);
		$I->click('#i-edit');
		$I->waitForElementVisible('#in-edit-cas');
		$I->click('#in-edit-cas');
		$I->click('#modal-inst-edit .popup-submit');
		$I->waitForText("Le CAS est configuré mais n'est activé pour aucune institution", 3, 'li.err');
		
		$I->seeILeftTrace( [Trace::TEACHER_CREATE_USER,Trace::TEACHER_ADD_STUDENT,Trace::MANAGER_ADD_TEACHER_STATUS]);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("admin", 4); // 1 for connecting
	}

	public function editUser(AcceptanceTester $I) {
		// Open UGA
		$this->selectInstiution($I, 2);
		// Open student 2
		$I->click('#u-table>tbody>tr#user-5');
		$I->waitForText("mission1 du indefini au indefini", 3, '#uc-table');
		$I->see("(mission1) privée", '#ur-table');
		// Change prénom, nom, e-mail, intitutlé
		$I->click("#u-edit");
		$I->waitForElementVisible("#t-edit-first-name");
		$I->fillField('#t-edit-first-name', 'toto');
		$I->fillField('#t-edit-name', 'plop');
		$I->click('#t-edit-popup .popup-submit');
		// See change in selected
		$I->waitForText('toto', 3, '#u-table');
		$I->see('plop', '#u-table');
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("admin", 1); // 1 for connecting
	}

	private function impersonate(AcceptanceTester $I, $name, $expectedRole)
	{
		$I->click('#u-impersonate');
		switch($expectedRole) {
			case 'learner':
				$url = '/reports';
				$seeAdmin = 'dontSee';
				$seeManager = 'dontSee';
				$nameselector = '#welcome';
				break;
			case 'admin' :
				$url = '/teacher/missions';
				$seeAdmin = 'see';
				$seeManager = 'see';
				$nameselector = '#authoring_menu';
				break;
			case 'manager' :
				$url = '/teacher/missions';
				$seeAdmin = 'dontSee';
				$seeManager = 'see';
				$name = $name[0].'.';
				$nameselector = '#authoring_menu';
				break;
			case 'teacher' :
				$url = '/teacher/missions';
				$seeAdmin = 'dontSee';
				$seeManager = 'dontSee';
				$nameselector = '#authoring_menu';
				break;
		}
		$name = $expectedRole == 'learner' ? $name : $name[0].'.';
		// see Shinji
		$I->waitForText($name, 8, $nameselector);
		$I->seeCurrentUrlEquals($url);
		$I->$seeAdmin('Admin');
		$I->$seeManager('Enseignants');
		$I->logout(false);
		$I->wait(.5);
		$I->seeCurrentUrlEquals('/admin');
	}

	private function changeRole (AcceptanceTester $I, $inst, $user, $short, $extended)
	{
		$this->selectInstiution($I, $inst);
		$I->click('#u-table>tbody>tr#user-'.$user);
		$I->click('#u-edit');
		$I->waitForText("Modifier un utilisateur");
		$I->selectOption('#t-edit-short-role', $short);
		$I->click('#t-edit-short-role option[value="'.$short.'"]');
		if ($extended) {
			$I->click('#'.$extended);
		}
		$I->click("#t-edit-popup-valid");
		$I->waitForText('Changement de rôle utilisateur');
		$I->click('#confirmation-popup-confirm-ok');
		$I->click('#confirmation-popup .popup-submit');
		$rolename = self::ROLESNAME[$extended ? $extended : $short];
		$I->waitForText($rolename, 3, "#u-table tr#user-$user");
	}
	
	public function changeRoleAndImpersonate(AcceptanceTester $I) {
		// OpenUGA
		$this->selectInstiution($I, 2);
		// Open Student 2
		$I->click('#u-table>tbody>tr#user-5');
		// Open UGA
		$this->impersonate($I, 'Shinji', 'learner');
		$this->changeRole($I, 2, 5, 'teacher', null);
		// Open Student 2
		$I->click('#u-table>tbody>tr#user-5');
		$this->impersonate($I, 'Shinji', 'teacher');
		$this->changeRole($I, 2, 5, 'teacher', 'manager');
		// Open Student 2
		$I->click('#u-table>tbody>tr#user-5');
		$this->impersonate($I, 'Shinji', 'manager');
		$this->changeRole($I, 2, 5, 'teacher', 'admin');
		// Open Student 2
		$I->click('#u-table>tbody>tr#user-5');
		$this->impersonate($I, 'Shinji', 'admin');
		
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("admin", 1); // 1 for connecting
	}
	
	public function addTeacher(AcceptanceTester $I) {
		// Click Todai
		$this->selectInstiution($I, 1);
		// Add teacher class
		$I->click('#i-edit');
		$I->selectOption('#in-edit-class', 7);
		$I->click('#modal-inst-edit .popup-submit');
		// Click add User
		$I->click('#u-add');
		$I->waitForElementVisible('#t-popup');
		$I->fillField('t-add-first-name', 'titi');
		$I->fillField('t-add-name', 'tutu');
		$I->fillField('t-add-email', 'titi.tutu@exampe.org');
		$I->click('#t-add-send-email');
		$I->seeInField('#t-add-login', 'tutut');
		$I->click('#t-popup-valid');
		$I->waitForText("L'utilisateur a bien été ajouté en tant qu'enseignant.");
		$I->click("#institution-2");
		$this->selectInstiution($I, 1, 1);
		// Click UGA
		$this->selectInstiution($I, 2);
		$I->click('#u-add');
		$I->waitForElementVisible('#t-popup');
		$I->fillField('#t-login-ldap', 'foobar');
		$I->scrollTo('#t-popup');
		$I->click("Interroger l'annuaire");
		$I->waitForText("Aucun utilisateur trouvé dans l'annuaire", 3, '.alert-danger');
		$I->fillField('#t-login-ldap', 'user-CAS');
		$I->click("Interroger l'annuaire");
		// click validate
		$I->waitForElementVisible('#t-add-ldap-popup');
		$I->see("Masako CAS", '#t-add-ldap-popup');
		$I->click('#t-add-ldap-popup-valid');
		$I->waitForText("Droits enseignants accordés à l'utilisateur", 3, '.alert-success');

		$I->seeILeftTrace([Trace::TEACHER_CREATE_USER,Trace::TEACHER_ADD_STUDENT,Trace::MANAGER_ADD_TEACHER_STATUS]);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("admin", 4); // 1 for connecting
	}

	public function deleteUser(AcceptanceTester $I) {
		// Click todai
		$this->selectInstiution($I, 1);
		$I->waitForElementVisible('#u-table');
		$I->see('Teacher6', '#u-table');
		$I->click('#u-table>tbody>tr#user-15');
		$I->click('#u-delete');
		$I->waitForElementVisible("#confirmation-popup");;
		$I->see("vous confirmez la suppression définitive de l'utilisateur teacher6");
		$I->click("#confirmation-popup-confirm-ok");
		$I->click("#confirmation-popup .popup-submit");
		$I->dontSee('Teacher6', '#u-table');
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("admin", 1); // 1 for connecting
	}

	public function anonymizeUser(AcceptanceTester $I) {
		$this->selectInstiution($I, 1);
		$I->waitForElementVisible('#u-table');
		$I->dontSee('Nymous', '#u-table');
		$I->see('Teacher6', '#u-table');
		$I->click('#u-table>tbody>tr#user-15');
		$I->click('#u-anonymize');
		$I->waitForElementVisible("#confirmation-popup");;
		$I->see("vous confirmez l'anonymisation définitive de l'utilisateur teacher6");
		$I->click("#confirmation-popup-confirm-ok");
		$I->click("#confirmation-popup .popup-submit");
		$I->waitForText('Nymous');
		$I->dontSee('Teacher6', '#u-table');
		$I->see('Nymous', '#u-table');
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("admin", 1); // 1 for connecting
	}
	
	public function extplatform(AcceptanceTester $I) {
		$I->see("API", '#e-table>tbody>tr');
		$I->see("http://labnbook-moodle.fr", '#e-table>tbody>tr');
		$I->see("UGA", '#e-table>tbody>tr');
		$I->see("Alice (alice@example.org)", '#e-table>tbody>tr');
		$I->click('#e-table>tbody>tr:last-child');
		$I->see("1 utilisateurs dont 1 enseignants");
		$I->see("/api", "#table-extplatform-3 tr:nth-child(1)");
		$I->see("TV8b<Nx@h3DtVf2=TvXqrPD=JBR|tzLL", "#table-extplatform-3 tr:nth-child(2)");
		$I->see("3", "#table-extplatform-3 tr:nth-child(3)");
		$I->click('#e-edit');
		$I->waitForElementVisible('#modal-extp-edit');
		$I->fillField('#extp-edit-name', 'API changed');
		$I->click('#modal-extp-edit .popup-submit');
		$I->see("API changed", '#e-table>tbody>tr');
		$I->see("http://labnbook-moodle.fr", '#e-table>tbody>tr');
		$I->see("UGA", '#e-table>tbody>tr');
		$I->click('#e-add');
		// add extplatform
		$I->waitForElementVisible('#modal-extp-add');
		$I->fillField('#extp-add-name', 'New API');
		$I->fillField('#extp-add-url', 'http://new-moodle.labnbook.fr');
		$I->fillField('#extp-add-contacts', 'Bob (bob@example.org)');
		$I->click('#modal-extp-add .popup-submit');
		$I->see("New API", '#e-table>tbody>tr:nth-child(1)');
		$I->see("Bob (bob@example.org)", '#e-table>tbody>tr:nth-child(1)');
		$I->see("http://new-moodle.labnbook.fr", '#e-table>tbody>tr:nth-child(1)');
		$I->see("Tōdai", '#e-table>tbody>tr:nth-child(1)');
		// delete
		$I->click('#e-delete');
		$I->dontSee("New API", '#e-table>tbody>tr');
		$I->dontSee("http://new-moodle.labnbook.fr", '#e-table>tbody>tr');
		$I->dontSee("Tōdai", '#e-table>tbody>tr');
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("admin", 1); // 1 for connecting
	}

	public function reports(AcceptanceTester $I) {
		$I->amOnPage("/teacher/reports");
		$I->see("Rapports", "#authoring_menu .on");
		$I->seeNumberOfElements("#reports-table tbody tr", 1);
		$I->see("Aucune donnée disponible dans le tableau");
		$I->fillSelect2('#reports-filter-mission', 'mission1');
		$I->click('#display-all-classes');
		$I->seeNumberOfElements("#reports-table tbody tr", 5);
		$I->see("Supprimé le");
		$I->clickWithLeftButton(['css' => "#report-5 td"]);
		$I->click("#ra-team-restore");
		$I->dontSee("Supprimé le");
		$I->seeILeftTrace(Trace::TEACHER_SELECT_REPORT);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("admin", 2); // 1 for connecting
	}
}
