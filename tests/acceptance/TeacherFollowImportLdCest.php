<?php

class TeacherFollowImportLdCest
{
	public function teacherImportLd(AcceptanceTester $I)
	{
		$I->loginAs('teacher1', 'mdp');
		$I->wantTo("Test if a teacher can import a labdoc in mode follow");
		// Add teacher1 in a class and in a team in order to create a report for teacher 1 as a student
		$I->amOnPage("/teacher/students");
		$I->click("#class-1 > td");
		$I->waitForElementVisible("#students");
		$I->click("#add-students-btn");
		$I->waitForElementVisible("#add-students");
		$I->click('#add-students-method input[value="form"]');
		$I->waitForElementVisible("#add-students-le-add-first-name");
		$I->fillField("#add-students-le-add-first-name", "Yukiko");
		$I->fillField("#add-students-le-add-name", "Teacher1");
		$I->click("#add-students");
		$I->waitForElementVisible("#le-add-popup-title");
		$I->click(".fa.fa-check[title='Ajouter cet étudiant à votre classe']");
		$I->see("Prénom");
		$I->click("#cl-actions .fa.fa-users");
		$I->waitForElementVisible("#tc-mission");
		$I->selectOption("#tc-mission","1");
		$I->waitForElementVisible("#tc-students-without-team");;
		$I->clickAndAcceptPopup("#tc-popup-submit");
		$I->see("Teacher1","#tc-student-with-team-panel");
		$I->click("#tc-popup-submit");
		
		// On report page teacher1 start a new report
		$I->amOnPage("/reports");
		$I->waitForElementVisible("#nouvelles");
		$I->click("#nouvelles");
		$I->waitForElementVisible("#btn_form_submit");
		$I->click("#btn_form_submit");
		
		// Add a text Ld in the report in order to import it in scope follow
		$I->waitForElementVisible("#headers_wrapper");
		$I->executeJS("addLD(1, 'text');");
		$I->waitForElementVisible("#ld_name_6");
		$I->fillField("#ld_name_6", "import ld");
		$I->switchToIFrame("#ld_txt_6_ifr");
		$I->waitForElementVisible('#tinymce', 5);
		$I->typeIn('#tinymce', "Texte aléatoire");
		$I->switchToWindow();
		$I->executeJS("validateLD(6, true);");
		$I->see("Texte aléatoire");
				
		// Check if teacher1 find the ld to  import in scope follow
		$I->amOnPage("/report/2?sc=follow");
		$I->waitForElementVisible('.ld_import_icon');
		$I->click(".ld_import_icon > img");
		$I->waitForElementVisible('.select_ld');
		$I->see('import ld');
		
	}

}
