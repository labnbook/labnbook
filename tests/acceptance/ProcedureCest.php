<?php

/*
 * @license http://www.gnu.org/licenses/gpl-3.0.html  GNU GPL v3
 */

use App\Trace;

class ProcedureCest
{
	const REPORT_ID = 3;
	const LABDOC_ID = 4;

	public function testRawInputInQuestion(AcceptanceTester $I)
    {
		$I->loginAs('student3', 'mdp');
		
		// We dont have trace 3 triggered
		$I->amOnPage("/report/" . self::REPORT_ID);

		$I->wantTo("(protocol) Input strange characters into a question");
		$labdocId = self::LABDOC_ID;
		$textPasted = "<code class=\"none\">α &lt; Ὤ </code> <br /> <p>\"d'Alembert <span>Straße\"</span>";
		$textExpected = "α < Ὤ   \"d'Alembert Straße\"";

		$I->comment("Display the protocol");
		$I->waitForElement("#labdoc_{$labdocId} .comment");
		$I->scrollTo("#labdoc_{$labdocId}", 0, -150);
		$I->waitForElementNotVisible('.labdoc-content-loading', 3);
		$I->waitForText("labdoc protocole", 3, "#labdoc_{$labdocId}");
		$I->see("Ma question");
		$I->see("Mon commentaire", "#labdoc_content_{$labdocId} .comment");

		$I->comment("Change the question's content");
		$I->modifyLabdoc($labdocId);
		$I->scrollTo("#labdoc_{$labdocId}", 0, -150);
		$I->click("#content_question_{$labdocId} .QHPContent.QHPMEditable");
		$I->clearText("#content_question_{$labdocId} .QHPContent.QHPMEditable");
		$I->dontSee("Ma question", "#content_question_{$labdocId}");
		$I->setHtmlContent("#content_question_{$labdocId} .QHPContent.QHPMEditable", $textPasted);
		$I->click("#content_question_{$labdocId} .QHPValidate");

		$I->waitForElementNotVisible('.labdoc-content-loading', 2);
		$I->comment("check the input was cleaned");
		$I->dontSee("Ma question", "#content_question_{$labdocId}");
		$I->dontSeeInSource($textPasted);
		$I->see($textExpected, "#content_question_{$labdocId}");

		$I->logout(); // logout
		$matches = $I->seeILeftTrace([Trace::CONNECT_LNB, Trace::ENTER_REPORT, Trace::EDIT_LD, Trace::MODIFY_LD, Trace::DISCONNECT_LNB], [3]);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("student3", $matches);
    }

	/**
	 * @dataprovider treeInputProvider
	 *
	 * @param AcceptanceTester $I
	 * @param \Codeception\Example $data
	 */
	public function testRawInputInTree(AcceptanceTester $I, \Codeception\Example $data)
    {
		$I->loginAs('student3', 'mdp');
		$I->amOnPage("/report/" . self::REPORT_ID);

		$labdocId = self::LABDOC_ID;
		$I->wantTo("(protocol) Change the tree's content");
		$I->waitForElement("#labdoc_{$labdocId} .comment");
		$I->modifyLabdoc($labdocId);
		$I->click("#procedure_{$labdocId} .jstree-last content");
		$I->click("Modifier", "#vakata-contextmenu");
		$I->setHtmlContent("#procedure_{$labdocId} div[contenteditable]", $data['pasted']);
		$I->click("#validateTask_{$labdocId}");

		$I->waitForElementNotVisible('.labdoc-content-loading', 2);
		$I->comment("check the input was cleaned");
		$I->seeInSource($data['expected']);
	
		$I->logout(); // logout
		$matches = $I->seeILeftTrace([Trace::CONNECT_LNB, Trace::ENTER_REPORT, Trace::EDIT_LD, Trace::MODIFY_LD, Trace::DISCONNECT_LNB], [3]);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("student3", $matches);
    }

	protected function treeInputProvider() {
		return [
			[
				// the raw input (HTML) pasted in the editable div
				'pasted' => "nbsp : et encore&nbsp;: <strong>&amp; &lt;-&gt;</strong>",
				// the raw output (HTML) read in the web page's source
				'expected' => "nbsp&nbsp;: et encore&nbsp;: &amp; &lt;-&gt;", // "nbsp : et encore : &amp; &lt;-&gt;",
			],
		];
	}
}
