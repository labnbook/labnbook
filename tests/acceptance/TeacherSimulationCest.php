<?php

namespace Tests\acceptance;

use AcceptanceTester;
use App\Trace;

class TeacherSimulationCest
{
	public function _before(AcceptanceTester $I)
	{
		$I->loginAs('teacher9', 'mdp');
		$I->amOnPage("/teacher/simulations");
		$I->dontSeeSpecials();
		$I->seeElement('.simulation-header');
		$I->selectOption(".simulation-code-selector", ['value' => "simulation-test"]);
		$I->waitForElementVisible(".simulation-table-wrapper");
	}
	
	private function addNewElement(AcceptanceTester $I, string $elementSelector, string $buttonText, int $positionExpected, string $description)
	{
		$I->click('.simulation-header-'.$elementSelector);
		$I->waitForElementVisible(".simulation-setup-".$elementSelector);
		$I->click($buttonText);
		$I->waitForElementVisible(".sortable-".$elementSelector."-item");
		$I->see(strval($positionExpected));
		$I->fillField(".sortable-".$elementSelector."-item:nth-of-type(".($positionExpected+1).") td:nth-child(3) .textarea-content", $description);
		$I->seeInSource($description);
	}
	
	private function linkElement(AcceptanceTester $I, string $elementSourceSelector, int $elementSourcePosition, int $indexSelectedOption)
	{
		$I->click('.simulation-header-'.$elementSourceSelector);
		$I->waitForElementVisible(".simulation-setup-".$elementSourceSelector);
		$I->selectOption(".sortable-".$elementSourceSelector."-item:nth-of-type(".($elementSourcePosition+1).") td:nth-last-child(2) select", $indexSelectedOption);
	}
	
	public function teacherCreateAndFillASimulation(AcceptanceTester $I)
	{
		$I->comment('Add new knowledge type to simulation');
		$I->see('Connaissances');
		$this->addNewElement($I, 'global-knowledge', 'Ajouter une connaissance', 0, 'description de la connaissance 0');
		$this->addNewElement($I, 'global-knowledge', 'Ajouter une connaissance', 1, 'description de la connaissance 1');

		$I->comment('Add new task type to simulation and link them to global knowledge');
		$I->see('Types de tâche');
		$this->addNewElement($I, 'task-type', 'Ajouter un type de tâche', 0, 'description du type de tâche 0');
		$this->addNewElement($I, 'task-type', 'Ajouter un type de tâche', 1, 'description du type de tâche 1');
		$this->linkElement($I, 'task-type', 0, 2);
		$this->linkElement($I, 'task-type', 1, 1);
		$I->click('.simulation-header-global-knowledge');
		$I->waitForElementVisible(".simulation-setup-global-knowledge");
		$I->see('1', ".sortable-global-knowledge-item:nth-of-type(1) td:nth-last-child(2)");
		$I->see('0', ".sortable-global-knowledge-item:nth-of-type(2) td:nth-last-child(2)");
		
		$I->comment('Add new constraint to simulation and link them to task type');
		$I->see('Contraintes');
		$this->addNewElement($I, 'constraint', 'Ajouter une contrainte', 0, 'description de la contrainte 0');
		$this->addNewElement($I, 'constraint', 'Ajouter une contrainte', 1, 'description de la contrainte 1');
		$this->linkElement($I, 'constraint', 0, 2);
		$this->linkElement($I, 'constraint', 1, 1);
		$I->click('.simulation-header-task-type');
		$I->waitForElementVisible(".simulation-setup-task-type");
		$I->see('1', ".sortable-task-type-item:nth-of-type(1) td:nth-last-child(3)");
		$I->see('0', ".sortable-task-type-item:nth-of-type(2) td:nth-last-child(3)");
	}
	
	
	
}
