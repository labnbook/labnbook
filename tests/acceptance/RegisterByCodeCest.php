<?php

use App\Trace;

class RegisterByCodeCest
{
	const CODE = "code_classe7";
	const CODE_CAS = "code_classe1";

	/**
	 * Handle registerByCode intermediate steps
	 * @param AcceptanceTester $I
	 * @param string $first user_name
	 * @param string $first last_name
	 * @param string $email
	 * @param string $action back|confirm (login manual| create account)
	 * @param string $code
	 * @param boolean exists
	 */
	private function registerByCode($I, $first, $last, $email, $action, $code, $exists=false)
	{
		$I->fillField("input#user_code", $code);
		$I->click('#login-code');
		$I->waitForText("Utilisation d'un code d'inscription fourni par un enseignant");
		$I->click("connectez-vous");
		if ($code === self::CODE_CAS) {
			$I->see("Compte institutionnel");
		} else {
			$I->dontSee("Compte institutionnel");
		}
		$I->dontSeeSpecials();
		if ($action == "create") {
			$I->click("créez un nouveau compte");
			$I->fillField('input[name=user_name]', $last);
			$I->fillField('input[name=first_name]', $first);
			$I->executeJS('$("input[name=first_name]").trigger("keyup")');
			$I->wait(2); // For homonyme trigger
			if ($exists) {
				$I->waitForText('Nous détectons', 3, '.alert-warning');
				// Abort
				return;
			} else {
				$I->dontSee('Nous détectons', '.alert-warning');
			}
			$I->fillField('input#user_login_create', $first.'.'.$last);
			$I->fillField('input#create-account-pwd', 'test42TEST4242');
			$I->fillField('input#create-account-pwd2', 'test42TEST4242');
			$I->fillField('input[name=email]', $email);
			$I->click('input[name=accept_cgu]');
			$I->click('#user_create_validate');
		}
	}

	public function authByCas(AcceptanceTester $I)
	{
		$I->wantTo("create a new CAS user by class code");
		// remove the CAS user from the DB, so that LnB creates a new account
		$I->updateInDatabase('user', ['login' => 'whatever', 'user_name' => 'titi', 'first_name' => 'toto', 'email' => '', 'inst_number' => null], ['login' => 'user-CAS']);

		$I->amOnPage('/login');
		$this->registerByCode($I, 'titi', 'toto', '', '', self::CODE_CAS);
		$I->click('#login-CAS input[type=submit]');

		$I->seeCurrentUrlMatches('#/reports#');
		$I->see("Vous avez été inscrit à LabNBook avec votre compte institutionnel", '.alert-success');
		$I->dontSeeSpecials();

		$I->seeILeftTrace(Trace::CONNECT_LNB);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("user-CAS", 1);
	}

	public function authByExistingCas(AcceptanceTester $I)
	{
		$I->wantTo("update a CAS user by class code (enter a class)");
		$I->amOnPage('/login');
		$this->registerByCode($I, 'titi', 'toto', '', '', self::CODE_CAS);
		$I->click('#login-CAS input[type=submit]');

		// fake CAS is configured with: CAS_MASQUERADE=user-CAS
		$I->dontSee("Vous avez été inscrit à LabNBook avec votre compte institutionnel", '.alert-success');
		$I->dontSeeSpecials();
		
		$I->seeILeftTrace(Trace::CONNECT_LNB);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("user-CAS", 1);
	}

	public function authByPassword(AcceptanceTester $I)
	{
		$I->wantTo("Register by code using an existing account");
		$I->amOnPage('/login');
		$this->registerByCode($I, 'titi', 'toto', '', '', self::CODE);
		$I->fillField('#user_login', 'student1');
		$I->fillField('#user_pwd', 'mdp');
		$I->click('Se connecter');
		$I->waitForElementVisible('#page-alerts');
		$I->dontSeeSpecials();
		
		$I->seeILeftTrace(Trace::CONNECT_LNB);
	}

	public function authByCreatingAccount(AcceptanceTester $I)
	{
		$firstname = "firsttest";
		$lastname = "lasttest";
		$username = $firstname.'.'.$lastname;

		$I->wantTo("create a new user by class code, with password authentication, triggering homonymes");
		$I->amOnPage('/login');

		// Just trigger the alert mechanism for homonymes
		$this->registerByCode($I, 'kotaro', 'student1', '', 'create', self::CODE_CAS, true);

		$I->wantTo("create a new user by class code, with password authentication");
		$I->amOnPage('/login');
		$this->registerByCode($I, $firstname, $lastname, '', 'create', self::CODE);
		$I->waitForElementVisible('#page-alerts');
		$I->see("Votre compte LabNBook a été créé", '.alert-success');
		$I->dontSee("Créer un compte", '.lb_dialog_box h2');
		$I->seeInDatabase('user', ['login' => $username]);

		$I->see("Bienvenue $firstname");
		$I->dontSeeSpecials();
		
		$I->seeILeftTrace(Trace::CONNECT_LNB);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin($username, 1);
	}

	public function inputWrongCode(AcceptanceTester $I)
	{
		$I->amOnPage('/login');
		$I->fillField('input#user_code', self::CODE . " err");
		$I->click('#login-code');
		$I->waitForElementVisible('.alert-danger');
		$I->see("Le code fourni n'existe pas ou a expiré", '.alert-danger');
		$I->dontSeeSpecials();
	}
}
