<?php


use App\Trace;

class AnnotationsCest
{
	const REPORT_ID = 3;

	private $annotations = [
		// labdocId => [text of annotation on title, text highlighted]
		"2" => ["Ma première annotation"],
		"4" => ["Annotation dans le protocole"],
	];

	public function addAnnotations(AcceptanceTester $I)
    {
		$I->loginAs('teacher1', 'mdp');
		$I->amOnPage("/report/". self::REPORT_ID."/?sc=follow" );
		$I->dontSeeSpecials();
		$I->dontSeeElement(".annotator-hl");
		$I->seeILeftTrace(Trace::TEACHER_FOLLOW_REPORT);

		$I->comment("Add annotations on labdoc titles");
		foreach ($this->annotations as $labdocId => $annotation) {
			$this->annotations[$labdocId][] = $I->grabTextFrom("#labdoc_{$labdocId} .labdoc_name");
			$I->highlightLabdocTitleAndClick($labdocId);
			$I->click(".annotator-adder button");
			$I->fillField(".annotator-ed-textarea", $annotation[0]);
			$I->click(".annotator-save");
			$I->dontSeeElement(".annotator-ed-textarea");
			$I->seeILeftTrace(Trace::TEACHER_ANNOTATE_REPORT);
			
		}

		$I->comment("check that the first annotation can be reused to create a new one");
		$I->highlightLabdocTitleAndClick(3);
		$I->click(".annotator-adder button");
		$I->waitForElementVisible(".previous-annotations.annotator-category-0 > div");
		$I->seeNumberOfElements(".previous-annotations.annotator-category-0 > div", count($this->annotations));
		foreach ($this->annotations as $labdocId => $annotation) {
			$I->see($annotation[0], ".previous-annotations.annotator-category-0");
		}
		$firstText = reset($this->annotations)[0];
		$I->click('.previous-annotations [data-text="' . $firstText . '"]');
		$I->seeInField(".annotator-ed-textarea", $firstText);
		$I->click(".annotator-save");
		$this->annotations["3"] = [$firstText, $I->grabTextFrom("#labdoc_3 .labdoc_name")];
		$I->seeNumberOfElements(".annotator-hl", count($this->annotations));

		$I->comment("cancelling a new annotation should not save it");
		$I->highlightLabdocTitleAndClick(5); // labdoc ID = 5
		$I->click(".annotator-adder button");
		$I->click(".annotator-cancel");

		$I->comment("annotations should stay after a page reload");
		$I->amOnPage("/report/". self::REPORT_ID . "/?sc=follow");
		$I->seeNumberOfElements(".annotator-hl", count($this->annotations));

		// now as a student
		$friendP = $I->haveFriend('student4');
		$share = (object) [
			'reportId' => self::REPORT_ID,
			'annotations' => $this->annotations,
		];

		$I->seeILeftGoodNumberOfTracesSinceLastLogin('teacher1', 5);

		$friendP->does(function(AcceptanceTester $I) use($share) {
			$I->wantTo("view the new annotations");
			$I->loginAs('student4', 'mdp');
			$I->amOnPage("/report/" . $share->reportId);
			$I->seeNumberOfElements(".annotator-hl", count($share->annotations));

            /* Some JS in this often breaks the tests (alerts, sync AJAX, slow exec...)
            
			$I->wantTo("Update the labdocs while keeping the annotations");
            $I->executeJS(<<<EOJS
window.alert = function(txt) { $("body").prepend("<h1>" + txt + "</h1>"); }
EOJS
            );
			foreach ($share->annotations as $id => $annotation) {
				$I->see($annotation[1], "#labdoc_$id .labdoc_name .annotator-hl");

				// Check that modifications don't remove them
				$I->modifyLabdoc($id);
				$I->executeJS("validateLD($id)");
				//$I->waitForElementVisible("#labdoc_$id .ld_name_actions .fa-check", 5);
				//$I->click(['css' => "#labdoc_$id .ld_name_actions .fa-check"]); // Valider (non-semantic HTML)
				$I->waitForElementNotVisible("#labdoc_$id .ld_name_actions .fa-check", 5);
				$I->waitForElementNotVisible('.labdoc-content-loading', 5);
				$I->seeElement("#labdoc_$id .labdoc_name .annotator-hl");
				$I->see($annotation[1], "#labdoc_$id .labdoc_name .annotator-hl");
			}
             */

			$I->wantTo("view the first annotation and trigger reading");
			$I->moveMouseOver(".annotator-hl");
			$I->wait(3);
			$I->moveMouseOver(".lb_ld_menu_btn");
			$I->wait(1);
			$I->seeILeftTrace(Trace::CHECK_ANNOTATION);
			$I->seeILeftGoodNumberOfTracesSinceLastLogin('student4', 3);
		});
	}
}
