<?php

use App\Trace;

class TeacherClassesCest
{
	const NUM_CLASSES_ACTIVE = 6;
	const NUM_CLASSES_ARCHIVE = 1;
	// Test longer class name (> 30 cf issue fix#1459)
	public const CLASSNAME = "Quisque nec justo vel sapien placerat tincidunt. Proin bibendum.";

	public function _before(AcceptanceTester $I) {
		$I->loginAs('teacher1', 'mdp');
		$I->amOnPage("/teacher/students");
		$I->waitForElementNotVisible('#cl-table-loading .fa-spinner', 3);
		$I->dontSeeSpecials();
	}

	public function viewClasses(AcceptanceTester $I)
    {
		$I->see("Classes", ".table-caption");
		$I->dontSee("Étudiants", ".table-caption");

		$I->waitForElement("#classes tbody", 1);
		$I->seeNumberOfElements("#classes tbody > tr", self::NUM_CLASSES_ACTIVE);
		$I->dontSee("Précédent", "#classes");
		
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("teacher1", 1); 
	}

	public function selectClass(AcceptanceTester $I)
    {
		$I->wantTo("select a class");
		$I->seeElement("#classes .lba-context-icons.actions-inactive");
		$I->click("tr#class-1 td"); // by id_class
		$I->see("Yukiko Teacher1", "#classes .description-row");
		$I->dontSeeElement("#classes .lba-context-icons.actions-inactive");

		$I->see("Étudiants", ".table-caption");
		$I->seeNumberOfElements("#students tbody > tr", 5);
		$I->seeElement("#students .lba-context-icons.actions-inactive");
		$I->dontSeeSpecials();

		$I->seeILeftGoodNumberOfTracesSinceLastLogin("teacher1", 1);
	}

	public function sortClasses(AcceptanceTester $I)
    {
		// default: sorted by name
		$I->see("classe LDAP", "#classes tbody > tr:last-child td");
		// sort so that the last class has the most students
		$I->click("#classes [title=\"Trier par nombre d'étudiants\"]");
		$I->see("classe 1", "#classes tbody > tr:last-child td");
		$I->see("5", "#classes tbody > tr:last-child td:last-child");

		$I->seeILeftGoodNumberOfTracesSinceLastLogin("teacher1", 1);
	}

	public function filterClasses(AcceptanceTester $I)
    {
		$I->checkOption("archives");
		$I->seeNumberOfElements("#classes tbody > tr", self::NUM_CLASSES_ACTIVE + self::NUM_CLASSES_ARCHIVE);
		$I->uncheckOption("archives");
		$I->seeNumberOfElements("#classes tbody > tr", self::NUM_CLASSES_ACTIVE);

		$I->click("#classes i.fa-search");
		$I->typeIn("#classes .search-input", "ldap");
		$I->seeNumberOfElements("#classes tbody > tr", 1);
		$I->clearText("#classes .search-input");
		$I->seeNumberOfElements("#classes tbody > tr", self::NUM_CLASSES_ACTIVE);

		$I->seeILeftGoodNumberOfTracesSinceLastLogin("teacher1", 1);
	}

	public function addClass(AcceptanceTester $I)
    {
		$I->wantTo("add a new class");
		$I->dontSee("Ajouter une classe", ".popup header");
		$I->click(['css' => "tr#class-1 td"]); // select a class (side noise)
		$I->click(['css' => "#classes .actions [data-action=add]"]);
		$I->seeElement('.popup header');
		$I->see("Ajouter une classe", ".popup header");
		$I->selectOption(".popup #cl-add-inst", "UGA");
		/**
		 * @todo Enable this part again, once the alert() is removed or reliably testable
		 *
		$I->click(".popup .popup-submit"); // fails: no name
		$I->acceptPopup();
		*/
		
		$I->fillField("#cl-add-name", self::CLASSNAME);
		$I->click("#cl-popup-valid");

		$I->waitForElementNotVisible('.popup header');
		$I->waitForElementNotVisible('#cl-table-loading .fa-spinner', 2);
		// Only a short visible part should be visible due to the width of the column
		$I->see(substr(self::CLASSNAME, 0, 20), "#classes td");

		$I->seeILeftTrace(Trace::TEACHER_ADD_CLASS);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("teacher1", 2);
	}

	public function deleteClass(AcceptanceTester $I)
    {
		$I->wantTo("delete a class");
		$I->click("#classes i.fa-search");
		$I->typeIn("#classes .search-input", "ldap");
		$I->seeNumberOfElements("#classes tbody > tr", 1);
		$I->see("Classe LDAP", "#classes td");
		$I->click("#classes td");
		$I->click("#classes .actions [data-action=archive]");
		
		// Wait for archive / delete popup
		$I->waitForElementVisible("#archive-popup");
		// Select delete radio button
		$I->click("#archive-popup #delete-input");
		$I->clickAndAcceptPopup("#archive-popup .popup-submit");
		
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("teacher1", 2);
    }

	public function unsubscribeTeacherFromClass(AcceptanceTester $I)
	{
		$I->wantTo("Unsubscribe from a class");
		$I->logout();
		$I->loginAs('teacher2', 'mdp');
		$I->amOnPage("/teacher/students");
		$I->dontSeeSpecials();
		$I->click("#classes i.fa-search");
		$I->typeIn("#classes .search-input", "philosophie");
		$I->seeNumberOfElements("#classes tbody > tr", 1);
		$I->see("philosophie", "#classes td");

		// Open archive popup
		$I->click("#classes td");
		$I->click("#classes .actions [data-action=archive]");
		// Wait for archive / delete popup
		$I->waitForElementVisible("#archive-popup");
		// Don't see unsubscribe because only one teacher
		$I->dontSeeElement("#archive-popup #unsubscribe-input");
		$I->click("#archive-popup .popup-cancel");

		// Add a teacher to the class
		$I->click("#classes .actions [data-action=edit]");
		$I->see("Modifier la classe", ".popup header");
		$I->see("Teacher2", ".popup"); // assigned teachers
		$I->fillSelect2("#cl-add-teacher-or-team", "Teacher1");
		$I->seeNumberOfElements("#cl-teachers-list > span", 2);
		$I->click("#cl-popup-valid");

		// Open archive popup
		$I->click("#classes .actions [data-action=archive]");
		// Don't see unsubscribe because only one teacher
		$I->seeElement("#archive-popup #unsubscribe-input");
		// Wait for archive / delete popup
		$I->waitForElementVisible("#archive-popup");
		$I->click("#archive-popup #unsubscribe-input");
		$I->click("#archive-popup .popup-submit");

		$I->dontSee("philosophie", "#classes td");
	}
	
	public function archiveClass(AcceptanceTester $I)
    {
		$I->wantTo("Archive a class");
		$I->click("#classes i.fa-search");
		$I->typeIn("#classes .search-input", "ldap");
		$I->seeNumberOfElements("#classes tbody > tr", 1);
		$I->see("Classe LDAP", "#classes td");
		$I->click("#classes td");
		$I->click("#classes .actions [data-action=archive]");
		
		// Wait for archive / delete popup
		$I->waitForElementVisible("#archive-popup");
		$I->click("#archive-popup .popup-submit");

		$I->seeILeftGoodNumberOfTracesSinceLastLogin("teacher1", 1);
		
		$I->dontSee("Classe LDAP", "#classes td");
		
		$I->click(".alert-close");
		$I->click('#display-archives');
		
		$I->see('Classe LDAP', "#classes td");
    }
	

	public function modifyClass(AcceptanceTester $I)
    {
		$I->wantTo("modify the attributes of a class");
		$I->click("tr#class-1 td"); // by id_class
		$I->waitForElementNotVisible(".popup header");

		$I->click("#classes .actions [data-action=edit]");
		$I->see("Modifier la classe", ".popup header");
		$I->see("Teacher1", ".popup"); // assigned teachers
		
		// Update classname
		$I->fillField("#cl-add-name", self::CLASSNAME . " lorem ipsum");
		$I->click("#cl-popup-valid");
		$I->waitForElementNotVisible(".popup header");
		// Only a short visible part should be visible due to the width of the column
		$I->see(substr(self::CLASSNAME, 0, 20), "#classes #class-1 td");
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("teacher1", 1);
		$I->logout(); // logout
    }

	public function teacherTeamManagementNotAvailable(AcceptanceTester $I)
	{
		$I->wantTo("Check if teacherTeam management is available only for manager ");
		$I->dontSee("#teacher_button_menu_button");
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("teacher1", 1);
	}
}
