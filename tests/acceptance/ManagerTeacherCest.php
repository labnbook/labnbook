<?php

use App\Trace;

/**
 * Description of TeacherTeacherTeamCest
 * @author Anthony Geourjon <anthony.geourjon@univ-grenoble-alpes.fr>
 */
class ManagerTeacherCest
{

	public function _before(AcceptanceTester $I) {
		$I->loginAs('teacher2', 'mdp');
		$I->amOnPage("/manager/teachers");
		$I->dontSeeSpecials();
		$I->waitForElementNotVisible('#tt-table-loading .fa-spinner', 3);
	}

	public function addNewTeacher(AcceptanceTester $I)
    {
		$I->wantTo("Add new teacher");
		$I->click("#t-add-teacher");
		$I->dontSeeSpecials();
		$I->click("#t-add-form");
		$I->fillField("#t-add-first-name", "Didier");
		$I->fillField("#t-add-name", "Teacher10");
		$I->fillField("#t-add-email", "teacher10@example.org");
		$I->fillField("#t-add-login", "Teacher10");
		$I->fillField("#t-add-password", "Password123");
		$I->click("#t-popup-valid");
		$I->waitForText("L'utilisateur a bien été ajouté en tant qu'enseignant.", 3, ".alert-info");
		$I->dontSeeSpecials();
		$I->seeILeftTrace([Trace::TEACHER_CREATE_USER, Trace::TEACHER_ADD_STUDENT, Trace::MANAGER_ADD_TEACHER_STATUS]);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("teacher2", 4);
		
	}

	public function addStudentAsTeacher(AcceptanceTester $I)
	{
		$I->wantTo("Add new teacher from student account");
		$I->click("#t-add-teacher");
		$I->click("#t-add-form");
		$I->dontSeeSpecials();
		$I->fillField("#t-add-first-name", "Évariste");
		$I->fillField("#t-add-name", "Student4");
		$I->click('#t-popup header');
		$I->waitForText("Enseignant existant ?");
		$I->dontSeeSpecials();
		$I->click("#t-add-popup section ul li i");
		$I->waitForText("L'utilisateur a bien été ajouté en tant qu'enseignant.", 3, ".alert-info");
		$I->dontSeeSpecials();
		$I->seeILeftTrace(Trace::MANAGER_ADD_TEACHER_STATUS);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("teacher2", 2);
	}
    
	public function addLdapUserAsATeacher(AcceptanceTester $I)
	{
		$I->wantTo("Add new teacher from Ldap account");
		$I->click("#t-add-teacher");
		$I->click("#t-add-ldap");
		$I->dontSeeSpecials();
		$I->fillField("#t-login-ldap", "foobar");
		$I->click("Interroger l'annuaire");
		$I->see("Aucun utilisateur trouvé dans l'annuaire", ".alert-danger");
		$I->fillField("#t-login-ldap", "user-CAS");
		$I->click("Interroger l'annuaire");
		$I->waitForText("Enseignant trouvé !");
		$I->dontSeeSpecials();
		$I->click("#t-add-ldap-popup-valid");
		$I->see("Droits enseignants accordés à l'utilisateur", ".alert-success");
		
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("teacher2", 1);
	}

	public function removeTeacherRight(AcceptanceTester $I)
	{
		$I->wantTo("Remove teacher right");
		//Give teacher right
		$I->click("#t-add-teacher");
		$I->click("#t-add-form");
		$I->fillField("#t-add-first-name", "Évariste");
		$I->fillField("#t-add-name", "Student4");
		$I->click('#t-popup header');
		$I->waitForText("Enseignant existant ?");
		$I->click("#t-add-popup section ul li i");
		$I->see("L'utilisateur a bien été ajouté en tant qu'enseignant.", ".alert-info");
		// Remove teacher right
		$I->fillField("#t-search-teacher", "Évariste");
		$I->waitForText("1 résultat");
		$I->clickAndAcceptPopup(".remove-status-enable");
		$I->see("Le statut enseignant de cet utilisateur a bien été retiré", ".alert-info");
		
		$I->seeILeftTrace([Trace::MANAGER_ADD_TEACHER_STATUS, Trace::MANAGER_REMOVE_TEACHER_STATUS]);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("teacher2", 3);
	}
	
}
