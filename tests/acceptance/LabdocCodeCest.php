<?php

use App\Trace;
use Codeception\Util\Locator;

class LabdocCodeCest {

	// https://github.com/php-webdriver/php-webdriver/blob/25a5655ea2dc63aed314535d637f83f66a984e67/lib/WebDriverKeys.php
	public const TAB = "\xEE\x80\x84";
	public const ENTER = "\xEE\x80\x87";
	public const SHIFT = "\xEE\x80\x88";
	public const ARROW_LEFT = "\xEE\x80\x92";
	public const ARROW_UP = "\xEE\x80\x93";
	public const ARROW_RIGHT = "\xEE\x80\x94";
	public const ARROW_DOWN = "\xEE\x80\x95";

	public const WAIT_TIME = 30;

	public function _before(AcceptanceTester $I)
	{
		$I->wantTo("Add a labdoc code as a teacher");
		
		// Login
		$I->loginAs('teacher1', 'mdp');
		$I->amOnPage("/teacher/missions");
		
		// Edit mission
		$I->typeIn('#private-missions .search-input', "mission1");
		$I->click('#my-missions tr[role=row] td');
		$I->click('.fas.fa-pencil-alt');

		// Add a new labdoc code
		$I->click('#at_rep_struct .edit-mission-title');
		$I->waitForElementVisible('#rp_1');
		$I->scrollTo('#rp_1');
		$I->click('#rp_code_1');
		$I->waitForElementVisible('iframe#ld_code_iframe_6', 10);
		$I->fillField("#ld_name_6", "Labdoc code");
		
		// Switch to iframe
		$I->switchToIFrame("jupyter_iframe_6");
		
		// Check first cell contains export_file function
		$I->waitForElementVisible('.jp-Notebook-ExecutionIndicator[data-status="idle"]', self::WAIT_TIME);
		$firstCellInput = $I->executeJS('return document.querySelectorAll(".jp-Notebook-cell")[0].textContent;');
		$I->assertStringContainsString("def export_file", $firstCellInput);
		
		// Write code to second cell
		$I->pressKey('.jp-NotebookPanel-notebook',self::TAB);
		$I->type('a = 1234567654321');
		
		// Execute and wait for result
		$I->click('button[data-command="runmenu:run"]');
		$I->type('a');
		$I->click('button[data-command="runmenu:run"]');
		$I->waitForElementVisible('.jp-Notebook-ExecutionIndicator[data-status="idle"]', self::WAIT_TIME);
		$thirdCellOutput = $I->executeJS('return document.querySelectorAll(".jp-Cell-outputWrapper")[2].textContent;');
		$I->assertStringContainsString("1234567654321", $thirdCellOutput);
		$I->click('.clear-output-button');
		$thirdCellOutput = $I->executeJS('return document.querySelectorAll(".jp-Cell-outputWrapper")[2].textContent;');
		$I->assertEmpty($thirdCellOutput);
		
		// validate and exit
		$I->switchToWindow();
		$I->click('#ld_edit_validate_6');
		$I->waitForElementVisible('#page-alerts');
		$I->see("3 rapport(s) d'étudiants sur 3 ont été mis à jour", "#page-alerts");
		$I->executeJS("logout();");
	}

	public function OpenLabdocCode(AcceptanceTester $I)
	{
		$I->wantTo("modify a labdoc code as a student");
		// Login
		$I->loginAs('student1', 'mdp');
		$I->amOnPage("/report/2");
		
		// Wait for iframe
		$I->waitForElementVisible('iframe#ld_code_iframe_7', 10);
		$I->switchToIFrame("jupyter_iframe_7");
		$I->waitForElement('.jp-Notebook-cell', 10);

		// Check second cell contains a = 1234567654321 without output
		$secondCellInput = $I->executeJS('return document.querySelectorAll(".jp-Notebook-cell")[1].textContent;');
		$I->assertStringContainsString("a = 1234567654321", $secondCellInput);
		$thirdCellOutput = $I->executeJS('return document.querySelectorAll(".jp-Cell-outputWrapper")[2].textContent;');
		$I->assertEmpty($thirdCellOutput);
		
		// Modify labdoc code
		$I->switchToWindow();
		$I->executeJS("editLD(7);");

		// Wait for iframe
		$I->waitForElementVisible('iframe#ld_code_iframe_7', 10);
		// Avoid JS alert popup, this issue seems to be linked to the title focus of the labdoc
		$I->disableAlertPopup();
		$I->switchToIFrame("jupyter_iframe_7");
		$I->wait(1);
		$I->waitForElementVisible('.jp-Notebook-ExecutionIndicator[data-status="idle"]', self::WAIT_TIME);
		
		// Check first cell contains export_file function
		$firstCellInput = $I->executeJS('return document.querySelectorAll(".jp-Notebook-cell")[0].textContent;');
		$I->assertStringContainsString("def export_file", $firstCellInput);

		// Write code to fourth cell
		$I->pressKey('.jp-NotebookPanel-notebook',self::TAB . self::ARROW_DOWN . self::ARROW_DOWN . self::ARROW_DOWN . self::ARROW_DOWN);
		$I->type('b = "chaine arbitraire"');
		$I->click('button[data-command="runmenu:run"]');
		$I->type('b');
		
		// Run all and check a and b outputs
		$I->pressKey('.jp-NotebookPanel-notebook',self::TAB);
		$I->click('button[data-command="runmenu:run"]');
		$I->click('button[data-command="runmenu:run"]');
		$I->click('button[data-command="runmenu:run"]');
		$I->click('button[data-command="runmenu:run"]');
		$I->waitForElementVisible('.jp-Notebook-ExecutionIndicator[data-status="idle"]', self::WAIT_TIME);
		$thirdCellOutput = $I->executeJS('return document.querySelectorAll(".jp-Cell-outputWrapper")[2].textContent;');
		$I->assertStringContainsString("1234567654321", $thirdCellOutput);
		$fifthCellOutput = $I->executeJS('return document.querySelectorAll(".jp-Cell-outputWrapper")[4].textContent;');
		$I->assertStringContainsString("chaine arbitraire", $fifthCellOutput);
	}
}
