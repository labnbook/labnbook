<?php

/**
 * @license http://www.gnu.org/licenses/gpl-3.0.html  GNU GPL v3
 */

use App\Trace;

/**
 * Description of AddStudentsToClassCest
 *
 * @author François Gannaz <francois.gannaz@silecs.info>
 * @author David Beniamine <David.Beniamine@tetras-libre.fr>
 */
class AddStudentsToClassCest
{
	private const ID_CLASSES = [3, 4, 7];
	private const METHODS = ['csv', 'form', 'class', 'number'];
	private const DEFAULT_METHOD = 'number';

	// Login as teacher1
	public function _before(AcceptanceTester $I){
		$I->loginAs('teacher1', 'mdp');
		$I->amOnPage("/teacher/students");
		$I->dontSeeSpecials();
	}

	// Open add students popup and validate submit button visibility
	private function openAddStudent(AcceptanceTester $I, int $id){
		$I->waitForElementVisible("tr#class-$id td");
		$I->click(['css' => "tr#class-$id td"]);
		$I->click(['css' => "#add-students-btn"]);
		$I->waitForElementVisible('#add-students');
		$I->dontSeeSpecials();
		$this->hasAction($I, 'submit', false);
		if ($id != 7) {
			$I->see("Interroger l'annuaire");
		}
	}

	// check that there is the right number of students in a class
	private function seeStudentsInClass(AcceptanceTester $I, int $id, int $num){
		if ($num > 0) {
			$I->waitForElementVisible("#le-table > tbody > tr");
			$I->seeNumberOfElements("#le-table > tbody > tr", $num);
		} else {
			$I->dontSeeElement("#le-table > tbody > tr");
		}
		$I->see((string)$num, "tr#class-$id td:last-child");
	}

	// Choose method and validate submit button visibility
	private function chooseMethod(AcceptanceTester $I, string $method){
		$selector_pre = 'input[name=student-method][value=';
		foreach(self::METHODS as $m){
			if ($m != self::DEFAULT_METHOD) {
				$test = "dontSeeCheckboxIsChecked";
			} else {
				$test = "seeCheckboxIsChecked";
			}
			$I->$test($selector_pre.$m.']');
		}
		$I->click($selector_pre.$method.']');
		$canValidate=false;
		switch($method){
			case 'class':
				$selector='#le-add-student-class-form';
				break;
			case 'csv':
				$selector='#csv-input';
				break;
			case 'number':
				$selector='#le-add-studentnumbers-popup-btn';
				break;
			case 'form':
				$canValidate=true;
				$selector='#add-students-le-add-first-name';
				break;
		}
		$I->waitForElementVisible($selector);
		$this->hasAction($I, 'submit', $canValidate);
	}

	// Check if action (submit/cancel) is visible
	private function hasAction(AcceptanceTester $I, string $action,bool $shouldSee){
		$selector='#add-students header .popup-'.$action;
		if($shouldSee){
			$I->waitForElementVisible($selector, 3);
		}else{
			$I->dontsee($selector);
		}
	}

	// Validate (submit/cancel) add-student dialog
	private function validate(AcceptanceTester $I, string $action, $willFail=false){
		$this->hasAction($I, $action, true);
		$I->click(['css' => "#add-students header .popup-$action"]);
		if(!$willFail){
			$I->waitForElementNotVisible('#add-students');
		}
	}

	// Select a class
	private function chooseClass(AcceptanceTester $I, int $id){
		$selector='#le-add-student-class-select';
		$I->click(['css' => $selector]);
		$I->selectOption($selector,$id);
		$I->waitForElementVisible('#le-add-student-class-students');
		$this->numStudentsSelected($I, 0);
	}

	// Check that student selected counter is ok
	private function numStudentsSelected(AcceptanceTester $I, int $num){
		$I->see($num . " étudiants sélectionnés", '#le-add-student-class-num-selected');
	}

	// Import students from class
	public function importFromClass(AcceptanceTester $I) {
		$I->comment('Import students from class');

		// Add student 4 from class 2
		$I->comment('Import student 4 from class 2');
		$this->openAddStudent($I, self::ID_CLASSES[0]);
		$this->chooseMethod($I, 'class');
		$I->seeNumberOfElements('#le-add-student-class-select>option', 3);
		$this->chooseClass($I, 2);
		$I->seeNumberOfElements('#le-add-student-class-students input', 2);
		$I->click(['css' => '#le-add-checkbox-4']);
		$this->numStudentsSelected($I, 1);
		$this->validate($I, 'submit');
		$I->waitForElementVisible('#le-table tr#learner-4', 3);
		// Hide class table
		$I->click(['css' => "tr#class-" . self::ID_CLASSES[0] . " td"]);
		$I->seeILeftTrace(Trace::TEACHER_ADD_STUDENT);

		// Add students all students from class 1 but 3
		$I->comment('Import student all students from class 1 but 61');
		$this->openAddStudent($I, self::ID_CLASSES[0]);
		$this->chooseMethod($I, 'class');
		$I->seeNumberOfElements('#le-add-student-class-select>option', 3);
		$this->chooseClass($I, 1);
		$I->seeNumberOfElements('#le-add-student-class-students input', 5);
		$I->click(['css' => '#le-add-checkbox-all']);
		$this->numStudentsSelected($I, 5);
		$I->click(['css' => '#le-add-checkbox-61']);
		$this->numStudentsSelected($I, 4);
		$this->validate($I, 'submit');
		$I->waitForElementVisible('.alert-warning');
		$I->see('Des étudiants étaient déjà dans la classe.', ".alert-warning");
		$I->see("Kōtaro Student1", ".alert-warning");
		$I->waitForElementVisible('#le-table tr#learner-4', 3);
		$this->seeStudentsInClass($I, self::ID_CLASSES[0], 4);
		$I->seeILeftTrace([Trace::TEACHER_ADD_STUDENT,Trace::TEACHER_ADD_STUDENT,Trace::TEACHER_ADD_STUDENT]);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin('teacher1', 5);
	}

	public function importFromCsvEmpty(AcceptanceTester $I){
		$I->comment('Import empty class');
		$this->openAddStudent($I, self::ID_CLASSES[0]);
		$this->chooseMethod($I, 'csv');
		$I->attachFile("#csv-input", "empty.csv");
		$I->waitForText("Aucun étudiant", 3, ".alert-danger");
	}

	public function importFromCsvOk(AcceptanceTester $I){
		$I->comment('Import good class');
		$this->openAddStudent($I, self::ID_CLASSES[0]);
		$this->chooseMethod($I, 'csv');
		$I->attachFile("#csv-input", "class_ok.csv");
		$this->hasAction($I, 'submit', true);

		$users = (object) [
			'total' => 4,
			'existing' => 1,
		];

		$I->seeNumberOfElements(".popup #csv-content tbody tr", $users->total);
		$I->seeInField(".popup #csv-content input[name=first_name]", "Kōtaro");
		$I->seeInField(".popup #csv-content input[name=first_name]", "PrenomA");
		$I->seeInField(".popup #csv-content input[name=first_name]", "PrenomB");
		$I->seeInField(".popup #csv-content input[name=first_name]", "PrenomC");
		// A student is already in the class
		$I->seeElement(".popup #csv-content .csv-option-cell .fa-exclamation-triangle");
		$I->seeNumberOfElements(".popup #csv-content .csv-option-cell .fa-exclamation-triangle", $users->existing);

		// remove
		for($i=0; $i<4; $i++) {
			$I->clickAndAcceptPopup('.cl-delete.fa-trash-alt');
		}
		$I->see("Aucun étudiant n'a été ajouté à la classe");
		$I->attachFile("#csv-input", "class_ok.csv");
		$this->hasAction($I, 'submit', true);
		$I->seeNumberOfElements(".popup #csv-content tbody tr", $users->total);

		$this->validate($I, 'submit');
		$this->seeStudentsInClass($I, self::ID_CLASSES[0], $users->total);
		$I->seeILeftTrace([Trace::TEACHER_CREATE_USER,Trace::TEACHER_ADD_STUDENT]);
		$emails = $users->total-$users->existing;
		$I->see($users->total." Étudiants ajoutés à la classe et ".$emails." mails envoyés", ".alert-info");
		$I->click('.alert-close');
		$I->click('.alert-close');
		// Hide class table
		$I->click(['css' => "tr#class-" . self::ID_CLASSES[0] . " td"]);

		$I->comment("import again the same CSV");
		$this->openAddStudent($I, self::ID_CLASSES[0]);
		$this->chooseMethod($I, 'csv');
		$I->attachFile("#csv-input", "class_ok.csv");
		$I->waitForElementVisible(".popup #csv-content .csv-option-cell .fa-exclamation-triangle");
		$I->seeNumberOfElements(".popup #csv-content .csv-option-cell .fa-exclamation-triangle", $users->total);
		$this->validate($I, 'submit');
		$this->seeStudentsInClass($I, self::ID_CLASSES[0], $users->total);

		$I->seeILeftGoodNumberOfTracesSinceLastLogin('teacher1', 3);
	}

	public function importFromCsvUgly(AcceptanceTester $I){
		$I->wantTo("Import a bad CSV (many errors and warnings)");
		$this->openAddStudent($I, self::ID_CLASSES[0]);
		$this->chooseMethod($I, 'csv');
		$I->attachFile("#csv-input", "class_ugly.csv");
		$this->hasAction($I, 'submit', true);

		$I->seeNumberOfElements(".popup #csv-content tbody tr", 23);
		$I->seeInField(".popup #csv-content input[name=first_name]", "Jérôme");
		$I->seeElement(".popup #csv-content .csv-option-cell .lb-warning");
		$I->seeNumberOfElements(".popup #csv-content .csv-option-cell .lb-warning", 10);
		$I->seeNumberOfElements(".popup #csv-content .csv-option-cell .lb-error", 14);

		$I->see("14 erreur(s)", ".popup");
		$this->validate($I, 'submit', true);
		$I->see("14 erreur(s)", ".popup");

		$I->seeILeftGoodNumberOfTracesSinceLastLogin('teacher1', 1);
	}

	private function checkSimilarity($I, $similarities) {
		if ($similarities) {
			// Trigger a similarity search and handle it
			$I->click('#add-students header');
			$I->waitForText("Étudiant existant ?");
			$I->click('#le-add-popup i.popup-cancel');
		}
	}

	private function failAddingStudent(AcceptanceTester $I, string $msg, $withSimilarity=false){
		$this->checkSimilarity($I, $withSimilarity);
		$this->validate($I, 'submit', true);
		$I->waitForText($msg, 3, '.alert-danger');
		// We cannot click by codeception as the modal intercepts the click, so we use JQuery
		$I->executeJS("$('.alert-danger .alert-close').click()");
	}

	private function testAddStudent(AcceptanceTester $I, int $cid, string $popupClick,  $exitAfterClick=false, $alreadyInDb=false){
		$prefix='add-students-le-add';

		$this->failAddingStudent($I, 'Le champ "Prénom" doit être renseigné');

		$I->fillField("#$prefix-first-name", 'aa');
		$I->waitForFieldFilled("#$prefix-login", "a");
		$this->failAddingStudent($I, 'Le champ "Nom" doit être renseigné', false);

		$I->fillField("#$prefix-name", 'superlongnom');
		$I->waitForFieldFilled("#$prefix-login", "superloa");
		if (!$alreadyInDb) {
			$this->failAddingStudent($I, "Vous devez définir un mot de passe (et le fournir à l'étudiant) ou indiquer une adresse e-mail pour qu'il soit généré aléatoirement et envoyé par e-mail.");
		}

		$I->fillField("#$prefix-email", 'aa');
		$I->waitForAttr("#$prefix-password", "placeholder", "Généré aléatoirement");
		if(!empty($popupClick)){
			$I->clickWithLeftButton(['css'=>$popupClick]);
			if($exitAfterClick){
				return;
			}
		}
		$this->failAddingStudent($I, "Le format de l'adresse e-mail n'est pas reconnu");

		$I->fillField("#$prefix-email", '');
		$I->fillField("#$prefix-password", 'aa');
		$this->failAddingStudent($I, 'Pour envoyer un e-mail récapitulatif, définissez une adresse mail');

		$I->fillField("#$prefix-email", 'aa@bb.cc');
		if (!$alreadyInDb) {
			$this->failAddingStudent($I, 'Le mot de passe doit comporter au moins 8 caractères dont une majuscule et une minuscule');
		}
		$I->fillField("#$prefix-password", 'aabbccdD');
		if(!empty($popupClick)){
			$I->waitForElementVisible($popupClick);
			$I->clickWithLeftButton(['css'=>$popupClick]);
		}
	}

	public function importFromForm(AcceptanceTester $I){
		$I->wantTo('Add student by form');
		$this->openAddStudent($I, self::ID_CLASSES[0]);
		$this->chooseMethod($I, 'form');
		$this->testAddStudent($I,self::ID_CLASSES[0], '');
		$this->validate($I, 'submit');
		$this->seeStudentsInClass($I, self::ID_CLASSES[0], 1);

		$I->seeILeftTrace([Trace::TEACHER_CREATE_USER, Trace::TEACHER_ADD_STUDENT]);
				
		// Hide class table
		$I->click(['css' => "tr#class-" . self::ID_CLASSES[0] . " td"]);

		// Add same student again
		$I->wantTo('Add same student again');
		$this->openAddStudent($I, self::ID_CLASSES[0]);
		$this->chooseMethod($I, 'form');
		$this->testAddStudent($I, self::ID_CLASSES[0], '#le-add-popup-cancel', false, true);
		$this->failAddingStudent($I, "La mise à jour n'a pas pu être faite : Il existe déjà un utilisateur ayant cet intitulé de compte (login)");

		$I->wantTo('Add student with same name but different password');
		$I->fillField("#add-students-le-add-password", '');
		$this->failAddingStudent($I, "La mise à jour n'a pas pu être faite : Il existe déjà un utilisateur ayant cet intitulé de compte (login)");
		
		$I->wantTo('Add student with a different login');
		$I->fillField("#add-students-le-add-login", 'superlob');
		$this->validate($I, 'submit');
		$this->seeStudentsInClass($I, self::ID_CLASSES[0], 2);

		$I->seeILeftTrace([Trace::TEACHER_CREATE_USER, Trace::TEACHER_ADD_STUDENT]);
		
		$I->wantTo('Add an existing student');
		$this->openAddStudent($I, self::ID_CLASSES[1]);
		$this->chooseMethod($I, 'form');
		$this->testAddStudent($I, self::ID_CLASSES[1], '#le-add-popup .fa-check', true, true);
		$this->seeStudentsInClass($I, self::ID_CLASSES[1], 1);

		$I->seeILeftTrace( Trace::TEACHER_ADD_STUDENT);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("teacher1", 6);
	}

	public function importFromLdapChangeLogin(AcceptanceTester $I){
		$I->wantTo("Import students by its student numbers (LDAP) twice changing the number in between");
		$this->openAddStudent($I, self::ID_CLASSES[0]);
		$this->chooseMethod($I, 'number');

		$I->fillField(".popup textarea", "12345678");
		$I->click("Interroger l'annuaire");
		$this->hasAction($I, 'submit', true);

		$I->seeNumberOfElements(".popup #csv-content tbody > tr", 1);

		$I->seeInField("tr:first-child input[name=first_name][disabled]", "Dimitri");
		$I->seeInField("tr:first-child input[name=inst_number][disabled]", "12345678");
		$I->seeInField("tr:first-child input[name=email]", "pas.de.ldap@dev.com");
		$I->uncheckOption("#le-add-send-email");

		$I->dontSeeInDatabase('user', ['user_name' => 'Karamazov']);
		$this->validate($I, 'submit');
		$I->seeInDatabase('user', ['user_name' => 'Karamazov']);
		$I->seeInDatabase('user', ['login' => 'login-agalan']);
		$I->updateInDatabase('user', ['inst_number' => '23456789', 'email'=>'vieux.ldap@dev.com'], ['user_name' => 'Karamazov']);

		$I->amOnPage("/teacher/students");
		$this->openAddStudent($I, self::ID_CLASSES[0]);
		$this->chooseMethod($I, 'number');

		$I->fillField(".popup textarea", "12345678");
		$I->click("Interroger l'annuaire");
		$this->hasAction($I, 'submit', true);

		$I->seeNumberOfElements(".popup #csv-content tbody > tr", 1);

		$I->seeInField("tr:first-child input[name=first_name][disabled]", "Dimitri");
		$I->seeInField("tr:first-child input[name=inst_number][disabled]", "12345678");
		$I->seeInField("tr:first-child input[name=email]", "pas.de.ldap@dev.com");
		$I->uncheckOption("#le-add-send-email");

		$I->seeInDatabase('user', ['login' => 'login-agalan.1']);
		$I->dontSeeInDatabase('user', ['login' => 'login-agalan']);
		$this->validate($I, 'submit');
		$I->seeInDatabase('user', ['login' => 'login-agalan.1']);
		$I->seeInDatabase('user', ['login' => 'login-agalan']);

	}

	public function importFromLdap(AcceptanceTester $I){
		$I->wantTo("Import students by their student numbers (LDAP)");
		$this->openAddStudent($I, self::ID_CLASSES[0]);
		$this->chooseMethod($I, 'number');

		$I->fillField(".popup textarea", "12345678 Bidon mais connu de dev\n87654321 inconnu de dev\n92345678 ok + 44 ignoré");
		$I->click("Interroger l'annuaire");
		$this->hasAction($I, 'submit', true);

		$I->see("Les numéros suivants n'ont pas été trouvés dans l'annuaire de l'établissement", ".alert-warning");
		$I->see("87654321", ".alert-warning");
		$I->dontSee("44", ".alert-warning");
		$I->seeNumberOfElements(".popup #csv-content .csv-option-cell .lb-warning", 0);
		$I->seeNumberOfElements(".popup #csv-content .csv-option-cell .lb-error", 0);
		$I->seeNumberOfElements(".popup #csv-content tbody > tr", 2);

		$I->seeInField("tr:first-child input[name=first_name][disabled]", "Dimitri");
		$I->seeInField("tr:first-child input[name=inst_number][disabled]", "12345678");
		$I->seeInField("tr:first-child input[name=email]", "pas.de.ldap@dev.com");
		$I->seeInField("input[name=email]", "pc@etu.uga.fr");

		for($i=0; $i<2; $i++) {
			$I->waitForText('Les numéros suivants');
			$I->clickAndAcceptPopup('.cl-delete.fa-trash-alt');
		}
		$I->waitForText("Aucun étudiant n'a été ajouté à la classe");
		$I->fillField(".popup textarea", "12345678 Bidon mais connu de dev\n87654321 inconnu de dev\n92345678 ok + 44 ignoré");
		$I->click("Interroger l'annuaire");
		$this->hasAction($I, 'submit', true);
		$I->seeNumberOfElements(".popup #csv-content tbody > tr", 2);

		$I->fillField("tr:first-child input[name=email]", "fake@etu.uga.fr");
		$I->uncheckOption("#le-add-send-email");

		$I->dontSeeInDatabase('user', ['user_name' => 'Karamazov']);



		$this->validate($I, 'submit');
		$I->click('.alert-close');
		$I->click('.alert-close');

		$I->seeInDatabase('user', ['user_name' => 'Karamazov']);
		$this->seeStudentsInClass($I, self::ID_CLASSES[0], 2);
		// Hide class table
		$I->click(['css' => "tr#class-" . self::ID_CLASSES[0] . " td"]);

		// import containingthe same users
		$I->wantTo("Import students (LDAP) that are already in LB");
		$I->amOnPage("/teacher/students");
		$I->waitForElementVisible("tr#class-" . self::ID_CLASSES[1] . " td");
		$I->click(['css' => "tr#class-" . self::ID_CLASSES[1] . " td"]); // empty class
		$I->dontSee("Karamazov", "#students tbody td");
		$I->click(['css' => "tr#class-" . self::ID_CLASSES[1] . " td"]);
		$this->openAddStudent($I, self::ID_CLASSES[1]);
		$this->chooseMethod($I, 'number');

		$I->fillField(".popup textarea", "2 étudiants déjà présents 12345678 et 92345678 dans une autre classe");
		$I->click("Interroger l'annuaire");

		$I->waitForElementVisible(".popup #csv-content .csv-option-cell .lb-warning");
		$I->seeNumberOfElements(".popup #csv-content .csv-option-cell .lb-warning", 2); // both are already in the DB
		$I->seeNumberOfElements(".popup #csv-content tbody > tr", 2);

		$I->seeInField("input[name=email]", "pc@etu.uga.fr");
		$I->seeInField("input[name=email][disabled]", "fake@etu.uga.fr");
		$I->uncheckOption("#le-add-send-email");

		$this->validate($I, 'submit');
		
		$I->seeILeftTrace([Trace::TEACHER_CREATE_USER, Trace::TEACHER_ADD_STUDENT, Trace::TEACHER_ADD_STUDENT]);
		
        $I->seeElement('#students tbody td');
		$I->see("Karamazov", "#students tbody td");
		$I->see("de Champignac", "#students tbody td");
		$I->see("2 Étudiants ajoutés à la classe", ".alert-info");
		$I->click('.alert-close');

		// Try open LDAP menu from class
		$I->wantTo("Check ldap not available for class without ldap inst");
		$this->openAddStudent($I, self::ID_CLASSES[2]);
		$I->dontSee("input[name=student-method][value=number]");
		
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("teacher1", 4);
	}

	public function resetOnCancel(AcceptanceTester $I){
		$I->wantTo('Check that popup is correctly reset on cancel');
		$this->openAddStudent($I, self::ID_CLASSES[0]);
		$this->chooseMethod($I, 'csv');
		$this->validate($I, 'cancel');
		// Hide class table
		$I->click(['css' => "tr#class-" . self::ID_CLASSES[0] . " td"]);
		$this->openAddStudent($I, self::ID_CLASSES[0]);
		$this->validate($I, 'cancel');
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("teacher1", 1);
	}
}
