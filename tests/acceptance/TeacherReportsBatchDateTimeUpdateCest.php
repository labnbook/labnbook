<?php

use App\Trace;

class TeacherReportsBatchDateTimeUpdateCest
{
	const NULL_DATE = NULL;
	const START_DATE_1 = '2005-05-05 05:05:00';
	const START_DATE_2 = '2006-06-06 06:06:00';
	const START_DATE_3 = '2007-07-07 07:07:00';
	const END_DATE_1 = '3008-08-08 08:08:00';
	const END_DATE_2 = '3009-09-09 09:09:00';
	const END_DATE_3 = '3010-10-10 10:10:00';
	const ID_REPORT_1 = 1;
	const ID_REPORT_2 = 2;

	public function _before(AcceptanceTester $I)
	{
		$I->loginAs('teacher1', 'mdp');
		$I->amOnPage("/teacher/reports");
	}
	
	public function testBatchDateTimeUpdate(AcceptanceTester $I)
	{
		// 2 reports batch update
		// Start dates, end dates are different
		$I->comment("Start dates, end dates are different");
		$this->updateSingleReportDateTime($I, self::ID_REPORT_1, self::START_DATE_1, self::END_DATE_1);
		$this->updateSingleReportDateTime($I, self::ID_REPORT_2, self::START_DATE_2, self::END_DATE_2);
		// -- Do nothing
		$I->comment("Start dates, end dates are different, do nothing");
		$this->updateMultipleReportsDateTime($I, array(self::ID_REPORT_1, self::ID_REPORT_2), 'start', false);
		$this->checkSingleReportsDateTime($I, self::ID_REPORT_1, 'start', self::START_DATE_1);
		$this->checkSingleReportsDateTime($I, self::ID_REPORT_1, 'end', self::END_DATE_1);
		$this->checkSingleReportsDateTime($I, self::ID_REPORT_2, 'start', self::START_DATE_2);
		$this->checkSingleReportsDateTime($I, self::ID_REPORT_2, 'end', self::END_DATE_2);
		// -- Update only start date with null (=erase dates)
		$I->comment("Start dates, end dates are different, update only start date with null (=erase dates)");
		$this->updateMultipleReportsDateTime($I, array(self::ID_REPORT_1, self::ID_REPORT_2), 'start', null);
		$this->checkSingleReportsDateTime($I, self::ID_REPORT_1, 'start', null);
		$this->checkSingleReportsDateTime($I, self::ID_REPORT_1, 'end', self::END_DATE_1);
		$this->checkSingleReportsDateTime($I, self::ID_REPORT_2, 'start', null);
		$this->checkSingleReportsDateTime($I, self::ID_REPORT_2, 'end', self::END_DATE_2);
		$this->updateSingleReportDateTime($I, self::ID_REPORT_1, self::START_DATE_1, self::END_DATE_1);
		$this->updateSingleReportDateTime($I, self::ID_REPORT_2, self::START_DATE_2, self::END_DATE_2);
		// -- Update only end date with non-null value
		$I->comment("Start dates, end dates are different, update only end date with non-null value");
		$this->updateMultipleReportsDateTime($I, array(self::ID_REPORT_1, self::ID_REPORT_2), 'end', self::END_DATE_3);
		$this->checkSingleReportsDateTime($I, self::ID_REPORT_1, 'start', self::START_DATE_1);
		$this->checkSingleReportsDateTime($I, self::ID_REPORT_1, 'end', self::END_DATE_3);
		$this->checkSingleReportsDateTime($I, self::ID_REPORT_2, 'start', self::START_DATE_2);
		$this->checkSingleReportsDateTime($I, self::ID_REPORT_2, 'end', self::END_DATE_3);
		$this->updateSingleReportDateTime($I, self::ID_REPORT_1, self::START_DATE_1, self::END_DATE_1);
		$this->updateSingleReportDateTime($I, self::ID_REPORT_2, self::START_DATE_2, self::END_DATE_2);
		
		// Start dates match (non-null value), end dates match (null value)
		$I->comment("Start dates match (non-null value), end dates match (null value)");
		$this->updateSingleReportDateTime($I, self::ID_REPORT_1, self::START_DATE_1, null);
		$this->updateSingleReportDateTime($I, self::ID_REPORT_2, self::START_DATE_1, null);
		// -- Update only start date with null (=erase dates)
		$I->comment("Start dates match (non-null value), end dates match (null value), update only start date with null (=erase dates)");
		$this->updateMultipleReportsDateTime($I, array(self::ID_REPORT_1, self::ID_REPORT_2), 'start', null);
		$this->checkSingleReportsDateTime($I, self::ID_REPORT_1, 'start', null);
		$this->checkSingleReportsDateTime($I, self::ID_REPORT_1, 'end', null);
		$this->checkSingleReportsDateTime($I, self::ID_REPORT_2, 'start', null);
		$this->checkSingleReportsDateTime($I, self::ID_REPORT_2, 'end', null);
		$this->updateSingleReportDateTime($I, self::ID_REPORT_1, self::START_DATE_1, null);
		$this->updateSingleReportDateTime($I, self::ID_REPORT_2, self::START_DATE_1, null);
		// -- Update only end date with non-null value
		$I->comment("Start dates match (non-null value), end dates match (null value), update only end date with non-null value");
		$this->updateMultipleReportsDateTime($I, array(self::ID_REPORT_1, self::ID_REPORT_2), 'end', self::END_DATE_3);
		$this->checkSingleReportsDateTime($I, self::ID_REPORT_1, 'start', self::START_DATE_1);
		$this->checkSingleReportsDateTime($I, self::ID_REPORT_1, 'end', self::END_DATE_3);
		$this->checkSingleReportsDateTime($I, self::ID_REPORT_2, 'start', self::START_DATE_1);
		$this->checkSingleReportsDateTime($I, self::ID_REPORT_2, 'end', self::END_DATE_3);
		$this->updateSingleReportDateTime($I, self::ID_REPORT_1, self::START_DATE_1, null);
		$this->updateSingleReportDateTime($I, self::ID_REPORT_2, self::START_DATE_1, null);
		
		// Start dates match (non-null) but end dates are different
		$I->comment("Start dates match (non-null) but end dates are different");
		$this->updateSingleReportDateTime($I, self::ID_REPORT_1, self::START_DATE_1, null);
		$this->updateSingleReportDateTime($I, self::ID_REPORT_2, self::START_DATE_1, self::END_DATE_2);
		// -- Update only end date with null (=erase dates)
		$I->comment("Start dates match (non-null) but end dates are different, update only end date with null (=erase dates");
		$this->updateMultipleReportsDateTime($I, array(self::ID_REPORT_1, self::ID_REPORT_2), 'end', null);
		$this->checkSingleReportsDateTime($I, self::ID_REPORT_1, 'start', self::START_DATE_1);
		$this->checkSingleReportsDateTime($I, self::ID_REPORT_1, 'end', null);
		$this->checkSingleReportsDateTime($I, self::ID_REPORT_2, 'start', self::START_DATE_1);
		$this->checkSingleReportsDateTime($I, self::ID_REPORT_2, 'end', null);
		$this->updateSingleReportDateTime($I, self::ID_REPORT_1, self::START_DATE_1, null);
		$this->updateSingleReportDateTime($I, self::ID_REPORT_2, self::START_DATE_1, self::END_DATE_2);
		// -- Update only start date with non-null value
		$I->comment("Start dates match (non-null) but end dates are different, update only start date with non-null value");
		$this->updateMultipleReportsDateTime($I, array(self::ID_REPORT_1, self::ID_REPORT_2), 'start', self::START_DATE_3);
		$this->checkSingleReportsDateTime($I, self::ID_REPORT_1, 'start', self::START_DATE_3);
		$this->checkSingleReportsDateTime($I, self::ID_REPORT_1, 'end', null);
		$this->checkSingleReportsDateTime($I, self::ID_REPORT_2, 'start', self::START_DATE_3);
		$this->checkSingleReportsDateTime($I, self::ID_REPORT_2, 'end', self::END_DATE_2);		
	}

	private function updateSingleReportDateTime($I, $idReport, $sqlStartDateTime, $sqlEndDateTime)
	{
		// Clicks and open popup
		$I->click(['css' => "#report-$idReport input[type=checkbox]"]);
		$I->click(['css' => "#modify-selected-reports"]);
		$I->waitForElementVisible("#te-popup");
		// Fill date time
		if (is_null($sqlStartDateTime)){
			$phpStartDate = '';
			$phpStartTime = '';
		} else {
			$phpStartDateTime = strtotime($sqlStartDateTime);
			$phpStartDate = date("Y-m-d", $phpStartDateTime);
			$phpStartTime = date("H:i", $phpStartDateTime);
		}
		if (is_null($sqlEndDateTime)){
			$phpEndDate = '';
			$phpEndTime = '';
		} else {
			$phpEndDateTime = strtotime($sqlEndDateTime);
			$phpEndDate = date("Y-m-d", $phpEndDateTime);
			$phpEndTime = date("H:i", $phpEndDateTime);
		}
		$I->executeJs("document.querySelector('#te-start-date').value = '$phpStartDate'");
		$I->executeJs("document.querySelector('#te-start-time').value = '$phpStartTime'");
		$I->executeJs("document.querySelector('#te-end-date').value = '$phpEndDate'");
		$I->executeJs("document.querySelector('#te-end-time').value = '$phpEndTime'");
		// Valid popup
		$I->click(['css' => "#te-popup-valid"]);
		$I->waitForElementNotVisible("#te-popup");
		$I->click(['css' => "#report-$idReport input[type=checkbox]"]);
	}

	private function updateMultipleReportsDateTime($I, $arrayOfIdReports, $startOrEnd, $sqlDateTime)
	{
		// Clicks and open popup
		foreach($arrayOfIdReports as $idReport){
			$I->click(['css' => "#report-$idReport input[type=checkbox]"]);
		}
		$I->click(['css' => "#modify-selected-reports"]);
		$I->waitForElementVisible("#batch-update-popup");
		// Fill date time
		if ($sqlDateTime !== false) {
			if (is_null($sqlDateTime)) {
				$phpDate = '';
				$phpTime = '';
			} else {
				$phpDateTime = strtotime($sqlDateTime);
				$phpDate = date("Y-m-d", $phpDateTime);
				$phpTime = date("H:i", $phpDateTime);
			}
			if ($I->grabValueFrom("input[name=" . $startOrEnd . "_datetimes_are_unified]") == 0){
				$I->click(['css' => "#modify-" . $startOrEnd . "-datetime"]);
			}
			$I->executeJs("document.querySelector('#batch-update-popup input[name=" . $startOrEnd . "_date]').value = '$phpDate'");
			$I->executeJs("document.querySelector('#batch-update-popup input[name=" . $startOrEnd . "_time]').value = '$phpTime'");
		}
		// Valid popup
		$I->click(['css' => "#batch-update-popup .popup-valid"]);
		if ($sqlDateTime !== false) {
			$I->see("rapport(s) modifié(s)", '.alert');
			$I->click(['css' => "#page-alerts .alert-close"]);
		} else {
			$I->see("rapport(s) modifié(s)", '.alert-danger');
			$I->click(['css' => "#page-alerts .alert-close"]);
		}
		// Re-open and check fields
		$I->click(['css' => 'input#reports-checkbox-all']);
		$I->click(['css' => 'input#reports-checkbox-all']);
		if ($sqlDateTime !== false) {
			foreach ($arrayOfIdReports as $idReport) {
				$this->checkSingleReportsDateTime($I, $idReport, $startOrEnd, $sqlDateTime);
			}
		}
	}
	private function checkSingleReportsDateTime($I, $idReport, $startOrEnd, $sqlDateTime)
	{
		if (is_null($sqlDateTime)) {
			$phpDate = '';
			if ($startOrEnd === 'start') {
				$phpTime = '00:00';
			} else {
				$phpTime = '23:59';
			}
		} else {
			$phpDateTime = strtotime($sqlDateTime);
			$phpDate = date("Y-m-d", $phpDateTime);
			$phpTime = date("H:i", $phpDateTime);
		}
		$I->click(['css' => "#report-$idReport input[type=checkbox]"]);
		$I->click(['css' => "#modify-selected-reports"]);
		$I->waitForElementVisible("#te-popup");
		$I->seeInField("#te-$startOrEnd-date", $phpDate);
		$I->seeInField("#te-$startOrEnd-time", $phpTime);
		$I->click(['css' => "#te-popup-cancel"]);
		$I->waitForElementNotVisible("#te-popup");
		$I->click(['css' => "#report-$idReport input[type=checkbox]"]);
	}
}
