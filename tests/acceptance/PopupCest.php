<?php

use App\Trace;

// Tests with popup needed to be accepted
class PopupCest{

	public function submitReport(AcceptanceTester $I){
		$I->wantTo("Submit a report");
		$I->loginAs('student1', 'mdp');
		$I->click("#encours > .menu_mission_item"); // cannot click() on text, because the HTML is misleading
		$I->waitForElementVisible("#btn_form_submit");
		$I->click("Continuer");
		$I->seeCurrentUrlMatches('#/report/#');
		$I->click("#lb_menubar_menu");
		$I->clickAndAcceptPopup("Rendre le rapport à l'enseignant");
		$I->wait(5);
		$I->seeInDatabase('report', ['id_report' => '2', 'status' => 'wait']);
		$I->seeILeftTrace([Trace::ENTER_REPORT, Trace::SUBMIT_REPORT]);
		$I->seeCurrentUrlMatches('#/reports#');
		
	}
}
