<?php

namespace Tests\acceptance;
use AcceptanceTester;
use App\Trace;

class TeacherFollowFreezeReportCest
{
    public function freezeReport(AcceptanceTester $I)
    {
        $I->wantTo("Test whether a teacher is able to freeze a report and leave the corresponding trace");
        $I->loginAs('teacher1', 'mdp');
        $I->amOnPage("/report/3?sc=follow");
        // We are not in freeze mode
        $I->assertEquals(false, $I->executeJS('return window.global_stop_sync'));
        // Click the freeze button
        $I->click(['css' => "#lb-freeze-sync i.fa-snowflake"]);
        // We are in freeze mode
        $I->assertEquals(true, $I->executeJS('return window.global_stop_sync'));
        // Unfreeze
        $I->click(['css' => "#lb-freeze-sync i.fa-snowflake"]);
        // We are not in freeze mode anymore
        $I->assertEquals(false, $I->executeJS('return window.global_stop_sync'));
        // Click on the diff button
        $I->click(['css' => ".ld-diff-toggle-button i.fa-plus-minus"]);
        $I->waitForText('Source');
        // We are in freeze mode
        $I->assertEquals(false, $I->executeJS('return window.global_stop_sync'));
        // Unfreeze
        $I->click(['css' => "#lb-freeze-sync i.fa-snowflake"]);
        // We are not in freeze mode anymore
        $I->assertEquals(true, $I->executeJS('return window.global_stop_sync'));
        // Check traces
        $I->seeILeftTrace([
            Trace::CONNECT_LNB, 
            Trace::TEACHER_FOLLOW_REPORT, 
            Trace::TEACHER_FREEZE_REPORT,
            Trace::TEACHER_DISPLAY_DIFFS,
            Trace::TEACHER_FREEZE_REPORT
        ]);
        $I->seeILeftGoodNumberOfTracesSinceLastLogin('teacher1', 5);
    }

}
