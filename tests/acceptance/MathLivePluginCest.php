<?php

use App\Trace;

class MathLivePluginCest {

    public const ENTER = "\xEE\x80\x87";
    public const ARROW_RIGHT = "\xEE\x80\x94";

    public function _before(AcceptanceTester $I) {
        $I->wantTo("Create an equation in a mission as a teacher");

        // Edit a mission
        $I->loginAs("teacher1", "mdp");
        $I->amOnPage("/teacher/missions");
        $I->dontSeeSpecials();
        $I->seeElement("#mission-1");
        $I->click("#my-missions tr#mission-1 td");
        $I->click(".fas.fa-pencil-alt");

        // Add a labdoc text
        $I->waitForElement("#at_rep_struct");
        $I->click("#at_rep_struct div.edit-mission-title");
        $I->click("#rp_text_1");
        $I->typeIn("#ld_name_6", "labdoc texte avec une équation");

        // Open MathLive editor
        $I->waitForElement("iframe.tox-edit-area__iframe");
        $I->switchToIFrame("#ld_txt_6_ifr");
        $I->waitForElementVisible('#tinymce', 5);
        $I->typeIn('#tinymce', "Texte introductif");
        $I->pressKey('#tinymce',self::ENTER);
        $I->switchToWindow();
        $I->click("button[title='Éditeur d’équations - Mathlive']");

        // Insert an equation
        $I->switchToIFrame("[src='/libraries/tinymce/plugins/mathlive-equation-editor/editor/equation_editor.html']");
        $I->waitForElementVisible('.ML__keyboard', 5);
        $I->click('.MLK__plate .MLK__layer .MLK__rows .MLK__row:first-child div:nth-of-type(3)');
        $I->type("27.04");
        $I->seeInField("textarea#mathlive-latex-formula", "\sqrt{27.04}");
        $I->click("#mathlive-calculator");
        $I->see("5.2", "#mathlive-result .ML__base span:nth-last-of-type(1)");
        $I->click("#mathlive-insert-calculator-result");
        $I->seeInField("textarea#mathlive-latex-formula", "\sqrt{27.04}\space = \space 5.2");
        $I->switchToWindow();
        $I->click(".tox-dialog__footer .tox-dialog__footer-end button[title='Enregistrer']");

        // Check insertion
        $I->switchToIFrame("#ld_txt_6_ifr");
        $I->waitForElementVisible("#tinymce span[data-katex='\\\\sqrt{27.04}\\\\space = \\\\space 5.2']");
        $I->see("5.2", "span.mord");
        $I->switchToWindow();
        $I->click("#ld_edit_validate_6");
        $I->executeJS("logout();");
    }

    public function studentEditEquation(AcceptanceTester $I)
    {
        $I->wantTo("Modify an equation as a student");

        // Login
        $I->loginAs('student1', 'mdp');
        $I->amOnPage("/report/2");

        // Check equation
        $I->see("27.04", "div#labdoc_7 span.mord");
        $I->see("5.2", "div#labdoc_7 span.mord");

        // Edit labdoc 
        $I->executeJS("editLD(7)");
        $I->waitForElement("iframe.tox-edit-area__iframe");
        $I->switchToIFrame("#ld_txt_7_ifr");
        $I->waitForElementVisible('#tinymce', 5);

        // Open MathLive editor with the current equation
        $I->click("#tinymce span.ew_ltxformula");
        $I->switchToWindow();
        $I->click("button[title='Éditeur d’équations - Mathlive']");
        $I->switchToIFrame("[src='/libraries/tinymce/plugins/mathlive-equation-editor/editor/equation_editor.html']");
        $I->waitForElementVisible('.ML__keyboard', 5);
        $I->seeInField("textarea#mathlive-latex-formula", "\sqrt{27.04}\space = \space 5.2");

        // Modify and center the equation
        $I->click("#mathlive-formula");
        $I->pressKey("#mathlive-formula", self::ARROW_RIGHT.self::ARROW_RIGHT.self::ARROW_RIGHT.self::ARROW_RIGHT.self::ARROW_RIGHT.self::ARROW_RIGHT.self::ARROW_RIGHT.self::ARROW_RIGHT.self::ARROW_RIGHT.self::ARROW_RIGHT.self::ARROW_RIGHT.self::ARROW_RIGHT.self::ARROW_RIGHT.self::ARROW_RIGHT.self::ARROW_RIGHT);
        $I->type('001');
        $I->seeInField("textarea#mathlive-latex-formula","\sqrt{27.04}\space=\space5.2001");
        $I->click("#mathlive-equation-center-input");
        $I->switchToWindow();
        $I->click(".tox-dialog__footer .tox-dialog__footer-end button[title='Enregistrer']");

        // Check the equation in TinyMCE
        $I->switchToIFrame("#ld_txt_7_ifr");
        $I->waitForElementVisible("#tinymce");
        $I->see("5.2001", "#tinymce span.katex-display span.mord");

        // Check the equation in LNB
        $I->switchToWindow();
        $I->executeJS("validateLD(7, true)");
        $I->see("5.2001", "span.katex-display span.mord");
    }
}
