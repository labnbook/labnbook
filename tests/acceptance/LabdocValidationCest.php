<?php

use App\Trace;

class LabdocValidationCest {

    public function _before(AcceptanceTester $I) {
        // Login
        $I->loginAs('student1', 'mdp');
        $I->amOnPage("/report/2");
        $I->waitForElementVisible("#report_part_1 #part_title_actions_1 img[src='/images/report/ld_type_text.svg']");
    }

    public function doubleClickToAddLabdoc(AcceptanceTester $I)
    {
        $I->wantTo("Add a labdoc by double-clicking on the corresponding icon");

        $I->doubleClick("#report_part_1 #part_title_actions_1 img[src='/images/report/ld_type_text.svg']");
        $I->waitForElement("iframe.tox-edit-area__iframe");
        $I->assertEquals(true, $I->executeJS('return parseInt(global_edited_ld) === 6'));
        $I->seeNumberOfElements('iframe', 1);
    }

    public function checkEmptyTitleBeforeValidation(AcceptanceTester $I)
    {
        $I->wantTo("Check whether manual validation with an empty title raises an alert");

        $I->click("#report_part_1 #part_title_actions_1 img[src='/images/report/ld_type_text.svg']");
        $I->waitForElement("iframe.tox-edit-area__iframe");
        $I->switchToIFrame("#ld_txt_6_ifr");
        $I->waitForElementVisible('#tinymce', 5);
        $I->typeIn('#tinymce', "Texte aléatoire");
        $I->switchToWindow();
        $I->click("#ld_name_actions_6 div[title='Enregistrer une nouvelle version du labdoc'] i");
        $I->waitForElementVisible('.alert-danger');
        $I->see("Le labdoc ne peut pas être validé car son titre n'est pas renseigné.", "#page-alerts");
    }
    
    public function checkEmptyTitleAutoSave(AcceptanceTester $I)
    {
        $I->wantTo("Check whether auto-save validation with an empty title works");

        $I->click("#report_part_1 #part_title_actions_1 img[src='/images/report/ld_type_text.svg']");
        $I->waitForElement("iframe.tox-edit-area__iframe");
        $I->switchToIFrame("#ld_txt_6_ifr");
        $I->waitForElementVisible('#tinymce', 5);
        $I->typeIn('#tinymce', "Texte aléatoire 2");
        $I->switchToWindow();
        $I->executeJS("(async () => { await  saveLocalLDVersion(global_edited_ld); autoUpdateLDinDB(false); })()");
        $I->wait(1);
        $I->reloadPage();
        $I->see("Nom de labdoc à indiquer", "span#ld_name_6");
        $I->see("Texte aléatoire 2", "#labdoc_content_6");
    }

    public function cancelOrValidateEmptyContent(AcceptanceTester $I)
    {
        $I->wantTo("Check whether cancel and validate empty content works");

        $I->click("#report_part_1 #part_title_actions_1 img[src='/images/report/ld_type_text.svg']");
        $I->fillField("input#ld_name_6", "Nom aléatoire de labdoc");
        $I->switchToWindow();

        $I->clickAndCancelPopup("#ld_name_actions_6 div[title='Enregistrer une nouvelle version du labdoc'] i");
        $I->assertEquals(true, $I->executeJS('return parseInt(global_edited_ld) === 6'));
        $I->seeNumberOfElements('iframe', 1);

        $I->clickAndAcceptPopup("#ld_name_actions_6 div[title='Enregistrer une nouvelle version du labdoc'] i");
        $I->see("Le texte est vide, cliquez sur modifier pour ajouter du texte", "#labdoc_content_6");
    }

    public function checkAutoValidLDWhileAddingALabdoc(AcceptanceTester $I)
    {
        $I->wantTo("Check whether a labdoc auto-validates while adding a labdoc");

        $I->click("#report_part_1 #part_title_actions_1 img[src='/images/report/ld_type_text.svg']");
        $I->fillField("input#ld_name_6", "Nom aléatoire de labdoc 4");
        $I->waitForElement("iframe.tox-edit-area__iframe");
        $I->switchToIFrame("#ld_txt_6_ifr");
        $I->waitForElementVisible('#tinymce', 5);
        $I->typeIn('#tinymce', "Texte aléatoire 4");
        $I->switchToWindow();

        $I->click("#report_part_1 #part_title_actions_1 img[src='/images/report/ld_type_dataset.svg']");
        $I->waitForElementVisible('.alert-info');
        $I->see("Un labdoc a été validé automatiquement.", "#page-alerts");
    }
    
    public function checkAutoValidLDWhileEditingALabdoc(AcceptanceTester $I)
    {
        $I->wantTo("Check whether a labdoc auto-validates while editing a labdoc");

        $I->click("#report_part_1 #part_title_actions_1 img[src='/images/report/ld_type_text.svg']");
        $I->waitForElement("iframe.tox-edit-area__iframe");
        $I->switchToIFrame("#ld_txt_6_ifr");
        $I->waitForElementVisible('#tinymce', 5);
        $I->typeIn('#tinymce', "Texte aléatoire 4");
        $I->switchToWindow();

        $I->doubleClick("#ld_name_1");
        $I->waitForElementVisible('.alert-info');
        $I->see("Un labdoc a été validé automatiquement.", "#page-alerts");
    }
    public function checkAlertBeforeEditingALabdoc(AcceptanceTester $I)
    {
        $I->wantTo("Check whether an empty labdoc raises an alert before editing another labdoc");

        $I->click("#report_part_1 #part_title_actions_1 img[src='/images/report/ld_type_text.svg']");
        $I->moveMouseOver("#labdoc_1 i.fa-ellipsis-v");
        $I->clickAndCancelPopup("#labdoc_1 table.lb_ld_menu tr[title='Modifier le labdoc']");
        $I->assertEquals(true, $I->executeJS('return parseInt(global_edited_ld) === 6'));
        $I->seeNumberOfElements('iframe', 1);

        $I->moveMouseOver("#labdoc_1 i.fa-ellipsis-v");
        $I->clickAndAcceptPopup("#labdoc_1 table.lb_ld_menu tr[title='Modifier le labdoc']");
        $I->see("Le texte est vide, cliquez sur modifier pour ajouter du texte", "#labdoc_content_6");
        $I->assertEquals(true, $I->executeJS('return parseInt(global_edited_ld) === 1'));
    }

    public function checkAlertBeforeSubmitAReport(AcceptanceTester $I)
    {
        $I->wantTo("Check whether an empty labdoc raises an alert before submitting a report");

        $I->click("#report_part_1 #part_title_actions_1 img[src='/images/report/ld_type_text.svg']");

        $I->moveMouseOver("#lb_menubar_menu .fa-bars");
        $I->clickAndCancelPopup("#lb_menubar_submenu a:nth-child(2)");
        $I->assertEquals(true, $I->executeJS('return parseInt(global_edited_ld) === 6'));
        $I->seeNumberOfElements('iframe', 1);

        $I->moveMouseOver("#lb_menubar_menu .fa-bars");
        $I->moveMouseOver("#lb_menubar_submenu");
        $I->clickAndAcceptPopup("#lb_menubar_submenu a:nth-child(2)");
        $I->amOnPage("/reports");
    }

    public function exitAReport(AcceptanceTester $I)
    {
        $I->wantTo("Check whether an empty labdoc raises an alert before exiting a report");

        $I->click("#report_part_1 #part_title_actions_1 img[src='/images/report/ld_type_text.svg']");

        $I->moveMouseOver("#lb_menubar_menu .fa-bars");
		$I->click("#lb_menubar_submenu a:nth-child(1)");
        $I->assertEquals("undefined", $I->executeJS('return typeof(global_id_report)'));
	}

    public function exportAReportToPDF(AcceptanceTester $I)
	{
        $I->moveMouseOver("#lb_menubar_menu .fa-bars");
        $I->clickAndAcceptPopup("#lb_menubar_submenu a:nth-child(2)");
        $I->waitForElementVisible("#lb_pdf_dialog");
    }

    public function checkAlertBeforeOpenVersioning(AcceptanceTester $I)
    {
        $I->wantTo("Check whether an empty labdoc raises an alert before opening versioning");

        $I->click("#report_part_1 #part_title_actions_1 img[src='/images/report/ld_type_text.svg']");
        $I->moveMouseOver("#labdoc_1 i.fa-ellipsis-v");
        $I->clickAndCancelPopup("#labdoc_1 table.lb_ld_menu tr[title='Accéder aux différentes versions du labdoc']");
        $I->assertEquals(true, $I->executeJS('return parseInt(global_edited_ld) === 6'));
        $I->seeNumberOfElements('iframe', 1);

        $I->moveMouseOver("#labdoc_1 i.fa-ellipsis-v");
        $I->clickAndAcceptPopup("#labdoc_1 table.lb_ld_menu tr[title='Accéder aux différentes versions du labdoc']");
        $I->waitForElementVisible("#ld_versions");
    }

}
