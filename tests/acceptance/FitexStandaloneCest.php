<?php

class FitexStandaloneCest
{
	public $saveCode;

	public function importAndSave(AcceptanceTester $I)
    {
		$I->amOnPage("/tools_standalone/fitex_stdalone.php");
		$I->click(".fx_main_menu");
		$I->click(".context-menu-item:nth-child(5)"); // "Importer..."

		$I->uncheckOption("has_header");
		$I->runAndActOnPopup('attachFile', ['.fx_import_div [type="file"]', "fitex_good-data.csv"], true); // confirm overwrite

		$I->dontSeeElement('.fitex-import');
		$I->seeNumberOfElements("th.fx_col_menu", 2);
		$I->see("data1", '.fx_header_name');
		$I->see("data2", '.fx_header_name');
		$I->seeNumberOfElements("tr.fx_default_row", 2);
		$I->seeNumberOfElements('.fx_editable_cell', 4);

		$I->click('#fxstd_saved_status');
		$saveCode = $I->grabTextFrom('#fxstd_code_code');
		$I->amOnPage("/");

		$I->wantTo("access to the saved Fitex data");
		$I->amOnPage("/tools_standalone/fitex_stdalone.php");
		$I->seeNumberOfElements("th.fx_col_menu", 4); // default is 4 columns
		$I->click('td.fxstd_menu_item.fxstd_btn');
		$I->fillField('#fxstd_code', $saveCode);
		$I->click('.fa-check.fxstd_btn');
		$I->waitForElementVisible("th.fx_col_menu");
		$I->seeNumberOfElements("th.fx_col_menu", 2);
		$I->seeNumberOfElements('.fx_editable_cell', 4);
	}
}
