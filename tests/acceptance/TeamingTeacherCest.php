<?php

use App\Trace;
use Codeception\Util\Locator;

/**
 * @group teaming
 */
class TeamingTeacherCest
{

	const ID_CLASS = 1;

	public function testProcess(AcceptanceTester $I) {
		$I->loginAs('teacher1', 'mdp');
		$I->amOnPage("/teacher/students");
		$this->createTeamConfig($I);
		$I->seeILeftTrace(Trace::TEACHER_ADD_TEAMING);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin('teacher1', 2);


		$this->studentSelectsHisTeam($I);
		$this->studentsHasNoTeam($I);

		$I->click(['css' => "tr#class-" . self::ID_CLASS . " td"]);
		$this->modifyTeamConfig($I);
		$I->seeILeftTrace(Trace::TEACHER_EDIT_TEAMING);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin('teacher1', 2);
	}

	private function switchUser($I, $name) {
		$I->logout();
		$I->loginAs($name, 'mdp');
	}

	private function createTeamConfig(AcceptanceTester $I) {
		$I->waitForElementVisible("tr#class-" . self::ID_CLASS . " td");
		$I->click(['css' => "tr#class-" . self::ID_CLASS . " td"]);
		$I->click(['css' => '[data-action="openTeamConfig"]']);
		$I->waitForElementVisible("#tc-popup");
		$I->dontSeeSpecials();

		$I->waitForElementVisible('#tc-mission option[value="3"]');
		$I->selectOption("#tc-mission", ['value' => "3"]);

		$I->click(['css' => '#tc-toggle-reports-options']);
		$I->click(['css' => '#tc-toggle-future-students-options']);
		$I->click(['css' => '#tc-toggle-students-options']);
		
		$I->seeNumberOfElements(".without-team", 5);
		$I->seeNumberOfElements(".with-team", 0);
		$I->selectOption("#tc-reuse-team-choice-mission",['value' => "8"]);
		$I->click(["css" => "#tc-reuse-team-choice"]);
		$I->seeNumberOfElements(".without-team", 3);
		$I->seeNumberOfElements(".with-team", 2);
		$I->seeNumberOfElements(".not-draggable", 0);
		
		$I->selectOption("#tc-teaming-method", ['text' => "uniquement par les enseignants"]);
		
		$I->typeIn('input[name=start_date]', "01012019"); // JS events, so cannot use $I->fillInField()
		$I->seeInField('input[name=start_date]', "2019-01-01"); // value is stored with ISO format

		$I->see("Prévenir les étudiants par e-mail de leur mise en équipe");
		$I->dontSee("Taille maximale");
		
		$I->dontSeeSpecials();
		
		// Not working with new teaming 
		$I->comment("create a team with 2 members");
		$firstStudent = Locator::elementAt('.left-pane .without-team', 1);
		$secondStudent = Locator::elementAt('.left-pane .without-team', 2);
		$I->clickWithLeftButton($firstStudent);
		$I->clickWithLeftButton($secondStudent);
		$I->clickWithLeftButton(Locator::elementAt('.team', 1));

		$I->click("#tc-popup-submit"); // submit
	
		$I->waitForText("mission3", 3, ".class-team-missions");
	}

	private function modifyTeamConfig(AcceptanceTester $I) {
		$I->click("mission3");
		$I->waitForElementVisible("#tc-popup");
		$I->seeOptionIsSelected("#tc-mission", "mission3");
		$I->seeInField('input[name=start_date]', "2019-01-01"); // value is stored with ISO format
        //self::unfoldAll($I);
		$I->click("#tc-toggle-reports-options");
		$I->selectOption('[name=tc-msg-send-ld]', "1");
	
		$I->seeNumberOfElements('#tc-fieldset-started-reports', 1);
		$I->click("#tc-popup-submit");  // submit
	}

	private function studentSelectsHisTeam(AcceptanceTester $I) {
		$I->comment("student1 opens their report");
		$this->switchUser($I, 'student1');
		$I->see("mission3");
		$I->click("mission3");
		$I->waitForElementVisible("#btn_form_submit");
		$I->click("Commencer");
		$I->waitForElementVisible("#headers_wrapper");
		$I->seeILeftTrace(Trace::ENTER_REPORT);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin('student1', 2);
		$this->switchUser($I, 'teacher1');
    }

	private function studentsHasNoTeam(AcceptanceTester $I) {
		$I->comment("student3 has no report");
		$this->switchUser($I, 'student3');
		// student has no team and cannot create one
		$I->dontSee("mission3");
		$this->switchUser($I, 'teacher1');
	}
}
