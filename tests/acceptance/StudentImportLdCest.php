<?php

class StudentImportLdCest
{
	private function addReportPart(AcceptanceTester $I)
	{
		$I->loginAs('teacher1', 'mdp');
		$I->amOnPage("/teacher/mission/8?");
		$I->click("#at_rep_struct .edit-mission-title");
        $I->click("#rp_add");
		$I->typeIn("#rp_edit_title_0", "Partie 1");
		$I->click("#rp_edit_validate_");
		$I->logout();
	}
	
	public function studentImportLd(AcceptanceTester $I)
	{
		$I->wantTo("Test if a student can import a labdoc from report page");
		$this->addReportPart($I);
		$I->loginAs('student1', 'mdp');
		$I->amOnPage("/report/6?");
		$I->waitForElement("#report_part_2 .ld_import_icon img");
		$I->click("#report_part_2 .ld_import_icon img");
		$I->waitForElement("#report_part_2 .select_ld");
		$I->click('(//div[@class="ld_to_import"])[1]');
		$I->see("labdoc texte 1", "#report_part_2 .labdoc .ld_name_actions span.labdoc_name_text");
	}
}
