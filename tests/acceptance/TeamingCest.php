<?php

use App\Trace;
use Codeception\Util\Locator;

/**
 * @group teaming
 */
class TeamingCest
{
	public function loadTeamConfig(AcceptanceTester $I) {
		$I->wantTo("Check an existing team config");
		$I->loginAs('teacher1', 'mdp');
		$I->amOnPage("/teacher/students");

		$I->waitForElementVisible("tr#class-1 td");
		$I->click(['css' => "tr#class-1 td"]);
		$I->see("mission1", ".class-team-missions"); // there is already a team_config
		$I->dontSeeSpecials();
		$I->dontSee("mission3", ".class-team-missions"); // no team_config yet with mission3
		$I->dontSeeElement('#tc-popup');

		$I->click("mission1");
		$I->waitForElementVisible("#tc-popup");
		
		$I->see("Mise en équipe : attribution d'une mission à la classe");
		$I->see("5 étudiants sur 5 sont en équipe");
		$I->click("#tc-toggle-reports-options");
		$I->click("#tc-toggle-future-students-options");
		$I->seeOptionIsSelected('[name=tc-msg-team]', "1");
		$I->seeOptionIsSelected('[name=tc-msg-teacher]', "1");
		$I->seeOptionIsSelected('#tc-msg-class', "1"); // "aux étudiants de la classe"
		$I->seeOptionIsSelected('#tc-msg-mission', "Aucune");
		$I->seeOptionIsSelected('[name=tc-msg-send-ld]', "0");
		$I->seeOptionIsSelected('[name=tc-msg-save-ld]', "1");
		$I->seeOptionIsSelected('#tc-import-ld', "mission3"); // mix of allow_import and allow_import_id_mission
		$I->seeOptionIsSelected("#tc-teaming-method",  "de façon aléatoire");


		$I->click("#tc-toggle-reports-options");
		$I->seeNumberOfElements(".without-team", 0);
		$I->seeNumberOfElements(".with-team", 5);
		$I->seeNumberOfElements(".not-draggable", 4);
	}

	public function deleteTeamConfig(AcceptanceTester $I) {
		$I->wantTo("Delete an existing team config (without report started)");
		$I->loginAs('teacher1', 'mdp');
		$I->amOnPage("/teacher/students");

		$I->waitForElementVisible("tr#class-1 td");
		$I->click(['css' => "tr#class-1 td"]);
		$I->see("mission1", ".class-team-missions"); // there is already a team_config
		$I->dontSeeSpecials();
		$I->dontSee("mission3", ".class-team-missions"); // no team_config yet with mission3
		$I->dontSeeElement('#tc-popup');

		$I->click("mission1");
		$I->waitForElementVisible("#tc-popup");

		$I->see("Mise en équipe : attribution d'une mission à la classe");

		$I->seeNumberOfElements(".not-draggable", 4);

		$I->see("Supprimer la mise en équipe", ".disabled");
		
		$I->click("#tc-popup-submit"); // Save. 
		$I->click("mission8");
		$I->clickAndAcceptPopup("#tc-delete-teaming");
		$I->see("Mise en équipe supprimée.", ".alert-info"); 
		$I->seeILeftTrace(Trace::TEACHER_EDIT_TEAMING);
	}
	
}
