<?php

use App\Trace;
use Codeception\Util\Locator;

class TeacherReportsCest
{
	const NUM_REPORTS_EMPTY = 3;
	const NUM_REPORTS_ACTIVE = 2;
	const NUM_REPORTS_DONE = 1;

	public function _before(AcceptanceTester $I) {
		$I->loginAs('teacher1', 'mdp');
		$I->amOnPage("/teacher/reports");
		$I->dontSeeSpecials();
	}

	public function viewReports(AcceptanceTester $I)
    {
		$reportIdWithUpdates = 3;
		$I->seeElement("#report-{$reportIdWithUpdates} a.labdoc-follow");
		$I->see("Rapports", "#authoring_menu .on");
		$I->seeNumberOfElements("#reports-table tbody tr", self::NUM_REPORTS_ACTIVE + self::NUM_REPORTS_DONE + self::NUM_REPORTS_EMPTY);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("teacher1", 1);
	}

	public function archiveReports(AcceptanceTester $I)
    {
		$I->seeElement("#report-1");
		$I->clickWithLeftButton(['css' => "#report-1 td"]);
		$I->seeElement('.lba-context-actions .batch-action');
		$I->click(['css' => "#archive-report"]);

		// Wait for archive / delete popup
		$I->waitForElementVisible("#archive-popup");
		// Select archive radio button
		$I->click("#archive-popup #archive-input");
		$I->click("#archive-popup .popup-submit");
		
		$I->waitForElementNotVisible("#report-1");

		$I->checkOption("display-archives");
		$I->seeElement("#report-1");
		$I->seeILeftTrace([Trace::TEACHER_SELECT_REPORT, Trace::TEACHER_ARCHIVE_REPORT]);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("teacher1", 3);
	}

	public function archiveMultipleReports(AcceptanceTester $I)
	{
		$I->seeElement("#report-1");
		$I->click(['css' => "#report-1 td input"]);
		$I->click(['css' => "#report-2 td input"]);
		$I->seeElement('.lba-context-actions .batch-action');
		$I->click(['css' => "#archive-report"]);
		
		// Wait for archive / delete popup
		$I->waitForElementVisible("#archive-popup");
		// Select delete radio button
		$I->click("#archive-popup #archive-input");
		$I->click("#archive-popup .popup-submit");
		
		$I->waitForElementNotVisible("#report-1");
		$I->waitForElementNotVisible("#report-2");

		$I->checkOption("display-archives");
		$I->seeElement("#report-1");
		$I->seeElement("#report-2");

		$I->seeILeftTrace([Trace::TEACHER_ARCHIVE_REPORT, Trace::TEACHER_ARCHIVE_REPORT]);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("teacher1", 3);
	}

	public function updateSingleReport(AcceptanceTester $I)
    {
		$I->click(['css' => "#modify-selected-reports"]); // inactive
		$I->dontSeeElement(".popup");
		$I->click(['css' => "#report-3 input[type=checkbox]"]);
		//$I->seeElement('.lba-context-actions .single-action');
		$I->dontSeeSpecials();
		$I->click(['css' => "#modify-selected-reports"]);
		$I->waitForElementVisible("#te-popup");

		$endDate = date('Y-m-d', time() + 86400);
		// fillField() fails on date|time input fields
		//$I->fillField("#te-end-date", $endDate);
		//$I->fillField("#te-end-time", '23:57');
		$I->executeJs("document.querySelector('#te-end-date').value = '$endDate'");
		$I->executeJs('document.querySelector("#te-end-time").value = "23:57"');
		$I->fillSelect2("#te-add-learner", "Student1 Kōtaro");
		$I->waitForText("Student1 Kōtaro", 3, '#te-learners-list');
		$I->click(['css' => "#te-popup-valid"]);
		$I->dontSeeElement(".popup");
		$I->click(['css' => "#modify-selected-reports"]);
		$I->seeElement('.popup');
		$I->seeInField("#te-end-date", $endDate);
		$I->seeInField("#te-end-time", '23:57');
		$I->seeILeftTrace([Trace::TEACHER_EDIT_REPORT,Trace::TEACHER_SELECT_REPORT]);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("teacher1", 3);
	}

	public function updateMultipleReports(AcceptanceTester $I)
    {
		$I->click(['css' => "#report-2 input[type=checkbox]"]);
		$I->dontSee("#modify-selected-reports"); // hidden
		$I->dontSeeElement(".popup");
		$I->click(['css' => "#report-3 input[type=checkbox]"]);
		$I->dontSee("#modify-selected-reports"); // button is hidden
		$I->click(['css' => "#modify-selected-reports"]);
		$I->seeElement("#batch-update-popup");

		$endDate = date('Y-m-d', time() + 2*86400);
		$I->executeJs("document.querySelector('#batch-update-popup input[name=end_date]').value = '$endDate'");
		$I->executeJs("document.querySelector('#batch-update-popup input[name=end_time]').value = '23:55'");
		$I->click(['css' => "#batch-update-popup .popup-valid"]);
		$I->see("2 rapport(s) modifié(s)", '.alert-success');
		$I->dontSeeElement(".popup");

		$I->click(['css' => "#report-3 input[type=checkbox]"]); 
		$I->click(['css' => "#modify-selected-reports"]); 
		$I->seeElement('.popup');
		$I->seeInField("#te-end-date", $endDate);
		$I->seeInField("#te-end-time", '23:55');

		$I->seeILeftTrace([Trace::TEACHER_EDIT_REPORT, Trace::TEACHER_EDIT_REPORT]);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("teacher1", 3);
	}

	public function deleteMultipleReports(AcceptanceTester $I)
	{
		$I->seeElement("#report-1");
		$I->click(['css' => "#report-1 input[type=checkbox]"]);
		$I->click(['css' => "#report-2 input[type=checkbox]"]);
		$I->seeElement('.lba-context-actions .batch-action');
		$I->click(['css' => ".lba-context-actions #archive-report"]);
		// Wait for archive / delete popup
		$I->waitForElementVisible("#archive-popup");
		// Select delete radio button
		$I->click("#archive-popup #delete-input");
		$I->clickAndAcceptPopup("#archive-popup .popup-submit");
		
		$I->waitForElementNotVisible("#report-1");
		$I->waitForElementNotVisible("#report-2");
		
		$I->seeILeftTrace([Trace::TEACHER_DELETE_REPORT, Trace::TEACHER_DELETE_REPORT]);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("teacher1", 3);
	}
}
