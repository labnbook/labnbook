<?php

use App\Trace;

class TeacherStudentsCest
{
	public function _before(AcceptanceTester $I) {
		$I->loginAs('teacher1', 'mdp');
		$I->amOnPage("/teacher/students");
		$I->waitForElement("tr#class-1", 3);
		$I->dontSeeSpecials();
	}

	public function selectStudents(AcceptanceTester $I)
    {
		$I->waitForElementVisible("tr#class-1 td");
		$I->click(['css' => "tr#class-1 td"]); // by id_class
		$I->seeElement("#students .lba-context-icons.actions-inactive");
		$I->dontSeeSpecials();

		$I->click(['css' => "#learner-4 td"]); // by id_user
		$I->dontSeeElement("#students .lba-context-icons.actions-inactive");
		$I->see("student1@example.org", "#students .description-row");

		$I->click(['css' => "#learner-4 td"]); // by id_user
		$I->seeElement("#students .lba-context-icons.actions-inactive");
		$I->dontSee("student1@example.org", "#students .description-row");
    }

	public function createUser(AcceptanceTester $I)
	{
		$I->click(['css' => "tr#class-3 td"]);
		$I->clickWithLeftButton(['css' => "#add-students-btn"]);
		$I->waitForElement('.popup-contents');
		$I->dontSeeSpecials();
        $I->seeElement('[name="student-method"]');
		$I->selectOption(['name' => 'student-method'], 'form');

		$I->waitForElementVisible('[name="add-students-le-add-first-name"]');
		$I->fillField("Prénom", "Muad'dib");
		$I->fillField("Nom * :", "Atréide's");
		$I->fillField("N° étudiant UGA", "01234");
		$I->fillField("E-mail", "paul@example.com");
		$I->fillField("Intitulé du compte (login)", "muaddib");
		$I->click(['css' => "#add-students .popup-submit"]);

		$I->waitForElementNotVisible("#add-students", 5);
        $I->waitForElementVisible('#le-table tbody td');
		$I->see("Muad'dib", '#le-table td');

		self::updateUser($I);

		$I->seeILeftTrace([Trace::TEACHER_CREATE_USER,Trace::TEACHER_ADD_STUDENT]);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin('teacher1', 3);
	}

	/**
	 * @param AcceptanceTester $I
	 */
	private static function updateUser(AcceptanceTester $I)
	{
		$I->wantTo("update an user that I just created");

		$I->see("Muad'dib", '#le-table tbody tr:first-child td'); // the target is in the first line
		$I->click(['css' => '#le-table tbody tr:first-child td']);
        // selected, now edit the profile
		$I->click(['css' => "#le-edit"]);
		$I->waitForElementVisible('#le-add-first-name', 5);
		$I->seeInField("Prénom", "Muad'dib");
		$I->seeInField("Nom * :", "Atréide's");
		$I->seeInField("N° étudiant UGA", "01234");
		$I->seeInField("E-mail", "paul@example.com");
		$I->seeInField("Intitulé du compte (login)", "muaddib");
		$I->fillField("Nom * :", "Atréides");
		$I->fillField("N° étudiant UGA", "");
		$I->fillField("E-mail", "");
		$I->fillField("Mot de passe", "AbracadabrA");
		$I->click(['css' => "#le-popup .popup-submit"]);

		$I->waitForElementNotVisible("#add-students", 5);
        $I->waitForElementVisible('#le-table tbody td');
		$I->click(['css' => '#le-table tbody tr:first-child td']);
		$I->click(['css' => "#le-edit"]);
		$I->waitForElementVisible('#le-add-first-name', 5);
		$I->seeInField("Prénom", "Muad'dib");
		$I->seeInField("Nom * :", "Atréides");
		$I->dontSeeInField("N° étudiant UGA", "01234");
		$I->dontSeeInField("E-mail", "paul@example.com");
	}
}
