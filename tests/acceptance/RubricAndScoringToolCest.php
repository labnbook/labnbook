<?php

namespace Tests\acceptance;

use AcceptanceTester;
use App\Trace;

class RubricAndScoringToolCest
{
	public function teacherAutoSaveRubric(AcceptanceTester $I) {
		$I->wantTo("Create a rubric as a teacher");

		$I->loginAs("teacher1", "mdp");
		$I->amOnPage("/teacher/missions");
		$I->dontSeeSpecials();
		$I->seeElement("#mission-1");
		$I->click("#my-missions tr#mission-1 td");
		$I->click(".fas.fa-pencil-alt");

		$I->waitForElement("#at_rep_eval");
		$I->click("#at_rep_eval .edit-mission-title");
		$I->waitForText("Créer une grille d’évaluation");
		$I->click("#at_rep_eval button");
		$I->waitForElement("#rubric");

		$I->waitForText("Ajouter un groupe de critères");
		$I->waitForElement("tr.add-group i.fa-plus");
		$I->click("tr.add-group i.fa-plus");
		$I->waitForElementVisible('textarea[placeholder="Intitulé du groupe de critères #1"]');
		$I->fillField('textarea[placeholder="Intitulé du groupe de critères #1"]', "groupe 1");

		$I->waitForText("Ajouter un critère");
		$I->click("tr.add-criterion i.fa-plus");
		$I->waitForElementVisible('textarea[placeholder="Intitulé du critère #1"]');
		$I->fillField('textarea[placeholder="Intitulé du critère #1"]', "critère 1");

		$I->waitForText("Ajouter un observable");
		$I->click("td.add-descrob div div:nth-child(3) i");
		$I->waitForElementVisible('textarea[placeholder="Observable #1"]');
		$I->fillField('textarea[placeholder="Observable #1"]', "ob 1");
		$I->fillField('tr.criterion td:nth-of-type(2) input[placeholder="Poids"]', "1,8");

		$I->click("td.add-descrob.right-stacked");
		$I->waitForElementVisible('textarea[placeholder="Observable #2"]');
		$I->fillField('textarea[placeholder="Observable #2"]', "ob 2");
		$I->fillField('tr.criterion td:nth-of-type(3) input[placeholder="Poids"]', "2");

		$I->fillField(".final-assessment-editor input", "12.4");
		$I->click("#rubric thead");
		$I->waitForElementNotVisible(".save-or-cancel i.fa-circle-exclamation");

        $I->waitForText("Grille critériée sauvegardée", 30, '.alert-success');
        $I->waitForElementNotVisible('.alert-success');

        $I->click('.save-or-cancel button[title="Sauvegarder"]');
        $I->waitForText("Grille critériée sauvegardée", 1, '.alert-success');
        $I->waitForElementVisible("#ru_1");

        $I->seeILeftTrace([Trace::SCORING_SAVE_GRID, Trace::SCORING_SAVE_GRID]);
	}

	public function moveCriterionFromGroup(AcceptanceTester $I) {
		$I->wantTo("Create a rubric as a teacher");

		$I->loginAs("teacher1", "mdp");
		$I->amOnPage("/teacher/missions");
		$I->dontSeeSpecials();
		$I->seeElement("#mission-1");
		$I->click("#my-missions tr#mission-1 td");
		$I->click(".fas.fa-pencil-alt");

		$I->waitForElement("#at_rep_eval");
		$I->click("#at_rep_eval .edit-mission-title");
		$I->waitForText("Créer une grille d’évaluation");
		$I->click("#at_rep_eval button");
		$I->waitForElement("#rubric");

		$I->waitForText("Ajouter un groupe de critères");
		$I->waitForElement("tr.add-group i.fa-plus");
		$I->click("tr.add-group i.fa-plus");
		$I->waitForElementVisible('textarea[placeholder="Intitulé du groupe de critères #1"]');
		$I->fillField('textarea[placeholder="Intitulé du groupe de critères #1"]', "groupe 1");

		$I->waitForText("Ajouter un critère");
		$I->click("tr.add-criterion i.fa-plus");
		$I->waitForElementVisible('textarea[placeholder="Intitulé du critère #1"]');
		$I->fillField('textarea[placeholder="Intitulé du critère #1"]', "critère 1");

		$I->waitForText("Ajouter un observable");
		$I->click("td.add-descrob div div:nth-child(3) i");
		$I->waitForElementVisible('textarea[placeholder="Observable #1"]');
		$I->fillField('textarea[placeholder="Observable #1"]', "ob 1");
		$I->fillField('tr.criterion td:nth-of-type(2) input[placeholder="Poids"]', "1,8");

		$I->click("td.add-descrob.right-stacked");
		$I->waitForElementVisible('textarea[placeholder="Observable #2"]');
		$I->fillField('textarea[placeholder="Observable #2"]', "ob 2");
		$I->fillField('tr.criterion td:nth-of-type(3) input[placeholder="Poids"]', "2");

		$I->waitForText("Ajouter un critère");
		$I->click("tr.add-criterion i.fa-plus");
		$I->waitForElementVisible('textarea[placeholder="Intitulé du critère #2"]');
		$I->fillField('textarea[placeholder="Intitulé du critère #2"]', "critère 2");

		$I->waitForText("Ajouter un observable");
		$I->click("td.add-descrob div div:nth-child(1) i");
		$I->waitForElementVisible('textarea[placeholder="Descripteur #1"]', 3);
		$I->fillField('textarea[placeholder="Descripteur #1"]', "des 1");
		$I->fillField('tr.criterion:nth-of-type(3) td:nth-of-type(2) input[placeholder="Poids"]', "1,8");

		$I->click("td.add-descrob.right-stacked:nth-of-type(3)");
		$I->waitForElementVisible('textarea[placeholder="Descripteur #2"]');
		$I->fillField('textarea[placeholder="Descripteur #2"]', "des 2");
		$I->fillField('tr.criterion:nth-of-type(3) td:nth-of-type(3) input[placeholder="Poids"]', "2");
		$I->fillField('.final-assessment-editor input', '20');
		$I->click("#rubric thead");
		$I->scrollTo("#rubric thead");
		$I->wait(1);
		$I->click('Sauvegarder');
		$I->wait(1);

		$I->amOnPage("/teacher/reports");
		$I->waitForText('Equipe_a_deux');
		$I->click('#report-3');
		$I->click('.fa-file');
		$I->switchToNextTab();
		$I->waitForText('Consignes');
		$I->click('#lb_menubar_eval .lnb-widget-menu-btn');
		$I->waitForText('Évaluation');
		$I->click('td.descrob');

		// Go back to rubric
		$I->switchToNextTab();
		$I->amOnPage("/teacher/missions");
		$I->dontSeeSpecials();
		$I->seeElement("#mission-1");
		$I->click("#my-missions tr#mission-1 td");
		$I->click(".fas.fa-pencil-alt");

		// Add a group
		$I->waitForElement("#at_rep_eval");
		$I->click("#at_rep_eval .edit-mission-title");
		$I->moveMouseOver('#ru_context_menu');
		$I->click('#ru_context_menu .fa-pencil-alt');
		$I->waitForText("Ajouter un groupe de critères");
		$I->waitForElement("tr.add-group i.fa-plus");
		$I->click("tr.add-group i.fa-plus");
		$I->waitForElementVisible('textarea[placeholder="Intitulé du groupe de critères #2"]');
		$I->fillField('textarea[placeholder="Intitulé du groupe de critères #2"]', "groupe 2");
		$I->click('.separator');
		// Move criterion
		$js = <<<EOJS
fakeid = $('table.rubric>tbody:nth-of-type(2)').attr('data-id') ;
event = {
  from: {
	dataset: {
	  type: 'criteria_group',
	  id: 1
	}
  },
   to: {
	dataset: {
	  type: 'criteria_group',
	  id: fakeid
	}
  },
  item: {
	dataset: {
	  type: 'criterion',
	  id: 2
	}
  }
};
getAlpineData('rubric').rubNS.sortAddAndRemove(event);
EOJS;
		$I->executeJS($js);

		$I->switchToNextTab();
		// Back to report : save eval
		$I->runAndActOnPopup('click', ['.buttonbox>button:nth-of-type(1)'], 'true');
		$I->wait(1);
		$I->switchToNextTab();
		$I->scrollTo("#rubric thead");
		// Save rubric
		$I->click('Sauvegarder');
		$I->wait(1);

		// Check that the evaluation is up to date
		$I->switchToNextTab();
		$I->reloadPage();
		$I->click('#lb_menubar_eval .lnb-widget-menu-btn');
		$I->waitForText('Évaluation');
		$I->see('groupe 1', '#assessment>tbody:nth-of-type(1)');
		$I->see('Critère 1', '#assessment>tbody:nth-of-type(1)');
		$I->see('ob 1', '.descrob.selected');
		$I->see('groupe 2', '#assessment>tbody:nth-of-type(2)');
		$I->see('Critère 2', '#assessment>tbody:nth-of-type(2)');

	}

	public function teacherAssessReport(AcceptanceTester $I)
	{
		$I->wantTo("Assess a report and publish evaluation");
    
        // Create rubric in DB
        $I->execSQL("INSERT INTO `rubric` (`id_rubric`, `id_activity`, `name`, `description`, `grading`, `min_grade`, `max_grade`, `min_bonus`, `max_bonus`, `created_at`, `updated_at`) VALUES (1, 1, 'Évaluation', NULL, 12.40, 0.00, 12.40, 0.00, 0.00, '2024-01-19 12:41:36', '2024-01-19 12:41:36');");
        $I->execSQL("INSERT INTO `criteria_group` (`id_criteria_group`, `id_origin`, `position`, `description`, `relative_weight`, `created_at`, `updated_at`, `id_rubric`, `id_assessment`) VALUE (1, NULL, 0, 'groupe 1', NULL, '2024-01-19 12:41:36', '2024-01-19 12:41:36', 1, NULL);");
        $I->execSQL("INSERT INTO `criterion` (`id_criterion`, `id_origin`, `id_rubric`, `id_assessment`, `id_section`, `id_criteria_group`, `type`, `position`, `description`, `relative_weight`, `comment`, `activated`, `created_at`, `updated_at`) VALUES (1, NULL, 1, NULL, NULL, 1, 'ob', 0, 'critère 1', NULL, NULL, 1, '2024-01-19 12:41:36', '2024-01-19 12:41:36');");
        $I->execSQL("INSERT INTO `descrob` (`id_descrob`, `id_origin`, `id_criterion`, `description`, `value`, `position`, `selected`, `created_at`, `updated_at`) VALUES (1165, NULL, 1, 'ob 1', 1.80, 0, 0, '2024-01-19 12:41:36', '2024-01-19 12:41:36'), (1166, NULL, 1, 'ob 2', 2.00, 1, 0, '2024-01-19 12:41:36', '2024-01-19 12:41:36');");
		
        $I->loginAs("teacher1", "mdp");
		$I->amOnPage('/report/2?sc=follow');
		$I->click('#lb_menubar_eval .lnb-widget-menu-btn');
		$I->waitForElementVisible("#scoring");

        $I->click("tr.criterion td:nth-of-type(2) div.hidden-checkbox-wrapper");
        $I->clickAndAcceptPopup(".buttonbox button i.fa-globe");

        $I->waitForElementVisible(".assessment-published-wrapper");
        $I->see("Évaluation (rendue)");

        $I->waitForElementVisible(".assessment-reinit-button-wrapper button i.fa-undo");
        $I->click(".assessment-reinit-button-wrapper button i.fa-undo");
        $I->waitForElementVisible(".buttonbox button i.fa-check");
        $I->click(".buttonbox button i.fa-check");
        $I->waitForElementVisible(".assessment-reinit-button-wrapper");

        $I->seeILeftTrace([Trace::TEACHER_FOLLOW_REPORT, Trace::SCORING_SAVE_EVAL, Trace::SCORING_PUBLISH_EVAL, Trace::SCORING_EDIT_EVAL, Trace::SCORING_SAVE_EVAL]);
	}

    public function studentViewAssessment(AcceptanceTester $I)
    {
        $I->wantTo("View a published assessment");

        // Create rubric and assessment in DB
        $I->execSQL("INSERT INTO `rubric` (`id_rubric`, `id_activity`, `name`, `description`, `grading`, `min_grade`, `max_grade`, `min_bonus`, `max_bonus`, `created_at`, `updated_at`) VALUES (1, 1, 'Évaluation', NULL, 12.40, 0.00, 12.40, 0.00, 0.00, '2024-01-19 11:41:36', '2024-01-19 11:41:36');");
        $I->execSQL("INSERT INTO `assessment` (`id_assessment`, `id_production`, `id_rubric_origin`, `bonus`, `bonus_comment`, `score`, `score_comment`, `published`, `grading`, `min_grade`, `max_grade`, `min_bonus`, `max_bonus`, `created_at`, `updated_at`) VALUES (1, 2, 1, NULL, NULL, 5.87, NULL, 1, 12.40, 0.00, 12.40, 0.00, 0.00, '2024-01-19 12:54:03', '2024-01-19 12:54:04');");
        $I->execSQL("INSERT INTO `criteria_group` (`id_criteria_group`, `id_origin`, `position`, `description`, `relative_weight`, `created_at`, `updated_at`, `id_rubric`, `id_assessment`) VALUES (1, NULL, 0, 'groupe 1', NULL, '2024-01-19 11:41:36', '2024-01-19 11:41:36', 1, NULL), (2, 1, 0, 'groupe 1', NULL, '2024-01-19 12:54:03', '2024-01-19 12:54:03', NULL, 1);");
        $I->execSQL("INSERT INTO `criterion` (`id_criterion`, `id_origin`, `id_rubric`, `id_assessment`, `id_section`, `id_criteria_group`, `type`, `position`, `description`, `relative_weight`, `comment`, `activated`, `created_at`, `updated_at`) VALUES (1, NULL, 1, NULL, NULL, 1, 'ob', 0, 'critère 1', NULL, NULL, 1, '2024-01-19 11:41:36', '2024-01-19 11:41:36'), (2, 1, NULL, 1, NULL, 2, 'ob', 0, 'critère 1', NULL, NULL, 1, '2024-01-19 12:54:03', '2024-01-19 12:54:03');");
        $I->execSQL("INSERT INTO `descrob` (`id_descrob`, `id_origin`, `id_criterion`, `description`, `value`, `position`, `selected`, `created_at`, `updated_at`) VALUES (1165, NULL, 1, 'ob 1', 1.80, 0, 0, '2024-01-19 11:41:36', '2024-01-19 11:41:36'), (1166, NULL, 1, 'ob 2', 2.00, 1, 0, '2024-01-19 11:41:36', '2024-01-19 11:41:36'), (1167, 1165, 2, 'ob 1', 1.80, 0, 1, '2024-01-19 12:54:03', '2024-01-19 12:54:04'), (1168, 1166, 2, 'ob 2', 2.00, 1, 0, '2024-01-19 12:54:03', '2024-01-19 12:54:04');");
        $I->execSQL("INSERT INTO `assessed_student` (`id_assessed_student`, `id_assessment`, `id_student`, `missing`, `created_at`, `updated_at`) VALUES (148, 1, 4, 0, '2024-01-19 12:54:04', '2024-01-19 12:54:04');");

        $I->loginAs("student1", "mdp");
        $I->amOnPage('/report/2?');
		$I->click('#lb_menubar_eval .lnb-widget-menu-btn');
        $I->waitForElementVisible("#scoring");
    
        $I->see("Évaluation (rendue)");
        $I->see("5,9 / 12,4");
        $I->seeILeftTrace([Trace::ENTER_REPORT, Trace::SCORING_VIEW_EVAL]);

		// Quick test on individual bonus
		$I->execSQL("UPDATE `assessed_student` set `bonus_comment` = 'peu faire mieux', `bonus` = -2 where id_assessed_student = 148");
		$I->reloadPage();
        $I->waitForElementVisible("#widgets_column");
		$I->click('#lb_menubar_eval .lnb-widget-menu-btn');
        $I->waitForElementVisible("#scoring");
		$I->see("peu faire mieux");

    }
}
