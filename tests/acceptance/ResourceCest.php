<?php

use App\Trace;

class ResourceCest{
	
	public function resource (AcceptanceTester $I){
		$I->wantTo("Edit a mission");
		$I->loginAs('teacher1', 'mdp');
		$I->amOnPage("/teacher/missions");
		$I->seeElement('#mission-1');
		$I->click("#mission-1");
		$I->click('.fas.fa-pencil-alt');
		$I->see("Paramètres généraux de la mission", '.edit-mission-title');
		$I->click('#at_rep_res .edit-mission-title');
		$I->click("#bloc_rd_add");
		
		// Add a file resource will be a better test. But it's a bit tricky with codeception 
		$I->wantTo("Add a URL resource");
		$I->click("#rd_type_link");
		$I->fillField("#rd_name", "Lien vers le site LabNBook");
		$I->fillField("#rd_url", "https://labnbook.fr");
		$I->click("i[title='Ajouter le document']");
		$I->wait(2);
		$I->seeILeftTrace([Trace::TEACHER_ADD_RESOURCE, Trace::TEACHER_MODIFY_MISSION]);
		$I->logout();
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("teacher1", 4 );
		
		$I->wantTo("Open a resource");
		$I->loginAs('student1', 'mdp');
		$I->waitForElementClickable("#encours > .menu_mission_item");
		$I->click("#encours > .menu_mission_item"); // cannot click() on text, because the HTML is misleading
		$I->waitForElementVisible("#btn_form_submit");
		$I->click("Continuer");
		$I->seeCurrentUrlMatches('#/report/#');
		$I->seeILeftTrace(Trace::ENTER_REPORT);

		$I->click("#lb_menubar_ressource .lnb-widget-menu-btn");
	
		$I->see("Lien vers le site LabNBook", ".widg-res-link");
		$I->click("Lien vers le site LabNBook");
		$I->switchToNextTab();
		$I->see("Le cahier de laboratoire collaboratif pour l’enseignement"); // Any text on the website
		$I->seeILeftTrace(Trace::OPEN_RESOURCE);
		$I->switchToPreviousTab();

		$I->wantTo("Add a resource (student)");
		$I->click("#add_doc_link");
		$I->fillField("#name_resource", "Lien vers wiki");
		$I->fillField("#url_document_address", "https://fr.wikipedia.org/");
		$I->click("button[title='Ajouter la ressource']");
		$I->seeILeftTrace(Trace::ADD_RESOURCE);

		$I->wantTo("Open assignment");
		$I->click("button.lnb-widget-menu-btn[title='Ressources']");
		$I->click(".widg-res-link .fa-chevron-right");
		$I->see("Réussir", "#widg-res-short-assignment > p");
		$I->seeILeftTrace(Trace::OPEN_ASSIGNMENT);
		
		$I->wantTo("Open detailed assignment");
		$I->click("a[title='Consulter la consigne détaillée (nouvel onglet)']");
		$I->wait(2);
		// second trace is not fired in reality (file is present) but in chromium it cannt be avoided
		$I->seeILeftTrace([Trace::OPEN_DETAILED_ASSIGNMENT]);
		// Database must be updated with a real resource file. File demanded is not found so redirect on the report page and add EnterReport trace 
		
		$I->wantTo("Open report part assignment");
		try {
			$I->see("Tout doit s'inscrire ci-dessous.");
			$I->click("span[title='Afficher les consignes spécifiques pour cette partie du rapport']");
			$I->click("span[title='Afficher les consignes spécifiques pour cette partie du rapport']");
			$I->see("Consigne de labdoc");
		} catch (Exception $e) {
			$I->click("span[title='Afficher les consignes spécifiques pour cette partie du rapport']");
			$I->see("Tout doit s'inscrire ci-dessous.");
			$I->see("Consigne de labdoc");
		}
	
		$I->wait(2);
		
		$I->seeILeftTrace(Trace::OPEN_RP_ASSIGNMENT);
		
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("student1", 7 );
	}
}
