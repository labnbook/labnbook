<?php

use App\Trace;

class SharedLabdocCest
{
	const LD_NAME = "Shared Labdoc";
	const LD_SHARED_TAG = "[Partagé au sein de la classe]";
	const ID_LABDOC_TEACH = 6;
	const ID_LABDOC_STUD = 7;
	const STUDENT_1_TEXT = "Student 1 has modified this LD";

	public function shareLabdocInClass(AcceptanceTester $I) {
		// Login as teacher
		$I->loginAs('teacher1', 'mdp');
		// Add shared LD in mission 1
		$I->amOnPage("/teacher/mission/1");
		$I->click('#at_rep_struct .edit-mission-title');
		$I->scrollTo('#rp_1');
		$I->click('#rp_text_1');
		$I->fillField("#ld_name_" . self::ID_LABDOC_TEACH, self::LD_NAME);
		$I->click('#ld_shared_' . self::ID_LABDOC_TEACH);
		$I->click('#ld_edit_validate_' . self::ID_LABDOC_TEACH);
		$I->wait(1);
		$I->click('#authoring_menu .fa-file');
		$I->switchToNextTab();
		$I->waitForText(self::LD_NAME);
		$I->see(self::LD_SHARED_TAG);
		$I->amOnPage("/teacher/missions");
		$I->typeIn('#private-missions .search-input', "mission1");
		$I->click('#my-missions tr[role=row] td');
		$I->click('.fa-file-circle-check');
		$I->switchToNextTab();
		$I->waitForText(self::LD_NAME);
		$I->see(self::LD_SHARED_TAG);
		$I->logout();

		// Login student 1 edit
		$I->loginAs('student1', 'mdp');
		$I->click("#encours .menu_mission_item"); // cannot click() on text, because the HTML is misleading
		$I->waitForElementVisible("#cadre_mission");
		$I->see("Mission première");
		$I->click("Continuer");
		$I->waitForElement('#widgets_column');
		$I->seeILeftTrace(Trace::ENTER_REPORT);
		$I->dontSeeSpecials();
		$I->see("Mission première", '#lb_mission_name');
		$I->see(self::LD_NAME);
		$I->see(self::LD_SHARED_TAG);
		$I->executeJS('editLD(' . self::ID_LABDOC_STUD . ')');
		$I->waitForElement("#ld_txt_" . self::ID_LABDOC_STUD . "_ifr");
		$I->switchToIFrame("#ld_txt_" . self::ID_LABDOC_STUD . "_ifr");
		$I->executeJS('document.getElementById("tinymce").innerHTML = "<p>' . self::STUDENT_1_TEXT . '</p>";');
		$I->switchToIFrame();
		$I->click('div[title="Enregistrer une nouvelle version du labdoc"]');
		$I->logout();

		// Login Student 3 see Student 1 text
		$I->loginAs('student3', 'mdp');
		$I->click("#encours .menu_mission_item"); // cannot click() on text, because the HTML is misleading
		$I->waitForElementVisible("#cadre_mission");
		$I->see("Mission première");
		$I->click("Continuer");
		$I->waitForElement('#widgets_column');
		$I->seeILeftTrace(Trace::ENTER_REPORT);
		$I->dontSeeSpecials();
		$I->see("Mission première", '#lb_mission_name');
		$I->see(self::LD_NAME);
		$I->see(self::LD_SHARED_TAG);
		$I->see(self::STUDENT_1_TEXT);

		// Database checks
		// First labdoc is shared, no origin no report no teamconfig
		$I->seeInDatabase('labdoc', ['id_labdoc' => self::ID_LABDOC_TEACH, 'id_report_part' => 1, 'id_report' => null, 'id_ld_origin' => null, 'id_team_config' => null, 'shared' => 1]);
		// Second labdoc is shared in teamconfig with origin, no id_report
		$I->seeInDatabase('labdoc', ['id_labdoc' => self::ID_LABDOC_STUD, 'id_report_part' => 1, 'id_report' => null, 'id_ld_origin' => self::ID_LABDOC_TEACH, 'id_team_config' => 1, 'shared' => 1]);
		// third and fourth labdoc are in solution / test report, so no shared, origin a report and no teamconfig
		$I->seeInDatabase('labdoc', ['id_labdoc' => self::ID_LABDOC_STUD + 1, 'id_report_part' => 1, 'id_report' => 8, 'id_ld_origin' => self::ID_LABDOC_TEACH, 'id_team_config' => null, 'shared' => 0]);
		$I->seeInDatabase('labdoc', ['id_labdoc' => self::ID_LABDOC_STUD + 2, 'id_report_part' => 1, 'id_report' => 9, 'id_ld_origin' => self::ID_LABDOC_TEACH, 'id_team_config' => null, 'shared' => 0]);
	}
}
