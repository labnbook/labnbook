<?php

use App\Trace;

class CollabDashboardCest
{
	private const EMPTY_ID_REPORT = 1;
	private const NON_EMPTY_ID_REPORT = 2;
	private const ID_LABDOC = 1;
	
	public function _before(AcceptanceTester $I) {
		// write some text in a labdoc text to generate a trace
		$I->loginAs('student1', 'mdp');
		$I->amOnPage("/report/" . self::NON_EMPTY_ID_REPORT);
		$I->waitForElementNotVisible('.labdoc-content-loading', 2);
		$I->executeJS("editLD(" . self::ID_LABDOC . ");");
		$I->waitForElementVisible("iframe#ld_txt_" . self::ID_LABDOC . "_ifr", 2);
		$I->switchToIFrame("iframe#ld_txt_" . self::ID_LABDOC . "_ifr");
		$I->waitForElementVisible("body#tinymce", 2);
		$I->typeIn("body#tinymce", " some new text to generate trace");
		$I->switchToFrame();
		$I->executeJS("validateLD(" . self::ID_LABDOC . ");");
		$I->executeJS("logout();");
	}
	
	public function testCollabDashboardIsEmpty(AcceptanceTester $I)
	{
		$I->wantTo("See an error message when collaborative dashboard is not available");
		$I->loginAs('teacher1', 'mdp');
		$I->amOnPage("/teacher/reports");
		$I->click("tr#report-" . self::EMPTY_ID_REPORT);
		$I->waitForElementVisible("tr.expanded div.collaborative-dashboard-button-container button", 2);
		$I->click("tr.expanded div.collaborative-dashboard-button-container button");
		$I->waitForElementVisible("div.alert-danger", 2);
	}

	public function testCollabDashboardIsNotEmpty(AcceptanceTester $I)
	{
		$I->wantTo("See a collaborative dashboard button when clicking on a report row from teacher/reports");
		$I->loginAs('teacher1', 'mdp');
		$I->amOnPage("/teacher/reports");
		$I->click("tr#report-" . self::NON_EMPTY_ID_REPORT);
		$I->waitForElementVisible("tr.expanded div.collaborative-dashboard-button-container button", 2);
		$I->click("tr.expanded div.collaborative-dashboard-button-container button");
		$I->waitForElementVisible("div.collaborative-dashboard-container div.plot-container", 2);
	}

	public function testCollabDashboardLeavesTrace(AcceptanceTester $I)
	{
		$I->wantTo("See a trace 55 when clicking on the collaborative dashboard button");
		self::testCollabDashboardIsNotEmpty($I);
		$I->seeILeftTrace(Trace::TEACHER_OPEN_COLLABORATIVE_DASHBOARD);
	}
}
