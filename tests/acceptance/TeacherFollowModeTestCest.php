<?php

class TeacherFollowModeTestCest
{
	private function anotherDataset(AcceptanceTester $I)
    {
		$I->loginAs('student3', 'mdp');
		$I->amOnPage("/report/3?");
		$I->executeJs("addLD(1, 'dataset')");
		$ld_number = 6; //count($I->grabMultiple("[id^=labdoc_]"));
		$I->typeIn("#ld_name_" . $ld_number, "titre dataset");
		$I->executeJs("validateLD(" . $ld_number . ")");
		$I->logout();
	}

	public function modeTest(AcceptanceTester $I)
	{
		$I->wantTo("Test if a teacher can open a dataset in mode follow");
		$this->anotherDataset($I);
		$I->loginAs('teacher1', 'mdp');
		$I->amOnPage("/report/3?sc=follow");
		// Two datasets with test mode enabled
		$I->seeNumberOfElements("[id^=ld_test_without_saving_]", 2);
		// We are not in freeze mode
		$I->assertEquals(false, $I->executeJS('return window.global_stop_sync'));
		// Open the first one and check the class is correct
		$I->click(['css' => "#ld_test_without_saving_5"]);
		$I->assertEquals(true, $I->executeJS('return $("#ld_test_without_saving_5").is(":hidden")'));
		$I->assertEquals(true, $I->executeJS('return $("#ld_exit_test_without_saving_5").is(":visible")'));
		// Open another dataset in test mode
		$I->click("div.alert .alert-close");
		$I->scrollTo("#ld_name_6");
		$I->click(['css' => "#ld_test_without_saving_6"]);
		$I->assertEquals(true, $I->executeJS('return $("#ld_exit_test_without_saving_6").is(":visible")'));
		// Return back to read-only mode
		$I->click("div.alert .alert-close");
		$I->scrollTo("#ld_name_5", 0, -500);
		$I->click(['css' => "#ld_exit_test_without_saving_5"]);
		$I->assertEquals(true, $I->executeJS('return $("#ld_test_without_saving_5").is(":visible")'));
	}

}
