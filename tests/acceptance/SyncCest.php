<?php

use Codeception\Attribute\Skip;


/*
 * @license http://www.gnu.org/licenses/gpl-3.0.html  GNU GPL v3
 */

class SyncCest
{
    private $reportId = 3;
    private $partId = 1;
    private $labdocId = 4;
    private $users = ['student3', 'student4'];
    private $textBefore = "Ma question";
    private $textAfter = "labdoc modifié";

    private static function synchronize(AcceptanceTester $I, $reportId)
    {
        $I->executeJS("synchronize($reportId);");
        $I->seeElement(".labdoc-content-loading");
        $I->waitForElementNotVisible(".labdoc-content-loading", 2);
    }

	#[Skip]
    private function syncProcedure(AcceptanceTester $I)
    {
		// TODO need to add trace tests
        $I->wantTo("Synchronize a procedure labdoc between 2 students");
        $I->loginAs($this->users[0], 'mdp');
        $I->amOnPage("/report/{$this->reportId}");
        $I->scrollTo("#labdoc_{$this->labdocId}", 0, -50);
        $I->waitForText($this->textBefore, 3, "#content_question_{$this->labdocId}");
        $I->click("#close_resource");

        $friend = $I->haveFriend($this->users[1]);
        $data = (object) get_object_vars($this);
        $friend->does(function (AcceptanceTester $I) use ($data) {
            $I->loginAs($data->users[1], 'mdp');
            $I->amOnPage("/report/{$data->reportId}");
            $I->modifyLabdoc($data->labdocId);
            $I->dontSee("id_team_config", "#labdoc_{$data->labdocId}");
            $I->click("#close_resource");
            $I->click("#content_question_{$data->labdocId} .QHPContent.QHPMEditable");
            $I->clearText("#content_question_{$data->labdocId} .QHPContent.QHPMEditable");
            // codecept_debug($I->grabTextFrom("#content_question_{$data->labdocId} .QHPContent.QHPMEditable"));
            $I->dontSee($data->textBefore, "#part_content_{$data->partId}");
            $I->typeIn("#content_question_{$data->labdocId} .QHPContent.QHPMEditable", $data->textAfter);
            $I->click(['css' => '.QHPValidate']);
            $I->executeJS("synchronize({$data->reportId});");
            // modification not submitted (not "validée") but still synchronized
        });

        $I->see($data->textBefore, "#content_question_{$data->labdocId}");
        self::synchronize($I, $data->reportId);
        $I->dontSee($data->textBefore, "#content_question_{$data->labdocId}");
        $I->see($data->textAfter, "#content_question_{$data->labdocId}");
        $I->seeElement("#labdoc_{$data->labdocId} .fa-lock");

        $friend->does(function (AcceptanceTester $I) use ($data) {
            $I->click(['css' => "#labdoc_{$data->labdocId} .fa-check"]);
            $I->see($data->textAfter, "#content_question_{$data->labdocId}");
            // modification submitted ("validée") and synchronized
        });
        $friend->leave();

        $I->seeElement("#labdoc_{$data->labdocId} .fa-lock");
        $I->see($data->textAfter, "#content_question_{$data->labdocId}");
        self::synchronize($I, $data->reportId);
        $I->dontSeeElement("#labdoc_{$data->labdocId} .fa-lock");
        $I->see($data->textAfter, "#content_question_{$data->labdocId}");
    }
}
