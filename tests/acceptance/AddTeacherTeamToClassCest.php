<?php

/**
 * @license http://www.gnu.org/licenses/gpl-3.0.html  GNU GPL v3
 */

/**
 * Description of AddStudentsToClassCest
 *
 * @author Anthony Geourjon <anthony.geourjon@univ-grenoble-alpes.fr>
 */
class AddTeacherTeamToClassCest
{
	// Login as teacher2
	public function _before(AcceptanceTester $I){
		$I->loginAs('teacher2', 'mdp');
		$I->amOnPage("/manager/teachers");
		$I->dontSeeSpecials();
	}

	// Check nb of teacher in class before adding team
	public function checkNbTeacherClassBefore (AcceptanceTester $I){
		$I->amOnPage("/teacher/students");
		$I->click("#classes i.fa-search");
		$I->typeIn("#classes .search-input", "philosophie");
		$I->click("#classes td");
		$I->click("#classes .actions [data-action=edit]");
		$I->seeNumberOfElements("#cl-teachers-list > span", 1);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin('teacher2', 1);
	}
	
	public function checkNbTeacherInTeamBeforeAdding (AcceptanceTester $I){
		$I->amOnPage("/manager/teachers");
		$I->typeIn("#teacherTeams .search-input", "philosophie");
		$I->seeNumberOfElements("#teacherTeams tbody > tr", 1);
		$I->dontSeeSpecials();
		$I->see("Equipe pédago philosophie UGA", "#teacherTeams td");
		$I->click("#teacherTeams td");
		$I->click("#teacherTeams .actions [data-action=edit]");
		$I->seeNumberOfElements("#tt-teachers-list > span", 3);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin('teacher2', 1);
	}
	
	public function addTeacherTeamInClass (AcceptanceTester $I){
		$I->amOnPage("/teacher/students");
		$I->click("#classes i.fa-search");
		$I->typeIn("#classes .search-input", "philosophie");
		$I->click("#classes td");
		$I->click("#classes .actions [data-action=edit]");
		$I->see("Modifier la classe", ".popup header");
		$I->fillSelect2("#cl-add-teacher-or-team", "Equipe pédago philosophie UGA");
		$I->seeNumberOfElements("#cl-teachers-list > span", 3);
		$I->click("#cl-popup-valid");
		$I->waitForElementNotVisible("#cl-popup-valid");
		$I->see("Pierre Teacher3", "#classes .description-row");
		
		$this->addNewTeacherInTeacherTeam($I);
		$this->checkNbTeacherInClassAfterAdding($I);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin('teacher2', 1);
	}
	
	private function addNewTeacherInTeacherTeam(AcceptanceTester $I){
		$I->amOnPage("/manager/teachers");
		$I->typeIn("#teacherTeams .search-input", "philosophie");
		$I->seeNumberOfElements("#teacherTeams tbody > tr", 1);
		$I->see("Equipe pédago philosophie UGA", "#teacherTeams td");
		$I->click("#teacherTeams td");
		$I->click("#teacherTeams .actions [data-action=edit]");
		$I->see("Modifier l'équipe pédagogique", ".popup header");
		$I->seeNumberOfElements("#tt-teachers-list > span", 3);
		$I->fillSelect2("#tt-add-teacher", "Teacher1 Yukiko");
		$I->waitForText("Teacher1", 3, "#tt-teachers-list");
		$I->seeNumberOfElements("#tt-teachers-list > span", 4);
		$I->dontSeeSpecials();
		$I->click("#tt-popup-valid");
		$I->click("#tt-add-popup-valid");
	}
	
	private function checkNbTeacherInClassAfterAdding (AcceptanceTester $I){
		$I->amOnPage("/teacher/students");
		$I->click("#classes i.fa-search");
		$I->typeIn("#classes .search-input", "philosophie");
		$I->click("#classes td");
		$I->click("#classes .actions [data-action=edit]");
		$I->seeNumberOfElements("#cl-teachers-list > span", 4);
	}
}
