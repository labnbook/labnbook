<?php

use App\Trace;

class FitexCest
{
	private $reportId = 3;
	private $labdocId = 5;

	public function _before(AcceptanceTester $I) {
		$I->loginAs('student3', 'mdp');
		$I->amOnPage("/report/{$this->reportId}");
		$I->dontSeeSpecials();
		$I->modifyLabdoc($this->labdocId);

		$I->waitForElement("#labdoc_{$this->labdocId} tr.fx_default_row", 1);
		$I->seeNumberOfElements("#labdoc_{$this->labdocId} tr.fx_default_row", 5);
		$I->seeNumberOfElements("#labdoc_{$this->labdocId} th.fx_col_menu", 4);
	}

	private function clickImport($I) {
		$I->click(".context-menu-item:nth-child(5)");
	}

	public function importReplaceOds(AcceptanceTester $I){
		$I->wantTo("Import an ODS that replaces data (2×3)");


		$I->click("#labdoc_{$this->labdocId} .fx_main_menu");
		$I->see("Importer des données...", ".context-menu-item");
		$this->clickImport($I);

		$I->see("Importer un fichier de données");
		$I->checkOption("has_header");
		$I->seeOptionIsSelected('.fx_import_div [name="mode"]', "remplacer tout");
		$I->runAndActOnPopup('attachFile', ['.fx_import_div [type="file"]', "fitex_sample-data.ods"], true);

		$I->dontSeeElement('.fx_import_div');
		$I->seeNumberOfElements("#labdoc_{$this->labdocId} th.fx_col_menu", 2);
		$I->see("nommage", '.fx_header_name');
		$I->see("Nom_nommage", '.fx_col_header');
		$I->seeNumberOfElements("#labdoc_{$this->labdocId} tr.fx_default_row", 3);

		$I->executeJS("validateLD({$this->labdocId})");
		$I->waitForElementNotVisible('.labdoc-content-loading', 2);
		$I->waitForElement("#labdoc_{$this->labdocId} tr.fx_default_row[data-row='2']");
		$I->seeNumberOfElements("#labdoc_{$this->labdocId} tr.fx_default_row", 3);
		
		$matches = $I->seeILeftTrace([Trace::CONNECT_LNB, Trace::ENTER_REPORT, Trace::EDIT_LD, Trace::MODIFY_LD, Trace::VALIDATE_LD], [3]);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("student3", $matches); // 1 for connecting
    }

	/**
	 * @depends importReplaceOds
	 * @param AcceptanceTester $I
	 */
	public function importAppendCsvRight(AcceptanceTester $I)
    {
		$I->wantTo("Import a CSV that adds 2 columns with 9 rows each");

		$I->scrollTo("#labdoc_{$this->labdocId}");
		$I->click(['css' => "#labdoc_{$this->labdocId} .fx_main_menu"]);
		$this->clickImport($I);

		$I->uncheckOption("has_header");
		$I->selectOption('.fx_import_div [name="mode"]', "ajouter en colonnes");
		$I->attachFile('.fx_import_div [type="file"]', "fitex_bad-data.csv");

		$I->dontSeeElement('.fx_import_div');
		$I->seeNumberOfElements("#labdoc_{$this->labdocId} th.fx_col_menu", 4 + 2);
		$I->see("data1", '.fx_header_name');
		$I->see("data6", '.fx_header_name');
		$I->seeNumberOfElements("#labdoc_{$this->labdocId} tr.fx_default_row", 9);
		$I->seeNumberOfElements('.fx_editable_cell', 9 * (4 + 2));

		$I->executeJS("validateLD({$this->labdocId})");
		$I->waitForElementNotVisible('.labdoc-content-loading', 2);
		$I->waitForElement("#labdoc_{$this->labdocId}");
		$I->scrollTo("#labdoc_{$this->labdocId}");
		$I->seeNumberOfElements("#labdoc_{$this->labdocId} tr.fx_default_row", 9);

		$matches = $I->seeILeftTrace([Trace::CONNECT_LNB, Trace::ENTER_REPORT, Trace::EDIT_LD, Trace::MODIFY_LD, Trace::VALIDATE_LD], [3]);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("student3", $matches);
    }

	/**
	 * @depends importAppendCsvRight
	 *
	 * @param AcceptanceTester $I
	 */
	public function importAppendCsvBottom(AcceptanceTester $I)
    {
		$I->wantTo("Import a CSV that adds 2 rows");
		$I->click("#labdoc_{$this->labdocId} .fx_main_menu");
		$this->clickImport($I);

		$I->uncheckOption("has_header");
		$I->selectOption('.fx_import_div [name="mode"]', "ajouter en lignes");
		$I->attachFile('.fx_import_div [type="file"]', "fitex_good-data.csv");

		$I->dontSeeElement('.fx_import_div');
		$I->seeNumberOfElements("#labdoc_{$this->labdocId} th.fx_col_menu", 4);
		$I->see("data1", '.fx_header_name');
		$I->dontSee("import1", '.fx_header_name'); // only rows are imported, no columns
		$I->seeNumberOfElements("#labdoc_{$this->labdocId} tr.fx_default_row", 5 + 2);
		$I->seeNumberOfElements('.fx_editable_cell', 4 * (5 + 2));

		$I->executeJS("validateLD({$this->labdocId})");
		$I->waitForElementNotVisible('.labdoc-content-loading', 2);
		$I->waitForElement("#labdoc_{$this->labdocId} tr.fx_default_row");
		$I->seeNumberOfElements("#labdoc_{$this->labdocId} tr.fx_default_row", 5 + 2);

		$matches = $I->seeILeftTrace([Trace::CONNECT_LNB, Trace::ENTER_REPORT, Trace::EDIT_LD, Trace::MODIFY_LD, Trace::VALIDATE_LD], [3]);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("student3", $matches);
    }
}
