<?php
/**
 * Description of TeacherTeacherTeamCest
 * @author Anthony Geourjon <anthony.geourjon@univ-grenoble-alpes.fr>
 */
class TeacherTeacherTeamCest
{
	const NUM_TEACHER_TEAMS_ACTIVE = 3;
	const NUM_TEACHER_TEAMS_ARCHIVE = 1;

	public function _before(AcceptanceTester $I) {
		$I->loginAs('teacher2', 'mdp');
		$I->amOnPage("/manager/teachers");
		// Other test use cl-table-loading but no trace of them
		$I->waitForElementNotVisible('#tt-table-loading .fa-spinner', 3);
		$I->dontSeeSpecials();
	}

	public function viewTeacherTeam(AcceptanceTester $I)
    {
		$I->see("Equipes pédagogiques", ".table-caption");
		$I->waitForElement("#teacherTeams tbody", 1);
		$I->seeNumberOfElements("#teacherTeams tbody > tr", self::NUM_TEACHER_TEAMS_ACTIVE);
		$I->dontSee("Précédent", "#teacherTeams");
		$I->seeILeftGoodNumberOfTracesSinceLastLogin('teacher2', 1);
	}

	public function selectTeacherTeam(AcceptanceTester $I)
    {
		$I->wantTo("select a team");
		$I->seeElement("#teacherTeams .lba-context-icons.actions-inactive");
		$I->click("tr#teacherTeam-1 td"); // by id_team
		$I->see("Teacher2 Alain", "#teacherTeams .description-row");
		$I->dontSeeElement("#teacherTeams .lba-context-icons.actions-inactive");
		$I->seeILeftGoodNumberOfTracesSinceLastLogin('teacher2', 1);
    }

	public function sortTeacherTeam(AcceptanceTester $I)
    {
		// default: sorted by name
		$I->see("Equipe pédago physique UGA", "#teacherTeams tbody > tr:last-child td");
		// sort so that the last class has the most students
		// Two times for desc sorting
		$I->click("#teacherTeams [title=\"Trier par nombre d'enseignants\"]");
		$I->click("#teacherTeams [title=\"Trier par nombre d'enseignants\"]");
		$I->see("Equipe pédago chimie UGA", "#teacherTeams tbody > tr:last-child td");
		$I->see("1", "#teacherTeams tbody > tr:last-child td:last-child");
		$I->seeILeftGoodNumberOfTracesSinceLastLogin('teacher2', 1);
	}

	public function filterTeacherTeam(AcceptanceTester $I)
    {
		$I->checkOption("archives");
		$I->seeNumberOfElements("#teacherTeams tbody > tr", self::NUM_TEACHER_TEAMS_ACTIVE + self::NUM_TEACHER_TEAMS_ARCHIVE);
		$I->uncheckOption("archives");
		$I->seeNumberOfElements("#teacherTeams tbody > tr", self::NUM_TEACHER_TEAMS_ACTIVE);
		$I->typeIn("#teacherTeams .search-input", "maths");
		$I->seeNumberOfElements("#teacherTeams tbody > tr", 1);
		$I->clearText("#teacherTeams .search-input");
		$I->seeNumberOfElements("#teacherTeams tbody > tr", self::NUM_TEACHER_TEAMS_ACTIVE);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin('teacher2', 1);
	}

	public function addTeacherTeam(AcceptanceTester $I)
    {
		$I->wantTo("add a new teacher team");
		$I->dontSee("Ajouter une équipe pédagogique", ".popup header");
		$I->click(['css' => "#teacherTeams .actions [data-action=add]"]);
		$I->seeElement('.popup header');
		$I->see("Ajouter une équipe", ".popup header");
		$I->selectOption(".popup #tt-add-inst", "UGA");
		$I->fillField("#tt-add-name", "Equipe pédago sport UGA");
		$I->see("- Teacher2 Alain ", "#tt-teachers-list span");
		$I->click(['css' => "#tt-popup .popup-submit"]);
		$I->waitForElementNotVisible('#tt-popup .popup header');
		$I->waitForElementNotVisible('#tt-table-loading .fa-spinner', 2);
		$I->see("Equipe pédago sport UGA", "#teacherTeams td");
		$I->seeILeftGoodNumberOfTracesSinceLastLogin('teacher2', 1);
    }

	public function deleteTeacherTeam(AcceptanceTester $I)
    {
		$I->wantTo("delete a teacher team");
		$I->typeIn("#teacherTeams .search-input", "physique");
		$I->seeNumberOfElements("#teacherTeams tbody > tr", 1);
		$I->see("Equipe pédago physique UGA", "#teacherTeams td");
		$I->click("#teacherTeams td");
		$I->click("#teacherTeams .actions [data-action=archive]");

		// Wait for archive / delete popup
		$I->waitForElementVisible("#archive-popup");
		// Select delete radio button
		$I->click("#archive-popup #delete-input");
		$I->click("#archive-popup .popup-submit");
		
		$I->seeILeftGoodNumberOfTracesSinceLastLogin('teacher2', 1);
    }

	public function modifyTeacherTeam(AcceptanceTester $I)
    {
		$I->wantTo("modify the attributes of a team");
		$I->typeIn("#teacherTeams .search-input", "chimie");
		$I->click("tr#teacherTeam-1 td");
		$I->waitForElementNotVisible(".popup header");
		$I->dontSeeSpecials();
		$I->click("#teacherTeams .actions [data-action=edit]");
		$I->see("Modifier l'équipe pédagogique", ".popup header");
		$I->see("- Teacher2 Alain", ".popup"); // assigned teachers
		$I->fillSelect2("#tt-add-teacher", "Teacher1 Yukiko");
		$I->click("#tt-popup-valid");
		$I->waitForElementNotVisible(".popup header");
		$I->see("Teacher2 Alain", "#teacherTeams .description-row");
		$I->seeILeftGoodNumberOfTracesSinceLastLogin('teacher2', 1);
    }
    
    public function addingTeacherInTeamLinkedToMission (AcceptanceTester $I){
		$I->wantTo("Add a teacher in a team already linked on a mission");
		$I->typeIn("#teacherTeams .search-input", "philosophie");
		$I->click("tr#teacherTeam-4 td"); 
		$I->waitForElementNotVisible(".popup header");
		$I->click("#teacherTeams .actions [data-action=edit]");
		$I->see("Modifier l'équipe pédagogique", ".popup header");
		$I->see("- Teacher2 Alain", ".popup"); // assigned teachers
		$I->fillSelect2("#tt-add-teacher", "Teacher1 Yukiko");
		$I->click("#tt-popup-valid");
		//$I->waitForElementNotVisible("#tt-add-popup");
		$I->see("Teacher1 Yukiko", "#tt-add-teachers-message");
		$I->click("#tt-add-popup-valid");
		$I->dontSeeElement( "#tt-add-teachers-message");

		// Check in mission wiew if link is ok
		$I->amOnPage("teacher/mission/6");
		$I->click('#at_rep_param .edit-mission-title');
		$I->see("Teacher1 Yukiko", "#linked_teachers");
		$I->seeILeftGoodNumberOfTracesSinceLastLogin('teacher2', 1);
	}
	
	public function addingTeacherInTeamLinkedToMissionWithoutLink (AcceptanceTester $I){
		$I->wantTo("Add a teacher in a team already linked on a mission without add link");
		$I->typeIn("#teacherTeams .search-input", "philosophie");
		$I->click("tr#teacherTeam-4 td");
		$I->waitForElementNotVisible(".popup header");
		$I->click("#teacherTeams .actions [data-action=edit]");
		$I->see("Modifier l'équipe pédagogique", ".popup header");
		$I->fillSelect2("#tt-add-teacher", "Teacher8 Henri");
		$I->click("#tt-popup-valid");
		//$I->waitForElementNotVisible("#tt-add-popup");
		$I->see("Teacher8 Henri", "#tt-add-teachers-message");
		$I->click("#tt-add-popup-cancel");
		$I->dontSeeElement( "#tt-add-teachers-message");

		// Check in mission wiew if link is ok
		$I->amOnPage("teacher/mission/6");
		$I->click('#at_rep_param .edit-mission-title');
		$I->dontSee("Teacher8 Henri", "#linked_teachers");
		$I->seeILeftGoodNumberOfTracesSinceLastLogin('teacher2', 1);
	}

	public function addingTeacherInTeamLinkedToClass (AcceptanceTester $I){
		$I->wantTo("Add a teacher in a team already linked on a class");
		$I->typeIn("#teacherTeams .search-input", "philosophie");
		$I->click("tr#teacherTeam-4 td");
		$I->waitForElementNotVisible(".popup header");
		$I->click("#teacherTeams .actions [data-action=edit]");
		$I->see("Modifier l'équipe pédagogique", ".popup header");
		$I->see("- Teacher2 Alain", ".popup"); // assigned teachers
		$I->fillSelect2("#tt-add-teacher", "Teacher1 Yukiko");
		$I->click("#tt-popup-valid");
		//$I->waitForElementNotVisible("#tt-add-popup");
		$I->see("Teacher1 Yukiko", "#tt-add-teachers-message");
		$I->click("#tt-add-popup-valid");
		$I->dontSeeElement( "#tt-add-teachers-message");

		// Check in mission wiew if link is ok
		$I->amOnPage("teacher/students");
		$I->click("#classes i.fa-search");
		$I->typeIn("#classes .search-input", "philosophie");
		$I->click("#classes td");
		$I->click("#classes .actions [data-action=edit]");
		$I->see("Teacher1 Yukiko", "#cl-teachers-list");
		$I->seeILeftGoodNumberOfTracesSinceLastLogin('teacher2', 1);
	}
	
	public function addingTeacherInTeamLinkedToClassWithoutLink (AcceptanceTester $I){
		$I->wantTo("Add a teacher in a team already linked on a class without add link");
		$I->typeIn("#teacherTeams .search-input", "philosophie");
		$I->click("tr#teacherTeam-4 td");
		$I->waitForElementNotVisible(".popup header");
		$I->click("#teacherTeams .actions [data-action=edit]");
		$I->see("Modifier l'équipe pédagogique", ".popup header");
		$I->fillSelect2("#tt-add-teacher", "Teacher8 Henri");
		$I->click("#tt-popup-valid");
		//$I->waitForElementNotVisible("#tt-add-popup");
		$I->see("Teacher8 Henri", "#tt-add-teachers-message");
		$I->click("#tt-add-popup-cancel");
		$I->dontSeeElement( "#tt-add-teachers-message");

		// Check in mission wiew if link is ok
		$I->amOnPage("teacher/students");
		$I->click("#classes i.fa-search");
		$I->typeIn("#classes .search-input", "philosophie");
		$I->click("#classes td");
		$I->click("#classes .actions [data-action=edit]");
		$I->dontSee("Henri Teacher8", "#cl-teachers-list");
		$I->seeILeftGoodNumberOfTracesSinceLastLogin('teacher2', 1);
	}
}
