<?php

use App\Trace;

class TeacherMissionListCest
{
	public function _before(AcceptanceTester $I) {
		$I->loginAs('teacher1', 'mdp');
		$I->amOnPage("/teacher/missions");
		$I->dontSeeSpecials();
	}

	public function _after(AcceptanceTester $I) {
		$I->logout();
	}

	public function viewPrivateMissions(AcceptanceTester $I)
    {
		$privateMissions = "#private-missions";
		$I->see("Missions personnelles", "$privateMissions .table-caption");

		$I->waitForElement("$privateMissions tbody", 1);
		$I->seeNumberOfElements("$privateMissions tbody tr", 3);
		$I->dontSee("Précédent", $privateMissions);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("teacher1", 1);
	}

	public function sortPrivateMissions(AcceptanceTester $I)
    {
		$privateMissions = "#private-missions";
		$I->click("$privateMissions [title=\"Trier par code de mission\"]");
		$I->see("mission8", "$privateMissions tbody > tr:last-child td");
		$I->click("$privateMissions [title=\"Trier par nom de mission\"]");
		$I->see("mission1", "$privateMissions tbody > tr:last-child td");
		$I->click("$privateMissions [title=\"Trier par nom de mission\"]");
		$I->see("mission3", "$privateMissions tbody > tr:last-child td");
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("teacher1", 1);
	}

	public function filterPrivateMissions(AcceptanceTester $I)
    {
		$privateMissions = "#private-missions";
		$I->typeIn("$privateMissions .search-input", "premier");
		$I->seeNumberOfElements("$privateMissions tbody tr", 1);
		$I->dontSee("mission3", $privateMissions);
		$I->see("mission1", $privateMissions);
		$I->clearText("$privateMissions .search-input");
		$I->see("mission3", $privateMissions);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("teacher1", 1);
	}

	public function openPrivateMissions(AcceptanceTester $I)
    {
		$privateMissions = "#private-missions";
		$I->typeIn("$privateMissions .search-input", "vide");
		$I->dontSee("La troisième mission est vide", $privateMissions);
		$I->click("$privateMissions tbody td");
		$I->see("La troisième mission est vide", $privateMissions);
		$I->click("$privateMissions [title=\"Tester la mission (nouvel onglet)\"]");
		$I->switchToNextTab();
		//$I->click('#close_resource');
		$I->see("mission 3 vide", '#lb_mission_name');
		$I->closeTab();
		$I->seeILeftTrace(Trace::TEACHER_TEST_MISSION);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin("teacher1", 2);
    }

	/*
	// TODO Add test data for public missions
	public function testPublicMissions(AcceptanceTester $I)
    {
		$publicMissions = "#public-missions";
		$I->see("Missions publiques", "$publicMissions .table-caption");
		$I->seeNumberOfElements("$publicMissions tbody tr", 2);
    }

	public function sortPublicMissions(AcceptanceTester $I)
    {
		$publicMissions = "#public-missions";
		$I->waitForElement("$publicMissions tbody", 1);
		$I->click("$publicMissions [title=\"Trier par code de mission\"]");
		$I->see("Demo", "$publicMissions tbody > tr:first-child td");
		$I->click("$publicMissions [title=\"Trier par code de mission\"]");
		$I->see("pKaBBT", "$publicMissions tbody > tr:first-child td");
    }

	public function filterPublicMissions(AcceptanceTester $I)
    {
		$publicMissions = "#public-missions";
		$I->typeIn("$publicMissions .search-input", "Demo");
		$I->seeNumberOfElements("$publicMissions tbody tr", 1);
		$I->dontSee("pKaBBT", $publicMissions);
		$I->see("en France et au Japon", $publicMissions);
		$I->clearText("$publicMissions .search-input");
		$I->see("pKaBBT", $publicMissions);
    }

	public function openPublicMissions(AcceptanceTester $I)
    {
		$publicMissions = "#public-missions";
		$I->dontSee("L'objectif de ce TP", $publicMissions);
		$I->typeIn("$publicMissions .search-input", "pka");
		$I->click("$publicMissions tbody tr:first-child td");
		$I->see("L'objectif de ce TP", $publicMissions);
		$I->click('[title="Tester la mission (nouvel onglet)"]', $publicMissions);
		$I->switchToNextTab();
		$I->see("pKaBBT : Diagramme de prédominance d'un indicateur coloré", '#report-header-title');
		$I->closeTab();

		$I->seeElement("#ressource");
		$I->click("#close_resource");
		$I->dontSeeElement("#ressource");
    }
	*/
}
