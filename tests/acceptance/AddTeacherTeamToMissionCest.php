<?php

use App\Trace;
/**
 * @license http://www.gnu.org/licenses/gpl-3.0.html  GNU GPL v3
 */

/**
 * Description of AddTeacherTeamToMissionCest
 * @author Anthony Geourjon <anthony.geourjon@univ-grenoble-alpes.fr>
 */
class AddTeacherTeamToMissionCest
{
	// Login as teacher2
	public function _before(AcceptanceTester $I){
		$I->loginAs('teacher2', 'mdp');
		$I->amOnPage("/teacher/missions");
		$I->dontSeeSpecials();
	}

	// Check nb of teacher in class before adding team
	public function checkNbTeacherMissionBefore (AcceptanceTester $I){
		$I->amOnPage("teacher/mission/5");
		$I->click('#at_rep_param .edit-mission-title');
		$I->seeNumberOfElements("#linked_teachers > div", 1);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin('teacher2', 1);
	}
	
	public function checkNbTeacherInTeamBeforeAdding (AcceptanceTester $I){
		$I->amOnPage("/manager/teachers");
		$I->typeIn("#teacherTeams .search-input", "physique");
		$I->seeNumberOfElements("#teacherTeams tbody > tr", 1);
		$I->see("Equipe pédago physique UGA", "#teacherTeams td");
		$I->click("#teacherTeams td");
		$I->click("#teacherTeams .actions [data-action=edit]");
		$I->seeNumberOfElements("#tt-teachers-list > span", 2);
		$I->dontSeeSpecials();
		$I->seeILeftGoodNumberOfTracesSinceLastLogin('teacher2', 1);
	}
	
	public function addTeacherTeamInMission (AcceptanceTester $I){
		$I->amOnPage("teacher/mission/5");
		$I->click('#at_rep_param .edit-mission-title');
		$I->fillSelect2("#mi-add-teacher", "Equipe pédago physique UGA");
		//$I->click("#mi-add-teacher");
		$I->waitForText('Yukiko', 3, '#linked_teachers');
		$I->seeNumberOfElements("#linked_teachers > div", 2);
		$I->see("Teacher1 Yukiko", "#linked_teachers");
		
		$this->addNewTeacherInTeacherTeam($I);
		$this->checkNbTeacherInMissionAfterAdding($I);
		$I->seeILeftTrace(Trace::TEACHER_MODIFY_MISSION);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin('teacher2', 2);
	}

	private function addNewTeacherInTeacherTeam(AcceptanceTester $I){
		$I->amOnPage("/manager/teachers");
		$I->typeIn("#teacherTeams .search-input", "physique");
		$I->seeNumberOfElements("#teacherTeams tbody > tr", 1);
		$I->see("Equipe pédago physique UGA", "#teacherTeams td");
		$I->click("#teacherTeams td");
		$I->click("#teacherTeams .actions [data-action=edit]");
		$I->see("Modifier l'équipe pédagogique", ".popup header");
		$I->seeNumberOfElements("#tt-teachers-list > span", 2);
		$I->fillSelect2("#tt-add-teacher", "Teacher3 Pierre");
		$I->seeNumberOfElements("#tt-teachers-list > span", 3);
		$I->click("#tt-popup-valid");
		$I->click("#tt-add-popup-valid");
	}

	
	private function checkNbTeacherInMissionAfterAdding (AcceptanceTester $I){
		$I->amOnPage("teacher/mission/5");
		$I->click('#at_rep_param .edit-mission-title');
		$I->seeNumberOfElements("#linked_teachers > div", 3);
	}
	
}
