<?php

use Codeception\Attribute\Skip;

class UserProfileCest
{
	public function _after(AcceptanceTester $I) {
		$I->logout();
	}

	public function updatePassLocal(AcceptanceTester $I)
    {
		$I->wantTo("update the profile (change password)");
		$I->loginAs('student1', 'mdp');
		$I->see("Mon compte", '#header');
		$I->dontSeeSpecials();
		$I->click('#header button#my-account');
		$I->see("Informations personnelles");
		$I->dontSeeSpecials();
		$I->see("Identifiant");
		$I->seeInField("#modal-account input[name=c_login]", "student1");

		$I->see("Mot de passe", "#modal-account label");
		$I->seeElement("#modal-account input[name=prev_pwd]", ['placeholder' => 'inchangé']);
		$I->dontSee("Nouveau mot de passe", "#modal-account label");
		$I->click("#modal-account button[title='Modifier le mot de passe']");
		$I->see("Nouveau mot de passe", "#modal-account label");
		$I->seeElement("#modal-account input[name=prev_pwd]", ['placeholder' => 'nécessaire']);

		$I->fillField("input[name=c_pwd]", "Pas de passe");
		$I->fillField("input[name=c_pwd_2]", "Pas de passe");
		$I->click("button.modal-submit");
		$I->see("vous devez indiquer votre ancien mot de passe", ".modal-alert-message");

		$I->fillField('#modal-account input[name=prev_pwd]', "mdp");
		$I->click("button.modal-submit");
		$I->waitForText("Mise à jour effectuée", 3, ".alert-success");
		$I->dontSee("Modifier un compte existant", "#modal-account");

		$I->logout();
		$I->seeILeftGoodNumberOfTracesSinceLastLogin('student1', 2);
		$I->loginAs('student1', "Pas de passe");
		$I->see("Mon compte", '#header');
	}

	public function updateLoginLocal(AcceptanceTester $I)
    {
		$I->wantTo("update the profile (change login)");
		$I->loginAs('student1', 'mdp');
		$I->see("Mon compte", '#header');
		$I->click('#header button#my-account');
		$I->see("Informations personnelles", "#modal-account");
		$I->see("Identifiant", "#modal-account label");
		$I->seeInField("#modal-account input[name=c_login]", "student1");

		$I->see("Mot de passe", "#modal-account label");
		$I->seeElement("#modal-account input[name=prev_pwd]", ['placeholder' => 'inchangé']);
		$I->seeElement("#modal-account input[name=c_login]", ['disabled' => 'true']);
		$I->click("#modal-account i[id=mod_login]");
		$I->seeElement("#modal-account input[name=prev_pwd]", ['placeholder' => 'nécessaire']);

		$I->fillField("input[name=c_login]", "student10");
		$I->click("button.modal-submit");
		$I->see("Mot de passe requis pour changer l'identifiant", ".modal-alert-message");

		$I->fillField('#modal-account input[name=prev_pwd]', "mdp");
		$I->click("button.modal-submit");
		$I->waitForText("Mise à jour effectuée", 3, ".alert-success");
		$I->dontSee("Modifier un compte existant", "#modal-account");

		$I->logout();
		$I->loginAs('student10', "mdp");
		$I->see("Mon compte", '#header');
	}

	public function switchToCasWithoutMerge(AcceptanceTester $I)
    {
		$I->wantTo("switch an UGA user to CAS authentication");
		$I->updateInDatabase('user', ['pwd' => 'f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17', 'first_name' => 'bibi'], ['login' => 'user-CAS']);
		$I->loginAs('user-CAS', 'mdp');
		$I->waitForElementVisible("#my-account");
		$I->click("#my-account");
		$I->seeInField("#modal-account input[name=c_login]", "user-CAS");
		$I->see("Utiliser votre compte institutionnel", "#modal-account");

		$I->click("#c_choose_cas_authentication");
		$I->clickAndAcceptPopup("button.modal-submit");

		$I->seeInDatabase('user', ['login' => 'user-CAS', 'pwd' => null ]);
		
		
		$I->seeElement('.alert-info');
		$I->see("La connexion à votre compte LabNBook se fera désormais par l'annuaire institutionnel", '.alert-info');
		$I->see("Vos informations personnelles ont été mises à jour", '.alert-info'); // requires LDAP
		$I->amOnPage('/reports');
		$I->waitForElementVisible("#my-account");
		$I->click("#my-account");
		$I->seeElement('input[disabled][name=c_first_name][value=Masako]'); // requires LDAP
		$I->see("Vous utilisez votre compte institutionnel UGA pour vous connecter", '#modal-account .comment');
		$I->seeILeftGoodNumberOfTracesSinceLastLogin('user-CAS', 1);
    }

	private function openMergeDialog(AcceptanceTester $I, $username)
	{
		if (strpos($username, 'teacher') !== False) {
			$I->click("#lb_auth_menu_menu");
			$I->click("#edit_account_btn");
		} else {
			$I->click("Mon compte");
		}
		$I->seeInField("#modal-account input[name=c_login]", $username); 
		$I->see("Utiliser votre compte institutionnel", "#modal-account");

		$I->click("#c_choose_cas_authentication");
		$I->clickAndAcceptPopup("button.modal-submit");

		$I->waitForText("Vous avez déjà un compte LabNBook accessible depuis l'annuaire institutionnel", 3, ".alert-info");
		$I->see("n cochant cette case vous acceptez que vos deux comptes soient fusionnés");
	}

	private function seeLinksInDB(AcceptanceTester $I, $links, $old_id) {
		foreach ($links['deletes'] as $table) {
			$I->dontSeeInDatabase($table, ['id_user' => $old_id]);
		}
		foreach ($links['updates'] as $table => $entries) {
			foreach($entries as $row) {
				$I->seeInDatabase($table, $row);
			}
		}
	}

	public function switchToCasWithMergeTeacher(AcceptanceTester $I)
    {
		$I->wantTo("Merge a teacher to an existing CAS user");
		// First try and cancel
		$I->loginAs('teacher1', 'mdp');
		$this->openMergeDialog($I, "teacher1");
		// Close alert
		$I->click('div.alert .alert-close');
		$I->click('div.alert .alert-close');
		$I->click("a.popup-cancel");
		$I->logout();

		// Insert - update data for conflicts
		$new_id_teacher = 3;
		$old_id = 2;
		// Create a teacher to force a merge
		$I->updateInDatabase('teacher', ['teacher_domain' => 'toto', 'teacher_level' => 'univ', 'teacher_comment'=> 'tutu'], ['id_teacher' => $new_id_teacher]);
		$I->updateInDatabase('teacher', ['role' => 'manager'], ['id_teacher' => $old_id]);
		// Link a duplicate class
		$I->haveInDatabase('link_class_teacher', ['id_teacher' => $new_id_teacher, 'id_class' => 6]);
		// Add another extplatfor with duplicate links
		$I->haveInDatabase('extplatform', ['id_extplatform' => 4, 'id_inst' => $old_id]);
		$I->haveInDatabase('link_extplatform_user', ['id_user' => $new_id_teacher, 'id_extplatform' => 4, 'id_user_ext' => 45]);
		$I->haveInDatabase('link_extplatform_user', ['id_user' => $old_id, 'id_extplatform' => 4, 'id_user_ext' => 42]);
		// Add link in mission for conflict
		$I->haveInDatabase('link_mission_teacher', ['id_teacher' => $new_id_teacher, 'id_mission' => 1, 'teacher_type' => 'teacher']);
		$I->haveInDatabase('link_teacher_tteam', ['id_teacher' => $new_id_teacher, 'id_teacher_team' => 2, 'status' => 'manager']);
		// Annotation
		$I->haveInDatabase('annotation', [
			'id_annotation' => 1,
			'id_teacher' => $old_id,
			'id_report' => 1,
			'id_report_part' => 1,
			'json' => '{"a":"b"}'
		]);
		// Conversation
		$I->haveInDatabase('conversation', ['id_conversation' => 1,'id_report' => 2,'title' => 'test']);
		$I->haveInDatabase('link_conversation_user', ['id_conversation' => 1,'id_user'=> 1]);
		$I->haveInDatabase('link_conversation_user', ['id_conversation' => 1,'id_user'=> $old_id]);
		// message
		$I->haveInDatabase('message', ['id_user_sender' => $old_id, 'id_conversation' => 1,'msg_content' => 'coucou']);
		$I->haveInDatabase('message', ['id_user_sender' => 1, 'id_conversation' => 1,'msg_content' => 'salut']);
		// Comment
		$I->haveInDatabase('comment', ['id_comment' => 1,'id_user' => $old_id,'id_labdoc' => 1,'content' => 'je commente']);
		$I->haveInDatabase('comment', ['id_comment' => 2,'id_user' => 1,'id_labdoc' => 1,'content' => 'tu commente']);

		// Second try actual merge user should still exists
		$I->loginAs('teacher1', 'mdp');
		$this->openMergeDialog($I, "teacher1");
		$I->click("#confirmation-popup-confirm-ok");
		$I->click("a.popup-submit");
		$I->waitForText("La connexion se fera désormais par le compte institutionnel CAS");
		$I->dontSee("Impossible de fusionner");
		// I should be teacher
		// Should not be Masako but teacher menu
		$I->seeElement("#authoring_menu");
		$I->dontSee("Masako");
		$links = [
			'deletes' => [
				'message_var',
				'user'
			],
			'updates' => [
				'teacher' => [
					['id_teacher' => $new_id_teacher, 'teacher_domain' => 'toto Biologie', 'teacher_level' => 'univ', 'teacher_comment' => 'tutu', 'role' => 'manager']
				],
				'link_class_teacher' => [
					['id_teacher' => $new_id_teacher, 'id_class' => 1],
					['id_teacher' => $new_id_teacher, 'id_class' => 2],
					['id_teacher' => $new_id_teacher, 'id_class' => 3],
					['id_teacher' => $new_id_teacher, 'id_class' => 4],
					['id_teacher' => $new_id_teacher, 'id_class' => 5],
					['id_teacher' => $new_id_teacher, 'id_class' => 6],
					['id_teacher' => $new_id_teacher, 'id_class' => 7],
				],
				'link_extplatform_user' => [
					['id_user' => $new_id_teacher, 'id_extplatform' => 3, 'id_user_ext' => 45],
					['id_user' => $new_id_teacher, 'id_extplatform' => 4, 'id_user_ext' => 45],
					['id_user' => $new_id_teacher, 'id_extplatform' => 4, 'id_user_ext' => 42]
				],
				'link_inst_teacher' => [
					['id_teacher' => $new_id_teacher, 'id_inst' => 2]
				],
				'link_class_learner' => [
					// No Data
				],
				'link_mission_teacher' => [
					['id_teacher' => $new_id_teacher, 'id_mission' => 1, 'teacher_type' => 'designer'],
					['id_teacher' => $new_id_teacher, 'id_mission' => 3, 'teacher_type' => 'designer'],
					['id_teacher' => $new_id_teacher, 'id_mission' => 8, 'teacher_type' => 'designer']
				],
				'link_teacher_tteam' => [
					['id_teacher' => $new_id_teacher, 'id_teacher_team' => 2, 'status' => 'manager']
				],
				'trace' => [
					// No test here
				],
				'labdoc' => [
					// No Data
				],
				'labdoc_status' => [
					// No Data
				],
				'annotation' => [
					['id_annotation' => 1, 'id_teacher' => $new_id_teacher]
				],
				'comment' => [
					['id_comment' => 1,'id_user' => $new_id_teacher,'id_labdoc' => 1,'content' => 'je commente'],
					['id_comment' => 2,'id_user' => 1,'id_labdoc' => 1,'content' => 'tu commente']

				],
				'message' => [
					['id_user_sender' => $new_id_teacher, 'id_conversation' => 1,'msg_content' => 'coucou'],
					['id_user_sender' => 1, 'id_conversation' => 1,'msg_content' => 'salut']
				],
				'link_annotation_learner' => [
					// No Data
				],
				'link_conversation_user' => [
					['id_conversation' => 1, 'id_user' => $new_id_teacher],
					['id_conversation' => 1, 'id_user' => 1]
				],
				'link_report_learner' => [
					// No Data
				],
				'teacher_report_status' => [
					// No Data
				]
			]
		];
		$this->seeLinksInDB($I, $links, $old_id);
    }


	#[Skip('Broken as Masako cannot be both a teacher and a student since #1050')]
	public function switchToCasWithMergeStudent(AcceptanceTester $I)
    {
		$I->wantTo("Merge a student to an existing CAS user");
		$I->loginAs('student1', 'mdp');
		$this->openMergeDialog($I, "student1");
		// Close alert
		$I->click('div.alert .alert-close');
		$I->click('div.alert .alert-close');
		$I->click("a.popup-cancel");
		$I->logout();

		$new_user = 3;
		$old_id = 4;
		// Add data
		$I->haveInDatabase('annotation', [
			'id_annotation' => 1,
			'id_teacher' => 1,
			'id_report' => 1,
			'id_report_part' => 1,
			'json' => '{"a":"b"}'
		]);
		$I->haveInDatabase('link_annotation_learner', ['id_annotation' => 1, 'id_user' => $old_id]);
		$I->haveInDatabase('link_annotation_learner', ['id_annotation' => 1, 'id_user' => 5]);

		// Second try actual merge user should still exists
		$I->loginAs('student1', 'mdp');
		$this->openMergeDialog($I, "student1");
		$I->click("#confirmation-popup-confirm-ok");
		$I->click("a.popup-submit");
		$I->waitForText("La connexion se fera désormais par le compte institutionnel CAS");
		$I->dontSee("Impossible de fusionner");
		$I->dontSeeElement("#authoring_menu");
		$I->see("Masako");
		$links = [
			'deletes' => [
				'message_var',
				'user',
			],
			'updates' => [
				'teacher' => [
					// No Data
				],
				'link_class_teacher' => [
					// No Data
				],
				'link_extplatform_user' => [
					// No Data
				],
				'link_inst_teacher' => [
					// No Data
				],
				'link_class_learner' => [
					// No Data
				],
				'link_mission_teacher' => [
					// No Data
				],
				'link_teacher_tteam' => [
					// No Data
				],
				'trace' => [
					// No test here
				],
				'labdoc' => [
					['id_labdoc' => 1, 'last_editor' => $new_user]
				],
				'labdoc_status' => [
					['id_labdoc' => 1, 'id_user' => $new_user]
				],
				'annotation' => [
					// No Data
				],
				'comment' => [
					// No Data
				],
				'message' => [
					// No Data
				],
				'link_annotation_learner' => [
					// No Data
					['id_annotation' => 1, 'id_user' => $new_user],
					['id_annotation' => 1, 'id_user' => 5]

				],
				'link_conversation_user' => [
					// No Data
				],
				'link_report_learner' => [
					['id_report' => 2, 'id_user' => $new_user],
					['id_report' => 6, 'id_user' => $new_user],
				],
				'teacher_report_status' => [
					// No Data
				]
			]
		];
		$this->seeLinksInDB($I, $links, $old_id);
    }

	public function acceptConditions(AcceptanceTester $I)
	{
		$I->loginAs('student1', 'mdp');
		$I->dontSeeElement('.cgu');
		$I->logout();
		$I->updateInDatabase('user', ['cgu_accept_time' => null], ['login' => 'student1']);
		$I->loginAs('student1', 'mdp');
		$I->waitForText('vous acceptez', 3);
		$I->click('.modal-cancel');
		$I->seeCurrentUrlMatches('#/login#');
		$I->loginAs('student1', 'mdp');
		$I->waitForText('vous acceptez', 3);
		$I->click('.modal-submit');
		$I->logout();
		$I->seeILeftGoodNumberOfTracesSinceLastLogin('student1', 2);
		$I->loginAs('student1', 'mdp');
		$I->dontSeeElement('.cgu');
	}
}
