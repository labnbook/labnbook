<?php

use App\Trace;

class CommunicationCest {

	public function createConversationAndSendMesage(AcceptanceTester $I){
		$I->wantTo("Create a chat");
		$I->haveInDatabase('link_report_learner', ['id_user' => 5, 'id_report' => 2]);
		$I->loginAs('student1', 'mdp');
		$I->click("#encours > .menu_mission_item");
		$I->waitForElementVisible("#btn_form_submit");
		$I->click("Continuer");
		//$I->waitForElement('#close_resource');
		$I->seeILeftTrace(Trace::ENTER_REPORT);
		$I->click('#lb_menubar_messagerie button.lnb-widget-menu-btn');
		$I->see('conversation');
		$I->click('#msg_menu_conversations_add');
		$I->see('Titre');
		$I->scrollTo('.msg_participants_list_action');
		$I->dontSee("Ajouter tous les étudiants d'une classe");
		$I->see('Ajouter les enseignants du rapport');
		$I->see('Ajouter tous les étudiants de l\'équipe');
		$I->fillField("#msg_conv_title", "Ma conversation");
		$I->click('button[title="Ajouter tous les étudiants de l\'équipe"]');
		$I->see('Student2', '#msg_participants_list');
		$I->click('#msg_participants_list i.fa-trash-alt');
		$I->dontSee('Student1', '#msg_participants_list');
		$I->click('button[title="Ajouter les enseignants du rapport"]');
		$I->see('Yukiko Teacher1', '#msg_participants_list');
		$I->fillSelect2('#msg_add_participants_select', 'Student2');
		$I->see('Student2', '#msg_participants_list');
		$I->click('button[title=Valider]');
		$I->see('mission1 Ma conversation');
		$I->seeILeftTrace(Trace::ADD_CONVERSATION);
		$I->fillField("#text_msg_send", "Salut");
		$I->click('button[title="Envoyer le message"]');
		$I->seeILeftTrace(Trace::ADD_MESSAGE);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin('student1', 4);
		$I->logout(false);
		$I->loginAs('teacher1', 'mdp');
		$I->click('i[title="Ouvrir la messagerie de LabNBook"]');
		$I->see('mission1 Ma conversation');
		$I->click('#conv_1');
		$I->seeILeftTrace(Trace::OPEN_CONVERSATION);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin('teacher1', 2);
	}
	
	// Tests are not covering conversation right
	public function createConversationAsATeacher(AcceptanceTester $I){
		$I->loginAs('teacher1', 'mdp');
		$I->click('i.fa-envelope');
		$I->click('#msg_menu_conversations_add');
		$I->see("Ajouter tous les étudiants d'une classe");
		$I->dontSee('Ajouter les enseignants du rapport');
		$I->dontSee('Ajouter tous les étudiants de l\'équipe');
		$I->fillSelect2('#msg_add_participants_select', 'Teacher3');
		$I->see('Teacher3', '#msg_participants_list');
		$I->fillSelect2('#msg_add_participants_class', 'classe 1');
		$I->see('Student1', '#msg_participants_list');
		$I->see('Student2', '#msg_participants_list');
		$I->see('Student3', '#msg_participants_list');
		$I->see('Student4', '#msg_participants_list');
		$I->see('Student5', '#msg_participants_list');
	}
	
}
