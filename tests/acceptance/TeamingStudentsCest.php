<?php

use App\Trace;

/**
 * @group teaming
 */
class TeamingStudentsCest
{
	const ID_CLASS = 1;

	private $newReportId;

	public function testProcess(AcceptanceTester $I) {
		$I->loginAs('teacher1', 'mdp');
		$I->amOnPage("/teacher/students");
		$this->createTeamConfig($I);
		$I->seeILeftTrace(Trace::TEACHER_ADD_TEAMING);
		
		$this->viewTeamConfig($I);
		$I->logout();
		$I->seeILeftTrace(Trace::DISCONNECT_LNB);
		
		$I->seeILeftGoodNumberOfTracesSinceLastLogin('teacher1', 3);

		$this->studentSelectsNewTeam($I);
		$this->studentsSelectsExistingTeam($I);
	}

	private function createTeamConfig(AcceptanceTester $I) {
		$I->wantTo("create a student choice team config");
		$I->waitForElementVisible("tr#class-" . self::ID_CLASS . " td");
		$I->click(['css' => "tr#class-" . self::ID_CLASS . " td"]);
		$I->click(['css' => '[data-action="openTeamConfig"]']);
		$I->waitForElementVisible("#tc-popup");
		$I->dontSeeElement('.alert-danger');

		$I->waitForElementVisible('#tc-mission option[value="3"]');
		$I->selectOption("#tc-mission", ['value' => "3"]);

		$I->click(['css' => '#tc-toggle-reports-options']);
		$I->click(['css' => '#tc-toggle-future-students-options']);
		$I->click(['css' => '#tc-toggle-students-options']);
		
		$I->selectOption("#tc-teaming-method", ['text' => "au choix des étudiants"]);

		//$I->dontSee("Prévenir les étudiants par e-mail de leur mise en équipe");
		// TODO  other param ?
		$I->fillField("#tc-future-max-team-number", 3);
		$I->fillField("#tc-future-team-size-optimal", 2);
		
		$I->seeNumberOfElements(".without-team", 5);
		$I->seeNumberOfElements(".with-team", 0);

		$I->click("#tc-popup-submit"); // submit
		$I->waitForText("mission3", 3, ".class-team-missions");
	}

	private function viewTeamConfig(AcceptanceTester $I) {
		$I->click("mission3");
		$I->waitForElementVisible("#tc-popup");
		$I->seeOptionIsSelected("#tc-mission", "mission3");
		$I->click("#tc-toggle-future-students-options");
		$I->seeInField("#tc-future-team-size-optimal", "2");
	}

	private function studentSelectsNewTeam(AcceptanceTester $I) {
		$I->loginAs('student1', 'mdp');
		$I->see("mission3");
		$I->click("mission3");
		$I->see("mission 3 vide", '.mission_name');
		$I->click("S'inscrire dans une équipe");

		$id_team_config = $I->grabFromCurrentUrl('~/teamconfig/(\d+)/select~');
		$I->dontSeeSpecials();

		$I->see("Équipe vide");
		$I->seeNumberOfElements(".team", 1);
		$I->seeElement(".team_members.empty_members");
		$I->click('div.team:nth-child(2)'); // first team
		$I->dontseeElement(".team_members.empty_members");
		$I->seeElement(".team_members");
		$I->seeNumberOfElements(".team", 1); // 1 existing team
		$I->see("Student1", ".team .team_members");
		$I->dontSee("Équipe vide");
		$I->click("Valider");

		$this->newReportId = (int) $I->grabFromCurrentUrl('~/report/(\d+)~');
		$I->logout();

		$I->seeILeftTrace(Trace::DISCONNECT_LNB);
		$I->seeILeftGoodNumberOfTracesSinceLastLogin('student1', 3);
    }

	private function studentsSelectsExistingTeam(AcceptanceTester $I) {
		$reportId = $this->newReportId;
		$I->loginAs('student2', 'mdp');
		$I->click("mission3");
		$I->see("mission 3 vide", '.mission_name');
		$I->click("S'inscrire dans une équipe");

		$I->seeElement(".team");
		$I->seeNumberOfElements(".team", 2);
		$I->see("Student1");

		$friendP = $I->haveFriend('student3');
		$friendP->does(function(AcceptanceTester $I) use($reportId) {
			$I->comment("rejoint une équipe qui n'a pas encore commencé son rapport");
			$I->loginAs('student3', 'mdp');
			$I->click("mission3");
			$I->click("S'inscrire dans une équipe");
			$I->click('div.team:nth-child(2)'); // first team
			$I->see("Student3", ".team .team_members");
			$I->seeNumberOfElements(".team", 2);

			$I->click("Valider");
			$I->seeInCurrentUrl("report/$reportId");
			$I->see("mission 3 vide", '#lb_mission_name');
			
			$I->seeILeftTrace(Trace::ENTER_REPORT);
			$I->seeILeftGoodNumberOfTracesSinceLastLogin('student3', 2);
		});

		$friendM = $I->haveFriend('student4');
		$friendM->does(function(AcceptanceTester $I) use($reportId) {
			$I->comment("essaie de rejoindre une équipe qui n'a pas encore commencé, puis en crée une");
			$I->loginAs('student4', 'mdp');
			$I->click("mission3");
			$I->click("S'inscrire dans une équipe");
			$I->seeNumberOfElements(".team", 2);
			$I->click('div.team:nth-child(2)'); // first team
			$I->seeNumberOfElements(".team", 2);
			$I->click('div.team:nth-child(3)'); // first team
			$I->see("Student4", ".team .team_members");
			$I->seeNumberOfElements(".team", 2);
			$I->dontSee("Équipe vide");
			$I->click("Valider");
		
			$I->seeILeftGoodNumberOfTracesSinceLastLogin('student4', 2);
		});

		// avant synchro
		$I->comment("back to student2 who tries to join a full team");
		$I->waitForText("Student4"); // Wait for synchronize
		$I->seeNumberOfElements(".team", 3);
		$I->click('div.team:nth-child(2)'); // first team
		//$I->waitForText("sa taille maximale.", 3, '.alert-warning');
		$I->dontSee("Student2");

		// après refus et donc après synchro
		$I->comment("after failure, teams are synced with the server, student1 joins the next team");
		$I->seeNumberOfElements(".team", 3);
		$I->click('div.team:nth-child(4)'); // third team
		$I->seeNumberOfElements(".team", 3);
		$I->see("Student2");

		$I->comment("change d'équipe");
		$I->click('div.team:nth-child(2)'); // second team
		$I->seeNumberOfElements(".team", 3);
		$I->dontSee("Équipe vide");
		$I->click("Valider");

		$I->seeILeftGoodNumberOfTracesSinceLastLogin('student2', 2);
	}
}
