<?php

use App\Trace;

class DiffsCest
{
	private const ID_REPORT = 3;
	private const ID_LABDOC = 2;

	public function testDiffsTrace(AcceptanceTester $I)
	{
		$I->wantTo("See a trace 56 when clicking on the diffs button");
		$I->loginAs('teacher1', 'mdp');
		$I->amOnPage("/report/" . self::ID_REPORT . "?sc=follow");
		$I->waitForElementNotVisible('#report-wait-for-init');
		$I->executeJS("labdocDiff(" . self::ID_LABDOC .", 0).loadOrtoggleModifications();");
		$I->seeILeftTrace(Trace::TEACHER_DISPLAY_DIFFS);
	}
}
