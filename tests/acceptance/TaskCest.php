<?php

namespace Tests\acceptance;

use AcceptanceTester;
use App\Trace;
use Codeception\Util\Locator;
use \Facebook\WebDriver\WebDriverKeys;

/**
 * @group teaming
 */
class TaskCest
{

	public function _before(AcceptanceTester $I)
	{
		$I->amOnPage("/login");
		$I->loginAs('student1', 'mdp');
		$I->see("mission1", ".menu_mission_item");
		$I->click("#encours .menu_mission_item"); // cannot click() on text, because the HTML is misleading
		$I->waitForText("Continuer");
		$I->click("Continuer");
		$I->seeILeftTrace(Trace::ENTER_REPORT);

		// TODO WIP test on new interface
		$I->wait(2);
		$I->executeJS("$('#ressource').hide()");

		$I->click("#lb_menubar_task");

		$I->dontSeeSpecials();

		$I->waitForText("Liste des tâches");
		$I->seeILeftTrace(Trace::TASK_OPEN_LIST);

	}

	private function seeTask(AcceptanceTester $I, $text, $relative_id) {
		$tid = 293 + $relative_id; // 293 is the id of Tache 1 in database
		// order is 1, 2, 3, 2.1, 2.2
		$I->waitForElementVisible('#task-' . $tid);
		// TODO cannot check text I do not know why ...
	}


	public function TaskCRUD(AcceptanceTester $I)
	{
		// TODO Trace left are not checked
		$I->wantTo("CRUD Task");

		// **************************
		// Test if task is displayed
		// **************************
		$this->seeTask($I, "Tache 1", 0);
		$this->seeTask($I, "Sous tache 2.1", 3);
		
		// **************************
		// Test if task is deletable
		// **************************
		$I->moveMouseOver("#task-295 > div");
		//$I->click("#task-295 > .task-action"); // TODO WIP
		// TODO Implement delete task test
		
		// **************************
		// Test if task is creatable
		// **************************
		$I->click('#create-task-input input');
		$I->type("Tache 4");
		$I->pressKey('#create-task-input input', WebDriverKeys::ENTER);
		$this->seeTask($I, "Tache4", 5);
		$I->seeILeftTrace(Trace::TASK_CREATE);

		// **************************
		// Test if task is editable
		// **************************
		// TODO this test can fail, perhaps if report sync at the wrong time. This can be
		// avoid if refresh is improved and reload only if needed

		$I->click("#task-293 textarea");
		$I->type("plop");
		$I->click(".task-header-search");
		$this->seeTask($I, "Tache 1plop", 0);
		$I->seeILeftTrace(Trace::TASK_EDIT);
	}
	
	public function TaskMove(AcceptanceTester $I)
	{
		$I->wantTo("Move task");
	
		// **************************
		// Test if task is movable to another task
		// **************************
		//assert(false, "Not implemented yet");
		
		// **************************
		// Test if task is movable to other position
		// **************************
		//assert(false, "Not implemented yet");
		
		// **************************
		// Test if subtask is movable to first level
		// **************************	
		//assert(false, "Not implemented yet");
		
		// **************************
		// Test if subtask is movable to another subtask
		// **************************
		//assert(false, "Not implemented yet");
	}
}
