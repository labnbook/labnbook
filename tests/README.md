Testing LabBbook
================

- No PHP framework, so no functional tests on PHP.
- No JS modules, so no tests on JS code.
- Many PHP functions have side effects or global state,
  so unit testing would be meaningless.

Tests use CodeCeption, with acceptance tests (light integration tests).


Requirements
------------

Debian
```
sudo apt install chromium chromium-driver php-cli

mkdir tmp
wget -O tmp/codecept.phar https://codeception.com/codecept.phar

echo "CREATE DATABASE labnbook_test" | mysql
echo "CREATE USER IF NOT EXISTS labnbook@localhost IDENTIFIED BY 'perlinpinpin'" | mysql
echo "GRANT ALL PRIVILEGES ON .* TO labnbook@localhost | mysql
```

Running tests
-------------

```
# all the tests
./tests.sh

# display detailed progress (steps)
# create a HTML report
# stop a the first failure
./tests.sh --html --steps -f

# Only run a single test class
./tests.sh --steps -f acceptance LoginCest
```

Beware: The config `common/cnx_var.php` will be reset by the process.

Useful files
------------

- `tests.sh`: a shell script that automates the process
- `codeception.yml`: global config of Codeception. Will probably change rarely.
- `tests/acceptance.suite.yml`: configures the *acceptance* tests.
  Launches Chromium and PHP in the background, and prepares the DB data.
- `common/cnx_var_test.php`: LabNBook config file
  that will temporariy replace the normal config during the tests.
- `common/cnx_var_dev.php` will become the config after the tests.
