<?php

return [
    'url_prefix' => ENV('SCORING_URL_PREFIX', 'scoring/'),
    'linked_model_classes' => [
        'activity' => ENV('SCORING_ACTIVITY_CLASS', '\App\Mission'),
        'production' => ENV('SCORING_PRODUCTION_CLASS', '\App\Report'),
        'section' => ENV('SCORING_SECTION_CLASS', '\App\ReportPart'),
    ],
];
