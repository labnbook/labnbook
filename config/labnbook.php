<?php

return [
    /*
    |--------------------------------------------------------------------------
    | LabNBook configurations
    |--------------------------------------------------------------------------
    |
    |
    */

    "locker_validity" => env('LOCKER_VALIDITY', 61),  // 61 seconds - locker validity for a labdoc
    "refresh_period" => env('REFRESH_PERIOD', 20), // 20 seconds
    "old_refresh_period" => json_decode(env('OLD_REFRESH_PERIOD', '{}'), true), // 20 seconds
    "separator" => "-|-", // séparateur des attributs pour les traces
    'i18n' => env('i18n', true),
    "token_ttl" => env('TOKEN_TTL', 1800),
    "token_leeway" => env('TOKEN_LEEWAY', 20),
    "csrf_level" => env('CSRF_LEVEL', 'log'),
    "ips" => explode(',', env('IPS', "")),
    "maintenance" => env('MAINTENANCE_MESSAGE'),
    "bug_email_report_to" => env('BUG_EMAIL_REPORT_TO'),
    "support_email_to" => env('SUPPORT_EMAIL_TO'),
    "rgpd_email_to" => env('RGPD_EMAIL_TO'),
    "log_sql" => env('LOG_SQL', false), //log each sql query
    "js_debug" => env('JS_DEBUG', false), // debug js such as i18next
    "assets_hashes" => \App\Helper::initAssetsHashes(),
    "cas_institution_name" => env("CAS_INSTITUTION_NAME", "UGA / G-INP"),
    "cas_logo_filepath" => env("CAS_LOGO_FILEPATH", "images/logo-uga.svg"),
    "cas_name" => env("CAS_NAME", "AGALAN"),
    "ldap_directory_class" => env("LDAP_DIRECTORY_CLASS", '\App\Processes\Ldap\Agalan'),
    "id_mission_test" => env("ID_MISSION_TEST", null),
    "id_user_test" => env("ID_USER_TEST", null),
    "cgu_url" => env("CGU_URL", null),
    "php_expected_max_upload" => env("EXPECTED_MAX_UPLOAD", 8388608),
    "php_recommended_max_execution_time" => 60,
    "php_recommended_max_input_time" => 120,
    "php_recommended_max_input_vars" => 100000,
    "php_recommended_memory_limit" => 268435456,
    "beta_testers" => json_decode(env('BETA_TESTERS', '{}'), true),
];
