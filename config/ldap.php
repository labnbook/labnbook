<?php

return [
    'host'       => env('LDAP_HOST', ''),
    'port'       => env('LDAP_PORT', 636),
    'simulate_error' => env('LDAP_SIMULATE_ERROR', 0),
    'encryption' => env('LDAP_ENCRYPTION', 'tls'),

    'credentials_bind'     => env('LDAP_CREDENTIALS_BIND', ''),
    'credentials_password' => env('LDAP_CREDENTIALS_PASSWORD', ''),

    /*
     * If not empty, the LDAP lib will use the config('ldap_masquerade_users')
     * instead of querying the server.
     *
     * Useful only for testing without a real LDAP server.
     */
    'masquerade'       => env('LDAP_MASQUERADE', false),
    'masquerade_users' => [
        [
            'uid' => 'user-CAS', // login
            'cn' => "Masako CAS user", // full name
            'givenName' => "Masako", // first name
            'sn' => "CAS user", // last name
            'mail' => 'user-cas@example.org',
            'o' => 'u123', // organisation
            'employeeType' => ['P'],
            // AGALAN fields
            'aglnOrganizationUid' => ['u123_apo-12345679'], // student IDs
            'aglnPrimaryOrganizationName' => 'u123',
            'aglnPersonStatus' => 'OFFI',
            'aglnMailStatus' => 'normal',
            'aglnMailEffectiveAddr' => [
                'user-cas@etu.example.org,user-cas@example.org'
            ],
        ],
        [
            'uid' => 'login-agalan', // login
            'cn' => "Dimitri Karamazov", // full name
            'givenName' => "Dimitri", // first name
            'sn' => "Karamazov", // last name
            'mail' => 'pas.de.ldap@dev.com',
            'o' => 'u123', // organisation
            'employeeType' => ['E'],
            // AGALAN fields
            'aglnOrganizationUid' => ['u123_apo-12345678'], // student IDs
            'aglnPrimaryOrganizationName' => 'u123',
            'aglnPersonStatus' => '',
            'aglnMailStatus' => 'normal',
            'aglnMailEffectiveAddr' => [
                'pas.de.ldap@dev.com'
            ],
        ],
        [
            'uid' => 'paco', // login
            'cn' => "Pacôme De Champignac", // full name
            'givenName' => "Pacôme", // first name
            'sn' => "De Champignac", // last name
            'mail' => 'pc@etu.uga.fr',
            'o' => 'u123', // organisation
            'employeeType' => ['E'],
            // AGALAN fields
            'aglnOrganizationUid' => ['biber-987', 'u123_apo-92345678'], // student IDs
            'aglnPrimaryOrganizationName' => 'u123',
            'aglnPersonStatus' => '',
            'aglnMailStatus' => 'normal',
            'aglnMailEffectiveAddr' => [
                'pc@etu.uga.fr'
            ],
        ]
    ],
];
