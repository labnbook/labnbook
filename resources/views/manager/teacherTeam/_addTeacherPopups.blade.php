<div class="popup" id="t-popup">
    <header>
        <span class="title" id="t-popup-title">{{ __('Inscrire un enseignant') }}</span>
        <span class="actions">
            <i class="fa fa-check popup-submit" title="Valider" id="t-popup-valid"></i>
            <i class="fa fa-times popup-cancel" title="Annuler" id="t-popup-cancel"></i>
        </span>
    </header>
    <section>
        <form autocomplete="off">
            <fieldset class="" id="t-popup-method">
                <legend>{{__("Méthode")}}</legend>
                <div>
                    <label for="t-add-ldap" value="LDAP" onclick="setAddTeacherMethod('ldap')">
                        <input type="radio" id="t-add-ldap" value="LDAP" name="t-add-select-method">
                        {{__("Utilisation de l'annuaire :cas_name (conseillé)", ["cas_name" => config('labnbook.cas_name')]) }}
                    </label>
                </div>
                <div>
                    <label for="t-add-form" value="FORM" onclick="setAddTeacherMethod('form')">
                        <input type="radio" id="t-add-form" value="form" name="t-add-select-method">
                        {{__("Recherche / inscription par nom-prénom") }}
                    </label>
                </div>
            </fieldset>
            <fieldset class="t-fieldset t-add-ldap">
                <div>
                    <label for ="t-login-ldap">{{__("Recherche par identifiant de compte ou email")}}</label>
                    <input id="t-login-ldap" type="text" placeholder="{{ __("identifiant ou email") }}">
                </div>
                <div>
                    <div class="text-right">
                    <button type="button" onclick="queryLdapForTeacher()">
                        {{__("Interroger l'annuaire")}}
                    </button>
                    </div>
                </div>
            </fieldset>
                <fieldset class="t-fieldset t-add-form">
                    <legend>{{ __('Informations personnelles') }}</legend>
                    <label for="t-add-first-name" title="obligatoire" >{{ __('Prénom') }} * :</label> <input type="text" id="t-add-first-name" name="t-add-first-name" /><br/>
                    <label for="t-add-name" title="obligatoire" >{{ __('Nom') }} * :</label> <input type="text" id="t-add-name" name="t-add-name"/><br/>
                    <label for="t-add-email" title="{{ __('E-mail ou mot de passe nécessaire') }}">E-mail (*) : </label> <input type="email" id="t-add-email" name="t-add-email" title="{{ __('E-mail ou mot de passe nécessaire') }}"/><br/>
                </fieldset>
                <fieldset class="t-fieldset t-add-form">
                    <legend>{{ __('Informations de connexion') }}</legend>
                    <label for="t-add-login">{{ __('Intitulé du compte (login)') }} * :</label> <input type="text" id="t-add-login" name="t-add-login" /><br/>
                    <label for="t-add-password" title="{{ __('E-mail ou mot de passe nécessaire : 8 caractères ou plus dont une majuscule et une minuscule') }}" >{{ __('Mot de passe') . " (*)"}}{!!__("&nbsp;:")!!} </label> <input type="text" id="t-add-password" name="t-add-password" title="{{ __('E-mail ou mot de passe nécessaire : 8 caractères ou plus dont une majuscule et une minuscule') }}"/><br/>
                </fieldset>
            <div title="{{ __("En cochant cette case, l'enseignant recevra un e-mail de confirmation lors de la création ou de la mise à jour de son compte") }}" id="t-add-send-email-div"><br/><input type="checkbox" id="t-add-send-email" name="t-add-send-email" />&nbsp;<label for="t-add-send-email"> {{ __('Envoyer un e-mail récapitulatif') }}</label></div>

        </form>
    </section>
</div>


<div class="popup" id="t-add-ldap-popup">
    <header>
        <span id="t-add-popup-title" class="title">{{ __('Enseignant trouvé !') }}</span>
        <span class="actions" >
            <i class="fa fa-check popup-submit" title="Valider" id="t-add-ldap-popup-valid" onclick="addTeacherFromLdap()"></i>
            <i class="fa fa-times popup-cancel" title="{{ __('Annuler') }}" id="t-add-ldap-popup-cancel"></i>
        </span>
    </header>
    <section>
    </section>
</div>

<div class="popup" id="t-add-popup">
    <header>
        <span id="t-add-popup-title" class="title">{{ __('Enseignant existant ?') }}</span>
        <span class="actions" >
            <i class="fa fa-times popup-cancel" title="{{ __('Annuler') }}" id="t-add-popup-cancel"></i>
        </span>
    </header>
    <section>
    </section>
</div>
