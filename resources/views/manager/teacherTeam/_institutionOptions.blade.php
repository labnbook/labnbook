<?php
/** @var \App\Institution[] $institutions */

 if (count($institutions) > 1) {
     echo '<option value="0"></option>';
}

foreach ($institutions as $inst) {
?>
<option value="{{$inst->id_inst}}"> {{$inst->name}} </option>
<?php
}
