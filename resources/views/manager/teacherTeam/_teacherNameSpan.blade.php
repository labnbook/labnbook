<?php
    /** @var \App\User $user */
    if ($user): ?>
        <span data-id-teacher="{{ $user->id_user }}" >-&nbsp;{{ $user->user_name . ' ' . $user->first_name }}&nbsp;<i onclick="deleteTeacherFromTeacherTeam({{ $user->id_user }})" title="{{ __("Dissocier l'enseignant de l'équipe") }}" class="far fa-trash-alt"></i>&nbsp;</span>
    <?php endif ?>
