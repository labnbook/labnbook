<?php
/** @var \App\User $user 
 * @var \App\Institution $inst
 */
/** @var \App\Helper $helper */
?>
@extends('layouts.labnbook')


@section('title', __('Gestion des équipes pédagogiques'))

@section('head')
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script>
        <!-- Variable transmises par PHP au javascript -->
        window.global_current_id_inst    =  {{ Auth::user()->id_inst }};
        window.global_ldap    =  {{ $inst->hasCAS() }};
    </script>

    <?= $helper->loadAsset("/libraries/papaparse.min.js") ?>

    <?= $helper->loadAsset("/libraries/datatables/datatables.min.css") ?>
    <?= $helper->loadAsset("/libraries/datatables/datatables-all.js") ?>

    <?= $helper->loadAsset("/css/flexboxgrid.min.css") ?>
    <?= $helper->loadAsset("/css/teacher/common.css") ?>
    <?= $helper->loadAsset("/js/teacher/authoring.js") ?>

    <!--Styles et fonctions de la page -->
    <?= $helper->loadAsset("/css/manager/teacherTeam.css") ?>
    <?= $helper->loadAsset("/js/manager/teacherTeam.js", 'defer') ?>
    <?= $helper->loadAsset("/js/manager/addTeacher.js", 'defer') ?>

@endsection

@section('bodyattributes', 'id=page-students')

@section('body')
    @include('teacher/_menu', [
        'page_name' => request()->path(),
        'refresh_period' => config('labnbook.refresh_period'),
        ])

    <div id="wrapper-content">
        <div id="teachers">
            <section>
                <label for="t-search-teacher">{{__("Rechercher un enseignant dans la base LabNBook (institution :inst)", ["inst" => $inst->name  ])}}{!!__("&nbsp;:")!!} </label>
                <input type="text" id="t-search-teacher" name="t-search-teacher" placeholder="{{__("prénom, nom, mail")}}"/>
                <div id="t-teacher-results" class="hide"></div>
            </section>
        </div>
        <div>
            <label for="t-add-teacher">{{ __("Inscrire un enseignant (création de compte ou promotion de compte étudiant)")}}{!!__("&nbsp;:")!!} </label><button type="button" class="lb_btn" id="t-add-teacher" onclick="openAddTeacher()">{{__("Inscrire")}}</button>
        </div>
        <!-- tableau des équipes -->
        <div class="width-medium" id="teacherTeams">
            <section>
                <label for="teacherTeams">{{__("Créer et gérer des équipes pédagogiques")}}{!!__("&nbsp;:")!!}</label>
                <div class="table-caption">
                    <strong>{{ __('Equipes pédagogiques') }}</strong>
                    <span class="tb-search">
                        <input class="search-input" type="text" placeholder="{{ __('Recherche') }}"/>
                    </span>
                    <span class="display-archives" title="{{ __('Afficher les éléments archivés') }}">
                        <input id="display-archives" name="display-archives" type="checkbox">
                        <label for="display-archives"> {{ __('archives') }}</label>
                    </span>
                    <span class="actions" id="tt-actions">
                        <span class="lba-context-icons actions-inactive">
                            <a href="#" data-action="edit" title="{{ __("Modifier l'équipe pédagogique") }}"
                               class="single-action"><i class="fas fa-pencil-alt"></i></a>
                            <a href="#" data-action="duplicate" title="{{ __("Dupliquer l'équipe pédagogique") }}"
                               class="single-action"><i class="fa-solid fa-clone"></i></a>
                            <a href="#" data-action="archive" title="{{ __("Archiver / supprimer l'équipe pédagogique")}}"
                               class="single-action"><i class="fa fa-broom"></i></a>
                        </span>
                        <a href="#" data-action="add" title="{{ __('Ajouter une équipe pédagogique') }}"><i
                            class="fa fa-plus"></i></a>
                    </span>
                </div>
                <table class="table-base" id="tt-table">
                    <thead>
                    <tr>
                        <th title="{{ __('Trier par nom') }}" class="sorting_asc">{{ __('Nom') }}</th>
                        <th title="{{ __("Trier par nombre d'enseignants") }}">{{ __('Enseignants') }}</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>{{ __('Nom') }}</th>
                        <th>{{ __('Enseignants') }}</th>
                    </tfoot>
                </table>
            </section>
        </div>
    </div>

    <div class="popup" id="tt-popup" title="{{ __('Ajouter ou modifier une équipe pédagogique') }}">
        <header>
            <span class="title" id="tt-popup-title"></span>
            <span class="actions">
                <i class="fa fa-check popup-submit" title="Valider" id="tt-popup-valid"></i>
                <i class="fa fa-times popup-cancel" title="Annuler" id="tt-popup-cancel"></i>
            </span>
        </header>
        <section>
            <label for="tt-add-name">{{ __("Nom de l'équipe") }} * :</label> <input type="text" id="tt-add-name" name="tt-add-name"/><br/>
            <label for="tt-add-inst">{{ __('Institution') }} * :</label> <select id="tt-add-inst"
                                                                                 onchange='selectInstForTeacherTeam()'></select><br/>
            <fieldset id="tt-teacher-datalist">
                <legend>{{ __('Enseignants associés') }}</legend>
                <div id="tt-teachers-list"></div>
                <div>{{__('Associer un enseignant')}}{!!__("&nbsp;:")!!}
                    <x-select2 id="tt-add-teacher" on-select-function="addTeacherInTeacherTeam(selectedElement)"></x-select2>
                </div>
            </fieldset>
        </section>
    </div>


    <div class="popup" id="tt-add-popup"> 
        <header>
            <span id="tt-add-popup-title" class="title">{{ __('Répercussion des modifications effectuées') }}</span>
        </header>
        <section>
            <div>{{ __("L'équipe pédagogique modifiée est liée")}}</div>
            <div><span class="tt-add-popup-emphase">{{ __('aux classes : ') }}</span> <span id="tt-add-classes-list"></span></div>
            <div><span class="tt-add-popup-emphase">{{ __('aux missions : ') }}</span> <span id="tt-add-missions-list"></span></div>
            <div id="tt-add-teachers-message"></div>
            <div id="tt-add-popup-btn">
                <button type="button" class="lb_btn" id="tt-add-popup-valid" onclick="acceptConfirmationModal()">Oui</button>
                <button type="button" class="lb_btn" id="tt-add-popup-cancel" onclick="closeConfirmationModal()">Non</button>
                <button type="button" class="lb_btn hide" id="tt-add-popup-ok" onclick="closeConfirmationModal()">OK</button>
            </div>
        </section>
    </div>
    @include('manager/teacherTeam/_addTeacherPopups')
    @include('components.widgets.message.index', ['mv' => new App\Views\Widgets\Message($user->id_user, 0)])
    @include('teacher/_archivePopup', ['xDataFunction' => "archivePopup('teacherTeamActions')"])
@endsection
