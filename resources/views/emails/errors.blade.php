@component('mail::message')
<?php
    /** @var array $data */
    $rootUrl = config('app.url');
?>
{{__("Bonjour,")}}

{{__("Une erreur est survenue sur")}} {{$rootUrl}} : `{{$data['message']}}`

## Contexte

+ Message :         `{!!$data['message']!!}`
+ Url :             `{{Request::url()}}`
+ Classe :          `{{$data['class']}}`
+ Code :            `{{$data['code']}}`
+ Id Utilisateur :  `{{Auth::id()}}`

## Trace :

```
{!!$data['trace']!!}
```

## Informations supplémentaires


### Requête

```
@JSON($data['request'], JSON_PRETTY_PRINT)

```

### Session

```
@JSON($data['session'], JSON_PRETTY_PRINT)

```


{{__("Cet e-mail est envoyé automatiquement. Les réponses ne seront pas traitées.")}}
@endcomponent
