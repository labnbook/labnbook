@component('mail::message')
<?php
    /** @var array $data */
    /** @var string $url */
    /** @var string $token */
    $rootUrl = config('app.url');
    $url = $rootUrl.'/password/reset';
?>
{{__("Bonjour,")}}


{{__("La réinitialisation du mot de passe dans LabNBook a été demandée.")}}

<?php if (count($data) === 1) : ?>
{{__("Un compte LabNBook correspond")}} <?php else : ?> {{count($data)}} {{__("comptes LabNBook correspondent")}} <?php endif ?> {{__("à votre adresse e-mail.")}}

<?php foreach($data as $login => $token): ?>
* {{__("Compte")}} **{{$login}}** - {{__("Cliquez sur ce lien pour mettre à jour le mot de passe")}} : {{$url."/".$token}}
<?php endforeach?>

{{__("Cet e-mail est envoyé automatiquement. Les réponses ne seront pas traitées.")}}
@endcomponent
