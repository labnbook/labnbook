@component('mail::message')
<?php
    $url = config('app.url');
?>
Bonjour,

Ceci est un mail de test de la plateforme la plateforme [{{$url}}]({{$url}}).

Cet e-mail est envoyé automatiquement. Les réponses ne seront pas traitées.
@endcomponent
