Bonjour,

Vous avez été inscrit par l'enseignant {{$teacher}} à la mission {{$mission}}.

Accès : {{$url}}

{{$sDate}}{{$eDate}}

Cet e-mail est envoyé automatiquement. Les réponses ne seront pas traitées.
