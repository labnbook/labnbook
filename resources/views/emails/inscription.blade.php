@component('mail::message')
<?php
    /** @var array $data */
    /** @var string $url */
    /** @var string $token */
    $url = config('app.url');
?>
{{__('Bonjour,')}}

@if ($data['password'] !== null)

{{__('Un compte LabNBook vous a été attribué sur la plateforme [:url](:url)', ['url' => $url])}}

{{__('Voici vos informations de connexion :')}}

- {{__('Compte :')}} {{$data['login']}}
- {{__('Mot de passe :')}} {{empty($data['password']) ? __('Celui que vous avez choisi') : $data['password']}}

@else

{{__('Vous avez été inscrit sur la plateforme LabNBook&nbsp;:')}}

[{{$url}}]({{$url}})

{{__('Connectez vous avec votre compte institutionnel :name.', ['name' => config('labnbook.cas_institution_name')])}}


@endif

{{__('Cet e-mail est envoyé automatiquement. Les réponses ne seront pas traitées.')}}
@endcomponent
