@component('mail::message')
<?php
    /** @var array $data */
    $rootUrl = config('app.url');
?>
{{__("Bonjour,")}}

{{__("Ceci est un rapport de test d'adminsitration de")}} {{$rootUrl}}

@foreach ($data as $type => $messages)

@if (count($messages) > 0)

## {{$type}}

@foreach($messages as $m)

+ {{$m}}

@endforeach

@endif

@endforeach


{{__("Cet e-mail est envoyé automatiquement. Les réponses ne seront pas traitées.")}}
@endcomponent
