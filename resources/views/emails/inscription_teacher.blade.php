@component('mail::message')

<p>Bonjour,</p>

<p>Vous avez été inscrit par l'enseignant <em> {{$teacher}} </em> à la mission
<a href="{{$url}}">{{$mission}}</a>.</p>

<p>{{$sDate}}<br />{{$eDate}}</p>

<p>Cet e-mail est envoyé automatiquement. Les réponses ne seront pas traitées.</p>
