<div class="popup" id="confirmation-popup" title="" data-reset-input="true">
    <header>
        <span class="popup-title" ></span>
        <span class="actions" >
        <a href="#" title="{{ __("Valider") }}" class="popup-submit"><i class="fa fa-check disabled"></i></a>
        <a href="#" title="{{ __("Annuler") }}" class="popup-cancel"><i class="fa fa-times"></i></a>
    </span>
    </header>
    <section>
        <form>
         
                <input type="checkbox" id="confirmation-popup-confirm-ok" name="confirmation-popup-confirm-ok" />
                <span class="message">
                </span>
           
            <input type="hidden" id="confirmation-popup-id-user" value="{{Auth::id()}}" />
        </form>
    </section>
</div>
