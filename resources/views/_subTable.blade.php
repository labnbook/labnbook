<?php
    /**
    * @var \App\Helper $helper
    */
    /**
     * @var array $data [['col1' => val, ...], ...]
    */
    /**
    * @var string $classes HTML classes for the table
    */
    /**
    * @var string $id HTML id for the table
    */
    /**
    * @var string $title title for the table
    */
?>
<?php if(!empty($data)) {
    $keys = array_keys($data[0]);
?>
<table class="{{$classes}}" id="{{$id}}">
    <thead>
        <tr>
            <th colspan="{{count($keys)}}" style="text-align:center">
            {{$title}}
        </th>
        </tr>
        <tr>
            <?php foreach ($keys as $k): ?>
        <th>
            {{$k}}
        </th>
        <?php endforeach ?>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($data as $entry): ?>
    <tr>
        <?php foreach ($entry as $key => $val): ?>
            <td>{{$val}}</td>
        <?php endforeach ?>
    </tr>
    <?php endforeach ?>
    </tbody>
    </table>
<?php
    }
