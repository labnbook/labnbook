<?php
    /** @var \App\Helper $helper */
?>
@extends('layouts.home_minimal')
@section('maincontent')
    <h2> @yield('message')</h2>
    <div>
        <p><strong>{{ $helper->pullPolicyErrorMessage('') }}</strong></p>
    </div>
    <div>
        <p> {{__("Vous pouvez")}}{!!__("&nbsp;:")!!}</p>
        <ul>
            <li><a href="{{ route('home') }}" title='{{__("Retourner à l'accueil")}}'>{{__("Retourner à l'accueil")}}</a></li>
            <li><a href="{{ url()->previous()}}" title='{{__("Page précédente")}}'>{{__("Aller à la page précédente")}}</a></li>
            @yield('links')
        </ul>
        <p> {{__("Ou, si ce comportement vous semble anormal,")}} <a href="{{ $helper->getMailto('bug') }}">{{__("envoyer un mail aux développeurs de LabNBook")}}.</a></p>
        @yield('contents')
    </div>
@endsection
