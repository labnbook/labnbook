@extends('errors::minimal')

@section('title', __('Forbidden'))
@section('message', $exception->getMessage() ?$exception->getMessage() : __('Forbidden'))
