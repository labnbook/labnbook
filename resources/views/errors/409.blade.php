@extends('errors::minimal')

@section('title', __('Conflict'))
@section('message', $exception->getMessage() ?$exception->getMessage() : __('Conflict'))
