@extends('errors::minimal')

@section('title', __('Page Expired'))
@section('message', __('Page Expired'))
@section('links')
    <li><a href="{{ url()->current() }}" title="{{__("Réactualiser la page")}}">{{("Réactualiser la page")}}</a></li>
@endsection
@section('contents')
    <?php if(config('app.debug') == true) :?>
    <h2> {{__("Aide au debug")}} </h2>
    <p> {{__("Cette erreur est souvent due à un token CSRF manquant.")}}</p>
    {{__("Pour ajouter un token CSRF dans un formulaire ou dans un ajax, vous pouvez")}}{!!__("&nbsp;:")!!}
    <ul>
        <li>{{__("Dans une vue blade")}}{!!__("&nbsp;:")!!} <code><?= htmlspecialchars('@csrf') ?></code></li>
        <li>{{__("En php")}}{!!__("&nbsp;:")!!} <code>csrf_token()</code></li>
        <li>{{__("En javascript")}}{!!__("&nbsp;:")!!}
            <ul>
                <li>{{__("Les requêtes Jquery sont configurées automatiquement à condition de mettre")}} <code><?= htmlspecialchars('<?= $helper->configureAjax()  ?>') ?></code> {{__("juste après le chargement de ")}}jquery</li>
                <li>{{__("Il est possible de récupérer le token manuellement")}}<code>document.head.querySelector("meta[name=csrf-token][content]").content;</code> </li>
            </ul>
        </li>
    </ul>
    <?php endif ?>
@endsection
