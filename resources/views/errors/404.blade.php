@extends('errors::minimal')

@section('title', __('Page non trouvée'))
@section('message', __('Page non trouvée'))
