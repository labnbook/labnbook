@extends('errors::minimal')

@section('title', __('Maintenance'))

@section('message')
    <?php echo $exception->getMessage() ? $exception->getMessage() :  __('Service unavailable'); ?>
@endsection

