<?php

/**************************************
 * This is a simple multi select component using Alpine JS and served as a blade component
 * It can be used with : 
    <div x-data="multiSelectComponent( { options : availableOptions, selected : selectedOptions, onChange: callback, changeData: data })" >
        @include('_multiSelect')
    </div>
 * 
 * where availableOptions is an array of objects with value and displayValue fields
 *  example : [{ value: '2', selectValue : 'Option1', displayValue: 'Op 1' }, ... ]
 * and selectedOptions is an array of objects with value and displayValue fields
 *  example : [{ value: '2', selectValue : 'Option1', displayValue: 'Op 1' }, ... ]
 * 
 *  onChange is an optionnal callback that will be called on changes with selected and changeData
 * 
 * Notes :  displayValue can be any html content, selectValue is the text value that will be displayed in the dropdown
 * 
 * This component has not been tested with multiple instances on the same page
 */

?>

@once
    <style>
        .dropdown-menu {
            display: none;
            position: relative;
            background-color: white;
            border: 1px solid #ccc;
            z-index: 9999;
            width: 200px;
            max-height: 300px;
            overflow: scroll;
        }
        .dropdown-menu.show {
            display: block;
        }
        .dropdown-item {
            padding: 8px;
            cursor: pointer;
        }
        .dropdown-item:hover {
            background-color: #f1f1f1;
        }
    </style>
@endonce

<div class="border p-2" @click="toggleDropdown" @click.outside="closeDropdown">
    <template x-if="selected.length === 0">
        <span class="text-gray-500">{{__("Choisir...")}}</span>
    </template>
    <template x-for="option in selected" :key="option.value">
            <span class="mr-2 bg-blue-200 text-blue-800 p-1 rounded" :title="option.selectValue">
                <span x-html="option.displayValue" ></span>
                <span class="cursor-pointer" @click.stop="removeOption(option)"></span>
            </span>
    </template>
</div>
<div class="dropdown-menu mt-1 border" :class="{'show': open}">
    <template x-for="option in options" :key="option.value">
        <div class="dropdown-item" @click.stop="toggleOption(option)">
            <input type="checkbox" :checked="isSelected(option)" class="w-15px d-inline">
            <span x-html="option.selectValue"></span>
        </div>
    </template>
</div>

@once
    <script>
        function multiSelectComponent(config) {
            return {
                open: true,
                selected: config.selected || [],
                options: config.options || [],
                onChange: config.onChange || null,
                changeData: config.changeData || null,
                toggleDropdown() {
                    this.open = !this.open;
                },
                closeDropdown() {
                    this.open = false;
                },
                toggleOption(option) {
                    // get only value field from this.selected
                    if( this.selected.map(item => item.value).includes(option.value) ) {
                        this.selected = this.selected.filter(item => item.value !== option.value);
                    } else {
                        this.selected.push(option);
                    }
                    if (this.onChange) {
                        this.onChange({
                            changeData: this.changeData,
                            selected: this.selected
                        });
                    }
                },
                removeOption(option) {
                    this.selected = this.selected.filter(item => item.value !== option.value);
                    if (this.onChange) {
                        this.onChange({
                            changeData: this.changeData,
                            selected: this.selected
                        });
                    }
                },
                isSelected(option) {
                    return this.selected.map(item => item.value).includes(option.value);
                }
            };
        }
    </script> 
@endonce
