<?php
/**
 * @var string $classWrapper
 * @var Array $items
 */
$itemsNumber = count($items);
?>

<div class="{{$classWrapper}} position-relative"
     x-data="{openConfig:false, numberTotal:{{$itemsNumber}}, numberChecked:0 }" x-init="openConfig=false" x-on:click.outside="openConfig=false">
    <button class="protocol-step-config-button border-none ph-2 border-radius-2 bg-transparent col-secondary pointer-hover"
            x-on:click="openConfig=!openConfig">
        <i class="fa-solid fa-gear"></i>
        <span class="protocol-authorizations-title col-secondary" x-text="'{{__('Autorisations étudiants : ')}}'+numberChecked+'/'+numberTotal"></span>
    </button>
    <div x-show="openConfig" class="position-absolute t-100p r-0 z-index-1000 bg-white box-shadow-0-0-4-lightgrey p-3">
        @foreach($items as $item)
            <div class="p-3 col-grey">
                <label class="pointer-hover no-wrap">
                    <input x-on:change="numberChecked += (({{$item['xModel']}})?1:-1)" x-init="numberChecked += ({{$item['xModel']}})" type="checkbox" x-model="{{$item['xModel']}}"/>
                    {{$item['label']}}
                </label>
            </div>
        @endforeach
    </div>
</div>
