<template x-if="!proNS.isTeacher && proNS.simulationCode">
    <div class="flex-column-baseline gap-10 mv-5" x-on:synchronize-simulation.window="proNS.simulation.synchronizeProtocol()">
        <div class="flex-row-center gap-10">
            <button class="border-grey border-radius-10 ph-8 pv-2 bg-whitesmoke bg-lightgrey-hover col-primary fs-14 pointer-hover"
                    type="button"
                    x-show="proNS.ldEdition"
                    :id="proNS.id_ld+'_protocol_submission'"
                    :title="proNS.simulation.getTitleSimulationButton()"
                    x-on:click.stop="if(!proNS.simulation.isRunning){proNS.closeAllEditContent();proNS.simulation.checkProtocol(true);}else{proNS.simulation.simulationPaused=!proNS.simulation.simulationPaused}">
                <i :class="{'fas fa-pause':!proNS.simulation.simulationPaused,'fas fa-play':proNS.simulation.simulationPaused}"></i>
                <span>{{__('Simuler l\'expérience')}}</span>
            </button>
            <button class="border-grey border-radius-10 ph-8 pv-2 bg-whitesmoke bg-lightgrey-hover col-primary fs-14 pointer-hover"
                    type="button"
                    x-show="proNS.ldEdition && !proNS.simulation.isRunning"
                    :id="proNS.id_ld+'_protocol_submission'"
                    title="{{__("Vous pouvez demander une aide sur votre protocole. Le système essaie d'adapter le message d'aide à vos connaissances.")}}"
                    x-on:click.stop="if(!proNS.simulation.isRunning){proNS.closeAllEditContent();proNS.simulation.checkProtocol(false);}">
                <i class="fas fa-question"></i>
                <span>{{__('Obtenir une aide')}}</span>
            </button>
        </div>
        <div class="col-primary fs-14" x-show="proNS.simulation.checkProtocolInProcess">
            <i class="fa fa-spinner fa-pulse"></i>
            <span>{{__('Evaluation du protocole en cours...')}}</span>
        </div>
        <div :id="proNS.id_ld+'_simulation_feedbacks'" x-show="(proNS.ldEdition || window.global_scope === 'follow') && proNS.simulation.progression !== null"
             class="p-10 w-100p-22 flex-column gap-10 border-radius-2 border-primary">
            <template x-if="proNS.simulation.feedbackSimulation">
                <div x-html="proNS.simulation.feedbackSimulation"></div>
            </template>
            <template x-if="proNS.simulation.feedbackError">
                <div class="simulation-feedback-error flex-row-center gap-5 flex-wrap w-100p">
                    <div class="fw-bold">{{__('Erreur')}}</div>
                    <div x-text="proNS.simulation.feedbackError"></div>
                </div>
            </template>
            <template x-if="proNS.simulation.feedbackIndications">
                <div class="simulation-feedback-indications w-100p line-height-1-5rem">
                    <div class="fw-bold float-left mr-5">{{__('Indications')}} : </div>
                    <div x-html="proNS.simulation.feedbackIndications"></div>
                </div>
            </template>
            <template x-if="proNS.simulation.failedConstraints && proNS.simulation.failedConstraints.length>0">
                <div class="simulation-follow-failed-constraint flex-row-baseline gap-5 w-100p">
                    <div class="fw-bold flex-shrink-0">{{__('Erreur(s)')}} :</div>
                    <div class="flex-column border-lightgrey border-radius-2 overflow-hidden">
                        <template x-for="(failedConstraint, index) in proNS.simulation.failedConstraints">
                            <div class="p-3" :class="{'border-b-lightgrey':index<proNS.simulation.failedConstraints.length-1}" x-text="failedConstraint.constraint_message + ' - ' +failedConstraint.technical_feedback "></div>
                        </template>
                    </div>
                </div>
            </template>
            <template x-if="proNS.simulation.progression !== null">
                <div class="flex-row-center gap-5" :class="{'w-100p':proNS.simulation.feedbackSimulationEnabled, 'w-50p':!proNS.simulation.feedbackSimulationEnabled}">
                    <div class="fw-bold">{{__('Avancement')}} : </div>
                    <div class="flex-grow-1 border-radius-10 border-primary h-10 overflow-hidden">
                        <div class="bg-primary h-100p " :style="'width:'+(proNS.simulation.progression*100)+'%'"></div>
                    </div>
                    <template x-if="proNS.simulation.feedbackSimulationEnabled">
                        <div x-text="proNS.simulation.feedbackSimulationEnabled"></div>
                    </template>
                </div>
            </template>
            <template x-if="proNS.simulation.globalKnowledges && proNS.simulation.globalKnowledges.length>0">
                <div class="simulation-global-knowledges flex-row-center gap-5 flex-wrap w-100p">
                    <div class="fw-bold">{{__('Connaissances')}} :</div>
                    <template x-for="globalKnowledge in proNS.simulation.globalKnowledges">
                        <div class="display-inline-flex gap-5" :title="globalKnowledge.title">
                            <i :style="'color: ' + globalKnowledge.color" :class="{'fas fa-circle-up': globalKnowledge.evolution==='up',
                        'fas fa-circle-down':globalKnowledge.evolution==='down',
                        'fas fa-circle':globalKnowledge.evolution==='steady'}"></i>
                            <span x-text="globalKnowledge.description"></span>
                        </div>
                    </template>
                </div>
            </template>
        </div>
    </div>
</template>

<template x-if="proNS.ldEdition && !proNS.isTeacher && proNS.simulationCode && proNS.isSuperManager">
    @include('labdoc/copex/_simulation_debug_constraints')
</template>

<template x-if="!proNS.isTeacher && proNS.simulationCode">
    <div>
        <div :id="proNS.id_ld+'_simulation_display_div'" class="display-div"></div>
        <canvas :id="proNS.id_ld+'_simulation_canvas'" class="w-100p h-450" x-init="proNS.simulation.initSimulation()"></canvas>
    </div>
</template>

