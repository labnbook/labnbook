<div class="protocol-node-button-wrapper flex-row justify-space-between flex-shrink-0 gap-3 min-w-60">
    <button :title="'{{__('Déplacer')}}'" class="protocol-node-button-move border-none ph-2 border-radius-2 bg-transparent col-primary" type="button"
            :class="{ 'pointer-hover node-handle': proNS.isTeacher || node.isMovable, 'opacity-40':!(proNS.isTeacher || node.isMovable)}"
            :disabled="!(proNS.isTeacher || node.isMovable)">
        <i class="fa-solid fa-up-down-left-right move-hover"></i>
    </button>
    <button :title="'{{__('Dupliquer')}}'" class="protocol-node-button-duplicate border-none p-3 border-radius-2 bg-transparent col-primary" type="button"
            x-show="proNS.ldEdition && sectionEdition"
            x-on:click.stop="if (proNS.isTeacher || node.isDuplicable) { proNS.duplicateNode(node); }"
            :class="{ 'pointer-hover': proNS.isTeacher || node.isDuplicable, 'opacity-40': !(proNS.isTeacher || node.isDuplicable)}"
            :disabled="!(proNS.isTeacher || node.isDuplicable)">
        <i class="far fa-copy"></i>
    </button>
    <button :title="proNS.getSwitchText(node)" class="protocol-node-button-switch border-none p-3 border-radius-2 bg-transparent col-primary" type="button"
            :class="{ 'pointer-hover': proNS.checkSwitchableNode(node), 'opacity-40':!proNS.checkSwitchableNode(node)}"
            :disabled="!proNS.checkSwitchableNode(node)"
            x-show="proNS.ldEdition && sectionEdition && (proNS.isTeacher || proNS.isFreeActionAllowed())"
            x-on:click.stop="proNS.switchStepAction(node); firstInit = true;">
        <i class="fas fa-arrow-right-arrow-left toggle-icon"></i>
    </button>
    <button :title="'{{__('Supprimer')}}'" class="protocol-node-button-delete border-none p-3 border-radius-2 bg-transparent col-primary" type="button"
            x-show="proNS.ldEdition && sectionEdition"
            x-on:click.stop="if (proNS.isTeacher || node.isDeletable) { proNS.removeNode(node); } 
            if($el.closest('ul').closest('li')){$el.closest('ul').closest('li').classList.remove('child-hover');}"
            :class="{ 'pointer-hover': proNS.isTeacher || node.isDeletable, 'opacity-40':!(proNS.isTeacher || node.isDeletable)}"
            :disabled="!(proNS.isTeacher || node.isDeletable)">
        <i class="fas fa-trash-can"></i>
    </button>
    <button :title="'{{__('Fermer l’édition')}}'" class="protocol-node-button-validate border-none p-3 border-radius-2 bg-transparent col-primary pointer-hover" type="button"
            x-show="proNS.ldEdition && sectionEdition"
            x-on:click.stop="node.inEdition = false">
        <i class="fas fa-check"></i>
    </button>
</div>
