<div class="constraints-state flex-column gap-5 w-100p mv-10">
    <h3>{{__('Etat des contraintes')}}</h3>
    <div class="flex-row-center justify-space-between gap-5 w-100p border-b-lightgrey">
        <div class="w-5p fw-bold">{{__('Pos')}}</div>
        <div class="w-35p fw-bold">{{__('Clé')}}</div>
        <div class="w-50p fw-bold">{{__('Description')}}</div>
        <div class="w-10p fw-bold">{{__('Etat')}}</div>
    </div>
    <div class="overflow-y-scroll max-h-300">
        <template x-for="(constraint, index) in proNS.simulation.constraintStates.sort((a, b) => a.position - b.position)" :key="constraint.id_constraint">
            <div class="flex-row-baseline justify-space-between gap-5 w-100p border-b-lightgrey"
                 x-effect="if(proNS.simulation.previous_error && constraint.id_constraint === proNS.simulation.previous_error.id_constraint){
                            $nextTick(()=>{$el.parentElement.scrollTo({top:$el.offsetTop-$el.parentElement.offsetTop-20,left:0,behavior:'smooth'});});
                         }">
                <div class="w-5p" x-text="index"></div>
                <div class="w-35p break-word" x-text="constraint.constraintKey"></div>
                <div class="w-50p" x-text="constraint.description"></div>
                <div class="w-10p" :class="{ 'col-green': constraint.current_state===1, 'col-red': constraint.current_state===0 }" x-text="constraint.current_state===null?'irrelevant':(constraint.current_state?'valid':'invalid')"></div>
            </div>
        </template>
    </div>
</div>
