<div class="m-5 border-lightgrey border-radius-2 p-10 bg-white">
    <div class="mb-10">
        {{__('Modifier la configuration de l’exercice ci-dessous ')}}
        <a href='/tool_copex/v2_json/titrab/resources/recapitulatif-exercices-TitrAB.pdf' target='_blank' rel='noopener noreferrer'>{{__('(exercices disponibles)')}}</a>
    </div>
    <div class="flex-row-center gap-5" x-data="{ levelExercise: null, level: null, exercise: null }">
        <span>{{__('Niveau')}}</span>
        <select x-model="level">
            <option value="" :label="__('Choix d’un niveau')" disabled :selected="true"></option>
            <template x-for="keyLevel in Object.keys(proNS.simulation.simulationMaterial).map(key => key.split('_')[0])
                .filter((value, index, arr) => arr.indexOf(value)===index)">
                <option :value="keyLevel" :label="proNS.getLevelLabel(keyLevel)"></option>
            </template>
        </select>
        <span>{{__('Exercice')}}</span>
        <select x-model="exercise" :disabled="!level"
                x-on:change="levelExercise = level + '_' + exercise">
            <option value="" :label="__('Choix d’un exercice')" disabled :selected="true"></option>
            <template x-for="(keyExercise, index) in ['ex1','ex2','ex3']">
                <option :value="keyExercise" :label="__('Exercice ')+(index+1)"></option>
            </template>
        </select>
        <button :title="__('Importer le matériel correspondant')" type="button"
                class="col-grey bg-transparent border-none ml-10"
                :class="{ 'col-primary pointer-hover': exercise }"
                :disabled="!exercise"
                x-on:click="proNS.importMaterial(levelExercise); level=''; exercise=''">
            <i class="fas fa-check" ></i>
        </button>
    </div>
</div>
