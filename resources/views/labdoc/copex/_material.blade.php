<div class="material-header-wrapper flex-row-center justify-space-between">
    <div class="material-header flex-row-center gap-5">
        <span class="material-title fs-13 fw-bold col-grey">
            <span x-text="'{{__('Liste du matériel')}}'" :title="'{{__('inclus la verrerie, le matériel de mesure, les solutions, les réactifs, ...')}}'"></span>
        </span>
        <template x-if="proNS.ldEdition && sectionEdition">
            <button :title="'{{__('Ajouter un nouveau matériel.')}}'"
                    class="material-add-button col-primary pointer-hover p-3 bg-transparent border-radius-2 border-none visibility-hidden"
                    :class="{'visibility-visible': (proNS.isTeacher || proNS.LD.materialList.isEditable)}"
                x-on:click="m = proNS.addMaterial(); $dispatch('add-material-message', { material: m });">
                <i class="fa-solid fa-plus"></i>  
            </button>
        </template>
    </div>
    <template x-if="proNS.ldEdition && sectionEdition && proNS.isTeacher">
        @include('labdoc/copex/_student_configuration_tooltip', [
            'classWrapper' => 'material-authorizations-wrapper',
            'items' => [ 
                ['label' => __('Le matériel peut être édité ou ajouté'), 'xModel' => "proNS.LD.materialList.isEditable"],
                ['label' => __('Le matériel est (dé)sélectionnable'), 'xModel' => "proNS.LD.materialList.isSelectable"]
             ]
        ])
    </template>
    
</div>
<div>
    <div x-data="{ isMaterialClicked: false, materialObject: null }"
         x-on:prons-update-object-clicked.window="$nextTick(()=>{
             newMaterialObject = proNS.LD.materialList.listItems.find(m=>materialObject && m.id===materialObject.id);
             if(newMaterialObject!==undefined){
                 materialObject = newMaterialObject;
                 isMaterialClicked = sectionEdition;
             }else {
                isMaterialClicked = false;
             }
         })"
         x-on:add-material-message.window="$nextTick(() => { materialObject = $event.detail.material; isMaterialClicked = true; })">
        @include('labdoc/copex/_material_edition_list')
        <template x-if="sectionEdition">
            @include('labdoc/copex/_material_edition_details')
        </template>
    </div>
</div>
