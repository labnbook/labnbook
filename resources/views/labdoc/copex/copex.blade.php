<?php
/**
 * @var \App\Labdoc $ld
 * @var int $edition
 * @var \App\Helper $helper
 * @var Object $quantities_data
 * @var boolean $is_teacher
 * @var boolean $is_super_manager
 */

if (isset($ld->report)) {
    $simulation_code = $ld->report->mission->simulation_code ?? '';
} else {
    $simulation_code = $ld->mission->simulation_code ?? '';
}
if ($simulation_code === 'titrab') {
    $simulation_material = file_get_contents("./tool_copex/v2_json/titrab/titrab_material.json");
} else {
    $simulation_material ='';
}
$copex_configuration_structured_actions_title = __("Actions structurées");
$copex_configuration_structured_actions_message = __("Les actions structurées permettent de guider le travail de conception des étudiants. <br /><br />Certaines actions structurées proposent aux étudiants de sélectionner du matériel dans une liste. Dans ce cas, il est possible de donner un type au matériel afin de restreindre le choix des étudiants.<br />");
?>
<div class="protocol" 
     x-data="{ proNS: protocolCtrl({{$ld->id_labdoc}},{{$is_teacher}},{{$is_super_manager}},{{$edition}},{{$quantities_data}},'{{$simulation_code}}',`{{$simulation_material}}`) }"
     x-init="proNS.init(); $watch('proNS.LD', ()=>proNS.updateUndoRedo()); $nextTick(()=>{ if (proNS.ldEdition) { window.scrollTo({left:0, top:$el.parentElement.offsetTop-150, behavior: 'smooth'}); }})"
     x-on:prons-undo-redo-action.window="proNS.undoRedo($event.detail)"
     x-on:procedure-content-required.window="proNS.sendContentBack($event.detail.id_ld)">

    <template x-if="!proNS.isTeacher && proNS.ldEdition && proNS.simulationCode && (window.global_scope==='test' || proNS.isSuperManager)">
        {{-- In report test a teacher can reset his knowledge skills --}}
        <div class="protocol-reset-feedback-knowledge flex-row-center">
            <button class="protocol-button-switch-advanced-configuration border-none p-3 bg-transparent border-radius-2 bg-whitesmoke bg-lightgrey-hover col-primary ml-auto pointer-hover outline-lightgrey-hover" type="button"
                    :title="__('Remettre à 0 son niveau de connaissance')"
                    x-on:click="proNS.simulation.resetFeedbackKnowledge()"
                    x-text="__('Réinitialiser les niveaux de rétroaction')">
            </button>
        </div>
    </template>
    
    <template x-if="proNS.isTeacher && proNS.ldEdition && (!proNS.simulationCode || proNS.isSuperManager)">
        {{-- Display the structured actions and material types edition --}}
        <div class="protocol-switch-advanced-configuration flex-row-center">
            <button class="protocol-button-switch-advanced-configuration border-none p-3 bg-transparent border-radius-2 bg-whitesmoke bg-lightgrey-hover col-primary ml-auto pointer-hover outline-lightgrey-hover" 
                    type="button"
                    x-on:click.stop="proNS.LD.isAdvancedSettings = !proNS.LD.isAdvancedSettings">
                <span x-text="proNS.LD.isAdvancedSettings?'{{__('Cacher la configuration des actions structurées')}}':'{{__('Afficher la configuration des actions structurées')}}'"></span>
            </button>
            <i class="fa fa-info-circle lba_icon_default ml-5"
               x-on:mouseover="global_tooltip_popup.openTooltipPopup('{{ $copex_configuration_structured_actions_title }}', '{{ $copex_configuration_structured_actions_message }}', $el)"
               x-on:click="global_tooltip_popup.openTooltipPopup('{{ $copex_configuration_structured_actions_title }}', '{{ $copex_configuration_structured_actions_message }}', $el)">
            </i>
        </div>
    </template>
    
    <template x-if="proNS.isTeacher && proNS.ldEdition && proNS.simulationCode">
        {{-- Simulation presentation --}}
        @include('labdoc/copex/_simulation_teacher_presentation')
    </template>
    
    <div class="overall-concern-wrapper" x-data="{ firstInit:false }">
        {{-- Overall concern blade --}}
        @include('labdoc/copex/_overall_concern')
    </div>

    <div class="material-wrapper mb-5 p-5" x-data="{ sectionEdition: false, firstInit:false}"
         x-on:click.outside="$nextTick(() => { sectionEdition = false })"
         x-on:click="$nextTick(() => { if (proNS.ldEdition && !sectionEdition) { sectionEdition = true; }})"
         x-on:close-edition.window="sectionEdition=false"
         :class="{ 'pointer-hover outline-lightgrey-hover': proNS.ldEdition && !sectionEdition, 'bg-whitesmoke': sectionEdition }">
        {{-- Material blade --}}
        @include('labdoc/copex/_material')
    </div>

    <template x-if="proNS.isTeacher && (!proNS.simulationCode || proNS.isSuperManager) && proNS.LD.isAdvancedSettings">
        <div class="structured-action-wrapper mb-5 p-5" x-data="{ sectionEdition: false, firstInit:false }"
             x-on:click.outside="$nextTick(() => { sectionEdition = false })"
             x-on:click="$nextTick(() => { if (proNS.ldEdition && !sectionEdition) { sectionEdition = true; }})"
             x-on:close-edition.window="sectionEdition=false"
             :class="{ 'pointer-hover outline-lightgrey-hover': proNS.ldEdition && !sectionEdition, 'bg-whitesmoke': sectionEdition }">
            {{-- Structured action blade --}}
            @include('labdoc/copex/_structured_action')
        </div>
    </template>

    <template x-if="!(proNS.isTeacher && proNS.simulationCode) || proNS.isSuperManager">
        <div class="protocol-wrapper mb-5 p-5" x-data="{ sectionEdition: proNS.ldEdition, firstInit:false }">
            {{-- Protocol blade --}}
            @include('labdoc/copex/_protocol')
        </div>
    </template>

    @include('labdoc/copex/_simulation')
</div>
