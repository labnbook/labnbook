<?php
$depth = 0;
?>

<div class="protocol-header p-0 w-100p flex-row-center justify-space-between gap-5">
    <span class="protocol-header-title fs-13 fw-bold col-grey" x-text="'{{__('Mode opératoire')}}'"></span>
    <template x-if="proNS.ldEdition && proNS.isTeacher">
        @include('labdoc/copex/_student_configuration_tooltip', [
            'classWrapper' => 'protocol-authorizations-wrapper',
            'items' => [ 
                ['label' => __('Ajout d’étapes possibles à la racine du mode opératoire'), 'xModel' => "proNS.LD.protocol.isSubStep"],
                ['label' => __('Ajout d’actions possible à la racine du mode opératoire'), 'xModel' => "proNS.LD.protocol.isInsideAction"]
             ]
        ])
    </template>
</div>

<ul class="protocol-list-root list-style-type-none p-0" x-init="if(proNS.ldEdition){proNS.initializeSortingNodes($el); }"
    data-node-id="root" :class="{ 'edit-mode': sectionEdition  }"
    x-data="{ node: null, rootParent:true }">
    <template x-for="(node,index) in proNS.LD.protocol.nodes" :key="'node_' + node.id + '_' + node.position + '_' + proNS.undoRedoDone + '_'
              + (node.nodeType === 'action' ? '_' + node.parameterValues.map(p => p.parameterId).join('_') : '')">
        <li class="protocol-ul-li node-item list-style-type-none pv-0"
            :class="{ 'editable-node':proNS.isTeacher || node.isEditable || node.nodeType==='step', 'edit-node': node.inEdition, 'action': node.nodeType === 'action', 'step': node.nodeType === 'step'}"
            x-init="$watch('node.inEdition', () => {if(!node.inEdition){
                            proNS.traceUserAction('student_modify_'+node.nodeType,node)
                            } else {
                            proNS.saveCurrentNodeEditedState(node);
                        }})"
            x-data="{rootParent:false}"
            :data-node-id="node.id">
            @include('labdoc/copex/_protocol_step',['depth'=>$depth])
            @include('labdoc/copex/_protocol_action')
        </li>
    </template>
    @include('labdoc/copex/_protocol_new_node_buttons',['depth'=>$depth, 'add_inside'=>0])
</ul>
