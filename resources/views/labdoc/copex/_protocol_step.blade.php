<?php
/**
 * @var int $depth
 */
?>

<template x-if="node.nodeType === 'step'">
    <div class="protocol-step-item step-item"
         :data-node-id="node.id"
         :class="{'outline-lightgrey':node.isActive || node.inEdition || node.nodes.some(n => n.nodeType === 'action' && n.inEdition),
                'pl-20 mb-2': node.inEdition}"
         x-on:click="if(proNS.ldEdition && sectionEdition){
                    const relatedStep = $event.target.closest('.step-item');
                    $nextTick(()=>{node.isActive = (relatedStep && parseInt(relatedStep.dataset.nodeId) === parseInt(node.id))});
                }"
         x-on:click.outside="node.isActive = false"
         x-on:close-edition.window="if(parseInt($event.detail.id_node) !== parseInt(node.id)){node.isActive=false;}"
         x-data="{stepDepth:{{$depth}}}">
        <div :class="{'bg-whitesmoke pv-5 mt-5':node.inEdition}"
             class="protocol-step-item-wrapper"
             x-on:click="if(proNS.ldEdition && sectionEdition){
                    proNS.closeAllEditContent(node.id); 
                    $nextTick(()=>{node.inEdition = true;});
                    }"
             x-on:click.outside="node.inEdition=false;"
             x-on:close-edition.window="if(parseInt($event.detail.id_node) !== parseInt(node.id)){node.inEdition=false;}">
            @include('labdoc/copex/_protocol_step_header')
        </div>
        <ul x-init="if(proNS.ldEdition){proNS.initializeSortingNodes($el);}" 
            class="protocol-step-item-content p-0 ml-26 pb-5"
            :data-node-id="node.id">
            <template x-for="(node, index) in node.nodes" :key="'step_node_'+node.id + '_' + node.position + '_'
              + (node.nodeType === 'action' ? '_' + node.parameterValues.map(p => p.parameterId).join('_') : '')">
                <li class="protocol-step-item-sub-node node-item list-style-type-none pv-0"
                    :class="{ 'editable-node':proNS.isTeacher || node.isEditable || node.nodeType==='step',
                    'edit-node': node.inEdition, 'action': node.nodeType === 'action', 'step': node.nodeType === 'step' }"
                    :data-node-id="node.id"
                    x-init="$watch('node.inEdition', () => {if(!node.inEdition){
                            proNS.traceUserAction('student_modify_'+node.nodeType,node)
                        } else {
                            proNS.saveCurrentNodeEditedState(node);
                        }
                        })"
                    x-on:mouseenter="$el.closest('ul').closest('li').classList.add('child-hover')"
                    x-on:mouseleave="$el.closest('ul').closest('li').classList.remove('child-hover')">
                    @if($depth<4)
                        @include('labdoc/copex/_protocol_step',['depth'=>$depth + 1])
                    @endif
                    @include('labdoc/copex/_protocol_action')
                </li>
            </template>
            @include('labdoc/copex/_protocol_new_node_buttons',['depth'=>$depth + 2, 'add_inside'=>1])
        </ul>
    </div>
</template>
