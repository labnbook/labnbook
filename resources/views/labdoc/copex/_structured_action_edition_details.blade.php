<template x-if="isStructuredActionClicked">
    <div class="structured-action-details flex-column mv-5 p-5 outline-lightgrey"
         x-data="{ initialName: '', isActionTextPartEditable: true }"
         x-init="initialName = structuredActionObject.name"
         x-on:click.outside="isStructuredActionClicked = false;if (!structuredActionObject.isFreeAction && structuredActionObject.name === '') { structuredActionObject.name = initialName; } ">
        <template x-if="structuredActionObject.isFreeAction">
            <div class="structured-action-details-free-action-wrapper flex-row-center justify-space-between">
                <div class="structured-action-details-free-action-description col-grey" x-text="'{{__('L’action libre est une action non structurée qui permet à l’étudiant de créer une action personnalisée.')}}'"></div>
                <template x-if="proNS.ldEdition && sectionEdition && proNS.isTeacher">
                    <div>
                        <label class="pointer-hover col-grey">
                            <input type="checkbox" x-model="structuredActionObject.isUsable"/>
                            {{__('Action accessible aux étudiants')}}
                        </label>
                    </div>
                </template>
            </div>
        </template>
        <template x-if="!structuredActionObject.isFreeAction">
            <div>
                {{-- Structured action name --}}
                <div class="structured-action-details-wrapper flex-row-center justify-space-between">
                    <div class="structured-action-details-name-wrapper flex-row-start gap-5 w-50p mb-5" :class="{ 'col-danger': structuredActionObject.name === '' || proNS.nameAlreadyExists(structuredActionObject, proNS.LD.actionList) }">
                        <div class="structured-action-details-name flex-grow-1 display-grid">
                            <textarea class="structured-action-details-name-textarea grid-area-1-1-2-2 font-inherit p-5 border-none outline-lightgrey overflow-hidden resize-none" rows="1"
                                      :class="{ 'input-error': structuredActionObject.name === '' || proNS.nameAlreadyExists(structuredActionObject, proNS.LD.actionList) }"
                                      x-model="structuredActionObject.name"
                                      x-init="$nextTick(()=>{$el.focus()})"
                                      x-on:prons-focus-structured-action-name.window="$nextTick(()=>{$el.focus()})"
                                      :placeholder="'{{__('Nom de l’action')}}'"></textarea>
                            <div x-text="(structuredActionObject.name || '{{__('Nom de l’action')}}')+' '"
                                 class="structured-action-details-name-div grid-area-1-1-2-2 font-inherit p-5 border-none outline-lightgrey visibility-hidden pre-wrap"></div>
                        </div>
                        <template x-if="structuredActionObject.name === '' || proNS.nameAlreadyExists(structuredActionObject, proNS.LD.actionList)">
                            <i class="structured-action-details-name-alert fas fa-circle-exclamation align-self-center" :title="'{{__('Il faut renseigner un nom d’action unique.')}}'"></i>
                        </template>
                        <button type="button"
                                class="structured-action-details-name-delete col-primary pointer-hover bg-transparent border-none ml-10 mt-2"
                                x-on:click="isStructuredActionClicked = false;">
                            <i class="fas fa-check" ></i>
                        </button>
                        <button :title="'{{__('Supprimer cette action')}}'" type="button"
                                class="structured-action-details-name-delete col-primary pointer-hover bg-transparent border-none ml-10 mt-2"
                                x-on:click="isStructuredActionClicked = !proNS.removeStructuredAction(structuredActionObject)">
                            <i class="fas fa-trash-can" ></i>
                        </button>
                    </div>
                    <template x-if="proNS.ldEdition && sectionEdition && proNS.isTeacher">
                        <div>
                            <label class="pointer-hover col-grey">
                                <input type="checkbox" x-model="structuredActionObject.isUsable"/>
                                {{__('Action accessible aux étudiants')}}
                            </label>
                        </div>
                    </template>
                </div>
                <template x-if="proNS.isTeacher && proNS.simulationCode">
                    <div class="flex-column-baseline gap-5">
                        <div class="flex-row-center gap-5">
                            <h4 x-text="'{{__('Variable de simulation')}}'"></h4>
                            <select x-model="structuredActionObject.simulationVariable">
                                <option value="" :label="'{{__('Choix d’une variable...')}}'" :selected="!structuredActionObject.simulationVariable"></option>
                                <template x-for="name in proNS.getSimulationActionVariableNames()">
                                    <option :value="name" :label="name" :selected="name === structuredActionObject.simulationVariable" :disabled="proNS.isSimulationVariableUsedinList(name, proNS.LD.actionList)"></option>
                                </template>
                            </select>
                        </div>
                        {{-- remove x-show to display this part --}}
                        <div x-show="false">
                            <label class="pointer-hover">{{__('Produit une mesure simulée')}}
                                <input type="checkbox" x-model="structuredActionObject.canBeSimulated"
                                       x-effect="structuredActionObject.canBeSimulated = (structuredActionObject.canBeSimulated?true:false)"
                                       class="structured-action-details-can-be-simulated-input m-0 p-0" />
                            </label>
                        </div>
                    </div>
                </template>
                <div x-data="{isActionTextPartClicked:false,selectedActionTextPart:null}"
                     x-on:close-edition.window="isActionTextPartClicked=false"
                     x-on:prons-update-object-clicked.window="$nextTick(()=>{
                             newActionTextPart = structuredActionObject.actionTextParts.find(atp=>selectedActionTextPart && atp.id===selectedActionTextPart.id);
                             if(newActionTextPart!==undefined){
                                 selectedActionTextPart = newActionTextPart;
                                 isActionTextPartClicked = sectionEdition;
                             }else {
                                isActionTextPartClicked = false;
                             }
                         })">
                    <div class="structured-action-details-content min-h-1em line-height-2em p-10 mv-5 focus-child bg-white"
                         x-data="{sortableStructuredAction:null}"
                         x-on:click="$nextTick(()=>{if(!$el.contains(document.activeElement)){$dispatch('prons-focus-last-text',{})}})"
                         x-init="structuredActionObject.update=true;$nextTick(()=>{sortableStructuredAction=proNS.initializeSortingStructuredActionTextParts($el, structuredActionObject)})"
                         x-on:re-initialize-sorting.window="sortableStructuredAction.destroy();$nextTick(()=>{sortableStructuredAction=proNS.initializeSortingStructuredActionTextParts($el, structuredActionObject)})">
                        <template x-for="(actionTextPart, index) in structuredActionObject.actionTextParts"
                                  :key="'structured-action_' + structuredActionObject.id + '_' + actionTextPart.id + '_'+structuredActionObject.update">
                            <span class="sortable-structured-action-text-part-item">
                                <template x-if="actionTextPart.domElement === 'text'">
                                    <span contentEditable="true" class="structured-action-details-content-text-editable outline-none-focus"
                                          x-on:prons-focus-last-text.window="if(index===structuredActionObject.actionTextParts.length-1){$nextTick(()=>{proNS.setEndOfContenteditable($el)});}"
                                          :class="{'ph-5':index>0,'pr-5':index===0}"
                                          x-init="$el.innerText = actionTextPart.value"
                                          x-on:keydown.enter.stop.prevent
                                          x-on:keyup="actionTextPart.value = $el.innerText"
                                          :data-placeholder="(index===0 && structuredActionObject.actionTextParts.length<=1)?'{{__('Texte de l’action...')}}':''"></span>  
                                </template>
                                <template x-if="actionTextPart.domElement !== 'text'">
                                    <span contentEditable="false" tabindex="0" 
                                          x-on:keyup="$nextTick(()=>{if($event.code==='Delete'){ 
                                            isActionTextPartClicked = !proNS.removeActionTextPart(selectedActionTextPart, structuredActionObject);}
                                            if (selectedActionTextPart.materialCreation) { $dispatch('prons-remove-material-from-protocol', { structuredActionId:structuredActionObject.id, actionTextPartId: selectedActionTextPart.id })}})"
                                          class="structured-action-details-content-placeholder bg-lightgrey pointer-hover sortable-structured-action-text-part-handle"
                                          :class="{'bg-primary col-white':isActionTextPartClicked && selectedActionTextPart.id === actionTextPart.id}"
                                          x-on:click.stop="$nextTick(()=>{isActionTextPartClicked = true; selectedActionTextPart = actionTextPart})"
                                          x-text="proNS.getPlaceholder(actionTextPart.domElement, actionTextPart.isMaterialList)"></span>
                                </template>
                            </span>
                        </template>
                        <span contentEditable="false" class="structured-action-details-content-field mh-5">
                            <select class="structured-action-details-content-field-select border-none outline-lightgrey bg-whitesmoke p-3 pointer-hover"
                                    x-on:click.stop
                                    x-on:change="firstInit=true; 
                                    selectedActionTextPart = proNS.addElementToStructuredAction(structuredActionObject, $event.target.value); 
                                    isActionTextPartClicked = true;
                                    $nextTick(() => { $event.target.selectedIndex=0;})">
                                <option value="" :label="'{{__('Ajouter un paramètre...')}}'" disabled selected></option>
                                <option value="numberSelect" :label="'{{__('Grandeur')}}'"></option>
                                <option value="selectMaterial" :label="'{{__('Matériel utilisé')}}'"></option>
                                <option value="materialInputText" :label="'{{__('Matériel créé')}}'"></option>
                                <option value="inputText" :label="'{{__('Champ libre')}}'"></option>
                                <option value="selectCustom" :label="'{{__('Item de liste')}}'"></option>
                            </select>
                        </span>
                    </div>
                    <template x-if="isActionTextPartClicked">
                        <div class="structured-action-text-part-wrapper w-100p p-5 mt-10 bg-whitesmoke outline-lightgrey"
                             x-on:click.outside="isActionTextPartClicked = false">
                            <button class="structured-action-text-part-delete col-primary pointer-hover float-right bg-transparent border-none"
                                    type="button" :title="'{{__('Supprimer cette partie')}}'"
                                    x-on:click=" 
                                    await $nextTick(); isActionTextPartClicked = !proNS.removeActionTextPart(selectedActionTextPart, structuredActionObject);
                                    if (selectedActionTextPart.materialCreation) { $dispatch('prons-remove-material-from-protocol', { structuredActionId:structuredActionObject.id, actionTextPartId: selectedActionTextPart.id });}">
                                <i class="fas fa-trash-can"></i>
                            </button>
                            {{-- input text detail --}}
                            <template x-if="selectedActionTextPart.domElement === 'inputText'">
                                <div class="structured-action-text-part-input-text flex-column-baseline gap-5">
                                    <div class="structured-action-text-part-input-text-info col-grey" x-text="'{{__('Un champ libre peut contenir tout texte')}}'"></div>
                                    <div class="structured-action-text-part-input-text-property flex-row-center gap-5">
                                        <span class="structured-action-text-part-input-text-property-title col-grey" x-text="'{{__('Taille du champ (en nombre de caractères) :')}}'"></span>
                                        <input class="structured-action-text-part-input-text-property-input font-inherit w-50 p-3 border-none outline-lightgrey" type="number" min="0" x-model="selectedActionTextPart.lengthMax"/>
                                    </div>
                                </div>
                            </template>

                            {{-- select detail --}}
                            <template x-if="selectedActionTextPart.domElement === 'select'" >
                                <div class="structured-action-text-part-select-wrapper flex-column-baseline gap-5">
                                    <template x-if="selectedActionTextPart.isMaterialList">
                                        <div class="structured-action-text-part-select-content flex-column-baseline gap-5">
                                            <div class="structured-action-text-part-select-info col-grey mb-5" x-text="'{{__('Matériel utilisé dans l’action, choisi avec un menu déroulant')}}'"></div>
                                            <div class="structured-action-text-part-select-types-wrapper flex-row-center gap-5">
                                                <span x-text="'{{__('Le matériel peut être choisi parmi les types suivants : ')}}'" class="structured-action-text-part-select-type-info col-grey"></span>
                                                <template x-if="selectedActionTextPart.filterTypes && selectedActionTextPart.filterTypes.length > 0">
                                                    <div class="structured-action-text-part-select-options flex-row-center gap-5">
                                                        <template x-for="type in selectedActionTextPart.filterTypes">
                                                            <div class="structured-action-text-part-select-option flex-row-center gap-5">
                                                                <span x-text="type.name"></span>
                                                                <button class="structured-action-text-part-select-option-delete pointer-hover bg-transparent border-none col-primary"
                                                                        x-on:click="proNS.removeType(type.name, selectedActionTextPart.filterTypes)"
                                                                        :title="'{{__('Supprimer ce type')}}'">
                                                                    <i class="fas fa-trash-can"></i>
                                                                </button>
                                                            </div>
                                                        </template>
                                                    </div>
                                                </template>
                                                <select class="structured-action-text-part-select-options-add border-none outline-lightgrey bg-whitesmoke p-3 pointer-hover" 
                                                        :title="'{{__('Ajouter un type')}}'"
                                                        x-on:change="proNS.addType($event.target.options[$event.target.selectedIndex].value, selectedActionTextPart, 'filterTypes');
                                                            $nextTick(() => { $event.target.selectedIndex=0;})">
                                                    <option value="" label="" selected></option>
                                                    <template x-for="type in proNS.getAllMaterialTypes(selectedActionTextPart.filterTypes)">
                                                        <option :value="type.name" :label="type.name"></option>
                                                    </template>
                                                </select>
                                            </div>
                                        </div>
                                    </template>
                                    <template x-if="!selectedActionTextPart.isMaterialList">
                                        <div class="structured-action-text-part-custom-list-wrapper flex-column-baseline gap-5" 
                                             x-data="{isListClicked:!isNaN(parseInt(selectedActionTextPart.listId))}" 
                                             x-effect="isListClicked=!isNaN(parseInt(selectedActionTextPart.listId))">
                                            <div class="structured-action-text-part-choose-custom-list flex-row-center gap-5">
                                                <span class="structured-action-text-part-choose-custom-list-info col-grey" x-text="'{{__('Choix dans une liste')}} : '"></span>
                                                <select class="structured-action-text-part-custom-list-select border-none outline-lightgrey bg-whitesmoke p-3 pointer-hover" 
                                                        x-model="selectedActionTextPart.listId"
                                                        x-on:change="if($event.target.value){isListClicked=true;} else {isListClicked=false;}"
                                                        :title="'{{__('Sélectionner une liste')}}'">
                                                    <option value="" label=""></option>
                                                    <template x-for="list in proNS.LD.customLists">
                                                        <option :value="list.id" :label="list.listName || '{{__('Nom de la liste')}}'" :selected="parseInt(list.id) === parseInt(selectedActionTextPart.listId)"></option>
                                                    </template>
                                                </select>
                                            </div>
                                            <template x-if="!isListClicked">
                                                <div class="flex-row-center gap-5">
                                                    <span class="structured-action-text-part-new-custom-list-info col-grey" x-text="'{{__('Ajouter une nouvelle liste.')}}'"></span>
                                                    <button :title="'{{__('Ajouter une nouvelle liste.')}}'"
                                                            x-on:click="selectedActionTextPart.listId=proNS.addCustomList().id; isListClicked=true;"
                                                            class="structured-action-text-part-custom-list-add-button col-primary pointer-hover p-3 bg-transparent border-radius-2 border-none">
                                                        <i class="fas fa-plus"></i>
                                                    </button>
                                                </div>
                                            </template>
                                            <template x-if="isListClicked && proNS.LD.customLists.some(list => parseInt(list.id) === parseInt(selectedActionTextPart.listId))">
                                                <div x-data="{selectedCustomList:proNS.LD.customLists.find(list => parseInt(list.id) === parseInt(selectedActionTextPart.listId))}"
                                                     x-effect="if(selectedActionTextPart && selectedActionTextPart.domElement === 'select' && !selectedActionTextPart.isMaterialList){
                                                     selectedCustomList=proNS.LD.customLists.find(list => parseInt(list.id) === parseInt(selectedActionTextPart.listId));
                                                     }"
                                                     class="structured-action-text-part-custom-list w-100p">
                                                    @include('labdoc/copex/_custom_lists')
                                                </div>
                                            </template>
                                        </div>
                                    </template>
                                </div>
                            </template>

                            {{-- number select detail --}}
                            <template x-if="selectedActionTextPart.domElement === 'numberSelect'" >
                                <div class="structured-action-text-part-quantity-wrapper flex-column-baseline gap-5">
                                    <div class="structured-action-text-part-quantity-info col-grey" x-text="'{{__('Une grandeur est un champ comprenant une valeur numérique et un menu déroulant d’unité (si besoin)')}}'"></div>
                                    <div class="structured-action-text-part-quantity flex-row-center gap-5">
                                        <span class="structured-action-text-part-quantity-title col-grey" x-text="'{{__('Type de grandeur : ')}}'"></span>
                                        <select class="structured-action-text-part-quantity-select border-none outline-lightgrey bg-whitesmoke p-3 pointer-hover" x-model="selectedActionTextPart.listId">
                                            <option value="" label=""></option>
                                            <template x-for="quantity in proNS.quantitiesData">
                                                <option :value="quantity.id" :label="quantity.name[i18next.language]" :selected="quantity.id===selectedActionTextPart.listId"></option>
                                            </template>
                                        </select>
                                    </div>
                                </div>
                            </template>

                            {{-- material input detail --}}
                            <template x-if="selectedActionTextPart.domElement === 'materialInputText'">
                                <div class="structured-action-text-part-created-material-wrapper flex-column-baseline gap-5" x-data="{ id: $id(`ld-${proNS.id_ld}-copex`)}">
                                    <div class="structured-action-text-part-created-material-info col-grey" x-text="'{{__('Champ libre permettant d’indiquer le nom d’un matériel créé au cours de l’action')}}'"></div>
                                    <div class="structured-action-text-part-created-material flex-row-center gap-5">
                                        <span x-text="'{{__('Type(s) du matériel créé :')}}'" class="structured-action-text-part-created-material-title col-grey"></span>
                                        <template x-for="type in selectedActionTextPart.types">
                                            <div class="structured-action-text-part-created-material-type flex-row-center gap-5">
                                                <span x-text="type.name"></span>
                                                <button class="structured-action-text-part-created-material-type-delete pointer-hover bg-transparent border-none col-primary mr-10"
                                                        x-on:click="proNS.removeType(type.name, selectedActionTextPart.types);
                                                        $dispatch('prons-update-material-types-from-protocol', { structuredActionId:structuredActionObject.id, actionTextPartId: selectedActionTextPart.id })"
                                                        :title="'{{__('Supprimer ce type')}}'">
                                                    <i class="fas fa-trash-can"></i>
                                                </button>
                                            </div>
                                        </template>
                                        <div class="structured-action-text-part-created-material-type-edit flex-row-center gap-5 ml-5" x-data="{ newType: null }">
                                            <input :list="id" type="text" class="min-w-40 w-40 p-2" x-init="$nextTick(()=>{$el.style.width = Math.max(35,proNS.getTextWidth($el))+5 + 'px';})"
                                                   x-model="newType" 
                                                   x-on:change="proNS.addType(newType, selectedActionTextPart, 'types'); newType=null;
                                                   $dispatch('prons-update-material-types-from-protocol', { structuredActionId:structuredActionObject.id, actionTextPartId: selectedActionTextPart.id })"
                                                   x-on:click.outside="proNS.addType(newType, selectedActionTextPart, 'types'); newType=null;
                                                   $dispatch('prons-update-material-types-from-protocol', { structuredActionId:structuredActionObject.id, actionTextPartId: selectedActionTextPart.id })"
                                                   x-on:keyup.enter="proNS.addType(newType, selectedActionTextPart, 'types'); newType=null;
                                                   $dispatch('prons-update-material-types-from-protocol', { structuredActionId:structuredActionObject.id, actionTextPartId: selectedActionTextPart.id })"
                                                   x-on:input="if ($event.inputType === 'insertReplacementText') { 
                                                                    proNS.addType(newType, selectedActionTextPart, 'types'); newType=null;
                                                                    $dispatch('prons-update-material-types-from-protocol', { structuredActionId:structuredActionObject.id, actionTextPartId: selectedActionTextPart.id })
                                                               } else { $el.style.width = Math.max(15,proNS.getTextWidth($el))+10 + 'px'; }" />
                                            <datalist :id="id">
                                                <template x-for="type in proNS.getAllMaterialTypes(selectedActionTextPart.types)">
                                                    <option :value="type.name" x-text="type.name"></option>
                                                </template>
                                            </datalist>
                                        </div>
                                    </div>
                                </div>
                            </template>
                        </div>
                    </template>
                    <template x-if="proNS.isTeacher && proNS.simulationCode && selectedActionTextPart">
                        <div class="flex-row-center gap-5 mv-5">
                            <h4 x-text="'{{__('Variable de simulation')}}'"></h4>
                            <select x-model="selectedActionTextPart.simulationVariable">
                                <option value="" :label="'{{__('Choix d’une variable...')}}'" :selected="!selectedActionTextPart.simulationVariable"></option>
                                <template x-for="name in proNS.getSimulationTextPartVariableNames()">
                                    <option :value="name" :label="name" :selected="name === selectedActionTextPart.simulationVariable" :disabled="proNS.isSimulationVariableUsedinList(name, proNS.LD.actionList.flatMap(action =>  action.actionTextParts))"></option>
                                </template>
                            </select>
                        </div>
                    </template>
                </div>
            </div>
        </template>
    </div>
</template>
