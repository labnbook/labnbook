<ul class="structured-actions-list m-0 column-count-4" x-init="proNS.initializeSortingStructuredActionList($el)">
    <template x-for="(structuredAction, structuredActionIndex) in proNS.LD.actionList" :key="`action_${structuredAction.id}_${structuredAction.position}`">
        <li x-show="sectionEdition || structuredAction.isFreeAction || structuredAction.name" class="structured-action-item pr-5 pt-3 break-inside-avoid-column"
        :class="{'col-grey': !structuredAction.isUsable, 'sortable-structured-action-item':sectionEdition}">
            <span :class="{'fw-bold col-primary':sectionEdition && isStructuredActionClicked && structuredActionObject && structuredActionObject.id===structuredAction.id,
                    'col-danger':sectionEdition && !structuredAction.isFreeAction && (!structuredAction.name || proNS.nameAlreadyExists(structuredAction, proNS.LD.actionList)),
                    'pointer-hover':sectionEdition}" 
                  :title="!structuredAction.isUsable ? '{{__('Cette action n’est pas utilisable dans le protocole par les étudiants ')}}' : ''"
                  class="structured-action-item-name"
                  x-text="structuredAction.isFreeAction?'{{__('Action libre')}}':(structuredAction.name || '{{__('Nouvelle action')}}')" 
                  x-on:click.stop="if(proNS.ldEdition){
                  proNS.closeAllEditContent();sectionEdition=true;structuredActionObject = structuredAction; isStructuredActionClicked=true; proNS.reInitializeSorting();$dispatch('prons-focus-structured-action-name')
                  }"></span>
        </li>
    </template>
</ul>
