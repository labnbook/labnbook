<div class="structured-actions-header-wrapper p-0 m-0" x-data="{ isStructuredActionClicked:false, structuredActionObject: null }">
    <div class="structured-actions-header flex-row-center gap-5">
        <span class="structured-actions-title fs-13 fw-bold col-grey">
            <span x-text="'{{__('Liste des actions')}}'"></span>
        </span>
        <template x-if="sectionEdition">
            <button :title="'{{__('Ajouter une nouvelle action structurée.')}}'"
                    class="structured-actions-add-button col-primary pointer-hover p-3 bg-transparent border-radius-2 border-none"
                    x-on:click="firstInit=true; $nextTick(()=>{structuredActionObject = proNS.addStructuredAction();
                                isStructuredActionClicked = true; $dispatch('prons-focus-structured-action-name')})">
                <i class="fa-solid fa-plus"></i>
            </button>
        </template>
    </div>

    <div x-on:prons-update-object-clicked.window="$nextTick(()=>{
             newStructuredActionObject = proNS.LD.actionList.find(action=>structuredActionObject && action.id===structuredActionObject.id);
             if(newStructuredActionObject!==undefined){
                 structuredActionObject = newStructuredActionObject;
                 isStructuredActionClicked = sectionEdition;
             }else {
                isStructuredActionClicked = false;
             }
         })">
        @include('labdoc/copex/_structured_action_edition_list')
        <template x-if="sectionEdition">
            @include('labdoc/copex/_structured_action_edition_details')
        </template>
    </div>
</div>

