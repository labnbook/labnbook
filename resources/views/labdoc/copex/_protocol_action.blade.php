<template x-if="node.nodeType === 'action' && proNS.LD.actionList.some(a => parseInt(a.id) === parseInt(node.idStructuredAction))">
    <div class="protocol-action-item action-item pl-2"
         :class="{'node-handle-visible-on-hover':proNS.ldEdition, 'bg-whitesmoke pv-5 mt-5 outline-lightgrey ml-20':node.inEdition}"
         x-on:click="if(((node.isDeletable||node.isDuplicable||node.isEditable||proNS.isTeacher) && proNS.ldEdition && sectionEdition)){
         proNS.closeAllEditContent(node.id); node.inEdition=true;
         }" 
         x-on:click.outside="node.inEdition=false"
         x-on:close-edition.window="if(parseInt($event.detail.id_node) !== parseInt(node.id)){node.inEdition=false;}">
        <div class="flex-row-center justify-space-between ph-5" :class="{'mb-2': node.inEdition }">
            <template x-if="node.inEdition">
                @include('labdoc/copex/_protocol_node_buttons')
            </template>
            <template x-if="proNS.ldEdition && sectionEdition && proNS.isTeacher && node.inEdition">
                @include('labdoc/copex/_student_configuration_tooltip', [
                    'classWrapper' => 'protocol-action-item-authorizations-wrapper',
                    'items' => [ 
                            ['label' => __('Contenu et commentaire modifiable'), 'xModel' => "node.isEditable"],
                            ['label' => __('Action duplicable'), 'xModel' => "node.isDuplicable"],
                            ['label' => __('Action déplaçable'), 'xModel' => "node.isMovable"],
                            ['label' => __('Action supprimable'), 'xModel' => "node.isDeletable"],
                        ]
                ])
            </template>
        </div>
        <div class="protocol-action-item-details gap-5" :class="{'flex-row-baseline':!node.inEdition,'flex-row-start':node.inEdition}">
            <template x-if="!node.inEdition">
                <button :title="'{{__('Déplacer')}}'" class="protocol-action-item-details-move visibility-hidden border-none ph-2 border-radius-2 bg-transparent col-primary" type="button"
                        :class="{ 'visibility-hidden':!node.inEdition,'pointer-hover node-handle': proNS.isTeacher || node.isMovable, 'opacity-40':!(proNS.isTeacher || node.isMovable)}"
                        :disabled="!(proNS.isTeacher || node.isMovable)">
                    <i class="fa-solid fa-up-down-left-right move-hover"></i>
                </button>
            </template>
            <span class="protocol-action-item-details-dash fw-bold">⁃</span>
            <div :class="{'flex-column gap-5 flex-grow-1 pr-5':node.inEdition, 'flex-row-baseline gap-5':!node.inEdition}">
                <div class="protocol-action-item-details-content display-inline"
                     :class="{'line-height-1-8em p-5 outline-lightgrey bg-white':node.inEdition
                     && !proNS.LD.actionList.find(a => parseInt(a.id) === parseInt(node.idStructuredAction)).isFreeAction}">
                    {{-- Free action --}}
                    <template x-if="proNS.LD.actionList.find(a => parseInt(a.id) === parseInt(node.idStructuredAction)) 
                    && proNS.LD.actionList.find(a => parseInt(a.id) === parseInt(node.idStructuredAction)).isFreeAction">
                        <div class="protocol-action-item-free-action flex-grow-1">
                            <template x-if="!node.inEdition || (!node.isEditable && !proNS.isTeacher)">
                                <span class="protocol-action-item-default pre-wrap" :class="{'fs-italic col-grey':!node.parameterValues[0].value}"
                                      x-text="node.parameterValues[0].value || '{{__('Action')}}'"></span>
                            </template>
                            <template x-if="node.inEdition && ( node.isEditable || proNS.isTeacher)">
                                <div class="protocol-action-item-free-action-edit flex-grow-1 display-grid">
                                        <textarea class="protocol-action-item-free-action-textarea grid-area-1-1-2-2 font-inherit p-4 border-none outline-lightgrey overflow-hidden resize-none" rows="1"
                                                  x-model="node.parameterValues[0].value"
                                                  x-init="$nextTick(()=>{$el.focus()})"
                                                  :placeholder="'{{__('Description de l’action')}}'"></textarea>
                                    <div x-text="(node.parameterValues[0].value || '{{__('Description de l’action')}}')+' '"
                                         class="protocol-action-item-free-action-div grid-area-1-1-2-2 font-inherit p-5 border-none outline-lightgrey visibility-hidden pre-wrap"></div>
                                </div>
                            </template>
                        </div>
                    </template>
                    {{-- Custom action --}}
                    <template x-if="proNS.LD.actionList.find(a => parseInt(a.id) === parseInt(node.idStructuredAction)) 
                        && !proNS.LD.actionList.find(a => parseInt(a.id) === parseInt(node.idStructuredAction)).isFreeAction">
                        <template x-for="actionTextPart in proNS.getActionTextParts(node)" :key="'action_text_part_'+actionTextPart.id + '_' + actionTextPart.position">
                            <div class="protocol-action-item-custom-action display-inline m-0 p-0"
                                 :class="{'mh-5':node.inEdition && ( node.isEditable || proNS.isTeacher),
                                           'highlighted':node.parameterValues.find(p => parseInt(p.parameterId) === parseInt(actionTextPart.id))
                                           ?node.parameterValues.find(p => parseInt(p.parameterId) === parseInt(actionTextPart.id)).highlighted:false}">
                                <!-- text -->
                                <template x-if="actionTextPart.domElement === 'text'">
                                    <span class="pre-wrap" x-text="actionTextPart.value"></span>
                                </template>

                                <!-- inputText -->
                                <template x-if="actionTextPart.domElement === 'inputText' && actionTextPart.nodeParameter">
                                    <div class="protocol-action-item-input-text display-inline m-0 p-0">
                                        <template x-if="!node.inEdition || (!node.isEditable && !proNS.isTeacher)">
                                            <span class="protocol-action-item-input-text-read-only bg-overall-concern" :class="{'col-primary':actionTextPart.nodeParameter.valueIterationVariable}" 
                                                  x-text="proNS.getReadOnlyTextParameter(actionTextPart)"></span>
                                        </template>
                                        <template x-if="node.inEdition && ( node.isEditable || proNS.isTeacher)">
                                            <div class="protocol-action-item-input-text-edit position-relative display-inline" x-data="{selectIteration:false}"
                                                 x-on:prons-focus-action-input.window="$nextTick(()=>{
                                                 if($event.detail.nodeId === node.id && $event.detail.parameterId === actionTextPart.nodeParameter.parameterId){
                                                    $el.getElementsByTagName('input')[0].focus()}
                                                 });"
                                                 x-on:click="selectIteration = true"
                                                 x-on:click.outside="selectIteration = false">
                                                <template x-if="actionTextPart.nodeParameter.valueIterationVariable">
                                                    <span class="protocol-action-item-input-text-iteration pointer-hover bg-lightgrey-hover col-primary"
                                                          x-on:prons-update-action-text-part.window="if(($event.detail.nodeId && parseInt(node.id) === parseInt($event.detail.nodeId)) ||
                                                          ($event.detail.variableId && actionTextPart.nodeParameter.valueIterationVariable && parseInt($event.detail.variableId) === parseInt(actionTextPart.nodeParameter.valueIterationVariable.id))) {
                                                            actionTextPart.nodeParameter.valueIterationVariable = null;
                                                          }"
                                                          x-text="actionTextPart.nodeParameter.valueIterationVariable?actionTextPart.nodeParameter.valueIterationVariable.name:''"></span>
                                                </template>
                                                <template x-if="!actionTextPart.nodeParameter.valueIterationVariable">
                                                    <input class="protocol-action-item-input-text-no-iteration line-height-1-5em font-inherit border-none outline-lightgrey" x-model="actionTextPart.nodeParameter.value"
                                                           x-on:input="selectIteration = false;$el.style.width = Math.max(15,proNS.getTextWidth($el))+10 + 'px'"
                                                           x-init="$nextTick(()=>{$el.style.width = Math.max(15,proNS.getTextWidth($el))+10 + 'px'});"
                                                           :maxlength="actionTextPart.lengthMax"/>
                                                </template>
                                                <template x-if="selectIteration && proNS.getParentStepIterationVariables(node, 'notMaterial').length>0">
                                                    <div class="protocol-action-item-input-text-select flex-column-baseline gap-3 position-absolute z-index-1000 bg-white l-0 t-100p mt-3 overflow-y-auto max-h-120 min-w-15 outline-lightgrey">
                                                        <div class="protocol-action-item-input-text-select-no-iteration bg-lightgrey-hover pointer-hover w-100p pl-3 pr-10 no-wrap"
                                                             x-on:click.stop="selectIteration = false; actionTextPart.nodeParameter.valueIterationVariable = null; 
                                                             $dispatch('prons-focus-action-input',{nodeId:node.id,parameterId:actionTextPart.nodeParameter.parameterId});"
                                                             x-text="actionTextPart.nodeParameter.value || '{{__('Champ libre')}}'">
                                                        </div>
                                                        <div class="protocol-action-item-input-text-select-iteration-title fw-bold pl-3 pr-10 w-100p no-wrap" x-text="'{{__('Variables itératives :')}}'"></div>
                                                        <template x-for="variable in proNS.getParentStepIterationVariables(node,  'notMaterial')">
                                                            <div class="protocol-action-item-input-text-select-iteration-item bg-lightgrey-hover pointer-hover w-100p pr-10 pl-20 no-wrap"
                                                                 x-on:click.stop="selectIteration = false; actionTextPart.nodeParameter.valueIterationVariable = variable;"
                                                                 x-text="variable.name">
                                                            </div>
                                                        </template>
                                                    </div>
                                                </template>
                                            </div>
                                        </template>
                                    </div>
                                </template>

                                <!-- inputText to create a new material -->
                                <template x-if="actionTextPart.domElement === 'materialInputText' && actionTextPart.nodeParameter && actionTextPart.materialCreated">
                                    <div class="protocol-action-item-create-material display-inline m-0 p-0"
                                         x-on:prons-update-material-types-from-protocol.window="if (parseInt($event.detail.actionTextPartId) === parseInt(actionTextPart.id)
                                         && (parseInt($event.detail.structuredActionId) === parseInt(node.idStructuredAction))) {
                                            actionTextPart.materialCreated.types = actionTextPart.types;
                                         }"
                                         x-on:prons-remove-material-from-protocol.window.once="if (parseInt($event.detail.actionTextPartId) === parseInt(actionTextPart.id)
                                         && (parseInt($event.detail.structuredActionId) === parseInt(node.idStructuredAction))) { proNS.removeMaterial(actionTextPart.materialCreated); }">
                                        <template x-if="!node.inEdition || (!node.isEditable && !proNS.isTeacher)">
                                            <span class="protocol-action-item-create-material-read-only bg-overall-concern" :class="{'col-primary':actionTextPart.nodeParameter.valueIterationVariable}" 
                                                  x-text="proNS.getReadOnlyTextParameter(actionTextPart)"></span>
                                        </template>
                                        <template x-if="node.inEdition && ( node.isEditable || proNS.isTeacher)">
                                            <div class="protocol-action-item-create-material-edit position-relative display-inline" x-data="{selectIteration:false}"
                                                 x-on:prons-focus-action-input.window="$nextTick(()=>{
                                                 if($event.detail.nodeId === node.id && $event.detail.parameterId === actionTextPart.nodeParameter.parameterId){
                                                    $el.getElementsByTagName('input')[0].focus();
                                                 }});"
                                                 x-on:click="selectIteration = true"
                                                 x-on:click.outside="selectIteration = false">
                                                <template x-if="actionTextPart.nodeParameter.valueIterationVariable">
                                                    <span class="protocol-action-item-create-material-iteration pointer-hover bg-lightgrey-hover col-primary"
                                                          x-on:prons-update-action-text-part.window="if(($event.detail.nodeId && parseInt(node.id) === parseInt($event.detail.nodeId)) ||
                                                          ($event.detail.variableId && actionTextPart.nodeParameter.valueIterationVariable && parseInt($event.detail.variableId) === parseInt(actionTextPart.nodeParameter.valueIterationVariable.id))) {
                                                            actionTextPart.nodeParameter.valueIterationVariable = null;
                                                          }"
                                                          x-text="actionTextPart.nodeParameter.valueIterationVariable?actionTextPart.nodeParameter.valueIterationVariable.name:''"></span>
                                                </template>
                                                <template x-if="!actionTextPart.nodeParameter.valueIterationVariable && actionTextPart.materialCreated">
                                                    <input class="protocol-action-item-create-material-no-iteration line-height-1-5em font-inherit border-none outline-lightgrey" x-model="actionTextPart.materialCreated.name"
                                                           x-on:input="selectIteration = false;$el.style.width = Math.max(15,proNS.getTextWidth($el))+10 + 'px'"
                                                           x-on:focus="selectIteration = true"
                                                           x-init="$nextTick(()=>{$el.style.width = Math.max(15,proNS.getTextWidth($el))+10 + 'px'});"
                                                           :maxlength="actionTextPart.lengthMax"/>
                                                </template>
                                                <template x-if="selectIteration && proNS.getParentStepIterationVariables(node, 'material').length>0">
                                                    <div class="protocol-action-item-create-material-select flex-column-baseline gap-3 position-absolute z-index-1000 bg-white l-0 t-100p mt-3 overflow-y-auto max-h-120 w-130 outline-lightgrey">
                                                        <div class="protocol-action-item-create-material-select-no-iteration bg-lightgrey-hover pointer-hover pl-3 w-100p-3"
                                                             x-show="actionTextPart.nodeParameter.valueIterationVariable"
                                                             x-on:click.stop="selectIteration = false; actionTextPart.nodeParameter.valueIterationVariable = null; 
                                                             $dispatch('prons-focus-action-input',{nodeId:node.id,parameterId:actionTextPart.nodeParameter.parameterId});"
                                                             x-text="actionTextPart.materialCreated.name || '{{__('Nom du matériel')}}'">
                                                        </div>
                                                        <div class="protocol-action-item-create-material-select-iteration-title fw-bold pl-3" x-text="'{{__('Variables itératives :')}}'"></div>
                                                        <template x-for="variable in proNS.getParentStepIterationVariables(node, 'material')">
                                                            <div class="protocol-action-item-create-material-select-iteration-item bg-lightgrey-hover pointer-hover pl-20 w-100p-20"
                                                                 x-on:click.stop="selectIteration = false; actionTextPart.nodeParameter.valueIterationVariable = variable; actionTextPart.materialCreated.name = '';"
                                                                 x-text="variable.name">
                                                            </div>
                                                        </template>
                                                    </div>
                                                </template>
                                            </div>
                                        </template>
                                    </div>
                                </template>

                                <!-- select -->
                                <template x-if="actionTextPart.domElement === 'select' && actionTextPart.nodeParameter">
                                    <div class="protocol-action-item-select display-inline m-0 p-0"
                                        x-on:prons-remove-selected-material-from-protocol.window="if($event.detail.materialId === actionTextPart.nodeParameter.value){actionTextPart.nodeParameter.value=null;}">
                                        <template x-if="!node.inEdition || (!node.isEditable && !proNS.isTeacher)">
                                            <span class="protocol-action-item-select-read-only bg-overall-concern"
                                                  :class="{'col-primary':actionTextPart.nodeParameter.valueIterationVariable}"
                                                  x-on:prons-update-action-text-part.window="if(($event.detail.nodeId && parseInt(node.id) === parseInt($event.detail.nodeId)) ||
                                                          ($event.detail.variableId && actionTextPart.nodeParameter.valueIterationVariable && parseInt($event.detail.variableId) === parseInt(actionTextPart.nodeParameter.valueIterationVariable.id))) {
                                                            actionTextPart.nodeParameter.valueIterationVariable = null;
                                                          }"
                                                  x-text="proNS.getReadOnlyTextParameter(actionTextPart, node)"></span>
                                        </template>
                                        <template x-if="node.inEdition && ( node.isEditable || proNS.isTeacher)">
                                            <select class="protocol-action-item-select-edit border-none outline-lightgrey bg-whitesmoke ph-2 pointer-hover" 
                                                    x-model="actionTextPart.nodeParameter.value"
                                                    x-on:change="proNS.selectProtocolMaterialOption(actionTextPart, $event)">
                                                <option value="" label="" data-material-origin="none"></option>
                                                <template x-for="option in proNS.getSelectOptions(actionTextPart, node)">
                                                    <option :value="option.value" :label="option.label" data-material-origin="materialList"
                                                            :selected="parseInt(option.value) === parseInt(actionTextPart.nodeParameter.value)"></option>
                                                </template>
                                                <template x-if="proNS.getParentStepIterationVariables(node, 'material').length>0">
                                                    <optgroup :label="'{{__('Variable itérative')}}'">
                                                        <template x-for="variable in proNS.getParentStepIterationVariables(node, 'material')">
                                                            <option :value="variable.id" :label="variable.name" data-material-origin="iterativeVariable" x-init="$el.variable = variable"
                                                            :selected="(actionTextPart.nodeParameter.valueIterationVariable && actionTextPart.nodeParameter.valueIterationVariable.id === variable.id)"></option>
                                                        </template>
                                                    </optgroup>
                                                </template>
                                                <template x-if="proNS.getPreviouslyCreatedMaterialsByIteration(node).length>0 || proNS.getPreviouslyCreatedMaterials(actionTextPart, node).length>0">
                                                    <optgroup :label="'{{__('Matériels précédement créé')}}'">
                                                        <template x-for="option in proNS.getPreviouslyCreatedMaterials(actionTextPart, node)">
                                                            <option :value="option.value" :label="option.label" data-material-origin="materialList"
                                                                    :selected="parseInt(option.value) === parseInt(actionTextPart.nodeParameter.value)"></option>
                                                        </template>
                                                        <template x-for="materialName in proNS.getPreviouslyCreatedMaterialsByIteration(node)">
                                                            <option :value="materialName" :label="materialName" data-material-origin="materialCreated"
                                                            :selected="actionTextPart.nodeParameter.valueNameMaterialCreated === materialName"></option>
                                                        </template>
                                                    </optgroup>
                                                </template>
                                            </select>
                                        </template>
                                    </div>
                                </template>
                                
                                <!-- numberSelect -->
                                <template x-if="actionTextPart.domElement === 'numberSelect' && actionTextPart.nodeParameter">
                                    <div class="protocol-action-item-quantity display-inline" 
                                         x-effect="if(actionTextPart.nodeParameter.valueIterationVariable && proNS.getParentStepIterationVariables(node, 'physicalQuantity', actionTextPart).length===0) {
                                            actionTextPart.nodeParameter.valueIterationVariable=null;
                                         }">
                                        <template x-if="!node.inEdition || (!node.isEditable && !proNS.isTeacher)">
                                            <span class="protocol-action-item-quantity-read-only bg-overall-concern" :class="{'col-primary':actionTextPart.nodeParameter.valueIterationVariable}"
                                                  x-text="proNS.getReadOnlyTextParameter(actionTextPart, node)"></span>
                                        </template>
                                        <template x-if="node.inEdition && ( node.isEditable || proNS.isTeacher)">
                                            <div class="protocol-action-item-quantity-edit position-relative display-inline" x-data="{selectIteration:false}"
                                                 x-on:prons-focus-action-input.window="$nextTick(()=>{
                                                 if($event.detail.nodeId === node.id && $event.detail.parameterId === actionTextPart.nodeParameter.parameterId){
                                                    $el.getElementsByTagName('input')[0].focus();
                                                 }});"
                                                 x-on:click="selectIteration = true"
                                                 x-on:click.outside="selectIteration = false">
                                                <template x-if="actionTextPart.nodeParameter.valueIterationVariable">
                                                    <span class="protocol-action-item-quantity-iteration pointer-hover bg-lightgrey-hover col-primary"
                                                          x-on:prons-update-action-text-part.window="if(($event.detail.nodeId && parseInt(node.id) === parseInt($event.detail.nodeId)) ||
                                                          ($event.detail.variableId && actionTextPart.nodeParameter.valueIterationVariable 
                                                          && parseInt($event.detail.variableId) === parseInt(actionTextPart.nodeParameter.valueIterationVariable.id))) {
                                                            actionTextPart.nodeParameter.valueIterationVariable = null;
                                                          }"
                                                          x-text="proNS.getReadOnlyTextValueIterationNumberSelect(actionTextPart.nodeParameter.valueIterationVariable)"></span>
                                                </template>
                                                <template x-if="!actionTextPart.nodeParameter.valueIterationVariable">
                                                    <input class="protocol-action-item-quantity-no-iteration line-height-1-5em font-inherit border-none outline-lightgrey mr-5 appearance-textfield-important" x-model="actionTextPart.nodeParameter.valueQuantity"
                                                           type="number"
                                                           x-on:input="selectIteration = false; $el.style.width = Math.max(15,proNS.getTextWidth($el))+22 + 'px'"
                                                           x-init="$nextTick(()=>{$el.style.width = Math.max(15,proNS.getTextWidth($el))+22 + 'px'});"/>
                                                </template>
                                                <template x-if="selectIteration && proNS.getParentStepIterationVariables(node, 'physicalQuantity', actionTextPart).length>0">
                                                    <div class="protocol-action-item-quantity-select flex-column-baseline gap-3 position-absolute z-index-1000 bg-white l-0 t-100p mt-3 overflow-y-auto max-h-120 w-130 outline-lightgrey">
                                                        <div class="protocol-action-item-quantity-select-no-iteration bg-lightgrey-hover pointer-hover pl-3 w-100p-3"
                                                             x-show="actionTextPart.nodeParameter.valueIterationVariable"
                                                             x-on:click.stop="selectIteration = false; actionTextPart.nodeParameter.valueIterationVariable = null; 
                                                             $dispatch('prons-focus-action-input',{nodeId:node.id,parameterId:actionTextPart.nodeParameter.parameterId});"
                                                             x-text="'{{__('Grandeur')}}'">
                                                        </div>
                                                        <div class="protocol-action-item-quantity-iteration-title fw-bold pl-3" x-text="'{{__('Variables itératives :')}}'"></div>
                                                        <template x-for="variable in proNS.getParentStepIterationVariables(node, 'physicalQuantity', actionTextPart)">
                                                            <div class="protocol-action-item-quantity-select-iteration-item bg-lightgrey-hover pointer-hover pl-20 w-100p-20"
                                                                 x-on:click.stop="selectIteration = false; actionTextPart.nodeParameter.valueIterationVariable = variable;"
                                                                 x-text="variable.name">
                                                            </div>
                                                        </template>
                                                    </div>
                                                </template>
                                            </div>
                                        </template>
                                        <template x-if="node.inEdition && ( node.isEditable || proNS.isTeacher)">
                                            <div class="protocol-action-item-quantity-unit-no-iteration display-inline">
                                                <template x-if="!actionTextPart.nodeParameter.valueIterationVariable">
                                                    <select class="protocol-action-item-quantity-unit-iteration border-none outline-lightgrey bg-whitesmoke ph-2 pointer-hover" x-model="actionTextPart.nodeParameter.valueUnit">
                                                        <option value="" label=""></option>
                                                        <template x-for="option in proNS.getSelectOptions(actionTextPart)">
                                                            <option :value="option.value" :label="option.label"
                                                                    :selected="parseInt(option.value) === parseInt(actionTextPart.nodeParameter.valueUnit)"></option>
                                                        </template>
                                                    </select>
                                                </template>
                                            </div>
                                        </template>
                                    </div>
                                </template>
                            </div>
                        </template>
                    </template>
                </div>
                
                <!-- comment -->
                <template x-if="node.comment || (sectionEdition && node.isEditable) || (proNS.ldEdition && proNS.isTeacher && sectionEdition)">
                    <div class="protocol-action-item-comment-wrapper display-inline-block m-0 p-0">
                        <template x-if="!node.inEdition || (!node.isEditable && !proNS.isTeacher)">
                            <span class="protocol-action-item-comment-read-only pre-wrap col-comment-font fs-italic" x-text="node.comment"></span>
                        </template>

                        <template x-if="node.inEdition && ( node.isEditable || proNS.isTeacher)">
                            <div class="protocol-action-item-comment-edit flex-grow-1 display-grid">
                                <textarea class="protocol-action-item-comment-textarea grid-area-1-1-2-2 p-4 border-none outline-lightgrey overflow-hidden resize-none col-comment-font" rows="1"
                                          x-model="node.comment"
                                          :placeholder="'{{__('Commentaire')}}'"></textarea>
                                <div x-text="(node.comment || '{{__('Commentaire')}}')+' '"
                                     class="protocol-action-item-comment-div grid-area-1-1-2-2 font-inherit p-4 border-none outline-lightgrey visibility-hidden pre-wrap"></div>
                            </div>
                        </template>
                    </div>
                </template>
            </div>
            <template x-if="proNS.ldEdition && !proNS.isTeacher && proNS.simulationCode && !node.inEdition &&  proNS.simulation.canActionBeSimulated(node)">
                {{-- remove x-show to display this part --}}
                <button x-show="false" class="border-none p-5 border-radius-2 bg-transparent col-primary fs-14 pointer-hover ml-auto" type="button"
                        :title="__('Simuler jusqu’à cette action')"
                        x-on:click.stop="proNS.simulation.checkProtocol(true, node.id)">
                    <i class="fas fa-play"></i>
                </button>
            </template>
        </div>
        <div class="protocol-action-item-image ml-10"
             :class="{ 
                'ml-0': node.urlImg !== undefined, 
                'editable':(node.isEditable && proNS.ldEdition && sectionEdition && node.inEdition)
             }"
             x-show="node.urlImg || (node.isEditable && proNS.ldEdition && sectionEdition && node.inEdition)">
            <template x-if="node.urlImg">
                <div class="protocol-action-item-image-wrapper mt-5 pr-10" :class="{'flex-column gap-3 child-visible-on-hover':node.inEdition && proNS.isTeacher}">
                    <div class="protocol-action-item-image-content bg-no-repeat bg-size-contain min-h-100 max-h-250 flex-column justify-space-between w-100p"
                         :class="{'bg-pos-x-left':node.imgAlignment && node.imgAlignment==='left',
                                'bg-pos-x-center':node.imgAlignment && node.imgAlignment==='center',
                                'bg-pos-x-right':node.imgAlignment && node.imgAlignment==='right'}"
                         x-on:prons-save-img-height.window="if(node.inEdition && $el.style.height){ node.imgHeight=$el.style.height;}"
                         x-on:prons-restore-action-img-height.window="$nextTick(()=>{$el.style.height = node.imgHeight;})"
                         x-init="$el.style.height = node.imgHeight; new ResizeObserver(proNS.resizeDiv).observe($el)"
                         :style="'background-image:url('+node.urlImg+');'">
                        <button :title="'{{__('Supprimer cette image')}}'"
                                class="protocol-action-item-image-delete align-self-end z-index-1000 col-white fs-14 m-5 p-5 pointer-hover bg-black-60 border-none border-radius-2 visibility-hidden"
                                type="button"
                                x-show="proNS.isTeacher || node.isEditable"
                                x-on:click="proNS.removeImage(node)">
                            <i class="fas fa-trash-can"></i>
                        </button>
                        <div class="align-self-center z-index-1000 ">
                            <button :title="'{{__('Aligner à gauche')}}'"
                                    class="protocol-action-item-image-align-left col-white fs-14 m-5 p-5 pointer-hover bg-black-60 border-none border-radius-2 visibility-hidden"
                                    type="button"
                                    x-show="proNS.isTeacher || node.isEditable"
                                    x-on:click="node.imgAlignment = 'left'">
                                <i class="fa-solid fa-angle-left"></i>
                            </button>
                            <button :title="'{{__('Aligner au centre')}}'"
                                    class="protocol-action-item-image-align-center col-white fs-14 m-5 p-5 pointer-hover bg-black-60 border-none border-radius-2 visibility-hidden"
                                    type="button"
                                    x-show="proNS.isTeacher || node.isEditable"
                                    x-on:click="node.imgAlignment = 'center'">
                                <i class="fa-solid fa-angle-up"></i>
                            </button>
                            <button :title="'{{__('Aligner à droite')}}'"
                                    class="protocol-action-item-image-align-right col-white fs-14 m-5 p-5 pointer-hover bg-black-60 border-none border-radius-2 visibility-hidden"
                                    type="button"
                                    x-show="proNS.isTeacher || node.isEditable"
                                    x-on:click="node.imgAlignment = 'right'">
                                <i class="fa-solid fa-angle-right"></i>
                            </button>
                        </div>
                    </div>
                    <template x-if="node.inEdition">
                        <button :title="'{{__('Redimensionner')}}'"
                                x-init="proNS.initImageResizeEvent($el, $el.previousElementSibling.previousElementSibling)"
                                class="protocol-action-item-image-resize border-none outline-white border-radius-25p w-30 h-5 bg-black-60 row-resize-hover align-self-center visibility-hidden"></button>
                    </template>
                </div>
            </template>
            <template x-if="false && node.isEditable && proNS.ldEdition && sectionEdition && node.inEdition && !node.urlImg && proNS.isTeacher">
                <div class="protocol-action-item-image-url-add-wrapper mt-5" x-data="{openField:false,newUrl:''}">
                    <i x-show="!openField" x-on:click="openField = true" class="protocol-action-item-image-url-add fa-regular fa-image pointer-hover" :title="'{{__('Ajouter une image')}}'"></i>
                    <input x-init="$el.focus()" x-on:click.outside="openField=false" x-show="openField" x-on:keyup.enter="node.urlImg = newUrl" 
                           :placeholder="'{{__('Url')}}'+'...'"
                           class="protocol-action-item-image-url-input font-inherit p-3 border-none outline-lightgrey" x-model="newUrl"/>
                </div>
            </template>
        </div>
    </div>
</template>
