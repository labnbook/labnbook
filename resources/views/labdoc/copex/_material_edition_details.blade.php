<template x-if="isMaterialClicked">
    <div class="material-details-wrapper border-lightgrey mv-10 mh-10 p-10"
         x-data="{ initialName: '' }" x-init="initialName = materialObject.name"
         x-on:click.outside.stop="if (materialObject.name === '') { materialObject.name = initialName; } isMaterialClicked = false">
        <div class="material-details-content flex-row-start justify-space-between gap-5">
            <div :class="{ 'w-50p': materialObject.urlImg, 'w-100p': !materialObject.urlImg }">
                <template x-if="proNS.isTeacher || proNS.LD.materialList.isEditable">
                    <div class="material-details-static-properties w-100p flex-column-baseline gap-5">
                        {{-- Material name edition --}}
                        <div class="material-details-name-wrapper flex-row-start gap-5 w-100p" :class="{ 'col-danger': materialObject.name === '' || proNS.nameAlreadyExists(materialObject, proNS.LD.materialList.listItems) }">
                            <div class="flex-grow-1 display-grid max-w-50p">
                                <textarea class="material-details-name-textarea grid-area-1-1-2-2 font-inherit p-5 border-none outline-lightgrey overflow-hidden resize-none" rows="1"
                                      :class="{ 'input-error': materialObject.name === '' || proNS.nameAlreadyExists(materialObject, proNS.LD.materialList.listItems) }"
                                      x-model="materialObject.name"
                                      x-init="$nextTick(()=>{$el.focus()})"
                                      x-on:prons-clean-material-details.window="$nextTick(()=>{$el.focus()})"
                                      :placeholder="'{{__('Nom du matériel')}}'"></textarea>
                                <div x-text="(materialObject.name || '{{__('Nom du matériel')}}')+' '"
                                     class="material-details-name-div grid-area-1-1-2-2 font-inherit p-5 border-none outline-lightgrey visibility-hidden pre-wrap"></div>
                            </div>
                            <template x-if="materialObject.name === '' || proNS.nameAlreadyExists(materialObject, proNS.LD.materialList.listItems)">
                                <i class="fas fa-circle-exclamation align-self-center" :title="'{{__('Il faut renseigner un nom de matériel unique.')}}'"></i>
                            </template>
                            {{-- Material quantity edition --}}
                            <div class="material-details-quantity-wrapper flex-row-center gap-5 ml-5">
                                <span x-text="'{{__('Quantité :')}}'" class="material-details-quantity-title fw-bold flex-shrink-0 col-grey"></span>
                                <input type="text" class="material-details-quantity font-inherit p-5 border-none outline-lightgrey" x-model="materialObject.quantity"
                                       x-init="$nextTick(()=>{$el.style.width = Math.max(45,proNS.getTextWidth($el))+10 + 'px';})"
                                       x-on:input="$el.style.width = Math.max(45,proNS.getTextWidth($el))+10 + 'px'"/>
                            </div>
                            <div class="ml-5">
                                <button type="button"
                                        class="material-details-item-delete bg-transparent border-none col-primary pointer-hover ml-10 mt-2"
                                        x-on:click="isMaterialClicked = false">
                                    <i class="fas fa-check" ></i>
                                </button>
                                <button :title="'{{__('Supprimer ce matériel')}}'" type="button"
                                        class="material-details-item-delete bg-transparent border-none ml-10 mt-2"
                                        :class="{'col-primary pointer-hover':!(!materialObject.isDeletable || !proNS.LD.materialList.isEditable && !proNS.isTeacher),
                                        'col-grey':!materialObject.isDeletable || !proNS.LD.materialList.isEditable && !proNS.isTeacher}"
                                        :disabled="!materialObject.isDeletable || !proNS.LD.materialList.isEditable && !proNS.isTeacher"
                                        x-show="(proNS.isTeacher && (proNS.isSuperManager || materialObject.properties.length===0)) 
                                            || proNS.LD.materialList.isEditable"
                                        x-on:click="isMaterialClicked = !proNS.removeMaterial(materialObject)">
                                    <i class="fas fa-trash-can" ></i>
                                </button>
                                <template x-if="proNS.isSuperManager">
                                    <button class="col-primary pointer-hover bg-transparent border-none ml-10 mt-2"
                                            x-on:click="materialObject = proNS.duplicateMaterial(materialObject);$dispatch('prons-clean-material-details', {});"
                                            :title="__('Dupliquer le matériel')">
                                        <i class="fas fa-copy"></i>
                                    </button>
                                </template>
                            </div>
                        </div>
                        {{-- Material description edition --}}
                        <div class="material-details-description-wrapper flex-row-start gap-5 w-100p mv-3">
                            <span x-text="'{{__('Description :')}}'" class="material-details-description-title fw-bold flex-shrink-0 col-grey mt-4"></span>
                            <div class="material-details-description flex-grow-1 display-grid">
                                <textarea class="material-details-description-textarea grid-area-1-1-2-2 font-inherit p-5 border-none outline-lightgrey overflow-hidden resize-none" rows="1"
                                      x-model="materialObject.description"
                                      :placeholder="proNS.isTeacher ? '{{__('Cette description sera accessible au survol du nom du matériel ou bien au clic sur ce dernier')}}'
                                        : '{{__('Description du matériel')}}'"></textarea>
                                <div x-text="(materialObject.description || (proNS.isTeacher ? '{{__('Cette description sera accessible au survol du nom du matériel ou bien au clic sur ce dernier')}}'
                                : '{{__('Description du matériel')}}') )+' '"
                                     class="material-details-description-div grid-area-1-1-2-2 font-inherit p-5 border-none outline-lightgrey visibility-hidden pre-wrap"></div>
                            </div>
                        </div>
                    </div>
                </template>
                <template x-if="!(proNS.isTeacher || proNS.LD.materialList.isEditable)">
                    <div class="material-details-read-only-wrapper display-inline-block m-0 p-0">
                        {{-- Material name read only --}}
                        <div class="material-details-read-only-name flex-row-center gap-10">
                            <span class="material-details-read-only-name-title font-inherit fs-14 pv-5" x-html="materialObject.name"></span>
                            {{-- Material quantity read only --}}
                            <div class="material-details-read-only-quantity flex-row-center gap-5" x-show="materialObject.quantity">
                                <span x-text="'{{__('Quantité :')}}'" class="material-details-read-only-quantity-title fw-bold flex-shrink-0 col-grey"></span>
                                <span class="material-details-read-only-quantity-content pre-wrap font-inherit pv-5 min-w-0 min-h-1em flex-grow-1" x-html="materialObject.quantity"></span>
                            </div>
                        </div>
                        {{-- Material description read only --}}
                        <div class="material-details-read-only-description flex-row-start gap-5 mv-3" x-show="materialObject.description">
                            <span x-text="'{{__('Description :')}}'" class="material-details-read-only-description-title fw-bold flex-shrink-0 col-grey mt-4"></span>
                            <span class="material-details-read-only-description-content pre-wrap font-inherit pv-5 min-w-0 min-h-1em flex-grow-1" x-html="materialObject.description"></span>
                        </div>
                    </div>
                </template>
                {{-- Types edition --}}
                <template x-if="(proNS.isTeacher && proNS.LD.isAdvancedSettings) || (proNS.isTeacher && !proNS.isSuperManager && proNS.simulationCode)">
                    <div class="material-details-types-wrapper flex-row-center flex-wrap mv-3 gap-5" x-data="{ id: $id(`ld-${proNS.id_ld}-copex`) }">
                        <span x-text="'{{__('Types ')}}'" class="material-details-types-title fw-bold flex-row-center flex-shrink-0 col-grey"></span>
                        <span class="material-details-types-colon fw-bold flex-row-center flex-shrink-0 col-grey">:</span>
                        <template x-for="(type, idx) in materialObject.types">
                            <div class="material-details-type-item-wrapper flex-row-center gap-5">
                                <input class="material-details-type-item-input font-inherit ml-10 w-25 border-none outline-lightgrey p-3" type="text" x-model="type.name"
                                       x-init="$nextTick(() => { $el.style.width = (proNS.getTextWidth($el) + 10) + 'px'; })"
                                       x-on:resize-input.window="$nextTick(() => { $el.style.width = (proNS.getTextWidth($el) + 10) + 'px'; })"
                                       x-on:input="$el.style.width = (proNS.getTextWidth($el) + 10)  + 'px'"
                                       :placeholder="'{{__('Nom du type')}}'" />
                                <template x-if="materialObject.types.some(t => t.id !== type.id && t.name === type.name) || type.name === ''">
                                    <i class="material-details-type-item-alert fas fa-circle-exclamation col-danger"
                                       :title="type.name === ''?'{{__('Le type doit être renseigné')}}':'{{__('Ce type existe déjà')}}'"></i>
                                </template>
                                <button class="material-details-type-item-delete pointer-hover bg-transparent border-none col-primary"
                                        x-on:click="proNS.removeType(type.name, materialObject.types)"
                                        :title="'{{__('Supprimer ce type')}}'">
                                    <i class="fas fa-trash-can"></i>
                                </button>
                            </div>
                        </template>
                        <div class="material-details-type-item-select m-0 ml-10 pointer-hover flex-row-center" x-data="{ newType: null, inputWidth: 30 }"
                             x-on:click.outside="proNS.addType(newType, materialObject, 'types'); newType = null;"
                             x-on:keyup.enter="proNS.addType(newType, materialObject, 'types'); newType = null;">
                            <input :list="id" class="material-details-type-item-select-input min-w-40 w-40 border-none outline-lightgrey p-2"
                                   x-model="newType" x-on:change="proNS.addType(newType, materialObject, 'types'); newType = null;"
                                   x-on:input="if ($event.inputType === 'insertReplacementText') { proNS.addType(newType, materialObject, 'types'); newType = null; } else { $el.style.width = Math.max(35,(proNS.getTextWidth($el) + 5)) + 'px'; }" />
                            <datalist :id="id">
                                <template x-for="type in proNS.getAllMaterialTypes(materialObject.types)">
                                    <option :value="type.name" x-text="type.name"
                                            x-init="inputWidth = Math.min(300, Math.max(inputWidth, proNS.getTextWidth($el) + 10)); $root.getElementsByTagName('input')[0].style.width = inputWidth + 'px';"></option>
                                </template>
                            </datalist>
                        </div>
                    </div>
                </template>
            </div>
            <div class="material-details-image-wrapper ph-5" :class="{ 'w-45p': materialObject.urlImg, 'editable': proNS.isTeacher }"
                 x-show="materialObject.urlImg || proNS.isTeacher">
                <template x-if="materialObject.urlImg">
                    <div class="material-details-image-content-wrapper flex-column gap-3 child-visible-on-hover">
                        <div class="material-details-image-content max-h-300 min-h-100 bg-pos-x-right bg-no-repeat bg-size-contain text-align-right"
                             x-on:prons-clean-material-details.window="$nextTick(()=>{$el.style.height = materialObject.imgHeight})"
                             x-on:prons-save-img-height.window="if($el.style.height){ materialObject.imgHeight=$el.style.height;}"
                             x-init="$el.style.height = materialObject.imgHeight; new ResizeObserver(proNS.resizeDiv).observe($el)"
                             :style="'background-image:url('+materialObject.urlImg+');'">
                            <button :title="'{{__('Supprimer cette image')}}'"
                                    class="material-details-image-delete col-white z-index-1000 fs-14 m-5 p-5 pointer-hover bg-black-60 border-none border-radius-2 visibility-hidden" type="button"
                                    x-show="proNS.isTeacher || proNS.LD.materialList.isEditable"
                                    x-on:click="proNS.removeImage(materialObject)">
                                <i class="fas fa-trash-can"></i>
                            </button>
                        </div>
                        <button :title="'{{__('Redimensionner')}}'"
                                x-init="proNS.initImageResizeEvent($el, $el.previousElementSibling)"
                                x-on:prons-clean-material-details="proNS.initImageResizeEvent($el, $el.previousElementSibling)"
                                class="material-details-image-resize border-none outline-white border-radius-25p w-30 h-5 bg-black-60 row-resize-hover align-self-center visibility-hidden"></button>
                    </div>
                </template>
                <template x-if="false && !materialObject.urlImg && proNS.isTeacher">
                    <div x-data="{ openField: false, newUrl: '' }">
                        <button :title="'{{__('Ajouter une image')}}'" class="material-details-image-add col-primary pointer-hover bg-transparent border-none mr-10" type="button"
                                x-show="!openField" x-on:click="openField = true">
                            <i class="fa-regular fa-image"></i>
                        </button>
                        <input x-init="$el.focus()" x-show="openField" x-on:keyup.enter="materialObject.urlImg = newUrl;"
                               x-on:click.outside="openField=false"
                               :placeholder="'{{__('Url...')}}'"
                               class="material-details-image-url font-inherit p-3 border-none outline-lightgrey" x-model="newUrl" />
                    </div>
                </template>
            </div>
        </div>
        {{-- Properties edition --}}
        <template x-if="proNS.isTeacher && proNS.isSuperManager && proNS.simulationCode && proNS.LD.isAdvancedSettings">
            <div class="material-details-properties-wrapper mv-5" :class="{'flex-row-center gap-5':proNS.getMaterialProperties(materialObject).length === 0}">
                <div x-text="'{{__('Propriétés :')}}'" class="material-details-properties-title fw-bold col-grey pv-5"></div>
                <template x-if="proNS.getMaterialProperties(materialObject).length > 0">
                    <div class="material-details-properties-header flex-row-center gap-5 pv-3">
                        <span x-text="'{{__('Nom')}}'" class="material-details-properties-header-name col-grey w-175"></span>
                        <span x-text="'{{__('Grandeur')}}'" class="material-details-properties-header-quantity col-grey w-175"></span>
                        <span x-text="'{{__('Valeur')}}'" class="material-details-properties-header-value col-grey w-175"></span>
                        <span x-text="'{{__('Unité')}}'" class="material-details-properties-header-unit col-grey w-100"></span>
                    </div>
                </template>
                <template x-for="property in proNS.getMaterialProperties(materialObject)">
                    <div class="material-details-property flex-row-center gap-5 pv-3">
                        <input class="material-details-property-name font-inherit p-3 border-none outline-lightgrey w-175" x-model="property.name" />
                        <select class="material-details-property-quantity-select border-none outline-lightgrey bg-whitesmoke p-3 pointer-hover w-175" x-model="property.physicalQuantityId"
                                x-on:change="property.unitId = null">
                            <option value="" label=""></option>
                            <template x-for="quantity in proNS.quantitiesData">
                                <option :value="quantity.id" :label="quantity.name[i18next.language]" :selected="quantity.id === property.physicalQuantityId"></option>
                            </template>
                        </select>
                        <input type="text" class="material-details-property-value font-inherit p-3 border-none outline-lightgrey w-175"  x-model="property.value" />
                        <select class="material-details-property-unit-select border-none outline-lightgrey bg-whitesmoke p-3 pointer-hover w-100" x-model="property.unitId">
                            <option value="" label=""></option>
                            <template x-for="unit in proNS.getUnitList(property.physicalQuantityId)">
                                <option :value="unit.id" :label="unit.label" :selected="unit.id === property.unitId"></option>
                            </template>
                        </select>
                        <button x-on:click="proNS.removeListItem(materialObject.properties, property);"
                                class="material-details-property-delete col-primary pointer-hover bg-transparent border-none mr-10" type="button">
                            <i class="fas fa-times"></i>
                        </button>
                        <template x-if="proNS.isSuperManager">
                            <button x-on:click="property.hidden = !property.hidden"
                                    class="pointer-hover bg-transparent border-none"
                                    :title="property.hidden ? __('Afficher cette information aux étudiants') : __('Masquer cette information aux étudiants')">
                                <i class="fas fa-eye col-secondary" :class="{'col-silver': property.hidden, 'col-secondary': !property.hidden }"></i>
                            </button>
                        </template>
                    </div>
                </template>
                <button x-on:click="proNS.addPropertyToMaterial(materialObject)"
                        class="material-details-property-add col-primary pointer-hover bg-transparent border-none" type="button">
                    <i class="fas fa-plus"></i>
                </button>
            </div>
        </template>
        <template x-if="!proNS.isTeacher && proNS.getMaterialProperties(materialObject).filter(p => p.hidden !== true).length>0">
            {{-- Properties read only --}}
            <div class="material-details-properties-read-only mv-5">
                <div x-text="'{{__('Propriétés :')}}'" class="material-details-properties-read-only-title fw-bold col-grey pv-5"></div>
                <template x-for="property in proNS.getMaterialProperties(materialObject).filter(p => p.hidden !== true)">
                    <div class="material-details-properties-read-only-content flex-row-center pv-3">
                        <span class="material-details-properties-read-only-name font-inherit-except-color p-3 border-none col-grey" x-text="property.name + ' :'"></span>
                        <span class="material-details-properties-read-only-value font-inherit-except-color p-3 pl-1 border-none col-grey" x-text="property.value"></span>
                        <span class="material-details-properties-read-only-unit font-inherit-except-color p-3 pl-1 border-none col-grey" x-text="proNS.getPhysicalUnitLabel(property)"></span>
                        <span class="material-details-properties-read-only-quantity font-inherit-except-color p-3 pl-1 border-none col-grey" x-text="proNS.getPhysicalQuantityName(property.physicalQuantityId)"></span>
                    </div>
                </template>
            </div>
        </template>
    </div>
</template>
