<ul class="material-list pl-20 column-count-4" x-init="proNS.initializeSortingMaterialList($el)">
    <template x-for="material in proNS.getMaterialColumns()" :key="`material_${material.id}_${material.position}`">
        <li x-data="{ id: `ld_${proNS.id_ld}_mat_${material.id}` }"
            x-show="sectionEdition || (material.name && material.selected)"
            :title="material.description"
            class="material-list-item pr-5 pt-3 m-0 p-0 break-inside-avoid-column"
            :class="{ 'flex-row-start gap-5 list-style-type-none sortable-material-item': sectionEdition && (proNS.isTeacher || proNS.LD.materialList.isSelectable),
                    'fw-bold col-primary': sectionEdition && (materialObject && materialObject.id === material.id && isMaterialClicked),
                }">
            <template x-if="sectionEdition">
                <div class="material-list-item-editable-wrapper flex-row-start gap-5 w-100p">
                    <template x-if="sectionEdition && (proNS.isTeacher || proNS.LD.materialList.isSelectable)">
                        <div class="material-list-item-editable-selection display-inline" :title="material.selected?'{{__('Matériel utilisable dans le protocole')}}':'{{__('Matériel non utilisable dans le protocole')}}'">
                            <input type="checkbox" :id="id" x-model="material.selected" 
                                   x-init="$watch('material.selected', () => proNS.traceUserAction('student_'+(material.selected?'select':'unselect')+'_material', material))"
                                   class="material-list-item-editable-selection-input m-0 p-0 overflow-hidden display-none" />
                            <div class="material-list-item-editable-selection-hidden-checkbox-wrapper hidden-checkbox-wrapper mt-2 p-0 overflow-hidden unset-box-sizing"
                                 :class="{'pointer-hover':proNS.LD.materialList.isSelectable}"
                                 x-on:click.prevent.stop="$nextTick(() => { if (proNS.isTeacher || proNS.LD.materialList.isSelectable) { 
                             material.selected = !material.selected; isMaterialClicked = materialObject && materialObject.id === material.id; } });">
                                <div class="material-list-item-editable-selection-hidden-checkbox hidden-checkbox m-0 p-0 overflow-hidden"></div>
                            </div>
                        </div>
                    </template>
                    <label :for="id" class="material-list-item-editable-label-for-checkbox pointer-hover mt-2 flex-row gap-5"
                           x-on:click.prevent.stop="materialObject = material; isMaterialClicked = true; proNS.resizeInput(); $dispatch('prons-clean-material-details', {})">
                        <span x-text="material.name ? (material.name + (material.quantity ? (' (' + material.quantity + ')') : '')) : '{{__('Nom du matériel')}}'" class="material-list-item-label" 
                              :class="{ 'col-danger':!material.name || proNS.nameAlreadyExists(material, proNS.LD.materialList.listItems)}"></span>
                        <template x-if="material.description">
                            <i class="fa fa-info-circle col-grey"></i>
                        </template>
                    </label>
                </div>
            </template>
            <template x-if="!sectionEdition">
                <div x-on:click="if(proNS.ldEdition){proNS.closeAllEditContent(); sectionEdition = true; materialObject = material; isMaterialClicked = true; proNS.resizeInput(); $dispatch('prons-clean-material-details', {});}">
                    <span x-text="material.name + (material.quantity ? (' (' + material.quantity + ')') : '')"
                          class="material-list-item-read-only">
                    </span>
                    <template x-if="material.description">
                        <i class="fa fa-info-circle col-grey"></i>
                    </template>
                </div>
            </template>
        </li>
    </template>
</ul>
