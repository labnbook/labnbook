<div class="protocol-step-header-wrapper flex-row-center justify-space-between mb-5 ph-5">
    <template x-if="node.inEdition">
        @include('labdoc/copex/_protocol_node_buttons')
    </template>
    <template x-if="proNS.ldEdition && sectionEdition && proNS.isTeacher && node.inEdition">
        @include('labdoc/copex/_student_configuration_tooltip', [
            'classWrapper' => 'protocol-step-header-authorizations-wrapper',
            'items' => [ 
                    ['label' => __('Intitulé et commentaire modifiable'), 'xModel' => "node.isEditable"],
                    ['label' => __('Tableau des itérations modifiable'), 'xModel' => "node.isIterable"],
                    ['label' => __('Etape duplicable'), 'xModel' => "node.isDuplicable"],
                    ['label' => __('Etape déplaçable'), 'xModel' => "node.isMovable"],
                    ['label' => __('Etape supprimable'), 'xModel' => "node.isDeletable"],
                    ['label' => __('Ajout de sous-étapes possible'), 'xModel' => "node.isSubStep"],
                    ['label' => __('Ajout d’action possible'), 'xModel' => "node.isInsideAction"],
                ]
        ])
    </template>
</div>
<div class="protocol-step-details gap-5 pb-0" :class="{'node-handle-visible-on-hover':proNS.ldEdition, 'flex-row-baseline':!node.inEdition,'flex-row-start':node.inEdition}">
    <template x-if="!node.inEdition">
        <button :title="'{{__('Déplacer')}}'" class="protocol-step-details-move border-none ph-2 border-radius-2 bg-transparent col-primary" type="button"
                :class="{ 'visibility-hidden':!node.inEdition,'pointer-hover node-handle': proNS.isTeacher || node.isMovable, 'opacity-40':!(proNS.isTeacher || node.isMovable)}"
                :disabled="!(proNS.isTeacher || node.isMovable)">
            <i class="fa-solid fa-up-down-left-right move-hover"></i>
        </button>
    </template>
    <div class="protocol-step-details-content gap-5 m-0" :class="{'flex-row-baseline': !node.inEdition, 'flex-column flex-grow-1 ph-5':node.inEdition}">
        <template x-if="!node.inEdition && parseInt(node.iterationNumber)  > 1">
            <i class="fas fa-repeat" title="{{__("L'ensemble des actions de cette étape sont répétées selon les indications du tableau")}}"></i>
        </template>
        <!-- Step name -->
        <template x-if="!node.inEdition || (!proNS.isTeacher && !node.isEditable)">
            <span class="protocol-step-details-name-read-only pre-wrap fw-bold" :class="{'fs-italic col-grey':!node.name}" x-init="$el.style.fontSize = Math.max(12,14-stepDepth)+'px'"
              x-text="node.name || '{{__('Etape')}}'"></span>
        </template>

        <template x-if="node.inEdition && (node.isEditable || proNS.isTeacher)">
            <div class="protocol-step-details-name-edit flex-grow-1 display-grid">
                <textarea class="protocol-step-details-name-textarea grid-area-1-1-2-2 font-inherit p-4 border-none outline-lightgrey overflow-hidden resize-none fw-bold" rows="1"
                          x-model="node.name"
                          x-init="$el.style.fontSize = Math.max(12,14-stepDepth)+'px'; $nextTick(()=>{$el.focus()})"
                          :placeholder="'{{__('Nom de l’étape')}}'"></textarea>
                <div x-text="(node.name || '{{__('Etape')}}')+' '"
                     x-init="$el.style.fontSize = Math.max(12,14-stepDepth)+'px'"
                     class="protocol-step-details-name-div grid-area-1-1-2-2 font-inherit p-4 border-none outline-lightgrey visibility-hidden pre-wrap fw-bold"></div>
            </div>
        </template>
        <!-- Step comment -->
        <template x-if="(!node.inEdition && sectionEdition) || (!sectionEdition && node.comment) || (!proNS.isTeacher && !node.isEditable)">
            <span class="protocol-step-details-comment-read-only pre-wrap col-comment-font fs-italic" x-text="node.comment"></span>
        </template>

        <template x-if="node.inEdition && (node.isEditable || proNS.isTeacher)">
            <div class="protocol-step-details-comment-edit flex-grow-1 display-grid">
                <textarea class="protocol-step-details-comment-textarea grid-area-1-1-2-2 p-4 border-none outline-lightgrey overflow-hidden resize-none col-comment-font" rows="1"
                          x-model="node.comment"
                          :placeholder="'{{__('Commentaire')}}'"></textarea>
                <div x-text="(node.comment || '{{__('Commentaire')}}')+' '"
                     class="protocol-step-details-comment-div grid-area-1-1-2-2 font-inherit p-4 border-none outline-lightgrey visibility-hidden pre-wrap"></div>
            </div>
        </template>
    </div>
</div>
<template x-if="proNS.isTeacher && proNS.simulationCode && node.inEdition">
    <div class="protocol-step-details-simulation-variable flex-row-center gap-5 mv-5">
        <h4 x-text="'{{__('Variable de simulation')}}'"></h4>
        <select x-model="node.simulationVariable">
            <option value="" :label="'{{__('Choix d’une variable...')}}'" :selected="!node.simulationVariable"></option>
            <template x-for="name in proNS.getSimulationStepVariableNames()">
                <option :value="name" :label="name" :selected="name === node.simulationVariable" :disabled="proNS.isSimulationVariableUsedInProtocol(name, proNS.LD.protocol)"></option>
            </template>
        </select>
    </div>
</template>

<template x-if="parseInt(node.iterationNumber) > 0 || node.inEdition">
    <div class="protocol-step-header-iterations mt-5 ml-22">
        <template x-if="node.inEdition && (proNS.isTeacher || node.isIterable)">
            <span>
				<span class="protocol-step-header-iterations-info col-grey" x-text="'{{__('Réaliser les actions de cette étape ')}}'"></span>
				<input class="protocol-step-header-iterations-number-input outline-lightgrey border-none pv-2 pl-3 w-40" type="number" x-model="node.iterationNumber" min="1"
                       :class="{'input-error col-danger':isNaN(parseInt(node.iterationNumber))}" x-on:input="proNS.updateIterationTable(node)"/>
				<span class="protocol-step-header-iterations-times col-grey" x-text="'{{__(' fois.')}}'"></span>
				<span x-show="node.iterationNumber>1">
					<span class="protocol-step-header-iterations-modifying col-grey" x-text="'{{__('En modifiant ')}}'"></span>
					<input class="protocol-step-header-iterations-variables outline-lightgrey border-none pv-2 pl-3 w-40" type="number" x-model="node.variableNumber" min="0"
                           :class="{'input-error col-danger':isNaN(parseInt(node.variableNumber))}" x-on:input="proNS.updateIterationTable(node)"/>
					<span class="protocol-step-header-iterations-variables-parameters col-grey" x-text="'{{__(' paramètre(s).')}}'"></span>
				</span>
            </span>
        </template>
        <template x-if="!node.inEdition && (proNS.isTeacher || node.isIterable) && parseInt(node.iterationNumber) > 1">
            <span class="protocol-step-header-iterations-read-only col-grey" x-text="'{{__('Réaliser les actions de cette étape ')}}' + node.iterationNumber + '{{__(' fois.')}}'"></span>
        </template>
        <template x-if="node.iterationVariables.length >= 1">
            <table class="protocol-step-header-iterations-table border-black border-collapse m-0 mb-5 mt-2">
                <thead>
                <tr>
                    <th class="protocol-step-header-iterations-table-header ph-5 border-grey"></th>
                    <template x-for="index in Array.from(Array(Number(node.iterationNumber)).keys())">
                        <th class="protocol-step-header-iterations-table-header-cell ph-5 pv-2 border-grey col-grey text-align-center" x-text="index + 1"></th>
                    </template>
                </tr>
                </thead>
                <tbody>
                <template x-for="(variable, index) in node.iterationVariables">
                    <tr>
                        <td class="protocol-step-header-iterations-table-cell border-grey bg-white">
                            <!-- Step iteration variable name -->
                            <template x-if="!node.inEdition || (!proNS.isTeacher && !node.isIterable)">
                                <span class="protocol-step-header-iterations-table-variable-read-only display-inline-block ph-3 pv-2" 
                                      :class="{'col-grey fs-italic':!variable.name, 'col-primary':proNS.isVariableUsedInProtocol(node, variable)}" 
                                      x-text="(variable.name || '{{__('Paramètre ')}}')"></span>
                            </template>
                            <template x-if="node.inEdition && (proNS.isTeacher || node.isIterable)">
                                <div class="protocol-step-header-iterations-table-variable-editable display-inline-block m-0 p-0">
                                    <button x-on:click="variable.typeMaterial = !variable.typeMaterial; $dispatch('prons-update-action-text-part', {variableId:variable.id});"
                                            class="protocol-step-header-iterations-table-variable-material pointer-hover bg-transparent border-none pl-2"
                                            :title="'{{__('Définir cette variable comme ')}}' + (variable.typeMaterial ? '{{__('une grandeur')}}' : '{{__('un matériel')}}')">
                                        <i :class="{'fas fa-flask col-primary':variable.typeMaterial, 'fas fa-flask col-silver':!variable.typeMaterial}"></i>
                                    </button>
                                    <input type="text" class="protocol-step-header-iterations-table-variable-name font-inherit w-0 border-none outline-none p-3" x-model="variable.name"
                                           x-init="$nextTick(()=>{$el.style.width = (proNS.getTextWidth($el)?proNS.getTextWidth($el)+10:65)+'px';})"
                                           :placeholder="'{{__('Paramètre ')}}'"
                                            x-on:input="$el.style.width = (proNS.getTextWidth($el)?proNS.getTextWidth($el)+10:65)+'px'"/>
                                </div>
                            </template>
                            <div class="protocol-step-header-iterations-table-variable-unit display-inline-block m-0 p-0">
                                <!-- Step iteration variable unit -->
                                <template x-if="(!node.inEdition || (!proNS.isTeacher && !node.isIterable)) && !variable.typeMaterial && variable.unit">
                                    <span class="protocol-step-header-iterations-table-variable-unit-name display-inline-block p-3" 
                                          x-html="'['+proNS.getHTMLUnitName(variable.unit.name)+']'"></span>
                                </template>
                                <template x-if="node.inEdition && !variable.typeMaterial && (proNS.isTeacher || node.isIterable)">
                                    <div class="protocol-step-header-iterations-table-variable-unit-edit-wrapper flex-row-center ph-2" x-data="{unitsList:null, unitModel:''}">
                                        <span>[</span>
                                        <div class="protocol-step-header-iterations-table-variable-unit-edit flex-column-center gap-5 position-relative">
                                            <input type="text" class="protocol-step-header-iterations-table-variable-unit-input outline-none outline-none-focus border-none p-3" 
                                                   x-model="unitModel" :placeholder="'{{__('Unité')}}'"
                                                   x-init="unitModel=variable.unit?variable.unit.name:'';$nextTick(()=>{$el.style.width = unitModel?proNS.getTextWidth($el)+5+'px':'35px';});"
                                                   x-on:prons-resize-unit-input.window="$nextTick(()=>{$el.style.width = unitModel?proNS.getTextWidth($el)+5+'px':'35px';})"
                                                   x-on:input="$el.style.width = proNS.getTextWidth($el)+5+'px'; unitsList=unitModel?proNS.searchUnit(unitModel):null;const unit = proNS.findUnit(unitModel);variable.unit = unit?unit:null;"
                                                   x-on:click.outside="unitsList=null;"
                                                   x-on:click="unitsList=proNS.searchUnit(unitModel)">
                                            <template x-if="unitsList">
                                                <div class="protocol-step-header-iterations-table-variable-unit-select-wrapper flex-column-baseline gap-3 position-absolute z-index-1000 bg-white l-0 t-100p overflow-y-auto max-h-120 min-w-15 outline-lightgrey">
                                                    <template x-for="unit in unitsList">
                                                        <div class="protocol-step-header-iterations-table-variable-unit-item w-100p-4 p-2 outline-none-focus bg-lightgrey-focus pointer-hover bg-lightgrey-hover"
                                                             x-html="proNS.getHTMLUnitName(unit.name)"
                                                             x-on:click="variable.unit = unit; unitModel=unit.name;unitsList=null;$dispatch('prons-resize-unit-input');"></div>
                                                    </template>
                                                </div>
                                            </template>
                                        </div>
                                        <span>]</span>
                                    </div>
                                </template>
                            </div>
                            <template x-if="!(node.inEdition && (proNS.isTeacher || node.isIterable)) && variable.description">
                                <span class="protocol-step-header-iterations-table-variable-description display-inline-block p-2 fs-italic col-comment-font"
                                      x-text="variable.description"></span>
                            </template>
                            <template x-if="node.inEdition && (proNS.isTeacher || node.isIterable)">
                                <input type="text" class="protocol-step-header-iterations-table-variable-description-input outline-none outline-none-focus border-none p-3 fs-italic col-comment-font"
                                       x-model="variable.description" :placeholder="'{{__('description')}}'"
                                       x-init="$nextTick(()=>{$el.style.width = (proNS.getTextWidth($el)?proNS.getTextWidth($el)+10:70)+'px';})"
                                       x-on:input="$el.style.width = (proNS.getTextWidth($el)?proNS.getTextWidth($el)+10:70)+'px';">
                            </template>
                        </td>
                        <template x-for="(value, index) in variable.values" :key="'variable_index_' + index">
                            <td class="protocol-step-header-iterations-table-value-cell border-grey bg-white">
                                <template x-if="!node.inEdition || (!proNS.isTeacher && !node.isIterable)">
                                    <span class="protocol-step-header-iterations-table-value-read-only display-inline-block p-2" x-text="variable.values[index]"></span>
                                </template>
                                <template x-if="node.inEdition && (proNS.isTeacher || node.isIterable)">
                                    <input class="protocol-step-header-iterations-table-value-editable font-inherit w-0 border-none outline-none p-3" x-model="variable.values[index]"
                                           x-init="$nextTick(()=>{$el.style.width = Math.max(proNS.getTextWidth($el),15)+10 + 'px';})"
                                           x-on:input="$el.style.width = Math.max(proNS.getTextWidth($el),15)+10 + 'px'"/>
                                </template>
                            </td>
                        </template>
                    </tr>
                </template>
                </tbody>
            </table>
        </template>
    </div>
</template>
<template x-if="!node.inEdition && node.nodes.length === 0 && !proNS.dragNode">
    <span class="col-grey fs-italic ml-40" x-text="'({{__('Etape vide')}})'"></span>
</template>
