<div class="custom-lists-item-details flex-row-start gap-5 w-100p">
    <div class="custom-lists-item-details-name flex-row-start w-175 min-w-150 gap-5" 
         :class="{ 'col-danger': selectedCustomList.listName === '' || proNS.nameAlreadyExists(selectedCustomList, proNS.LD.customLists, 'listName') }">
        <div class="custom-lists-item-details-name-editable flex-grow-1 display-grid">
            <textarea class="custom-lists-item-details-name-editable-textarea grid-area-1-1-2-2 font-inherit p-5 border-none outline-lightgrey overflow-hidden resize-none" rows="1"
                      :class="{ 'input-error': selectedCustomList.listName === '' || proNS.nameAlreadyExists(selectedCustomList, proNS.LD.customLists, 'listName') }"
                      x-model="selectedCustomList.listName"
                      x-init="$nextTick(()=>{$el.focus()})"
                      x-on:prons-focus-custom-list-name.window="$nextTick(()=>{$el.focus()})"
                      :placeholder="'{{__('Nom la liste')}}'"></textarea>
            <div x-text="(selectedCustomList.listName || '{{__('Nom de la liste')}}')+' '"
                 class="custom-lists-item-details-name-editable-div grid-area-1-1-2-2 font-inherit p-5 border-none outline-lightgrey visibility-hidden pre-wrap"></div>
        </div>
        <button :title="'{{__('Supprimer la liste.')}}'"
                x-on:click="isListClicked=false; $nextTick(()=>{proNS.removeList(selectedActionTextPart.listId);selectedActionTextPart.listId=null;});"
                class="structured-action-text-part-custom-list-add-button col-primary pointer-hover p-3 bg-transparent border-radius-2 border-none">
            <i class="fas fa-trash-can"></i>
        </button>
        <template x-if="selectedCustomList.listName === '' || proNS.nameAlreadyExists(selectedCustomList, proNS.LD.customLists, 'listName')">
            <i class="custom-lists-item-details-name-alert fas fa-circle-exclamation col-danger align-self-center" :title="'{{__('Il faut renseigner un nom de liste unique.')}}'"></i>
        </template>
    </div>
    <ul class="custom-list-item-details-items list-style-type-none flex-row-center flex-wrap gap-5 m-0 p-0 pl-10" 
        :class="{'mt-5':selectedCustomList.listItems.length===0}">
        <li class="custom-list-item-details-items-title list-style-type-none fw-bold flex-row-center gap-5">
            <span x-text="'{{__('Items : ')}}'"></span>
        </li>
        <template x-for="(item, idx) in selectedCustomList.listItems">
            <li class="custom-list-item-details-item-wrapper list-style-type-none flex-row-center gap-5">
                <div class="custom-list-item-details-item flex-row-center gap-5 p-3"
                     :class="{'col-danger':selectedCustomList.listItems.some(i=>i.id!==item.id && i.name===item.name) || item.name===''}">
                    <input class="custom-list-item-details-item-input font-inherit p-3 w-25 border-none outline-lightgrey" type="text" x-model="item.name"
                           :class="{'outline-danger':selectedCustomList.listItems.some(i=>i.id!==item.id && i.name===item.name) || item.name===''}"
                           x-init="$nextTick(()=>{$el.style.width = Math.max(25,proNS.getTextWidth($el)+10) + 'px'; 
                       if(firstInit){$el.focus();firstInit=false;}})"
                           x-on:input="$el.style.width = Math.max(25,proNS.getTextWidth($el)+10) + 'px'"/>
                    <template x-if="selectedCustomList.listItems.some(i=>i.id!==item.id && i.name===item.name) || item.name===''">
                        <i class="custom-list-item-details-item-alert fas fa-circle-exclamation col-danger"
                           :title="item.name===''?'{{__('Le nom d’un élement ne peut pas être vide')}}':'{{__('Cet élément est déjà présent dans la liste.')}}'"></i>
                    </template>
                    <button class="custom-list-item-details-item-delete p-2 ml-auto col-primary pointer-hover bg-transparent border-none"
                            x-on:click="proNS.removeListItem(selectedCustomList.listItems, item)"
                            :title="'{{__('Supprimer cet élement')}}'">
                        <i class="fas fa-trash-can"></i>
                    </button>
                </div>
            </li>
        </template>
        <li>
            <div class="custom-list-item-details-item-add-wrapper flex-row-center justify-end gap-5">
                <button class="custom-list-item-details-item-add col-primary pointer-hover bg-transparent border-none"
                        :title="'{{__('Ajouter un élement à la liste')}}'"
                        x-on:click="firstInit=true; proNS.addItemToCustomList(selectedCustomList.listItems)">
                    <i class="fas fa-plus"></i>
                </button>
            </div>
        </li>
    </ul>
</div>

