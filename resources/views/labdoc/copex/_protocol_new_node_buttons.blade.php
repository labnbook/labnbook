<?php
/**
 * @var int $depth
 * @var boolean $add_inside
 */     
?>
<template x-if="sectionEdition && (proNS.checkNewNodeButtons(node?node:false))">
    <li x-data="{depth:{{$depth}}, addInside:{{$add_inside}}}"
        class="protocol-new-node-button-item list-style-type-none edit-node">
        <div class="protocol-new-node-button-wrapper flex-row-center gap-10 pv-3 ml-22">
            <div class="protocol-new-node-button-action-add-wrapper flex-row-center gap-5">
                <template x-if="proNS.LD.actionList.filter(a => a.isUsable || proNS.isTeacher).length===1 && proNS.isActionButton(node)">
                    <button class="protocol-new-node-button-action-add border-none border-radius-2 pointer-hover bg-grey ph-5 pv-2 fs-12"
                            x-data="{firstStructuredActionAvailable:proNS.LD.actionList.filter(a => a.isUsable || proNS.isTeacher)[0], actionName:null}"
                            x-init="actionName=firstStructuredActionAvailable.name!=='' ? firstStructuredActionAvailable.name : '{{__('Action libre')}}';"
                            x-on:click.stop="proNS.addNewNode({nodeType:'action',idStructuredAction:firstStructuredActionAvailable.id}, node, addInside)">
                        <span class="protocol-new-node-button-action-add-title col-white"
                              x-text="firstStructuredActionAvailable.isFreeAction ? '{{__('Ajouter une action')}}' : ('{{__('Ajouter')}} : ' + actionName)"></span>
                    </button>
                </template>
                <template x-if="proNS.LD.actionList.filter(a => a.isUsable || proNS.isTeacher).length>1 && proNS.isActionButton(node)">
                    <select x-on:click.stop="" 
                            x-init="await $nextTick(); $el.style.width = (proNS.getTextWidth($el, '{{__('Ajouter une action')}}') + 30) + 'px';"
                            x-on:change="proNS.addNewNode({nodeType:'action',idStructuredAction:$event.target.value}, node, addInside); $event.target.selectedIndex=0;"
                            class="protocol-new-node-button-structured-action-select border-none border-radius-2 pointer-hover bg-grey pv-2 ph-5 col-white fs-12">
                        <option value="" :label="'{{__('Ajouter une action')}}'" disabled selected></option>
                        <template x-for="structuredAction in proNS.LD.actionList.filter(elt => elt.isUsable || proNS.isTeacher).sort((a, b) => a.name < b.name ? -1 : 1)">
                            <option :value="structuredAction.id" :label="structuredAction.isFreeAction ? '{{__('Action libre')}}' : structuredAction.name"></option>
                        </template>
                    </select>
                </template>
            </div>
            <template x-if="depth<5 && proNS.isStepButton(node)">
                <button class="protocol-new-node-button-step-add border-none border-radius-2 pointer-hover bg-grey ph-5 pv-2 fs-12"
                        x-on:click.stop="proNS.addNewNode({nodeType:'step'}, node, addInside)">
                    <span class="protocol-new-node-button-step-add-title col-white" x-text="addInside?'{{__('Ajouter une sous-étape')}}':'{{__('Ajouter une étape')}}'"></span>
                </button>
            </template>
        </div>
    </li>
</template>
