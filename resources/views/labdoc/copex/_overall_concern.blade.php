<ul class="overall-concern-list p-0 list-style-type-none">
    <template x-for="(overallConcern, index) in proNS.LD.overallConcerns.sort((a, b) => a.position - b.position)"
              :key=`overall_concern_${overallConcern.id}_${overallConcern.position}`>
        <li x-data="{sectionEdition:false, oldContentValue:null}" x-show="(proNS.isTeacher && proNS.ldEdition) || overallConcern.isDisplayed" 
            x-init="if(!proNS.isTeacher){
                    $watch('sectionEdition',(sectionEdition)=>{
                        if(sectionEdition){
                            oldContentValue = overallConcern.content;
                        } else if(oldContentValue !== overallConcern.content) {
                            proNS.traceUserAction('student_modify_context', overallConcern);
                        }
                    })
                }"
            class='overall-concern-list-item p-5 mb-5' :class="{'pointer-hover outline-lightgrey-hover':(overallConcern.isEditable || proNS.isTeacher) && proNS.ldEdition && !sectionEdition, 'bg-whitesmoke': sectionEdition}"
            x-on:close-edition.window="sectionEdition=false"
            x-on:click="proNS.closeAllEditContent();if((overallConcern.isEditable || proNS.isTeacher) && proNS.ldEdition){sectionEdition=true}" x-on:click.outside="sectionEdition=false">
            <!-- Header -->
            <div class="overall-concern-list-item-header-wrapper flex-row-center justify-space-between">
                <!-- Title + button -->
                <div class="overall-concern-list-item-header fs-13 fw-bold" :class="{'col-grey': !proNS.isTeacher || !proNS.ldEdition || !sectionEdition }">
                    <!-- Only teachers in section edition are able to modify the title -->
                    <!-- teacher -->
                    <template x-if="proNS.isTeacher">
                        <div class="overall-concern-list-item-titles-teacher-wrapper display-inline m-0 p-0">
                            <template x-if="(!proNS.ldEdition || !sectionEdition) && overallConcern.isDisplayed">
                                <span class="overall-concern-list-item-title-read-only-teacher" 
                                      x-text="proNS.getOverallConcernLabelForTeacher(overallConcern)"></span>
                            </template>
                            <template x-if="(!proNS.ldEdition || !sectionEdition) && !overallConcern.isDisplayed">
                                <span class="overall-concern-list-item-title-read-only-hidden-teacher fw-normal fs-italic" 
                                      x-text="'{{__('Élément masqué aux étudiants')}}'"></span>
                            </template>
                            <template x-if="proNS.ldEdition && sectionEdition">
                                <div class="overall-concern-list-item-titles-teacher-edit-wrapper display-inline m-0 p-0 flex-row-center gap-5">
                                    <input class="overall-concern-list-item-title-1-edition-teacher font-inherit w-fit p-3 border-none outline-lightgrey"
                                           x-model="overallConcern.title_1" />
                                    <span class="col-grey" x-text="'{{__(' ou ')}}'"></span>
                                    <input class="overall-concern-list-item-title-2-edition-teacher font-inherit w-fit p-3 border-none outline-lightgrey"
                                           x-model="overallConcern.title_2" />
                                    <template x-if="overallConcern.title_1 && overallConcern.title_2">
                                        <span class="fs-italic fw-normal col-grey" x-text="'{{__(' (au choix des étudiants) ')}}'"></span>
                                    </template>
                                </div>
                            </template>
                        </div>
                    </template>
                    <!-- student -->
                    <template x-if="!proNS.isTeacher">
                        <div class="overall-concern-list-item-titles-student-wrapper display-inline m-0 p-0" x-show="overallConcern.isDisplayed">
                            <span class="overall-concern-list-item-title-read-only-student col-lightgrey'"
                                  x-text="overallConcern[overallConcern.selectedTitle] || overallConcern.title_1 || overallConcern.title_2"></span>
                        </div>
                    </template>

                    <!-- Switch button is only available for student in section edition -->
                    <template x-if="!proNS.isTeacher && proNS.ldEdition && sectionEdition && overallConcern.title_1 && overallConcern.title_2">
                        <button x-show="overallConcern.isDisplayed"
                                x-on:click="overallConcern.selectedTitle = proNS.getOverallConcernNextLabel(overallConcern)"
                                class="overall-concern-list-item-header-button col-primary pointer-hover bg-transparent border-none ml-10"
                                :title="'{{__('Changer pour : ')}}' + overallConcern[proNS.getOverallConcernNextLabel(overallConcern)]">
                            <i class="fas fa-arrow-right-arrow-left"></i>
                        </button>
                    </template>
                </div>
                
                <!-- Authorizations -->
                <template x-if="proNS.ldEdition && proNS.isTeacher && sectionEdition">
                    @include('labdoc/copex/_student_configuration_tooltip', [
                        'classWrapper' => 'overall-concern-list-item-authorizations-wrapper',
                        'items' => [ 
                            ['label' => __('Item affiché'), 'xModel' => "overallConcern.isDisplayed"],
                            ['label' => __('Item éditable'), 'xModel' => "overallConcern.isEditable"]
                         ]
                    ])
                </template>
            </div>

            <!-- Assignment + Content -->
            <div class="overall-concern-list-item-assignment-and-content-wrapper content mt-5">
                <!-- teacher -->
                <template x-if="proNS.isTeacher">
                    <div class="overall-concern-list-item-assignment-and-content-wrapper-teacher display-inline m-0 p-0">
                        <!-- Assignment -->
                        <template x-if="(!proNS.ldEdition || !sectionEdition) && overallConcern.assignment && !overallConcern.content && overallConcern.isDisplayed">
                            <div class="overall-concern-list-item-assignment-teacher mt-5 col-grey fs-italic pre-wrap" x-text="overallConcern.assignment"></div>
                        </template>
                        <template x-if="proNS.ldEdition && sectionEdition">
                            <div class="">
                                <span class="overall-concern-list-item-assignment-label col-grey no-wrap" x-text="'{{__('Consigne pour les étudiants :')}}'"></span>
                                <div class="overall-concern-list-item-editable-assignment-teacher w-100p display-grid">
                                    <textarea class="overall-concern-list-item-editable-assignment-teacher-textarea grid-area-1-1-2-2 font-inherit p-5 border-none outline-lightgrey overflow-hidden resize-none" rows="1" 
                                              x-model="overallConcern.assignment"
                                              x-init="$nextTick(()=>{$el.focus()})"></textarea>
                                    <div x-text="overallConcern.assignment+' '"
                                         class="overall-concern-list-item-editable-assignment-teacher-div grid-area-1-1-2-2 font-inherit p-5 border-none outline-lightgrey visibility-hidden pre-wrap"></div>
                                </div>
                            </div>
                        </template>
                        <!-- Content -->
                        <template x-if="(!proNS.ldEdition || !sectionEdition) && overallConcern.content && overallConcern.isDisplayed">
                            <div class="overall-concern-list-item-content-teacher mt-5 pre-wrap" x-text="overallConcern.content"></div>
                        </template>
                        <template x-if="proNS.ldEdition && sectionEdition">
                            <div class="mt-5">
                                <span class="overall-concern-list-item-content-label col-grey no-wrap" x-text="'{{__('Contenu par défaut :')}}'"></span>
                                <div class="overall-concern-list-item-editable-content-teacher w-100p display-grid">
                                    <textarea class="overall-concern-list-item-editable-content-teacher-textarea grid-area-1-1-2-2 font-inherit p-5 border-none outline-lightgrey overflow-hidden resize-none" rows="1"
                                          x-model="overallConcern.content"
                                          x-init="$nextTick(()=>{$el.focus()})"></textarea>
                                    <div x-text="overallConcern.content+' '"
                                         class="overall-concern-list-item-editable-content-teacher-div grid-area-1-1-2-2 font-inherit p-5 border-none outline-lightgrey visibility-hidden pre-wrap"></div>
                                </div>
                            </div>
                        </template>
                    </div>
                </template>
                <!-- student -->
                <template x-if="!proNS.isTeacher">
                    <div class="overall-concern-list-item-assignment-and-content-wrapper-student display-inline m-0 p-0" x-show="overallConcern.isDisplayed">
                        <template x-if="(overallConcern.assignment && !overallConcern.content) || (proNS.ldEdition && sectionEdition)">
                            <div class="overall-concern-list-item-assignment-teacher mt-5 col-grey fs-italic pre-wrap" x-text="overallConcern.assignment"></div>
                        </template>
                        <template x-if="!(proNS.ldEdition && sectionEdition) || !overallConcern.isEditable">
                            <div class="overall-concern-list-item-content-student-read-only mt-5 pre-wrap" x-text="overallConcern.content"></div>
                        </template>
                        <template x-if="proNS.ldEdition && sectionEdition && overallConcern.isEditable">
                            <div class="overall-concern-list-item-editable-content-student w-100p mt-5 display-grid">
                                <textarea class="overall-concern-list-item-editable-content-student-textarea grid-area-1-1-2-2 font-inherit p-5 border-none outline-lightgrey overflow-hidden resize-none" rows="1"
                                          x-model="overallConcern.content"
                                          :placeholder="(overallConcern[overallConcern.selectedTitle] || overallConcern.title_1 || overallConcern.title_2) + '...'"
                                          x-init="$nextTick(()=>{$el.focus()})"></textarea>
                                <div x-text="overallConcern.content+' '"
                                     class="overall-concern-list-item-editable-content-student-div grid-area-1-1-2-2 font-inherit p-5 border-none outline-lightgrey visibility-hidden pre-wrap"></div>
                            </div>
                        </template>
                    </div>
                </template>
            </div>
        </li>
    </template>
</ul>
