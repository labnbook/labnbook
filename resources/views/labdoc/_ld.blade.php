<?php
    /** @var \App\Helper $helper */
    /**
    * View a labdoc
    * @var \App\Views\Labdoc $ldv
    */
//display labdocs :
// either the scope is not pdf (default, test, or view)
// either the user selects ld to display and the chosen ld corresponds to the current ld
// either the user chose to not to display draft ld and the current ld is not draft
// either the user chose to display every ld
if ((! $ldv->is_pdf) || ($ldv->choice_ld == 'chose_labdoc' && $ldv->ld_chosen == $ldv->ld->id_labdoc) || ($ldv->choice_ld == 'no_draft' && !$ldv->ld->draft) || $ldv->choice_ld == 'all_ld') {
?>
@include('labdoc._head', ['ldv' => $ldv])
<?php
    if (!($ldv->ld->draft && $ldv->is_follow && $ldv->is_teacher )) { //the teacher doesn't see the content of draft ld
?>
    @include('labdoc._contents', ['ldv' => $ldv])
<?php
    }
}
