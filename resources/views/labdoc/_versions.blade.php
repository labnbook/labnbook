<?php
    /** @var \App\Helper $helper */
    /** @var \App\Views\Labdoc $ldv */
    /** @var \App\User $user */
    /** @var StdClass[] $versions */
?>

@include('labdoc._head', ['ldv' => $ldv])

<div class="ld-versions-title-bar">
    <div>{{__("Versions disponibles")}} {{__("(cliquez pour afficher)")}}{!!__("&nbsp;:")!!}</div>
    <div class="ld_versions_button lb-a" onclick="closeLDVersioning({{$ldv->ld->id_labdoc}})">
        {{__("Fermer la restauration")}} <i class="fa fa-times"></i>
    </div>
</div>

@if (empty($versions))
    <p>{{__("Pas d'historique de versions pour ce labdoc")}}</p>
@else
    <ul id="ld_versions">
        @foreach ($versions->contents as $vers)
        <li id="ld_version_{{$vers->id_trace}}" 
            @class(['ld_version_current' => $vers->current]) 
            @selected($vers->current) 
            onclick='displayLDVersion({{ $ldv->ld->id_labdoc }}, {{ $vers->id_trace }})'
        >
            {{ $vers->date_format }}
            @isset($vers->id_trace)
                {{ " " . __("par ") . $vers->user }}
            @endisset
            @if ($vers->current)
                {{ " - " . __("version du rapport") }}
            @endif
            @empty($vers->id_trace)
                {{ " - " . __("version initiale du labdoc") }}
            @endempty
            @if ($user->isAdmin())
                {{ " " . "(V." . $vers->id_trace . ")" }}
            @endif
            <div style="display: none" id='ld_version_content_{{$vers->id_trace}}'>{{$vers->data}}</div>
            <div style="display: none" id="ld_version_name_{{$vers->id_trace}}">
                @isset($vers->name)
                    {{ $vers->name }}
                @endisset
            </div>
        </li>
        @endforeach
    </ul>
@endif

<div class="ld-versions-title-bar">
    <div>{{__("Titre")}}{!!__("&nbsp;:")!!} <span id='ld_versions_name'></span></div>
    <div id="ld_versions_restore" class="ld_versions_button lb-a" onclick="validateLD('{{$ldv->ld->id_labdoc}}')" data-id_version="{{ $vers->id_trace }}">
        {{__("Restaurer cette version")}}<i class="fas fa-sync"></i>
    </div>
</div>

<div id="labdoc_content_{{ $ldv->ld->id_labdoc }}" class="ld_versions_content ld_{{ $ldv->ld->type_labdoc }}"></div>

@if ($user->isAdmin())
    <pre id="ld_versions_code"></pre>
@endif
