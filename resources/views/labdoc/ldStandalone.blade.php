<?php
    /** @var \App\Helper $helper */
    /** @var \App\Labdoc $ld */
?>
@extends('layouts.labnbook')
@section('title', $ld->name)
@section('head')
    @include('labdoc/_standalonehead')
@endsection


@section('body')

    <!-- css -->
    {!! $helper->loadAsset("/libraries/font_awesome/fontawesome-free-6.1.1-web/css/svg-with-js.css") !!}
    {!! $helper->loadAsset("/libraries/font_awesome/fontawesome-free-6.1.1-web/css/all.css") !!}
    {!!  $helper->loadAsset("/css/report.css")  !!}
    {!!  $helper->loadAsset("/tool_copex/v1_xml/copex.css")  !!}
    {!!  $helper->loadAsset("/tool_copex/v2_json/copex.css")  !!}
    {!!  $helper->loadAsset("/tool_copex/v2_json/dosage-spectro/dosage-spectro.css")  !!}
    {!!  $helper->loadAsset("/tool_fitex/fitex.css")  !!}


    <!-- Plugin EpsilonWriter -->
    {!! $helper->loadAsset("/libraries/pica/pica.min.js") !!}
    {!! $helper->loadAsset("/js/upload_image.js") !!}

 
<p><br/><b>&nbsp; {{__("Labdoc attaché")}}{!!__("&nbsp;:")!!} {{$ld->name}}</b><br/>&nbsp;</p>
<div id="labdoc_content_{{ $ld->id_labdoc }}" class="labdoc_content ld_{{ $ld->type_labdoc }} {{ $ld->type_labdoc === "text" ? " tinyMCE-text" : "" }}" data-id_ld="{{ $ld->id_labdoc }}"></div>

<script>
$(document).ready(function(){
    displayLDContent({{$ld->id_labdoc}}, 0, 1);
});
</script>

@endsection
