<?php
    /** @var \App\Helper $helper */
    /** @var \App\Labdoc $ld */
?>
{!! $helper->loadAsset("/libraries/jquery/jquery-2.2.4.min.js") !!}
{!! $helper->loadAsset("/libraries/jquery/jquery-ui.min.js") !!}
{!! $helper->loadAsset("/libraries/jquery/jquery-ui.min.css") !!}
{!!  $helper->configureAjax()  !!}

{!! $helper->loadAsset("/css/report.css") !!}
{!! $helper->loadAsset("/js/labdoc.js") !!}

<?php if ($ld->type_labdoc == "text") : ?>
    {!! $helper->loadAsset("/libraries/tinymce/tinymce.min.js") !!}
    <!-- Plugin EpsilonWriter enabled -->
    {!!  $helper->loadAsset("/libraries/katex/katex.min.js") !!}
    {!!  $helper->loadAsset("/libraries/katex/contrib/mhchem.min.js") !!}
    {!!  $helper->loadAsset("/libraries/katex/katex.min.css") !!}

    <?php elseif ($ld->type_labdoc == "procedure") : ?>
    {!! $helper->loadAsset("/tool_copex/v1_xml/copex.css") !!}
    {!! $helper->loadAsset("/tool_copex/v1_xml/jstree/jstree_pre1.0_fix_1/jquery.jstree.js") !!}
    {!! $helper->loadAsset("/tool_copex/v1_xml/copex_model.js") !!}
    {!! $helper->loadAsset("/tool_copex/v1_xml/copex_view.js") !!}
    {!! $helper->loadAsset("/tool_copex/v1_xml/copex_control.js") !!}
    {!! $helper->loadAsset("/tool_copex/v2_json/copex.css") !!}
    {!! $helper->loadAsset("/tool_copex/v2_json/copex.js") !!}
    {!! $helper->loadAsset("/tool_copex/v2_json/simulation.js") !!}
    {!! $helper->loadAsset("/tool_copex/v2_json/titrab/titrab.js") !!}
    {!! $helper->loadAsset("/tool_copex/v2_json/dosage-spectro/dosage-spectro.js") !!}
    {!! $helper->loadAsset("/tool_copex/v2_json/dosage-spectro/dosage-spectro.css") !!}
    <?php elseif ($ld->type_labdoc == "dataset") : ?>
    {!! $helper->loadAsset("/tool_fitex/fitex.css") !!}
    {!!  $helper->loadAsset("/libraries/mathjs/math.min.js")  !!}
    {!!  $helper->loadAsset("/libraries/ml-levenberg-marquardt/ml-levenberg-marquardt.js")  !!}
    {!! $helper->loadAsset("/tool_fitex/libraries/jquery.flot.all.min.js") !!}
    {!! $helper->loadAsset("/tool_fitex/libraries/jquery.contextMenu.min.js") !!}
    {!! $helper->loadAsset("/tool_fitex/libraries/jquery.contextMenu.css") !!}
    {!! $helper->loadAsset("/tool_fitex/fitex_view.js") !!}
    {!! $helper->loadAsset("/tool_fitex/fitex_control.js") !!}
    {!! $helper->loadAsset("/tool_fitex/fitex_model.js") !!}
    <?php elseif ($ld->type_labdoc == "drawing") : ?>
    {!! $helper->loadAsset("/tool_zwibbler/zwibbler.css") !!}
    {!! $helper->loadAsset("/tool_zwibbler/zwibbler2.js") !!}
    {!! $helper->loadAsset("/tool_zwibbler/zwibbler_fn_for_lb.js") !!}

    @include('tool/zwibbler')
    <?php endif ?>

<script>
    window.global_tab_ld = <?= json_encode([
        $ld->id_labdoc => [
            "ld_history" => [$ld->labdoc_data],
            "ld_type" => $ld->type_labdoc,
            "current_i" => 0,
            "max_i" =>  0,
            "send_in_db" => false,
        ]
    ]) ?>;
</script>
