<?php
    /** @var \App\Helper $helper */
    /** @var \App\Views\Labdoc $ldv */

	// contenu du LD
    $htmlClass = $ldv->edition ? "edited_labdoc_content edited_ld_" : "labdoc_content ld_";
	$display = $ldv->extend ? "block" : "none";
	$attributes = [
        'id_labdoc' => (int) $ldv->ld->id_labdoc,
		'edition' => (int) $ldv->edition,
		"extend" => (int)$ldv->extend,
		"scope" => $ldv->scope
	];
?>
<div id="labdoc_assignment_container_{{$ldv->ld->id_labdoc}}"
     class="lb_assignment labdoc_assignment"
     >
     <i class="fa fa-times lb_assignment_close" onclick="$('#labdoc_assignment_container_{{$ldv->ld->id_labdoc}}').hide()" title="{{ __("Fermer les consignes") }}"></i>
    <span id="labdoc_assignment_{{$ldv->ld->id_labdoc}}" data-attributes="{{json_encode($attributes)}}"></span>
</div>
<div class="labdoc-content-loading {{$htmlClass}}{{$ldv->ld->type_labdoc}} {{ $ldv->ld->type_labdoc === "text" ? " tinyMCE-text" : "" }}" id="labdoc_content_{{$ldv->ld->id_labdoc}}" style="display: {{$display}}" data-attributes="{{json_encode($attributes)}}">
    {{__("Mise à jour en cours... Si ce message s'affiche trop longtemps, appuyez sur la touche F5 de votre clavier !")}}
</div>
