<?php
    /** @var \App\Helper $helper */
    /** View labdoc header
    * @var \App\Views\Labdoc $ldv
    */
    //définition des variables qui permettent de tester la vue de l'utilisateur

    // détermination si le ld est locked ou non (valable que si on est hors édition, sinon, cela a déjà été vérifié)
    $ld_locked = false;
    if (($ldv->is_default || $ldv->is_test) && !$ldv->edition && $ldv->ld->locked && time() - $ldv->ld->locked < config('labnbook.locker_validity') && $ldv->ld->last_editor != Auth::id()) {
        $ld_locked = true;
        /** @var \\App\User $user_lock */
        $user_lock = $ldv->ld->user_last_editor;
    }
?>
{{--display the table that contains the LD name and all the icons--}}
<div id="ld_name_actions_{{$ldv->ld->id_labdoc}}"
        @class(["ld_name_actions",'ld_locked' => $ld_locked, 'ld_new' => $ldv->getLdStatus()->mod_icon])
        ld_type="{!! htmlspecialchars($ldv->ld->type_labdoc) !!}"
        x-data x-init="$store.reportLabdocs.storeOrUpdateLabdoc({{$ldv->ld->id_labdoc}}, '{{addslashes($ldv->ld->name)}}', {{$ldv->ld->shared?1:0}},
            {{$ldv->ld->position?$ldv->ld->position:0}}, {{$ldv->ld->reportPart?$ldv->ld->reportPart->position:0}}, {{$ldv->ld->last_id_version?$ldv->ld->last_id_version:0}}, {{$ldv->ld->draft && !$ldv->is_pdf}})"
       :class="{'ld_locked':$store.reportLabdocs.getLdStatus('{{$ldv->ld->id_labdoc}}') === 'locked_ld', 'ld_new':$store.reportLabdocs.getLdStatus('{{$ldv->ld->id_labdoc}}') === 'modified_ld'}"
        x-on:click="hideStar({{$ldv->ld->id_labdoc}})">
    <div class="ld_name_actions_header">
        {{--    Button for showing / hiding the LD--}}
        @if (!$ldv->edition && !$ldv->is_pdf) 
            {{--  on a accès à extend (de la table ld_status), et on mémorise l'état du ld--}}
            <div class='lb_show_hide_ld'
            @if(!($ldv->is_follow && $ldv->ld->draft))
                id='lb_show_hide_ld_{{$ldv->ld->id_labdoc}}' onclick='showHideLD({{$ldv->ld->id_labdoc}})'>
                @if ($ldv->extend)
                    <i class='fa fa-chevron-down' title='{{__(' Replier le labdoc')}}'></i>
                @else
                    <i class='fa fa-chevron-right' title='{{__(' Déplier le labdoc')}}'></i>
                @endif
            @else
                >
            @endif
            </div>
        @endif
        {{--  icone de type du LD : l'icone ainsi que le nom du LD sont draggable--}}
        <div @class(["ld_icon_name", "ld-draggable lb_move_btn"=>(($ldv->ld->draggable || $ldv->is_solution) && ($ldv->is_default || $ldv->is_test)), 'edition'=>$ldv->edition])
             title="{{(($ldv->ld->draggable || $ldv->is_solution) && ($ldv->is_default || $ldv->is_test))?__(" Faites glisser ce labdoc pour modifier sa position dans le rapport"):''}}">
            @php $ld_name_class = "print"; @endphp       
            @if(!$ldv->is_pdf)
                <div class="icon-with-badge" id="mod_ld_{{$ldv->ld->id_labdoc}}">
                    @if ($ldv->ld->type_labdoc == 'text')
                        <img src='/images/report/ld_type_text.svg' width='16' alt='LD texte' title="{{__('Labdoc texte')}}"/>
                        @php $ld_name_class = "text"; @endphp
                    @elseif($ldv->ld->type_labdoc == 'drawing')
                        <img src='/images/report/ld_type_drawing.svg' width='16' alt='LD dessin' title='{{__(' Labdoc dessin')}}' />
                        @php $ld_name_class = "drawing"; @endphp
                    @elseif ($ldv->ld->type_labdoc == 'dataset')
                        <img src='/images/report/ld_type_dataset.svg' width='16' alt='LD données' title='{{__(' Labdoc jeu de données')}}'/>
                        @php $ld_name_class = "dataset"; @endphp
                    @elseif ($ldv->ld->type_labdoc == 'procedure')
                        <img src='/images/report/ld_type_procedure.svg' width='16' alt='LD protocole' title='{{__(' Labdoc protocole')}}' />
                        @php $ld_name_class = "procedure"; @endphp
                    @elseif($ldv->ld->type_labdoc == 'code')
                        <img src='/images/report/ld_type_code.svg' width='16' alt='LD code' title='{{__(' Labdoc code')}}' />
                        @php $ld_name_class = "code"; @endphp
                    @endif
                    {{-- Badge Locked--}}
                    <div class="badge locked_ld" x-show="$store.reportLabdocs.getLdStatus('{{$ldv->ld->id_labdoc}}') === 'locked_ld'"
                         :title="$store.reportLabdocs.getLockedTitle('{{$ldv->ld->id_labdoc}}')">
                        <i class="fa fa-lock"></i>
                    </div>
                    {{-- Badge Modified--}}
                    <div class="badge modified_ld" x-show="$store.reportLabdocs.getLdStatus('{{$ldv->ld->id_labdoc}}') === 'modified_ld'"
                         :title="$store.reportLabdocs.getModifiedTitle('{{$ldv->ld->id_labdoc}}')">
                        1
                    </div>
               </div>
            @endif
        
            @if($ldv->edition)
                <input type="text" value="{!! htmlspecialchars($ldv->ld->name) !!}" placeholder="{{__("Titre du labdoc")}}" class="editable_ld_name"
                       id="ld_name_{{$ldv->ld->id_labdoc}}" {{(!$ldv->ld->editable_name && !$ldv->is_solution)?'disabled':''}}/>
            @else
                <span x-data class='labdoc_name labdoc_name_{!! htmlspecialchars($ld_name_class) !!}' 
                      :class="{'empty-ld-name':!$store.reportLabdocs.isLdName('{{$ldv->ld->id_labdoc}}')}"
                      id='ld_name_{{$ldv->ld->id_labdoc}}'>
                    {!! $ldv->ld->name!==''?htmlspecialchars($ldv->ld->name):__('Nom de labdoc à indiquer') !!}
                </span>
                @if(!$ldv->is_pdf)
                    <span x-show="$store.reportLabdocs.isDraft('{{$ldv->ld->id_labdoc}}')" class='labdoc_draft' title="{{__("Le contenu du labdoc est caché à l'enseignant")}}">
                        [ {{__("caché aux enseignants")}} ]
                    </span>
                @endif
            @endif
        </div>
        
        {{--boutons de undo redo--}}
        @if($ldv->edition)
            <div class='lnb_undoredo'>
                <i class='fa fa-undo disabled' id='lnb_undo_{{$ldv->ld->id_labdoc}}' 
                   title='{{__(' Annuler la dernière modification')}}'></i>&nbsp;
                <i class='fas fa-redo disabled' id='lnb_redo_{{$ldv->ld->id_labdoc}}' 
                   title='{{__('Rétablir la modification')}}'></i>
            </div>
        @endif
    
        {{-- bouton de commentaire--}}
        @if($ldv->is_default || $ldv->is_test || $ldv->is_view)
            <div id="comment_icon_{{$ldv->ld->id_labdoc}}" 
                @class(['comment_icon', 'in-edition' => $ldv->edition])>
                <div class="icon-with-badge">
                    <span class="fa-layers fa-1x" onclick="Alpine.store('commentWidgetIdLd',{{$ldv->ld->id_labdoc}});
                    window.dispatchEvent(new CustomEvent('report-toggle-widget-comment'));">
                        <i class="far fa-comment" title="{{__("Commenter le labdoc")}}"></i>
                        <i x-data x-show="$store.reportLabdocs.isCommentOnLd({{$ldv->ld->id_labdoc}})" class="fa-layers-counter fa-layers-bottom-right fa-2x">1</i>
                    </span> 
                </div>
            </div>
        @endif

        {{-- menu des LD : edition / duplication / suppression / brouillon / récupération--}}
        @if(!$ldv->versioning && !$ldv->edition && ($ldv->is_default || $ldv->is_test || $ldv->is_view) || ($ldv->is_follow && $ldv->ld->shared))
            @if( ($ldv->is_view|| !($ldv->ld->duplicatable || $ldv->ld->deleteable || $ldv->ld->editable)) && !$ldv->is_solution) 
                {{-- seulement le bouton toggle de draft--}}
                <div class='lb_ld_menu_btn' id='ld_status_{{$ldv->ld->id_labdoc}}' 
                    :title="$store.reportLabdocs.getDraftTitle('{{$ldv->ld->id_labdoc}}')"
                    onclick='toggleDraftStatus({{$ldv->ld->id_labdoc}})'>
                    <i class='far fa-eye' ></i>
                </div>
            @else
                {{-- il y a au moins deux boutons à afficher : on crée un menu--}}
                @if(!$ldv->is_view)
                    <div class='lb_ld_menu_btn' :class="{'touched': touched}" {{$ld_locked?'style="display:none"':''}} x-data="{leftAligned:true, touched:false }"
                        x-on:touchstart="touched=true;leftAligned=document.getElementById('report_column').offsetWidth-$el.getBoundingClientRect().right<50"
                        x-on:touchstart.outside="touched=false"
                        x-on:mouseover="leftAligned=document.getElementById('report_column').offsetWidth-$el.getBoundingClientRect().right<50">
                        <i class="lb-ld-menu-icon fa fa-ellipsis-v" title="{{__('Menu du labdoc')}}"></i>
                        <table class='lb_ld_menu' :class="{'left-aligned':leftAligned }">
                            @if($ldv->ld->editable || $ldv->is_solution)
                                <tr onclick='(async () => { await editLD({{$ldv->ld->id_labdoc}}); return false; })();' 
                                    title='{{__('Modifier le labdoc')}}'>
                                    <td>
                                        <i class='fas fa-pencil-alt'></i>
                                    </td> 
                                    <td>{{__('Modifier')}}</td>
                                </tr>
                            @endif
                            @if(!$ldv->is_follow && ($ldv->ld->duplicatable || $ldv->is_solution))
                                <tr onclick='duplicateLD({{$ldv->ld->id_labdoc}},{{$ldv->ld->id_report_part}},1,1); return false;' 
                                    title='{{__('Dupliquer le labdoc')}}'>
                                    <td>
                                        <i class='far fa-clone'></i>
                                    </td>
                                    <td>{{__('Dupliquer')}}</td>
                                </tr>
                            @endif
                            @if(!$ldv->is_solution && !$ldv->ld->shared)
                                <tr onclick='toggleDraftStatus({{$ldv->ld->id_labdoc}})' 
                                    id='ld_status_{{$ldv->ld->id_labdoc}}'
                                    :title="$store.reportLabdocs.getDraftTitle('{{$ldv->ld->id_labdoc}}')">
                                    <td>
                                        <i class='far fa-eye'></i>
                                    </td>
                                    <td x-text="$store.reportLabdocs.isDraft('{{$ldv->ld->id_labdoc}}')?'{{__('Montrer')}}':'{{__('Cacher')}}'"></td>
                                </tr>
                            @endif
                            <tr onclick='openLDVersioning({{$ldv->ld->id_labdoc}})' 
                                title="{{__("Accéder aux différentes versions du labdoc")}}">
                                <td>
                                    <i class='fa fa-history'></i>
                                </td>
                                <td>{{__('Restaurer')}}</td>
                            </tr>
                            @if($ldv->ld->deleteable || $ldv->is_solution)
                                <tr onclick='deleteLD({{$ldv->ld->id_labdoc}})' 
                                    title='{{__('Supprimer le labdoc')}}'>
                                    <td>
                                        <i class='far fa-trash-alt'></i>
                                    </td>
                                    <td>{{__('Supprimer')}}</td>
                                </tr>
                            @endif
                        </table>
                    </div>
                @endif
             @endif
        @else
            {{--Case of a teacher who wants to test a labdoc dataset or code--}}
            @if($ldv->is_teacher && $ldv->is_follow && ($ldv->ld->type_labdoc == "code" || $ldv->ld->type_labdoc == "dataset" 
                || ($ldv->ld->mission->simulation_code && $ldv->ld->type_labdoc == "procedure")))
                <div class="ld_test_without_saving" id="ld_test_without_saving_{{$ldv->ld->id_labdoc}}" 
                    data-in-edit="false"
                    title="{{__('Tester le labdoc (sans le sauvegarder)')}}"
                    onclick="teacherEditLD({{$ldv->ld->id_labdoc}}); return false;">
                    <i class="fas fa-wrench lb-ld-button"></i>
                </div>
                <div class="ld_exit_test_without_saving" id="ld_exit_test_without_saving_{{$ldv->ld->id_labdoc}}" 
                    title="{{__('Quitter le mode test')}}" 
                    onclick="teacherUnEditLD({{$ldv->ld->id_labdoc}}); return false;" style="display:none">
                    <i class="fa fa-times lb-ld-button"></i>
                </div>
            @endif
        @endif
        {{--boutons--}}
        @if($ldv->edition)
            <div title="{{__("Enregistrer une nouvelle version du labdoc")}}"
                onclick="validateLD({{$ldv->ld->id_labdoc}}, true)">
                <i class="fa fa-check lb-ld-button"></i> 
            </div>
        @elseif($ldv->is_shared)
            <div>
               <span class="edit_menu">{{__("[Partagé au sein de la classe]")}}</span>
            </div>
        @endif
        {{--date du calcul de différence--}}
        @if ($ldv->is_teacher && $ldv->is_follow && !$ldv->ld->shared && $ldv->ld->type_labdoc === 'text')
            <div id="lb-toggle-modifications-{{$ldv->ld->id_labdoc}}" x-data="labdocDiff({{$ldv->ld->id_labdoc}}, {{$ldv->last_id_version_reviewed}})"
                x-show="$store.reportLabdocs.getLdStatus('{{$ldv->ld->id_labdoc}}') !== 'locked_ld' && !$store.reportLabdocs.isDraft('{{$ldv->ld->id_labdoc}}')"
                class="ld-diff-menu" :class="{'display-diff': displayDiff}">
                <div x-show="displayDiff" class="ld-diff-date">
                    <span>{{__('Source : ')}}</span>
                    <template x-if="labdocData && labdocData.versioning.length>0">
                        <select x-model="id_version" x-on:change="loadOrtoggleModifications(true)">
                            <option value="" label="{{__('Version initiale')}}"></option>
                            <template x-for="version in labdocData.versioning">
                                <option :value="version.id_trace" :label="version.date" :selected="parseInt(id_version)===parseInt(version.id_trace)"></option>
                            </template>
                        </select>
                    </template>
                    <template x-if="labdocData && labdocData.versioning.length===0">
                        <span>{{__('Initiale')}}</span>
                    </template>
                </div>
                <button x-show="!loading" x-on:click="loadOrtoggleModifications()" class="ld-diff-toggle-button">
                    <i :class="{'fa-solid fa-plus-minus':!displayDiff, 'fa fa-times':displayDiff}"
                        :title="displayDiff?'{{__("Quitter le mode d’affichage des modifications")}}'
                                :'{{__("Afficher les modifications apportées à ce labdoc par rapport à une version source")}}'"></i>
                </button>
                <i x-show="loading" class="fa fa-spinner fa-pulse"></i>
            </div>
        @endif
        @if(!$ldv->is_follow)
        <div x-data x-show="$store.reportLabdocs.isNewLd('{{$ldv->ld->id_labdoc}}')" title="{{__("Annuler l'ajout du labdoc")}}"
            onclick="cancelLD({{$ldv->ld->id_labdoc}})">
            <i class="fa fa-times lb-ld-button"></i> 
        </div>
        @endif
    </div>
</div>
