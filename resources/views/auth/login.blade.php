<?php
    /** @var \App\Helper $helper */
    /** @var boolean $bind is the login form for a binding (Cas, Moodle, Class code) */
?>
@extends('layouts.home')

@push('header')
    <?= $helper->loadAsset("/js/login.js") ?>
    <?= $helper->loadAsset("/libraries/bowser.min.js") ?>
    <?= $helper->loadAsset("/css/login.css") ?>
@endpush

@section('bodyattributes', "onload='login_setFocus();'")
@section('title', __('Connexion'))

@section('topbar')
    <div class="container">
        <div class="row center-xs">
            <div class="col-xs-11 col-sm-11">
                <div id="lb_logo">
                    <a class="lb-a" href="https://labnbook.fr" target="_blank"><img src="/images/LNB-logo.svg" alt="LabNBook"/></a>
                    <a class="lb-a" href="https://labnbook.fr" target="_blank"><h1 class="lb-h1">{{__('Le cahier numérique partagé pour écrire et apprendre les sciences')}}</h1></a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('maincontent')   {{-- Utilisation de Flexbox Grid / http://flexboxgrid.com/ : container, row, col-xs... --}}
<div id="log-in-container">
    <div class="row center-xs">
        @include('auth/_login_block',
            [
                'login_title_cas' => config('labnbook.cas_institution_name'),
                'hasCas' => true
            ]
            )
    </div>

    @if($bind)
        <div class="row">
            <div class="col-xs-12">
                <section id="by-code" class="lb-section lb-section-orange">
                    <div class="row middle-xs">
                        <div class="col-xs-12 col-sm-7 middle-xs start-md">
                            <h2><span class="lb-h2">{{__('INSCRIPTION ')}}</span><span class="lb-h3"> {{__('avec un code')}}</span></h2>
                            <div id="lnb-code">
                                {{__("Votre enseignant vous a fourni un code d'inscription ?")}}
                                <ul>
                                    <li>{{__('si vous avez déjà un compte LabNBook, commencez par vous connecter, puis fournissez le code dans "Mon compte"')}}</li>
                                    <li>{{__("si vous n'avez pas de compte LabNBook, entrez le code ici pour créer votre compte")}}</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-5">
                            <div><input class="lb-login-input lb-input-orange" type="text" name="user_code" id="user_code" placeholder="{{__('Code')}}"/></div>
                            <div><input id="login-code" class="lb-login-input lb-login-input-submit lb-btn-orange" type="submit" value="{{__("Valider")}}"/></div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    @endif

</div>
@endsection
