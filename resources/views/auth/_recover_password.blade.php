<h2 class="lb-h2">{{__('Récupération de vos informations de connexion')}}</h2>
<p>{{__("Vous avez perdu votre identifiant ou votre mot de passe ? Indiquez l'adresse e-mail associée à votre compte :")}}</p>
<div id="recover-password-form">
    <p><input class="lb-login-input lb-input-blue" type="email" name="user_email" id="user_email" placeholder="{{__('E-mail')}}"/></p>
    <p><input id="login-mail" class="lb-login-input lb-login-input-submit lb-btn-blue" type="button" value="{{__('Valider')}}" /></p>
</div>