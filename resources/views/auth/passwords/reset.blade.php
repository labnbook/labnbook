<?php
/** @var \App\Helper $helper */
?>
@extends('layouts.labnbook')

@section('head')
    <?= $helper->loadAsset("/css/index.css") ?>
    <style type="text/css">
        input {
            padding: 2px;
        }

        #lb_header {
            border-collapse: collapse;
        }

        #lb_header tr > td {
            padding: 10px;
            background-color: #e7e8d7;
        }
    </style>

    <script>
        var token = @json($token);
    </script>

    <?= $helper->loadAsset("/js/reset_pwd.js") ?>

@endsection

@section('title', __('Mise à jour de mot de passe'))
@section('body')
    <table id="lb_header" width="100%">
        <tr>
            <td><img width="450" src="/images/LNB-logo.svg" alt="LabNBook"/></td>
            <td><h1 class="lb-h1">{{__("Mise à jour de mot de passe")}}</h1></td>
        </tr>
    </table>

    <br/>
    <p>{{__("Votre mot de passe doit comprendre entre 8 et 20 caractères avec des lettre minuscules et majuscules.")}}</p>
    <br/>
    <table>
        <tr>
            <td width="20px">
            </td>
            <td align="right">{{__("Nom du compte")}}{!!__("&nbsp;:")!!} </td>
            <td>
                <input required type="text" name="login" id="login" size="18"/></td>
        </tr>
        <tr>
            <td></td>
            <td align="right">{{__("Nouveau mot de passe")}}{!!__("&nbsp;:")!!} </td>
            <td><input required type="password" name="pwd" id="new_pwd" size="18"/></td>
        </tr>
        <tr>
            <td></td>
            <td align="right">{{__("Confirmation du mot de passe")}}{!!__("&nbsp;:")!!}</td>
            <td><input required type="password" name="pwd2" id="new_pwd2" size="18"/></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td style="text-align:center"><input class="btn" type="button" name="maj_pwd" value="{{__("Mettre à jour")}}" onclick="setNewPwd()"/></td>
        </tr>
    </table>
    <br/>

    <table id="ff_lb" width="100%">
        <tr>
            <td align="center"><a href="/">{{__("Retour à la page d'accueil")}}</a></td>
        </tr>
    </table>


@endsection
