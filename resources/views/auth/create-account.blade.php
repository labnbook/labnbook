<?php
/** @var \App\Helper $helper */
/** @var \App\Processes\Ldap\LdapUser $ldapUser */
/** @var boolean $hasCas */
/** @var array $similarities */
/** @var array $id_inst */
/** @var string $authType */

// $authType = "Moodle";

if ($authType === 'Cas') {
    $context = __("Créer un accès LabNBook avec votre compte institutionnel");
} elseif ($authType == 'code') {
    $context = __("Utilisation d'un code d'inscription fourni par un enseignant");
} else {
    $context = __("Liaison d'un compte Moodle à LabNBook");
}
$title = __("Gestion de compte");
$cas_name = config('labnbook.cas_institution_name');
?>

@extends('layouts.home')

@push('header')
    <?= $helper->loadAsset("/js/user_bind.js") ?>
    <?= $helper->loadAsset("/css/login.css") ?>
    <?= $helper->loadAsset("/libraries/bowser.min.js") ?>
@endpush

@section('title', $title)

@section('topbar')
    <div class="container">
        <div class="row center-xs">
            <div class="col-xs-11 col-sm-11">
                <div id="lb_logo">
                    <a class="lb-a" href="/login"><img src="/images/LNB-logo.svg" alt="LabNBook"/></a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('maincontent')
<div id="bind_main_content">
    <h2 class="lb-h2"> {{ $context }} </h2>
    <div id="create-account-content">
        @if($authType == 'Cas')
            <div class="create-account-option" id="homonymes-cas">
                <h3 class="lb-h3">{{__('Nous avons détecté des comptes LabNBook qui pourraient vous appartenir.')}}</h3>
                <p>{{__("Si vous souhaitez qu'un de ces comptes soit lié à votre connexion institutionnelle, choississez le ci-dessous et indiquez son mot de passe actuel.")}}
                    {{' '}}{{__("Attention : ceci supprimera l'accès actuel au compte récupéré.")}}</p>
                <div class="row center-xs">
                    <section class="lb-section lb-section-orange col-xs-12 col-sm-9 col-md-7">
                        @foreach ($homonymes as $u)
                            @if ($u->isInternal())
                                <div class='homonymes'>
                                    <input class="account-checkbox" type="radio" id="{{$u->login}}" name="login" value="{{$u->login}}" onclick="showRadioForm('{{$u->login}}')">
                                    <div class="row middle-xs">
                                        <label for="{{$u->login}}">{{$u->first_name . ' '. $u->user_name . ' (' . __("compte") . ' ' . $u->login . ')'}}</label>
                                        <span class='homonymes-form'>
                                            <form id="login-form-{{$u->login}}" class="login" method="POST" style="display:none" action="/login">
                                                <input class="lb-login-input-ltl lb-input-orange" type="password" name="password" placeholder="{{__("Mot de passe")}}">
                                                <input type="hidden" name="login" value="{{$u->login}}">
                                                @csrf
                                            </form>
                                        </span>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                        <div class='homonymes'>
                            <input class="account-checkbox" type="radio" id="none" name="login" value="none" onclick="showRadioForm('none')">
                            <div class="row">
                                <label for="none">{{__('Ne pas reprendre de compte ci-dessus')}}</label>
                                <form id="login-form-none" action='/auth/cas/login' method="POST">
                                    @csrf
                                </form>
                            </div>
                        </div>
                        <div>
                            <button id="cas-continue" class="lb-login-input lb-login-input-submit lb-btn-orange" onclick="handleCasContinue()">{{__("Créer l'accès")}}</button>
                        </div>
                    </section>
                </div>
                <div class="create-account-option">
                    <p>{{__("Si vous souhaitez vous connecter sans utiliser l'annuaire institutionnel ou si vous avez oublié un mot de passe, retournez à la page de connexion de LabNBook en cliquant sur l'image en haut d'écran.")}}</p>
                </div>
            </div>

        @else <!-- Connexion via code or Moodle -->
            <?php  $disabled_bind = ($authType === "Moodle") ? 'readonly' : ''; ?>

            <div x-data="{displayed_connexion:false}">

                @if(!$homonymes->isEmpty()) <!-- this case can happen only for a Moodle connexion -->
                    <div class="create-account-option" id="homonymes_list">
                        <h3 class="lb-h3">{{__('Nous avons détecté des comptes LabNBook qui pourraient vous appartenir.')}}</h3>
                        <p>{{__('Si un de ceux-ci vous appartient choississez-le et connectez-vous.')}}</p>
                        <div class="row center-xs">
                            <div class="col-xs-12 col-sm-9 col-md-7">
                                <section class="lb-section lb-section-orange">
                                    @foreach ($homonymes as $u)
                                        <div class='homonymes'>
                                            <a class="lb-a" href='#' x-on:click="displayed_connexion = true; await $nextTick(); prelogin('{{$u->isInternal()}}', '{{$u->login}}', 'true');">
                                                {{$u->first_name.' '.$u->user_name}}&nbsp;-&nbsp;
                                                {{__("compte :") . " " . $u->login}}{{$u->isInternal() ? "" : " ".__("(compte institutionnel :casname)", ['casname' => $cas_name])}}
                                            </a>
                                        </div>
                                    @endforeach
                                </section>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="create-account-option">
                    <h3 class="lb-h3">
                        {{__("Si vous avez déjà un accès à LabNBook : ")}}
                        <a class="lb-a" href='#' x-on:click="displayed_connexion=!displayed_connexion">
                             {{__("connectez-vous")}}
                            <i class="display-option" :class="{'fa fa-chevron-right fa-align-right':!displayed_connexion,
                                'fa fa-chevron-down fa-align-right':displayed_connexion}"></i>
                        </a>
                    </h3>
                    <template x-if="displayed_connexion">
                        <div id="log-in" class="row center-xs">
                            @include('auth/_login_block',
                                [
                                    'login_title_cas' => $cas_name,
                                    'hasCas' => $hasCas
                                ]
                                )
                        </div>
                    </template>
                </div>

                <div class="create-account-option" x-data="{displayed_creation:false}">
                    <h3 class="lb-h3">
                        {{__("Si vous n'avez pas d'accès à LabNBook : ")}}
                        <a class="lb-a" href='#' x-on:click="displayed_creation=!displayed_creation">
                            {{__("créez un nouveau compte")}}
                            <i class="display-option" :class="{'fa fa-chevron-right fa-align-right':!displayed_creation,
                                'fa fa-chevron-down fa-align-right':displayed_creation}"></i>
                        </a>
                    </h3>
                    @if (old('accept_cgu'))
                        <span x-init="displayed_creation=true"></span>
                    @endif
                    <div class="row center-xs">
                        <template x-if="displayed_creation">
                            <div class="col-xs-12 col-sm-10 col-md-8">
                                <section class="lb-section lb-section-orange">
                                    <form action="{{$confirmUrl}}" method="POST" id="create-account"
                                          x-on:keyup.debounce.500ms="if($event.target.tagName === 'INPUT' && $event.target.type !== 'password') {checkFormHomonymes()}">
                                        @csrf
                                        <input type="hidden" name="confirm" value="ok"/>
                                        <input type="hidden" name="id_inst" value="{{$id_inst}}"/>
                                        @if ($authType === "code")
                                            <input type="hidden" name="code" value="{{$class_code}}"/>
                                        @endif
                                        <h2 class="lb-h2">CRÉATION DE COMPTE</h2>
                                        <div class="row middle-xs create-account-item">
                                            <label>{{"Prénom"}}{!!__("&nbsp;:")!!}</label>
                                            <input
                                                class="lb-login-input-ltl lb-input-orange input-large"
                                                name="first_name"
                                                type="text" {{$disabled_bind}}
                                                value="{{old('first_name') ? old('first_name') : $user->first_name}}"
                                                placeholder="{{__("requis")}}"
                                                data-error="{{__("Veuillez indiquer votre prénom")}}"
                                                required/>
                                        </div>
                                        <div class="row middle-xs create-account-item">
                                            <label>{{"Nom"}}{!!__("&nbsp;:")!!}</label>
                                            <input
                                                class="lb-login-input-ltl lb-input-orange input-large"
                                                name="user_name"
                                                type="text" {{$disabled_bind}}
                                                value="{{old('user_name') ? old('user_name') : $user->user_name}}"
                                                placeholder="{{__("requis")}}"
                                                data-error="{{__("Veuillez indiquer votre nom")}}"
                                                required/>
                                        </div>
                                        <div class="row middle-xs create-account-item">
                                            <label>{{"N° étudiant"}}{!!__("&nbsp;:")!!}</label>
                                            <input
                                                class="lb-login-input-ltl lb-input-orange input-large"
                                                name="inst_number"
                                                type="text" {{$disabled_bind}}
                                                value="{{old('inst_number') ? old('inst_number') : $user->inst_number}}"/>
                                        </div>
                                        <div class="row middle-xs create-account-item">
                                            <label>{{"E-mail"}}{!!__("&nbsp;:")!!}</label>
                                            <input
                                                class="lb-login-input-ltl lb-input-orange input-large"
                                                id="create-account-email"
                                                name="email"
                                                type="text"
                                                value="{{old('email') ? old('email') : $user->email}}"/>
                                        </div>
                                        <div class="account-info">
                                            {{__("Une adresse e-mail permet de récupérer un mot de passe oublié")}}
                                        </div>
                                        <div class="row middle-xs create-account-item">
                                            <label> {{"Identifiant"}}{!!__("&nbsp;:")!!}</label>
                                            <input class="lb-login-input-ltl lb-input-orange"
                                                   name="login"
                                                   type="text"
                                                   id="user_login_create"
                                                   value="{{old('login') ? old('login') : $user->login}}"
                                                   placeholder="{{__("requis")}}"
                                                   data-error="{{__("Veuillez indiquer un identifiant pour votre compte LabNBook")}}"
                                                   required/>
                                        </div>
                                        <div class="row middle-xs create-account-item" x-data="{showPassword:false}">
                                            <label>{{__('Mot de passe')}}{!!__("&nbsp;:")!!}</label>
                                            <input class="lb-login-input-ltl lb-input-orange"
                                                   name="password"
                                                   :type="showPassword?'text':'password'"
                                                   id="create-account-pwd"
                                                   pattern="(?=.*[a-z])(?=.*[A-Z]).{8,}"
                                                   placeholder="{{__("requis")}}"
                                                   data-error="{{__("Veuillez indiquer un mot de passe d'au moins 8 lettres avec des minuscules et des majuscules")}}"
                                                   required/>
                                            <button type="button" class="toggle-password" x-on:click="showPassword=!showPassword">
                                                <i class="far" :class="{'fa-eye-slash':showPassword,'fa-eye':!showPassword}"></i>
                                            </button>
                                        </div>
                                        <div class="row middle-xs create-account-item" x-data="{showPassword:false}">
                                            <label>{{__('Confirmation')}}{!!__("&nbsp;:")!!}</label>
                                            <input class="lb-login-input-ltl lb-input-orange"
                                                   name="password2"
                                                   :type="showPassword?'text':'password'"
                                                   id="create-account-pwd2"
                                                   placeholder="{{__("requis")}}"
                                                   required/>
                                            <button type="button" class="toggle-password" x-on:click="showPassword=!showPassword">
                                                <i class="far" :class="{'fa-eye-slash':showPassword,'fa-eye':!showPassword}"></i>
                                            </button>
                                        </div>
                                        <div class="account-info">
                                            {{__("8 caractères minimum avec au moins une majuscule et une minuscule")}}
                                        </div>
                                        @if ($authType === 'Moodle')
                                            <div class="account-info">
                                                {{__("Le mot de passe ne sert qu'en cas de connexion directe à LabNBook")}}
                                            </div>
                                        @endif
                                        @if (config('labnbook.cgu_url'))
                                            <div id="lnb-cgu">
                                                <input type="checkbox" required
                                                       name="accept_cgu"
                                                       data-error="{{__("Vous devez lire et accepter les CGU")}}"
                                                    @if (old('accept_cgu'))
                                                        checked
                                                    @endif
                                                />
                                                {{__("J'ai lu et j'accepte")}}&nbsp;<a href='{!! config("labnbook.cgu_url")!!}' target='_blank'>{{__("les Conditions Générales d'Utilisation de LabNBook")}}</a>
                                            </div>
                                        @endif
                                        <button id="user_create_validate" class="lb-login-input lb-login-input-submit lb-btn-orange" type="button" onclick="createAccount()">
                                            {{__("Créer le compte")}}
                                        </button>
                                    </form>
                                </section>
                            </div>
                        </template>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>

@endsection
