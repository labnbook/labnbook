<div class="col-xs-12 col-sm lnb-connexion">
    <section class="lb-section lb-section-blue center" x-data="{open_recover:false}">
        <h2 class="lb-h2">{{__('CONNEXION')}}</h2>
        <h3 class="lb-h3">{{__("avec un compte LabNBook")}}</h3>
        <form id="login-lnb" action="{{ route('login') }}" method="POST">
            @csrf
            <p><input class="lb-login-input lb-input-blue" type="text" name="login" id="user_login" value="{{ old('login') }}"
                      placeholder="{{__('Compte')}}"/></p>
            <p><input class="lb-login-input lb-input-blue" type="password" name="password" id="user_pwd" placeholder="{{__('Mot de passe')}}"/></p>
            <p><input class="lb-login-input lb-login-input-submit lb-btn-blue" type="submit" value="{{__("Se connecter")}}"/></p>
        </form>
        <p id="recover-password-container">
            <a class="lb-a" href="#" x-on:click="open_recover=!open_recover">
                {{__("Mot de passe oublié")}}
                <i class="fa fa-align-right" :class="{'fa-chevron-right':!open_recover,'fa-chevron-down':open_recover}"></i>
            </a>
        </p>

        <div id="recover-password" :class="{'visible': open_recover}">
            @include('auth/_recover_password')
        </div>
    </section>
</div>

@if (!empty(config('cas.cas_hostname')) && config('cas.cas_hostname')!='none' && (!isset($hasCas) || $hasCas))
    <div class="col-xs-12 col-sm lnb-connexion">
        <section class="lb-section lb-section-green center">
            <h2 class="lb-h2">{{__('CONNEXION')}}</h2>
            <h3 class="lb-h3">{{__("avec un compte institutionnel") . " " . $login_title_cas}}</h3>
            <div id="login-CAS">
                <p>
                    <a class="lb-a" href="#" onclick="$('#frm-login-cas').submit()"
                       title="{{__("Se connecter par le service d'authentification de l'Université Grenoble-Alpes")}}">
                        <img src="/{{config('labnbook.cas_logo_filepath')}}"
                             alt="{{__('Connexion CAS-:name', ['name' => config('labnbook.cas_name')])}}" width="120px"/>
                    </a>
                </p>
                <form id="frm-login-cas" action="{{ route('login-cas')}}" method="POST">
                    @csrf
                    <p><input class="lb-login-input lb-login-input-submit lb-btn-green" type="submit" value="{{__("Se connecter")}}"/></p>
                </form>
            </div>
        </section>
    </div>
@endif
