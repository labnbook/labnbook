<?php
    /** @var \App\Helper $helper */
$possibleLanguages = $helper->allLabnbookLanguages();
?>
<?php if (!$user_lang) :?>
    <option value="NULL" selected></option>
<?php endif ?>
<?php foreach ($possibleLanguages as $key => $language) :?>
    <option value="{{$key}}"{{($user_lang === $key ? " selected" : "")}}>{{$language}}</option>
<?php endforeach ?>
