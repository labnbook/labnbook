<?php
    /** @var Helper $helper */
    /** @var TeamConfig $settings */
    /** @var array $teams */
    /** @var boolean $canAdd */

?>
@extends('layouts.home')

@push('header')
    <?= $helper->loadAsset("/js/report/team_choice.js"); ?>
    <?= $helper->loadAsset("/css/team_choice.css"); ?>
@endpush

@section('title', __('Equipes'). " / ".htmlspecialchars($settings->mission->code))

@section('topbar')
    <div class="container">
        <div class="row center-xs">
            <div class="col-xs-11 col-sm-11">
                <div id="lb_logo">
                    <a class="lb-a" href="/report/"><img src="/images/LNB-logo.svg" alt="LabNBook"/></a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('maincontent')
    <h2 class="lb-h2">{{__('Rapport :code', ['code' => $settings->mission->code])}}</h2>
    <h3 class="lb-h3">{{__("Choisissez l'équipe à rejoindre puis validez votre choix en bas de page")}}</h3>
    <p>
        {!! __("La taille souhaitée des équipes est de <strong> :size </strong> personnes.", [ 'size' => $settings->size_opt ])!!}
        @if($settings->teams_max)
            {{ __("Au maximum : :max équipes.", [ 'max' => $settings->teams_max ])}}
        @endif
    </p>
    <p><strong>{{__("Attention :")}}</strong> {{__("choix définitif ! Seul votre enseignant peut modifier la composition des équipes.")}}</p>
    <div
        class="teams"
        x-data='{
            settings: @json($settings),
            teams: @json($teams),
            user: @json(auth()->user()),
            canAdd: @json($canAdd),
            current_team_id: null}'
        x-init="TeamsCtrl.init($data)">

        <template x-for="team in teams" :key="team.id_report">
            <div class="team" :class="TeamsCtrl.canJoin(team) ? 'btn_pointer' : ''" x-on:click="TeamsCtrl.join(team)">
                <input type="radio"
                       class="team-input"
                       name="selected_team"
                       x-show="TeamsCtrl.canJoin(team) || current_team_id === team.id_report"
                       x-model="current_team_id"
                       :value="team.id_report"
                       />
                <span class="pad_radio" x-show="!TeamsCtrl.canJoin(team) && current_team_id !== team.id_report"></span>
                <div class="team_header lb-section-orange">
                    <span x-text="team.name"></span>
                </div>
                <div class="team_members">
                    <template x-for="(m, i) in team.members" :key="m.id_user">
                        <span>
                            <span x-text="TeamsCtrl.displayName(m)" :class="TeamsCtrl.isMe(m) ? 'self' : ''"></span>
                            <span x-show="i < team.members.length -1">&nbsp;-&nbsp;</span>
                        </span>
                    </template>
                </div>
                <div x-show="team.members.length ===0" class="team_members empty_members">
                    {{__("Équipe vide")}}
                </div>
            </div>
        </template>
        <div class="button_box" x-data="{waiting: false}">
            <i x-show="waiting" class="fa fa-spinner fa-pulse"></i>
            <button class="lb-select-team-btn lb-btn-blue" x-show="!waiting" x-on:click="waiting=true; await TeamsCtrl.submit(); waiting = false">{{__("Valider")}}</button>
            <button class="lb-select-team-btn lb-btn-orange" x-on:click="window.location.href='/report/'" title="{{__('Retour à la page de choix des missions')}}">{{__("Annuler")}}</button>
        </div>
    </div>
@endsection
