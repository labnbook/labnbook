<?php
/** @var \App\Views\Report $rv */
$widgets = [
        'ressource' => true,
        'task' => !$rv->is_follow,
        'comments' => !$rv->is_pdf && !$rv->is_follow,
        'messagerie' => ((($rv->is_default || $rv->is_view ) && $rv->widg_msg) || $rv->is_follow || ($rv->is_test && $rv->widg_msg)),
        'scoring' => (($rv->is_test || $rv->is_follow || $rv->is_solution) && $rv->rubricExists()) || (!$rv->is_solution && ($rv->is_default || $rv->is_view) && ($rv->assessmentPublished() || $rv->mission->rubric_broadcast)),
        'trash' => $rv->is_default
    ];
$allowed_widget_type = json_encode(array_keys(array_filter($widgets)));
?>
<div id="widgets_column" class="widget-column column" 
     x-data="{ rvNS: reportViewCtrl({{$rv->report}}, '{{$rv->scope}}', {{Auth::user()->widgetConfig}}) }" 
         x-init="rvNS.init('{{$allowed_widget_type}}'); $watch('rvNS.reduced', () => rvNS.reducedExpandColumn()); 
         $watch('rvNS.fixed_to_bottom', () => rvNS.moveColumnPosition()); $nextTick(()=>{rvNS.adjustWidgetColumnSize()});"
    :class="{'reduced':rvNS.reduced, 'expanded': !rvNS.reduced, 'small-screen':rvNS.responsive_small_size}">
    <div :class="{'fixed-bottom':!rvNS.reduced && rvNS.fixed_to_bottom, 'fixed-right': !rvNS.reduced && !rvNS.fixed_to_bottom}">
        <template x-if="!rvNS.reduced && !rvNS.responsive_small_size">
            <div class="widget-column-handle handle-resize" :class="{'left-right':!rvNS.fixed_to_bottom, 'top-down': rvNS.fixed_to_bottom}">
                <button class="widget-extend-btn">
                </button>
            </div>
        </template>
        <div class="widget-part">
            <div class="widgets-buttons" :class="{'lb-section-green':rvNS.reduced}"
                x-on:report-toggle-widget-comment.window="rvNS.toggleWidgetItem(WIDGET.COMMENTS)">
                
                <div class="widgets-buttons-left-part">
                    @if($widgets['ressource'])
                        <div id="lb_menubar_ressource" class="lb-menubar-btn" data-widget-type="ressource">
                            <button class="lnb-widget-menu-btn" type="button" :class="{'active':rvNS.active_widget_type===WIDGET.RESOURCES}"
                                    x-on:click="rvNS.toggleWidgetItem(WIDGET.RESOURCES)" title="{{__("Ressources")}}">
                                <i class="lb-widget-icon far fa-folder-open"></i>
                                <i x-show="false" class="fa-layers-counter fa-layers-top-right fa-3x"></i>
                            </button>
                        </div>
                    @endif

                    @if($widgets['task'])
                        <div id="lb_menubar_task" class="lb-menubar-btn" data-widget-type="task">
                            <button class="lnb-widget-menu-btn" type="button" :class="{'active':rvNS.active_widget_type===WIDGET.TASK}"
                                    x-on:click="rvNS.toggleWidgetItem(WIDGET.TASK)" title="{{__("Ouvrir la liste des tâches du rapport")}}">
                                <i class="lb-widget-icon fa fa-list-check"></i>
                            </button>
                        </div>
                    @endif

                    @if($widgets['comments'])
                        <div id="lb_menubar_comments" class="lb-menubar-btn" data-widget-type="comments">
                            <button class="lnb-widget-menu-btn" type="button" :class="{'active':rvNS.active_widget_type===WIDGET.COMMENTS}"
                                    x-on:click="rvNS.toggleWidgetItem(WIDGET.COMMENTS)" title="{{__("Ouvrir les commentaires")}}">
                                <i class="lb-widget-icon far fa-comment"></i>
                                <i x-show="$store.reportLabdocs.getTotalNewComment()>0" class="fa-layers-counter fa-layers-top-right fa-3x" x-text="$store.reportLabdocs.getTotalNewComment()"></i>
                            </button>
                        </div>
                    @endif

                    @if($widgets['messagerie'])
                        <div id="lb_menubar_messagerie" class="lb-menubar-btn" data-widget-type="messagerie">
                            <button class="lnb-widget-menu-btn" type="button" :class="{'active':rvNS.active_widget_type===WIDGET.MESSAGING}"
                                    x-on:click="rvNS.toggleWidgetItem(WIDGET.MESSAGING)" title="{{__("Messagerie")}}">
                                <i class="lb-widget-icon fa fa-envelope"></i>
                                <i x-show="false" class="fa-layers-counter fa-layers-top-right fa-3x"></i>
                            </button>
                        </div>
                    @endif

                    @if($widgets['scoring'])
                        <div id="lb_menubar_eval" class="lb-menubar-btn">
                            <button class="lnb-widget-menu-btn" type="button" :class="{'active':rvNS.active_widget_type===WIDGET.SCORING}"
                                    x-on:click="rvNS.toggleWidgetItem(WIDGET.SCORING)" title="{{__("Evaluation")}}">
                                <i class="lb-widget-icon fas fa-table"></i>
                            </button>
                        </div>
                    @endif

                    @if($widgets['trash'])
                        <div id="lb_menubar_trash" class="lb-menubar-btn" data-widget-type="trash">
                            <button class="lnb-widget-menu-btn" type="button" :class="{'active':rvNS.active_widget_type===WIDGET.TRASH}"
                                    x-on:click="rvNS.toggleWidgetItem(WIDGET.TRASH)" title="{{__("Ouvrir la corbeille à labdocs")}}">
                                <i class="lb-widget-icon far fa-trash-alt"></i>
                            </button>
                        </div>
                    @endif
                </div>
                
                <div class="widgets-buttons-right-part">
                    <template x-if="!rvNS.reduced && !rvNS.responsive_small_size">
                        <div>
                            <button class="lnb-widget-menu-btn widget-action-btn" type="button" x-on:click="rvNS.fixed_to_bottom = !rvNS.fixed_to_bottom"
                                    :title="rvNS.fixed_to_bottom?'{{__("Positionner à droite")}}':'{{__("Positionner en bas")}}'">
                                <i class="lb-widget-icon fas" :class="{'fa-angles-down':!rvNS.fixed_to_bottom, 'fa-angles-right':rvNS.fixed_to_bottom}"></i>
                            </button>
                        </div>
                    </template>

                    <template x-if="!rvNS.reduced">
                        <div>
                            <button class="lnb-widget-menu-btn widget-action-btn" type="button" x-on:click="rvNS.reduced = true" title="{{__("Réduire les widgets")}}">
                                <i class="lb-widget-icon fas fa-times"></i>
                            </button>
                        </div>
                    </template>
                </div>

            </div>
            <div class="widget-content" x-show="!rvNS.reduced">
                @if($widgets['ressource'])
                    <x-resource-widget x-show="rvNS.active_widget_type === WIDGET.RESOURCES" :report="$rv->report" :add-resource="$rv->add_resource" :display-new-doc="$rv->report->displayNewRessources(Auth::user())"></x-resource-widget>
                @endif
                @if($widgets['task'])
                    <x-task-widget x-show="rvNS.active_widget_type === WIDGET.TASK" :report="$rv->report"></x-task-widget>
                @endif
                @if($widgets['comments'])
                    <x-comment-widget x-show="rvNS.active_widget_type === WIDGET.COMMENTS"></x-comment-widget>
                @endif
                @if($widgets['messagerie'])
                    <x-message-widget x-show="rvNS.active_widget_type === WIDGET.MESSAGING" :id-report="$rv->report->id_report"></x-message-widget>
                @endif
                @if ($widgets['scoring'])
                    <x-scoring-widget x-show="rvNS.active_widget_type === WIDGET.SCORING" :rv="$rv"></x-scoring-widget>
                @endif
                @if($widgets['trash'])
                    <x-trash-widget x-show="rvNS.active_widget_type === WIDGET.TRASH"></x-trash-widget>
                @endif
            </div>
        </div>
    </div>
</div>
