<?php
/**
 * @var \App\Helper $helper
 * @var object $record
 * @var object $id_user
*/
if (empty($record->report) && isset($record->team_config) && $record->team_config->method == \App\TeamConfig::METHOD_TEACHERCHOICE) {
    // do not display when the user has no team and cannot create one
    return;
}
$selector = (isset($record->report) ?
    "#report-{$record->report->id_report}" :
    (isset($record->team_config) ?
        "#teamconfig-{$record->team_config->id_team_config}" :
        "#mission-{$record->mission->id_mission}"
    )
);
$last_synchro = (Auth::user())->last_synchro;

// News for the report
$total_news = 0;
if (isset($record->report)) {
    $message_title = [];
    $nb_ld = $record->report->getNbLDModified($id_user);
    if ($nb_ld){
        $total_news += $nb_ld;
        $message_title[] = $nb_ld.__(" labdoc(s) modifié(s)");
    }
    $nb_com = $record->report->getNbNewComments($id_user);
    if ($nb_com){
        $total_news += $nb_com;
        $message_title[] = $nb_com.__(" nouveau(x) commentaire(s) sur des labdocs");
    }
    $nb_msg = $record->report->getNbNewMessages($id_user);
    if ($nb_msg){
        $total_news += $nb_msg;
        $message_title[] = $nb_msg.__(" nouveau(x) message(s)");
    }
    $nb_annot = $record->report->getNbnewAnnotations($id_user);
    if ($nb_annot){
        $total_news += $nb_annot;
        $message_title[] = $nb_annot.__(" nouvelle(s) annotation(s) d'enseignants");
    }
    $title = implode('&#13;&#10;',$message_title);
}
?>

<div class="menu_mission_item" title="{!!$title!!}"
     onclick="displayMissionDescription(
     {{isset($record->report) ? $record->report->id_report : '0'}},
     {{!isset($record->report) && isset($record->team_config) ? $record->team_config->id_team_config : '0'}},
     {{!isset($record->report) && !isset($record->team_config) && isset($r->mission) ? $record->mission->id_mission : '0'}})"
     data-codename="{{$record->mission->code}}">
    <a class="lb-a" href="#">
        {{$record->mission->code }}
        @if ($total_news)
            <div class="menu-mission-icon">{{$total_news}}</div>
        @endif
    </a>
</div>

