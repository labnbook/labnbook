<?php
/** @var \App\Helper $helper */
/** @var \App\ReportView $rv */
$content =  '';
$rp = "" ;
?>
<x-modal id="lb_pdf_dialog"
         header-class="modal-fixed"
         title="Exporter le rapport en PDF"
         :x-data="json_encode(['displayedModal'=>true])"
         :include-alert="false"
         click-submit-action="document.getElementById('select_ld_print').submit();displayedModal=false"
         click-cancel-action="displayedModal=false">
    <form id='select_ld_print' method='post' target='_blank' action='/report/{{$rv->report->id_report}}?sc=pdf' x-data="{choice_ld:'no_draft'}">
        @csrf
        <fieldset>
            <legend>{{__('Parties et consignes')}}</legend>
            <div class="field-line">
                <input type='checkbox' name='print_assignment' id='print_assignment' value='print'/>
                <label for='print_assignment'>{{__("Afficher les consignes")}}</label>
            </div>
            <div class="field-line">
                <input type='checkbox' name='print_empty_rp' id='print_empty_rp' value='print'/>
                <label for='print_empty_rp'>{{__("Afficher les parties sans labdoc")}}</label>
            </div>
        </fieldset>
        <fieldset>
            <legend>{{__('Types de labdocs')}}</legend>
            <template x-if="'{{Auth::User()->getRole()}}' !== 'teacher' || global_scope !== 'follow'">
                <div class="field-line">
                    <input type='radio' name='choice_ld' id='all_radio' value='all_ld' x-model="choice_ld"/>
                    <label for='all_radio'>{{__("Afficher tous les labdocs")}}</label>
                </div>
            </template>
            <div class="field-line">
                <input type='radio' name='choice_ld' id='nodraft_radio' value='no_draft' checked='checked' x-model="choice_ld"/>
                <label for='nodraft_radio'>{{__("Afficher tous les labdocs sauf ceux cachés")}}</label>
            </div>
            <div class="field-line">
                <input type='radio' name='choice_ld' id='labdocs_radio' value='chose_labdoc' x-model="choice_ld"/>
                <label for='labdocs_radio'>{{__("Choisir les labdocs à afficher")}}</label>
            </div>
        </fieldset>
        <template x-if="choice_ld === 'chose_labdoc'">
            <fieldset id="lb_pdf_choose_ld">
                <legend>{{__('Labdocs à afficher')}}</legend>
                @foreach($rv->labdocs() as $ld)
                    @if(!($ld->draft && $rv->is_teacher && $rv->is_follow))
                        @if($ld->title !== $rp)
                            @php $rp = $ld->title @endphp
                            <h3>{{$rp}}</h3>
                        @endif
                        <div class="field-line">
                            <input type="checkbox" id="ld_chosen{{$ld->id_labdoc}}" name="ld_chosen{{$ld->id_labdoc}}" value="{{$ld->id_labdoc}}" {{$ld->draft ? ' class="hidden-content"' : ""}}/>
                            <label for="ld_chosen{{$ld->id_labdoc}}"> {{$ld->name . ($ld->draft ? " (" . __('caché aux enseignants') . ")" : "")}} </label>
                        </div>
                    @endif
                @endforeach
            </fieldset>
        </template>
    </form>
</x-modal>
