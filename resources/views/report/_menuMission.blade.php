<?php
    /** @var \App\Helper $helper */
    /** @var object[] $records */
    /** @var string $dom_id */
    /** @var string $child_dom_id */
    /** @var boolean $extend */
    /** @var string $title */
    if ($extend) {
        $class="fa-chevron-down";
        $i_title = __("Replier les rapports");
        $display="";
    } else {
        $class="fa-chevron-right";
        $i_title = __("Déplier les rapports");
        $display="style='display:none'";
    }
    if (!$records) {
        $class="";
    }
?>
<div id="{{$dom_id}}" class="menu_mission" >
    
    <!-- Title -->
    <h3 class="menu_mission_title lb-h3" onclick='toggleMissionsCategory($(this))'>
        {{ $title }}
        <i class="fa {{$class}}" title="{{$i_title}}"></i>
    </h3>

    <!-- Content -->
    <div id="{{$child_dom_id}}" class="menu_mission_contents" {!!$display!!}>
        
        @foreach ($records as $r)
            @include('report/_missionMenuItem', ['record' => $r, 'id_user' => $id_user])
        @endforeach
        
    </div>
    
</div>
