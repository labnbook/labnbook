<?php
/** @var \App\Helper $helper */
/** @var \App\Views\Report $rv */
?>
@extends('layouts.home')

@section('title', $rv->title)

@push('header')
    <script type="text/javascript">
        // Variables globales javascript
        window.global_scope = "{{ $rv->scope }}";
        window.global_id_report = "{{  $rv->report->id_report  }}";
        window.global_id_mission = "{{  $rv->mission->id_mission  }}";
        window.global_stop_sync = Boolean("{{ $rv->freeze_lds_on_init }}");
        window.global_msg = "{{ $rv->widg_msg ? "1":"0" }}";
        window.global_tab_ld = {!!  json_encode($rv->report->fetchAllLabdocs())  !!};
        window.global_external_test_report = Boolean("{{ $rv->is_external_test_report }}");
    </script>

    <!-- css -->
    {!!  $helper->loadAsset("/css/report.css")  !!}
    {!!  $helper->loadAsset("/css/labdoc_versions.css")  !!}
    {!!  $helper->loadAsset("/css/labdoc_diff.css")  !!}
    {!!  $helper->loadAsset("/tool_fitex/fitex.css")  !!}
    {!!  $helper->loadAsset("/tool_copex/v1_xml/copex.css")  !!}
    {!!  $helper->loadAsset("/tool_copex/v2_json/copex.css")  !!}
    {!!  $helper->loadAsset("/tool_copex/v2_json/dosage-spectro/dosage-spectro.css")  !!}
    @if ($rv->is_follow) {!! $helper->loadAsset("/tool_fitex/fitex_follow.css")  !!} @endif
    @if ($rv->is_pdf) {!! $helper->loadAsset("/css/report_print.css") !!} @endif

    <!-- Libraries -->
    @if (!$rv->read_only)
        <!-- TinyMCE -->
        {!!  $helper->loadAsset("/libraries/tinymce/tinymce.min.js")  !!}
        {!!  $helper->loadAsset("/libraries/katex/katex.min.js") !!}
        {!!  $helper->loadAsset("/libraries/katex/contrib/mhchem.min.js") !!}
        {!!  $helper->loadAsset("/libraries/katex/katex.min.css") !!}
    @endif
    {!!  $helper->loadAsset("/libraries/katex/katex.min.js") !!}
    {!!  $helper->loadAsset("/libraries/katex/contrib/mhchem.min.js") !!}
    {!!  $helper->loadAsset("/libraries/katex/katex.min.css") !!}

    <!-- functions -->
    @php
        if ($rv->is_pdf && !$rv->puppeteer) {
            $helper->addAlert("info", __('Préparation du PDF en cours').' <i class="fa fa-spinner fa-pulse"></i>');
            echo $helper->loadAsset("/js/puppeteer/Ajax_PDF.js");
    @endphp
        <script>prepare_pdf(@json($rv->puppeteerData)); </script>
    @php
        }
    @endphp
    <!-- MAIN JS -->
    {!!  $helper->loadAsset("/js/report.js")  !!}
    {!!  $helper->loadAsset("/js/labdoc.js")  !!}
    {!!  $helper->loadAsset("/js/widgets/trash.js"); !!}

    <!-- COPEX -->
    {!!  $helper->loadAsset("/tool_copex/v1_xml/jstree/jstree_pre1.0_fix_1/jquery.jstree.js")  !!}
    {!!  $helper->loadAsset("/tool_copex/v1_xml/copex_model.js")  !!}
    {!!  $helper->loadAsset("/tool_copex/v1_xml/copex_view.js")  !!}
    {!!  $helper->loadAsset("/tool_copex/v1_xml/copex_control.js")  !!}
    {!!  $helper->loadAsset("/libraries/sortable/Sortable.min.js")  !!}
    {!!  $helper->loadAsset("/tool_copex/v2_json/copex.js")  !!}
    {!!  $helper->loadAsset("/tool_copex/v2_json/simulation.js")  !!}
    {!!  $helper->loadAsset("/tool_copex/v2_json/titrab/titrab.js")  !!}
    {!!  $helper->loadAsset("/tool_copex/v2_json/dosage-spectro/dosage-spectro.js")  !!}
    {!!  $helper->loadAsset("/libraries/plotly/plotly-2.11.1.min.js") !!} 

    <!-- FITEX -->
    {!!  $helper->loadAsset("/libraries/mathjs/math.min.js")  !!}
    {!!  $helper->loadAsset("/libraries/ml-levenberg-marquardt/ml-levenberg-marquardt.js")  !!}
    {!!  $helper->loadAsset("/tool_fitex/libraries/jquery.flot.all.min.js")  !!}
    {!!  $helper->loadAsset("/tool_fitex/libraries/jquery.contextMenu.min.js")  !!}
    {!!  $helper->loadAsset("/tool_fitex/libraries/jquery.contextMenu.css")  !!}
    {!!  $helper->loadAsset("/tool_fitex/fitex_view.js")  !!}
    {!!  $helper->loadAsset("/tool_fitex/fitex_control.js")  !!}
    {!!  $helper->loadAsset("/tool_fitex/fitex_model.js")  !!}

    <!-- ZWIBBLER -->
    {!!  $helper->loadAsset("/tool_zwibbler/zwibbler2.js")  !!}
    {!!  $helper->loadAsset("/tool_zwibbler/zwibbler_fn_for_lb.js")  !!}
    {!!  $helper->loadAsset("/tool_zwibbler/zwibbler.css")  !!}

    <!-- Annotator -->
    {!!  $helper->loadAsset("/libraries/annotator-full.1.2.10/annotator-full.min.js")  !!}
    {!!  $helper->loadAsset("/js/widgets/annotator.js")  !!}
    {!!  $helper->loadAsset("/libraries/annotator-full.1.2.10/annotator.min.css")  !!}
    {!!  $helper->loadAsset("/libraries/annotator-full.1.2.10/plugins/categories.js")  !!}
    {!!  $helper->loadAsset("/libraries/annotator-full.1.2.10/plugins/categories.css")  !!}
    {!!  $helper->loadAsset("/libraries/annotator-full.1.2.10/plugins/header.js")  !!}
    {!!  $helper->loadAsset("/libraries/annotator-full.1.2.10/plugins/header.css")  !!}
    {!!  $helper->loadAsset("/libraries/annotator-full.1.2.10/plugins/resize.js")  !!}
    {!!  $helper->loadAsset("/libraries/annotator-full.1.2.10/plugins/resize.css")  !!}

    <!-- labdocs code -->
    <?= $helper->loadAsset("/css/labdoc_code.css") ?>
    
    <script type="text/javascript">
        // variable transmises à report_init.js
        @php
            $report_env = array(
                "print_empty_rp" => $rv->print_empty_rp,
                "print_assignment" => $rv->print_assignment,
                "scope_js" => $rv->scope,
                "id_user_js" => Auth::id(),
                "id_report_js" => $rv->report->id_report,
                "refresh_period" => config('labnbook.refresh_period'),
                "show_assignments" => $rv->assignment,
                "report_end_datetime_js" => $rv->report->end_datetime,
                "freeze_lds_on_init" => $rv->freeze_lds_on_init,
                "open_scoring_on_init" => $rv->open_scoring_on_init
            );
        @endphp
        var report_env = {!!  json_encode($report_env)  !!};
    </script>
    {!! $helper->loadAsset("/js/report_init.js") !!}
@endpush

<!-- ----- ------------------------------------------------------------------- ----- -->
<!-- ----- Logged in top bar section is not displayed in pdf and follow scopes ----- -->
<!-- ----- ------------------------------------------------------------------- ----- -->
@section('bodyattributes', 'class="scope-'.$rv->is_pdf ? "pdf" : "default" .'"')

@section('maincontent')
    
    <div id="headers_wrapper" @class(['pdf' => $rv->is_pdf])>

        <section id="report_header">
            <div id="report-header-left-btn" class="row middle-sm start-xs">
                @if(($rv->is_default || $rv->is_view) && !$rv->is_solution)
                    <div id="change-report-btn">
                        <a class="lb-a" href="/reports"><img src="/images/LNB_icon.svg" alt="{{__('Choisir un autre rapport')}}" title="{{__('Choisir un autre rapport')}}"/></a>
                    </div>
                @endif
                @if(!$rv->is_pdf)
                    <div id="lb-toggle" class="header-btn" onclick="toggleAllLD()"><i id="lb_toggle_all_ld" class="fa fa-chevron-right"></i></div>
                @endif
            </div>

            <div id="report-header-title">
                @if($rv->is_pdf)
                    LabNBook -
                @endif
                {{ $rv->mission->code }}
                <span id="lb_mission_name">{{ $rv->mission->name }}</span>
            </div>

            <div id="report-header-right-btn" class="row middle-sm around-sm end-xs reverse-xs">
                    @if (($rv->is_default || $rv->is_view) && !$rv->is_solution)
                        <div id="lb_menubar_menu" class="lb-menubar-menu" :class="{'touched':touched}" x-data="{touched:false}"
                            x-on:touchstart="touched=true"
                            x-on:touchstart.outside="touched=false">
                            <i class="lb-menubar-menu-icon fa fa-bars"></i>
                            <div id='lb_menubar_submenu' class="lb-menubar-submenu">
                                <a class="lb-a" href="/reports/" onclick="return lockedByEditing()">
                                    <div class="lb-menubar-submenu-item">
                                        <img class="lb-menubar-submenu-icon" src="/images/LNB_icon-2.svg" width="13" height="16"/>
                                        <span class="lb-menubar-submenu-text">{{__('Choisir un autre rapport')}}</span>
                                    </div>
                                </a>
                                <a class="lb-a" href="#" onclick="openDialogPrintPage('{{ Auth::User()->getRole() }}', {{ $rv->report->id_report }})">
                                    <div class="lb-menubar-submenu-item">
                                        <i class="lb-menubar-submenu-icon far fa-file-pdf"></i>
                                        <span class="lb-menubar-submenu-text">{{__('Exporter le rapport en PDF')}}</span>
                                    </div>
                                </a>
                                @if($rv->is_default && $rv->report->status !== 'tuto')
                                    <a class="lb-a" href="#" onclick="submitReport();">
                                        <div class="lb-menubar-submenu-item">
                                            <i class="lb-menubar-submenu-icon fa fa-paper-plane"></i>
                                            <span class="lb-menubar-submenu-text">{{__("Rendre le rapport à l'enseignant")}}</span>
                                        </div>
                                    </a>
                                @endif
                                <a class="lb-a" href="https://labnbook.fr/tutorials/" target="_blank">
                                    <div class="lb-menubar-submenu-item">
                                        <i class="lb-menubar-submenu-icon fa fa-question"></i>
                                        <span class="lb-menubar-submenu-text">{{__("Accéder à l'aide")}}</span>
                                    </div>
                                </a>
                            </div>
                        </div>
                    @endif
                    @if ($rv->is_follow)
                        <div id="lb-freeze-sync" x-data="{stop_sync:window.global_stop_sync}"
                             class="header-btn" :class="{'active':stop_sync}"
                             x-on:click="stop_sync = !stop_sync;toggleFreezeSyncLD();"
                             :title="window.getTitleFreezeBtn(stop_sync)">
                            <i class="fa-regular fa-snowflake"></i>
                        </div>
                    @endif

                    @if (($rv->is_test || $rv->is_follow || $rv->is_solution) && !$rv->is_pdf)
                        <div id="lb_menubar_print" class="header-btn" onclick="openDialogPrintPage('{{Auth::User()->getRole()}}', '{{$rv->report->id_report}}')" title ="{{__("Charger la version PDF du rapport")}}">
                            <i class="fa fa-print"></i>
                        </div>
                    @endif

                    @if($rv->is_default || $rv->is_view)
                        <div id="lb_menubar_exit" class="header-btn" onclick="event.preventDefault(); logout();" title="{{__("Se déconnecter")}}">
                            <i class="fas fa-power-off"></i>
                        </div>
                    @endif
                </div>
        </section>
        
        <section id="report_sub_header">
            <div class="left">
                @if($rv->is_test)
                    {{ __("Rapport de test : les labdocs ne sont ni enregistrés, ni synchronisés") }}
                @else
                    @if($rv->is_pdf)
                        {{ date('d/m/y - H:i:s') }}
                        &nbsp;|&nbsp;
                    @endif
                    @if($rv->report->team_name)
                            <span class="team-name">{!! $rv->report->team_name . __('&nbsp;: ') !!}</span>
                    @endif
                    {!! $rv->users_in_group !!}
                @endif
            </div>
        </section>
    </div>

    @include('_flash-alerts')

    @if (!$rv->is_pdf)
        <div id="report-wait-for-init">
            <i class="fa fa-spinner fa-pulse"></i> {{__('Chargement en cours')}} ...
        </div>
    @endif

    <div id="report_content" data-responsive="large" @class(["initialized" => $rv->is_pdf])>
        <!-- Zone centrale -->
        <!-- Colonne de rapport -->
        <div id="report_column" class="column">
            
            <div id="lb_main_container">
                
                <!-- ZWIBBLER template -->
                @include('tool/zwibbler')
    
                    <!-- Report -->
                <section id="lb_report_container">
                    <div id="lb_report">
        
                        <!-- les autres RP -->
                        @foreach($rv->report->reportParts()->orderBy('position')->get() as $rp)
                            @php
                                $rpv = $rv->toReportPartView($rp);
                            @endphp
                            <div id="report_part_{{ $rp->id_report_part }}" class="lb_report_part" ld_type="{{ $rpv->type_labdoc }}"  data-id_rp="{{$rp->id_report_part}}">
                                <div id="part_title_actions_{{ $rp->id_report_part }}" class="lb_part_title_actions">
                                    <div class="lb_part_title" title="{{ $rpv->title_text }}">{{ $rp->position . " - " . $rp->title }}</div>
                                    <div class="rp_icon" data-id-report-part="{{ $rp->id_report_part }}">
                                        @if(($rp->assignment || $rp->hasLdAssignment()) && !$rv->is_pdf)
                                            <span class="lb_show_assignment" title='{{__("Afficher les consignes spécifiques pour cette partie du rapport")}}'>{{__('Consignes')}}</span>
                                        @endif
                                        @if($rpv->add_ld && ($rv->is_default || $rv->is_test))
                                            @if($rp->text)
                                                <img title="{{__("Ajouter un labdoc texte")}}" onclick="if(!event.detail || event.detail == 1){ addLD({{$rp->id_report_part}}, 'text') }"
                                                     alt="{{__("LD texte")}}" src="/images/report/ld_type_text.svg" width="16">
                                            @endif
                                            @if($rp->drawing)
                                                <img title="{{__("Ajouter un labdoc dessin")}}" onclick="if(!event.detail || event.detail == 1){ addLD({{$rp->id_report_part}}, 'drawing') }"
                                                    alt="{{__("LD dessin")}}" src="/images/report/ld_type_drawing.svg" width="16">
                                            @endif
                                            @if($rp->procedure)
                                                <img title="{{__("Ajouter un labdoc protocole")}}" onclick="if(!event.detail || event.detail == 1){ addLD({{$rp->id_report_part}}, 'procedure') }"
                                                    alt="{{__("LD protocole")}}" src="/images/report/ld_type_procedure.svg" width="16">
                                            @endif
                                            @if($rp->dataset)
                                                <img title="{{__("Ajouter un labdoc jeu de données")}}" onclick="if(!event.detail || event.detail == 1){ addLD({{$rp->id_report_part}}, 'dataset') }"
                                                    alt="{{__("LD données")}}" src="/images/report/ld_type_dataset.svg" width="16">
                                            @endif
                                            @if($rp->code)
                                                <img title="{{__("Ajouter un labdoc code")}}" onclick="if(!event.detail || event.detail == 1){ addLD({{$rp->id_report_part}}, 'code') }"
                                                     alt="{{__("LD code")}}" src="/images/report/ld_type_code.svg" width="16">
                                            @endif
                                        @endif
                                        @if($rv->is_follow || ($rv->is_solution && !$rv->is_view && !$rv->is_pdf) ||
                                            (($rp->text || $rp->drawing || $rp->procedure || $rp->dataset || $rp->code) && $rv->report->allow_import && !$rv->is_pdf && !$rv->is_view))
                                            <div class='ld_import_icon'>
                                                <img title="{{__("Importer un labdoc d'un autre rapport")}}"
                                                     @if($rv->is_follow)
                                                         onclick="displayLDToImport({{$rp->id_report_part}},-1,1,1,1,1,1)"
                                                     @elseif($rv->is_solution && !$rv->is_view)
                                                         onclick="displayLDToImport({{$rp->id_report_part}},-1,1,1,1,1,1)"
                                                     @else
                                                         onclick="displayLDToImport(
                                                         {{$rp->id_report_part}},
                                                         {{$rv->report->allow_import_id_mission ?? 0 }},
                                                         {{$rp->text}},
                                                         {{$rp->drawing}},
                                                         {{$rp->procedure}},
                                                         {{$rp->dataset}},
                                                         {{$rp->code}})"
                                                     @endif
                                                     alt="{{__("Importer un labdoc")}}"
                                                     src="/images/report/ld_type_import.svg" width="16">
                                                <div x-data="{is_show_import:false, wait_for_data:true, is_follow:{{$rv->is_follow?1:0}}===1, 
                                                    m_code:'{{$rv->report->allow_import_id_mission?\App\Mission::find($rv->report->allow_import_id_mission)->code:''}}'}"
                                                     x-show="is_show_import"
                                                     x-on:click.outside="is_show_import=false" class="ld_import_menu">
                                                    <template x-if="wait_for_data">
                                                        <div class="ld_import_menu_wait">{{__('Construction du menu...')}}</div>
                                                    </template>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                @if($rp->assignment)
                                    <div id="lb_p_assignment_{{$rp->id_report_part}}" class="lb_div_assignment lb_assignment tinyMCE-text">
                                        @if(!$rv->is_pdf)
                                            <i class="fa fa-times lb_assignment_close" title="{{__("Fermer les consignes")}}" 
                                               onclick=" $('#lb_p_assignment_{{$rp->id_report_part}}').hide(); updateIconForToggleAllLD();"></i>
                                        @endif
                                        {!! $rp->assignment !!}
                                    </div>
                                @endif
                                <div id="part_content_{{ $rp->id_report_part }}" class="part_content">
                                        <!-- ici les LD -->
                                    @include('report/_report_part', ['rpv' => $rpv])
                                </div>
                            </div>
                        @endforeach
                    </div>
                </section>
            </div>
        </div>
        
        <!-- Widgets -->
        @if(!$rv->is_pdf)
            @include('report/_report_widgets', ['rv' => $rv])
        @endif
    </div>

    {!!  $helper->loadAsset("/libraries/pica/pica.min.js")  !!}
    {!!  $helper->loadAsset("/js/upload_image.js")  !!}
    {!!  $helper->loadAsset("/libraries/js-xlsx/xlsx.full.min.js")  !!}

@endsection
