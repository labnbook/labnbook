<?php
    /** @var \App\Helper $helper */
    /** @var \App\Views\ReportPart $rpv */
    $idrp = $rpv->reportPart ? $rpv->reportPart->id_report_part : 0;
?>
<ul id="rp_sortable_{{ $idrp }}" class="report-part-sortable" data-id_rp="{{ $idrp }}">
@foreach($rpv->labdocs as $ld)
    <?php
        $ldv = $rpv->toLabdocView($ld);
    ?>
        <div id="labdoc_{{ $ld->id_labdoc }}" class="sortable-labdoc {{ join(" ", $ldv->classes) }}" x-data :class="{'ld_draft':$store.reportLabdocs.isDraft('{{$ld->id_labdoc}}')}" data-id_ld="{{$ld->id_labdoc}}">
            @include('labdoc._ld', ['ldv' => $ldv])
        </div>
@endforeach
</ul>
<ul id="rp_shared_ld_{{ $idrp }}" data-id_rp="{{ $idrp }}">
@foreach($rpv->labdocs_shared as $ld)
    <?php
        $ldv = $rpv->toLabdocView($ld);
    ?>
    <div id="labdoc_{{ $ld->id_labdoc }}" class="{{ join(" ", $ldv->classes) }}" data-id_ld="{{$ld->id_labdoc}}">
        @include('labdoc._ld', ['ldv' => $ldv])
    </div>
@endforeach
</ul>
