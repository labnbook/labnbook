<?php
    /** @var \App\Helper $helper */
    /** @var \App\User $user */
    /** @var object $missionsRecords{ new: [{ mission, report?, team_config? }], on: ... } */
?>

@extends('layouts.home')

@section('title', __('Choix du rapport'))

@push('header')
    <?= $helper->loadAsset("/css/report_choice.css") ?>
    <?= $helper->loadAsset("/js/report_choice.js") ?>
    <?= $helper->loadAsset("/libraries/katex/katex.min.css") ?>
    <?= $helper->loadAsset("/libraries/katex/katex.min.js") ?>
    <?=  $helper->loadAsset("/libraries/katex/contrib/mhchem.min.js") ?>
    <link rel="prefetch" href="{{ $helper->getCacheproofUrl("/libraries/mathjs/math.min.js") }}">
    <link rel="prefetch" href="{{ $helper->getCacheproofUrl("/tool_zwibbler/zwibbler2.js") }}">
    <link rel="prefetch" href="{{ $helper->getCacheproofUrl("/tool_copex/v1_xml/jstree/jstree_pre1.0_fix_1/jquery.jstree.js") }}">
    <link rel="prefetch" href="{{ $helper->getCacheproofUrl("/tool_fitex/fitex_view.js") }}">
    <link rel="prefetch" href="{{ $helper->getCacheproofUrl("/tool_fitex/fitex_control.js") }}">
    <link rel="prefetch" href="{{ $helper->getCacheproofUrl("/tool_fitex/fitex_model.js") }}">
    <link rel="prefetch" href="{{ $helper->getCacheproofUrl("/libraries/js-xlsx/xlsx.full.min.js") }}">
@endpush

@section('topbar')

    <div id="header-content">
        <div class="row middle-xs center-xs start-md between-sm">
            <!-- LOGO -->
            <div class="col-xs-12 col-sm-12 col-md-5">
                <div id="lb_logo">
                    <a href="/reports"><img src="/images/LNB-logo.svg" alt="LabNBook"/></a>
                </div>
            </div>
            <!-- BUTTONS -->
            <div id="nav" class="lnb-topbar-button-box row col-xs-12 col-sm-12 col-md-7 center-xs end-md">
                <div class="box">
                    <a href="#" onclick="openAccountManagement({{Auth::id()}})" id="edit_account_btn">
                        <button class="lnb-topbar-button lb-btn-blue" type="button" id="my-account"><i class="fa fa-user"></i> {{__('Mon compte')}}</button>
                    </a>
                </div>
                @if (Auth::user()->hasTeacher())
                    <div class="box">
                        <a href="{{Auth::user()->teacher->getInterfaceUrl()}}">
                            <button id="lb_btn_teacher_interface" class="lnb-topbar-button lb-btn-green" type="button">{{__("Interface enseignant")}}</button>
                        </a>
                    </div>
                @else
                    <div class="box">
                        <a href="{{ ($helper::selectLanguage() === "en") ? "https://labnbook.fr/en/student-resources/" : "https://labnbook.fr/ressources-etudiants/" }}" target="_blank">
                            <button id="lb_btn_help" class="lnb-topbar-button lb-btn-green" type="button"><i class="fa fa-question"></i> {{__("Aide")}}</button>
                        </a>
                    </div>
                @endif
                <div class="box">
                    <a href="#" onclick="event.preventDefault(); logout();" >
                        <button id="lb_btn_logout" class="lnb-topbar-button lb-btn-orange lb-btn-logout" type="button"><i class="fa fa-power-off"></i> {{__('Déconnexion')}}</button>
                    </a>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('maincontent')
    <div>
        <section id="welcome_report">
            <h2 id='welcome' class="lb-h2" var1="{{Auth::user()->first_name}}" >
                {{__('Bienvenue')}} {{Auth::user()->first_name}}, {{__('choisissez le rapport sur lequel vous souhaitez travailler')}}
            </h2>
        </section>
        <div class="report-choice-layout">
            <!-- Reports menu - Left -->
            <div id="left_menu" class="section-parent lb-report-choice-left">
                <section class="lb-section lb-section-blue center">
                    <div class="mission-search">
                        <i class="fa fa-search"></i>
                        <input class="search-input" type="text" placeholder="{{__('Rechercher un rapport')}}" title="{{__("Rechercher un rapport par son nom")}}" />
                    </div>
                    <?php
                    $missionsTypes = [
                        'new_missions' => [
                            'extend' => true,
                            'child_dom_id' => "nouvelles",
                            'title' => __("Nouveaux rapports"),
                            'records' => $missionsRecords->new,
                        ],
                        'current_missions' => [
                            'extend' => true,
                            'child_dom_id' => "encours",
                            'title' => __("Rapports en cours"),
                            'records' => $missionsRecords->on,
                        ],
                        'delivered_missions' => [
                            'extend' => false,
                            'child_dom_id' => 'enattente',
                            'title' => __('Rapports rendus'),
                            'records' => $missionsRecords->wait
                        ],
                        'archived_missions' => [
                            'extend' => false,
                            'child_dom_id' => 'archives',
                            'title' => __('Rapports archivés'),
                            'records' => $missionsRecords->arc
                        ],
                        'tutorial_missions' => [
                            'extend' => true,
                            'child_dom_id' => 'tutorial',
                            'title' => __('Rapport découverte'),
                            'records' => $missionsRecords->tuto
                        ],
                    ];
                    foreach($missionsTypes as $id => $mt) {
                    ?>
                    @include('report/_menuMission',
                        [
                            'records' => $mt['records'],
                            'id_user' => $user->id_user,
                            'dom_id' => $id,
                            'child_dom_id' => $mt['child_dom_id'],
                            'extend' => $mt['extend'],
                            'title' => $mt['title'],
                        ])
                    <?php
                    }
                    ?>
                </section>
            </div>
            
            <!-- Reports description - Center -->
            <div id="report_content" data-responsive="large" >
                <div id="cadre_mission">
                    @php 
                        $current_selected_mission = \App\Views\ReportIndex::getSelectedMission($user, Auth::user()->current_report);
                    @endphp
                    @if($current_selected_mission)
                        @include('report._mission', ['r' => $current_selected_mission])
                    @else
                        <section class="lb-section lb-section-orange center report_header">
                            <div class="mission_description">
                                {{__('Aucun rapport sélectionnée')}}
                            </div>
                        </section>
                    @endif
                </div>
                @include('report/_report_index_widgets', ['user' => $user])
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <div class="row around-xs between-sm">
        <div>
            <div id="lb_confident"
                 onclick="$('#lb_confident_frame').toggle()">{{__('Informations sur vos données & CGU')}}</div>
            <div id="lb_confident_frame">
                <i class="fa fa-times" title="{{__('Fermer')}}" onclick="$('#lb_confident_frame').hide()"></i>
                <ul>
                    <li>{{__("Dans un rapport, tous les labdocs ainsi que les contenus de l'outil « Ressources » sont partagés et peuvent être modifiés par les étudiants de l'équipe.")}}</li>
                    <li>{{__("Les labdocs sont visibles par votre enseignant tuteur sauf les labdocs « cachés ».")}}</li>
                    <li>{{__('Les échanges entre étudiants (Messages et Commentaires) sont privés et ne sont pas accessibles aux enseignants.')}}</li>
                    <li>{{__("Toutes les productions des utilisateurs (labdocs, échanges entre partenaires...) peuvent être consultées par les créateurs de LabNBook et utilisées exclusivement à des fins de recherche et d'amélioration du système.")}}</li>
                </ul>
                @if (config('labnbook.cgu_url'))
                    <div id="read-cgu">
                        <a href="{{config('labnbook.cgu_url')}}" target="_blank">{{__("Accéder aux Conditions Générales d'Utilisation de LabNBook")}}</a>
                    </div>
                @endif
                <p>&nbsp;</p>
                <p><strong>{{__('Données personnelles')}}</strong>{!!__("&nbsp;:")!!} {{__("vous disposez d’un droit d’accès, de modification, de rectification et de suppression de vos données personnelles utilisées lors de l'inscription (loi « Informatique et Libertés » du 6 janvier 1978). Pour toute demande, adressez-vous à")}}{!!__("&nbsp;:")!!}
                    <a href={{$helper->getMailto('RGPD')}}>webmaster</a>.</p>
            </div>
        </div>
        <div id="div_info">
            <p>{{__("En cas de bug")}}{!!__("&nbsp;:")!!}
                <a class="lb-a" href="{{ $helper->getMailto('bug') }}">{{__("envoyer un mail aux développeurs de LabNBook")}}</a>
            </p>
        </div>
    </div>
@endsection
