<?php
    /** @var \App\Helper $helper */
    /** @var object $r */

    // affichage des infos sur la mission
    if (isset($r->report)) {
        $htmlId = "report-{$r->report->id_report}";
    } elseif (isset($r->team_config)) {
        $htmlId = "teamconfig-{$r->team_config->id_team_config}";
    } elseif (isset($r->mission)) {
        $htmlId = "mission-{$r->mission->id_mission}";
    } else {
        Log::error("no report nor teamconfig for " . var_export($r, true));
        return;
    }

    $isActive = isset($r->report) && ($r->report->id_report == Auth::user()->current_report);

    // teachers && dates
    $teacherList = [];
    if (!empty($r->report)) {
        if(isset($r->report->teamConfig)) {
            $teacherList = $r->report->teamConfig->getTeachersList();
        }
        $start_date = $r->report->start_datetime;
        $end_date = $r->report->end_datetime;
    }
    elseif (!empty($r->team_config)) {
        $teacherList = $r->team_config->getTeachersList();
        $start_date = $r->team_config->start_datetime;
        $end_date = $r->team_config->end_datetime;
    } else {
        $start_date = null;
        $end_date = null;
    }
    $nb_teachers = count($teacherList);
?>


<div id="{{ $htmlId }}" class="row block-radio {{ ($isActive ? " active" : "") }}">
    @if (!empty($r->users) || !empty($r->report->team_name) || $nb_teachers > 0 || !is_null($end_date))
        <section class="lb-section lb-section-orange center report_header">

            <!-- Équipe -->
            @if (!empty($r->users) || !empty($r->report->team_name))
                <section class="report_header_info" id="report_header_info_{{$r->report->id_report.'_'.$r->report->id_mission}}">
                    <span class="report_header_info_title">
                        @if (empty($r->report->team_name) || !str_contains(strtolower($r->report->team_name), __("equipe")))
                            {{ __("Equipe") }}
                        @endif
                        @if (!empty($r->report->team_name))
                            {!! htmlspecialchars($r->report->team_name) !!}
                        @endif
                        @if (!empty($r->users))
                            {!! __("&nbsp;: ")!!}
                        @endif
                    </span>
                    @foreach($r->users as $u)
                        <span @class([ "team-member", "connected_user" => ($u->isConnected($r->report) && $u->id_user !== Auth::id()) ])>{!! htmlspecialchars($u->first_name.' '.$u->user_name) !!}</span>
                        @if (!$loop->last) - @endif
                    @endforeach
                </section>
            @endif

            <!-- Tuteurs -->
            @if ($nb_teachers > 0)
                <section class="report_header_info" id="report_header_info_teachers">
                    <span class="report_header_info_title">{!! __("Tuteur") . ($nb_teachers > 1 ? "s" : "") !!}{!! __("&nbsp;: ") !!}</span>
                    @foreach ($teacherList as $c_teacher)
                        <span class="team-member">{{$c_teacher}}</span>
                        @if (!$loop->last) - @endif
                    @endforeach
                </section>
            @endif

            <!-- Due date -->
            @if (!is_null($end_date))
                <section class="report_header_info">
                    <span class="report_header_info_title">{!! __('Date de rendu') . __("&nbsp;: ") !!}</span>
                    <span>{{ $helper->datetimeToFr($end_date) }}</span>
                </section>
            @endif
        </section>
    @endif

    <section class="lb-section lb-section-orange center">
        <h2 class="mission_code lb-h2">{{ $r->mission->code }}
            <span class="mission_name lb-h3">{{ $r->mission->name }}</span>
        </h2>
        <div class="mission_description tinyMCE-text">{!! $r->mission->description !!}</div>
        <div class="mission-choice-btn">
            @php
                $date = date_create(DB::SELECT('SELECT NOW() as now')[0]->now); // la date SQL est juste. Pas la fonction php time() !
                $current_time = date_timestamp_get($date);
            @endphp

            @if (!empty($r->team_config))
                @if ((is_null($start_date) || $current_time > strtotime($start_date)) && (is_null($end_date) || $current_time < strtotime($end_date)))
                    @if ($r->team_config->method == \App\TeamConfig::METHOD_STUDENTCHOICE)
                        <a class="lb-a" href="/teamconfig/{{$r->team_config->id_team_config}}/select">
                            <button type="button" class="lb-mission-btn lb-btn-orange">{{__("S'inscrire dans une équipe")}}</button>
                        </a>
                    @elseif ($r->team_config->method === \App\TeamConfig::NO_METHOD)
                        @php
                            $click_message = __(
                                "Votre enseignant a attribué la mission :code à votre classe. Cependant il ne vous a pas mis dans une équipe et vous n\\'avez pas de rapport. Contactez votre enseignant pour qu\\'il vous attribue un rapport",
                                ['code' => $r->mission->code]
                            );
                        @endphp
                        <button type="button"
                                class="lb-mission-btn lb-btn-orange"
                                x-on:click="alertGently('{{$click_message}}', 'danger')"
                                >
                            {{__("Commencer")}}
                        </button>
                    @elseif ($r->team_config->method === \App\TeamConfig::METHOD_TEACHERCHOICE)
                        {{-- unreachable: there is no link to display this mission without report --}}
                        <p>{{__("Vous n'avez pas encore d'équipe. Seul un enseignant peut vous inscrire.")}}</p>
                    @else
                        @php logger("Mission {$r->mission->id_mission} should have a report matching team_config {$r->team_config->id_team_config}") @endphp
                    @endif
                @endif

            @elseif (!empty($r->report))
                @php
                $text_button = "";
                if ($r->report->status === 'new'
                    && (is_null($start_date) || $current_time > strtotime($start_date))
                    && (is_null($end_date) || $current_time < strtotime($end_date))) {
                    $text_button = __("Commencer");
                } elseif ((in_array($r->report->status, ['on', 'tuto']))  && (is_null($end_date) || $current_time < strtotime($end_date))) {
                    $text_button = __("Continuer");
                } elseif ($r->report->status === 'wait' || $r->report->status === 'arc') {
                    $text_button = __("Consulter");
                }
                @endphp
                @if ($text_button)
                    {{-- les paramètres post et les boutons --}}
                    <form class="form_choice_mission" status="{{$r->report->status}}" method="get" action="/report/{{$r->report->id_report}}"  onsubmit="disableAndShowSpinner($(this).find('button'))">
                        <button type="submit" id="btn_form_submit" class="lb-mission-btn lb-btn-orange">
                            <i class="fa fa-spinner fa-pulse" style="display:none"></i>
                            {{$text_button}}
                        </button>
                    </form>
                @endif

            @else
                {{-- Mission tutoriel --}}
                <a class="lb-a" href="/reports/enterTutorial/{{$r->mission->id_mission}}">
                    <button type="button" class="lb-mission-btn lb-btn-orange">{{__("Commencer la mission découverte")}}</button>
                </a>
            @endif

        </div>
    </section>
</div>
