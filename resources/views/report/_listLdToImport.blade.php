<?php
/** 
 * @var Collection|Labdocs[] $lds 
 * @var int $id_rp 
 */
use App\Labdocs;
use Illuminate\Support\Collection;

?>
<div id="ld_import_menu_item_rp_{{$id_rp}}" class="ld_import_menu_item_name" 
     x-data="{lds_to_import:{{$lds}},chosen_mission:null,msg_no_lds_to_import:getMsgNoLdsToImport(is_follow, m_code)}"
     x-init="is_show_import=true; wait_for_data=false; console.log(lds_to_import)">
    <template x-if="lds_to_import.length===0">
        <p class="no_lds_msg" x-text="msg_no_lds_to_import"></p>
    </template>
    <template x-if="lds_to_import.length>0">
        <div>
            <div class="header">
                {{__('Importer un labdoc')}}
                <div class="select_mission">
                    <span x-text="'{{__('Depuis le rapport :')}}'"></span>
                    <select x-model="chosen_mission">
                        <template x-for="mission_code in lds_to_import.map(item => item.code).filter((value, index, self) => self.indexOf(value) === index).sort()">
                            <option :value="mission_code" x-text="mission_code"></option>
                        </template>
                    </select>
                </div>
            </div>
            <div class="content" x-init="chosen_mission=lds_to_import[0].code">
                <template x-if="lds_to_import[0].origin_type">
                    <div class="select_ld">
                        <template x-if="lds_to_import.some(i => i.origin_type === 'mission' && i.code === chosen_mission)">
                            <div class="sub_select_ld">
                                <div class="origin_title">{{__('Mission')}}</div>
                                <template x-for="ld in lds_to_import.filter(i => i.origin_type === 'mission' && i.code === chosen_mission)">
                                    <div class="ld_to_import" x-on:click="importLD(ld.id_labdoc,{{$id_rp}});is_show_import = false;" :title="'{{__('Importer ce labdoc dans la partie')}}'">
                                        <img :src="`/images/report/ld_type_${ld.type_labdoc}.svg`" width="16">
                                        <span x-text="ld.name"></span>
                                    </div>
                                </template>
                            </div>
                        </template>
                        <template x-if="lds_to_import.some(i => i.origin_type === 'solution' && i.code === chosen_mission)">
                            <div class="sub_select_ld">
                                <div class="origin_title">{{__('Rapport enseignant')}}</div>
                                <template x-for="ld in lds_to_import.filter(i => i.origin_type === 'solution' && i.code === chosen_mission)">
                                    <div class="ld_to_import" x-on:click="importLD(ld.id_labdoc,{{$id_rp}});is_show_import = false;" :title="'{{__('Importer ce labdoc dans la partie')}}'">
                                        <img :src="`/images/report/ld_type_${ld.type_labdoc}.svg`" width="16">
                                        <span x-text="ld.name"></span>
                                    </div>
                                </template>
                            </div>
                        </template>
                        <template x-if="lds_to_import.some(i => i.origin_type === 'student' && i.code === chosen_mission)">
                            <div class="sub_select_ld">
                                <div class="origin_title">{{__('Rapports')}}</div>
                                <template x-for="ld in lds_to_import.filter(i => i.origin_type === 'student' && i.code === chosen_mission)">
                                    <div class="ld_to_import" x-on:click="importLD(ld.id_labdoc,{{$id_rp}});is_show_import = false;" :title="'{{__('Importer ce labdoc dans la partie')}}'">
                                        <img :src="`/images/report/ld_type_${ld.type_labdoc}.svg`" width="16">
                                        <span x-text="ld.name"></span>
                                    </div>
                                </template>
                            </div>
                        </template>
                    </div>
                </template>
                <template x-if="!lds_to_import[0].origin_type">
                    <div class="select_ld">
                        <template x-for="ld in lds_to_import.filter(i => i.code === chosen_mission)">
                            <div class="ld_to_import" x-on:click="importLD(ld.id_labdoc,{{$id_rp}});is_show_import = false;" :title="'{{__('Importer ce labdoc dans la partie')}}'">
                                <img :src="`/images/report/ld_type_${ld.type_labdoc}.svg`" width="16">
                                <span x-text="ld.name"></span>
                            </div>
                        </template>
                    </div>
                </template>
            </div>
        </div>
    </template>
</div>
