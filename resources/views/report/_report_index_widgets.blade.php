<?php
    /** @var \App\User $user */
$widgets = [
    'task' => true,
    'calendar' => true
];
$allowed_widget_type = json_encode(array_keys(array_filter($widgets)));
?>
<div id="widgets_column" class="widget-column report-choice"
     x-data="{ rvNS: reportViewCtrl(null, 'default', {}) }" 
     x-init="rvNS.init('{{$allowed_widget_type}}'); $watch('rvNS.reduced', () => rvNS.reducedExpandColumn()); 
        $watch('rvNS.fixed_to_bottom', () => rvNS.moveColumnPosition()); $nextTick(()=>{rvNS.adjustWidgetColumnSize()});"
    :class="{'reduced':rvNS.reduced, 'expanded': !rvNS.reduced, 'not-fixed':!rvNS.responsive_small_size, 'small-screen':rvNS.responsive_small_size}">
    <div :class="{'fixed-bottom':!rvNS.reduced && rvNS.fixed_to_bottom, 'fixed-right': !rvNS.reduced && !rvNS.fixed_to_bottom}">
        <template x-if="!rvNS.reduced && !rvNS.responsive_small_size">
            <div class="widget-column-handle handle-resize" :class="{'left-right':!rvNS.fixed_to_bottom, 'top-down': rvNS.fixed_to_bottom}">
                <button class="widget-extend-btn">
                </button>
            </div>
        </template>
        <div class="widget-part">
            <div class="widgets-buttons" :class="{'lb-section-green':rvNS.reduced}">

                <div class="widgets-buttons-left-part">
                    @if($widgets['task'])
                        <div id="lb_menubar_task" class="lb-menubar-btn" data-widget-type="task">
                            <button class="lnb-widget-menu-btn" type="button" :class="{'active':rvNS.active_widget_type===WIDGET.TASK}"
                                    x-on:click="rvNS.toggleWidgetItem(WIDGET.TASK)" title="{{__("Ouvrir ma liste des tâches personnelle")}}">
                                <i class="fa fa-list-check"></i>
                            </button>
                        </div>
                    @endif

                    @if($widgets['calendar'])
                        <div id="lb_menubar_calendar" class="lb-menubar-btn" data-widget-type="task">
                            <button class="lnb-widget-menu-btn" type="button" :class="{'active':rvNS.active_widget_type===WIDGET.CALENDAR}"
                                    x-on:click="rvNS.toggleWidgetItem(WIDGET.CALENDAR)" title="{{__("Ouvrir mon calendrier")}}">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </div>
                    @endif
                </div>
                
                <div class="widgets-buttons-right-part">
                    <template x-if="!rvNS.reduced">
                        <div>
                            <button class="lnb-widget-menu-btn widget-action-btn" type="button" x-on:click="rvNS.reduced = true" title="{{__("Réduire les widgets")}}">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </template>
                </div>
                
            </div>
            <div class="widget-content" x-show="!rvNS.reduced">
                @if($widgets['task'])
                    <x-task-widget x-show="rvNS.active_widget_type === WIDGET.TASK" :report="null"></x-task-widget>
                @endif
                @if($widgets['calendar'])
                    <x-calendar-widget x-show="rvNS.active_widget_type === WIDGET.CALENDAR"></x-calendar-widget>
                @endif
            </div>
        </div>
    </div>
</div>
