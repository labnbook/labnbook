<?php
    /**
     * @var \App\Helper $helper
     */
    /**
     * @var array $teacherMissions
     */
    /**
     * @var array $teacherClasses
     */
    /**
     * @var array $classes
     */
    /**
     * @var array $reports
     */
      $htmlClasses = "minitable";
?>
@include('_subTable', ['data' => $teacherMissions, 'classes' => $htmlClasses, 'title' => __("Gère les missions"), 'id'=> 'tm-table'])
@include('_subTable', ['data' => $teacherClasses, 'classes' => $htmlClasses, 'title' => __("Gère les classes"), 'id'=> 'tc-table'])
@include('_subTable', ['data' => $classes, 'classes' => $htmlClasses, 'title' => __('Appartient aux classes'), 'id'=> 'uc-table'])
@include('_subTable', ['data' => $reports, 'classes' => $htmlClasses, 'title' => __('A commme rapports'), 'id'=> 'ur-table'])
