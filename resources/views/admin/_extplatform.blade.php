<?php
    /** @var \App\Exterplatform $extplarform **/
    /** @var string $api_url **/
?>

<h3>{{__("Usage de la plateforme")}}</h3>
<p>
{{__( ":countu utilisateurs dont :countt enseignants",
    [
        "countu" => $extplatform->users()->count(),
        "countt" => $extplatform->teachers()->count(),
    ])}}
</p>
<h3>{{__("Informations de configuration du plugin")}}</h3>
<table id="table-extplatform-{{$extplatform->id_extplatform}}">
    <tr><td>
        {{__("URL d'API")}}
    </td><td>
        {{$api_url}}
    </td><tr><td>
        {{__("Clé jwt")}}
    </td><td>
        {{$extplatform->secret}}
    </td></tr><tr><td>
        {{__("Id plateforme externe")}}
    </td><td>
        {{$extplatform->id_extplatform}}
    </td></tr>
</table>
