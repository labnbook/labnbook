<?php

    /** @var array $messages **/
?>

@extends('layouts.labnbook')
@section('title', __('Administration'))


@section('head')

    <?= $helper->loadAsset("/libraries/datatables/datatables.min.css") ?>
    <?= $helper->loadAsset("/libraries/datatables/datatables-all.js") ?>

    <!-- Styles et fonctions communes aux pages Enseignants -->
    <?= $helper->loadAsset("/css/teacher/common.css") ?>
    <?= $helper->loadAsset("/js/teacher/authoring.js") ?>

    <!-- Styles et fonctions de la page -->
    <?= $helper->loadAsset("/css/admin/admin.css") ?>
    <?= $helper->loadAsset("/js/admin/admin.js") ?>
    <?= $helper->loadAsset("/js/manager/addTeacher.js", 'defer') ?>

@endsection

@section('body')
    @include('teacher/_menu', [
        'page_name' => request()->path(),
        'refresh_period' => config('labnbook.refresh_period'),
        ])

        <div id="wrapper-content">
            <section>
                <h2>{{__("État de la plateforme")}}</h2>
                <div class="platform-info">
                    <div>
                        <ul id="adminstatus"></ul>
                    </div>
                    <h4 onclick='toggleDebugInfos()' class="btn_pointer" title="{{__("Afficher l'état de la plateforme")}}">
                        <i id='toggle-debug' class="fa fa-caret-right"></i>
                        {{__("Détails techniques")}}
                    </h4>
                    <div id="debug" style="display:none">
                        <p>{{$tz }}</p>
                        <code> {!!$syst!!} </code>
                        <p> {{__("Version")}} : {{$vers}}</p>
                    </div>
                    <button type="button" class="lb_btn" id="send-test-email" onclick="sendTestMail()">{{ __("Tester l'envoi d'email ") }}</button>
                </div>
            </section>

            <!-- Teacher Management -->
            <section class="width-medium" id="institutions" >
                <h2>{{__("Gestion des utilisateurs")}}</h2>
                <div class="table-caption">
                    <strong>{{ __('Institutions') }}</strong>
                    <span class="tb-search">
                        <input class="search-input" type="text" placeholder="{{ __('Recherche') }}" />
                    </span>
                    <span class="options actions" id="le-actions">
                        <span class="lba-context-icons actions-inactive">
                            <a href="#" onclick="openEditInstDialog()" title="{{ __("Modifier l'institution") }}" class="single-action"><i id="i-edit" class="fas fa-pencil-alt"></i></a>
                            <a href="#" id='i-delete' onclick="deleteInstitution()" title="{{ __("Supprimer l'institution") }}" class="single-action"><i class="fa fa-trash-alt"></i></a>
                        </span>
                        <a href="#" onclick="openAddInstDialog()" title="{{ __('Ajouter une institution') }}"><i id="add-inst-btn" class="fa fa-plus"></i></a>
                    </span>
                </div>
                <table id="i-table" class="table-base">
                    <thead>
                        <tr>
                            <th title="{{ __('Trier par nom') }}">{{ __('Nom') }}</th>
                            <th title="{{ __('Trier par classe') }}">{{ __('Classe enseignants') }}</th>
                            <th title="{{ __('Trier par CAS') }}">{{ __('Connexion CAS') }}</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>{{ __('Nom') }}</th>
                            <th>{{ __('Classe enseignants') }}</th>
                            <th>{{ __('Connexion CAS') }}</th>
                        </tr>
                    </tfoot>
                </table>
                <div class="clear"></div>


                <div class="popup" id="modal-inst-add" title="{{__('Ajouter une institution')}}" data-reset-input="true">
                    <header>
                        <span class="title" >{{ __("Nouvelle institution") }}</span>
                        <span class="actions" >
                            <a href="#" title="{{ __("Valider") }}" class="popup-submit"><i class="fa fa-check"></i></a>
                            <a href="#" title="{{ __("Annuler") }}" class="popup-cancel"><i class="fa fa-times"></i></a>
                        </span>
                    </header>
                    <section>
                        <form>
                            <label for="in-add-name">{{ __("Nom de l'institution") }} * :</label>
                            <input required pattern=".{3,127}" type="text" id="in-add-name" name="in-add-name" maxlength="127" placeholder="{{ "(3-127 ".__("caractères").")" }}" /><br/>
                            <label for="in-add-cas">{{ __("Activer le CAS") }} * :</label>
                            <input type="checkbox" id="in-add-cas" name="in-add-cas"/><br/>
                        </form>
                    </section>
                </div>

                <div class="popup" id="modal-inst-edit" title="{{__("Modifier l'institution")}}" data-reset-input="true">
                    <header>
                        <span class="title" >{{ __("Modifier l'institution") }}<span id='edit-inst-name'></span></span>
                        <span class="actions" >
                            <a href="#" title="{{ __("Valider") }}" class="popup-submit"><i class="fa fa-check"></i></a>
                            <a href="#" title="{{ __("Annuler") }}" class="popup-cancel"><i class="fa fa-times"></i></a>
                        </span>
                    </header>
                    <section>
                        <form>
                            <label for="in-edit-name">{{ __("Nom de l'institution") }} :</label>
                            <input required pattern=".{3,127}" type="text" id="in-edit-name" name="in-edit-name" maxlength="127" placeholder="{{ "(3-127 ".__("caractères").")" }}" /><br/>
                            <label for="in-edit-cas">{{ __("Activer le CAS") }} :</label>
                            <input type="checkbox" id="in-edit-cas" name="in-edit-cas"/><br/>
                            <label for="in-edit-class">{{ __("Classe enseignant") }} :</label>
                            <select id="in-edit-class" name="in-edit-class"></select>
                        </form>
                    </section>
                </div>
            </section>

            <section class="width-medium" id="users"  style="display:none">
                <div class="table-caption">
                    <strong>{{ __('Utilisateurs') }}</strong>
                    <div class="dataTables_length">
                        <select class="pagination-size">
                            <option value="20">20</option>
                            <option value="50">50</option>
                            <option value="-1">{{ __('Tous') }}</option>
                        </select>
                        / <span id="num-visible-users"></span>
                    </div>
                    <span class="tb-search">
                        <input class="search-input" type="text" placeholder="{{ __('Recherche') }}" />
                    </span>
                    <span class="options actions">
                        <span class="lba-context-icons">
                            <span class="lba-context-icons actions-inactive">
                                <a href="#" onclick="openEditUserDialog()" title="{{ __("Modifier l'utilisateur") }}" class="single-action"><i id="u-edit" class="fas fa-pencil-alt"></i></a>
                                <a href="#" onclick="impersonateUser()" title="{{ __("Prendre l'identité de l'utilisateur") }}" class="single-action"><i id="u-impersonate" class="fa fa-user-secret"></i></a>
                                <a href="#" onclick="getUserDump()" title="{{ __("Télécharger l'extrait RGPD") }}" class="single-action"><i id="u-rgpd" class="fa fa-download"></i></a>
                                <a href="#" id="u-anonymize" onclick="anonymizeUser()" title="{{ __("Anonymiser l'utilisateur") }}" class="single-action"><i id="u-erase" class="fa fa-eraser"></i></a>
                                <a href="#" onclick="deleteUser()" title="{{ __("Supprimer l'utilisateur") }}" class="single-action"><i id="u-delete" class="fa fa-trash-alt"></i></a>
                            </span>
                    </span>
                    <a href="#" id="u-add" onclick="openAddTeacher()" title="{{ __('Inscrire un enseignant') }}"><i id="add-user-btn" class="fa fa-plus"></i></a>
                    </span>
                </div>
                <table id="u-table" class="table-base">
                    <thead>
                        <tr>
                            <th title="{{ __('Trier par nom') }}">{{ __('Nom') }}</th>
                            <th title="{{ __('Trier par prénom') }}">{{ __('Prénom') }}</th>
                            <th title="{{ __('Trier par email') }}">{{ __('Email') }}</th>
                            <th title="{{ __('Trier par rôle') }}">{{ __('Rôle') }}</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>{{ __('Nom') }}</th>
                            <th>{{ __('Prénom') }}</th>
                            <th>{{ __('Prénom') }}</th>
                            <th>{{ __('Rôle') }}</th>
                        </tr>
                    </tfoot>
                </table>
                <div class="clear"></div>
            </section>

            <!-- Popups -->
            <div class="popup" id="t-edit-popup">
                <header>
                    <span class="title" id="t-edit-popup-title">{{ __('Modifier un utilisateur') }}</span>
                    <span class="actions">
                        <i class="fa fa-check popup-submit" title="Valider" id="t-edit-popup-valid"></i>
                        <i class="fa fa-times popup-cancel" title="Annuler" id="t-edit-popup-cancel"></i>
                    </span>
                </header>
                <section>
                    <form autocomplete="off">
                        <fieldset class="t-fieldset">
                            <legend>{{ __('Informations personnelles') }}</legend>
                            <label for="t-edit-first-name" title="obligatoire" >{{ __('Prénom') }} * :</label> <input type="text" id="t-edit-first-name" name="t-edit-first-name" /><br/>
                            <label for="t-edit-name" title="obligatoire" >{{ __('Nom') }} * :</label> <input type="text" id="t-edit-name" name="t-edit-name"/><br/>
                            <label for="t-edit-email" title="{{ __('E-mail ou mot de passe nécessaire') }}">E-mail (*) : </label> <input type="email" id="t-edit-email" name="t-edit-email" title="{{ __('E-mail ou mot de passe nécessaire') }}"/><br/>
                        </fieldset>
                        <fieldset class="t-fieldset" id="user-role">
                            <legend>{{ __("Rôle")}}</legend>
                            <select id="t-edit-short-role" onchange="showHideSuperPowerSection($(this).val())">
                                <option value="learner">{{__("Étudiant")}}</option>
                                <option value="teacher">{{__("Enseignant")}}</option>
                            </select>
                            <div id="superpower">
                                <input type="radio" id="learner" name="t-edit-role"  data-display-name='{{__("étudiant")}}' style="display:none">
                                <input type="radio" id="teacher" name="t-edit-role" data-display-name='{{__("enseignant")}}'>
                                <label for ="teacher">{{__("Enseignant : crée des missions & gère des étudiants")}}</label><br/>
                                <input type="radio" id="manager"  data-display-name='{{__("gestionnaire")}}' name="t-edit-role">
                                <label for ="manager">{{__("Gestionnaire : gère des enseignants & des équipes pédagogiques")}}</label><br/>
                                <input type="radio" id="admin" data-display-name='{{__("administrateur")}}' name="t-edit-role">
                                <label for ="admin">{{__("Administrateur : toutes permissions")}}</label><br/>
                            </div>
                            <!--<option value="admin"   title="{{__("possède toutes les permissions")}}">{{__("Admin")}}</option> -->
                        </fieldset>
                        <fieldset class="t-fieldset">
                            <legend>{{ __('Informations de connexion') }}</legend>
                            <label for="t-edit-login">{{ __('Intitulé du compte (login)') }} * :</label> <input type="text" id="t-edit-login" name="t-edit-login" /><br/>
                            <label for="t-edit-account-type">{{ __('Type de compte') }} :</label> <input type="text" disabled="true" id="t-edit-account-type" /><br/>
                        </fieldset>
                    </form>
                </section>
            </div>

            @include('manager/teacherTeam/_addTeacherPopups')

            <!-- Extplaform Management -->
            <br />
            <section class="width-full" id="extplatforms" >
                <h2>{{__("Gestion des plateformes externes (Plugins Moodle)")}}</h2>
                <div class="table-caption">
                    <strong>{{ __('Plateformes') }}</strong>
                    <span class="tb-search">
                        <input class="search-input" type="text" placeholder="{{ __('Recherche') }}" />
                    </span>
                    <span class="options actions" id="extp-actions">
                        <span class="lba-context-icons actions-inactive">
                            <a href="#" onclick="openEditExtpDialog()" title="{{ __("Modifier la plateforme") }}" class="single-action"><i id="e-edit" class="fas fa-pencil-alt"></i></a>
                            <a href="#" id="e-delete" onclick="deleteExtp()" title="{{ __("Supprimer la plateforme") }}" class="single-action"><i class="fa fa-trash-alt"></i></a>
                        </span>
                        <a href="#" id="e-add" onclick="openAddExtpDialog()" title="{{ __('Ajouter une plateforme') }}"><i id="add-inst-btn" class="fa fa-plus"></i></a>
                    </span>
                </div>
                <table id="e-table" class="table-base">
                    <thead>
                        <tr>
                            <th title="{{ __('Trier par institution') }}">{{ __('Institution') }}</th>
                            <th title="{{ __('Trier par nom') }}">{{ __('Nom') }}</th>
                            <th title="{{ __('Trier par url') }}">{{ __('URL') }}</th>
                            <th title="{{ __('Trier par version plateforme') }}">{{ __('Version plateforme') }}</th>
                            <th title="{{ __('Trier par version plugin') }}">{{ __('Version plugin') }}</th>
                            <th title="{{ __('Trier par contacts') }}">{{ __('Contacts') }}</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>{{ __('Institution') }}</th>
                            <th>{{ __('Nom') }}</th>
                            <th>{{ __('URL') }}</th>
                            <th>{{ __('Version plateforme') }}</th>
                            <th>{{ __('Version plugin') }}</th>
                            <th>{{ __('Contacts') }}</th>
                        </tr>
                    </tfoot>
                </table>
                <div class="clear"></div>


                <div class="popup" id="modal-extp-add" title="{{__('Ajouter une plateforme externe')}}" data-reset-input="true">
                    <header>
                        <span class="title" >{{ __("Nouvelle platforme externe") }}</span>
                        <span class="actions" >
                            <a href="#" title="{{ __("Valider") }}" class="popup-submit"><i class="fa fa-check"></i></a>
                            <a href="#" title="{{ __("Annuler") }}" class="popup-cancel"><i class="fa fa-times"></i></a>
                        </span>
                    </header>
                    <section>
                        <form>
                            <label for="extp-add-name">{{ __("Nom de la plateforme") }} * :</label>
                            <input required pattern=".{3,127}" type="text" id="extp-add-name" name="extp-add-name" maxlength="127" placeholder="{{ "(3-127 ".__("caractères").")" }}" /><br/>
                            <label for="extp-add-contacts">{{ __("Contacts de la plateforme") }} :</label>
                            <input required pattern="" type="text" id="extp-add-contacts" name="extp-add-contacts" /><br/>
                            <label for="extp-add-url">{{ __("Url de la plateforme") }} :</label>
                            <input required pattern=".{3,254}" type="text" id="extp-add-url" name="extp-add-url" maxlength="254" placeholder="{{ "(3-254 ".__("caractères").")" }}" /><br/>
                            <label for="extp-add-inst">{{ __("Institution pour les utilisateurs de la plateforme") }} :</label>
                            <select id="extp-add-inst" name="extp-add-inst" ></select>
                        </form>
                    </section>
                </div>

                <div class="popup" id="modal-extp-edit" title="{{__("Modifier la plateforme externe")}}" data-reset-input="true">
                    <header>
                        <span class="title" >{{ __("Modifier la plateforme externe") }}<span id='edit-extp-name'></span></span>
                        <span class="actions" >
                            <a href="#" title="{{ __("Valider") }}" class="popup-submit"><i class="fa fa-check"></i></a>
                            <a href="#" title="{{ __("Annuler") }}" class="popup-cancel"><i class="fa fa-times"></i></a>
                        </span>
                    </header>
                    <section>
                        <form>
                            <label for="extp-edit-name">{{ __("Nom de la plateforme") }} :</label>
                            <input required pattern=".{3,127}" type="text" id="extp-edit-name" name="extp-edit-name" maxlength="127" placeholder="{{ "(3-127 ".__("caractères").")" }}" /><br/>
                            <label for="extp-edit-url">{{ __("Url de la plateforme") }} :</label>
                            <input required pattern=".{3,254}" type="text" id="extp-edit-url" name="extp-edit-url" maxlength="254" placeholder="{{ "(3-254 ".__("caractères").")" }}" /><br/>
                            <label for="extp-edit-contacts">{{ __("Contacts de la plateforme") }} :</label>
                            <input required pattern="" type="text" id="extp-edit-contacts" name="extp-edit-contacts" /><br/>
                            <label for="extp-edit-secret">{{ __("Clé JWT") }} :</label>
                            <input required pattern=".{3,254}" type="text" id="extp-edit-secret" name="extp-edit-secret" maxlength="254" placeholder="{{ "(3-254 ".__("caractères").")" }}" /><br/>
                            <label for="extp-edit-inst">{{ __("Institution pour les utilisateurs de la plateforme") }} :</label>
                            <select id="extp-edit-inst" name="extp-edit-inst" ></select>
                        </form>
                    </section>
                </div>
            </section>




            @include('components.widgets.message.index', ['mv' => new App\Views\Widgets\Message($user->id_user, 0)])
        @endsection
