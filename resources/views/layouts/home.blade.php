<?php
    /** @var \App\Helper $helper */
?>
@extends('layouts.labnbook')

<!-- ----- HTML <head> content ----- -->
@section('head')
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?= $helper->loadAsset("/js/report_layout.js") ?>
    <?= $helper->loadAsset("/css/report_layout.css") ?>
    <?= $helper->loadAsset("/css/widgets_layout.css") ?>
    <?= $helper->loadAsset("/css/home.css") ?>
    <?= $helper->loadAsset("/css/flexboxgrid.min.css") ?>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    @stack('header')
@endsection

<!-- ----- HTML <body> content ----- -->
@section('body')

    <div id="lb_modal_cover"></div>

    <div id="header"> @yield('topbar') </div>

    @include('_flash-alerts')

    <div id="maincontent"> @yield('maincontent') </div>

    <div id="footer"> @yield('footer') </div>

@endsection
