<!DOCTYPE html>
<html>
    <head>
        @include('layouts._head')
        <title> {{ config('app.name') }} - @yield('title')</title>
        @yield('head')
        @stack('component_css')
    </head>
    <body @yield('bodyattributes') >
        @include('_logout-form')
        @include('_confirmationPopup')
        @include('_tooltip-popup')
        @includeWhen((auth()->check() && auth()->user()->mustAcceptCGU()),'_accept_CGU')
        @yield('body')
    </body>
</html>
