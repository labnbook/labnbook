<?php
    /** @var \App\Helper $helper */
?>
@extends('layouts.labnbook')

@section('head')
    <?= $helper->loadAsset("/css/index.css") ?>
    @yield('header')
@endsection

@section('body')
    <div id="lb_modal_cover"></div>
    <div id="lnb-logo">
        <a href="https://labnbook.fr" target="_blank"><img src="/images/LNB-logo.svg" alt="LabNBook"/></a>
        <a href="https://labnbook.fr" target="_blank"><h1 class="lb-h1">{{__('Le cahier numérique partagé pour écrire et apprendre les sciences')}}</h1></a>
    </div>

    @include('_flash-alerts')

    <div id="maincontent">
        @yield('maincontent')
    </div>

@endsection
