<?php
    /** @var \App\Helper $helper */
?>
<meta charset="utf-8" />
<link rel="shortcut icon" type="image/png" href="/images/favicon_lnb.png" />
<link rel="icon" href="/images/favicon_lnb.png" />
<link rel="apple-touch-icon" href="/images/favicon_lnb.png" />
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<script type="text/javascript">
    var labnbook = {
        lang: <?= json_encode($helper->selectLanguage()) ?>,
        debug: <?= json_encode(config('labnbook.js_debug')) ?>,
        i18n: <?= json_encode(config('labnbook.i18n')) ?>,
        langFile: <?= json_encode($helper->geti18nLangFileUrl()) ?>
    };
</script>
<?php
    if (config('labnbook.i18n')) {
        echo $helper->loadAsset("/libraries/i18next/i18next.js");
        echo $helper->loadAsset("/libraries/i18next-xhr-backend/i18nextXHRBackend.js");
    }
    // We test app.env and not app.debug to separate background color changes from debug mode activation
    if (config('app.env') != 'production') {
        echo '<style>body { border-left: 10px solid orange !important; border-right: 10px solid orange !important; }</style>';
    }
?>
<?= $helper->loadAsset("/css/common.css"); ?>
<?= $helper->loadAsset("/css/lnb-light-colors.css"); ?>
<?= $helper->loadAsset('/libraries/stacktrace.js/stacktrace.min.js', 'async'); ?>
<!-- TODO probablement plus necessaire -->
<?php if (strncmp(config('app.url'), 'https://', 8) === 0): ?>
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests" />
<?php endif; ?>
{!!  $helper->loadAsset("/libraries/jquery/jquery-2.2.4.min.js")  !!}
<?= $helper->configureAjax() ?>
{!!  $helper->loadAsset("/libraries/jquery/jquery-ui.min.js")  !!}
{!!  $helper->loadAsset("/libraries/jquery/jquery-ui.min.css")  !!}
<?= $helper->loadAsset("/libraries/formJQuery/jquery.form.js") ?>
<?= $helper->loadAsset("/libraries/font_awesome/fontawesome-free-6.1.1-web/css/svg-with-js.css") ?>
<?= $helper->loadAsset("/libraries/font_awesome/fontawesome-free-6.1.1-web/css/all.css") ?>
<?= $helper->loadAsset("/js/common.js") ?>
<?= $helper->loadAsset("/libraries/alpine/alpinejs.js", 'defer') ?>
<?= $helper->loadAsset("/libraries/alpine/mask/mask.js", 'defer') ?>
<?= $helper->loadAsset("/libraries/select2.min.js", 'defer') ?>
<?= $helper->loadAsset("/libraries/select2.min.css", 'defer') ?>
