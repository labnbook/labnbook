<?php
    /** @var \App\Helper $helper */
    // Handle maintenance as any category
    $maintenance = config('labnbook.maintenance');
    $is_redirect = session()->pull('redirecting', false);
    if($maintenance && (request()->path() === 'login' || $is_redirect)){
        $helper->AddAlert('maintenance', $maintenance);
    }
    // Transform laravel validation error into labnbook alerts
    if (isset($errors)) {
        foreach($errors->all() as $error){
            $helper->AddAlert('warning', $error);
        }
    }
    $alerts =(array)Session::pull('alerts');
?>
@foreach($alerts as $cat => $messages)
    @foreach((array)$messages as $m)
        <div class="alert alert-{{$cat}}">
            <i onclick="this.parentNode.remove()" class="fa fa-times alert-close" title="{{__('Fermer')}}"></i>
            <!-- Messages are not escaped so we can use HTML in alerts !-->
            {!! __($m) !!}
        </div>
    @endforeach
@endforeach

