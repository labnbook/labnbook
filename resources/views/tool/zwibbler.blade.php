<div id="zwibbler_container_template" style="display:none">
    <zwibbler z-no-auto-init autoPickTool="true" background="grid" keyRedo="Ctrl+Y,Ctrl+Shift+Z" multilineText="true"
            showColourPanel="false" showHints="false" showToolbar="false" defaultFontSize="14" nudge="1" snap="10"
            class="flex-column" z-controller="labbook" touchRadius="5" defaultLineWidth="1" defaultBrushWidth="3"  defaultArrowStyle="solid" defaultArrowSize="8">
        <!-- Cf. https://zwibblerr.com/docs/#configuration-settings -->
    <div class="toolbar flex-row noselect" z-if="edition">
        <button z-click="useTool('rectangle')" z-selected="ctx.getCurrentTool()==='rectangle'" title="{{__('Ajouter un rectangle')}}">
            <img src="/tool_zwibbler/icons/menu_rectangle.png"></button>
        <button z-click="useTool('circle')" z-selected="ctx.getCurrentTool()==='circle'" title="{{__('Ajouter une ellipse')}}">
            <img src="/tool_zwibbler/icons/menu_circle.png"></button>
        <button z-click="useTool('line')" z-selected="ctx.getCurrentTool()==='line'" title="{{__('Ajouter une ligne')}}">
            <img src="/tool_zwibbler/icons/menu_line.png"></button>
        <button z-click="useTool('curve')" z-selected="ctx.getCurrentTool()==='curve'" title="{{__('Ajouter une courbe')}}">
            <img src="/tool_zwibbler/icons/menu_curve.png"></button>
        <button z-click="useTool('arrow')" z-selected="ctx.getCurrentTool()==='arrow'" title="{{__('Ajouter une flèche')}}">
            <img src="/tool_zwibbler/icons/menu_arrow.png"></button>
        <button z-click="useTool('brush')" z-selected="ctx.getCurrentTool()==='brush'" title="{{__('Dessiner librement')}}">
            <i class="fas fa-pencil-alt"></i></button>
        <button z-click="useTool('text')" z-selected="ctx.getCurrentTool()==='text'" title="{{__('Ajouter du texte')}}">
            <img src="/tool_zwibbler/icons/menu_text.png"></button>
        <button onclick='$("#fileinput").click()' title="{{__('Importer une image')}}">
            <i class="far fa-image"></i></button>
        <button z-show-popup="library" z-selected="ctx.getCurrentTool()==='imagestamp'" title="{{__('Ajouter une image de la bibliothèque')}}">
            <i class="far fa-folder-open"></i></button>
        <select z-if="teacher_mode" z-model="layer" z-on:input="ctx.setActiveLayer(layer)"
                                    style="margin: auto 70px; padding: 3px;" title="{{__("Le calque modifiable contient des éléments pouvant être modifiés par l'étudiant")}}">
            <option value="default" selected>{{__("Calque modifiable par l'étudiant")}}</option>
            <option value="teacher">{{__("Calque verrouillé pour l'étudiant")}}</option>
        </select>
        <div class="flex-row zoom" style="align-items: center">
            <button z-on:click="ctx.zoomOut()"><i class="fa fa-minus"></i></button>
            <input style="text-align : center; width:4em" type="text" z-zoom-percentage>
            <button z-on:click="ctx.zoomIn()"><i class="fa fa-plus"></i></button>
        </div>
    </div>
    <div class="stretch" style="position:relative">
        <div z-canvas class="full"></div>
        <!-- TODO Useful for LNB -->
        <div z-hide="!showDragHint" class="hint-text">{{__("Appuyez sur la touche ALT pour désactiver l’accrochage à la grille")}}</div>
        <div class="popup graphic-tools flex-row wrap noselect" z-moveable
             z-class="{hidden: ctx.summary.nodes.length === 0 && ctx.getCurrentTool() !== 'brush' && !editingText || !edition}">
            <button z-if="'fillStyle' in ctx.summary.properties && !('TextNode' in ctx.summary.types)"
                    z-click="colourProperty='fillStyle'" z-show-popup="palette" title="{{__('Couleur de remplissage')}}">
                <img src="/tool_zwibbler/icons/context_fill_color.png">
            </button>
            <button z-if="ctx.summary.properties['lineWidth'] > 0 && !ctx.summary.types.TextNode"
                    z-click="colourProperty='strokeStyle'" z-show-popup="palette" title="{{__('Couleur du trait')}}">
                <img src="/tool_zwibbler/icons/context_border_color.png">
            </button>
            <button z-if="'lineWidth' in ctx.summary.properties && !ctx.summary.types.TextNode"
                    z-show-popup="line-style"  title="{{__('Epaisseur du contour')}}">
                <img src="/tool_zwibbler/icons/context_border_style.png">
            </button>
            <button z-has="textFillStyle" z-click="colourProperty='textFillStyle'" z-show-popup="palette" title="{{__('Couleur du texte')}}">
                <img src="/tool_zwibbler/icons/context_txt_color.png">
            </button>
            <button z-has="fontSize" z-show-popup="text-size" title="{{__('Taille du texte')}}">
                <img src="/tool_zwibbler/icons/context_txt_style.png">
            </button>
            <button z-has="AnyNode" z-click="ctx.duplicate()" title="{{__("Dupliquer l'objet")}}">
                <img src="/tool_zwibbler/icons/context_duplicate.png">
            </button>
            <button z-has="AnyNode" z-click="ctx.bringToFront()" title="{{__('Mettre au premier plan')}}">
                <img src="/tool_zwibbler/icons/context_movefg.png">
            </button>
            <button z-has="AnyNode" z-click="ctx.sendToBack()" title="{{__("Mettre à l'arrière plan")}}">
                <img src="/tool_zwibbler/icons/context_movebg.png">
            </button>
            <button z-if="editElement" z-click="toggleSymbolPopup(this)" title="{{__('Symboles grecs')}}">
                <img src="/tool_zwibbler/icons/context_txt_symbol.png">
            </button>
            <button z-if="ctx.summary.nodes.length > 1 && !('GroupNode' in ctx.summary.types)"
                    z-click="ctx.createGroup(ctx.getSelectedNodes())" title="{{__('Grouper')}}">
                <img src="/tool_zwibbler/icons/context_group.png">
            </button>
            <button z-if="'GroupNode' in ctx.summary.types" z-click="ctx.ungroup(ctx.getSelectedNodes())" title="{{__('Dégrouper')}}">
                <img src="/tool_zwibbler/icons/context_ungroup.png">
            </button>
        </div>
        <div z-popup="line-style" class="graphic-tools flex-row wrap" z-show-position="bl tl"
                                                                      z-click="hideIfPen('line-style')">
            <button z-click="ctx.setProperties({lineWidth: 1, dashes: ''})">
                <img src="/tool_zwibbler/icons/submenu_border_1.png">
            </button>
            <button z-click="ctx.setProperties({lineWidth: 3, dashes: ''})">
                <img src="/tool_zwibbler/icons/submenu_border_3.png">
            </button>
            <button z-click="ctx.setProperties({lineWidth: 5, dashes: ''})">
                <img src="/tool_zwibbler/icons/submenu_border_5.png">
            </button>
            <button z-has="dashes" z-click="ctx.setProperties({lineWidth: 1, dashes: '7,3'})">
                <img src="/tool_zwibbler/icons/submenu_border_1dot.png">
            </button>
            <button z-has="dashes" z-click="ctx.setProperties({lineWidth: 3, dashes: '7,5'})">
                <img src="/tool_zwibbler/icons/submenu_border_3dot.png">
            </button>
            <button z-has="dashes" z-click="ctx.setProperties({lineWidth: 5, dashes: '7,7'})">
                <img src="/tool_zwibbler/icons/submenu_border_5dot.png">
            </button>
        </div>
        <div z-popup="text-size" class="graphic-tools flex-row wrap" z-show-position="bl tl">
            <button z-for="size in [10, 12, 14, 18, 24, 30]" z-click="ctx.setProperty('fontSize', size)">
                <img z-bind:src="'/tool_zwibbler/icons/submenu_font_'+size+'.png'">
            </button>
            <label>
                <input type="checkbox" z-checked="ctx.summary.properties.bold"
                                       z-on:click="ctx.setProperty('bold', !ctx.summary.properties.bold)"> {{__('Gras')}}
            </label>
        </div>
        <div z-popup="palette" class="flex-column" z-show-position="bl tl" z-click="hideIfPen('palette')">
            <div z-palette></div>
            <div class="flex-row wrap" z-if="colourProperty==='fillStyle'" style="width:320px">
                <button z-for="path in fillPatterns" z-style="{backgroundImage: 'url('+path+')'}"
                                                     z-click="ctx.setProperty('fillStyle', 'url('+path+')')">
                </button>
            </div>
        </div>
        <div class="symbol-popup popup graphic-tools symbols flex-row wrap" style="width:162px"
                                                                            z-on:click="symbolClicked($event)">
            <!-- http://www.htmlhelp.com/reference/html40/entities/symbols.html -->
            <a>&#x3B1;</a>
            <a>&#x3B2;</a>
            <a>&#x3B3;</a>
            <a>&#x3B4;</a>
            <a>&#x3B5;</a>
            <a>&#x3B6;</a>
            <a>&#x3B7;</a>
            <a>&#x3B8;</a>
            <a>&#x3B9;</a>
            <a>&#x3BA;</a>
            <a>&#x3BB;</a>
            <a>&#x3BC;</a>
            <a>&#x3BD;</a>
            <a>&#x3BE;</a>
            <a>&#x3BF;</a>

            <a>&#x3C1;</a>
            <a>&#x3C2;</a>
            <a>&#x3C3;</a>
            <a>&#x3C4;</a>
            <a>&#x3C5;</a>
            <a>&#x3C6;</a>
            <a>&#x3C7;</a>
            <a>&#x3C8;</a>
            <a>&#x3C9;</a>

            <a>&#x3D1;</a>
            <a>&#x3D2;</a>
            <a>&#x3D6;</a>

            <a>&#x391;</a>
            <a>&#x392;</a>
            <a>&#x393;</a>
            <a>&#x394;</a>
            <a>&#x395;</a>
            <a>&#x396;</a>
            <a>&#x397;</a>
            <a>&#x398;</a>
            <a>&#x399;</a>
            <a>&#x39A;</a>
            <a>&#x39B;</a>
            <a>&#x39C;</a>
            <a>&#x39D;</a>
            <a>&#x39E;</a>
            <a>&#x39F;</a>
            <a>&#x3A0;</a>
            <a>&#x3A1;</a>
            <a>&#x3A3;</a>
            <a>&#x3A4;</a>
            <a>&#x3A5;</a>
            <a>&#x3A6;</a>
            <a>&#x3A7;</a>
            <a>&#x3A8;</a>
            <a>&#x3A9;</a>
        </div>
        <div z-popup="library" class="flex-column noselect" z-show-position="bl tl" z-click-dismiss>
            <div style="text-align:center">
                <div class="flex-row" style="align-content:stretch">
                    <select z-model="currentImageFolder"
                            z-on:input="currentImageSubfolder=currentImageFolder.subfolders[0]">
                        <option z-for="folder in imageFolders" z-value="folder" z-text="folder.caption" z-if="!folder.local || teacher_mode || folder.subfolders[0].images.length>0"></option>
                    </select>
                    <select z-model="currentImageSubfolder" z-if="currentImageFolder.subfolders.length > 1">
                        <option z-for="subfolder in currentImageFolder.subfolders" z-value="subfolder"
                                                                                   z-text="subfolder.caption">
                        </option>
                    </select>
                </div>
            </div>
            <div class="image-library flex-row wrap stretch scroll-y">
                <div z-for="image in currentImageSubfolder.images" class="image-backing"  z-bind:title="image.title"  z-on:click="myStampTool(image, currentImageFolder)">
                    <img z-bind:src="getImage(image)" >
                    <div>
                        <button z-if="teacher_mode && currentImageFolder.local" class="remove-library-button"
                                z-no-dismiss z-on:click="removeDocumentClip(image),$event.stopPropagation()" title="{{__("Supprime l'élément de la bibliothèque")}}">
                            <span class="fas fa-trash-alt"></span>
                        </button>
                    </div>
                </div>
                <div z-if="currentImageFolder.local && teacher_mode" class="flex-row library-buttons">
                    <button z-disabled="!ctx.summary.nodes.length" z-on:click="addDocumentClip()" z-no-dismiss title="{{__("Ajoute les objets selectionnés à la bibliothèque de ce labdoc")}}">{{__("Ajouter l'objet sélectionné")}}</button>
                    <button z-on:click="uploadLibrary()" title="{{__("Importe une bibliothèque stockée sur l'ordinateur de l'utilisateur")}}"  z-no-dismiss >{{__("Importer une bibliothèque")}}  </button>
                    <button z-on:click="downloadLibrary()" title="{{__("Exporte la bibliothèque du labdoc courant sur l'ordinateur de l'utilisateur")}}"  z-no-dismiss>{{__("Exporter la bibliothèque")}} </button>
                </div>
            </div>
        </div>
    </div>
    <div z-if="edition" class="sizing-bar sizing-bar-vertical ui-resizable-s ui-resizable-n"> </div>
    </zwibbler>
</div>
