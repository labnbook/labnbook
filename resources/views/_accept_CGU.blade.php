<x-modal id="modal-cgu"
         header-class="modal-fixed"
         title="Consentement aux conditions générales d'utilisation" 
         :x-data="json_encode(['displayedModal' => true])"
         :include-alert="false"
         click-submit-action="acceptCGUs();displayedModal=false"
         click-cancel-action="logout(__('Vous ne pouvez pas utiliser LabNBook sans accepter les CGUs'))">
    <div class="modal-description"> 
        <span>{{__("En validant vous acceptez")}}
            <a target='_blank' href="{{config('labnbook.cgu_url')}}">{{__("les conditions générales d'utilisation de LabNBook")}}</a>
        </span>
    </div>
</x-modal>
