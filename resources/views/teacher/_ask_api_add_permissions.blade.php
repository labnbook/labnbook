@if (session('api_ask_permission_infos'))
<script type="text/javascript">
    $().ready(function() {
        if (confirm("{!!session('api_ask_permission_infos')['message']!!}")) {
            window.location = "{!!session('api_ask_permission_infos')['valid_url']!!}";
        }
    });
</script>
@endif
