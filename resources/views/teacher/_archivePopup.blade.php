<?php
    /** @var String $xDataFunction name of the related x-data function */
?>
<div class="popup" id="archive-popup" x-data="{{$xDataFunction}}" x-on:display-archive-popup.window="initArchivePopup()">
    <header>
        <span class="title">{{__('Faire le ménage')}}</span>
        <span class="actions" >
            <i class="fa fa-check popup-submit" title="{{__('Valider')}}" x-on:click="submitArchivePopup()"></i>
            <i class="fa fa-spinner fa-pulse " style="display:none" title="{{__('Patienter')}}"></i>
            <i class="fa fa-times popup-cancel" id="archive-popup-cancel" title="{{__('Annuler')}}"></i>
        </span>
    </header>
    <section>
        <form class="archive-popup-form">
            <div class="archive-popup-form-row" x-show="archiveLabel!==''">
                <input type="radio" value="archive" x-model="actionType" id="archive-input"/> 
                <label for="archive-input" class="archive-popup-form-row-label">
                    <span class="archive-popup-form-row-label-bold" x-text="archiveLabel"></span>
                    <span x-text="archiveDetailsLabel"></span>
                </label>
            </div>
            <div x-show="spreadArchive!=='' && actionType === 'archive'" x-html="spreadArchive"></div>
            <div class="archive-popup-form-row" x-show="unsubscribeLabel!==''">
                <input type="radio" value="unsubscribe" x-model="actionType" id="unsubscribe-input"/> 
                <label for="unsubscribe-input" class="archive-popup-form-row-label">
                    <span class="archive-popup-form-row-label-bold" x-text="unsubscribeLabel"></span>
                    <span x-text="unsubscribeDetailsLabel"></span>
                </label>
            </div>
            <div class="archive-popup-form-row" x-show="deleteLabel!==''">
                <input :disabled="!isDeletable" type="radio" value="delete" x-model="actionType" id="delete-input"/>
                <label for="delete-input" class="archive-popup-form-row-label">
                    <span class="archive-popup-form-row-label-bold" x-text="deleteLabel"></span>
                    <span x-text="deleteDetailsLabel"></span>
                </label>
            </div>
        </form>
    </section>
</div>
