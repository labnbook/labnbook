<?php
    /** @var \App\User $user */
    if ($user): ?>
        <span data-id-teacher="{{ $user->id_user }}" >-&nbsp;{{ $user->user_name . ' ' . $user->first_name }}&nbsp;<i onclick="deleteTeacherFromClass({{ $user->id_user }})" title="{{ __("Dissocier l'enseignant de la classe") }}" class="far fa-trash-alt"></i>&nbsp;</span>
    <?php endif ?>
