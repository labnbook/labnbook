<form id="le-add-studentnumbers-form" >
    <div>
        <textarea rows="3" name="inst-number"  placeholder="{{__("Entrez une liste de numéros d'étudiants (utiliser n'importe quel séparateur).")}}
{{__("Pour tous les comptes nouvellement créés, les étudiants se connecteront avec l'annuaire institutionnel.")}}
{{__("Cette méthode d'ajout est fortement conseillée !")}}"></textarea>
    </div>
    <div id="le-add-studentnumbers-popup-btn">
        <button type="button" onclick="queryLdapDirectory()" >{{ __("Interroger l'annuaire") }}</button>
    </div>
</form>
<div id="csv-content"></div>
<input type="checkbox" checked="checked" id="le-add-send-email" /><label for="le-add-send-email"> {{ __("Envoyer un message aux étudiants ajoutés (si leur adresse mail est définie)") }}</label>

