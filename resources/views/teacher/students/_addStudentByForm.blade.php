<fieldset class="le-fieldset">
    <legend>{{ __('Informations personnelles') }}</legend>
    <label for="{{ $prefix }}le-add-first-name" title="obligatoire" >{{ __('Prénom') }} * :</label> <input type="text" id="{{ $prefix }}le-add-first-name" name="{{ $prefix }}le-add-first-name" /><br/>
    <label for="{{ $prefix }}le-add-name" title="obligatoire" >{{ __('Nom') }} * :</label> <input type="text" id="{{ $prefix }}le-add-name" name="{{ $prefix }}le-add-name"/><br/>
    <label for="{{ $prefix }}le-add-inst-number">{{__('N° étudiant')}} <span id="{{ $prefix }}le-add-inst"></span> : </label> <input type="text" id="{{ $prefix }}le-add-inst-number" name="{{ $prefix }}le-add-inst-number"/><br/>
    <label for="{{ $prefix }}le-add-email" title="{{ __('E-mail ou mot de passe nécessaire') }}">E-mail (*) : </label> <input type="email" id="{{ $prefix }}le-add-email" name="{{ $prefix }}le-add-email" title="{{ __('E-mail ou mot de passe nécessaire') }}"/><br/>
</fieldset>
<fieldset class="le-fieldset">
    <legend>{{ __('Informations de connexion') }}<span id='le-add-cas'></span></legend>
    <label for="{{ $prefix }}le-add-login">{{ __('Intitulé du compte (login)') }} * :</label> <input type="text" id="{{ $prefix }}le-add-login" name="{{ $prefix }}le-add-login" /><br/>
    <label for="{{ $prefix }}le-add-password" title="{{ __('E-mail ou mot de passe nécessaire : 8 caractères ou plus dont une majuscule et une minuscule') }}" >{{ __('Mot de passe') . " (*)"}}{!!__("&nbsp;:")!!} </label> <input type="text" id="{{ $prefix }}le-add-password" name="{{ $prefix }}le-add-password" title="{{ __('E-mail ou mot de passe nécessaire : 8 caractères ou plus dont une majuscule et une minuscule') }}"/><br/>
</fieldset>
<div title="{{ __("En cochant cette case, l'étudiant recevra un e-mail de confirmation lors de la création ou de la mise à jour de son compte") }}" style="text-align: center" id="{{ $prefix }}le-add-send-email-div"><br/><input type="checkbox" id="{{ $prefix }}le-add-send-email" name="{{ $prefix }}le-add-send-email" />&nbsp;<label for="{{ $prefix }}le-add-send-email">{{ __('Envoyer un e-mail récapitulatif') }}</label></div>
