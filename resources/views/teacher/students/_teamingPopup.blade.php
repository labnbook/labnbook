<?php
/** @var \App\Teacher $teacher */
    $missions = $teacher->missions()->orderBy('code', 'ASC')->get()
?>
<div class="popup" id="tc-popup" title="" data-reset-input="true">
    <header>
        <span class="popup-title"></span>
        <span class="actions">
            <i title="{{ __("Valider") }}" id="tc-popup-submit" class="popup-submit fa fa-check disabled"> </i>
            <i title="{{ __("Annuler") }}" class="popup-cancel fa fa-times"> </i>
        </span>
    </header>
    <section>
        <div>
            <label class="tc-label">{{__('Mission attribuée')}}{!!__("&nbsp;:")!!}</label>
            <select id="tc-mission">
                <option value="0">-</option>
                @include('teacher/report/_missionOptions', ['missions' => $missions, 'selected' => null, 'includeArchive' => false, 'includeAllOption' => false])
            </select>
            <span class="tc-info-label" id="tc-students-number"></span>
            <button type="button" class="lb_btn" id="tc-delete-teaming">{{__("Supprimer la mise en équipe")}}</button>
        </div>

        <div id="tc-reuse">
            <div>
                <label class="tc-label">{{__("Réutiliser la mise en équipe de la mission")}}{!!__("&nbsp;:")!!}</label>
                <select id="tc-reuse-team-choice-mission">
                    <option value="0">-</option>
                </select>
                <button type="button" class="lb_btn" title="{{__("Réutiliser une mise en équipe d'une autre mission pour cette classe")}}"
                        id="tc-reuse-team-choice">{{__("Appliquer")}}</button>
                <i id="tc-reuse-tooltip" class="fa fa-info-circle lba_icon_default" onclick="tooltipReuseTeaming()"></i>
            </div>
        </div>

        <div id="tc-options" class="hide">
            <span onclick="toggleReportOption()">
                <i class="fa fa-caret-down"  id="tc-toggle-reports-options"></i>
                <span class="tc-label tc-label-toggle">{{__("Autorisations de travail des étudiants")}}</span> <i id="tc-report-authorizations-tooltip" class="fa fa-info-circle lba_icon_default" onclick="tooltipReportAuthorizations()"></i>
            </span>
            <div id="tc-report-options" class='subsection-content'>
                <div class="danger" id="tc-report-options-danger-message"></div>
                <label class="tc-label">{{__('Période de travail')}}{!!__("&nbsp;:")!!}</label>
                {{__('du')}}&nbsp;
                <input type="date" id="tc-start-date" name="start_date" placeholder="jj/mm/aaaa"/>
                <input type="time" id="tc-start-time" name="start_time" placeholder="hh:mm" value="00:00"/>
                {{__('au')}}&nbsp;
                <input type="date" id="tc-end-date" name="end_date" placeholder="jj/mm/aaaa"/>
                <input type="time" id="tc-end-time" name="end_time" placeholder="hh:mm" value="23:59"/>
                <i id="tc-work-period-tooltip" class="fa fa-info-circle lba_icon_default" onclick="tooltipWorkPeriod()"></i>
                <br>
                <label class="tc-label">{{__("Import de labdocs")}}{!!__("&nbsp;:")!!}</label>
                {{__('autorisé depuis les missions')}}&nbsp;
                <select id="tc-import-ld">
                    <option value="-1" selected>{{__('Toutes')}}</option>
                    <option value="0">{{__('Aucune')}}</option>
                    @include('teacher/report/_missionOptions', ['missions' => $missions, 'selected' => null, 'includeArchive' => true, 'includeAllOption' => false])
                </select>
                <i id="tc-import-ld-tooltip" class="fa fa-info-circle lba_icon_default" onclick="tooltipImportLD()"></i>
                <br>

                <label class="tc-label">{{__("Messagerie autorisée")}}{!!__("&nbsp;:")!!}</label>
                <input type="radio" value="1" name="tc-allow-msg"/> {{__('oui')}}
                <input type="radio" value="0" name="tc-allow-msg"/> {{__('non')}}
                <i id="tc-message-tooltip" class="fa fa-info-circle lba_icon_default" onclick="tooltipMessaging()"></i>
                <table class="hide" id="tc-messaging-options">
                    <tr>
                        <td rowspan="4" style="vertical-align: top">
                            <label>{{__('Destinataires autorisés')}}{!!__("&nbsp;:")!!}&nbsp;</label>
                        </td>
                        <td>
                            <label>{{__('les enseignants')}}{!!__("&nbsp;:")!!}&nbsp;</label>
                        </td>
                        <td>
                            <input type="radio" value="1" name="tc-msg-teacher" /> {{__('oui')}}
                            <input type="radio" value="0" name="tc-msg-teacher" checked/> {{__('non')}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>{{__("les étudiants de l'équipe")}}{!!__("&nbsp;:")!!}&nbsp;</label>
                        </td>
                        <td><input type="radio" value="1" name="tc-msg-team" /> {{__('oui')}}
                            <input type="radio" value="0" name="tc-msg-team" checked/> {{__('non')}}
                        </td>
                    </tr>
                    <tr>
                        
                        <td><label for="tc-msg-class">{{__('les étudiants de la classe')}}{!!__("&nbsp;:")!!}
                                &nbsp;</label></td>
                        <td><select id="tc-msg-class">
                                <option value="0">{{__('Aucune')}}</option>
                                @include('teacher/report/_classesOptions',['classes' => $teacher->classes()->orderBy('class.status')->orderBy('class.class_name')->get(), 'selected' => 0])
                            </select></td>
                    </tr>
                    <tr>
                        <td>
                            <label for="tc-msg-mission">{{__('les étudiants inscrits à la mission')}}{!!__("&nbsp;:")!!}
                                &nbsp;</label>
                        </td>
                        <td>
                            <select id="tc-msg-mission">
                                <option value="0">{{__('Aucune')}}</option>
                                @include('teacher/report/_missionOptions', ['missions' => $missions, 'selected' => null, 'includeArchive' => true, 'includeAllOption' => false])
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label> {{__("Permission d'attacher des labdocs aux messages envoyés")}}{!!__("&nbsp;:")!!}</label>
                        </td>
                        <td><input type="radio" value="1" name="tc-msg-send-ld"/> {{__('oui')}}
                            <input type="radio" value="0" name="tc-msg-send-ld" checked/> {{__('non')}}</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label> {{__("Permission d'enregistrer les labdocs reçus dans le rapport")}}{!!__("&nbsp;:")!!}
                            &nbsp;</label>
                        </td>
                        <td>
                            <input type="radio" value="1" name="tc-msg-save-ld"/> {{__('oui')}}
                            <input type="radio" value="0" name="tc-msg-save-ld" checked/> {{__('non')}}
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div id="tc-future-students" class="hide">
            <span onclick="toggleFutureStudentsOption()">
                <i class="fa fa-caret-down"  id="tc-toggle-future-students-options"></i>
                <span class="tc-label tc-label-toggle">{{__("Mode de répartition des étudiants")}}</span> <i id="tc-teaming-mode-tooltip" class="fa fa-info-circle lba_icon_default" onclick="tooltipTeamingMode()"></i>
            </span>
            <div id="tc-future-students-options" class='subsection-content'>
                <label class="tc-label">{{ __("Répartition des étudiants dans les équipes :") }}</label>
                <select id="tc-teaming-method" onchange="">
                    <option value="0">--</option>
                    <option value="1">{{__("au choix des étudiants")}}</option>
                    <option value="3">{{__("de façon aléatoire")}}</option>
                    <option value="2">{{__("uniquement par les enseignants")}}</option>
                </select>
                <i id="tc-teaming-method-tooltip" class="fa fa-info-circle lba_icon_default" onclick="tooltipTeamingMethod()"></i>
                <div id="tc-future-team-constraints-panel" class="hide">
                    <label class="tc-label">{{ __ ("Contraintes de répartition :") }}</label>
                    <table id="tc-future-team-constraints">
                        <tr>
                            <td>
                                <span>
                                    {{__("Taille des équipes :") }}
                                    <label for="tc-future-team-size-minimal">{{__("min.")}}{!!__("&nbsp;:")!!}&nbsp;</label>
                                    <input type="number" id="tc-future-team-size-minimal" name="tc-team-size-minimal" min="0"/>
                                    <label for="tc-future-team-size-optimal">{{__("optimum")."*"}}{!!__("&nbsp;:")!!}&nbsp;</label>
                                    <input type="number" id="tc-future-team-size-optimal" name="tc-team-size-optimal"  min="1"/>
                                    <label for="tc-future-team-size-maximal">{{__("max.")}}{!!__("&nbsp;:")!!}&nbsp;</label>
                                    <input type="number" id="tc-future-team-size-maximal" name="tc-team-size-maximal"  min="1"/>
                                </span>
                                <span>
                                    <label for="tc-future-max-team-number"> {{__("Nombre maximum d'équipes")}}{!!__("&nbsp;:")!!}&nbsp;</label>
                                <input type="number" id="tc-future-max-team-number" name="tc-max-team-number"  min="1"/>
                                </span>
                            </td>
                            <td>
                                <button type="button" class="lb_btn" title="Faire une répartion aléatoire des étudiants"
                                        id="tc-random-assignment">{{__("Appliquer")}}</button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

        <div id="tc-students" class="hide">
            <span onclick="toggleStudentsOption()">
                <i class="fa fa-caret-down"  id="tc-toggle-students-options"></i>
                <span class="tc-label tc-label-toggle">{{__("Répartition manuelle des étudiants et affichage des équipes formées")}}</span>
            </span>
            <div id="tc-students-options" class="subsection-content">
                <div class="row" id="tc-students-options-header">
                    <div class="col-xs-5 col-md-6">
                        <label class="tc-student-label hide" id="tc-title-student-without-team"></label>
                         <span id="tc-email">
                             <label class="tc-label" for="tc-email-on-create">{{__("Prévenir les étudiants par e-mail de leur mise en équipe :")}}</label>
                             <input type="checkbox" id="tc-email-on-create" name="tc-email-on-create" style="vertical-align: middle"/>
                        </span>
                    </div>
                    <div class="col-xs-7 col-md-6" >
                        <label class="tc-student-label hide" id="tc-title-student-with-team"></label>
                        <label class="tc-label">{{__("Préfixe du nom des équipes")}}{!!__("&nbsp;:")!!}&nbsp;</label>
                        <input type="text" id="tc-report-prefix" value="{{__("Equipe_")}}" placeholder="{{__("Equipe_")}}" style="margin-top : 0"/>
                        <i id="tc-report-prefix-tooltip" class="fa fa-info-circle lba_icon_default" onclick="tooltipReportPrefix()"></i>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-5 col-md-6" id="tc-student-without-team-panel">
                        <div>
                            <label class="tc-student-label tc-student-label-top">{{__("Etudiants sans rapport")}}</label>
                            <div class="tc-student-drop-zone" id="tc-students-without-team-wrapper">
                                <div id="tc-students-without-team-label">
                                    {{ __("Glissez-déposez les étudiants dans les équipes à droite") }}
                                </div>
                                <div id="tc-students-without-team" class="left-pane"></div>
                            </div>
                            <div>
                                <label class="tc-student-label">
                                    <span id="tc-label-external-student" class="tc-label"></span>
                                    <i id="tc-external-student-tooltip" class="fa fa-info-circle lba_icon_default" onclick="tooltipExternalStudent()"></i>
                                </label>
                                <div id="tc-external-student-panel">
                                    <label for="tc-add-external-learner">{{__('Rechercher un étudiant')}}{!!__("&nbsp;:")!!}</label>
                                    <x-select2 id="tc-add-external-learner" on-select-function="teamConfigReports.addStudentInTeaming(selectedElement)"></x-select2>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                   
                    <div class="col-xs-7 col-md-6" id="tc-student-with-team-panel">
                        <div>
                            <label class="tc-student-label tc-student-label-top" id="tc-legend-not-started-reports"></label>
                            <div id="tc-fieldset-not-started-reports" class="fieldset-report-drop-zone">
                            </div>
                        </div>
                       <div>
                           <label class="tc-student-label"><span id="tc-legend-started-reports"></span></label>
                           <div id="tc-info-started-reports" class="tc-info-label"></div>
                           <div id="tc-fieldset-started-reports" class="fieldset-report-drop-zone">
                           </div>
                       </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
</div>

