<form id='le-add-student-class-form' >
<p>{{__("Classe dans laquelle sélectionner des étudiants")}}{!!__("&nbsp;:")!!}
	<select id='le-add-student-class-select' onchange='updateLearnersFromClass(this.value)'>
		<option value='0'>---</option>
		<?php
            foreach ($classes as $cl) {
				echo "<option value='$cl->id_class'>".htmlspecialchars($cl->class_name)."</option>";
			}
		?>
	</select>
</p>
<p>
	<span id='le-add-student-class-num-selected'>0&nbsp;{{ __("étudiants sélectionnés") }}</span>
	<label>
		<input id='le-add-checkbox-all' type='checkbox' onclick='addStudentsToggleSelectAll()'/>
		{{ __("Sélectionner tous") }}
	</label>
</p>
<div id='le-add-student-class-students' style='columns:20ex'>
</div>
</form>

