<?php
/** @var \App\User[] $teachers */
/** @var boolean $editable */
foreach ($teachers as $t) {
    echo '<span data-id-teacher="' . $t->id_user . '">-&nbsp;' . $t->user_name . " " . $t->first_name;
    if ($editable) {
        echo "&nbsp;<i onclick=\"deleteTeacherFromClass(" . $t->id_user . ")\" title=\"" . e(__("Dissocier l'enseignant de la classe")) . "\" class=\"far fa-trash-alt\"></i>&nbsp;";
    }
    echo "</span> ";
}
