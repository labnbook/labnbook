<fieldset class="le-fieldset" id="le-add-student-class-code-form">
    <legend>{{ __('Code d’auto inscription') }}</legend>
    <label for="add-students-le-add-class-code" :title="'{{__('Obligatoire')}}'" >{{ __('Code de la classe') }} :</label> 
    <input type="text" id="add-students-le-add-class-code" name="add-students-le-add-class-code" value="{{$class_code}}"/>
    <br/>
    <div>{{__("Le code de classe permet aux étudiants de s’auto-inscrire à la classe.")}}
        <br>{{__('- S’ils ont déjà accès à la plateforme, ils doivent se connecter et indiquer le code dans leur espace "Mon Compte".')}}
        <br>{{__("- S’ils n’ont pas d'accès à la plateforme, ils indiquent le code sur la page de connexion de la plateforme, ce qui crée leur compte LabNBook et les inscrit dans la classe.")}}
    </div>
</fieldset>
