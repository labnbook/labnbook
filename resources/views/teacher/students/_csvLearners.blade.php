<?php
    /** @var array $learners */
     /** @var array $warnings */
      /** @var array $errors */
       /** @var int $nb_errors */
        /** @var int $nb_warnings */
         use \App\Processes\AddCSVLearners;
     ?>

<table><thead><tr>
            <td>{{ __('Prénom') }}<sup>*</sup></td>
            <td>{{ __('Nom') }}<sup>*</sup></td>
            <td>{{ __('Num. étudiant') }}</td>
            <td>{{ __('E-mail') }}<sup>¤</sup></td>
            <td>{{ __('Compte') }}<sup>*</sup></td>
            <td title={{ __('Obligatoire si email non renseigné') }}>{{ __('Mot de passe') }}<sup>¤</sup></td>
        </tr></thead><tbody>

            <?php
                $count = 0;
                foreach ($learners as $numl => $learner) {
                    if (!$learner) {
                        continue;
                    }
                    $count++;
                    $htmlClasses = [];
                    if ($learner[AddCSVLearners::PASSWORD] === AddCSVLearners::CAS_PASSWORD) {
                        $htmlClasses[] = "locked";
                    }
                    if ($learner['exists']) {
                        $htmlClasses[] = "already-in-db";
                    }
                ?>
                    <tr data-id="{{ (int) $numl }}" class="{{ join(" ", $htmlClasses) }}">
                        <td><input name="first_name" type="text" value="{{ $learner[AddCSVLearners::FIRST_NAME] }}" /></td>
                        <td><input name="user_name" type="text" value="{{ $learner[AddCSVLearners::USER_NAME] }}" /></td>
                        <td><input name="inst_number" type="text" value="{{ $learner[AddCSVLearners::INST_NUMBER] }}" /></td>
                        <td><input name="email" type="email" value="{{ $learner[AddCSVLearners::EMAIL] }}" /></td>
                        <td><input name="login" type="text" value="{{ $learner[AddCSVLearners::LOGIN] }}" /></td>
                        <td><input name="password" type="text" value="{{ $learner[AddCSVLearners::PASSWORD] }}" /></td>
                        <td class="csv-option-cell">
                    <?php if($warnings != null): ?>
                        <?php foreach ($warnings[$numl] as $val) :?>
                            <i class="lb-warning fa fa-exclamation-triangle" title="{{ $val }}" ></i>
                            <?php endforeach ?>
                        <?php foreach ($errors[$numl] as $val) : ?>
                            <i class="lb-error fa fa-exclamation-triangle" title="{{$val}}" ></i>
                        <?php endforeach ?>
                    <?php endif ?>
                    <i title="{{__("Supprimer la ligne")}}" class="cl-delete far fa-trash-alt"></i></td></tr>
                    <?php
                }
                echo __(":count étudiant(s)", ["count" => $count]);
                if ($nb_warnings + $nb_errors > 0) {
                ?>
                    {{ __(", :nb_warnings avertissement(s) et :nb_errors erreur(s) : passez sur les icônes", ["nb_warnings" => $nb_warnings, "nb_errors" => $nb_errors])}} 
                    <i class='fa fa-exclamation-triangle'></i> {{("pour en savoir plus.")}}<br/>
                    <?php
                }
                ?>
        {{__("Vous pouvez faire des modifications directement dans le tableau avant de valider la fenêtre.")}}<br/>
    <strong>{{__("Attention")}}:</strong> {{__("pour qu'une ligne soit prise en compte, elle ne doit pas comporter d'erreur - Vous pouvez supprimer des lignes si besoin.")}}<br/>
    <br/>
</tbody></table>
