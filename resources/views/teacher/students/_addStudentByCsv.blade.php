<div id="csv-load">
    {!! __('Votre fichier CSV doit utiliser le <strong>&nbsp;;&nbsp;</strong> comme séparateur.') !!} 
    {{ __("Il ne doit pas contenir de ligne d'entête.") }}<br/>
    {{__('Il doit contenir 6 colonnes')}}{!!__("&nbsp;:")!!}<br/>
    <blockquote><strong>
            {{ __('prénom') }}<sup>*</sup> &nbsp;;&nbsp;
            {{ __('nom') }}<sup>*</sup>  &nbsp;;&nbsp;
            {{ __("numéro d'étudiant") }} &nbsp;;&nbsp;
            {{ __("e-mail") }}<sup>¤</sup> &nbsp;;&nbsp;
            {{ __("intitulé du compte (login)") }} &nbsp;;&nbsp;
            {{ __("mot de passe") }}<sup>¤</sup>
        </strong>
    </blockquote>
    <p>
    <strong><sup>*</sup></strong> {{ __("information obligatoire") }}<br/>
    <strong><sup>¤</sup></strong> {{ __("il est nécessaire de renseigner soit le mot de passe, soit l'e-mail (pour envoyer le mot passe défini aléatoirement)") }}
    </p>
    <br/><input type="file" id="csv-input" onchange="getCSVFile()" />
</div>
<div id="csv-loading" style="display:none" >{{ __("Chargement...") }} <i class="fa fa-spinner fa-spin"></i></div>
<div class="alerts"></div>
<br/>
<div id="csv-confirm">
    <input type="checkbox" checked="checked" id="le-add-send-email" /><label for="le-add-send-email"> {{ __("Envoyer un message aux étudiants ajoutés (si leur adresse mail est définie)") }}</label>
    <div id="csv-content"></div>
</div>

