<?php
    /** @var \App\User $user */
    /** @var \int $id_class */
    /** @var \App\Helper $helper */
 ?>
@extends('layouts.labnbook')


@section('title', __('Gestion des classes'))

@section('head')
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <script>
        <!-- Variable transmises par PHP au javascript -->
        window.global_current_id_user    =  {{ $user->id_user }};
        <!-- Tell datatables that we should select the class id_class -->
        @if($id_class)
            window.global_select_class = {{$id_class}};
        @endif
    </script>

    <!-- TODO delete old assets -->
    <?= $helper->loadAsset("/css/teacher/teamingPopup.css") ?>

    <?= $helper->loadAsset("/libraries/papaparse.min.js") ?>

    <?= $helper->loadAsset("/libraries/datatables/datatables.min.css") ?>
    <?= $helper->loadAsset("/libraries/datatables/datatables-all.js") ?>

    <?= $helper->loadAsset("/css/flexboxgrid.min.css") ?>
    <?= $helper->loadAsset("/css/teacher/common.css") ?>
    <?= $helper->loadAsset("/js/teacher/authoring.js") ?>
    <?= $helper->loadAsset("/js/teacher/teaming/team-randomization.js") ?>

    <!--Styles et fonctions de la page -->
    <?= $helper->loadAsset("/css/teacher/students.css") ?>
    <?= $helper->loadAsset("/js/teacher/students.js", "defer") ?>
    <?= $helper->loadAsset("/js/teacher/teamingPopup.js", "defer") ?>
    <?= $helper->loadAsset("/js/teacher/addStudents.js", 'defer') ?>

@endsection

@section('bodyattributes', 'id=page-students')

@section('body')
    @include('teacher/_menu', [
        'page_name' => request()->path(),
        'refresh_period' => config('labnbook.refresh_period'),
        ])
    @include('teacher/students/_teamingPopup',  ['teacher' => $user->teacher])

    <div id="wrapper-content">
        <!-- tableau des classes -->
        <section class="width-small" id="classes">
            <div class="table-caption">
                <strong>{{ __('Classes') }}</strong>
                <span class="tb-search">
                    <div id="tb-search-class" style="display: none;">
                        <div>
                            <input class="search-input" type="text" placeholder="{{ __('Recherche') }}" />&nbsp;<i title="{{ __('Fermer la recherche') }}" class="fa fa-times-circle btn_close_search"></i>
                        </div>
                    </div>
                    <i title="{{ __('Rechercher') }}" class="fa fa-search"></i>
                </span>
                <span class="display-archives" title="{{ __('Afficher les éléments archivés') }}">
                    <input id="display-archives" name="display-archives" type="checkbox">
                    <label for="display-archives"> {{ __('archives') }}</label>
                </span>
                <span class="actions" id="cl-actions">
                    <span class="lba-context-icons actions-inactive">
                        <a href="#" data-action="openTeamConfig" title="{{ __('Mise en équipe : attribuer une mission aux étudiants de la classe') }}" class="single-action"><i class="fa fa-users"></i></a>
                        <a href="#" data-action="edit" title="{{ __('Modifier la classe') }}" class="single-action"><i class="fas fa-pencil-alt"></i></a>
                        <a href="#" data-action="sendmail" title="{{ __('Envoyer un e-mail à tous les étudiants de la classe') }}" class="single-action"><i class="fa fa-at"></i></a>
                        <a href="#" data-action="archive" title="{{ __('Archiver / supprimer la classe') }}" class="single-action"><i class="fa fa-broom"></i></a>
                    </span>
                    <a href="#" data-action="add" title="{{ __('Ajouter une classe') }}"><i class="fa fa-plus"></i></a>
                </span>
            </div>
            <table class="table-base" id="cl-table">
                <thead>
                    <tr>
                        <th title="{{ __('Trier par nom') }}" class="sorting_asc">{{ __('Nom') }}</th>
                        <th title="{{ __("Trier par nombre d'étudiants") }}">{{ __('Étudiants') }}</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>{{ __('Nom') }}</th>
                        <th>{{ __('Étudiants') }}</th>
                </tfoot>
            </table>
        </section>
        <section class="width-medium" id="students" style="display: none;">
            <div class="table-caption">
                <strong>{{ __('Étudiants') }}</strong>
                <div class="dataTables_length">
                    <select class="pagination-size">
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="-1">{{ __('Tous') }}</option>
                    </select>
                    / <span id="num-visible-students"></span>
                </div>
                <span class="tb-search">
                    <input class="search-input" type="text" placeholder="{{ __('Recherche') }}" />
                </span>
                <span class="options actions" id="le-actions">
                    <span class="lba-context-icons">
                        <a href="#" onclick="openEditLearnerPopup()" title="{{ __("Modifier l'étudiant") }}" class="single-action"><i id="le-edit" class="fas fa-pencil-alt"></i></a>
                        <a href="#" onclick="deleteLearnerFromClass()" title="{{ __("Retirer l'étudiant de la classe") }}" class="single-action"><i class="fa fa-trash-alt"></i></a>
                    </span>
                    <a href="#" onclick="openAddStudentDialog({{auth()->id()}})" title="{{ __('Ajouter des étudiants') }}"><i id="add-students-btn" class="fa fa-plus"></i></a>
                </span>
            </div>
            <table id="le-table" class="table-base">
                <thead>
                    <tr>
                        <th title="{{ __('Trier par nom') }}">{{ __('Nom') }}</th>
                        <th title="{{ __('Trier par prénom') }}">{{ __('Prénom') }}</th>
                        <th title="{{ __('Trier par date de dernière connexion') }}">{{ __('Connexion') }}</th>
                        <th title="{{ __('Trier par mission') }}">{{ __('Missions') }}</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>{{ __('Nom') }}</th>
                        <th>{{ __('Prénom') }}</th>
                        <th>{{ __('Connexion') }}</th>
                        <th>{{ __('Missions') }}</th>
                    </tr>
                </tfoot>
            </table>
            <div class="clear"></div>
        </section>
    </div>
    
    <div class="popup" id="cl-popup" title="{{ __('Ajouter ou modifier une classe') }}">
        <header>
           <span class="title" id="cl-popup-title"></span>
           <span class="actions" >
               <i class="fa fa-check popup-submit" title="Valider" id="cl-popup-valid"></i>
               <i class="fa fa-times popup-cancel" title="Annuler" id="cl-popup-cancel"></i>
           </span>
        </header>
        <section>
           <label for="cl-add-name">{{ __('Nom de la classe') }} * :</label> <input type="text" id="cl-add-name" name="cl-add-name" /><br/>
           <label for="cl-add-inst">{{ __('Institution') }} :</label> <select id="cl-add-inst" onchange='selectInstForClass()'></select><br/>
           <fieldset id="cl-teacher-datalist">
               <legend>{{ __('Enseignants associés') }}</legend>
               <div id="cl-teachers-list"></div>
        
               @if (!$user->teacher->isManager())
                   <div>{{__('Associer un enseignant')}}{!!__("&nbsp;:")!!}
                       <x-select2 id="cl-add-teacher-or-team" on-select-function="addTeacherInClass(selectedElement)"></x-select2>
                   </div>
                   <span id="cl-add-teacher-temp"></span>
                @else
                   <div>{{__('Associer un/des enseignants')}}{!!__("&nbsp;:")!!}
                       <x-select2 id="cl-add-teacher-or-team" name="cl-add-teacher" on-select-function="addTeacherOrTeamInClass(selectedElement)"></x-select2>
                   </div>
                   <span id="cl-add-teacher-or-team-temp"></span>
               @endif
             
           </fieldset>
        </section>
    </div>
    
    <div class="popup" id="add-students" title="{{ __('Ajouter des étudiants à cette classe') }}">
        <header>
           <span class="title">{{ __('title') }}</span>
           <span class="actions" >
               <i class="fa fa-check popup-submit" title="{{ __('Valider') }}" onclick="addStudentsDialog.allowSubmit(false);addStudentsDialog.submit()"></i>
               <i class="fa fa-times popup-cancel" title="{{ __('Annuler') }}" onclick="addStudentsDialog.close()"></i>
           </span>
        </header>
        <section class="popup-contents">
        </section>
    </div>
    
    <div class="popup" id="le-popup">
        <header>
            <span id="le-popup-title">{{__("Modifier l'étudiant")}}</span>
            <span class="actions" >
                <i class="fa fa-check popup-submit" title="{{ __("Ajouter l'étudiant") }}" id="le-popup-valid"></i>
                <i class="fa fa-times popup-cancel" title="{{ __('Annuler') }}" id="le-popup-cancel"></i>
            </span>
        </header>
        <section>
            @include('teacher/students/_addStudentByForm', ['prefix' => ''])
        </section>
    </div>
    
    <div class="popup" id="le-add-popup">
        <header>
           <span id="le-add-popup-title" class="title">{{ __('Étudiant existant ?') }}</span>
           <span class="actions" >
               <i class="fa fa-times popup-cancel" title="{{ __('Annuler') }}" id="le-add-popup-cancel"></i>
           </span>
        </header>
        <section></section>
    </div>
    
    <div id="teaming-popup"></div>

    @include('components.widgets.message.index', ['mv' => new App\Views\Widgets\Message($user->id_user, 0)])
    @include('teacher/_archivePopup', ['xDataFunction' => "archivePopup('classActions')"])
@endsection
