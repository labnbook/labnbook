<?php
    /** @var array $users */
     $count = 0;
     foreach($users as $user) {
 ?>
    <span data-id-learner="{{$user->id_user}}" >
 <?php
         if ($count != 0) {
             echo ' - ';
         }
     ?>
     {{$user->user_name}} {{$user->first_name}}
     &nbsp;<i title="{{__("Supprimer l'étudiant de la sélection")}}" onclick="reportsTable.deleteLearnerFromTeam({{$user->id_user}})" class="far fa-trash-alt"></i></span>
<?php
        $count++;
     }
