<div class="simulation-table-wrapper" x-init="simEditNS.selected_item = 'constraint'">
    <div class="table-caption simulation-header">
        <strong class="simulation-header-constraint clickable" :class="{'inactive': simEditNS.selected_item!=='constraint'}"
                x-on:click="simEditNS.selected_item='constraint'"> {{__('Contraintes')}}</strong>
        <strong class="simulation-header-task-type clickable" :class="{'inactive': simEditNS.selected_item!=='task_type'}"
                x-on:click="simEditNS.selected_item='task_type'"> {{__('Types de tâche')}}</strong>
        <strong class="simulation-header-global-knowledge clickable" :class="{'inactive': simEditNS.selected_item!=='global_knowledge'}"
                x-on:click="simEditNS.selected_item='global_knowledge'"> {{__('Connaissances')}}</strong>
    </div>
    
    <div class="simulation-setup-constraint" x-show="simEditNS.selected_item==='constraint'">
        @include('teacher.simulation._simulation_constraints_setup')
    </div>
    
    <div class="simulation-setup-task-type" x-show="simEditNS.selected_item==='task_type'">
        @include('teacher/simulation/_simulation_task_type_setup')
    </div>
    
    <div class="simulation-setup-global-knowledge" x-show="simEditNS.selected_item==='global_knowledge'">
        @include('teacher/simulation/_simulation_global_knowledge')
    </div>
    
</div>
