<div class="table-overflow-wrapper">
    <table class="dataTable table-base">
        <thead>
            <tr>
                <th></th>
                <th>{{ __('Index') }}</th>
                <th>{{ __('Description') }}</th>
                <th>{{ __('Msg Technologique') }}</th>
                <th>{{ __('Contraintes') }}</th>
                <th>{{ __('LG') }}</th>
                <th>{{ __('K') }}</th>
                <th></th>
            </tr>
        </thead>
        <tbody x-init="simEditNS.initializeSorting($el, 'sortable-task-type', 'taskTypes')">
            <template x-for="(taskType, taskTypeIndex) in simEditNS.dataMigration.taskTypes"
                      :key="'position_'+taskType.position+'_id_'+taskType.id_task_type">
                <tr class="sortable-task-type-item" :class="{'odd': taskTypeIndex%2===0, 'even': taskTypeIndex%2===1}"
                    x-on:simulation-scroll-to-task_type.window="if($event.detail.id_elem === taskType.id_task_type){await $nextTick();$el.scrollIntoView({ behavior: 'smooth', block: 'center'});}">
                    <td class="small-column movable sortable-task-type-handle">
                        <i class="fa-solid fa-up-down-left-right"></i>
                    </td>
                    <td class="small-column">
                        <span x-text="taskTypeIndex"></span>
                    </td>
                    <td>
                        <x-textarea-auto-resize x-model="taskType.description"></x-textarea-auto-resize>
                    </td>
                    <td :class="{'disabled-cell':taskType.learningGoal===0}">
                        <x-textarea-auto-resize x-model="taskType.technological"></x-textarea-auto-resize>
                    </td>
                    <td class="small-column">
                        <template x-for="(c, index) in simEditNS.getRelatedElementsById(taskType, 'id_task_type', 'constraints')">
                            <span class="clickable"
                                  :title="c.description"
                                  x-on:click="simEditNS.goToElement('constraint', c.id_constraint)"
                                  x-text="c.position+(index < simEditNS.getRelatedElementsById(taskType, 'id_task_type', 'constraints').length-1?', ':'')"></span>
                        </template>
                    </td>
                    <td class="small-column">
                        <select x-model="taskType.learningGoal">
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                        </select>
                    </td>
                    <td class="small-column">
                        <select x-show="taskType.learningGoal>0" x-model="taskType.id_global_knowledge" :title="simEditNS.getElementDescription(taskType.id_global_knowledge, 'id_global_knowledge', 'globalKnowledges')">
                            <option value="" label=""></option>
                            <template x-for="(globalKnowledge, index) in simEditNS.dataMigration.globalKnowledges">
                                <option :value="globalKnowledge.id_global_knowledge" :label="index" :selected="parseInt(globalKnowledge.id_global_knowledge)===parseInt(taskType.id_global_knowledge)"></option>
                            </template>
                        </select>
                    </td>
                    <td class="small-column">
                        <button class="remove-button" x-on:click="simEditNS.removeElement('taskTypes', taskTypeIndex)" :title="'{{__('Supprimer ce type de tâche')}}'">
                            <i class="fas fa-trash-can"></i>
                        </button>
                    </td>
                </tr>
            </template>
        </tbody>
    </table>
</div>
<button x-on:click="simEditNS.addNewElement('taskTypes', 'id_task_type');
     $nextTick(()=>{$el.previousElementSibling.scrollTop = $el.previousElementSibling.scrollHeight;});">
    {{__('Ajouter un type de tâche')}}
    <i class="fas fa-plus"></i>
</button>
