<div class="table-overflow-wrapper">
    <table class="dataTable table-base" x-data="{display_key:false, modif_key:false}">
        <thead>
        <tr>
            <th></th>
            <th>{{ __('Index') }}</th>
            <th>{{ __('SC') }}</th>
            <th>{{ __('Msg erreur') }}</th>
            <th class="clickable" x-on:click="display_key=!display_key" 
                x-on:dblclick="modif_key=!modif_key">{{ __('Clé') }}</th>
            <th>{{ __('Sim') }}</th>
            <th>{{ __('RC') }}</th>
            <th>{{ __('Cours') }}</th>
            <th>{{ __('Msg Technique') }}</th>
            <th>{{ __('Msg Tâche') }}</th>
            <th>{{ __('TT') }}</th>
            <th></th>
        </tr>
        </thead>
        <tbody x-init="simEditNS.initializeSorting($el, 'sortable-constraint', 'constraints')">
        <template x-for="(constraint, constraintIndex) in simEditNS.dataMigration.constraints"
                  :key="'position_'+constraint.position+'_id_'+constraint.id_constraint">
            <tr class="sortable-constraint-item" :class="{'odd': constraintIndex%2===0, 'even': constraintIndex%2===1}"
                x-on:simulation-scroll-to-constraint.window="if($event.detail.id_elem === constraint.id_constraint){await $nextTick();$el.scrollIntoView({ behavior: 'smooth', block: 'center'});}">
                <td class="movable sortable-constraint-handle small-column">
                    <i class="fa-solid fa-up-down-left-right"></i>
                </td>
                <td class="small-column" x-on:dblclick="simEditNS.insertElementAfter('constraints', 'id_constraint', constraintIndex)">
                    <span x-text="constraintIndex"></span>
                </td>
                <td>
                    <x-textarea-auto-resize x-model="constraint.description"></x-textarea-auto-resize>
                </td>
                <td>
                    <x-textarea-auto-resize x-model="constraint.message"></x-textarea-auto-resize>
                </td>
                <td>
                    <template x-if="modif_key">
                        <input type="text" x-model="constraint.key" style="width:500px"/>
                    </template>
                    <template x-if="!modif_key">
                        <span :class="{'constraint-key-extended': display_key, 'constraint-key-reduced': !display_key}" x-text="constraint.key"></span>
                    </template>
                </td>
                <td class="small-column">
                    <input type="checkbox" x-model="constraint.simulationEnabled">
                </td>
                <td>
                    <div class="rc-wrapper">
                        <template x-for="(relevanceConstraintId, index) in constraint.relevanceConstraintIds.sort(simEditNS.sortRelevanceIds())">
                            <div class="rc-inner-wrapper">
                                <span x-text="simEditNS.dataMigration.constraints.findIndex(cFb => parseInt(cFb.id_constraint) === parseInt(relevanceConstraintId))"
                                    :title="simEditNS.getElementDescription(relevanceConstraintId, 'id_constraint', 'constraints')"></span>
                                <button class="remove-button-rc" x-on:click="constraint.relevanceConstraintIds.splice(index,1)">
                                    <i class="fas fa-trash-alt"></i>
                                </button>
                            </div>
                        </template>
                    </div>
                    <select x-data="{chosenId:null}" x-model="chosenId" x-on:change="constraint.relevanceConstraintIds.push(chosenId); chosenId=null;">
                        <option value="" label=""></option>
                        <template x-for="relevanceConstraint in simEditNS.filterRelevanceIds(constraint)">
                            <option :value="relevanceConstraint.id_constraint" :label="simEditNS.dataMigration.constraints.findIndex(cFb => parseInt(cFb.id_constraint) === parseInt(relevanceConstraint.id_constraint))"></option>
                        </template>
                    </select>
                </td>
                <td>
                    <x-textarea-auto-resize x-model="constraint.theoretical"></x-textarea-auto-resize>
                </td>
                <td :class="{'disabled-cell':simEditNS.isRelatedTTLg0(constraint)}">
                    <x-textarea-auto-resize x-model="constraint.technical"></x-textarea-auto-resize>
                </td>
                <td>
                    <x-textarea-auto-resize x-model="constraint.correction"></x-textarea-auto-resize>
                </td>
                <td class="small-column">
                    <select x-model="constraint.id_task_type" :title="simEditNS.getElementDescription(constraint.id_task_type, 'id_task_type', 'taskTypes')">
                        <option value="" label=""></option>
                        <template x-for="(taskType, index) in simEditNS.dataMigration.taskTypes">
                            <option :value="taskType.id_task_type" :label="index" :selected="parseInt(taskType.id_task_type)===parseInt(constraint.id_task_type)"></option>
                        </template>
                    </select>
                </td>
                <td class="small-column">
                    <button class="remove-button"  :title="'{{__('Supprimer cette contrainte et ces messages')}}'"
                            x-on:click="simEditNS.removeElement('constraints', constraintIndex, constraint.id_constraint)">
                        <i class="fas fa-trash-can"></i>
                    </button>
                </td>
            </tr>
        </template>
        </tbody>
    </table>
</div>
<button x-on:click="simEditNS.addNewElement('constraints', 'id_constraint');
    $nextTick(()=>{$el.previousElementSibling.scrollTop = $el.previousElementSibling.scrollHeight;});">
    {{__('Ajouter une contrainte')}}
    <i class="fas fa-plus"></i>
</button>
