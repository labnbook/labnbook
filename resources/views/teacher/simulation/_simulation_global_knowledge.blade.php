<div class="table-overflow-wrapper">
    <table class="dataTable table-base">
        <thead>
        <tr>
            <th></th>
            <th>{{ __('Index') }}</th>
            <th>{{ __('Description') }}</th>
            <th>{{ __('TT') }}</th>
            <th></th>
        </tr>
        </thead>
        <tbody x-init="simEditNS.initializeSorting($el, 'sortable-global-knowledge', 'globalKnowledges')">
        <template x-for="(globalKnowledge, globalKnowledgeIndex) in simEditNS.dataMigration.globalKnowledges"
                  :key="'position_'+globalKnowledge.position+'_id_'+globalKnowledge.id_global_knowledge">
            <tr class="sortable-global-knowledge-item" :class="{'odd': globalKnowledgeIndex%2===0, 'even': globalKnowledgeIndex%2===1}">
                <td class="small-column movable sortable-global-knowledge-handle">
                    <i class="fa-solid fa-up-down-left-right"></i>
                </td>
                <td class="small-column">
                    <span x-text="globalKnowledgeIndex"></span>
                </td>
                <td>
                    <x-textarea-auto-resize x-model="globalKnowledge.description"></x-textarea-auto-resize>
                </td>
                <td class="small-column">
                    <template x-for="(tt, index) in simEditNS.getRelatedElementsById(globalKnowledge, 'id_global_knowledge', 'taskTypes')">
                        <span class="clickable"
                              :title="tt.description"
                              x-on:click="simEditNS.goToElement('task_type', tt.id_task_type)"
                              x-text="tt.position+(index < simEditNS.getRelatedElementsById(globalKnowledge, 'id_global_knowledge', 'taskTypes').length-1?', ':'')"></span>
                    </template>
                </td>
                <td class="small-column">
                    <button class="remove-button" x-on:click="simEditNS.removeElement('globalKnowledges', globalKnowledgeIndex)" :title="'{{__('Supprimer cette connaissance')}}'">
                        <i class="fas fa-trash-can"></i>
                    </button>
                </td>
            </tr>
        </template>
        </tbody>
    </table>
</div>
<button x-on:click="simEditNS.addNewElement('globalKnowledges', 'id_global_knowledge');
     $nextTick(()=>{$el.previousElementSibling.scrollTop = $el.previousElementSibling.scrollHeight;});">
    {{__('Ajouter une connaissance')}}
    <i class="fas fa-plus"></i>
</button>
