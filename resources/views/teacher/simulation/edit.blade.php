<?php
/** @var \App\Helper $helper */
?>
@extends('layouts.labnbook')


@section('title', __('Gestion des simulations'))

@section('head')

    <?= $helper->loadAsset("/css/teacher/common.css") ?>
    <?= $helper->loadAsset("/css/teacher/simulations_edition.css") ?>

    <?= $helper->loadAsset("/libraries/datatables/datatables.min.css") ?>
    <?= $helper->loadAsset("/libraries/datatables/datatables-all.js") ?>
    
    <?= $helper->loadAsset("/js/teacher/authoring.js") ?>
    <?= $helper->loadAsset("/libraries/sortable/Sortable.min.js") ?>
    <?= $helper->loadAsset("/js/teacher/simulations_edition.js") ?>

@endsection

@section('bodyattributes', 'id=page-missions')

@section('body')
    @include('teacher/_menu', [
    'page_name' => request()->path(),
    'refresh_period' => config('labnbook.refresh_period'),
    ])

    <div id="wrapper-content" x-data="{simulations_code:{{Auth::user()->getSimulationsRelated()}}, selected_code:null, code_changed:false}">

        <div class="table-caption simulation-header">
            <strong>{{__('Choisissez un TP simulé')}} : </strong>
            <select class="simulation-code-selector" x-model="selected_code">
                <option value=""></option>
                <template x-for="simulation_code in simulations_code">
                    <option :value="simulation_code.simulation_code" :label="simulation_code.simulation_code"></option>
                </template>
            </select>
            <template x-if="selected_code">
                <div class="simulation-header-buttons">
                    <button x-on:click="$dispatch('simulation-export-json-for-migration',{})">
                        {{__('Exporter les données de migration en JSON')}}
                    </button>
                    <button x-on:click="$dispatch('simulation-export-csv-data',{})">
                        {{__('Exporter les données en CSV')}}
                    </button>
                </div>
            </template>
        </div>
        
        <template x-if="selected_code">
            <div x-data="{simEditNS:simulationEditionCtrl(selected_code)}" x-init="simEditNS.init(); $watch('simEditNS.dataMigration', debounce(() => simEditNS.updateMigrationData(), 500));" 
                 x-effect="simEditNS.simulationCode = selected_code; simEditNS.init();"
                 x-on:simulation-export-json-for-migration.window="simEditNS.exportJSON()"
                 x-on:simulation-export-csv-data.window="simEditNS.exportCSV()">
                <template x-if="!simEditNS.loading">
                   @include('teacher/simulation/_simulation_setup')
                </template>
                <div x-show="simEditNS.loading"><i class="fa fa-spinner fa-pulse"></i></div>
            </div>
        </template>
    </div>
    @include('components.widgets.message.index', ['mv' => new App\Views\Widgets\Message(Auth::user()->id_user, 0)])
@endsection
