<?php
/**
 * @var \App\Mission $mission
 */
 $ressources = $mission->resources()->where('res_type', '<>', 'assignment')
                                    ->whereNull('id_report')
                                    ->orderBy('position', 'ASC')
                                    ->get(); 
?>
<div class="rd_draggable" style="height: 0;"></div>
<?php
foreach ($ressources as $ressource) {
        if ($ressource->res_type == "file") {
			$file_name = $ressource->res_path;
			$file_ext = strtolower(pathinfo($file_name, PATHINFO_EXTENSION));
			if ($file_ext == "pdf") {
				$ressource_type = "pdf";
			} else if ($file_ext == "png" || $file_ext == "jpg" || $file_ext == "jpeg" || $file_ext == "gif") {
				$ressource_type = "image";
			} else {
                $ressource_type = "file";
            }
            $path = "/storage/missions/".$mission->id_mission."/resources/" ;
		}
		else {
			$ressource_type = "link";
			$path = "";
		}
    ?>
        @include('teacher/missions/_RDLine', [
            'ressource' => $ressource,
            'path' => $path,
            'ressource_type' =>  $ressource_type,
        ])
    <?php
    }
