<?php
/**
 * @var \App\Mission $mission
 */

?>

    <!-- Zone liste de document -->
<div id="bloc_rd" class="mi-bloc" x-data="{is_add_rd:false}">
    <label class="edit-mission-header">{{ __("Documents") }} </label><span class="label_small">{{ __("(liens internet ou fichiers pdf/images/…)") }}</span>
    <div class="item_list">
        @include('teacher/missions/_RD', ['mission' => $mission ])
    </div>
    
    <template x-if="!is_add_rd">
        <div id="bloc_rd_add" class="lba_line bordure pointer" x-on:click="is_add_rd = true;">
            <i class="fa fa-plus lba_icon_default" title="{{ __("Ajouter un document") }}"></i>
            <span class="place_holder_text">{{ __("Ajouter un document") }}</span>
        </div>
    </template>

    <template x-if="is_add_rd">
        <div id="bloc_rd_form" class="form_edit bordure" x-data="{rd_type:'link'}">
            <div class="title">
                {{ __("Ajout d'un document") }}
                <span id="add_rd_buttons">
                    <i class="fa fa-check icon_baseline" title="{{ __("Ajouter le document") }}" x-on:click="is_add_rd=addRD()"></i>
                    <i class="fa fa-times icon_baseline" title="{{ __("Annuler") }}" x-on:click="is_add_rd=false"></i>
                </span>
                <span id="bloc_rd_form_fa_spinner" style="display: none"><i class="fa fa-spinner fa-pulse"></i></span>
            </div>
            <div>
                {{__("Type de document")}}{!!__("&nbsp;:")!!}
                <input type="radio" value="link" id="rd_type_link" class="fedit_radio" x-model="rd_type" autocomplete="off" checked>
                <label for="rd_type_link">{{ __("Lien internet") }}</label>
                <input type="radio" value="file" id="rd_type_file" class="fedit_radio" x-model="rd_type" autocomplete="off" >
                <label for="rd_type_file">{{ __("Fichier (pdf, image…") }} - 8 Mo max.)</label>
            </div>
            <div>
                <span class="input_label">{{__("Nom")}}{!!__("&nbsp;:")!!} </span>
                <input id="rd_name" class="write" name="name" type="text">
                <span id="block_rd_url" x-show="rd_type==='link'">
                    <span class="input_label">{{ __("Adresse")}}{!!__("&nbsp;:")!!} </span>
                    <input id="rd_url" class="lba_write rd_url" type="text" name="rd_url"/>
                </span>
                <span id="block_rd_file" x-show="rd_type==='file'">
                    <button type="button" class="btn ln_btn" x-on:click="selectRDFile(null)">{{ __("Sélectionner un fichier")}}</button>
                    <span id="rd_add_feedback_upload"></span>
                </span>
            </div>
        </div>
    </template>
</div>
