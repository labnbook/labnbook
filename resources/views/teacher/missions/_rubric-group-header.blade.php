<div class="group-header-wrapper">
    <div class="description-wrapper" x-data="{ openGroupDescriptionTextArea: false }"
         :class="{ 'rubric-invalid-group-text': group.invalidText }">
        <div class="description-content" x-show="!(openGroupDescriptionTextArea || group.invalidText)" 
             x-on:click="openGroupDescriptionTextArea = true; rubNS.rubric.hasChanged = true;
                         $nextTick(() => { $root.querySelector(`[x-ref='textarea_group_${group.id_criteria_group}']`).focus(); });"
             :title="rubNS.getInformation('changeGroupDescriptionButton', 'criteria_group')">
            <span x-text="group.description" :title="group.description"></span>
        </div>
        <div class="description-editor" x-show="openGroupDescriptionTextArea || group.invalidText" 
             x-on:click.outside.stop="openGroupDescriptionTextArea = false; rubNS.rubric.hasChanged = true;
                                      rubNS.check('invalidText', 'criteria_group', group)">
            <template x-if="group.invalidText">
                <i class="fas fa-circle-exclamation input-error" :title="rubNS.getInformation('groupErrorHover', 'criteria_group')"
                   x-on:click="global_tooltip_popup.openTooltipPopup('{{ __("Intitulé invalide") }}', rubNS.getInformation('groupDescriptionErrorIcon', 'criteria_group'), $el)"></i>
            </template>
            <textarea type="text" x-model.lazy="group.description" :title="rubNS.getInformation('groupTextareaHover', 'criteria_group')"
                      :x-ref="'textarea_group_' + group.id_criteria_group" lang="{{ $lang }}" spellcheck="true"
                      :placeholder="rubNS.getInformation('groupDescriptionPlaceholder', 'criteria_group', group)"
                      x-on:focus="$el.style.height = $el.scrollHeight + 'px'; rubNS.rubric.hasChanged = true"></textarea>
        </div>
    </div>
    <div class="controls-and-weight-wrapper">
        <div class="controls-wrapper">
            <i class="fas fa-up-down-left-right criteria_group-handle" :title="rubNS.getInformation('moveGroupButton', 'criteria_group')"
               x-show="rubNS.rubric.criteria_group.length > 1"></i>
            <i class="far fa-trash-alt" :title="rubNS.getInformation('deleteGroupButton', 'criteria_group')"
               x-on:click="rubNS.deleteGroup(group); rubNS.rubric.hasChanged = true"></i>
        </div>
        <div class="weight-wrapper" x-data="{ openGroupWeightInput: false }"
             :class="{ 'rubric-invalid-group-value': group.invalidValue }">
            <div class="weight-content" x-show="!(openGroupWeightInput || group.invalidValue)"
                 x-on:click="openGroupWeightInput = true; rubNS.rubric.hasChanged = true;
                             $nextTick(() => { $root.querySelector(`[x-ref='input_group_${group.id_criteria_group}']`).focus(); });">
                <i class="fas fa-weight-hanging" :title="rubNS.getInformation('changeGroupWeightButton', 'criteria_group')"></i>
                <span x-text="rubNS.displayValueOrComputedValue(group)" :title="rubNS.getInformation('changeGroupWeightButton', 'criteria_group')"></span>
            </div>
            <div class="weight-editor" x-show="openGroupWeightInput || group.invalidValue"
                 x-on:click.outside.stop="openGroupWeightInput = false; rubNS.rubric.hasChanged = true;
                                          rubNS.checkAndUpdateValue('invalidValue', 'criteria_group', group)">
                <template x-if="group.invalidValue">
                    <i class="fas fa-circle-exclamation input-error" :title="rubNS.getInformation('groupErrorHover', 'criteria_group')"
                       x-on:click="global_tooltip_popup.openTooltipPopup('{{ __("Poids invalide") }}', rubNS.getInformation('groupWeightErrorIcon', 'criteria_group'), $el)"></i>
                </template>
                <i class="fas fa-weight-hanging"></i>
                <input type="text" lang="{{ $lang }}" :title="rubNS.getInformation('groupInputHover', 'criteria_group')"
                       placeholder="auto" x-model.lazy="group.relative_weight"
                       :x-ref="'input_group_' + group.id_criteria_group"
                       x-on:keyup.enter.stop="openGroupWeightInput = false; rubNS.rubric.hasChanged = true;
                                              rubNS.checkAndUpdateValue('invalidValue', 'criteria_group', group)">
            </div>
        </div>
    </div>
</div>
