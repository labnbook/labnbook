<?php
    /** @var \App\User $user */
    /** @var \App\Helper $helper */
 ?>
@extends('layouts.labnbook')


@section('title', __('Gestion des missions'))

@section('head')

    <?= $helper->loadAsset("/css/teacher/common.css") ?>
    <?= $helper->loadAsset("/js/teacher/authoring.js") ?>

    <?= $helper->loadAsset("/libraries/datatables/datatables.min.css") ?>
    <?= $helper->loadAsset("/libraries/datatables/datatables-all.js", 'defer') ?>

    <?= $helper->loadAsset("/css/teacher/missions.css") ?>
    <?= $helper->loadAsset("/js/teacher/missions.js", 'defer') ?>
    <?= $helper->loadAsset("/libraries/katex/katex.min.css") ?>
    <?= $helper->loadAsset("/libraries/katex/katex.min.js") ?>
    <?=  $helper->loadAsset("/libraries/katex/contrib/mhchem.min.js") ?>

@endsection

@section('bodyattributes', 'id=page-missions')

@section('body')
    @include('teacher/_menu', [
        'page_name' => request()->path(),
        'refresh_period' => config('labnbook.refresh_period'),
        ])

        <div id="wrapper-content">
            <section class="width-full" id="private-missions">
                <div class="table-caption">
                    <strong>{{ __("Missions personnelles") }}</strong>
                    <div class="dataTables_length">
                        <select class="pagination-size">
                            <option value="15">15</option>
                            <option value="50">50</option>
                            <option value="-1">{{ __("Toutes") }}</option>
                        </select>
                        / <span id="num-visible-missions-private"></span>
                    </div>
                    <span class="tb-search">
                        <input class="search-input" type="text" placeholder={{__("Recherche")}} />
                    </span>
                    <span class="display-archives" title="{{ __("Afficher les éléments archivés") }}">
                        <input id="display-archives" name="display-archives" type="checkbox" />
                        <label for="display-archives">{{ __("afficher les missions archivées") }}</label>
                    </span>
                    <span class="actions" id="private-missions-actions">
                        <span class="lba-context-icons actions-inactive">
                            <a href="#" class="single-action" data-action="test" title="{{ __("Tester la mission (nouvel onglet)") }}"><i class="fa fa-file"></i></a>
                            <a href="#" class="single-action" data-action="edit" title="{{ __("Modifier la mission") }}"><i class="fas fa-pencil-alt"></i></a>
                            <a href="#" class="single-action" data-action="correct" title="{{ __("Ouvrir le rapport enseignants : corrigé, dépôt de labdocs... (nouvel onglet)") }}"><i class="fa-solid fa-file-circle-check"></i></a>
                            <a href="#" class="single-action" data-action="duplicate" title="{{ __("Dupliquer la mission") }}"><i class="fa-solid fa-clone"></i></a>
                            <a href="#" class="single-action" data-action="archive" title="{{ __("Archiver / supprimer la mission") }}"><i class="fa fa-broom"></i></a>
                        </span>
                        <a href="#" data-action="add" title="{{ __("Ajouter une mission") }}"><i class="fa fa-plus"></i></a>
                    </span>
                </div>
                <table class="table-base table-missions" id="my-missions">
                    <thead>
                        <tr>
                            <th title="{{ __("Trier par code de mission") }}" class="sorting_asc">{{ __("Code") }}</th>
                            <th title="{{ __("Trier par nom de mission") }}">{{ __("Nom") }}</th>
                            <th title="{{ __("Trier en fonction de votre rôle dans cette mission") }}">{{ __("Rôle") }}</th>
                            <th title="{{ __("Date de création de la mission") }}">{{ __("Création") }}</th>
                            <th title="{{ __("Trier par date de modification") }}">{{ __("Modif.") }}</th>
                            <th title="{{ __("Nombre d'équipes inscrites à cette mission") }}">{{ __("Rapports") }}</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>{{ __("Code") }}</th>
                            <th>{{ __("Nom") }}</th>
                            <th>{{ __("Rôle") }}</th>
                            <th>{{ __("Création") }}</th>
                            <th>{{ __("Modif.") }}</th>
                            <th>{{ __("Rapports") }}</th>
                        </tr>
                    </tfoot>
                </table>
            </section>

            <section id="public-missions" class="width-full" >
                <div class="table-caption">
                    <strong>{{ __("Missions publiques") }}</strong>
                    <div class="dataTables_length">
                        <select class="pagination-size">
                            <option value="15">15</option>
                            <option value="50">50</option>
                            <option value="-1">{{ __("Toutes") }}</option>
                        </select>
                        / <span id="num-visible-missions"></span>
                    </div>
                    <span class="tb-search">
                        <input class="search-input" type="text" placeholder="{{ __("Recherche") }}" />
                    </span>
                    <span class="actions">
                        <span class="lba-context-icons actions-inactive">
                            <a href="#" class="single-action" data-action="test" title="{{ __("Tester la mission (nouvel onglet)") }}"><i class="fa fa-file"></i></a>
                            <a href="#" class="single-action" data-action="duplicate" title="{{ __("Dupliquer la mission") }}"><i class="fa-solid fa-clone"></i></a>
                        </span>
                    </span>
                </div>
                <table class="table-base table-missions">
                    <thead>
                        <tr>
                            <th title="{{ __("Trier par code de mission") }}" class="sorting_asc">{{ __("Code") }}</th>
                            <th title="{{ __("Trier par nom de mission") }}">{{ __("Nom") }}</th>
                            <th>{{ __("Création") }}</th>
                            <th title="{{ __("Trier par date de modification") }}">{{ __("Modif.") }}</th>
                            <th>{{ __("Rapports") }}</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>{{ __("Code") }}</th>
                            <th>{{ __("Nom") }}</th>
                            <th>{{ __("Création") }}</th>
                            <th>{{ __("Modif.") }}</th>
                            <th>{{ __("Rapports") }}</th>
                        </tr>
                    </tfoot>
                </table>
            </section>

            <!-- popup d'ajout de mission -->

            <div class="popup" id="modal-mission-add" title="{{__('Ajouter une mission')}}" data-reset-input="true">
                <header>
                    <span class="title" >{{ __("Nouvelle mission") }}</span>
                    <span class="actions" >
                        <a href="#" title="{{ __("Valider") }}" class="popup-submit"><i class="fa fa-check"></i></a>
                        <a href="#" title="{{ __("Annuler") }}" class="popup-cancel"><i class="fa fa-times"></i></a>
                    </span>
                </header>
                <section>
                    <form>
                        <label for="mi-add-code">{{ __("Code de la mission") }} * <span class="label_small">{{ "(3-16 ".__("caractères").")" }}</span> : </label>
                        <input required pattern=".{3,16}" type="text" id="mi-add-code" name="mi-add-code" maxlength="16"/><br/><br/>
                        <label for="mi-add-name">{{ __("Nom de la mission") }} * <span class="label_small">{{ "(3-64 ".__("caractères").")" }}</span> :</label><br/>
                        <input required pattern=".{3,64}" type="text" id="mi-add-name" name="mi-add-name" maxlength="64"/><br/>
                    </form>
                </section>
            </div>

            @include('components.widgets.message.index', ['mv' => new App\Views\Widgets\Message($user->id_user, 0)])
            @include('teacher/_archivePopup', ['xDataFunction' => "archivePopup('missionActions')"])

        </div>
    @endsection
