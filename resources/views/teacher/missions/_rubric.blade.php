@php
    $lang = Lang::Locale();
    $rubric_broadcast_title = __("Diffusion aux étudiants");
    $rubric_broadcast_message = __("Si vous cochez cette case, les étudiants pourront voir cette grille.<br><br>Toutefois, ils n’auront accès à l’évaluation de leur travail que lorsque celle-ci sera rendue.");
    $rubric_help_title = __("Notions principales");
    $rubric_help_message = "<ul><li>"
        . __("Le <b>critère</b> est l’élément de base d’une évaluation par grille critériée.") . " "
        . __("Dans la grille ci-dessous, chaque ligne commençant par un rectangle gris représente un critère (exceptée la ligne «Bonus / Malus»).") . "</li><br /><li>"
        . __("Les critères sont rassemblés dans des <b>groupes de critères</b>.") . "<br />"
        . __("Ici, les groupes sont représentés avec des en-têtes bleues.") . "</li><br /><li>"
        . __("Un critère est composé de <b>descripteurs</b> ou d’<b>observables</b>.") . "<br />"
        . __("Un <b>descripteur</b> est un court texte qui décrit un <b>niveau de maîtrise</b> du critère.") . " "
        . __("Un <b>observable</b> est un élément d’une <b>checklist</b> qui témoigne de la conformité du travail de l’étudiant avec les attentes du correcteur.") . "</li><br /><li>"
        . __("Les <b>poids</b> de ces différents objets permettent d’ajuster leur importance relative dans l’évaluation.") . "<br />"
        . __("Seuls les poids des descripteurs et des observables sont obligatoires.") . "</li></ul><br />";
@endphp

{!! $helper->loadAsset("/js/scoring/rubric.js"); !!}
{!! $helper->loadAsset("/css/scoring/rubric.css"); !!}

<!-- rubNS stands for rubricNamespace -->
<div id="rubric" x-data="{ rubNS: rubricCtrl() }" x-init="rubNS.init()"
     :class="{ 'edition': rubNS.rubricEdition }" style="display:none"
     x-on:lnb-custom-event.window="rubNS.rubricAutoSave($event)">
    <span class="fa fa-spinner fa-pulse" x-show="!rubNS.dataLoaded"></span>
    <template x-if="rubNS.dataLoaded && !rubNS.rubric.initialized">
        <div class="creation-controls-wrapper" x-data="{ activities: {}, showApplyButton: false }"
             x-init="activities = await rubNS.getActivities()">
            <button type="button" class="lb lb_btn" title="{{ __('Créer une grille d’évaluation') }}"
                    x-on:click="rubNS.createRubric(); rubNS.rubric.hasChanged = true">
                {{ __("Créer une grille d’évaluation") }}
            </button>
            <div class="rubric-creation-separator" x-show="activities.length > 0"><span><i>{{ __("ou bien") }}</i></span></div>
            <div class="existing-rubric-selection" x-show="activities.length > 0">
                {{ __("Copier la grille de la mission") }}
                <select x-on:change="showApplyButton = true; rubNS.template_rubric = await rubNS.getRubric($event.target.value);">
                    <option disabled selected value="0">{{ "--" }}</option>
                    <template x-for="category in activities">
                        <optgroup :label="category.title">
                            <template x-for="activity in category.items">
                                <option :value="activity.value" x-text="activity.label"></option>
                            </template>
                        </optgroup>
                    </template>
                </select>
                <button type="button" class="btn lb_btn" title="{{ __('Copier la grille séléctionnée') }}"
                        x-show="showApplyButton" x-on:click="rubNS.duplicateRubric(rubNS.template_rubric); rubNS.rubric.hasChanged = true">
                    {{ __("Appliquer") }}
                </button>
            </div>
        </div>
    </template>

    <div class="report-options" x-show="rubNS.dataLoaded && rubNS.rubric.initialized">
        <span title="{{ __("Diffusion aux étudiants") }}">
            <input type="checkbox" id="rubric_broadcast" name="text" onclick='validContent(this.id);'
                   @if ($current_mission->rubric_broadcast) checked @endif>
            <label for="rubric_broadcast">
                {{ __("Autoriser la diffusion de la grille aux étudiants avant le rendu de l’évaluation") }}
            </label>
        </span>
        <i id="rubric_broadcast_tooltip"
           class="fa fa-info-circle lba_icon_default"
           onmouseover="global_tooltip_popup.openTooltipPopup('{{ $rubric_broadcast_title }}', '{{ $rubric_broadcast_message }}', '#rubric_broadcast_tooltip')"
           onclick="global_tooltip_popup.openTooltipPopup('{{ $rubric_broadcast_title }}', '{{ $rubric_broadcast_message }}', '#rubric_broadcast_tooltip')">
        </i>
    </div>
    <template x-if="rubNS.dataLoaded && rubNS.rubric.initialized">
        <div class="rubric-wrapper">
            <!-- RO mode -->
            <div x-show="!rubNS.rubricEdition">
                <div class="lba_line bordure ru_line" :id="'ru_' + rubNS.rubric.id_rubric">
                    <span x-text="rubNS.rubric.name"></span>
                    <span class="read context_menu"  id="ru_context_menu">
                        <i x-on:click="rubNS.editRubric()" class="fa fa-pencil-alt fa-fw lba_icon_default" title="{{ __("Éditer la grille d'évaluation") }}"></i>
                        <i x-on:click="await rubNS.deleteRubric()" class="far fa-trash-alt fa-fw lba_icon_default" title="{{ __("Supprimer la grille d'évaluation") }}"></i>
                    </span>
                </div>
                <div class="lba_line bordure" :id="'ru_' + rubNS.rubric.id_rubric + '_description'" x-show="rubNS.rubric.description != null">
                    <span x-effect="updateHtmlWithFormulas('#ru_' + rubNS.rubric.id_rubric + '_description', rubNS.rubric.description);"></span>
                </div>
            </div>
            <!-- RW mode -->
            <div x-show="rubNS.rubricEdition">
                <div class="existing-assessments">
                    <p x-text="rubNS.getInformation('existingAssessments', 'global')" x-show="rubNS.showExistingAssessments(false)"></p>
                    <p x-text="rubNS.getInformation('publishedAssessments', 'global')" x-show="rubNS.showExistingAssessments(true)"></p>
                </div>
                @include('teacher/missions/_rubric-name-description')
                <table class="rubric" :style="{ 'width': 'calc(' + rubNS.viewConfig.tablePercentWidth + '% - 0)' }"
                       :data-id="rubNS.rubric.id_rubric" data-type="rubric" x-init="rubNS.initializeSorting('criteria_group', $el)"
                       :class="{ 'rubric-invalid-rubric-empty': rubNS.rubric.empty }"
                       x-show="rubNS.showEditor">
                    <thead>
                        <tr>
                            <th :colspan="rubNS.viewConfig.columnsNumber">
                                <i id="rubric-help"
                                   class="fa fa-info-circle lba_icon_default"
                                   onmouseover="global_tooltip_popup.openTooltipPopup('{{ $rubric_help_title }}', '{{ $rubric_help_message }}', '#rubric-help')"
                                   onclick="global_tooltip_popup.openTooltipPopup('{{ $rubric_help_title }}', '{{ $rubric_help_message }}', '#rubric-help')">
                                </i>
                            </th>
                        </tr>
                    </thead>
                    <template x-for="group in rubNS.rubric.criteria_group.sort((a, b) => a.position - b.position)"
                              :key="group.id_criteria_group + '_' + group.position">
                        <tbody :data-id="group.id_criteria_group" data-type="criteria_group" class="criteria_group-item-draggable"
                               x-init="rubNS.initializeSorting('criterion', $el)">
                            <tr class="group" :class="{ 'rubric-invalid-group-empty': group.empty }">
                                <td class="group-header" :colspan="rubNS.viewConfig.columnsNumber - 1">
                                    @include('teacher/missions/_rubric-group-header')
                                </td>
                                <td class="group-score" :style="{ 'width': rubNS.viewConfig.criterionScorePercentWidth + '%' }">
                                    <div>
                                        <span class="score-value" x-text="'/&nbsp;' + rubNS.format(group.max_score)"
                                              :title="rubNS.getInformation('groupScoreHover', 'criteria_group')">
                                        </span>
                                    </div>
                                </td>
                            </tr>
                            <template x-if="group.empty || group.addEmptyTrOnDrag">
                                <tr class="criterion criterion-item-draggable empty-group"></tr>
                            </template>
                            <template x-if="!group.empty">
                                <template x-for="criterion in rubNS.getCriteriaFromGroup(group).sort((a, b) => a.position - b.position)"
                                          :key="criterion.id_criterion + '_' + criterion.position">
                                    <tr class="criterion criterion-item-draggable" :data-id="criterion.id_criterion"
                                        :class="{
                                            descriptor: criterion.type === 'descr',
                                            observable: criterion.type === 'ob',
                                            'rubric-invalid-criterion-empty': criterion.empty
                                        }"
                                        data-type="criterion" x-init="rubNS.initializeSorting('descrob', $el)">
                                        <td class="criterion-description" :style="{ 'width': rubNS.viewConfig.criterionTitlePercentWidth + '%' }">
                                            @include('teacher/missions/_rubric-criterion-description')
                                        </td>
                                        <template x-for="descrob, index in criterion.descrobs.sort((a, b) => a.position - b.position)"
                                                  :key="descrob.id_descrob + '_' + descrob.position">
                                            <td class="descrob descrob-item-draggable"
                                                :colspan="(rubNS.viewConfig.columnsNumber - 3) / criterion.descrobs.length"
                                                :style="{ 'width': rubNS.viewConfig.descrobColumnPercentWidth * (rubNS.viewConfig.columnsNumber - 3) / criterion.descrobs.length + '%' }"
                                                :data-id="descrob.id_descrob" data-type="descrob">
                                                @include('teacher/missions/_rubric-descrob')
                                            </td>
                                        </template>
                                        <template x-if="criterion.descrobs.length == 0">
                                            <td class="add-descrob"
                                                :style="{ 'width': (rubNS.viewConfig.tablePercentWidth - rubNS.viewConfig.criterionTitlePercentWidth - rubNS.viewConfig.criterionScorePercentWidth) + '%' }"
                                                :colspan="rubNS.viewConfig.columnsNumber - 2">
                                                <div class="controls-wrapper lba_line">
                                                    <div>
                                                        <i class="fas fa-plus lba_icon_default" :title="rubNS.getInformation('addDescriptorButton', 'descrob')"
                                                           x-on:click="criterion.type = 'descr'; idDescrob = rubNS.addDescrOb(null, criterion); rubNS.rubric.hasChanged = true;
                                                           $nextTick(() => { document.querySelector(`[x-ref='textarea_descrob_${idDescrob}']`).focus(); })"></i>
                                                        <span class="place_holder_text pointer" x-text="rubNS.getInformation('addDescriptorButton', 'descrob')"
                                                              x-on:click="criterion.type = 'descr'; idDescrob = rubNS.addDescrOb(null, criterion); rubNS.rubric.hasChanged = true;
                                                              $nextTick(() => { document.querySelector(`[x-ref='textarea_descrob_${idDescrob}']`).focus(); })"></span>
                                                    </div>
                                                    <div><span>{{ __("ou") }}</span></div>
                                                    <div>
                                                        <i class="fas fa-plus lba_icon_default" :title="rubNS.getInformation('addObservableButton', 'descrob')"
                                                           x-on:click="criterion.type = 'ob'; idDescrob = rubNS.addDescrOb(null, criterion); rubNS.rubric.hasChanged = true;
                                                           $nextTick(() => { document.querySelector(`[x-ref='textarea_descrob_${idDescrob}']`).focus(); })"></i>
                                                        <span class="place_holder_text pointer" x-text="rubNS.getInformation('addObservableButton', 'descrob')"
                                                              x-on:click="criterion.type = 'ob'; idDescrob = rubNS.addDescrOb(null, criterion); rubNS.rubric.hasChanged = true;
                                                              $nextTick(() => { document.querySelector(`[x-ref='textarea_descrob_${idDescrob}']`).focus(); })"></span>
                                                    </div>
                                                </div>
                                            </td>
                                        </template>
                                        <template x-if="criterion.descrobs.length > 0">
                                            <td class="add-descrob right-stacked"
                                                :style="{ 'width': rubNS.viewConfig.criterionAddDescrobWidth + '%' }"
                                                colspan="1">
                                                <div class="controls-wrapper lba_line">
                                                    <i class="fas fa-plus lba_icon_default" :title="rubNS.getInformation('addDescrobButton', 'descrob', null, criterion)"
                                                       x-on:click="idDescrob = rubNS.addDescrOb(null, criterion); rubNS.rubric.hasChanged = true;
                                                       $nextTick(() => { $root.querySelector(`[x-ref='textarea_descrob_${idDescrob}']`).focus(); })"></i>
                                                </div>
                                            </td>
                                        </template>
                                        <td class="criterion-score"
                                            :style="{ 'width': rubNS.viewConfig.criterionScorePercentWidth + '%' }">
                                            <div class="criterion-score-wrapper">
                                                <div class="criterion-grade">
                                                    <span x-text="'/&nbsp;' + rubNS.format(criterion.max_score)"
                                                          :title="rubNS.getInformation('criterionScoreHover', 'criterion')">
                                                    </span>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </template>
                            </template>
                            <tr class="add-criterion">
                                <td :colspan="rubNS.viewConfig.columnsNumber">
                                    <div class="controls-wrapper lba_line">
                                        <i class="fas fa-plus lba_icon_default" :title="rubNS.getInformation('addCriterionButton', 'criterion')"
                                           x-on:click="idCriterion = rubNS.addCriterion(null, group); rubNS.rubric.hasChanged = true;
                                           $nextTick(() => { $root.querySelector(`[x-ref='textarea_criterion_${idCriterion}']`).focus(); })"></i>
                                        <span class="place_holder_text pointer" x-text="rubNS.getInformation('addCriterionButton', 'criterion')"
                                              x-on:click="idCriterion = rubNS.addCriterion(null, group); rubNS.rubric.hasChanged = true;
                                              $nextTick(() => { $root.querySelector(`[x-ref='textarea_criterion_${idCriterion}']`).focus(); })"></span>
                                    </div>
                                </td>
                            </tr>
                            <tr class="separator">
                                <td :colspan="rubNS.viewConfig.columnsNumber"></td>
                            </tr>
                        </tbody>
                    </template>
                    <tbody>
                    <tr class="add-group">
                        <td :colspan="rubNS.viewConfig.columnsNumber">
                            <div class="controls-wrapper lba_line">
                                <i class="fas fa-plus lba_icon_default" :title="rubNS.getInformation('addGroupButton', 'criteria_group')"
                                   x-on:click="idGroup = rubNS.addGroup(null); rubNS.rubric.hasChanged = true;
                                      $nextTick(() => { $root.querySelector(`[x-ref='textarea_group_${idGroup}']`).focus(); })"></i>
                                <span class="place_holder_text pointer" x-text="rubNS.getInformation('addGroupButton', 'criteria_group')"
                                      x-on:click="idGroup = rubNS.addGroup(null); rubNS.rubric.hasChanged = true;
                                      $nextTick(() => { $root.querySelector(`[x-ref='textarea_group_${idGroup}']`).focus(); })"></span>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                    <tbody>
                        <tr class="separator">
                            <td :colspan="rubNS.viewConfig.columnsNumber"></td>
                        </tr>
                    </tbody>
                    <tbody>
                        <tr class="final-assessment">
                            @include('teacher/missions/_rubric-final-assessment')
                        </tr>
                    </tbody>
                </table>

                <div class="save-or-cancel" x-data="{ waiting: false }">
                    <i class="fas fa-circle-exclamation" title="{{ __("Anomalie détectée, cliquez pour en savoir davantage") }}"
                       x-show="rubNS.rubric.errors && rubNS.rubric.errors.length > 0"
                       x-on:click="global_tooltip_popup.openTooltipPopup('{{ __("Sauvegarde impossible") }}', rubNS.rubric.errorsHtml, $el)"></i>
                    <button type="button" class="lnb-rubric-save" title="{{ __("Sauvegarder") }}" x-show="!waiting"
                            :disabled="rubNS.formInvalid"
                            x-on:click="waiting = true; rubNS.showEditor = false; await rubNS.saveRubric(); waiting = false; rubNS.showEditor = true;">
                        {{ __("Sauvegarder") }}
                    </button>
                    <i class="fa fa-spinner fa-pulse" x-show="waiting"></i>
                </div>
            </div>
        </div>
    </template>
</div>
