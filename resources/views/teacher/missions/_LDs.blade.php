<?php
    /**
     * @var \App\ReportPart $rp
     */
     $query = $rp->labdocs()
                  ->whereNull('id_report')
                  ->whereNull('id_team_config')
                  ->orderBy('position', 'ASC')
                  ->select('labdoc.*');
    $ld_shared = $query->clone()
                  ->where('shared', 1)
                  ->get();
    $ld_report = $query->where('shared', 0)
                       ->get();
    foreach ([$ld_shared, $ld_report] as $source) {
        foreach ($source as $l) {
            $l->id_new_ld = 0;
            $l->shared_in_db = $l->shared;
            unset($l->labdoc_data);
        }
    }
 ?>
<div class="list_ld_part" id="list_ld_part_{{$rp->id_report_part}}"
    x-init='rpNS.initLabdocs(@JSON($ld_shared), @JSON($ld_report))'
     >
    <template x-if="rpNS.labdocs_report.length === 0">
        <div class="ld_draggable_report empty_drag_area">
        </div>
    </template>
     <template x-for="ld in rpNS.labdocs_report"
               :key="ld.id_labdoc+'_'+ld.position">
        @include('teacher/missions/_LD', ['is_new_ld' => 0])
    </template>
    <template x-if="rpNS.labdocs_shared.length === 0">
        <div class="ld_draggable_shared empty_drag_area">
        </div>
    </template>
     <template x-for="ld in rpNS.labdocs_shared"
               :key="ld.id_labdoc+'_'+ld.position">
        @include('teacher/missions/_LD', ['is_new_ld' => 0])
    </template>
</div>
