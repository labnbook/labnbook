<?php
/**
 * @var \App\Teacher[] $teachers
 */
 /**
  * @var int $id_teacher
  */
 /**
  * @var \App\Mission $mission
  */

$num_designers = $mission->countActualDesigners();
foreach($teachers as $teacher) {
    if (!$teacher->user->isAdmin()) {
        if ($teacher->teacher_type == 'teacher') {
            $selected_t = 'selected';
            $selected_d = '';
        } else {
            $selected_d = 'selected';
            $selected_t = '';
        }
        if ($teacher->teacher_type != 'designer' || $num_designers > 1) {
            $disabled="";
            $actionDelete="removeLinkedTeacher($teacher->id_teacher, $mission->id_mission)";
            $actionChange="updateLinkedTeacher('$teacher->id_teacher')";
        } else {
            $disabled="disabled";
            $actionDelete="";
            $actionChange="";
        }
    ?>
            <div id="tr_{{$teacher->id_teacher}}" class=" lba_line lba_border_top teacher_line">
                <span>{{$teacher->user_name.' '.$teacher->first_name}}</span>
                <span class="right_part"><select {{$disabled}} onchange="{{$actionChange}}">
                        <option {{$selected_t}} value="teacher">{{__('Tuteur')}}</option>
                        <option {{$selected_d}} value="designer">{{__('Concepteur')}}</option>
                    </select>
                    <i class="far fa-trash-alt lba_icon_default {{$disabled}}" title="{{__("Ne plus associer cet enseignant")}}" alt="{{__("Dissocier")}}" onclick="{{$actionDelete}}"></i></span>
            </div>
            <?php
                }
}
