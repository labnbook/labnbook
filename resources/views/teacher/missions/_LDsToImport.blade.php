<?php
    /**
     * @var int $id_user
     * @var \App\ReportPart $rp
     * @var Collection|Labdocs[] $lds
     */
    use App\Labdocs;
    use Illuminate\Support\Collection;
?>

<div id='import_ld_{{$rp->id_report_part}}_accordion' x-data="{lds_to_import:{{$lds}},chosen_mission:null}" x-init="rpNS.is_show_import=true">
    <template x-if="lds_to_import.length===0">
        <p class="no_lds_msg" x-text="__('Il n y aucun labdoc à importer dans cette partie')"></p>
    </template>
    <template x-if="lds_to_import.length>0">
        <div>
            <div class="header">
                {{__('Importer un labdoc')}}
                <div class="select_mission">
                    <span x-text="'{{__('Depuis la mission :')}}'"></span>
                    <select x-model="chosen_mission">
                        <template x-for="mission_code in lds_to_import.map(item => item.code).filter((value, index, self) => self.indexOf(value) === index)">
                            <option :value="mission_code" x-text="mission_code"></option>
                        </template>
                    </select>
                </div>
            </div>
            <div class="content" x-init="chosen_mission=lds_to_import[0].code">
                <div class="select_ld">
                    <template x-for="ld in lds_to_import.filter(i => i.code === chosen_mission)">
                        <div class="ld_to_import" x-on:click="rpNS.validateImportLD(ld.id_labdoc)" :title="'{{__('Importer ce labdoc dans la partie')}}'">
                            <img :src="`/images/report/ld_type_${ld.type_labdoc}.svg`" width="16">
                            <span x-text="ld.name"></span>
                        </div>
                    </template>
                </div>
            </div>
        </div>
    </template>
</div>
