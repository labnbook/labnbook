<?php
    /**
     * @boolean $is_new_ld 
     */
?>

<div x-data="{ ldNS: labdocCtrl(ld, ld.is_new_ld, ld.is_new_ld, 0) }" 
     x-init="ldNS.init(); if ( ldNS.is_new_ld) { ldNS.contextMenuEditLD();}"
     :id="'ld_' + ldNS.ld.id_labdoc"
     :data-id-ld="ldNS.ld.id_labdoc"
     :class='ldNS.ld.shared ? "ld_draggable_shared ld_item_draggable_shared" : "ld_draggable_report ld_item_draggable_report"'
     x-on:lnb-hide-ld-event.window="if(!ldNS.is_edit_mode){ldNS.is_displayed = false}"
     x-on:lnb-valid-current-edited-ld.window="if(ldNS.is_edit_mode){ldNS.validateEditLD(rpNS)}"
     >
    <div :class="{'ld_edit':ldNS.is_edit_mode}">
        <div class="lba_line lba_subline ld_line">
            <i class="show_ld lba_icon_default" :class="{'fa fa-caret-down':ldNS.is_edit_mode||ldNS.is_displayed,'fa fa-caret-right':!(ldNS.is_edit_mode||ldNS.is_displayed)}"
               x-on:click="if(ldNS.is_edit_mode){return;} ldNS.is_displayed=!ldNS.is_displayed; $nextTick(()=>{ldNS.displayLDContent()});"></i>
            <img class="icon_in_line_ld" :src="`/images/report/ld_type_${ldNS.ld.type_labdoc}.svg`" width="16">
            <span x-on:dblclick="if(!ldNS.is_edit_mode){ldNS.contextMenuEditLD();}"
                    :class="{'ld_name_edit':ldNS.is_edit_mode}">
                <template x-if="!ldNS.is_edit_mode">
                   <span class="rp_ld_title item_handle" :class="{'empty_title':!ldNS.ld.name}"
                         :id="`ld_name_${ldNS.ld.id_labdoc}`" x-text="ldNS.ld.name||__('Nom de labdoc à indiquer')"></span>
                </template>
                <template x-if="ldNS.is_edit_mode">
                    <input :id="`ld_name_${ldNS.ld.id_labdoc}`" name="ld_name"  type="text"
                           x-model="ldNS.ld.name" :value="ldNS.ld.name" x-init="$el.focus();$nextTick(()=>{window.scroll(0,$el.offsetTop-150)})">
                </template>
            </span>
            <template x-if="!ldNS.is_edit_mode">
                <!-- Menu contextuel des labdocs dans la partie -->
                <span class="read context_menu">
                    <span class="lba_icon_edit_ld">
                        <i class="fa fa-ellipsis-v lba_icon_default" ></i>
                    </span>
                    <span class="hover_menu_edition" >
                        <span class="menu_edition_item"
                              x-on:click="ldNS.contextMenuEditLD()" >
                            <i class="fas fa-pencil-alt fa-fw lba_icon_default" title="{{ __("Modifier") }}"></i>
                            {{ __("Modifier") }}
                        </span>
                        <span class="menu_edition_item" x-on:click="rpNS.duplicateLD(ldNS.ld.id_labdoc)">
                            <i class="fa-regular fa-clone fa-fw lba_icon_default" title="{{ __("Dupliquer") }}"></i>
                            {{ __("Dupliquer") }}
                        </span>
                        <span class="menu_edition_item" x-on:click="rpNS.contextMenuRemoveLD(ldNS.ld, ldNS.is_new_ld)">
                            <i class="far fa-trash-alt fa-fw lba_icon_default" title="{{ __("Supprimer") }}"></i>
                            {{ __("Supprimer") }}
                        </span>
                    </span>
                </span>
            </template>
            <template x-if="ldNS.is_edit_mode">
                <span class="edit_menu">
                    <i class="fa fa-check icon_baseline" :id="`ld_edit_validate_${ldNS.ld.id_labdoc}`"
                       x-on:click="if(ldNS.checkIfLDIsValid()) {ldNS.validateEditLD(rpNS);}" 
                       title="{{__("Valider le labdoc")}}"></i>
                    <i x-show="ldNS.is_new_ld" class="fa fa-times icon_baseline" :id="`ld_edit_cancel_${ldNS.ld.id_labdoc}`"
                       x-on:click="ldNS.is_edit_mode=false;rpNS.removeLD(ldNS.ld, 1);" :title="__('Annuler l\'ajout')"></i>
                </span>
            </template>
            <template x-if="ldNS.ld.shared && !ldNS.is_edit_mode">
                <span class="edit_menu">{{__("[Partagé au sein de la classe]")}}</span>
            </template>
        </div>
        <template x-if="ldNS.is_edit_mode">
            <div>
                <template x-if="ldNS.is_new_ld">
                    <div class="student_permission">
                        <input type="checkbox" class="ld_shared_edit_checkbox" :id="`ld_shared_${ldNS.ld.id_labdoc}`" x-model="ldNS.ld.shared" :checked="ldNS.ld.shared">
                        {{ __("Labdoc partagé au sein de la classe") }}
                        &nbsp;-&nbsp;
                        <span class="shared_labdoc_alert">{{__("Attention choix définitif")}}</span>
                        <i :id="`ld_shared_tooltip_${ldNS.ld.id_labdoc}`"
                              class="fa fa-info-circle lba_icon_default ld_shared_tooltip"
                              x-on:mouseover="global_tooltip_popup.openTooltipPopup(__('Labdoc partagé'), __('Quand le labdoc est partagé, tous les étudiants d\'une même classe peuvent modifier un labdoc commun'), '#ld_shared_tooltip_' + ldNS.ld.id_labdoc)"
                              x-on:click="global_tooltip_popup.openTooltipPopup(__('Labdoc partagé'), __('Quand le labdoc est partagé, tous les étudiants d\'une même classe peuvent modifier un labdoc commun'), '#ld_shared_tooltip_' + ldNS.ld.id_labdoc)"
                              >
                        </i>
                    </div>
                </template>
                <div class="student_permission">
                    <template x-if="ldNS.ld.shared && !ldNS.is_new_ld">
                        <span :id="`ld_shared_${ldNS.ld.id_labdoc}`">{{__("Labdoc partagé")}}
                        <i :id="`ld_shared_tooltip_${ldNS.ld.id_labdoc}`"
                              class="fa fa-info-circle lba_icon_default"
                              x-on:mouseover="global_tooltip_popup.openTooltipPopup(__('Labdoc partagé'), __('Quand le labdoc est partagé, tous les étudiants d\'une même classe peuvent modifier un labdoc commun'), '#ld_shared_tooltip_' + ldNS.ld.id_labdoc)"
                              x-on:click="global_tooltip_popup.openTooltipPopup(__('Labdoc partagé'), __('Quand le labdoc est partagé, tous les étudiants d\'une même classe peuvent modifier un labdoc commun'), '#ld_shared_tooltip_' + ldNS.ld.id_labdoc)"
                              >
                        </i>&nbsp;-&nbsp;</span>
                    </template>
                    {{__("Autorisations étudiant")}}{!!__("&nbsp;:")!!}
                    <input type="checkbox" class="fedit_checkbox" :id="`ld_duplicatable_${ldNS.ld.id_labdoc}`" x-model="ldNS.ld.duplicatable" :checked="ldNS.ld.duplicatable">{{ __("Dupliquer") }}
                    <input type="checkbox" class="fedit_checkbox" :id="`ld_editable_${ldNS.ld.id_labdoc}`" x-model="ldNS.ld.editable" :checked="ldNS.ld.editable">{{ __("Modifier") }}
                    <input type="checkbox" class="fedit_checkbox" :id="`ld_editable_name_${ldNS.ld.id_labdoc}`" x-model="ldNS.ld.editable_name" :checked="ldNS.ld.editable_name && !ldNS.ld.shared" :disabled="ldNS.ld.shared == 1">{{ __("Renommer") }}
                    <input type="checkbox" class="fedit_checkbox" :id="`ld_draggable_${ldNS.ld.id_labdoc}`" x-model="ldNS.ld.draggable" :checked="ldNS.ld.draggable && !ldNS.ld.shared" :disabled="ldNS.ld.shared == 1">{{ __("Déplacer") }}
                    <input type="checkbox" class="fedit_checkbox" :id="`ld_deleteable_${ldNS.ld.id_labdoc}`" x-model="ldNS.ld.deleteable" :checked="ldNS.ld.deleteable && !ldNS.ld.shared" :disabled="ldNS.ld.shared == 1">{{ __("Supprimer") }}
                </div>
                <div x-show="isProcedure(ldNS.ld.id_labdoc, 'xml', ldNS.ld.labdoc_data, ldNS.ld.type_labdoc)" class="xml_import_menu">
                    <span onclick="window.open('/data/Copex_manuel_config_XML.pdf', '_blank')" title="Comment configurer le code XML d'un labdoc protocole ?"><i class="fa fa-question-circle icon_baseline"></i></span>
                    <i class="fa fa-download icon_baseline" id="ld_edit_download" x-on:click="ldNS.downloadLD(ldNS.ld.id_labdoc)" title="Télecharger le code XML du labdoc protocole affiché"></i>
                    <i class="fa fa-upload icon_baseline" id="ld_edit_upload" x-on:click="setUploadForm('xml_proc', ldNS.ld.id_labdoc);" title="Définir le labdoc protocole à partir de son code XML"></i>
                </div>
            </div>
        </template>
        <div :class="{'edited_labdoc_assignment': ldNS.is_edit_mode,
                      'lba_write' : ldNS.is_edit_mode,
                      'lnb_tinymce_edit' : ldNS.is_edit_mode_assignment
                      }"
                x-show="ldNS.is_edit_mode"
            >
            <template x-if="ldNS.ld.assignment || ldNS.is_edit_mode_assignment">
                <div class="lb_assignment labdoc_assignment ld_show_content"
                     :class="!ldNS.is_edit_mode_assignment ? 'labdoc_assignment_readonly' : ''"
                     x-on:click="if(!ldNS.is_edit_mode_assignment){ldNS.editLDAssignment();};"
                     :id="`labdoc_assignment_${ldNS.ld.id_labdoc}`"
                     x-text="__('Chargement en cours, veuillez patienter')"
                     >
                </div>
            </template>
            <template x-if="!ldNS.ld.assignment && !ldNS.is_edit_mode_assignment">
                <div class="">
                    <button class="lb_btn "
                            x-on:click="ldNS.is_edit_mode_assignment = true; await $nextTick(); ldNS.editLDAssignment();"
                            >
                            {{__("Ajouter une consigne de labdoc")}}
                    </button>
                </div>
            </template>
        </div>
        <template x-if="ldNS.ld.assignment && !ldNS.is_edit_mode && show_ld_assignment_ro"
                  x-data="{show_ld_assignment_ro: false}"
                  x-init="$watch('rpNS.is_show_assignments', function(value) {show_ld_assignment_ro = value; displayText(ldNS.ld.id_labdoc, 0, 1)})"
                  >
            <div class="lb_assignment labdoc_assignment ld_show_content"
                  :id="`labdoc_assignment_${ldNS.ld.id_labdoc}`"
                >
                <i class="fa fa-times lb_assignment_close" x-on:click="show_ld_assignment_ro = false" title="{{ __("Fermer les consignes") }}"></i>
                <span
                     x-html="ldNS.ld.assignment"
                     >
                </span>
            </div>
        </template>
        <div :class="{'edited_labdoc_content lba_write':ldNS.is_edit_mode,
                'edited_ld_procedure':ldNS.ld.type_labdoc==='procedure' && ldNS.is_edit_mode,
              'edited_ld_dataset':ldNS.ld.type_labdoc==='dataset' && ldNS.is_edit_mode,
              'lnb_tinymce_edit':ldNS.ld.type_labdoc==='text' && ldNS.is_edit_mode,
              'edited_ld_code':ldNS.ld.type_labdoc==='code' && ldNS.is_edit_mode
            }">
            <div class="ld_show_content"
                 :class="{'labdoc_content':!ldNS.is_edit_mode,
                 'tinyMCE-text ld_text':ldNS.ld.type_labdoc==='text' && !ldNS.is_edit_mode,
                 'ld_procedure':ldNS.ld.type_labdoc==='procedure' && !ldNS.is_edit_mode,
                 'ld_dataset':ldNS.ld.type_labdoc==='dataset' && !ldNS.is_edit_mode,
                 'ld_drawing':ldNS.ld.type_labdoc==='drawing' && !ldNS.is_edit_mode,
                 'ld_code':ldNS.ld.type_labdoc==='code' && !ldNS.is_edit_mode}"
                 x-show="ldNS.is_displayed"
                 x-on:dblclick="if(!ldNS.is_edit_mode){ldNS.contextMenuEditLD();}" 
                 :id="`labdoc_content_${ldNS.ld.id_labdoc}`"></div>
        </div>
    </div>
</div>
