<?php
    /**
     * @var \App\Mission[] $mission
     */
     /**
     * @var \App\Mission $mission
     */
     /**
      * @var \App\Labdoc[] $teacherLabdocs
      */
?>
@extends('layouts.labnbook')

@section('title', __('export CSV'))

@section('head')
@endsection

@section('body')
    <h1>Export CSV</h1>

    <?php if (!extension_loaded('zip')) : ?>
        <div class="alert alert-danger">
            Le serveur PHP est mal configuré, la création d'une archive zip est impossible.
            Contactez l'administrateur du système.
        </div>
    <?php endif ?>

    <h2>Choix de la mission</h2>
    <ul>
        <?php
            foreach ($missions as $m) {
                echo "<li>"
                    . '<a href="?mission=' . $m['id_mission'] . '">' . htmlspecialchars("{$m['name']} ({$m['code']})") . "</a>"
                    . "</li>\n";
            }
        ?>
    </ul>

    <?php if ($mission) :?>
    <h2>Mission <?= htmlspecialchars("{$mission->name} ({$mission->code})") ?></h2>
    <h3>Choix du <em>labdoc</em>-modèle</h3>
    <ul>
        <?php
            Log::Debug($teacherLabdocs);
            foreach ($teacherLabdocs as $l) {
                echo "<li>"
                    . '<a href="?mission=' . $mission->id_mission . '&amp;labdoc=' . $l['id_labdoc'] . '">' . htmlspecialchars($l['name']) . "</a>"
                    . "</li>\n";
            }
        ?>
    </ul>
    <?php endif ?>
@endsection
