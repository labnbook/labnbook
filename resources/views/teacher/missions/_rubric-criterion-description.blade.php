<div class="criterion-description-wrapper">
    <div class="controls-wrapper">
        <i class="fa-regular fa-clone" :title="rubNS.getInformation('duplicateCriterionButton', 'criterion')"
           x-on:click="rubNS.addCriterion(criterion, group, true); rubNS.rubric.hasChanged = true"></i>
        <i class="fas fa-up-down-left-right criterion-handle" :title="rubNS.getInformation('moveCriterionButton', 'criterion')"></i>
        <i class="fas fa-arrow-right-arrow-left" :title="rubNS.getInformation('toggleCriterionTypeButton', 'criterion', criterion)"
           x-show="criterion.descrobs.length > 0"
           x-on:click="rubNS.toggleCriterionType(criterion); rubNS.rubric.hasChanged = true"></i>
        <i class="far fa-trash-alt" :title="rubNS.getInformation('deleteCriterionButton', 'criterion')"
           x-on:click="rubNS.deleteCriterion(criterion); rubNS.rubric.hasChanged = true"></i>
    </div>
    <div class="description-wrapper" x-data="{ openCriterionDescriptionTextArea: false }"
         :class="{ 'rubric-invalid-criterion-text': criterion.invalidText }">
        <div class="description-content" x-show="!(openCriterionDescriptionTextArea || criterion.invalidText)"
             x-on:click="openCriterionDescriptionTextArea = true; rubNS.rubric.hasChanged = true;
                         $nextTick(() => { $root.querySelector(`[x-ref='textarea_criterion_${criterion.id_criterion}']`).focus(); });"
             :title="rubNS.getInformation('changeCriterionDescriptionButton', 'criterion')">
            <span x-text="criterion.description" :title="criterion.description"></span>
        </div>
        <div class="description-editor" x-show="openCriterionDescriptionTextArea || criterion.invalidText"
             x-on:click.outside.stop="openCriterionDescriptionTextArea = false; rubNS.rubric.hasChanged = true;
                                      rubNS.check('invalidText', 'criterion', criterion)">
            <template x-if="criterion.invalidText">
                <i class="fas fa-circle-exclamation input-error" :title="rubNS.getInformation('criterionErrorHover', 'criterion')"
                   x-on:click="global_tooltip_popup.openTooltipPopup('{{ __("Intitulé invalide") }}', rubNS.getInformation('criterionDescriptionErrorIcon', 'criterion'), $el)"></i>
            </template>
            <textarea type="text" x-model.lazy="criterion.description" :title="rubNS.getInformation('criterionTextareaHover', 'criterion')"
                      :x-ref="'textarea_criterion_' + criterion.id_criterion" lang="{{ $lang }}" spellcheck="true"
                      :placeholder="rubNS.getInformation('criterionDescriptionPlaceholder', 'criterion', criterion)"
                      x-on:focus="$el.style.height = $el.scrollHeight + 'px'; rubNS.rubric.hasChanged = true"></textarea>
        </div>
    </div>
    <div class="weight-wrapper" x-data="{ openCriterionWeightInput: false }"
         :class="{ 'rubric-invalid-criterion-value': criterion.invalidValue }">
        <div class="weight-content" x-show="!(openCriterionWeightInput || criterion.invalidValue)"
             x-on:click="openCriterionWeightInput = true; rubNS.rubric.hasChanged = true;
                         $nextTick(() => { $root.querySelector(`[x-ref='input_criterion_${criterion.id_criterion}']`).focus(); });">
            <i class="fas fa-weight-hanging" :title="rubNS.getInformation('changeCriterionWeightButton', 'criterion')"></i>
            <span x-text="rubNS.displayValueOrComputedValue(criterion)" :title="rubNS.getInformation('changeCriterionWeightButton', 'criterion')"></span>
        </div>
        <div class="weight-editor" x-show="openCriterionWeightInput || criterion.invalidValue"
             x-on:click.outside.stop="openCriterionWeightInput = false; rubNS.rubric.hasChanged = true;
                                      rubNS.checkAndUpdateValue('invalidValue', 'criterion', criterion)">
            <template x-if="criterion.invalidValue">
                <i class="fas fa-circle-exclamation input-error" :title="rubNS.getInformation('criterionErrorHover', 'criterion')"
                   x-on:click="global_tooltip_popup.openTooltipPopup('{{ __("Poids invalide") }}', rubNS.getInformation('criterionWeightErrorIcon', 'criterion', criterion), $el)"></i>
            </template>
            <i class="fas fa-weight-hanging"></i>
            <input type="text" lang="{{ $lang }}" :title="rubNS.getInformation('criterionInputHover', 'criterion')"
                   placeholder="auto" x-model.lazy="criterion.relative_weight"
                   :x-ref="'input_criterion_' + criterion.id_criterion"
                   x-on:keyup.enter.stop="openCriterionWeightInput = false; rubNS.rubric.hasChanged = true;
                                          rubNS.checkAndUpdateValue('invalidValue', 'criterion', criterion)">
        </div>
    </div>
</div>
