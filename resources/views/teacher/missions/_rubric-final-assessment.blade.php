<td class="bonus-malus-title" :colspan="Math.floor(rubNS.viewConfig.columnsNumber / 2) - 1">
    <div class="bonus-malus-label">{{ __("Bonus / malus autorisé entre") }}{!! __("&nbsp;: ") !!}</div>
    <div class="bonus-malus-score-wrapper">
        <div class="bonus-malus-min-score-wrapper"
             x-data="{ openBonusMalusMinScoreInput: false }"
             :class="{ 'rubric-invalid-min-bonus-value': rubNS.rubric.minBonusInvalidValue }">
            <div class="bonus-malus-min-score-content" x-show="!(openBonusMalusMinScoreInput || rubNS.rubric.minBonusInvalidValue)"
                 x-on:click="openBonusMalusMinScoreInput = true; rubNS.rubric.hasChanged = true;
                             $nextTick(() => { $root.querySelector(`[x-ref='input_min_bonus_${rubric.id_rubric}']`).focus(); });">
                <span x-text="rubNS.displayValue(rubNS.rubric.min_bonus, true)" :title="rubNS.getInformation('changeMinBonusButton', 'rubric')"></span>
            </div>
            <div class="bonus-malus-min-score-editor" x-show="openBonusMalusMinScoreInput || rubNS.rubric.minBonusInvalidValue"
                 x-on:click.outside.stop="openBonusMalusMinScoreInput = false; rubNS.rubric.hasChanged = true;
                                          rubNS.checkAndUpdateValue('minBonusInvalidValue', 'rubric', rubNS.rubric)">
                <template x-if="rubNS.rubric.minBonusInvalidValue">
                    <i class="fas fa-circle-exclamation input-error" :title="rubNS.getInformation('minBonusErrorHover', 'rubric')"
                       x-on:click="global_tooltip_popup.openTooltipPopup('{{ __("Malus minimal invalide") }}', rubNS.getInformation('minBonusErrorIcon', 'rubric'), $el)"></i>
                </template>
                <input type="text" lang="{{ $lang }}" :title="rubNS.getInformation('minBonusInputHover', 'rubric')"
                       x-model.lazy="rubNS.rubric.min_bonus"
                       :x-ref="'input_min_bonus_' + rubric.id_rubric"
                       x-on:keyup.enter.stop="openBonusMalusMinScoreInput = false; rubNS.rubric.hasChanged = true;
                                              rubNS.checkAndUpdateValue('minBonusInvalidValue', 'rubric', rubNS.rubric)">
            </div>
        </div>

        <div class="bonus-separator">{{ __("et") }}</div>

        <div class="bonus-malus-max-score-wrapper"
             x-data="{ openBonusMalusMaxScoreInput: false }"
             :class="{ 'rubric-invalid-max-bonus-value': rubNS.rubric.maxBonusInvalidValue }">
            <div class="bonus-malus-max-score-content" x-show="!(openBonusMalusMaxScoreInput || rubNS.rubric.maxBonusInvalidValue)"
                 x-on:click="openBonusMalusMaxScoreInput = true;
                             $nextTick(() => { $root.querySelector(`[x-ref='input_max_bonus_${rubric.id_rubric}']`).focus(); });">
                <span x-text="rubNS.displayValue(rubNS.rubric.max_bonus, true)" :title="rubNS.getInformation('changeMaxBonusButton', 'rubric')"></span>
            </div>
            <div class="bonus-malus-max-score-editor" x-show="openBonusMalusMaxScoreInput || rubNS.rubric.maxBonusInvalidValue"
                 x-on:click.outside.stop="openBonusMalusMaxScoreInput = false; rubNS.rubric.hasChanged = true;
                                          rubNS.checkAndUpdateValue('maxBonusInvalidValue', 'rubric', rubNS.rubric)">
                <template x-if="rubNS.rubric.maxBonusInvalidValue">
                    <i class="fas fa-circle-exclamation input-error" :title="rubNS.getInformation('maxBonusErrorHover', 'rubric')"
                       x-on:click="global_tooltip_popup.openTooltipPopup('{{ __("Bonus maximal invalide") }}', rubNS.getInformation('maxBonusErrorIcon', 'rubric'), $el)"></i>
                </template>
                <input type="text" lang="{{ $lang }}" :title="rubNS.getInformation('maxBonusInputHover', 'rubric')"
                       x-model.lazy="rubNS.rubric.max_bonus"
                       :x-ref="'input_max_bonus_' + rubric.id_rubric"
                       x-on:keyup.enter.stop="openBonusMalusMaxScoreInput = false; rubNS.rubric.hasChanged = true;
                                              rubNS.checkAndUpdateValue('maxBonusInvalidValue', 'rubric', rubNS.rubric)">
            </div>
        </div>
    </div>
</td>

<td class="final-assessment-score" :colspan="Math.ceil(rubNS.viewConfig.columnsNumber / 2) + 1">
    <div>{{ __("Note totale sur") }}{!! __("&nbsp;:") !!}</div>
    <div class="final-assessment-wrapper" x-data="{ openFinalAssessmentInput: false }"
         :class="{ 'rubric-invalid-grading-value': rubNS.rubric.gradingInvalidValue }">
        <div class="final-assessment-content" x-show="!(openFinalAssessmentInput || rubNS.rubric.gradingInvalidValue)"
             x-on:click="openFinalAssessmentInput = true; rubNS.rubric.hasChanged = true;
                         $nextTick(() => { $root.querySelector(`[x-ref='input_final_assessment_${rubric.id_rubric}']`).focus(); });">
            <span x-text="rubNS.displayValue(rubNS.rubric.grading)" :title="rubNS.getInformation('changeGradingButton', 'rubric')"></span>
        </div>
        <div class="final-assessment-editor" x-show="openFinalAssessmentInput || rubNS.rubric.gradingInvalidValue"
             x-on:click.outside.stop="openFinalAssessmentInput = false; rubNS.rubric.hasChanged = true;
                                      rubNS.checkAndUpdateValue('gradingInvalidValue', 'rubric', rubNS.rubric)">
            <template x-if="rubNS.rubric.gradingInvalidValue">
                <i class="fas fa-circle-exclamation input-error" :title="rubNS.getInformation('gradingErrorHover', 'rubric')"
                   x-on:click="global_tooltip_popup.openTooltipPopup('{{ __("Barême invalide") }}', rubNS.getInformation('gradingErrorIcon', 'rubric'), $el)"></i>
            </template>
            <input type="text" required lang="{{ $lang }}" :title="rubNS.getInformation('gradingInputHover', 'rubric')"
                   x-model.lazy="rubNS.rubric.grading"
                   :x-ref="'input_final_assessment_' + rubric.id_rubric"
                   x-on:keyup.enter.stop="openFinalAssessmentInput = false; rubNS.rubric.hasChanged = true;
                                          rubNS.checkAndUpdateValue('gradingInvalidValue', 'rubric', rubNS.rubric)">
        </div>
    </div>
</td>
