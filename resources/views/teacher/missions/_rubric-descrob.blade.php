<div class="criterion-descrob-wrapper">
    <div class="controls-wrapper">
        <i class="fas fa-up-down-left-right descrob-handle" :title="rubNS.getInformation('moveDescrobButton', 'descrob', null, criterion)" 
           x-show="criterion.descrobs.length > 1"></i>
        <i class="far fa-trash-alt" :title="rubNS.getInformation('deleteDescrobButton', 'descrob', null, criterion)"
           x-on:click="rubNS.deleteDescrOb(descrob); rubNS.rubric.hasChanged = true"></i>
    </div>
    <div class="description-wrapper" x-data="{ openDescrobDescriptionTextArea: false }"
         :class="{ 'rubric-invalid-descrob-text': descrob.invalidText }">
        <div class="description-content" x-show="!(openDescrobDescriptionTextArea || descrob.invalidText)"
             x-on:click="openDescrobDescriptionTextArea = true; rubNS.rubric.hasChanged = true;
                         $nextTick(() => { $root.querySelector(`[x-ref='textarea_descrob_${descrob.id_descrob}']`).focus(); });"
             :title="rubNS.getInformation('changeDescrobDescriptionButton', 'descrob', null, criterion)">
            <template x-if="criterion.type === 'ob'">
                <i class="far fa-square" :title="descrob.description"></i>
            </template>
            <span x-text="descrob.description" :title="descrob.description"></span>
        </div>
        <div class="description-editor" x-show="openDescrobDescriptionTextArea || descrob.invalidText"
             x-on:click.outside.stop="openDescrobDescriptionTextArea = false; rubNS.rubric.hasChanged = true;
                                      rubNS.check('invalidText', 'descrob', descrob)">
            <template x-if="descrob.invalidText">
                <i class="fas fa-circle-exclamation input-error" :title="rubNS.getInformation('descrobErrorHover', 'descrob')"
                   x-on:click="global_tooltip_popup.openTooltipPopup('{{ __("Intitulé invalide") }}', rubNS.getInformation('descrobDescriptionErrorIcon', 'descrob', null, criterion), $el)"></i>
            </template>
            <textarea type="text" x-model.lazy="descrob.description" :title="rubNS.getInformation('descrobTextareaHover', 'descrob', null, criterion)"
                      :x-ref="'textarea_descrob_' + descrob.id_descrob" lang="{{ $lang }}" spellcheck="true"
                      :placeholder="rubNS.getInformation('descrobDescriptionPlaceholder', 'descrob', descrob, criterion)"
                      x-on:focus="$el.style.height = $el.scrollHeight + 'px'; rubNS.rubric.hasChanged = true"></textarea>
        </div>
    </div>
    <div class="weight-wrapper" x-data="{ openDescrobWeightInput: false }"
         :class="{ 'rubric-invalid-descrob-value': descrob.invalidValue }">
        <div class="weight-content" x-show="!(openDescrobWeightInput || descrob.invalidValue) && criterion.relative_weight !== 0"
             x-on:click="openDescrobWeightInput = true; rubNS.rubric.hasChanged = true;
                         $nextTick(() => { $root.querySelector(`[x-ref='input_descrob_${descrob.id_descrob}']`).focus(); })">
            <i class="fas fa-weight-hanging" :title="rubNS.getInformation('changeDescrobWeightButton', 'descrob', null, criterion)"></i>
            <span x-text="rubNS.displayValue(descrob.value)" :title="rubNS.getInformation('changeDescrobWeightButton', 'descrob', null, criterion)"></span>
        </div>
        <div class="weight-editor" x-show="(openDescrobWeightInput || descrob.invalidValue) && criterion.relative_weight !== 0"
             x-on:click.outside.stop="openDescrobWeightInput = false; rubNS.rubric.hasChanged = true;
                                      rubNS.checkAndUpdateValue('invalidValue', 'descrob', descrob);
                                      rubNS.checkDescriptorValue(criterion)">
            <template x-if="descrob.invalidValue">
                <i class="fas fa-circle-exclamation input-error" :title="rubNS.getInformation('descrobErrorHover', 'descrob')"
                   x-on:click="global_tooltip_popup.openTooltipPopup('{{ __("Poids invalide") }}', rubNS.getInformation('descrobWeightErrorIcon', 'descrob', descrob, criterion), $el)"></i>
            </template>
            <i class="fas fa-weight-hanging"></i>
            <input type="text" lang="{{ $lang }}" :title="rubNS.getInformation('descrobInputHover', 'descrob', null, criterion)"
                   placeholder="{{ __('Poids') }}" x-model.lazy="descrob.value"
                   :x-ref="'input_descrob_' + descrob.id_descrob"
                   x-on:keyup.enter.stop="openDescrobWeightInput = false; rubNS.rubric.hasChanged = true;
                                          rubNS.checkAndUpdateValue('invalidValue', 'descrob', descrob);
                                          rubNS.checkDescriptorValue(criterion)">
        </div>
    </div>
</div>
