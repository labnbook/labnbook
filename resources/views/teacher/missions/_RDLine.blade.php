<?php
    /**
     * @var \App\Resource $ressource
    */
    /**
     * @var string $path
    */
    /**
     * @var string $ressource_type
    */
    if ($ressource_type == "image") {
        $res_icon = "image";
        $color = "#915562";
    } else if ($ressource_type == "pdf") {
        $res_icon = "file-pdf";
        $color = "#DB0000";
    } else if ($ressource_type == "link") {
        $res_icon = "link";
        $color = "steelblue";
    } else {
        $res_icon = "file";
        $color = "dimgrey";
    }
    $href = $path.$ressource->res_path;
    $ressource->href = $href;
?>


<div id="rd_{{$ressource->id_ressource}}" 
     x-data="{rd:{{$ressource}},in_edit_mode:false}"
     data-id-rd="{{$ressource->id_ressource}}"
     class="rd_item_draggable rd_draggable item_handle" type="{{$ressource_type}}">
    
    <template x-if="!in_edit_mode">
        <div class="lba_line lba_border_top rd_line" :data-rd-href="rd.href">
            <span id="rd_url_{{$ressource->id_ressource}}">
            <i class="icon_in_line fa fa-{{$res_icon}}" style="color: {{$color}}"></i>
            <span class="item_handle" id="rd_name_{{$ressource->id_ressource}}" x-text="rd.name"></span>
            </span>
            <!-- Menu contextuel des documents ressource -->
            <span id="rd_context_menu" class="read context_menu">
                <i x-on:click="window.open(resUrl(rd))" class="far fa-eye fa-fw lba_icon_default" title="{{ __("Afficher") }}"></i>
                <i x-on:click="in_edit_mode=true" class="fas fa-pencil-alt fa-fw lba_icon_default" title="{{ __("Modifier") }}"></i>
                <i x-on:click="removeRD(rd)" class="far fa-trash-alt fa-fw lba_icon_default" title="{{ __("Supprimer") }}"></i>
            </span>
        </div>   
    </template>
    <template x-if="in_edit_mode">
        <div :id="`bloc_rd_form_${rd.id_ressource}`" class="form_edit lba_border">
            <div class="title">
                <span x-text="__('Modification d\'un lien')"></span>
                <i class="fa fa-check icon_baseline" :title="__('Valider la modification')" alt="Ajouter" x-on:click="in_edit_mode=await updateRD(rd);"></i>
                <i class="fa fa-times icon_baseline" :title="__('Annuler')" alt="Annuler" x-on:click="in_edit_mode=false"></i>
            </div>
            <div>
                <span class="input_label" x-text="__('Nom')"></span>
                <input type="text" :id="`rd_edit_name_${rd.id_ressource}`" class="lba_write" :name="`rd_edit_name_${rd.id_ressource}`" :value="rd.name" x-model="rd.name"/>
                
                <span :id="`block_rd_url${rd.id_ressource}`" x-show="rd.res_type==='url'">
                    <span class="input_label" x-text="__('Adresse')"></span>
                    <input :id="`rd_edit_url_${rd.id_ressource}`" class="lba_write rd_url" :name="`rd_edit_url_${rd.id_ressource}`" :value="rd.res_path" x-model="rd.res_path"/>
                </span>
                
                <span :id="`block_rd_file${rd.id_ressource}`" x-show="rd.res_type==='file'">
                    <span class="input_label" x-text="__('Fichier')"></span>
                    <span class="fa fa-paperclip fa-lg lba_icon_default" :title="__('Sélectionner un nouveau fichier (pdf ou image)')" alt="Sélectionner un fichier" x-on:click="selectRDFile(rd.id_ressource);" ></span> 
                    <span :id="`rd_add_feedback_upload_${rd.id_ressource}`" x-text="rd.file_name"></span>
                </span>
            </div>
        </div>
    </template>
</div>


