<?php
    /**
     * @var \App\ReportPart $rp
     */
     $rp->containsLdAssignment = $rp->hasLdAssignment();
?>
<div id="rp_{{$rp->id_report_part}}" class="lba_rp item_draggable part_draggable"
     x-data="{ rpNS: reportPartCtrl({{$rp}},false,false,false), new_rp:false}" 
     data-id-rp="{{$rp->id_report_part}}" data-position="{{$rp->position}}">
    @include('teacher/missions/_RPLine')
    <div class="ld_draggable" style="height: 0;"></div>
    <!-- Les labdocs -->
    @include('teacher/missions/_LDs', ['rp' => $rp])
</div>
