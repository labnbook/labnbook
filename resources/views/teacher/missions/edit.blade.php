<?php
    /**
     * @var \App\User $user
     */
    /** @var \App\Mission $current_mission  */
    /** @var \App\Helper $helper */
 ?>
@extends('layouts.labnbook')


@section('title', __("Édition d'une mission"))

@section('head')
    <script>
		<!-- Variable transmises par PHP au javascript -->
        window.global_current_id_user    =  {{ $user->id_user }};
        window.global_id_mission =  {{ $current_mission->id_mission }};
        window.global_tab_ld = {!!  json_encode($current_mission->fetchAllLabdocs())  !!};
	</script>

	<!-- Styles et fonctions de la page -->
	<?= $helper->loadAsset("/css/teacher/edit_mission.css") ?>
	<?= $helper->loadAsset("/js/teacher/missions_edit.js") ?>

	<!-- TinyMCE -->
	<?= $helper->loadAsset("/libraries/tinymce/tinymce.min.js") ?>

	<?=  $helper->loadAsset("/libraries/katex/katex.min.js") ?>
	<?=  $helper->loadAsset("/libraries/katex/contrib/mhchem.min.js") ?>
	<?=  $helper->loadAsset("/libraries/katex/katex.min.css") ?>

    <?= $helper->loadAsset("/libraries/sortable/Sortable.min.js"); ?>

	<!-- COPEX -->
	<?= $helper->loadAsset("/tool_copex/v1_xml/copex.css") ?>
    <?= $helper->loadAsset("/tool_copex/v1_xml/jstree/jstree_pre1.0_fix_1/jquery.jstree.js", "async") ?>
    <?= $helper->loadAsset("/tool_copex/v1_xml/copex_model.js", "async") ?>
    <?= $helper->loadAsset("/tool_copex/v1_xml/copex_view.js", "async") ?>
    <?= $helper->loadAsset("/tool_copex/v1_xml/copex_control.js", "async") ?>
    <?= $helper->loadAsset("/tool_copex/v2_json/copex.css") ?>
	<?= $helper->loadAsset("/tool_copex/v2_json/copex.js") ?>
	<?= $helper->loadAsset("/tool_copex/v2_json/simulation.js") ?>
	<?= $helper->loadAsset("/tool_copex/v2_json/titrab/titrab.js") ?>
	<?= $helper->loadAsset("/tool_copex/v2_json/dosage-spectro/dosage-spectro.js") ?>
	<?= $helper->loadAsset("/tool_copex/v2_json/dosage-spectro/dosage-spectro.css") ?>

	<!-- FITEX -->
	<?= $helper->loadAsset("/tool_fitex/fitex.css") ?>
    <?= $helper->loadAsset("/libraries/mathjs/math.min.js") ?>
    <?= $helper->loadAsset("/libraries/ml-levenberg-marquardt/ml-levenberg-marquardt.js") ?>
	<?= $helper->loadAsset("/tool_fitex/libraries/jquery.flot.all.min.js") ?>
    <?= $helper->loadAsset("/tool_fitex/libraries/jquery.contextMenu.min.js") ?>
    <?= $helper->loadAsset("/tool_fitex/libraries/jquery.contextMenu.css") ?>
    <?= $helper->loadAsset("/tool_fitex/fitex_view.js") ?>
    <?= $helper->loadAsset("/tool_fitex/fitex_control.js") ?>
    <?= $helper->loadAsset("/tool_fitex/fitex_model.js") ?>
    <?= $helper->loadAsset("/libraries/js-xlsx/xlsx.full.min.js") ?>

	<!-- ZWIBBLER -->
	<?= $helper->loadAsset("/tool_zwibbler/zwibbler.css") ?>
	<?= $helper->loadAsset("/tool_zwibbler/zwibbler2.js") ?>
	<?= $helper->loadAsset("/tool_zwibbler/zwibbler_fn_for_lb.js") ?>

	<!--Styles et fonctions communes aux pages Enseignants -->
	<?= $helper->loadAsset("/css/teacher/common.css") ?>
	<?= $helper->loadAsset("/js/teacher/authoring.js") ?>

	<!-- AllTools -->
	<?= $helper->loadAsset("/js/labdoc.js", "async") ?>
    
    <!-- labdocs code -->
    <?= $helper->loadAsset("/css/labdoc_code.css") ?>

    <script>
		$(function(){
			setInterval('autoSaveLD()', 10000);
		});
        $(function(){
            setInterval('autoSaveRubric()', 20000);
        });
	</script>

@endsection


@section('body')
<div id="wrapper">
    @include('teacher/_menu', [
        'page_name' => request()->path(),
        'refresh_period' => config('labnbook.refresh_period'),
        'mission' => $current_mission,
        ])

        <div id="wrapper-content">
            <!-- ------------------- BLOC PARAMETRES DE LA MISSION ------------------- -->
            <div class="read bloc_lb_mission_edit" id="at_rep_param">
                <div class="edit-mission-title" onclick="toggleSection('#at_rep_param', '#params')">
                    <i class="fa fa-caret-right toggle-section"></i>&nbsp;{{ __("Paramètres généraux de la mission") }}
                </div>
                <span id='params' style='display:none'>

                <!-- Zone code de la mission -->
                <div id="bloc_m_code">
                    <label for="m_code" class="edit-mission-header">{{ __("Code") }}</label>&nbsp;&nbsp;<span class="label_small">{{"(3-16 ".__("caractères").")" }}</span><br>
                    <input id="m_code" maxlength="16" class="bordure ligne" type="text" value="<?= htmlspecialchars($current_mission->code) ?>" onchange="updateInput('code','code',3,16);" tabindex=1>
                    <span id="m_code_save" class="hide"><?= htmlspecialchars($current_mission->code) ?></span>
                </div>

                <!-- Zone nom de la mission -->
                <div id="bloc_m_name">
                    <label for="m_name" class="edit-mission-header">{{ __("Nom") }}</label>&nbsp;&nbsp;<span class="label_small">{{ "(3-64 ".__("caractères").")" }}</span><br>
                    <input id="m_name" maxlength="64" class="bordure ligne" type="text"  value="<?= htmlspecialchars($current_mission->name) ?>" onchange='updateInput("name","nom",3,64);' tabindex=2>
                    <span id="m_name_save" class="hide"><?= htmlspecialchars($current_mission->name) ?></span>
                </div>

                <!-- Zone statut de la mission -->
                <div id="bloc_m_status">
                    <label for="m_status" class="edit-mission-header">{{ __("Visibilité") }}</label><br>
                    <select id="m_status" name="m_status" class="ligne" onchange="updateStatus();">
                        <option value="private">{{ __("Privé") }}</option>
                        <option value="public"  <?php if(in_array($current_mission->status, ["public", "tutorial"])) echo "selected" ?> >{{ __("Public") }}</option>
                    </select>
                </div>

                <!-- Zone enseignants associes -->
                <div id="mi-linked-teachers" class="mi-bloc">
                    <label class="edit-mission-header">{{ __("Enseignant(s) associé(s)") }}&nbsp;</label> <i id="teachers_tooltip" class="fa fa-info-circle lba_icon_default" onclick="messageLinkedTeacher('click');"></i>
                    <div id="linked_teachers">
                        @include('teacher/missions/_linkedTeachers',[
                            'teachers' => $current_mission->getLinkedTeachers(),
                            'id_teacher' => $user->id_user,
                            'mission' => $current_mission,
                            ])
                    </div>
                    <!-- on teste l'ajout par champ libre -->
                    <div id="bloc_teacher_add" class=" lba_line bordure">
                        <?php if (!$user->teacher->isManager()) :?>
                            <span class="place_holder_text">{{__("Associer un enseignant")}}{!!__("&nbsp;:")!!}</span>
                            <x-select2 id="mi-add-teacher" on-select-function="linkTeacher(selectedElement)"></x-select2>
                        <?php endif ?>
                        <?php if ($user->teacher->isManager()) :?>
                            <span class="place_holder_text">{{__("Associer un/des enseignants")}}{!!__("&nbsp;:")!!}</span>
                            <x-select2 id="mi-add-teacher" on-select-function="linkTeacher(selectedElement)"></x-select2>
                        <?php endif ?>
                    </div>
                </div>

                <!-- Zone texte de presentation -->
                <div id="bloc_lba_description" class="mi-bloc">
                    <div id="bloc_lba_description_read">
                        <label class="edit-mission-header">{{ __("Texte de présentation") }}</label>&nbsp;&nbsp;<span class="label_small">{{ __("(ce texte apparait sur la page de choix des missions)") }}</span>
                        <div id="lba_description_read" class="bordure tinyMCE-text" title="{{ __("Cliquer pour modifier le texte de présentation") }}" onclick='editContent("description");'>
                            {!!$current_mission->description!!}
                        </div>
                    </div>
                    <div id="bloc_lba_description_edit" class="bordure hide">
                        <div class="title">
                            <span>{{ __("Édition du texte de présentation") }}</span>
                            <i class="fa fa-check icon_baseline" title="{{ __("Enregistrer et quitter l'édition") }}" onclick='validContent("description");'></i>
                            <i class="fa fa-times icon_baseline" title="{{ __("Annuler et quitter l'édition") }}" onclick='cancelContent("description");'></i>
                        </div>
                        <div>
                            <div id="lba_description_edit"></div>
                        </div>
                    </div>
                </div>
                </span>

            </div>

            <!-- ------------------- BLOC RESSOURCES DE LA MISSION ------------------- -->
            <div class="read bloc_lb_mission_edit" id="at_rep_res">
                <div class="edit-mission-title" onclick="toggleSection('#at_rep_res', '#resources')">
                    <i class="fa fa-caret-right toggle-section"></i>&nbsp;{{ __("Consignes et documents accessibles dans l'outil 'Ressources'") }}&nbsp;&nbsp;<i class="fa fa-folder-open"></i></div>
                <span style="display:none" id="resources">
                <!-- Zone consigne générale -->
                <div id="bloc_lba_assignment">
                    <div id="bloc_lba_assignment_read">
                        <label class="edit-mission-header">{{ __("Consigne générale") }}</label>&nbsp;&nbsp;<span class="label_small">{{ __("(si ce cadre est vide, le texte de présentation défini dans la section précédente sera utilisé comme consigne générale)") }}</span>
                        <div id="lba_assignment_read" class="bordure tinyMCE-text" title="{{ __("Cliquer pour modifier la consigne générale") }}" onclick='editContent("assignment");'>
                            {!!$current_mission->assignment!!}
                        </div>
                    </div>
                    <div id="bloc_lba_assignment_edit"  class="bordure hide">
                        <div class="title">
                            <span>{{ __("Édition de la consigne courte") }}</span>
                            <i class="fa fa-check icon_baseline" title="{{ __("Enregistrer et quitter l'édition") }}" onclick='validContent("assignment");'></i>
                            <i class="fa fa-times icon_baseline" title="{{ __("Annuler et quitter l'édition") }}" onclick='cancelContent("assignment");'></i>
                        </div>
                        <div>
                            <div id="lba_assignment_edit"></div>
                        </div>
                    </div>
                </div>

                <!-- Zone consigne détaillée -->
                
                <?php
                    $assignment = $current_mission->resources()->where('res_type', 'assignment')->first();
                if ($assignment) {
                    $is_assignment = true ;
                    $assign_name = $assignment->name ;
                    $assign_path = storage_path('app/public')."/missions/".$current_mission->id_mission."/resources/".$assignment->res_path ;
                    $id_ressource = $assignment-> id_ressource ;
                }
                else {
                    $is_assignment = false ;
                    $assign_name = "" ;
                    $assign_path = "" ;
                    $id_ressource = "" ;
                }
                ?>
        <div id="bloc_res_la" class="mi-bloc">
            <label class="edit-mission-header">{{ __("Consigne détaillée") }}</label> <span class="label_small">{{ __("(fichier pdf)") }}</span> :
            <span id="lba_add_assignment" class="fa fa-paperclip fa-lg lba_icon_default" title="{{ __("Ajouter une consigne détaillée (fichier pdf)") }}" onclick="setUploadForm('assignment','consigne_longue');" <?php if ($is_assignment) { echo 'style="display:none"' ; }?>>
            <i class="fa fa-spinner fa-pulse" style="display:none"></i>
            </span>
            <span id="lba_assignment_link" <?= !$is_assignment ? 'style="display:none"' : ''?>>
                <a title="{{ __("Ouvrir la consigne détaillée") }}" target="_blank" href="<?php echo $assign_path ?>">
                    <i>{{ $is_assignment ? $assignment->res_path : '' }}</i></a>&nbsp;&nbsp;<i class="far fa-trash-alt icon_baseline" title="{{ __("Supprimer la consigne détaillée") }}" onclick="removeLongAssi({{$id_ressource}});"></i>
            </span>
        </div>
        
       <!-- ------------------- Bloc Report Part ------------------- -->         
        @include('teacher/missions/_RDs', ['mission' => $current_mission ])
</span>
        </div>
        <!-- ------------------- Bloc Report Part ------------------- -->
        <div id="at_rep_struct" class="read bloc_lb_mission_edit">
            <div class="edit-mission-title"  onclick="toggleSection('#at_rep_struct', '#lnb_report_struct'); hideAllLds();">
                <i class="fa fa-caret-right toggle-section"></i>&nbsp;{{ __("Structure du rapport et labdocs initiaux") }}
            </div>
            <span style='display:none' id='lnb_report_struct'>
            <?php
                // vérification du nombre de rapports engagés attaché à la mission
                $nb_reports = $current_mission->reports()->whereIn('status', ['on', 'wait'])->count();
                if ($nb_reports >= 1) { echo "<div class='lba_warning'>".__("Attention").__("&nbsp;:")." "; }
                if ($nb_reports==1) { echo __("une équipe a déjà commencé à travailler sur son rapport (en cours ou rendu)."); }
                else if ($nb_reports>1) { echo $nb_reports . " " . __("équipes ont déjà commencé à travailler sur leur rapport (en cours ou rendu)."); }
                if ($nb_reports >= 1) {
             ?>
        </div>
        <div class="label_small">
            {{ __("Ceci implique quelques précautions si vous souhaitez modifier la structure ou les contenus du rapport...") }}
            <p>
                - <b>{{__("Pour les parties de rapport")}}{!!__("&nbsp;:")!!} </b>
                <br/>&nbsp;&nbsp;&nbsp;{{ __("Ajout, modification, déplacement : tous ces changements sont répercutés dans les rapports étudiants.") }}
                <br/>&nbsp;&nbsp;&nbsp;{{ __("Suppression : ATTENTION !! TOUS LES labdocs inclus dans la partie de rapport, Y COMPRIS ceux créés par les étudiants, sont SUPPRIMÉS !") }}
            </p>
            <p>
                - <b>{{__("Pour les labdocs")}}{!!__("&nbsp;:")!!} </b>
                <br/>&nbsp;&nbsp;&nbsp;{{ __("Ajout : le labdoc est ajouté dans les rapports étudiants en fin de partie de rapport.") }}
                <br/>&nbsp;&nbsp;&nbsp;{{ __("Modification, suppression : ne sont reportés dans les rapports étudiants que si le labdoc n'a pas déjà été modifié par les étudiants.") }}
                <br/>&nbsp;&nbsp;&nbsp;{{ __("Déplacement : n'est pas reporté dans les rapports étudiants.") }}
            </p>
        </div>
        <?php
            }
        ?>
<script>
    window.global_nb_reports =  {{ $nb_reports }}
</script>
            <div class="report-options">
                    <span title="{{ __("Ajustement automatique") }}">
                        <input type="checkbox" id="auto_fit" name="text" onclick='validContent(this.id);' @if ($current_mission->auto_fit) checked @endif>
                        <label for="auto_fit">
                            {{ __("Autoriser l'ajustement automatique pour tous les labdocs jeu de données") }}
                        </label>
                    </span>
                <i id="auto_fit_tooltip"
                   class="fa fa-info-circle lba_icon_default"
                   onmouseover="global_tooltip_popup.openTooltipPopup('{{ __("Ajustement automatique") }}', '{{ __("Si vous cochez cette case...") }}', '#auto_fit_tooltip')"
                   onclick="global_tooltip_popup.openTooltipPopup('{{ __("Ajustement automatique") }}', '{{ __("Si vous cochez cette case...") }}', '#auto_fit_tooltip')">
                </i>
            </div>
            <div class="item_list" id="rp_list">
                @include('teacher/missions/_RPs', ['mission' => $current_mission])
            </div>
            <div x-data="{new_rp:false}" id="rp_add" class="bloc_add lba_add_rp pointer">
                <template x-if="new_rp">
                    <div id="rp_0" class="lba_rp item_draggable part_draggable"
                         x-data="{ rpNS: reportPartCtrl({id_report_part:0,position:document.getElementById('rp_list').children.length+1},true,false,false)}"
                         x-init="rpNS.init()">
                        @include('teacher/missions/_RPLine')
                    </div>
                </template>
                <template x-if="!new_rp">
                    <div class=" lba_line bordure"  x-on:click="validCurrentEditedLD(); validCurrentEditedRP();new_rp=true">
                        <i class="fa fa-plus lba_icon_default" title="{{ __("Ajouter une partie de rapport") }}"></i>
                        <span class="place_holder_text">{{ __("Ajouter une partie de rapport") }}</span>
                    </div>
                </template>
            </div>
        </div>
        </span>
        @if(!$current_mission->simulation_code)
            <div id="at_rep_eval" class="read bloc_lb_mission_edit">
                <div class="edit-mission-title" onclick="toggleSection('#at_rep_eval', '#rubric')">
                    <i class="fa fa-caret-right toggle-section"></i>&nbsp;{{ __("Grilles critériées pour l'évaluation") }}
                </div>
                @include('teacher/missions/_rubric')
            </div>
        @endif
</div>

<!-- Menu contextuel des labdocs dans le menu -->
<span id="ldm_context_menu" class="read context_menu" style="display: none">
    <i onclick="contextMenuEditLD(event);" class="fas fa-pencil-alt fa-fw lba_icon_default" title="{{ __("Modifier") }}"></i>
    <i onclick="contextMenuRemoveLDm(event);" class="far fa-trash-alt fa-fw lba_icon_default" title="{{ __("Supprimer") }}"></i>
</span>

<!-- Boite d'information (alternative a "alert()") -->
<div id="info_alert" class="read">
    <h4 id="info_alert_title"></h4><br>
    <p id="info_alert_comment"></p><br>
    <button onclick='$("#info_alert").hide();'>{{ __("Fermer") }}</button>
</div>

<!-- Vue zwibbler template -->
@include('tool/zwibbler')

<!-- Formulaire d'upload des fichiers -->
<form id="lba_upload_form" action="/storage/upload" method="post">
    <input id="lba_file" name="file_document" type="file" onkeydown="return false;" onchange="uploadFormChange();" accept=".pdf,.gif,.jpg,.jpeg,.png,.gz,.tgz,.zip,.csv,.py,.gum,.gum2,.rw3,.lab,.doc,.docx,.xls,.xlsx" />
    <input type="hidden" id="lba_res_type" name="type">
    <input type="hidden" id="lba_res_name" name="name">
    <input type="hidden" id="lba_res_id" name="id_resource">
    <input type="hidden" id="lba_context" name="context" value="mission">
    <input type="hidden" name="id_context" value="{{ $current_mission->id_mission}}">
</form>

<div class="popup" id="modal-mission-unlink-self" title="" data-reset-input="true">
    <header>
        <span class="popup-title" ></span>
        <span class="actions" >
            <a href="#" title="{{ __("Valider") }}" class="popup-submit"><i class="fa fa-check disabled"></i></a>
            <a href="#" title="{{ __("Annuler") }}" class="popup-cancel"><i class="fa fa-times"></i></a>
        </span>
    </header>
    <section>
        <form>
            <input type="checkbox" id="confirm-ok" name="confirm-ok" />
            <label for="confirm-ok"></label>
            <input type="hidden" id="mi-unlink-teacher-id" value="{{$user->id_user}}" />
            <input type="hidden" id="mi-unlink-teacher-action" value="" />
        </form>
    </section>
    
</div>

<?= $helper->loadAsset("/libraries/pica/pica.min.js") ?>
<?= $helper->loadAsset("/js/upload_image.js") ?>

@include('components.widgets.message.index', ['mv' => new App\Views\Widgets\Message($user->id_user, 0)])

@endsection
