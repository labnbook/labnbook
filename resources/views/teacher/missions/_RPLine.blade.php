<?php
    /**
     * 
     */
?>
<div class="rp_line" :class="{'rp_edit':rpNS.is_edit_mode,'lba_line':!rpNS.is_edit_mode}"
     x-on:lnb-valid-current-edited-rp.window="rpNS.validCurrentEditRp(new_rp);if(new_rp){new_rp=false;}" 
     x-on:lnb-close-ld-import-menu.window="rpNS.is_show_import = false;"
     x-on:dblclick="rpNS.switchToEditMode()">
    <span class="rp_line_title">
        <span :class="{'rp_number':!rpNS.is_edit_mode}" :id="'rp_position_'+ rpNS.rp.id_report_part" x-text="rpNS.rp.position"></span>
        <span>-</span>
        <span 
              :class="{'rp_title_edit':rpNS.is_edit_mode}">
            <template x-if="!rpNS.is_edit_mode">
                <span :id="'rp_title_'+ rpNS.rp.id_report_part" class="item_handle rp_ld_title"
                      :class="{'empty_title':!rpNS.rp.title}"
                      x-text="rpNS.rp.title || __('Nom de partie à indiquer')"></span>
            </template>
            <template x-if="rpNS.is_edit_mode">
                <input :id="`rp_edit_title_${rpNS.rp.id_report_part}`" class="" name="rp_edit_title"  type="text"
                       x-model="rpNS.rp.title" :value="rpNS.rp.title" x-init="$el.focus();$nextTick(()=>{window.scroll(0,$el.offsetTop-150)})">
            </template>
        </span>
        
        <template x-if="rpNS.is_edit_mode">
            <span class="rp_buttons">
                <i id="rp_edit_validate_" class="fa fa-check icon_baseline"
                   x-on:click="if(rpNS.checkIfRPIsValid()){rpNS.validateEditRP(rpNS.rp);new_rp=false;rpNS.is_edit_mode=false;}"
                   title="{{__('Valider')}}"></i>
                <i id="rp_edit_cancel_" class="fa fa-times icon_baseline"
                   x-on:click="
                    if (confirm(__('Les modifications de cette partie ne seront pas prises en compte'))) {
                       if(!new_rp){rpNS.getBackPreviousVersion();}rpNS.is_edit_mode=false;new_rp=false;rpNS.closeEditAssignment();
                    }
                   "
                   title="{{__('Annuler')}}"></i>
            </span>
        </template>
    </span>
    
    <template x-if="!rpNS.is_edit_mode">
        <!-- Menu contextuel des parties de rapport -->
        <span id="rp_context_menu" class="read context_menu">
            <span class="lba_icon_edit_ld">
                <i class="fa fa-ellipsis-v lba_icon_default" ></i>
            </span>
            <span class="hover_menu_edition">
                <span class="menu_edition_item" x-on:click="rpNS.switchToEditMode()">
                    <i class="fas fa-pencil-alt fa-fw lba_icon_default" title="{{ __("Modifier") }}"></i>
                    {{ __("Modifier") }}
                </span>
                <span class="menu_edition_item" x-on:click="rpNS.removeRP(rpNS.rp);">
                    <i class="far fa-trash-alt fa-fw lba_icon_default" title="{{ __("Supprimer") }}"></i>
                    {{ __("Supprimer") }}
                </span>
            </span>
        </span>
    </template>
   
    <span :class="{'right_part':!rpNS.is_edit_mode,'student_permission':rpNS.is_edit_mode}">
        <template x-if="(rpNS.rp.assignment || rpNS.rp.containsLdAssignment) && !rpNS.is_edit_mode"> <!-- TODO -->
             <span class="lb_show_assignment" :id="'rp_show_assi_'+ rpNS.rp.id_report_part"
                   x-on:click="rpNS.is_show_assignments = !rpNS.is_show_assignments; rpNS.is_show_assignment = !rpNS.is_show_assignment || rpNS.is_show_assignments" 
                   title='{{__("Afficher les consignes spécifiques pour cette partie du rapport")}}'>{{__('Consignes')}}...</span>
         </template>
        <span class="wrapper_icon_ld">
            <span x-show="rpNS.is_edit_mode">{{__('Types de labdocs pouvant être ajoutés par les étudiants : ')}}</span>
            <input x-show="rpNS.is_edit_mode" type="checkbox" name="text" x-model="rpNS.rp.text" :checked="rpNS.rp.text">
            <img :id="'rp_text_'+ rpNS.rp.id_report_part" class="icon_in_line_ld" :class="{'inactive':!rpNS.rp.text}" 
                 src="/images/report/ld_type_text.svg" width="16" 
                 :title="'{{__('Ajouter un labdoc texte')}}' + (rpNS.rp.text ? '' : '{{__(' - désactivé pour les étudiants')}}')"
                 alt="{{__("textes")}}" x-on:click="if(!rpNS.is_edit_mode && (!event.detail || event.detail == 1)){rpNS.validateAddLD(rpNS.rp, 'text')}">
            <input x-show="rpNS.is_edit_mode" type="checkbox" name="drawing" x-model="rpNS.rp.drawing" :checked="rpNS.rp.drawing">
            <img :id="'rp_drawing_'+ rpNS.rp.id_report_part" class="icon_in_line_ld" :class="{'inactive':!rpNS.rp.drawing}" 
                 src="/images/report/ld_type_drawing.svg" width="16" 
                 :title="'{{__('Ajouter un labdoc dessin')}}' + (rpNS.rp.drawing ? '' : '{{__(' - désactivé pour les étudiants')}}')"
                 alt="{{__("dessins")}}" x-on:click="if(!rpNS.is_edit_mode && (!event.detail || event.detail == 1)){rpNS.validateAddLD(rpNS.rp, 'drawing')}">
            <input x-show="rpNS.is_edit_mode" type="checkbox" name="dataset" x-model="rpNS.rp.dataset" :checked="rpNS.rp.dataset">
            <img :id="'rp_dataset_'+ rpNS.rp.id_report_part" class="icon_in_line_ld" :class="{'inactive':!rpNS.rp.dataset}" 
                 src="/images/report/ld_type_dataset.svg" width="16" 
                 :title="'{{__('Ajouter un labdoc jeu de donnée')}}' + (rpNS.rp.dataset ? '' : '{{__(' - désactivé pour les étudiants')}}')"
                 alt="{{__("données")}}" x-on:click="if(!rpNS.is_edit_mode && (!event.detail || event.detail == 1)){rpNS.validateAddLD(rpNS.rp, 'dataset')}">
            <input x-show="rpNS.is_edit_mode" type="checkbox" name="procedure" x-model="rpNS.rp.procedure" :checked="rpNS.rp.procedure">
            <img :id="'rp_procedure_'+ rpNS.rp.id_report_part" class="icon_in_line_ld" :class="{'inactive':!rpNS.rp.procedure}" 
                 src="/images/report/ld_type_procedure.svg" width="16" 
                 :title="'{{__('Ajouter un labdoc protocole')}}' + (rpNS.rp.procedure ? '' : '{{__(' - désactivé pour les étudiants')}}')"
                 alt="{{__("protocoles")}}" x-on:click="if(!rpNS.is_edit_mode && (!event.detail || event.detail == 1)){rpNS.validateAddLD(rpNS.rp, 'procedure')}">
            <input x-show="rpNS.is_edit_mode" type="checkbox" name="code" x-model="rpNS.rp.code" :checked="rpNS.rp.code">
            <img :id="'rp_code_'+ rpNS.rp.id_report_part" class="icon_in_line_ld" :class="{'inactive':!rpNS.rp.code}" 
                 src="/images/report/ld_type_code.svg" width="16" 
                 :title="'{{__('Ajouter un labdoc code')}}' + (rpNS.rp.code ? '' : '{{__(' - désactivé pour les étudiants')}}')"
                 alt="{{__("codes")}}" x-on:click="if(!rpNS.is_edit_mode && (!event.detail || event.detail == 1)){rpNS.validateAddLD(rpNS.rp, 'code')}">
            <img x-show="!rpNS.is_edit_mode" class="icon_in_line_ld inactive"
                 src="/images/report/ld_type_import.svg" width="16" 
                 title="{{__('Importer un labdoc')}}"
                 alt="{{__("codes")}}" x-on:click="if(!rpNS.is_edit_mode && (!event.detail || event.detail == 1)){rpNS.is_show_import=true; rpNS.getImportLD(rpNS.rp.id_report_part);}">
        </span>
    </span>
    <template x-if="rpNS.is_edit_mode">
        <div x-show="rpNS.is_edit_mode">
            <p>{{__("Consignes")}}{!!__("&nbsp;:")!!} </p>
            <div :id="`rp_edit_assi_${rpNS.rp.id_report_part}`" x-init="updateHtmlWithFormulas(`#rp_edit_assi_${rpNS.rp.id_report_part}`, rpNS.rp.assignment, () => rpNS.initEditAssignment(rpNS.rp.id_report_part))"></div>
        </div>
    </template>
</div>
<!-- Recuperation des LD importables -->
<div x-on:click.outside="rpNS.is_show_import=false" x-show="rpNS.is_show_import" :id="`import_ld_${rpNS.rp.id_report_part}`" class="ld_import_menu_mission"></div>

<template x-if="rpNS.rp.assignment && !rpNS.is_edit_mode && rpNS.is_show_assignments && rpNS.is_show_assignment">
    <div :id="'rp_div_assi_'+ rpNS.rp.id_report_part" class="lb_assignment lb_div_assignment tinyMCE-text"
         x-on:dblclick="rpNS.switchToEditMode()">
        <i class="fa fa-times lb_assignment_close" x-on:click="rpNS.is_show_assignment = false" title="{{ __("Fermer les consignes") }}"></i>
        <div :id="'rp_assi_'+ rpNS.rp.id_report_part" x-init="updateHtmlWithFormulas(`#rp_assi_${rpNS.rp.id_report_part}`, rpNS.rp.assignment)"></div>
    </div>
</template>
