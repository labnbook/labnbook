<?php
    /**
     * @var \App\Mission $mission
     */
     $report_parts = $mission->reportParts()->orderBy('position')->get();
 ?>
@foreach($report_parts as $report_part)
    @include('teacher/missions/_RP', ['rp' => $report_part])
@endforeach
