<div class="name-description">
    <div :class="{ 'rubric-invalid-name-text': rubNS.rubric.nameInvalidText }">
        <label :for="'rubric_name_' + rubNS.rubric.id_rubric" class="edit-mission-header">{{ __("Nom de la grille") }}</label>&nbsp;&nbsp;<span class="label_small" :class="{ 'warning-name': rubNS.rubric.nameInvalidText, 'valid-name': !rubNS.rubric.nameInvalidText }">{{ __("(ce champ est obligatoire)") }}</span><br />
        <input :id="'rubric_name_' + rubNS.rubric.id_rubric" class="bordure ligne" type="text"
               :title="rubNS.getInformation('changeRubricNameButton', 'rubric')" x-model="rubNS.rubric.name" 
               x-on:keyup="rubNS.check('invalidText', 'rubric', rubNS.rubric); rubNS.rubric.hasChanged = true"
               spellcheck="true"
               >

    </div>
    
    <div class="mi-bloc">
        <div :id="'bloc_lba_rubric_description_read'">
            <label class="edit-mission-header">{{ __("Description") }}</label>&nbsp;&nbsp;<span class="label_small">{{ __("(ce texte n’est visible que des concepteurs de la grille)") }}</span>
            <div :id="'lba_rubric_description_read'" class="bordure tinyMCE-text lba_read"
                 title="{{ __("Cliquer pour modifier la description") }}" x-init="updateHtmlWithFormulas('#lba_rubric_description_read', rubNS.rubric.description)"
                 x-on:click="rubNS.alpineEditContent('rubric_description');">
            </div>
        </div>
        <div :id="'bloc_lba_rubric_description_edit'" class="bordure hide bloc_lba_edit">
            <div class="title">
                <span>{{ __("Édition du texte de description") }}</span>
                <i class="fa fa-check icon_baseline" title="{{ __("Valider et quitter l'édition") }}"
                   x-on:click="rubNS.rubric.description = rubNS.alpineValidContent('rubric_description', false); rubNS.rubric.hasChanged = true"></i>
                <i class="fa fa-times icon_baseline" title="{{ __("Annuler et quitter l'édition") }}"
                   x-on:click="rubNS.alpineCancelContent('rubric_description');"></i>
            </div>
            <div>
                <div :id="'lba_rubric_description_edit'"></div>
            </div>
        </div>
    </div>
</div>
