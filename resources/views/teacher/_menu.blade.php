<?php
    /** @var string $page_name */
     /** @var int $refresh_period */
      /** @var \App\Mission|null $mission */
            /** @var \App\User $user */
      $user = Auth::user();
   ?>

<div id="authoring_menu">
    <div id="lb_modal_cover"></div>

    <div class="off authoring_menu_div" onclick="window.location='/reports/'" title="{{__('Accéder à mon espace étudiant')}}">
        <span class="authoring_menu_item"><img src='/images/LNB_icon.svg' height="27" alt="{{'LNB'}}"/></span>
    </div>

    <?php if (auth()->user()->isAdmin()) { ?>
       <div class="authoring_menu_div <?= $page_name === "admin" ? "on" : "off" ?>" onclick="window.location='/admin'">
           <span class="authoring_menu_item">{{ __("Admin") }}</span>
       </div>
    <?php } ?>

    <ul class="breadcrumb">

    <?php if (auth()->user()->teacher->isManager()) :?>
        <li id="teacher_button_menu_button" class="<?= $page_name === "manager/teachers" ? "on" : "off" ?>" onclick="window.location='/manager/teachers'" title="{{ __("Créer, modifier, supprimer des équipes pédagogiques") }}">
            <span class="authoring_menu_item">{{ __("Enseignants") }}</span>
        </li>
    <?php endif ?>

        <li class="<?= $page_name === "teacher/missions" ? "on" : "off" ?>" onclick="window.location='/teacher/missions'" title="{{__('Créer, modifier, supprimer vos missions') . "
" . __('Voir les missions publiques')}}">
           <span class="authoring_menu_item">{{__("Missions")}}</span>
        </li>

        <?php if (isset($mission) && $page_name === "teacher/mission/{$mission->id_mission}"){
            $code = $mission->code;
            ?>
            </ul>
            <ul class="breadcrumb">
               <li class="on">
                   <span class="authoring_menu_item">
                       <span>{{__('Mission')}} <?= htmlspecialchars($code) ?></span>
                       <a href="#" title="{{__("Visualiser le rapport telle qu'il apparait pour les étudiants")}}" onclick="openTestReport({{ (int) $mission->id_mission }})">
                           <i class="far fa-file"></i>
                       </a>
                       <a href="/teacher/missions">
                           <i class="fa fa-check" title="{{__("Fermer l'onglet (la mission est déjà enregistrée)")}}"></i>
                       </a>
                   </span>
               </li>
        <?php } ?>
                
        @if(Auth::user()->isSimulationManager())
            <li class="<?= $page_name === "teacher/simulations" ? "on" : "off" ?>" onclick="window.location='/teacher/simulations'" title="{{__("Editer un TP simulé") . "
    " . __("Modifier les paramètres d’un TP simulé")}}">
                <span class="authoring_menu_item">{{ __("Simulations") }}</span>
            </li>
        @endif
                
       <li class="<?= $page_name === "teacher/students" ? "on" : "off" ?>" onclick="window.location='/teacher/students'" title="{{__("Créer des classes, inscrire des étudiants") . "
" . __("Attribuer une mission (créer les équipes)")}}">
       <span class="authoring_menu_item">{{ __("Étudiants") }}</span>
       </li>

       <li class="<?= $page_name === "teacher/reports" ? "on" : "off" ?>" onclick="window.location='/teacher/reports'" title="{{__("Suivre et annoter les rapports de vos étudiants") . "
" . __("Modifier les paramètres des rapports et les équipes")}}">
           <span class="authoring_menu_item">{{ __("Rapports") }}</span>
       </li>

        @if ($page_name === "scoring/assessments/getGrades")
            <li class="on">
                <span class="authoring_menu_item">
                    <span>{{__('Évaluations')}} {{$code}}</span>
                </span>
            </li>
        @endif

    </ul>

    <div class="off authoring_menu_div"  id="lb_menubar_messagerie" onclick="toggleWidget('messagerie');">
        <span class="authoring_menu_item fa-layers fa-3x" >
            <i class="fa fa-envelope" title="{{ __("Ouvrir la messagerie de LabNBook") }}"></i>
            <span class="fa-layers-counter"></span>
        </span>
    </div>

    <div class="off authoring_menu_div" id="lb_auth_menu_menu">
        <span class="authoring_menu_item"><i class="fa fa-bars" class="authoring_menu_item"></i></span>

        <div id='lb_second_menu'>
            <a href="/reports/">
                <div class="lb_second_menu_item">
                    <span class="lb_second_menu_icon"><img src="/images/LNB_icon-2.svg" height="16"/></span>
                    <span class=lb_second_menu_text">{{ __("Accéder à mon espace étudiant") }}</span>
                </div>
            </a>
            <a href="https://labnbook.fr/ressources-enseignants/" target="_blank">
                <div class="lb_second_menu_item">
                    <span class="lb_second_menu_icon"><i class="fa fa-question"></i></span>
                    <span class=lb_second_menu_text">{{ __("Consulter la page d'aide") }}</span>
                </div>
            </a>
            <a href="{{ $helper->getMailto('suppport') }}" >
                <div class="lb_second_menu_item">
                    <span class="lb_second_menu_icon"><i class="fa fa-medkit"></i></span>
                    <span class=lb_second_menu_text">{{ __("Obtenir de l'assistance (mail)") }}</span>
                </div>
            </a>
            <a href="{{ $helper->getMailto('bug') }}">
                <div class="lb_second_menu_item">
                    <span class="lb_second_menu_icon"><i class="fa fa-bug" aria-hidden="true"></i></span>
                    <span class=lb_second_menu_text">{{ __("Signaler un bug (mail)") }}</span>
                </div>
            </a>
            <a href="#" id="edit_account_btn" onclick="openAccountManagement({{Auth::id()}})" >
                <div class="lb_second_menu_item" id="my-account-menu-teacher">
                    <span class="lb_second_menu_icon"><i class="fa fa-user"></i></span>
                    <span class=lb_second_menu_text">{{ __("Afficher les infos de mon compte") }}</span>
                </div>
            </a>
            <a href="#" onclick="logout()">
                <div class="lb_second_menu_item">
                    <span class="lb_second_menu_icon"><i class="fa fa-power-off"></i></span>
                    <span class=lb_second_menu_text">{{ __("Se déconnecter de LabNBook") }}</span>
                </div>
            </a>
        </div>
    </div>

    @impersonating($guard = null)
    <div class="authoring_menu_div" id="lb_menu_user">
        <span class="authoring_menu_item user_self">
            {{substr($user->first_name, 0, 1).". ".$user->user_name}}
        </span>
    </div>
    @endImpersonating

    <input type="hidden" id="refresh_period" value="<?= $refresh_period ?? 20 ?>">

</div>

    @include('_flash-alerts')
    @include('teacher/_ask_api_add_permissions')
