<?php
    /** @var \App\Teacher  $teacher */
    $classes = $teacher->classes()->orderBy('class.class_name')->get();
    foreach ($classes as $c) {
        echo '<option value="'.$c->id_class.'">'.htmlspecialchars($c->class_name).'</option>' ;
    }
