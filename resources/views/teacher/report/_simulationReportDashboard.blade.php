<?php
    /**
     *@var $dashboardData
     */
?>

<div  class="indicator simulation-indicator" id="simulation-dashboard">
    <div class="titre titre-left">
        <p title="{{__('Les connaissances des étudiants')}}">
            <i class="fa-solid fa-dice-d20"></i>
            {{__('Connaissances')}}
        </p>
    </div>
    <div class="data-right">
        @foreach($dashboardData['globalKnowledges'] as $knowledge)
            <span style="margin-right: 5px" title="{{$knowledge->knowledge!==null?round($knowledge->knowledge*100):'-'}}%">
                <i class="fas fa-circle" style="color:{{$knowledge->color}}"></i>
                <span>{{$knowledge->description}}</span>
            </span>
        @endforeach
    </div>
</div>
