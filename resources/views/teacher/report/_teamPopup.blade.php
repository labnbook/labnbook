<?php
    /** @var \App\Teacher $teacher */
    $missions = $teacher->missions()->orderBy('code', 'ASC')->get()
?>
<div class="popup" id="te-popup">
    <header>
        <span id="te-popup-title" class="title" ></span>
        <span class="actions" >
            <i class="fa fa-check popup-submit" title="{{__('Valider')}}" id="te-popup-valid"></i>
            <i class="fa fa-spinner fa-pulse " style="display:none" title="{{__('Patienter')}}" id="te-popup-wait"></i>
            <i class="fa fa-times popup-cancel" title="{{__('Annuler')}}" id="te-popup-cancel" onclick="resetTeamPopup()"></i>
        </span>
    </header>
    <section>
        <div class='subsection'>
            <label for="te-mission">{{__('Mission attribuée')}}{!!__("&nbsp;:")!!}&nbsp;</label>
            <select id="te-mission">
                <option value="0">-</option>
                @include('teacher/report/_missionOptions', ['missions' => $missions, 'selected' => null, 'includeArchive' => true, 'includeAllOption' => false])
            </select>
        </div>
        <span class="te-icon"><i class="fa fa-users" aria-hidden="true"></i></span><span class="popup-subtitle"> {{__("Paramètres du rapport / de l'équipe")}}</span>
        <table class="subsection">
            <tr class="te-updating-only">
                <td><label>{{__('Avancement du rapport')}}{!!__("&nbsp;:")!!}</label></td>
                <td></td>
                <td>
                    <select name="status" id="te-team-status">
                        <option value="new">{{__('Non débuté')}}</option>
                        <option value="on">{{__('En cours')}}</option>
                        <option value="wait">{{__('Rendu')}}</option>
                        <option value="arc">{{__('Archivé')}}</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>{{__('Période de travail')}}{!!__("&nbsp;:")!!}&nbsp;</td>
                <td>{{__('du')}}&nbsp;</td>
                <td><input type="date" id="te-start-date" name="te-start-date"/> <input type="time" id="te-start-time" name="te-start-time" value="00:00"/></td>
            </tr>
            <tr>
                <td></td>
                <td>{{__('au')}}&nbsp;</td>
                <td><input type="date" id="te-end-date" name="te-end-date"/> <input type="time" id="te-end-time" name="te-end-time" value="23:59"/>
                </td>
            </tr>
            <tr>
                <td><label  id="te-team-name-label" for="te-team-name">{{__("Nom de l'équipe")}}{!!__("&nbsp;:")!!}&nbsp;</label></td>
                <td></td>
                <td><input type="text" id="te-team-name" name="te-team-name"/></td>
            </tr>
            <tr class="te-creation-only">
                <td colspan="2">
                    <input type="checkbox" name="te-send-mail" id="te-send-mail"/>
                    <label for="te-send-mail"> {{__("Prévenir les étudiants par e-mail de la création de l'équipe")}}</label>
                </td>
            </tr>
        </table>

        <fieldset>
            <legend> {{__("Etudiants de l'équipe")}} </legend>
            <div id="te-learners-list">{{__('En construction...')}}</div>
            <label for="te-add-learner">{{__('Ajouter un étudiant')}}{!!__("&nbsp;:")!!}</label>
            <x-select2 id="te-add-learner" on-select-function="reportsTable.addLearnerToTheTeam(selectedElement)"></x-select2>
            <span id="te-add-learner-temp"></span>
        </fieldset>

        <fieldset class="te-creation-only">
            <legend>{{__('Mode de création des équipes')}} * </legend>
            <input type="radio" checked="checked" name="te-create" id="te-create-classic"  onclick="teamPopupInit(1)"/>
            <label for="te-create-classic"> {{__('Créer une équipe avec tous les étudiants sélectionnés')}}</label><br/>
            <input type="radio" name="te-create" id="te-create-random" onclick="teamPopupInit(2)"/>
            <label for="te-create-random-number"> {{__('Créer aléatoirement des équipes de')}} <input id="te-create-random-number" min="1" type="number" onClick="$('#te-create-random').prop('checked', true).trigger('click')"/> {{__('étudiants')}}</label><br/>
            <input type="radio" name="te-create" id="te-create-old-mission"  onclick="teamPopupInit(3)"/>
            <label for="te-create-old-mission"> {{__('Appliquer la même mise en équipe que la mission')}}</label> 
            <select id="te-create-old-mission-id" onChange="$('#te-create-old-mission').prop('checked', true).trigger('click')">
                <option value="0">-</option>
                @include('teacher/report/_missionOptions', ['missions' => $missions, 'selected' => null, 'includeArchive' => true, 'includeAllOption' => false])
            </select>
            <br/>
        </fieldset>

        <span class="te-icon"><img src="/images/import_ld.svg" width="18" alt="import" /></span><span class="popup-subtitle"> {{__("Autorisation d'import de labdocs")}}{!!__("&nbsp;:")!!}</span>
        <div class="subsection">
            {{__('depuis les missions')}}&nbsp;
            <select id="te-import-ld">
                <option value="-1">{{__('Toutes')}}</option>
                <option value="0">{{__('Aucune')}}</option>
                @include('teacher/report/_missionOptions', ['missions' => $missions, 'selected' => null, 'includeArchive' => true, 'includeAllOption' => false])
            </select>
        </div>
        <span class="te-icon"><i class="fa fa-envelope"></i></span><span class="popup-subtitle"> {{__("Autorisations sur l'envoi de messages - destinataires possibles")}}{!!__("&nbsp;:")!!}</span>
        <table class="subsection">
            <tr>
                <td><label>{{__('les enseignants')}}{!!__("&nbsp;:")!!}&nbsp;</label></td>
                <td><input type="radio" value="1" name="te-msg-teacher" checked /> {{__('oui')}}
                    <input type="radio" value="0" name="te-msg-teacher" /> {{__('non')}}</td>
            </tr><tr>
                <td><label>{{__("les étudiants de l'équipe")}}{!!__("&nbsp;:")!!}&nbsp;</label></td>
                <td><input type="radio" value="1" name="te-msg-team" checked /> {{__('oui')}}
                    <input type="radio" value="0" name="te-msg-team" /> {{__('non')}}</td>
            </tr><tr>
                <td><label for="te-msg-class">{{__('les étudiants de la classe')}}{!!__("&nbsp;:")!!}&nbsp;</label></td>
                <td><select id="te-msg-class" >
                        <option value="0">{{__('Aucune')}}</option>
                        @include('teacher/report/_classesAsOptions', ['teacher', $teacher])
                    </select></td>
            </tr><tr>
                <td><label for="te-msg-mission">{{__('les étudiants inscrits à la mission')}}{!!__("&nbsp;:")!!}&nbsp;</label></td>
                <td><select id="te-msg-mission" >
                        <option value="0">{{__('Aucune')}}</option>
                        @include('teacher/report/_missionOptions', ['missions' => $missions, 'selected' => null, 'includeArchive' => true, 'includeAllOption' => false])
                    </select></td>
            </tr>
        </table>
        <span class="te-icon"><i class="fa fa-paperclip"></i></span><span class="popup-subtitle"> {{__('Autorisations sur les labdocs attachés aux messages')}}</span>
        <table class="subsection">
            <tr>
                <td><label> {{__('Envoi de labdocs du rapport')}}{!!__("&nbsp;:")!!}&nbsp;</label></td>
                <td><input type="radio" value="1" name="te-msg-send-ld" /> {{__('oui')}}
                    <input type="radio" value="0" name="te-msg-send-ld" checked /> {{__('non')}}</td>
            </tr><tr>
                <td><label> {{__('Enregistrement dans le rapport des labdocs reçus')}}{!!__("&nbsp;:")!!}&nbsp;</label></td>
                <td><input type="radio" value="1" name="te-msg-save-ld" /> {{__('oui')}}
                    <input type="radio" value="0" name="te-msg-save-ld" checked /> {{__('non')}}<br/></td>
            </tr>
        </table>
    </section>
</div>
