<?php
/** @var int $selected 
  * @var boolean $includeArchive
  * @var boolean $includeAllOption
  * @var  \Illuminate\Database\Eloquent\Collection|\App\Mission[] $missions
*/

    $sel = !$selected  ? 'selected' : '';
    $html ="";
    // TODO refactor this parameter
    if($includeAllOption){
        $html = '<option '.$sel.' value="0">'.__('-- Toutes --').'</option>';
    } 
    $html2 = '';
    foreach($missions as $m) {
        if($m->status != "archive" || $includeArchive){
            $sel = $selected == $m->id_mission  ? 'selected' : '';
            $option = '<option '.$sel.' value="' . htmlspecialchars($m->id_mission) . '">' . htmlspecialchars($m->code);
            if ($m->status == "archive") {
                $html2 .= $option . " (archive)</option>";
            } else {
                $html .= $option . '</option>';
            }
        }
    }
    if (empty($missions)) {
        // Add fake option for selected class
        $value = isset($selected) ? $selected : 0;
        $html = "<option value='".$value."'>". __("Chargement...")."</option>";
    }
echo $html.$html2;
