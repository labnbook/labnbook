<?php
    /** @var \App\Teacher $teacher */
    /** @var  \Illuminate\Database\Eloquent\Collection|\App\Classe[] $classes */
    $missions = $teacher->missions()->orderBy('code', 'ASC')->get()
     
?>
<div class="popup" id="batch-update-popup">
    <header>
        <span class="title">{{__('Modifier les options de')}} <strong class="num-reports"></strong> {{__('rapports')}}</span>
        <span class="actions">
            <i class="fa fa-check popup-valid" title="{{__('Valider')}}"></i>
            <i class="fa fa-times popup-cancel" title="{{__('Annuler')}}"></i>
        </span>
    </header>
    <section>
        <form>
            <div>
                <div>
                    <span class="te-icon"><i class="fa fa-users"></i></span>
                    <span class="popup-subtitle">{{__('Paramètres des rapports / des équipes')}}</span>
                </div>
                <table class="subsection">
                    <tr>
                        <td><label>{{__('Avancement du rapport')}}{!!__("&nbsp;:")!!}</label></td>
                        <td></td>
                        <td><select name="status" id="batch-status">
                                <option value="">{{__('Inchangé')}}</option>
                                <option value="new" disabled>{{__('Non débuté')}}</option>
                                <option value="on">{{__('En cours')}}</option>
                                <option value="wait">{{__('Rendu')}}</option>
                                <option value="arc">{{__('Archivé')}}</option>
                            </select></td>
                    </tr><tr>
                        <td><label>{{__('Période de travail')}}{!!__("&nbsp;:")!!}</label></td>
                        <td>{{__('du')}}&nbsp;</td>
                        <td> 
                            <input type="date" name="start_date" placeholder="jj/mm/aaaa" />
                            <input type="time" name="start_time" placeholder="hh:mm" value="00:00" />
                            <input type="hidden" name="start_datetimes_are_unified" value="0" />
                            <span class="hidden">
                                <span>{{__('dates inchangées')}}</span>
                                <button type="button" class="lb_btn" id="modify-start-datetime" onclick="modifyReportsDatetime(this)">{{__('Modifier')}}</button>
                            </span>
                            <button type="button" class="lb_btn hidden" id="undo-modify-start-datetime" onclick="undoModifyReportsDatetime(this)">{{__('Annuler')}}</button>
                        </td>
                    </tr><tr>
                        <td></td>
                        <td>{{__('au')}}&nbsp;</td>
                        <td>
                            <input type="date" name="end_date" placeholder="jj/mm/aaaa" />
                            <input type="time" name="end_time" placeholder="hh:mm" value="23:59" />
                            <input type="hidden" name="end_datetimes_are_unified" value="0" />
                            <span class="hidden">
                                <span>{{__('dates inchangées')}}</span>
                                <button type="button" class="lb_btn" id="modify-end-datetime" onclick="modifyReportsDatetime(this)">{{__('Modifier')}}</button>
                            </span>
                            <button type="button" class="lb_btn hidden" id="undo-modify-end-datetime" onclick="undoModifyReportsDatetime(this)">{{__('Annuler')}}</button>
                        </td>
                    </tr>
                </table>
            </div>
            <div>
                <div>
                    <span class="te-icon"><img src="/images/import_ld.svg" width="18" alt="import"></span>
                    <span class="popup-subtitle">{{__("Autorisation d'import de labdocs")}}{!!__("&nbsp;:")!!} </span>
                </div>
                <div class="subsection">
                    <label>{{__('depuis les missions')}}{!!__("&nbsp;:")!!}</label>
                    <select name="allow_import_id_mission">
                        <option value="">{{__('Ne pas modifier')}}</option>
                        <option value="-1">{{__('Toutes')}}</option>
                        <option value="0">{{__('Aucune')}}</option>
                        @include('teacher/report/_missionOptions', ['missions' => $missions, 'selected' => null, 'includeArchive' => true, 'includeAllOption' => false])
                    </select>
                </div>
            </div>
            <div>
                <div>
                    <span class="te-icon"><i class="fa fa-envelope"></i></span>
                    <span class="popup-subtitle">{{__("Autorisations sur l'envoi de messages - destinataires possibles")}}{!!__("&nbsp;:")!!}</span>
                </div>
                <table class="subsection">
                    <tr>
                        <td><label>{{__('les enseignants')}}{!!__("&nbsp;:")!!}</label></td>
                        <td><label>
                                <input name="allow_msg_teacher" value="1" type="radio" />
                                {{__('oui')}}
                            </label>
                            <label>
                                <input name="allow_msg_teacher" value="0" type="radio" />
                                {{__('non')}}
                            </label>
                            <label>
                                <input name="allow_msg_teacher" value="" type="radio" checked />
                                {{__('ne rien changer')}}
                            </label></td>
                    </tr>
                    <tr>
                        <td><label>{{__("les étudiants de l'équipe")}}{!!__("&nbsp;:")!!}</label></td>
                        <td><label>
                                <input name="allow_msg_team" value="1" type="radio" />
                                {{__('oui')}}
                            </label>
                            <label>
                                <input name="allow_msg_team" value="0" type="radio" />
                                {{__('non')}}
                            </label>
                            <label>
                                <input name="allow_msg_team" value="" type="radio" checked />
                                {{__('ne rien changer')}}
                            </label></td>
                    </tr>
                    <tr>
                        <td><label>{{__('les étudiants de la classe')}}{!!__("&nbsp;:")!!}</label></td>
                        <td><select name="allow_msg_id_class">
                                <option value="" selected>{{__('Ne rien changer')}}</option>
                                <option value="0">{{__('Aucune')}}</option>
                                @include('teacher/report/_classesAsOptions', ['teacher', $teacher])
                            </select></td>
                    </tr>
                    <tr>
                        <td><label>{{__('Les étudiants de la mission')}}{!!__("&nbsp;:")!!}</label></td>
                        <td><select name="allow_msg_id_mission">
                                <option value="" selected>{{__('Ne rien changer')}}</option>
                                <option value="0">{{__('Aucune')}}</option>
                                @include('teacher/report/_missionOptions', ['missions' => $missions, 'selected' => null, 'includeArchive' => true, 'includeAllOption' => false])
                            </select></td>
                    </tr>
                </table>
            </div>
            <div>
                <div>
                    <span class="te-icon"><i class="fa fa-paperclip"></i></span>
                    <span class="popup-subtitle">{{__('Autorisations sur les labdocs attachés aux messages')}}</span>
                </div>
                <table class="subsection">
                    <tr>
                        <td><label>{{__('Envoi de labdocs du rapport')}}{!!__("&nbsp;:")!!}</label></td>
                        <td><label>
                                <input name="allow_attach_ld" value="1" type="radio" />
                                {{__('oui')}}
                            </label>
                            <label>
                                <input name="allow_attach_ld" value="0" type="radio" />
                                {{__('non')}}
                            </label>
                            <label>
                                <input name="allow_attach_ld" value="" type="radio" checked />
                                {{__('ne rien changer')}}
                            </label></td>
                    </tr>
                    <tr>
                        <td><label>{{__('Enregistrement des labdocs reçus')}}{!!__("&nbsp;:")!!}</label></td>
                        <td><label>
                                <input name="allow_save_ld" value="1" type="radio" />
                                {{__('oui')}}
                            </label>
                            <label>
                                <input name="allow_save_ld" value="0" type="radio" />
                                {{__('non')}}
                            </label>
                            <label>
                                <input name="allow_save_ld" value="" type="radio" checked />
                                {{__('ne rien changer')}}
                            </label></td>
                    </tr>
                </table>
            </div>
        </form>
    </section>
</div>
