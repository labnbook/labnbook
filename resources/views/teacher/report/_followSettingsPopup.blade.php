<?php
?>
<div class="popup" id="open-multi-reports-popup" x-data="reportsTable.followReports()">
    <header>
        <span class="title">{{__('Ouverture de plusieurs rapports')}}</span>
        <span class="actions" >
            <i class="fa fa-check popup-submit" title="{{__('Valider')}}" x-on:click="openReports()"></i>
            <i class="fa fa-spinner fa-pulse " style="display:none" title="{{__('Patienter')}}"></i>
            <i class="fa fa-times popup-cancel" id="open-multi-reports-popup-cancel" title="{{__('Annuler')}}"></i>
        </span>
    </header>
    <section>
        <p>{{__("Vous allez ouvrir plusieurs rapports, chacun dans un nouvel onglet.")}}</p>
        <p>{{__("Si seul un rapport s'affiche, autorisez les pop-ups pour LabNBook DANS L'ONGLET ACTUEL.")}}</p>
        <div id="open-multi-reports-options">
            <div id="open-multi-reports-options-title"><p>{{__("Options d'ouverture des rapports :")}}</p></div>
            <div>
                <p><input type="checkbox" x-model="freezeLds" id="open-multi-reports-freeze" name="open-multi-reports-freeze"/> <label for="open-multi-reports-freeze">{{ __('Geler la mise à jour des labdocs') }}</label></p>
                <p><input type="checkbox" x-model="openScoring" id="open-multi-reports-scoring" name="open-multi-reports-scoring"/> <label for="open-multi-reports-scoring">{{ __('Ouvrir le panneau des évaluations') }}</label></p>
            </div>
        </div>
    </section>
</div>
