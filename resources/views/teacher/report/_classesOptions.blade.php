<?php foreach($classes as $c) {
    $sel = isset($selected) && $selected == $c->id_class  ? 'selected' : '';
?>
    <option value="{{$c->id_class}}" {{$sel}} > {{$c->class_name}}
    <?php 
    if ($c->status == "archive") {
         echo " (archive)";
    }
    echo "</option>";
}
if (empty($classes)) {
    // Add fake option for selected class
    $value = isset($selected) ? $selected : 0;
    echo "<option value='".$value."'>". __("Chargement...")."</option>";
}
