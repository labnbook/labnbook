@extends('layouts.labnbook')

@section('title', __("Affichage des évaluations des étudiants"))

@section('head')
    <?= $helper->loadAsset("/css/teacher/common.css") ?>
    <?= $helper->loadAsset("/js/teacher/authoring.js") ?>
    <?= $helper->loadAsset("/css/teacher/report.css") ?>
    {!! $helper->loadAsset("/js/scoring/evaluations.js"); !!}
    {!! $helper->loadAsset("/css/scoring/evaluations.css"); !!}
@endsection

@section('body')

    @include('teacher/_menu', [
        'page_name' => request()->path(),
        'refresh_period' => config('labnbook.refresh_period'),
        'code' => $code,
        ])

<div class="scoring" id="wrapper-content">
    <section>
        <div id="evaluations"
            x-data='{grades: @JSON($grades)}'
            x-init="evaluationsCtrl.init(grades);console.log(grades)"
         >
         <template x-if="grades === null">
             <div class="alert alert-danger">
                 {{__("Aucune évaluation pour les rapports séléctionnés")}}
             </div>
         </template>
         <template x-if="grades !== null">
             <div>
                 <div class="alert alert-danger" x-show="grades.errors !== 0">
                     {{__("Certaines statisques n'ont pu être calculées car les évaluations ont été effectuées sur des versions différentes de la grille critériée")}}
                </div>
                <table id="scoring_eval">
                    <tr class="scoring_eval_main_head">
                        <th>
                            {{__("Étudiant")}}
                        </th>
                        <th>
                            {{__("Équipe")}}
                        </th>
                       <th>
                           <span x-text="__('Note / \{\{grading\}\}', {grading: grades.grading} )"></span>
                       </th>
                       <th>
                           {{__("Rendue")}}
                       </th>
                       <template x-for="c in grades.criteria">
                           <th data-unit="%">
                               <div x-text="c.title" x-bind:title="c.title" ></div>
                           </th>
                       </template>
                       <th x-show="grades.show_bonus">
                           {{__("Bonus Malus global")}}
                       </th>
                       <th x-show="grades.show_individual_bonus">
                           {{__("Bonus Malus individuel")}}
                       </th>
                   </tr>
                    <tr class="scoring_eval_overview">
                        <td colspan="2">
                            {{__("Nombre de valeurs")}}
                        </td>
                        <td>
                            <span x-text="grades.counts.total"></span>
                        </td>
                        <td>
                            <!-- Empty : published -->
                        </td>
                        <template x-for="c in grades.counts.criteria">
                            <td>
                                <span x-text="c"></span>
                            </td>
                        </template>
                        <td x-show="grades.show_bonus">
                            <span x-text="grades.counts.bonus"></span>
                        </td>
                        <td x-show="grades.show_individual_bonus">
                            <span x-text="grades.counts.individual_bonus"></span>
                        </td>
                    </tr>
                    <tr class="scoring_eval_overview">
                        <td colspan="2">
                            {{__("Moyenne")}}
                        </td>
                        <td>
                            <span x-text="evaluationsCtrl.format(grades.means.total)"></span>
                        </td>
                        <td>
                            <!-- Empty : published -->
                        </td>
                        <template x-for="c in grades.means.criteria">
                            <td :class='evaluationsCtrl.valueClass(c)'>
                                <span x-text="evaluationsCtrl.format(c, '%', true) "></span>
                            </td>
                        </template>
                        <td x-show="grades.show_bonus">
                            <span x-text="grades.means.bonus > 0 ? '+' + evaluationsCtrl.format(grades.means.bonus) : evaluationsCtrl.format(grades.means.bonus)"></span>
                        </td>
                        <td x-show="grades.show_individual_bonus">
                            <span x-text="grades.means.individual_bonus > 0 ? '+' + evaluationsCtrl.format(grades.means.individual_bonus) : evaluationsCtrl.format(grades.means.individual_bonus)"></span>
                        </td>
                    </tr>
                    <tr class="scoring_eval_overview">
                        <td colspan="2">
                            {{__("Ecart type")}}
                        </td>
                        <td>
                            <span x-text="evaluationsCtrl.format(grades.stdevs.total)"></span>
                        </td>
                        <td>
                            <!-- Empty : published -->
                        </td>
                        <template x-for="c in grades.stdevs.criteria">
                            <td>
                                <span x-text="evaluationsCtrl.format(c, '%', true) "></span>
                            </td>
                        </template>
                        <td x-show="grades.show_bonus">
                            <span x-text="evaluationsCtrl.format(grades.stdevs.bonus)"></span>
                        </td>
                        <td x-show="grades.show_individual_bonus">
                            <span x-text="evaluationsCtrl.format(grades.stdevs.individual_bonus)"></span>
                        </td>
                    </tr>
                    <template x-for="g in grades.grades">
                        <tr class="odd_even">
                            <td>
                                <span x-text="g.name"></span>
                            </td>
                            <td>
                                <span x-text="g.team"></span>
                            </td>
                            <td>
                                <template x-if="!g.missing">
                                    <span x-text="evaluationsCtrl.format(g.total)"></span>
                                </template>
                                <template x-if="g.missing">
                                    <span>{{__("Abs")}}</span>
                                </template>
                            </td>
                            <td>
                                <span class="fa fa-check no-pointer" x-show="g.published"></span>
                                <input type="hidden" x-model="g.published"></input>
                            </td>
                            <template x-for="c in g.criteria">
                                <td :class='c.activated && !g.missing ? evaluationsCtrl.valueClass(c.value) : ""'
                                    x-show="grades.counts.total > 0"
                                      >
                                    <span
                                            x-text="g.missing ? '-' : (c.activated ? evaluationsCtrl.format(c.value, '%', true) : '{{__('Neutralisé')}}')"
                                            :title="c.value === '-' || g.missing ? __('Non concerné') : ''"
                                    ></span>
                                </td>
                            </template>
                            <td x-show="grades.show_bonus">
                                <template x-if="!g.missing">
                                    <span x-text="g.bonus ? (g.bonus > 0 ? '+' + evaluationsCtrl.format(g.bonus) : evaluationsCtrl.format(g.bonus)) : 0"></span>
                                </template>
                            </td>
                            <td x-show="grades.show_individual_bonus">
                                <template x-if="!g.missing">
                                    <span x-text="g.individual_bonus ? (g.individual_bonus > 0 ? '+' + evaluationsCtrl.format(g.individual_bonus) : evaluationsCtrl.format(g.individual_bonus)) : 0"></span>
                                </template>
                            </td>
                        </tr>
                    </template>
                </table>
                <div class="scf_btn_actions">
                    <a x-on:click="await evaluationsCtrl.publishAll() && grades.grades.forEach(e => e.published = true);"
                       x-show="grades.grades.filter(e => e.published !== true).length > 0"
                       class="lb_btn btn_pointer">
                       {{__("Rendre toutes les évaluations")}}
                    </a>
                    <a x-on:click="evaluationsCtrl.getCSV()" class="lb_btn btn_pointer">{{__("Exporter en CSV")}}</a>
                </div>
            </div>
         </template>
        </div>
    </section>
</div>
@include('components.widgets.message.index', ['mv' => new App\Views\Widgets\Message(auth()->id(), 0)])
@endsection
