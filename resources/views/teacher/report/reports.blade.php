<?php
    /** @var \App\User $user */
    /** @var \App\Helper $helper */
    /**
     *@var  \Illuminate\Database\Eloquent\Collection|\App\Classe[] $classes
     */
    /**
     *@var  \Illuminate\Database\Eloquent\Collection|\App\Mission[] $missions
    */
?>
@extends('layouts.labnbook')


@section('title',__('Suivi des rapports'))

@section('head')


    <?= $helper->loadAsset("/libraries/d3/d3.min.js") ?>
    <?= $helper->loadAsset("/libraries/plotly/plotly-2.11.1.min.js") ?>


    <!-- #########################################################
        RESSOURCES LOCAL Authoring -->

        <!-- Styles et fonctions communes aux pages Enseignants -->
        <?= $helper->loadAsset("/css/teacher/common.css") ?>
        <?= $helper->loadAsset("/js/teacher/authoring.js") ?>

    <!-- #########################################################
        RESSOURCES LOCAL de la page -->

        <?= $helper->loadAsset("/libraries/datatables/datatables.min.css") ?>
    <?= $helper->loadAsset("/libraries/datatables/datatables-all.js") ?>

    <!-- Styles et fonctions de la page -->
    <?= $helper->loadAsset("/css/teacher/reports.css") ?>
    <?= $helper->loadAsset("/js/teacher/reports.js") ?>
    <?= $helper->loadAsset("/js/teacher/plotly.js") ?>
    <?= $helper->loadAsset("/js/teacher/assessments.js") ?>
@endsection

@section('body')
    @include('teacher/_menu', [
        'page_name' => request()->path(),
        'refresh_period' => config('labnbook.refresh_period'),
        'id_mission' => null,
        ])

        <div id="wrapper-content">
            <section class="width-full" id="reports">
                <div id="reports-filters" x-data="reportFiltersComponent">
                    <label>{{__('Filtre de classe')}}{!!__("&nbsp;:")!!}
                        <x-select2 id="reports-filter-class" on-select-function="Alpine.store('reportFilters').selectClass(selectedElement)"></x-select2>
                    </label>
                    <label>{{__('Filtre de mission')}}{!!__("&nbsp;:")!!}
                        <x-select2 id="reports-filter-mission" on-select-function="Alpine.store('reportFilters').selectMission(selectedElement)"></x-select2>
                    </label>
                </div>
                <div class="table-caption reports-table-caption">
                    <div class="table-caption-part">
                        <strong>{{__('Rapports')}}</strong>
                        <div class="dataTables_length">
                            <select class="pagination-size">
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="-1">{{__('Tous')}}</option>
                            </select>
                            / <span id="num-visible-reports"></span>
                        </div>
                        <span class="tb-search">
                            <input class="search-input" type="text" placeholder="{{__('Recherche')}}" title="{{__("Chercher un rapport par le nom d'équipe ou de ses membres")}}" />
                        </span>
                        <span class="display-archives">
                        {{__("Afficher les rapports")}}
                        <span title="{{__("Afficher les rapports archivés")}}">
                            <input id="display-archives" name="display-archives" type="checkbox"  
                                   x-data x-model="$store.reportFilters.archive"/>
                            <label for="display-archives">{{__('archivés')}}</label>
                        </span>
                        <span title="{{__("Afficher les rapports d'une mise en équipe effectuée hors de mes classes")}}">
                            <input id="display-all-classes" name="display-all-classes" type="checkbox" 
                                   x-data x-model="$store.reportFilters.teaming"/>
                            <label for="display-all-classes">{{__('hors de mes classes')}}</label>
                        </span>
                    </span>
                    </div>
                    <div class="table-caption-part">
                        <span id="number-report-selected"></span>
                        <span class="options actions" id="reports-actions">
                            <span class="lba-context-actions actions-inactive">
                                <a href="#" title="{{__("Afficher les rapports sélectionnés")}}" id="ra-see" class="batch-action"><i class="fa fa-file"></i></a>
                                <a href="#" title="{{__('Consulter les évaluations des rapports sélectionnés')}}" id="ra-assessment" class="batch-action"><i class="fas fa-table" ></i></a>
                                <a href="#" title="{{__('Modifier les options des rapports sélectionnés')}}" id="modify-selected-reports" class="batch-action"><i class="fa fa-gear"></i></a>
                                <?php if ($user->isAdmin()) : ?>
                                    <a href="#" title="{{__("Restaurer le rapport")}}" id="ra-team-restore" class="batch-action" style="display:none"><i class="fa fa-history"></i></a>
                                <?php endif ?>
                                <a href="#" title="{{__('Envoyer un message aux étudiants des rapports sélectionnés')}}" id="ra-new-conv" class="batch-action"><i class="fa fa-envelope"></i></a>
                                <a href="#" title="{{__('Envoyer un e-mail aux étudiants des rapports sélectionnés')}}" id="ra-email" class="batch-action"><i class="fa fa-at"></i></a>
                                <a href="#" title="{{__('Archiver / supprimer les rapports sélectionnés')}}" id="archive-report" class="batch-action" ><i class="fa fa-broom"></i></a>
                            </span>
                        </span>
                    </div>
                </div>
                <table id="reports-table" class="table-base<?= $user->isAdmin() ? ' always-reload' : '' ?>" style="width: auto; min-width: 100%;">
                    <thead>
                        <tr>
                            <th title="{{__('Trier par code de mission')}}">{{__('Mission')}}</th>
                            <th title="{{__("Trier par nom d'équipe")}}">{{__('Equipe')}}</th>
                            <th title="{{__('Trier par avancement')}}
{{__("- Non débuté : aucun étudiant n'a encore accédé au rapport")}}
{{__('- En cours : au moins un étudiant a accédé au rapport')}}
{{__("- Rendu : les étudiants attendent une validation par l'enseignant ; ils ne peuvent plus modifier le rapport")}}
{{__('- Archivé : le rapport est validé et non modifiable')}}">{{__('Avancement')}}</th>
                            <th title="{{__('Trier par date de dernière modification')}}">{{__('Modif.')}}</th>
                            <th title="{{__('Trier par nombre de labdocs modifiés')}}">{{__('LD')}}</th>
                            <th>{{__('Éval.')}}</th>
                            <th>
                                <input type="checkbox" id="reports-checkbox-all" title="{{__('Sélectionner tous les rapports')}}" />
                            </th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>{{__('Mission')}}</th>
                            <th>{{__('Equipe')}}</th>
                            <th>{{__('Avancement')}}</th>
                            <th>{{__('Modif.')}}</th>
                            <th>{{__('LD')}}</th>
                            <th>{{__('Éval.')}}</th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </section>
            
            <section id="traces-simulation-dashboard">
            </section>
            
            <section id="traces-heatmap">
                <h2 id="heatmap-title"></h2>
                <div id="heatmap-div"></div>
            </section>
        </div>
    

        @include('teacher/report/_teamPopup', ['teacher' => $user->teacher])
    
        @include('teacher/report/_followSettingsPopup')
        @include('teacher/_archivePopup', ['xDataFunction' => "archivePopup('reportsTable')"])

        @include('teacher/report/_batch_update_popup', [
            'teacher' => $user->teacher,
            'classes' => $classes
        ])
    
        @include('components.widgets.message.index', ['mv' => new App\Views\Widgets\Message($user->id_user, 0)])
@endsection
