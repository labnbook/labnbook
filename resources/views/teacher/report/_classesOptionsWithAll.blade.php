<?php
$sel = !$selected  ? 'selected' : '';
    if (!empty($classes)) {
        echo '<option '.$sel.' value="0">'.__('-- Toutes --').'</option>';
    }
?>
@include('teacher/report/_classesOptions', ['classes' => $classes, 'selected' => $selected])
