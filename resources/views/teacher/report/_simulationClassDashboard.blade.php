<?php
    /**
     *@var $dashboardData
     */
?>

@if(count($dashboardData['averageKnowledges'])>0 && count($dashboardData['frequentConstraints']))
    <div class="simulation-class-dashboard">
        <div class="simulation-class-dashboard-header">
            <h2 class="simulation-class-dashboard-title">{{__('Bilan du travail des étudiants')}}</h2>
        </div>
        <div class="simulation-class-dashboard-content">
            <div class="simulation-class-dashboard-section">
                <h3 class="simulation-class-dashboard-section-title">{{__('Connaissances moyennes')}}</h3>
                <div class="simulation-class-dashboard-section-content">
                    @foreach($dashboardData['averageKnowledges'] as $avgK)
                        <div>
                        <span style="font-weight: bold" title="{{round($avgK->averageKnowledge*100)}}%">
                            <i class="fas fa-circle" style="color:{{$avgK->color}}"></i> 
                            {{$avgK->description}} : 
                        </span>
                            @foreach($avgK->averageKnowledgeByTaskType as $tt)
                                <span>{{$tt->description}}</span>
                                <span>( {{$tt->tt_average_knowledge!==null?(round($tt->tt_average_knowledge*100).'%'):'-'}} )</span>
                                @if(!$loop->last)<span> , </span>@endif
                            @endforeach
                        </div>
                    @endforeach
                </div>
            </div>

            <div class="simulation-class-dashboard-section">
                <h3 class="simulation-class-dashboard-section-title">{{__('Erreurs les plus fréquentes')}}</h3>
                <div class="simulation-class-dashboard-section-content">
                    @foreach($dashboardData['frequentConstraints'] as $constraint)
                        <div>
                            <span>{{'(x'.$constraint->frequency.') - '.$constraint->constraint_message.' - '.$constraint->technical_feedback}}</span>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endif
