<?php
/** @var \App\Helper $helper */
/** @var \App\User $user */
/** @var boolean $inst_login */
/** @var boolean $ldap_cnx */
/** @var string $inst_name */
?>
<x-modal id="modal-account"
         header-class="modal-absolute"
         title="Mon compte"
         :x-data="json_encode(['displayedModal' => true, 'passwordEdition' => false, 'loginEdition' => false])"
         :include-alert="true"
         click-submit-action="displayedModal=!updateUser({{ $user->id_user }}, '{{ csrf_token() }}', loginEdition, passwordEdition);"
         click-cancel-action="displayedModal=false">
    <form autocomplete="off">
        <fieldset>
            <legend class="lb-h3">{{__('Informations personnelles')}}</legend>
            <div class="field-line">
                <label class="my-account-item-name" for='c_user_name'>{{__('Nom')}}{!!__("&nbsp;: ")!!}</label>
                <input type='text' disabled id='c_user_name' name='c_user_name' value="{!! htmlspecialchars($user->user_name) !!}"/>
            </div>
            <div class="field-line">
                <label class="my-account-item-name" for='c_first_name'>{{__('Prénom')}}{!!__("&nbsp;: ")!!}</label>
                <input type='text' disabled id='c_first_name' name='c_first_name' value="{!! htmlspecialchars($user->first_name) !!}"/>
            </div>
            <div class="field-line">
                <label class="my-account-item-name">{{__('Institution')}}{!!__("&nbsp;: ")!!}</label>
                <input type='text' disabled value="{!! htmlspecialchars($inst_name) !!}" />
            </div>
            @if ($user->inst_number)
                <div class="field-line">
                    <label class="my-account-item-name" for='c_inst_number'>{{__('N° étudiant')}}{!!__("&nbsp;: ")!!}</label>
                    <input type='text' disabled id='c_inst_number' name='c_inst_number' value="{{$user->inst_number}}"/>
                </div>
            @endif
            <div class="field-line">
                <label class="my-account-item-name" for='c_email'>{{__('E-mail')}}{!!__("&nbsp;: ")!!}</label>
                <input type='email' name='c_email' id='c_email' data-inst-login="{{$inst_login}}" data-previous-email="{{$user->email}}" value="{!! htmlspecialchars($user->email) !!}" />
            </div>
            @if(!$inst_login && !$user->email)
                <div class="account-info">{{__("Une adresse e-mail permet de récupérer un mot de passe oublié")}}</div>
            @endif
            <div class="field-line">
                <label class="my-account-item-name" for='c_lang'>{{__('Langue préférée')}}{!!__("&nbsp;: ")!!}</label>
                <select name='c_lang' id='c_lang'>
                    <option value="" label="{{__('Langue du navigateur')}}"></option>
                    @foreach($helper->allLabnbookLanguages() as $key => $value)
                        <option value="{{$key}}" {{$user->lang == $key ? "selected" : ""}}>{{$value}}</option>
                    @endforeach
                </select>
            </div>
        </fieldset>
        <fieldset x-data="{choose_cas:{{$inst_login?1:0}}}">
            <legend class="lb-h3">{{__('Informations de connexion')}}</legend>
            @if($inst_login)
                <div class="comment account-info">{{__('Vous utilisez votre compte institutionnel :inst pour vous connecter', ['inst' => $inst_name ])}}</div>
            @else
                @if ($ldap_cnx)
                    <div id="c_use_inst_account" class="field-line">
                        <span>{{__("Utiliser votre compte institutionnel :inst pour vous connecter", [ 'inst' => $inst_name ])}}</span>
                        <input x-model="choose_cas" type="checkbox" id="c_choose_cas_authentication">
                        <i class="fa fa-info-circle" id="c_inst_account_tooltip" onclick="openToolTipEditUser()" onmouseover="openToolTipEditUser()"></i>
                    </div>
                @endif
                <div class="more_info" x-show="!choose_cas">
                    <div class="field-line">
                        <label class="my-account-item-name" for='c_login'>{{__('Identifiant')}}{!!__("&nbsp;: ")!!}</label>
                        <input type='text' required id='c_login' name='c_login' value="{!! htmlspecialchars($user->login) !!}" placeholder="{{__('nécessaire')}}" :disabled="!loginEdition"/>
                        <button type="button" :class="{'disabled':!loginEdition}" title="{{__("Modifier l'identifiant")}}" x-on:click="loginEdition=!loginEdition">
                            <i id="mod_login" class="fas fa-pencil-alt"></i>
                        </button>
                    </div>
                    <div class="field-line" x-data="{type_password:true}">
                        <label class="my-account-item-name" for='prev_pwd' >{{__('Mot de passe')}}{!!__("&nbsp;: ")!!}</label>
                        <input :type="type_password?'password':'text'" autocomplete="off" id='prev_pwd' name='prev_pwd'  :placeholder="(passwordEdition || loginEdition)?'{{__('nécessaire')}}':'{{__('inchangé')}}'" :disabled="!passwordEdition && !loginEdition">
                        <button type="button" x-show="passwordEdition" x-on:click="type_password=!type_password">
                            <i :class="{'fa-eye-slash':!type_password, 'fa-eye':type_password}" class="far"></i>
                        </button>
                        <button type="button" :class="{'disabled':!passwordEdition}" title="{{__('Modifier le mot de passe')}}" x-on:click="passwordEdition=!passwordEdition">
                            <i class="fas fa-pencil-alt"></i>
                        </button>
                    </div>
                    <div id="change-password" x-show="passwordEdition">
                        <div class="field-line" x-data="{type_password:true}">
                            <label class="my-account-item-name" for="c_pwd">{{__('Nouveau mot de passe')}}{!!__("&nbsp;: ")!!}</label>
                            <input :type="type_password?'password':'text'" id="c_pwd" name="c_pwd" autocomplete="off"/>
                            <button type="button" x-on:click="type_password=!type_password">
                                <i class="far" :class="{'fa-eye-slash':!type_password, 'fa-eye':type_password}"></i>
                            </button>
                        </div>
                        <div class="field-line" x-data="{type_password:true}">
                            <label class="my-account-item-name" for="c_pwd_2">{{__('Confirmation')}}{!!__("&nbsp;: ")!!}</label>
                            <input :type="type_password?'password':'text'" autocomplete="off" id="c_pwd_2" name="c_pwd_2"/>
                            <button type="button" x-on:click="type_password=!type_password">
                                <i :class="{'fa-eye-slash':!type_password, 'fa-eye':type_password}" class="far fa-eye"></i>
                            </button>
                        </div>
                        <div class="account-info">{{__('8 à 20 caractères minuscules ET majuscules')}}</div>
                    </div>
                </div>
            @endif
        </fieldset>
        <fieldset>
            <div class="field-line">
                <label class="my-account-item-name" for='c_code'>{{__('Rejoindre une classe')}}{!!__("&nbsp;: ")!!}</label>
                <input type='text' id='c_code' name='c_code' placeholder="{{__("code d'inscription")}}" value="" />
            </div>
        </fieldset>
    </form>
</x-modal>
