<form id="frm-logout" action="/logout" method="POST" style="display: none;">
    <input type="hidden" id="logout-message" name="message" value=""></input>
    @csrf
</form>
<script>
    /**
     * Logs out from the application
     * @param {String} message an error message to display after logout
     */
    function logout(message){
        if (typeof message != "undefined") {
            document.getElementById('logout-message').value = message;
        }
        document.getElementById('frm-logout').submit();
    }
</script>
