@pushonce('component_css')
    {!! $helper->loadAsset("/css/widgets/textarea.css"); !!}
@endpushonce
<div class="textarea-wrapper {{$customWrapperClass}}">
    <textarea class="textarea-content @isset($custom){{$custom}}@endisset"
              x-data="{{$xData}}"
              x-init="{{$xInit}}"
              x-model="{{$xModel}}"
              x-on:focus="{{$xOnFocus}}"
              x-on:blur="{{$xOnBlur}}"
              placeholder="{{$placeholder}}"
              rows="1"></textarea>
    <div x-text="{{$xModel}}+' '"
         class="div-target @isset($custom){{$custom}}@endisset">
    </div>
</div>
