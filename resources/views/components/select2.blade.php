<?php
/**
 * @var string $id id for the select
 * @var string $name name for the select
 * @var string $selectClasses html classes for the select
 * @var string $onSelectFunction function bind to on select event
 */
?>
<div x-data="select2Component('{{$id}}')" title="{{$title}}" style="display: inline-block"
     x-on:lnb-select2-remove-id.window="removeId($event)"
     x-on:lnb-select2-load.window="await $nextTick();
    if($event.detail.selector === selector){
        initSelect2($event.detail.configSelect2);
        @isset($onSelectFunction)
            $watch('selectedElement', () => { 
                if(selectedElement || !configSelect2.clearOnSelect) {
                    {{$onSelectFunction}} 
                    if(configSelect2.clearOnSelect) {
                        $($refs.select2).val(null).trigger('change.select2');
                    }
                }
            });
        @endisset
    }"
>
    <i x-show="!initialized" class="fa fa-spinner fa-pulse"></i>
    <select x-show="initialized" x-ref="select2" type="text" id="{{$id}}" name="{{$name}}" class="{{$selectClasses}}">
    </select>
</div>
