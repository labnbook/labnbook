@once 
    {!! $helper->loadAsset("/css/widgets/widget.css"); !!} 
@endonce
<div class="widget-wrapper" id="{{$id}}" x-show="{{ $xShow }}" x-data>
    <div x-ref="header" class="widget-header @isset($headerClass) {{$headerClass}} @endisset">
        <div class="widget-title-wrapper">
            <h2 class="widget-title"> {{ $title }} </h2>
            @isset($headerSideTitle)
                {{ $headerSideTitle }}
            @endisset
        </div>
        @isset($header)
            {{ $header }}
        @endisset
    </div>
    <div class="widget-slot">
        {{ $slot }}
    </div>
</div>
