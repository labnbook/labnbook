@once
    {!! $helper->loadAsset("/css/widgets/comment.css"); !!}
    {!! $helper->loadAsset("/js/widgets/comment.js"); !!}
@endonce
<x-widget :id="$id" :x-show="$xShow" :header-class="'comment-header'">
    <x-slot:title>
        {{__('Commentaires')}}
    </x-slot:title>
    <x-slot:header>
        <div x-data="{ open: false }" class="comment-ld-select lb-custom-select" x-id="['dropdown-button']">
            <button x-on:click="open = !open" class="lb-custom-select-selector" type="button" :aria-expanded="open" :aria-controls="$id('dropdown-button')">
                <span x-text="$store.commentWidgetIdLd?$store.reportLabdocs.getLdName($store.commentWidgetIdLd):'{{__('Choisissez un labdoc')}}'"></span>
                <i class="fa" :class="{'fa-chevron-down':!open, 'fa-chevron-up':open}"></i>
            </button>
            <div x-show="open" x-on:click.outside="open = false" class="lb-custom-select-dropdown" :id="$id('dropdown-button')" x-transition.origin.top.right x-cloak>
                <div x-on:click="$store.commentWidgetIdLd = null; open = false;"
                         class="lb-custom-select-dropdown-option">
                    <span style="font-style: italic">{{__('Choisissez un labdoc')}}</span>
                </div>
                <template x-for="labdoc in $store.reportLabdocs.getSortedLabdocs()" :key="labdoc.id_ld+''+labdoc.ld_position+''+labdoc.rp_position">
                    <div x-on:click="$store.commentWidgetIdLd = labdoc.id_ld; open = false;"
                         class="lb-custom-select-dropdown-option" :class="{'lb-custom-select-highlighted-option':labdoc.new_com}">
                        <i class="fa fa-circle lb-custom-select-option-icon"></i>
                        <span x-text="labdoc.name"></span>
                    </div>
                </template>
            </div>
        </div>
    </x-slot:header>
    <!-- liste des commentaires -->
    <div id="comment_wrapper" x-data="commentWidget()" x-init="$watch('$store.commentWidgetIdLd',(id_ld)=>displayComments(id_ld))">
        <div id="comment_messages" x-html="commentLdView" x-on:report-display-comment.window="displayComments($event.detail.id_ld)">
            <!-- ldComments view -->
        </div>
        <div id="comment_send" class="widget-comment-send-wrapper" x-show="$store.commentWidgetIdLd"> <!-- l'envoi des messages -->
            <div id="comment_send_wrapper_left" class="widget-comment-send-wrapper-left">
                <textarea id="text_comment_send" class="widget-comment-send-textarea" name="message" x-model="currentComment"></textarea>
            </div>
            <div id="comment_send_wrapper_right" class="widget-comment-send-wrapper-right">
                <button class="widget-comment-send-btn" :disabled="addCommentDisabled" x-on:click="addComment()">
                    <i :class="{'fa-regular fa-paper-plane':!addCommentDisabled, 'fa fa-spinner fa-pulse':addCommentDisabled}" title="{{__('Envoyer le commentaire')}}"></i>
                </button>
            </div>
        </div>
    </div>
    <script>
        document.addEventListener('alpine:init', () => {
            Alpine.store('commentWidgetIdLd', '');
        });
    </script>
</x-widget>
