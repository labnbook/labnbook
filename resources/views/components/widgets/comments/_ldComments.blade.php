<?php
/** @var \App\Helper $helper */
/** @var \App\Labdoc $labdoc */
$user = Auth::user();
?>
@if (!$labdoc->comments()->exists())
    <?php
    $message = $labdoc->shared ? __("Les commentaires sont partagés entre les toutes les équipes de la classe. Ils ne sont pas visibles par l'enseignant") : __("Les commentaires sont partagés entre les membres de l'équipe. Ils ne sont pas visibles par l'enseignant.");
    ?>
    <div class="comment_warning">{{$message}}</div>
@else
    @foreach ($labdoc->comments()->orderBy('comment_time', 'ASC')->get() as $comment)
            <?php
            if ($comment->id_user == $user->id_user) {
                $sender = __("Moi : ");
                $c_user = $user;
            } else {
                $c_user = \App\User::find($comment->id_user);
                $sender = mb_substr($c_user->first_name,0,1) . mb_substr($c_user->user_name, 0, 1) . __("&nbsp;: ");
            }
            ?>
        <div class="comment_msg
        @if($comment->id_user == $user->id_user)
            lnb-hoverable
        @endif
        ">
            @if($comment->id_user == $user->id_user)
                <div class="widg-btn-box">
                    <i class="far fa-trash-alt lnb-btn" title="{{__("Supprimer mon commentaire")}}"
                       x-on:click="deleteComment({{$comment->id_comment}}, {{$comment->id_labdoc}})"></i>
                </div>
            @endif
            <span class="comment_time">{{ $helper->relativeDate($comment->comment_time)}}</span>
            <span class="comment_sender" title="{{$c_user->first_name}} {{$c_user->user_name}}">{!! $sender !!}</span>
                <span class="comment-text">{!! $comment->content!!}</span>
        </div>
    @endforeach
@endif
