<div class="final-assessment-comment-wrapper">
    <div class="final-assessment-comment-content">
        <span x-text="assessment.score_comment" :title="assessment.score_comment"
              x-on:click="global_tooltip_popup.openTooltipPopup('', assessment.score_comment, $el)"></span>
    </div>
</div>
