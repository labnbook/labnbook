<div class="criterion-description-wrapper">
    <div class="neutralize-criterion" x-show="assessment.editable"
         :title="assessmentCtrl.isActive(criterion) ? '{{__("Neutraliser ce critère")}}' : '{{__("Ré-activer ce critère")}}'"
         x-on:click="assessmentCtrl.toggleActive(criterion)">
        <i class="fas" :class="assessmentCtrl.isActive(criterion) ? 'fa-eye-slash' : 'fa-eye'"></i>
    </div>
    <div class="description-wrapper">
        <span x-text="criterion.description" :title="criterion.description"></span>
    </div>
</div>
<div class="criterion-weight"
     :title="'{{ __("Poids relatif de ce critère") }}'">
    <i class="fas fa-weight-hanging"></i>
    <span x-text="assessmentCtrl.displayWeight(criterion.relative_weight, criterion.computed_relative_weight, 2)"></span>
</div>
