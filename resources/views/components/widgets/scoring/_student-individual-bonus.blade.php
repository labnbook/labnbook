<template x-if="student = assessmentCtrl.getIndividualBonus({{Auth::id()}})">
    <tr class="bonus-malus individual-bonus-malus">
        <td class="bonus-malus-title"
                                      
            >
            <div>
                <span  x-text="__('Bonus / Malus individuel')"></span>
            </div>
        </td>
        <td :colspan="viewConfig.columnsNumber - 2" class="bonus-malus-comment">
            <div class="bonus-malus-comment-wrapper">
                <div class="bonus-malus-comment-content">
                    <span x-text="student.bonus_comment" :title="student.bonus_comment"></span>
                </div>
            </div>
        </td>
        <td class="bonus-malus-score">
            <div class="bonus-malus-score-wrapper">
                <div class="bonus-malus-score-content">
                    <span x-text="assessmentCtrl.displayIndividualBonusScore(student.bonus, 'bonus')"></span>
                </div>
            </div>
        </td>
    </tr>
</template>
