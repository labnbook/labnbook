<template x-if="criterion.type === 'descr' && index < criterion.descrobs.length - 1">
    <div class="two-descriptors-selection"
         :class="{selected: descrob.selected && criterion.descrobs[index + 1].selected}"
         x-on:click.stop="criterion.descrobs = await assessmentCtrl.selectAdjacentDescr(criterion, descrob)">
        <span class="fa-stack fa-2x">
            <i class="fas fa-circle fa-stack-1x"></i>
            <i class="far fa-circle fa-stack-1x"></i>
        </span>
    </div>
</template>
