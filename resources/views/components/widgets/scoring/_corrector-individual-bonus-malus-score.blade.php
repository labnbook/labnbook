<div class="bonus-malus-score-wrapper" x-data="{openBonusMalusScoreInput: assessmentCtrl.isEmpty(student.bonus)}">
    <div class="bonus-malus-score-content" x-show="!assessmentCtrl.isEmpty(student.bonus) && student.bonus !== 0 && !openBonusMalusScoreInput"
                                           x-on:click="openBonusMalusScoreInput = assessment.editable ? true : false">
        <span x-html="assessmentCtrl.displayIndividualBonusScore(student.bonus)"></span>
    </div>
    <div class="bonus-malus-score-editor"
         x-show="assessment.editable && (assessmentCtrl.isEmpty(student.bonus) || student.bonus === 0  || openBonusMalusScoreInput)"
         x-on:click.outside="openBonusMalusScoreInput = student.bonus === 0">
        <input type="text" lang="{{$lang}}" x-model="student.bonus"
                                            x-on:change="openBonusMalusScoreInput = student.bonus === 0"
                                            x-on:keyup.enter="openBonusMalusScoreInput = student.bonus === 0">
    </div>
</div>
