<div class="bonus-malus-comment-wrapper" x-data="{openBonusMalusCommentInput: assessmentCtrl.isEmpty(assessment.bonus_comment) }">
    <div class="bonus-malus-comment-content" x-show="!assessmentCtrl.isEmpty(assessment.bonus_comment) && !openBonusMalusCommentInput"
         x-on:click="openBonusMalusCommentInput = assessment.editable ? true : false">
        <span x-text="assessment.bonus_comment" :title="assessment.bonus_comment"></span>
    </div>
    <div class="bonus-malus-comment-editor" x-show="assessment.editable && (assessmentCtrl.isEmpty(assessment.bonus_comment) || openBonusMalusCommentInput)"
         x-on:click.outside="openBonusMalusCommentInput = assessmentCtrl.isEmpty(assessment.bonus_comment)">
        <textarea type="text" x-model="assessment.bonus_comment" rows="2" placeholder="{{__("Commentaire...")}}"></textarea>
    </div>
</div>
