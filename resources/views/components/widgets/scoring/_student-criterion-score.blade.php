<div class="criterion-comment" x-show="!assessmentCtrl.isEmpty(criterion.comment)">
    <span x-text="criterion.comment" :title="criterion.comment"
          x-on:click="global_tooltip_popup.openTooltipPopup('', criterion.comment, $el)">
    </span>
</div>
