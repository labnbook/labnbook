<div class="bonus-malus-comment-wrapper" x-data="{openBonusMalusCommentInput: assessmentCtrl.isEmpty(student.bonus_comment) }">
    <div class="bonus-malus-comment-content" x-show="!assessmentCtrl.isEmpty(student.bonus_comment) && !openBonusMalusCommentInput"
                                             x-on:click="openBonusMalusCommentInput = assessment.editable ? true : false">
        <span x-text="student.bonus_comment" :title="student.bonus_comment"></span>
    </div>
    <div class="bonus-malus-comment-editor" x-show="assessment.editable && (assessmentCtrl.isEmpty(student.bonus_comment) || openBonusMalusCommentInput)"
                                            x-on:click.outside="openBonusMalusCommentInput = assessmentCtrl.isEmpty(student.bonus_comment)">
        <textarea type="text" x-model="student.bonus_comment" rows="2" placeholder="{{__("Commentaire...")}}"></textarea>
    </div>
</div>
