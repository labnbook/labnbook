<template x-if="assessment.editable === true && assessment.assessed_students != null">
    <span class="students-checkboxes-wrapper">
        <template x-for="student in assessment.assessed_students.sort((a, b) => a.name.localeCompare(b.name))" :key="student.id_student">
            <div class="student-checkbox-wrapper">
                <input type="checkbox" x-model="student.missing" x-show="assessment.editable === true"
                       :checked="student.missing === 1" :id="'student_checkbox_' + student.id_student">
                <label :for="'student_checkbox_' + student.id_student"
                       x-text="student.name" x-show="student.missing || assessment.editable === true">
                </label></div><!-- do not break this line -->
        </template>
    </span>
</template>
<template x-if="assessment.editable !== true && assessment.assessed_students != null">
    @include('components.widgets.scoring._student-missing-student-list')
</template>
