<div class="students-list-wrapper">
    <template x-for="student in assessment.assessed_students.filter(s => s.missing)" :key="student.id_student">
        <div class="student-wrapper"><span x-text="student.name"></span></div><!-- do not break this line -->
    </template>
</div>
