<div class="bonus-malus-score-wrapper" x-data="{openBonusMalusScoreInput: assessmentCtrl.isEmpty(assessment.bonus)}">
    <div class="bonus-malus-score-content" x-show="!assessmentCtrl.isEmpty(assessment.bonus) && !openBonusMalusScoreInput"
         x-on:click="openBonusMalusScoreInput = assessment.editable ? true : false">
        <span x-text="assessmentCtrl.displayBonusScore(assessment.bonus)"></span>
    </div>
    <div class="bonus-malus-score-editor"
         x-show="assessment.editable && (assessmentCtrl.isEmpty(assessment.bonus) || openBonusMalusScoreInput)"
         x-on:click.outside="openBonusMalusScoreInput = assessmentCtrl.updateBonusScore(assessment.bonus)">
        <input type="text" lang="{{$lang}}" x-model="assessment.bonus"
               :placeholder="'&nbsp;' + assessment.min_bonus + '&nbsp;' + '{{__("à")}}' + '&nbsp;' + assessment.max_bonus"
               x-on:change="openBonusMalusScoreInput = assessmentCtrl.updateBonusScore(assessment.bonus)"
               x-on:keyup.enter="openBonusMalusScoreInput = assessmentCtrl.updateBonusScore(assessment.bonus)">
    </div>
</div>
