<input type="checkbox" 
       :id="'checkbox_' + descrob.id_descrob"
       :checked="descrob.selected"
       x-on:click="$event.preventDefault()">
<div class="hidden-checkbox-wrapper">
    <div class="hidden-checkbox"></div>
</div>
<label :for="'checkbox_' + descrob.id_descrob" 
       x-text="descrob.description" 
       :title="descrob.description">
</label>
