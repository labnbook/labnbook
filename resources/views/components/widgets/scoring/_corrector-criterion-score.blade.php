<div class="criterion-comment" x-show="!assessmentCtrl.isEmpty(criterion.comment) && !openCriterionTextArea"
     x-on:click="openCriterionTextArea = (assessment.editable && assessmentCtrl.isActive(criterion)) ? true : false">
    <span x-text="criterion.comment" :title="criterion.comment"></span>
</div>
<div class="criterion-comment-editor" x-show="assessment.editable && assessmentCtrl.isActive(criterion) && (assessmentCtrl.isEmpty(criterion.comment)|| openCriterionTextArea)"
     x-on:click.outside="openCriterionTextArea = assessmentCtrl.isEmpty(criterion.comment)">
    <textarea type="text" x-model="criterion.comment" rows="1" placeholder="{{__("Commentaire")}}"></textarea>
</div>
