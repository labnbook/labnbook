<!-- Assessment is not editable -->
<button type="button" class="assessment-header-btn" x-show="!waiting && assessment.editable === false"
        x-on:click="waiting = true; await assessmentCtrl.toggleEditable(false); waiting = false">
    <i class="fa fa-undo"></i>
    {{__("Éditer")}}
</button>
<!-- Assessment is editable -->
<div class="assessment-reinit-button-wrapper" x-show="!waiting && assessment.editable === true">
    @includeWhen($is_corrector, 'components.widgets.scoring._corrector-assessment-reinit-button')
</div>
