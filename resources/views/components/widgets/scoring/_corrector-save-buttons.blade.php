@php
    $return_title = __("Rendre l’évaluation aux étudiants");
    $return_message = __("Lorsque l’évaluation est rendue à l’étudiant, ce dernier peut la consulter depuis son rapport.")
            . "<br/>"
            . __("Attention : le rendu est définitif et implique que l’évaluation n’est plus modifiée en cas de changements effectués dans la grille de notation au niveau de la mission.");
@endphp

<template x-if="assessment !== null">
    <div class="buttonbox">
        <i class="fa fa-spinner fa-pulse action_icon" x-show="waiting"></i>
        <button type="button" class="assessment-header-btn" x-on:click="waiting = true; message = await assessmentCtrl.save(false, false); waiting = false"
                x-show="!waiting && (assessment.published === 0 || assessment.editable === true)">
            <i class="fa fa-check"></i>
            {{__("Enregister")}}
        </button>
        <div class="button-tooltip-wrapper">
            <button type="button" class="assessment-header-btn danger" title="{{ $return_title }}"
                    x-show="!waiting && assessment.published === 0"
                    x-on:click="waiting = true; await assessmentCtrl.toggleEditable(true); waiting = false">
                <i class="fa fa-globe"></i>
                {{__("Rendre")}}
            </button>
            <template x-if="!waiting && assessment.published === 0">
                <span class="fa fa-info-circle"
                      x-on:mouseover="global_tooltip_popup.openTooltipPopup('{{ $return_title }}', '{{ $return_message }}', $el)"
                      x-on:click="global_tooltip_popup.openTooltipPopup('{{ $return_title }}', '{{ $return_message }}', $el)">
                </span>
            </template>
        </div>
        <template x-if="assessment.published === 0">
            <div class="assessment-reinit-button-wrapper" x-show="!waiting">
                @includeWhen($is_corrector && !$is_test, 'components.widgets.scoring._corrector-assessment-reinit-button')
            </div>
        </template>
        <template x-if="assessment.published === 1">
            <div class="assessment-reinit-button-wrapper" x-show="!waiting">
                @includeWhen($is_corrector && !$is_test, 'components.widgets.scoring._corrector-assessment-edit-reinit-button')
            </div>
        </template>
    </div>
</template>
