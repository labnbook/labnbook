@php
    $reinit_title = __("Réinitialiser l\\'évaluation");
    $reinit_message = __("Toute l\\'évaluation va être supprimée et va être réinitialisée selon la définition actuelle de la grille critériée au niveau de la mission.");
@endphp

<button type="button" class="assessment-header-btn danger" title="{{ $reinit_message }}"
        x-on:click="waiting = true; await assessmentCtrl.reinit(); waiting = false">
    <i class= "fa fa-trash"></i>
    {{__("Réinitialiser")}}
</button>
<span class="fa fa-info-circle"
      x-on:mouseover="global_tooltip_popup.openTooltipPopup('{{ $reinit_title }}', '{{ $reinit_message }}', $el)"
      x-on:click="global_tooltip_popup.openTooltipPopup('{{ $reinit_title }}', '{{ $reinit_message }}', $el)">
</span>
