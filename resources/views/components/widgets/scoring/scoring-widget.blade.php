@php
    /** @var \App\Helper $helper */
    /** @var \App\Views\Report $rv*/
    
    $editable = $rv->is_follow || $rv->is_test || $rv->is_solution;
    $lang = Lang::Locale();
    $is_corrector = $editable;
    $is_student = !$editable;
    $who = "";
    if ($is_corrector) {
        $who = "corrector"; 
    } elseif ($is_student) {
        $who = "student";
    }
    $rubric_broadcast = $rv->mission->rubric_broadcast;
    $is_test = $rv->is_test || $rv->is_solution;
    $is_view_as_teacher = $rv->is_test || $rv->is_solution || $rv->is_follow;
@endphp

@once
    {!! $helper->configureAjax();  !!}
    {!! $helper->loadAsset("/css/widgets/scoring.css"); !!}
    {!! $helper->loadAsset("/js/widgets/scoring.js"); !!}
@endonce

<div x-data="{
            widgetLoaded : false,
            scoringVisible: true,
            waiting: false,
            message: '',
            assessment: {},
            id_production: window.global_id_report,
            viewConfig: {}
        }"
    x-init="
            assessment = await assessmentCtrl.init(id_production, {{$is_view_as_teacher}}); 
            widgetLoaded = true; 
            assessmentCtrl.setAssessmentProxy(assessment); 
            viewConfig = assessmentCtrl.getViewConfig('{{ $who }}', assessment);
            if (scoringVisible) { 
                assessmentCtrl.assessmentShown('{{ $who }}');
            }
        "
     style="height: 100%"
     x-show="{{$xShow}}">
    <!--  Div contenant la grille critériée -->
    <x-widget :id="$id" :x-show="$xShow" :header-class="'assessment-header'">
        <x-slot:title>
            {{__('Évaluation')}}
        </x-slot:title>
        <x-slot:header>
            <template x-if="assessment === null">
                <div class="assessment-status-wrapper">
                    <div class="assessment-published-wrapper">
                        <span class="assessment-header-label">({{__("non rendue")}})</span>
                    </div>
                </div>
            </template>
            <template x-if="assessment !== null">
                @if($is_test)
                    <div class="assessment-status-wrapper">
                        <div class="assessment-published-wrapper">
                            <span class="assessment-header-label">({{__("test")}})</span>
                        </div>
                    </div>
                @else
                    <div class="assessment-status-wrapper">
                        <!-- Assessment is published -->
                        <template x-if="assessment.published === 1">
                            <div class="assessment-published-wrapper">
                                <span class="assessment-header-label">({{__("rendue")}})</span>
                            </div>
                        </template>
                        <!-- Assessment is not published -->
                        <template x-if="assessment.published === 0">
                            <div class="assessment-published-wrapper">
                                <span class="assessment-header-label">({{__("non rendue")}})</span>
                            </div>
                        </template>
                    </div>
                @endif
            </template>
            @includeWhen($is_corrector && !$is_test, 'components.widgets.scoring._corrector-save-buttons')
            
        </x-slot:header>
        <div id="widg-scoring-content" class="drag_popup_scoring"
             data-id-report="{{ $rv->report->id_report }}"
             x-show="scoringVisible"
             x-on:widget-shown.camel="if (Object.keys(assessment).length !== 0) { assessmentCtrl.assessmentShown('{{ $who }}'); }">
            @if($is_corrector)<div class="error" x-show="message !== ''" x-text="message"></div>@endif
            <template x-if="assessment === null">
                <div class="assessment-missing-student-wrapper">
                </div>
            </template>
            <template x-if="assessment !== null">
                @if($is_test)
                    <div class="assessment-missing-student-wrapper">
                        <span class="assessment-header-label"></span>
                    </div>
                @else
                    <div class="assessment-missing-student-wrapper">
                        <span class="assessment-header-label"
                              @if($is_corrector)
                                  x-show="assessmentCtrl.isStudentMissingLabelShown(assessment) || (assessment.editable === true && assessment.assessed_students && assessment.assessed_students.length > 0)"
                              @elseif($is_student)
                                  x-show="assessmentCtrl.isStudentMissingLabelShown(assessment)"
                            @endif>
                            {{__("Étudiants absents : ")}}
                        </span>
                        @if($is_corrector)
                            @include('components.widgets.scoring._corrector-missing-student-list')
                        @elseif($is_student)
                            <template x-if="assessment.assessed_students != null">
                                @include('components.widgets.scoring._student-missing-student-list')
                            </template>
                        @endif
                    </div>
                @endif
            </template>
            <!-- Assessment table -->
            <template x-if="assessment !== null">
                <table id="assessment" :class="{ corrector: {{ $is_corrector ? 'true' : 'false' }}, student: {{ $is_student ? 'true' : 'false' }} }"
                       @if($is_student)x-show="assessment.published || {{ $rubric_broadcast ? 'true' : 'false' }}"@endif
                       @if($is_corrector)x-show="!waiting"@endif>
                    <template x-for="group in assessment.criteria_group" :key="group.id_criteria_group">
                        <tbody @if($is_student)x-show="assessmentCtrl.isGroupActive(group)"@endif>
                        <!-- Criterion group row -->
                        <tr class="group">
                            <td class="group-description" :colspan="viewConfig.columnsNumber - 1"
                                :style="{'width': 100 - viewConfig.criterionScorePercentWidth + '%'}">
                                <div>
                                <span x-text="group.description" :title="group.description"></span>
                                </div>
                                @if($is_corrector)
                                    <div class="group-weight"
                                         :title="'{{ __("Poids relatif de ce groupe") }}'">
                                        <i class="fas fa-weight-hanging"></i>
                                        <span x-text="assessmentCtrl.displayWeight(group.relative_weight, group.computed_relative_weight, 2)"></span>
                                    </div>
                                @endif
                            </td>
                            <td class="group-score" :style="{'width': viewConfig.criterionScorePercentWidth + '%'}">
                                <div>
                                <span class="score-value"
                                      x-text="assessmentCtrl.displayScore(group.computed_score, group.computed_maximum_reachable_score, 1)"
                                ></span>
                                </div>
                            </td>
                        </tr>
                        <template x-for="criterion in assessmentCtrl.getCriteriaFromGroup(group)" :key="criterion.id_criterion">
                            <!-- Criterion row -->
                            <tr class="criterion"
                                @if($is_corrector)
                                    :class="{
                                    'criterion-activated': assessmentCtrl.isActive(criterion),
                                    'criterion-neutralized': !assessmentCtrl.isActive(criterion),
                                    'assessment-editable': assessment.editable,
                                    'assessment-readonly': !assessment.editable,
                                    descriptor: criterion.type === 'descr', 
                                    observable: criterion.type === 'ob'
                                }"
                                @elseif($is_student)
                                    :class="{
                                    descriptor: criterion.type === 'descr', 
                                    observable: criterion.type === 'ob'
                                }"
                                x-show="assessmentCtrl.isActive(criterion)"
                                @endif>
                                <!-- Criterion description column -->
                                <td class="criterion-description" :style="{'width': viewConfig.criterionTitlePercentWidth + '%'}">
                                    @includeWhen($is_corrector, 'components.widgets.scoring._corrector-criterion-description')
                                    @includeWhen($is_student, 'components.widgets.scoring._student-criterion-description')
                                </td>
                                <!-- Criterion descriptors/observables columns -->
                                <template x-for="descrob, index in criterion.descrobs" :key="descrob.id_descrob">
                                    <td class="descrob"
                                        :class="{selected: descrob.selected}"
                                        :colspan="(viewConfig.columnsNumber - 2) / criterion.descrobs.length"
                                        :style="{ 'width': viewConfig.descrobColumnPercentWidth * (viewConfig.columnsNumber - 2) / criterion.descrobs.length + '%' }">
                                        <template x-if="criterion.type === 'descr'">
                                            <div class="criterion-descrob-wrapper"
                                                 @if($is_corrector)x-on:click="descrob.selected = await assessmentCtrl.selectOrToggleDescrob(criterion, descrob)"@endif>
                                            <span x-text="descrob.description" :title="descrob.description"></span>
                                            </div>
                                        </template>
                                        @includeWhen($is_corrector, 'components.widgets.scoring._corrector-two-descriptors-selector')
                                        <template x-if="criterion.type === 'ob'">
                                            <div class="criterion-descrob-wrapper">
                                                @includeWhen($is_corrector, 'components.widgets.scoring._corrector-observable-checkbox')
                                                @includeWhen($is_student, 'components.widgets.scoring._student-observable-checkbox')
                                            </div>
                                        </template>
                                        @if($is_corrector)
                                            <div class="descrob-weight"
                                                 :title="criterion.type === 'descr' ? '{{ __("Poids relatif de ce descripteur") }}' : '{{ __("Poids relatif de cet observable") }}'">
                                                <i class="fas fa-weight-hanging"></i>
                                                <span x-text="assessmentCtrl.displayWeight(descrob.value, descrob.value, 2)"></span>
                                            </div>
                                        @endif
                                    </td>
                                </template>
                                <!-- Criterion score column -->
                                <td class="criterion-score" :style="{'width': viewConfig.criterionScorePercentWidth + '%'}">
                                    <div class="criterion-score-wrapper" x-data="{openCriterionTextArea: assessmentCtrl.isEmpty(criterion.comment)}">
                                        @includeWhen($is_corrector, 'components.widgets.scoring._corrector-criterion-score')
                                        @includeWhen($is_student, 'components.widgets.scoring._student-criterion-score')
                                        <div class="criterion-grade">
                                            <span x-text="assessmentCtrl.displayScore(criterion.computed_score, criterion.computed_maximum_reachable_score, 1)"></span>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </template>
                        </tbody>
                    </template>
                    <tbody>
                    <tr class="separator">
                        <td :colspan="viewConfig.columnsNumber"></td>
                    </tr>
                    </tbody>
                    <template
                        @if($is_corrector)
                            x-if="(assessment.editable || !assessmentCtrl.isEmpty(assessment.bonus_comment) || !assessmentCtrl.isEmpty(assessment.bonus)) 
                        && !(assessment.min_bonus === 0 && assessment.max_bonus === 0)"
                        @elseif($is_student)
                            x-if="(!assessmentCtrl.isEmpty(assessment.bonus_comment) || !assessmentCtrl.isEmpty(assessment.bonus)) 
                        && !(assessment.min_bonus === 0 && assessment.max_bonus === 0)"
                        @endif>
                        <tbody>
                        <tr class="bonus-malus"
                            @if($is_corrector):class="{'assessment-editable': assessment.editable, 'assessment-readonly': !assessment.editable}"@endif>
                            <!-- Bonus comment column -->
                            <td class="bonus-malus-title" :style="{'width': viewConfig.criterionTitlePercentWidth + '%'}">
                                <div>
                                    <span>{{__("Bonus / Malus")}}</span>
                                </div>
                            </td>
                            <td class="bonus-malus-comment" :colspan="viewConfig.columnsNumber - 2">
                                @includeWhen($is_corrector, 'components.widgets.scoring._corrector-bonus-malus-comment')
                                @includeWhen($is_student, 'components.widgets.scoring._student-bonus-malus-comment')
                            </td>
                            <td class="bonus-malus-score">
                                @includeWhen($is_corrector, 'components.widgets.scoring._corrector-bonus-malus-score')
                                @includeWhen($is_student, 'components.widgets.scoring._student-bonus-malus-score')
                            </td>
                        </tr>
                        </tbody>
                    </template>
                    <template x-if="!assessmentCtrl.isEmpty(assessment.score)">
                        <tbody>
                        @includeWhen(!$is_corrector && !$is_test, 'components.widgets.scoring._student-individual-bonus')
                        <tr class="final-assessment"
                            @if($is_corrector):class="{'assessment-editable': assessment.editable, 'assessment-readonly': !assessment.editable}"@endif>
                            <td class="final-assessment-comment" :colspan="viewConfig.columnsNumber - 1">
                                @includeWhen($is_corrector, 'components.widgets.scoring._corrector-final-assessment-comment')
                                @includeWhen($is_student, 'components.widgets.scoring._student-final-assessment-comment')
                            </td>
                            <td class="final-assessment-score">
                                <div>
                                    <span x-text="assessmentCtrl.displayScore(assessmentCtrl.getIndividualScore({{Auth::id()}}, '{{$is_corrector}}'), assessment.grading, 1)"></span>
                                </div>
                            </td>
                        </tr>
                        @includeWhen($is_corrector && !$is_test, 'components.widgets.scoring._corrector-individual-bonus')
                        </tbody>
                    </template>
                </table>
            </template>
        </div>
    </x-widget>
</div>
