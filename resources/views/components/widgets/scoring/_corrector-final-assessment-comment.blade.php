<div class="final-assessment-comment-wrapper" x-data="{openGlobalCommentInput: assessmentCtrl.isEmpty(assessment.score_comment)}">
    <div class="final-assessment-comment-content" x-show="!assessmentCtrl.isEmpty(assessment.score_comment) && !openGlobalCommentInput"
         x-on:click="openGlobalCommentInput = assessment.editable ? true : false">
        <span x-text="assessment.score_comment" :title="assessment.score_comment"></span>
    </div>
    <div class="final-assessment-comment-editor" x-show="assessment.editable && (assessmentCtrl.isEmpty(assessment.score_comment) || openGlobalCommentInput)"
         x-on:click.outside="openGlobalCommentInput = assessmentCtrl.isEmpty(assessment.score_comment)">
        <textarea type="text" x-model="assessment.score_comment" rows="4" placeholder="{{__("Commentaire...")}}"></textarea>
    </div>
</div>
