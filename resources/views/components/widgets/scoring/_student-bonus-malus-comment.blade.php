<div>
    <span x-text="assessment.bonus_comment" 
          :title="assessment.bonus_comment"
          x-on:click="global_tooltip_popup.openTooltipPopup('', assessment.bonus_comment, $el)">
    </span>
</div>
