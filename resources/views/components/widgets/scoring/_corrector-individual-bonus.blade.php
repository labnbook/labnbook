<!-- TODO block edition if published-->
<template
    x-for="student in assessment.assessed_students.filter(s => !s.missing && ( s.bonus !== 0 || s.bonus_comment !== null))"
    :key="student.id_assessed_student">
    <tr class="bonus-malus individual-bonus-malus"
        :class="assessment.editable ? 'assessment-editable' : ''"
    >
        <td class="bonus-malus-title"
            x-data="{hover:false}"
            x-on:mouseenter="hover = true"
            x-on:mouseleave="hover = false"

        >
            <div>
                <!-- Don't escape name it has already be done -->
                <span x-text="__('Bonus / Malus pour \{\{- name\}\}', {name: student.name})"></span>
                <div class="delete-individual-bonus-malus"
                     x-show="assessment.editable"
                >
                    <i class="fa fa-trash"
                       x-show="hover"
                       x-on:click="student.bonus = 0; student.bonus_comment = null"
                    >
                    </i>
                </div>
            </div>
        </td>
        <td :colspan="viewConfig.columnsNumber - 2" class="bonus-malus-comment">
            @include('components.widgets.scoring._corrector-individual-bonus-malus-comment')
        </td>
        <td class="bonus-malus-score">
            @include('components.widgets.scoring._corrector-individual-bonus-malus-score')
        </td>
    </tr>
</template>
<template x-if="assessment.editable && assessment.assessed_students.length > 1 && assessment.assessed_students.filter(s => !s.missing && s.bonus === 0 && s.bonus_comment === null).length > 0">
    <!-- Line to add individual bonus -->
    <tr class="bonus-malus individual-bonus-malus-add"
        x-data="{'selected_student': 0}"
        :class="assessment.editable ? 'assessment-editable' : ''"
    >
        <td :colspan="viewConfig.columnsNumber">
            <div class="individual_bonus_wrapper">
                <div>
                    <span class="place_holder_text">
                    {{__("Ajouter un bonus malus individuel pour : ")}}
                    </span>
                    <select name="add-individual-bonus-to-student btn_pointer"
                            x-model="selected_student"
                            x-on:change="assessmentCtrl.addIndividualBonus(selected_student)">
                        <option value="0">--</option>
                        <template
                            x-for="student in assessment.assessed_students.filter(s => !s.missing && s.bonus ===0 && s.bonus_comment === null)"
                            :key="student.id_assessed_student">
                            <option :value="student.id_assessed_student"><span x-text="student.name"></span></option>
                        </template>
                    </select>
                </div>
            </div>
        </td>
    </tr>
</template>
