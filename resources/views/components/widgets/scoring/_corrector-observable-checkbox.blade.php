<input type="checkbox" 
       :id="'checkbox_' + descrob.id_descrob"
       x-model="descrob.selected" :disabled="!assessmentCtrl.isActive(criterion)"
       x-on:click.prevent="descrob.selected = await assessmentCtrl.selectOrToggleDescrob(criterion, descrob)">
<div class="hidden-checkbox-wrapper"
     x-on:click="descrob.selected = await assessmentCtrl.selectOrToggleDescrob(criterion, descrob)"><div class="hidden-checkbox"></div>
</div>
<label :for="'checkbox_' + descrob.id_descrob" 
       x-text="descrob.description" 
       :title="descrob.description"
       x-on:click.prevent="descrob.selected = await assessmentCtrl.selectOrToggleDescrob(criterion, descrob)">
</label>
