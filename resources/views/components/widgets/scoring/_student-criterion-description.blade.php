<div class="criterion-description-wrapper">
    <span x-text="criterion.description" :title="criterion.description"></span>
</div>
