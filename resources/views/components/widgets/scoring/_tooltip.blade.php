<?php
    /** @var {string} title **/
    /** @var {string} message **/
?>
<span class="fa fa-info-circle lba_icon_default"
      x-on:mouseover="global_tooltip_popup.openTooltipPopup('{{$title}}', '{{$message}}', $el)"
      x-on:click="global_tooltip_popup.openTooltipPopup('{{$title}}', '{{$message}}', $el)"
      >
</span>
