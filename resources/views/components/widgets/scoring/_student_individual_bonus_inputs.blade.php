<div class="individual_bonus_inputs_wrapper">
    <div>
        <textarea :name="'indivual-bonus-score_{{ $id }}"
               type="text"
               x-model="student.bonus_comment"
               x-show="selected_student == {{ $id }}"
               rows="2"
               placeholder="{{__('Commentaire...')}}"
               @if ($id == '0')
                   disabled
               @endif
        >
        </textarea>
    </div>
    <div>
        <input :name="'indivual-bonus-score_{{ $id }}'"
               type="numeric"
               x-model="student.bonus"
               x-show="selected_student == {{ $id }}"
               placeholder="{{__('bonus ou malus')}}"
               @if ($id == '0')
                   disabled
               @endif
        >
        </input>
    </div>
</div>
