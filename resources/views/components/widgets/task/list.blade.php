<?php
/** @var \App\Helper $helper */
/** @var \App\Task $task */
/** @var boolean $withChildren */

/***
 * Use variables from parent view
 * - task : Task
 * - reports_member : array of User
 */
?>
 
<div class="task-list">
    <template x-for="report in tasksNS.getReports()" :key="report.id_report">
        <div>
            <!-- USe sortable js to implement sortin fonctionality of tasks -->
            <?php
                $isPersonnalTasks = !$isReportTask  ? 'true' : 'false';
            ?>
            <div
                    :id="!{{$isPersonnalTasks}} || report.id_report === -1 ? 'sortable-tasks' : ''"
                 x-data="{isReadOnly: {{$isPersonnalTasks}} && report.id_report >0, isPersonnal: !report.id_report}"
                 x-show="tasksNS.isSelectedReport(report.id_report)"
                 >
                <template x-for="task in tasksNS.getReportTasks(report.id_report)"
                          :key="task.id_task+'_'+task.position+'_'+task.id_parent_task">
                    @include('components.widgets.task.task', ["withChildren" => true])
                </template>
            </div>
            <template x-if="report.id_report < 0 ">
                <div class="task-footer"
                     x-show="tasksNS.isSelectedReport(report.id_report)"
                     >
                    @include('components.widgets.task.add_task', ["isPersonnal" => true])
                </div>
            </template>
        </div>
    </template>
</div>
