<?php
/** @var \App\Helper $helper */
/** @var \App\Report|null $report */
/** @var \App\User[] $report_users */
/** @var string $title */
$id_report = $report ? $report->id_report : -1;
?>
@once
    {!! $helper->loadAsset("/libraries/sortable/Sortable.min.js") !!}
    {!! $helper->loadAsset("/css/widgets/task-utils.css"); !!}
    {!! $helper->loadAsset("/css/widgets/task.css"); !!}
    {!! $helper->loadAsset("/js/widgets/task/task-constant.js"); !!}
    {!! $helper->loadAsset("/js/widgets/task.js"); !!}
@endonce

<!-- Wait for i18next to init -->
<div x-data="{ tasksNS : window.tasksCtrl }"
             x-init='$nextTick(() => {tasksNS.init({{$id_report}}, @JSON($report_users), @JSON($reportNames))})'
    x-show="{{$xShow}}" style="height: 100%">
    <!-- task div -->
    <x-widget :id="$id" :title="$title" :x-show="$xShow" :header-class="'task-sub-header'">

        @if(!$isReportTask)
            <x-slot:title>
                {{__('Mes tâches personnelles')}}
            </x-slot:title>
        @else
            <x-slot:title>
                {{__('Liste des tâches')}} {{--:&nbsp;{{$reportTitle}}--}}
            </x-slot:title>
        @endif
        
        <x-slot:header-side-title>
            <div class="flex-list-input task-header-search">
                <input type="text"
                       class="task-input"
                       x-model="tasksNS.filter"
                       x-on:change="sendTaskFilterTrace(tasksNS.filter)"
                       placeholder="{{__('Rechercher')}}"
                >
                <span x-text="tasksNS.getNumberOfTasksCompleted() + '/' + tasksNS.getNumberOfTasks()"
                      title="{{__("Taches terminées / totales")}}">
                    </span>
            </div>
        </x-slot:header-side-title>

        <x-slot:header>
            <div class="task-header" x-show="tasksNS.initialized">
                @include('components.widgets.task.header_task', ['isReportTask' => $isReportTask])
            </div>
        </x-slot:header>

        <div id="task-content" class="widget drag_popup" x-data="{ tasksNS : window.tasksCtrl }"
             @tasksynchro.document="tasksNS.refresh()">
            <div class="task_content" x-show="tasksNS.initialized">
                @include('components.widgets.task.list', ["isReadOnly" => false, "withChildren" => true, 'isReportTask' => $isReportTask])
                @if ($isReportTask)
                    <div class="task-footer">
                        @include('components.widgets.task.add_task', ["isPersonnal" => false])
                    </div>
                @endif
            </div>
            <div x-show="!tasksNS.initialized">
                <i class="fa fa-pulse fa-spinner"></i>{{__("Chargement en cours")}}
            </div>
        </div>
    </x-widget>
</div>
