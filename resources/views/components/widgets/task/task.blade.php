<?php
/** @var \App\Helper $helper */
/** @var \App\Task $task */
/** @var boolean $withChildren */

/***
 * Use variables from parent view
 * - task : Task
 */
?>


<div   
        :data-taskid="task.id_task" x-bind:id="'task-' + task.id_task"
        x-show="tasksNS.isInFilter(task)"
        :class="{
         'task-completed' : task.isCompleted,
         'task-border task-container': task.id_parent_task == null
         }">
    <template x-if="tasksNS.initialized">
        <div class="lnb-hoverable"
             x-data='{ taskHasMouseOver : false }'
             :class="{'task-child' : task.id_parent_task != null}"
        >
            <div class="flex-list-input"
                 x-on:mouseover="taskHasMouseOver = true"
                 x-on:mouseover.outside="taskHasMouseOver = false">
                
                {{-- icône drag and drop--}}
                <div class="w-15px lnb-btn draggable" 
                     :class="{'task-sortable-handle' : task.id_parent_task == null,
                                    'subtask-sortable-handle': task.id_parent_task != null}">
                    <i x-show="taskHasMouseOver" class="fa-solid fa-arrows-up-down-left-right "></i>
                </div>
                {{-- Afficher / Masquer les sous tâches--}}
                <div x-show="task.id_parent_task == null && task.children.length">
                    <template x-if="task.id_parent_task == null">
                        <div class="w-15px lnb-btn" x-on:click="if (!isReadOnly) {tasksNS.toggleSubTask(task.id_task)}">
                            <i x-show="!task.hasSubtasksUnfolded" class="fas fa-chevron-right"
                               title="{{__("Voir les sous tâches associées")}}"></i>
                            <i x-show="task.hasSubtasksUnfolded" class="fas fa-chevron-down"
                               title="{{__("Masquer les sous tâches associées")}}"></i>
                        </div>
                    </template>
                </div>
                
                <!-- Task checkbox -->
                <div class="w-20px lnb-btn">
                    <div class="task-input-wrapper">
                        <input type="checkbox" x-model="task.isCompleted"
                               class="task_clickable"
                               title="{{__("Valider / annuler la réalisation de la tâche")}}"
                               x-on:click="if (!isReadOnly) {tasksNS.toggleIsCompleted(task.id_task);}"
                               :disabled="isReadOnly"
                        >
                    </div>
                </div>
                <!-- Task title -->
                <div class="w-100p"
                     :class="{ 'completed' : task.isCompleted }">
                    <div class="task-input-wrapper flex-justify-left"
                         title="{{__("Modifier le nom de la tâche")}}">
                        <span x-show="isReadOnly" x-text="task.title" :title="task.title"></span>
                        <div class="w-100p" x-show="!isReadOnly">
                            <x-textarea-auto-resize x-model="task.title" x-on-blur="tasksNS.updateTask(task)" x-on-focus="task.openInput='title'"></x-textarea-auto-resize>
                        </div>
                    </div>
                </div>
                <!-- Subtask count -->
                <template x-if="task.id_parent_task == null">
                    <div class="w-20px">
                        <template x-if="task.children && task.children.length">
                            <span
                                    x-text="tasksNS.getNumberOfSubtasksCompleted(task.id_task) + '/' + tasksNS.getNumberOfSubtasks(task.id_task)"></span>
                        </template>
                    </div>
                </template>
                <!-- Task estimation -->
                <div class="w-60px">
                    <div class="task-input-wrapper">
                        <div class="input-content task_clickable " x-show="task.openInput !== 'estimated_duration'"

                             x-on:click="if (!isReadOnly) {task.openInput = 'estimated_duration'; 
                             $nextTick(() => { $root.querySelector(`[x-ref='input_${task.id_task}']`).focus(); });}">
                            <span x-show="task.estimated_duration && task.estimated_duration > 0 ">
                                <span class="fs-small" x-text="task.estimated_duration + ' ' + __('min')" :title="task.estimated_duration">
                            </span>
                            </span>
                            <span x-show="taskHasMouseOver && (!task.estimated_duration || task.estimated_duration < 1 ) " x-transition>
                                <i class="fa-regular fa-hourglass lnb-btn" title="{{__("Ajouter une durée")}}"></i>
                            </span>
                        </div>
                        <div class="input-editor w-100p" x-show="task.openInput === 'estimated_duration'"
                             x-on:click.outside="tasksNS.updateTask(task)"
                             x-on:keyup.enter="tasksNS.updateTask(task);">
                            <input type="number" x-model="task.estimated_duration" class="task-input fs-small w-100p"
                                   min="0" :x-ref="'input_' + task.id_task" spellcheck="false" placeholder="{{__('min')}}">
                        </div>
                    </div>
                </div>
                <!-- Task due date -->
                <div class="w-100px">
                    <div class="task-input-wrapper flex-justify-center">
                        <div class="input-content task_clickable" x-show="task.openInput !== 'due_date'"
                             x-on:click="if (!isReadOnly) {task.openInput = 'due_date'; 
                         $nextTick(() => { $el.nextElementSibling.children[0].showPicker(); });}">
                             <span class="fs-small" x-show="task.due_date">
                                <span x-text="new Date(task.due_date).toLocaleDateString()" :title="task.due_date">
                                </span>
                             </span>
                            <span x-show="taskHasMouseOver && !task.due_date"
                                  x-transition>
                                <i class="fa-regular fa-calendar lnb-btn" title="{{__("Ajouter une échéance")}}"></i>
                            </span>
                        </div>
                        <div class="input-editor" x-show="task.openInput === 'due_date'"
                             x-on:click.outside="tasksNS.updateTask(task)"
                             x-on:keyup.enter="tasksNS.updateTask(task);">
                            <input type="date" x-model="task.due_date" x-value="task.due_date"
                                   class="fs-small task-input"
                                   :x-ref="'input_' + task.id_task" spellcheck="false"
                            ></input>
                        </div>
                    </div>
                </div>
                <!-- Task priority -->
                <div class="w-80px">
                    <div class="task-input-wrapper">
                        <div class="input-content task_clickable"
                             x-show="task.openInput !== 'priority'"
                             x-on:click="if (!isReadOnly) {task.openInput = 'priority'; $nextTick(() => { $root.querySelector(`[x-ref='input_${task.id_task}']`).focus(); });}"
                        >
                            <div x-show="task.priority" class="task-priority "
                                   :class="{
                                           'none': !task.priority,
                                           'low': task.priority === PRIORITIES.LOW,
                                           'medium': task.priority === PRIORITIES.MEDIUM,
                                           'high': task.priority === PRIORITIES.HIGH,
                                           }"
                                   :title="tasksCtrl.getPriorityTrans(task)"
                                 >
                                <i class="fas fa-exclamation-circle"></i>
                                <i class="fas fa-exclamation-circle" x-show="task.priority !== PRIORITIES.LOW"></i>
                                <i class="fas fa-exclamation-circle" x-show="task.priority === PRIORITIES.HIGH"></i>
                            </div>
                            <span x-show="taskHasMouseOver && !task.priority" x-transition>
                                <i class="fas fa-exclamation-circle lnb-btn" title="{{__("Ajouter une priorité")}}"></i>
                            </span>
                        </div>
                        <div class="input-editor w-100p" x-show="task.openInput == 'priority'"
                             x-on:click.outside="task.openInput=null"
                             x-on:change="tasksNS.updateTask(task)"
                        >
                            <select x-model="task.priority" :x-ref="'input_' + task.id_task"
                                    class="task-priority center">
                                <template x-for="priority in PRIORITIES_LIST">
                                    <option x-text="__(priority)" :value="priority"
                                            class="task-priority"></option>
                                </template>
                            </select>
                        </div>
                    </div>
                </div>
                <!-- Task assignees -->
                @if($isReportTask)
                    <template x-if="!isPersonnal">
                        <div class="w-100px">
                            <div class="task-input-wrapper task_clickable w-100p " :class="{'top':top}"
                                 x-data="{top:true}"
                                 x-on:click="if (!isReadOnly) {task.openInput = 'assignees';
                                 top = (($el.getBoundingClientRect().top)<(window.innerHeight-350) || ($el.offsetTop<350)); }">
                                <div class="input-content" x-show="task.openInput !== 'assignees'">
                                    <span x-show="task.assignees && task.assignees.length > 0" >
                                        <template x-for="assignee in task.assignees">
                                            <!-- <img x-bind:src="assignee.avatar" :title="assignee.fullname" /> -->
                                            <span x-html="assignee.displayValue"></span>
                                        </template>
                                    </span>
                                    <span class="lnb-btn" 
                                          x-show="taskHasMouseOver && (!task.assignees || task.assignees.length == 0)"
                                          x-transition>
                                        <i class="fa-solid fa-user-plus lnb-btn" title="{{__("Ajouter des responsables")}}"></i>
                                    </span>
                                </div>
                                <div class="input-editor" x-show="task.openInput === 'assignees'"
                                    x-on:click.outside="task.openInput=null">
                                    <div
                                        x-data="multiSelectComponent( {
                                        options : tasksNS.report_members,
                                        selected : task.assignees,
                                        onChange: (data) => {task.assignees = data.selected; tasksNS.updateAssignees(data)},
                                        changeData: task.id_task
                                        })"
                                        class="relative">
                                        @include('_multiSelect')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </template>
                @endif
                <!-- Task actions -->
                <div class="w-20px right" x-show="!isReadOnly">
                    <div class="task-input-wrapper">
                        <span x-show="taskHasMouseOver" class="task-action task_clickable" 
                              x-transition
                              x-on:click="tasksNS.deleteTask(task.id_task)">
                            <i class="fas fa-trash-alt lnb-btn" title="{{__("Supprimer la tâche")}}"></i>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </template>

    <!-- Subtasks and dropzone -->
    @if ($withChildren)
        <template x-if="task.id_parent_task == null">
            <div x-show="task.hasSubtasksUnfolded || tasksNS.inDragg " class="nested"
                 :class="{ 'task-drop-zone' : tasksNS.inDragg} " :data-taskid="task.id_task">
                <template x-for="task in task.children"
                          :key="task.id_task+'_'+task.position+'_'+task.id_parent_task">
                    @include('components.widgets.task.task', ['withChildren' => false])
                </template>
            </div>
        </template>
    @endif
</div>
