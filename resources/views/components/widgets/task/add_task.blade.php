<?php
/** @var \App\Helper $helper */
$placeholder = $isPersonnal ? __("Ajouter une tâche personnelle" ) : __("Ajouter une tâche ou un rôle");

?>

<div class="flex-list-input" x-data="{ newTask : tasksNS.newTask } "
    x-on:keyup.enter="tasksNS.createTask();">
    <!-- Task checkbox -->
    <!-- Task title -->
    <div class="w-100p" id="create-task-input">
        <div class="task-input-wrapper">
            <div class="input-editor w-100p" >
                <input type="text" x-model.lazy="newTask.title" class="task-input"
                       placeholder="{{$placeholder}}" title="{{__("Titre de la tâche")}}"
                ></input>
            </div>
        </div>
    </div>
    <div class="w-40px" x-on:click="tasksNS.createTask();">
        <div class="task-input-wrapper task-add-button lnb-btn" title="{{__("Créer la tâche")}}">
                <i class="fa fa-check"></i>
        </div>
    </div>
</div>
