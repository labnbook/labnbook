<?php
/** @var \App\Helper $helper */

?>



<div class="flex-list-input">

    <!-- Task meta --> {{-- Même pas sûr que ce soit nécessaire --}}
    <!-- Task title -->
    <div class="w-100p">
        {{__("Nom")}}
    </div>
    <!-- Task estimation -->
    <button class="w-60px task-order" 
         x-on:click="tasksNS.sortByCriteria !== SORT_BY.ESTIMATED_TIME_DESC ? tasksNS.sortBy(SORT_BY.ESTIMATED_TIME_DESC) : tasksNS.sortBy(SORT_BY.ESTIMATED_TIME_ASC)"
         :title="tasksNS.sortByCriteria === SORT_BY.ESTIMATED_TIME_DESC ? __('Durée descendante') : __('Durée ascendante')">
            {{__("Durée")}}&nbsp;
            <i class="fa"
               :class="{
                       'fa-caret-up': tasksNS.sortByCriteria !== SORT_BY.ESTIMATED_TIME_DESC,
                       'fa-caret-down': tasksNS.sortByCriteria == SORT_BY.ESTIMATED_TIME_DESC
                       }"
               ></i>
    </button>
    <!-- Task due date -->
    <button class="w-100px task-order"
         x-on:click="tasksNS.sortByCriteria !== SORT_BY.DUE_DATE_DESC ? tasksNS.sortBy(SORT_BY.DUE_DATE_DESC) : tasksNS.sortBy(SORT_BY.DUE_DATE_ASC)"
         :title="tasksNS.sortByCriteria === SORT_BY.ESTIMATED_TIME_DESC ? __('Echéance descendante') : __('Echéance ascendante')">
        {{__("Échéance")}}
            <i class="fa"
               :class="{
                       'fa-caret-up': tasksNS.sortByCriteria !== SORT_BY.DUE_DATE_DESC,
                       'fa-caret-down': tasksNS.sortByCriteria == SORT_BY.DUE_DATE_DESC
                       }">
            </i>
        
    </button>
    <!-- Task priority -->
    <button class="w-80px task-order"
         x-on:click="tasksNS.sortByCriteria !== SORT_BY.PRIORITY_DESC ? tasksNS.sortBy(SORT_BY.PRIORITY_DESC) : tasksNS.sortBy(SORT_BY.PRIORITY_ASC)"
         :title="tasksNS.sortByCriteria === SORT_BY.ESTIMATED_TIME_DESC ? __('Echéance descendante') : __('Echéance ascendante')">
        {{__("Priorité")}}
            <i class="fa"
               :class="{
                       'fa-caret-up': tasksNS.sortByCriteria !== SORT_BY.PRIORITY_DESC,
                       'fa-caret-down': tasksNS.sortByCriteria == SORT_BY.PRIORITY_DESC
                       }">
            </i>
        
    </button>
    @if($isReportTask)
        <!-- Task assignees no sort-->
        <div class="w-100px">
            {{__("Responsable")}}
        </div>
    @endif
    <!-- Task delete no sort-->
    <div class="w-20px right"></div>
</div>
