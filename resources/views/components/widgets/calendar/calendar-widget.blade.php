{!! $helper->loadAsset("/js/widgets/calendar.js"); !!}
{!! $helper->loadAsset("/libraries/fullcalendar/index.min.js"); !!}
{!! $helper->loadAsset("/libraries/fullcalendar/locales.min.js"); !!}
{!! $helper->loadAsset("/css/widgets/calendar.css"); !!}
<x-widget :id="$id" :x-show="$xShow">
    <x-slot:title>
        {{__('Calendrier')}}
    </x-slot:title>
    <div id="calendar-content"
         x-data="{calendarCtrl: window.calendarCtrl}"
         @tasks_ready.window="calendarCtrl.init($event, $el, $el.parentElement.parentElement, '{{date_default_timezone_get()}}')";
         @calendar_toggle.window="await $nextTick(); calendarCtrl.render()";
         @widget_resized.window="await $nextTick(); calendarCtrl.render()";>
         <div x-show="!calendarCtrl || !calendarCtrl.intialized">
            <i class="fa fa-spinner fa-pulse"></i>&nbsp;{{__("Chargement en cours")}}
         </div>
    </div>
</x-widget>
