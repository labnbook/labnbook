<?php
    /** @var \App\Helper $helper */
    /** @var \Illuminate\Database\Eloquent\Collection|\App\Resource[] $resources */
    /** @var  $displayNewDoc */
?>
@foreach ($resources as $doc)
    @php
        $path = '';
        if ($doc->res_type === "url") {
            if (preg_match('#^(https?:|)//#', $doc->res_path)) {
                $path = "";
            } else if ($doc->res_path[0] !== '/') {
                $path = "//";
            } else {
                $path = '/';
            }
        } else {
            $path = "/storage/reports/$doc->id_report/";
        }
        $encodedName = htmlspecialchars($doc->name);
    @endphp
    <div x-data="learnerDoc('{{$encodedName}}')">
        <template x-if="!editDoc">
            <div id="learner_doc_{{$doc->id_ressource}}" class="widg-res-learner-doc lnb-hoverable">
                <a class="resource-link widg-res-link" href="{{$path}}{{$doc->res_path}}" onclick="updateOrInsert({{$doc->id_ressource}})" target="_blank" title="{{__('Consulter le document (nouvel onglet)')}}">
                    <i @class(['widg-res-icon fa fa-circle', 'new-resource' => in_array($doc->id_ressource, $displayNewDoc)])></i>
                    <span id="res_doc_name_{{$doc->id_ressource}}" x-text="docName"></span>
                </a>
                <div class="widg-btn-box">
                    <i class="fas fa-pencil-alt lnb-btn" x-on:click="editDoc=true" title="{{__('Modifier le nom du document')}}"></i>
                    <i class="far fa-trash-alt lnb-btn" x-on:click="delDoc({{$doc->id_ressource}})" title="{{__('Supprimer le document')}}"></i>
                </div>
            </div>
        </template>
        <template x-if="editDoc">
            <div id="learner_doc_edit_{{$doc->id_ressource}}" class="widg-res-learner-doc widg-res-edit-doc">
                <i @class(['widg-res-icon fa fa-circle', 'new-resource' => in_array($doc->id_ressource, $displayNewDoc)])></i>
                <input type="text" class="widg-res-input-name" name="edit_document_name" id="res_doc_name_edit_{{$doc->id_ressource}}" x-model="docName">
                <div class="widg-btn-box">
                    <i class="fa fa-check lnb-btn" x-on:click="renameDoc({{$doc->id_ressource}})" title="{{__('Valider')}}"></i>
                    <i class="fa fa-times lnb-btn" x-on:click="editDoc=false"  title="{{__('Annuler')}}"></i>
                </div>
            </div>
        </template>
    </div>
@endforeach
