<?php
    /** @var \App\Helper $helper */
    /** @var  $mission */
    /** @var  $id_report */
    /** @var  $docs */
    /** @var  $detailedAssignment */
    /** @var  $displayNewDoc */
    /** @var  $addResource */
 ?>
@once
    {!! $helper->loadAsset("/css/widgets/resource.css"); !!}
    {!! $helper->loadAsset("/js/widgets/resource.js"); !!}
    {!! $helper->configureAjax()  !!}
@endonce
<x-widget :id="$id" :x-show="$xShow">
    <x-slot:title>
        {{__('Ressources')}}
    </x-slot:title>
    <div id="widg-res-content" class="widg-res-content">
        <!--  Assignments links -->
        <div id="widg_res_assignment" class="widg-res-section" x-data="{showAssignment: false}"
             x-init="$watch('showAssignment',(showAssignmentValue)=>{if(showAssignmentValue){traceOpenAssignment();}})">
            <!-- short assignment -->
            @if ($mission->assignment || $mission->description)
                <p class="widg-res-link widg-res-toggle-section"
                   x-on:click="showAssignment=!showAssignment">
                    <i class="fas" :class="{'fa-chevron-right': !showAssignment, 'fa-chevron-down':showAssignment}" style="padding-top: 4px"></i>
                    {{__('Consigne générale')}}
                </p>
                <div id="widg-res-short-assignment" class="tinyMCE-text widg-res-short-assignment" x-show="showAssignment">
                    @if($mission->assignment)
                        {!! $mission->assignment !!}
                    @else
                        {!! $mission->description !!}
                    @endif
                </div>
            @endif
            @if($detailedAssignment)
                <a href="/storage/missions/{{$mission->id_mission}}/resources/{{$detailedAssignment->res_path}}" target="_blank" class="resource-link widg-res-link"
                   title="{{__('Consulter la consigne détaillée (nouvel onglet)')}}">
                    <i class="widg-res-icon fa fa-circle"></i>
                    <span>{{__('Consigne détaillée')}}</span>
                </a>
            @endif
        </div>

        <!--  Given Docs -->
        @if(count($docs)>0)
            <div id="widg-res-given-docs" class="widg-res-section">
                <p class="widg-res-title">{{__('Documents fournis')}}</p>
                @foreach ($docs as $doc)
                    @php
                        $path = '';
                        if ($doc->res_type === "url") {
                            if (!preg_match('/http[s]?:\/\/.*/', $doc->res_path)) {
                                $path = "http://";
                            }else{
                                $path = "";
                            }
                        } else {
                            $path = sprintf("/storage/missions/%d/resources/", $mission->id_mission);
                        }
                    @endphp
                    <a href="{{ $path }}{{ $doc->res_path }}" class="resource-link widg-res-link" id="learner_doc_{{$doc->id_ressource}}" 
                       onclick="updateOrInsert({{$doc->id_ressource}})" target="_blank" title="{{__('Consulter le document (nouvel onglet)')}}">
                        <i @class(['widg-res-icon fa fa-circle', 'new-resource' => in_array($doc->id_ressource, $displayNewDoc)])></i>
                        <span>{{ $doc->name }}</span>
                    </a>
                @endforeach
            </div>
        @endif

        <!-- Students Docs -->
        <div id="widg-res-learner-docs" class="widg-res-section">
            <p class="widg-res-title">{{__('Documents partagés')}}</p>
            <div id="widg-res-docs-list">
                <!-- ici se placent les docs définis par les users : appel ajax -->
            </div>
        </div>
            @if ($addResource !== "invisible")
                <!-- Add resources link & box -->
                <div id="add_doc_link" class="widg-res-bottom-section widg-res-add-button" title="{{__('Ajouter un document (visible par les enseignants)')}}" onclick="toggleAddDoc({!! $addResource !== "active" !!})">
                    <i class="fa fa-plus"></i>
                    <span>{{__('Ajouter un document (lien, pdf ou image)')}}</span>
                </div>
                @if ($addResource === "active")
                    <form id="add_document" class="widg-res-bottom-section widg-res-add-form" action="/storage/upload" method="post" enctype="multipart/form-data" x-show="false">
                        <div class="widg-res-add-form-top">
                            <div class="widget-form-title">{{__('Ajouter un document partagé')}}</div>
                            <div class="widg-res-add-form-item">
                                <input type="radio" value="link" name="type_document" onclick="urlOrFile()" checked="checked" id="widg_res_add_link" autocomplete="off">
                                <label for ="widg_res_add_link"> {{__('lien internet')}}</label>
                            </div>
                            <div class="widg-res-add-form-item">
                                <input type="radio" value="file" name="type_document" onclick="urlOrFile()" id="widg_res_add_file" autocomplete="off">
                                <label for="widg_res_add_file"> {{__('fichier (pdf, csv, texte, image - 2 Mo max.)')}}</label>
                            </div>
                            <input type=hidden name='id_context' value={{ $id_report}} />
                            <input type=hidden name='context' value='report' />
                            <input type=hidden name='type' value='res_doc' />
                            <div class="widg-res-add-form-item">
                                <label class="widg-res-label" for="name">{{__('Nom')}}</label>
                                <input type="text" name="name" class="widg-res-input-text" id="name_resource"/>
                            </div>
                            <div class="widg-res-add-form-item" id="url_document">
                                <label class="widg-res-label" for="url">{{__('Adresse')}}</label>
                                <input type="text" name="url" class="widg-res-input-text" id="url_document_address">
                            </div>
                            <div class="widg-res-add-form-item" id="res-file" x-show="false">
                                <input name="file_document" class="widg-res-input-text" type="file" id="res_file_document"/>
                            </div>
                        </div>
                        <div>
                            <div id="add_document_btn" class="widg-res-add-form-btn widget-form-buttons">
                                <button type="button" class="widget-form-button widget-submit" onclick="addDoc()" title="{{__('Ajouter la ressource')}}">
                                    <i class="fa fa-check"></i>
                                </button>
                                <button type="button" class="widget-form-button widget-cancel" onclick="toggleAddDoc()" title="{{__('Annuler')}}">
                                    <i class="fa fa-times"></i>
                                </button>
                            </div>
                            <span id="add_document_spinner" class="widg-res-add-form-btn" x-show="false"><i class="fa fa-spinner fa-pulse"></i></span>
                        </div>
                    </form>
                @endif
            @endif
    </div>
    <div id="response_ajax_file"></div>
</x-widget>
