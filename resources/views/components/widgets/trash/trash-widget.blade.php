@once
    {!! $helper->loadAsset("/css/widgets/trash.css"); !!}
    {!! $helper->loadAsset("/js/widgets/trash.js"); !!}
    {!! $helper->configureAjax()  !!}
@endonce
<x-widget :id="$id" :x-show="$xShow">
    <x-slot:title>
        {!! __('Corbeille à labdocs')!!}
    </x-slot:title>
    <div id="trash_content">
        @include('components.widgets.trash._deleted', ['id_report' => 0, 'deleted' => []])
    </div>
</x-widget>
