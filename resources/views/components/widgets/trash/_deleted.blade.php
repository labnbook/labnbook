<?php
    /** @var \App\Helper $helper */
    /** @var int $id_report */
    /** @var \App\Labdoc[] $deleted */
?>
@if(count($deleted)===0)
    <span>{{__("Aucune suppression récente.")}}</span>
@else
    @php $rp=0 @endphp
    @foreach($deleted as $ld) 
        <div>
            @if($rp !== $ld->id_report_part)
                <div class="trash_rp_title">{{$ld->position." - ".$ld->title}}</div>
                @php $rp = $ld->id_report_part @endphp
            @endif
            <div id="trash_ld_{{$ld->id_labdoc}}" class="lnb-hoverable trash_deleted_ld">
                <img src="/images/report/ld_type_{{$ld->type_labdoc}}.svg" width="16">
                <div class="trash_name">
                    <div class="widg-btn-box">
                        <i class="fa fa-undo lnb-btn" title="{{__('Récupérer le labdoc')}}" onclick="restoreLD({{$id_report}},{{$ld->id_report_part}},{{$ld->id_labdoc}})"></i>
                        <i class="far fa-trash-alt lnb-btn" title="{{__('Supprimer définitivement')}}" onclick="realDeleteLD({{$id_report}},{{$ld->id_labdoc}})"></i>
                    </div>
                    {{$ld->name}} <span class="trash_deleted_ts">({{__('supprimé le')}} {!! date('d/m H:i', $ld->deleted) !!})</span>
                </div>
            </div>

        </div>
    @endforeach
@endif
