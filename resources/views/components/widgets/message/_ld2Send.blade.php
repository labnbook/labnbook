<?php
    /** @var \App\Helper $helper */
    /** @var \Illuminate\Database\Eloquent\Collection|\App\Labdoc[] $lds */
?>
<?php if (!$lds): ?>
    {{__('Vous ne pouvez pas attacher de labdoc dans le cadre de la mission actuelle.')}}
<?php else :?>
{{__('Envoyer un labdoc')}}{!!__("&nbsp;:") !!}
<br/>
<select id="ld_to_attach">
    <?php foreach ($lds as $ld): ?>
   <option id="labdoc{{$ld->id_labdoc}}" value="{{$ld->id_labdoc}}">{{$ld->name}}</option>
   <?php endforeach ?>
</select>
<?php endif ?>
