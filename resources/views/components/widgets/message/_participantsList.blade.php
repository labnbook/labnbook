<?php
    /** @var \App\Helper $helper */
    /** @var  \Illuminate\Database\Eloquent\Collection|\App\User[] $users */
    /** @var boolean $initials */
    /** @var boolean $is_report_context */
?>
@foreach ($users as $user)
    <span @class(["msg_participant", 'connected_user' => $user->isConnected()]) 
        data-id-user="{{$user->id_user}}"
          title="{{$user->isConnected()?__('Actuellement connecté'):''}}">
        @if($is_report_context)
            {!! htmlspecialchars($initials?mb_substr($user->first_name, 0, 1):$user->first_name)." ".htmlspecialchars($user->user_name) !!}
        @else
            {!! htmlspecialchars($initials?mb_substr($user->user_name, 0, 1):$user->user_name)." ".htmlspecialchars($user->first_name) !!}
        @endif
    </span>
    @if(!$loop->last), @endif
@endforeach
