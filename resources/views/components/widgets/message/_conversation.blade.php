<?php
    /** @var \App\Helper $helper */
    /** @var \App\Conversation $conv */
    /** @var int $id_report_context */
    /** @var int $id_report_conv */
    /** @var int $allow_save_ld */
    /** @var string $mission_tag */
    /** @var string|NULL $mission_code */
    /** @var string $scope */
    /** @var \App\User $user */
?>
<!-- DISPLAYS -->
    <!-- send to JS the id of the conversation : important if it's a newly created conversation -->
    <input type='hidden' id='msg_new_conv_id' value='{{$conv->id_conversation}}'>

    <!-- menu div -->
<?php
    $participants_spans = view('components.widgets.message._participantsList', ['users' => $conv->getParticipants($user->id_user), 'initials' => true, 'is_report_context' => $scope!==null]) ;
    if ($participants_spans == "") {
        $participants_spans = "<i>".__('aucun participant')."</i>";
    }
    $participants = $conv->displayParticipants();
?>
    <div id="msg_menu_messages" class="msg_menu">
        <i id="widget-message-back-btn" class="widget-message-btn fa fa-arrow-left" onclick="displayConversations()"  title="{{__('Retourner à la liste des conversations')}}"></i>
        <div id="msg_menu_msg_participants">
            <span id="msg_menu_msg_right_icons">
                <i class="fas fa-gear edit_conv_p widget-message-btn" onclick="editConvParticipants({{$conv->id_conversation}}, null, '', null)" title="{{__('Paramètres de la conversation')}}"></i>
            </span>
            <div id="msg_menu_msg_participants_text" title="{!!$participants!!}">
                <span id='msg_msg_code'>{{$mission_tag}}</span>
                {!! $conv->title ? $conv->title . " - " : "" !!}
                {!!$participants_spans!!}
            </div>
        </div>
    </div>

    <!-- msg div -->
    <div id="msg_msg_messages">
        @include('components.widgets.message._messages', [
            'conv' => $conv,
            'id_report_context' => $id_report_context,
            'mission_tag' => $mission_tag,
            'mission_code' => $mission_code,
            'id_report_conv' => $id_report_conv,
            'user' => $user,
            'scope' => $scope,
            'allow_save_ld' => $allow_save_ld,
        ])
    </div>

    <!-- send msg div -->
    <div id="msg_send" class="widget-message-send-wrapper"> <!-- l'envoi des messages -->

        <div id="msg_send_wrapper_left" class="msg_send_wrapper_left">
            <textarea id="text_msg_send" class="widget-message-send-textarea" name="message" ></textarea>
        </div>
        <div id="msg_send_wrapper_right" class="msg_send_wrapper_right">
        @php
            $allow_attach_ld = 0;
            if ($id_report_context && $user->getRole() != 'learner') {
                $allow_attach_ld = 1;
            } elseif ($id_report_context) {
                // check if the user can send LD in the context of his report
                $allow_attach_ld = \App\Report::whereKey($id_report_context)->pluck('allow_attach_ld')->first();
            }
        @endphp
            <button class="widget-message-btn" title="{{__('Envoyer le message')}}" onclick="sendMsg()">
                <i class="fa-regular fa-paper-plane"></i>
            </button>
            @if ($allow_attach_ld)
                <button class="widget-message-btn" title="{{__(" Envoyer un labdoc")}}" onclick="displayLD2Send()">
                    <i class="fa fa-paperclip"></i>
                </button>
            @endif
        </div>
        @if ($allow_attach_ld)
            <div id="ld_send_wrapper_left" class="msg_send_wrapper_left select-ld" style="display:none"></div>
            <div id="ld_send_wrapper_right" class="msg_send_wrapper_right" style="display:none">
                <button class="widget-message-btn" onclick="hideLD2Send()" title="{{__("Annuler l'envoi du labdoc")}}">
                    <i class="fa fa-times"></i>
                </button>
                <button class="widget-message-btn" onclick="sendLD()" title="{{__('Envoyer le labdoc sélectionné')}}">
                    <i class="fa-regular fa-paper-plane"></i>
                </button>
            </div>
        @endif
    </div> 
