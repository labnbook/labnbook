<?php
    /** @var \App\Helper $helper */
    /** @var \Illuminate\Database\Eloquent\Collection|\App\Conversation[] $conversations */
    /** @var \App\User $user */
    /** @var int $id_report_context */
        if ($conversations->isEmpty()) {
            echo '&nbsp;&nbsp;&nbsp;'.__('Aucune conversation à afficher');
            return;
        }
        // affichage des différentes conversations
        foreach($conversations as $conv) {
            $id_conv = $conv->id_conversation ;
            $convTitle = $conv->title;

            // code of the mission
            // participants a la conversations
            $participants = view('components.widgets.message._participantsList', ['users' => $conv->getParticipants($user->id_user), 'initials' => true, 'is_report_context' => $id_report_context!==0]) ;
            if ($participants == "") {
                $participants = "<i>".__('aucun participant')."</i>";
            }

            // y a-t-il un LD attaché ?
            $attached_ld = $conv->messages()->whereNotNull('id_labdoc')->exists();

            // contenu et date du dernier message
            $last_msg = $conv->messages()
                                     ->orderBy('msg_time', 'DESC')
                                     ->first();

            if ($last_msg) {
                // la conversation contient-elle de nouveaux messages ?
                $new_msg = (!$conv->last_seen || ($conv->last_seen < $last_msg->msg_time && $last_msg->id_user_sender !== 1));

                // calcul de la date relative
                $time = $helper->relativeDate($last_msg->msg_time) ;

                // affichage de la première ligne du message uniquement
                $msg_content = \App\Message::getSnapshotFromMsg($last_msg->msg_content);
            } else {
                $new_msg = false;
                $time="";
                $msg_content = "";
            }

            // affichages
        ?>
        <div class="msg_conversation <?php if($new_msg) { echo "has-new-msg" ; }?>" onclick='displayMessages({{$id_conv}}, null, 0, [])' id="conv_{{$id_conv}}" data-id_conv="{{$id_conv}}">
            <div class='conv_main lnb-hoverable' title='{{__('Ouvrir la conversation')}}'>
                <div class="conv_icons">
                    <div class="conv_state_icon">
                        <i title="{{__('Nouveau message dans la conversation')}}" class="fa fa-circle new-msg" style="display:
                        <?php echo $new_msg ? "inline" : "none" ; ?>
                        "></i>
                        <i class="fa fa-circle no-new-msg" style="display:
                        <?php echo $new_msg ? "none" : "inline" ; ?>
                        "></i>
                    </div>
                    @if($attached_ld)
                        <i class="fa fa-paperclip" title="{{__('Labdoc(s) attaché(s)')}}"></i>
                    @endif
                </div>
                <div class="conv-content">
                    <div class="widg-btn-box">
                        <i class="far fa-trash-alt lnb-btn" title="{{__('Quitter la conversation')}}" onclick="deleteConversation({{$id_conv}})"></i>
                    </div>
                    <div class='conv_title'>
                        <span class='conv_title_mcode'>{{$conv->missionTag()}}</span>
                        <span class='conv_title_title'>{{$convTitle}}</span>
                        @if(!$convTitle)
                            <span class='conv_title_participants'>{!! $participants !!}</span>
                        @endif
                    </div>
                    <div class='conv_timesnap'>
                        <span class='conv_time'>{{$time}}</span>
                        <span  class='conv_snapshot'>{{$msg_content}}</span>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }

