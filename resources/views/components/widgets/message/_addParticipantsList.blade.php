<?php
    /** @var \App\Helper $helper */
    /** @var int $id_conv */
    /** @var string $title */
    /** @var int $id_report_conv */
    /** @var string $scope */
    /** @var \App\User $user */
    /** @var Illuminate\Database\Eloquent\Collection|\App\User[] $recipients_list */
    /** @var Illuminate\Database\Eloquent\Collection|\App\User[] $teachers */
    /** @var Illuminate\Database\Eloquent\Collection|\App\User[] $team */
    /** @var int $allow_msg_id_class */
?>

@if ($id_conv)
    <div class="widget-form-title">{{__("Paramètres de la conversation")}}</div>
@else
    <div class="widget-form-title">{{__("Ajouter une conversation")}}</div>
@endif
@if ($id_conv && $conv->title)
   <!-- The title of the conversation -->
    <div id='msg_div_title'>
        <span class = 'msg_participants_list_title'>{{__('Titre')}}{!!__("&nbsp;:")!!} </span>{{$conv->title}}
    </div>
@else
   <div id='msg_div_title'>
       <span class = 'msg_participants_list_title'>{{__('Titre')}}{!!__("&nbsp;:")!!} </span>
       <input id='msg_conv_title' type='text' value='{{$title}}'>
   </div>
@endif

<!-- the participants to the conversation -->
<div id="msg_participants_list"><span class="msg_participants_list_title">{{__('Participants')}}{!!__("&nbsp;:")!!} </span>
    <?php if ($id_conv): ?>
        @include('components.widgets.message._participantsList', [
            'users' => $conv->getParticipants($user->id_user),
            'initials' => false,
            'is_report_context' => $scope!==null
            ])
    <?php endif ?>
</div>
    <?php if($can_add): ?>
        @include('components.widgets.message._participantsListLong', [
            'teachers' => $teachers,
            'team' => $team,
            'allow_msg_id_class' => $allow_msg_id_class,
            'id_conversation' => $id_conv
        ])
    <?php else :?>
        <br/>{{__("Vos droits ne vous permettent pas d'ajouter de participant")}}
    <?php endif ?>

        <!-- the buttons -->
<div id='msg_participants_btn' class="widget-form-buttons">
    <button type="button" class="widget-form-button widget-submit" title='{{__('Valider')}}' onclick='addParticipants({{$id_conv}}, {{$id_report_conv}})'>
        <i class='fa fa-check' ></i>
    </button>
    <button type="button" class="widget-form-button widget-cancel" title='{{__('Annuler')}}' onclick='
        <?php
            // définition du bouton retour
       if ($id_conv) {
       // conversation existante
       echo "displayMessages($id_conv, null, $id_report_conv, [])" ;
       } else {
       // nouvelle conversation
       echo "displayConversations()" ;
       }
   ?>
       '>
        <i class='fa fa-times'></i>
    </button>
</div>
