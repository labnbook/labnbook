<?php
    /** @var \App\Helper $helper */
    /** @var Illuminate\Database\Eloquent\Collection|\App\User[] $teachers */
    /** @var Illuminate\Database\Eloquent\Collection|\App\User[] $team */
    /** @var int $allow_msg_id_class */
    /** @var int $id_conversation */
?>
<div class="msg_add_participants">
    <div class="msg_participants_list_action" >{{__('Ajouter un participant')}}
        <x-select2 id="msg_add_participants_select" on-select-function="stageParticipant(selectedElement)"></x-select2>
    </div>
@if($teachers)
    @include('components.widgets.message._addParticipantsListDom', ['role' => 'teacher',
            'id_conversation' => $id_conversation, 'name' => __("Ajouter les enseignants du rapport")])
@endif
@if($team)
    @include('components.widgets.message._addParticipantsListDom', ['role' => 'team',
            'id_conversation' => $id_conversation, 'name' => __("Ajouter tous les étudiants de l'équipe")])
@endif
@if($allow_msg_id_class!==0)
    <div class="msg_participants_list_action" >
        {{__("Ajouter tous les étudiants d'une classe")}}
        @if (is_null($allow_msg_id_class))
            <x-select2 id="msg_add_participants_class" on-select-function="addParticipantsByRole('class', {{$id_conversation}}, selectedElement)"></x-select2>
        @elseif($allow_msg_id_class!==0)
            <button class="widget-message-btn" type="button"
                    onclick="addParticipantsByRole('class', {{$id_conversation}}, {id_class:{{$allow_msg_id_class}}})" title="{{__('Ajouter tous les étudiants de cette classe')}}">
                <i class="fa fa-check"></i>
            </button>
        @endif
    </div>
@endif
</div>
