<?php
/** @var int $id_report */
/** @var int $isStudent */
/** @var \App\Helper $helper */
?>
@once
    {!! $helper->loadAsset("/css/widgets/msg.css"); !!}
    {!! $helper->loadAsset("/js/widgets/msg.js"); !!}
    {!! $helper->configureAjax()  !!}
@endonce
<x-widget :id="$id" :x-show="$xShow" :header-class="'widget-message-header'">
    <x-slot:title>
        {{__("Messages")}}
    </x-slot:title>
    <x-slot:header>
        <!-- 2 divs containing the list of conversations -->
        <!-- menu -->
        <div id="msg_menu_conversations" class="widget-message-header-menu"><!-- menu de la liste des conversations -->
            <div id="search_conversation">
                <input class="widget-message-search-input" type="text" placeholder="{{__('Rechercher')}}" onkeyup="displayConversationList()" />
            </div>
            <div id="msg_menu_conversations_add">
                <button class="widget-message-btn" type="button" onclick="editConvParticipants(0, 0, '', null)" title="{{__('Ajouter une conversation')}}">
                    <i class="fa fa-plus"></i>
                </button>
            </div>
        </div>
    </x-slot:header>
    <div id="messagerie" class="student">
        <!-- main div -->
        <div id="msg_conversations" class="msg_scroll">
            @include('components.widgets.message._conversations', [
                    'conversations' => \App\Conversation::getConversationList('', $id_report, Auth::user()),
                    'id_report_context' => $id_report,
                    'user' => Auth::user(),
                ])
        </div> <!-- la liste des conversation -->

        <!-- 1 div containing the participants to a conversation -->
        <div id="msg_participants" class="msg_scroll"></div>

        <!-- 1 div containing the messages of a conversation -->
        <div id="msg_messages"></div>

        <!-- special parameters stored for the widget message -->
        <input type="hidden" id="msg_id_conv" value="conv">
        <!-- 0 if the widget is not opened or if the participants div is opened / 'conv' if the conversation list is opened / id_conv if a conv is opened -->
    </div>

</x-widget>
