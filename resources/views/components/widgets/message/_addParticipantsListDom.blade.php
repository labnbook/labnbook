<?php
    /** @var \App\Helper $helper */
    /** @var string $role */
    /** @var string $name */
    /** @var int $id_conversation */
?>
<div class="msg_participants_list_action">{{$name}}
    <button class="widget-message-btn" type="button"
            onclick="addParticipantsByRole('{{$role}}', {{$id_conversation}})" title="{{$name}}">
        <i class="fa fa-check"></i>
    </button>
</div>
