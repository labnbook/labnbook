<?php
/** @var \App\Views\Widgets\Message $mv */
/** @var \App\Helper $helper */
?>
{!! $helper->loadAsset("/css/widgets/msg.css"); !!}
{!! $helper->loadAsset("/css/widgets/widget.css"); !!}
{!! $helper->loadAsset("/js/widgets/msg.js"); !!}
{!! $helper->configureAjax()  !!}
<div id="messagerie" class="widget drag_popup teacher" style='z-index:200; 
         left:{{$mv->div_msg->pos_x}}px; top:{{$mv->div_msg->pos_y}}px; height:{{$mv->div_msg->height}}px; width:{{$mv->div_msg->width}}px;
		{{(!$mv->vis ? 'display:none;' : 'display:block')}}
	'>

    <!-- titre de la messagerie -->
    <div id="drag_msg" class="drag_popup_header">
        <span id="widg_msg_title">{{__('Messages')}}</span>
        <i id="close_widgmsg" class="fa fa-times drag_popup_close" title="{{__('Fermer la messagerie')}}"
           onclick="toggleWidget('messagerie')"></i>
    </div>
    <div id="msg_menu_conversations" class="widget-message-header-menu"><!-- menu de la liste des conversations -->
        <div id="search_conversation" class="widget-message-header-menu-right-part" >
            <input class="widget-message-search-input" type="text" placeholder="{{__('Rechercher')}}" onkeyup="displayConversationList()" />
        </div>
        <div id="msg_menu_conversations_add">
            <button class="widget-message-btn" type="button" onclick="editConvParticipants(0, 0, '', null)" title="{{__('Ajouter une conversation')}}">
                <i class="fa fa-plus"></i>
            </button>
        </div>
    </div>
    
    <!-- main div -->
    <div id="msg_conversations" class="msg_scroll">
        <?php if ($mv->vis) : ?>
        @include('components.widgets.message._conversations', [
                'conversations' => \App\Conversation::getConversationList('', $mv->id_report, Auth::user()),
                'id_report_context' => $mv->id_report,
                'user' => Auth::user(),
            ])
        <?php endif ?>
    </div> <!-- la liste des conversation -->

    <!-- 1 div containing the participants to a conversation -->
    <div id="msg_participants" class="msg_scroll"></div>

    <!-- 1 div containing the messages of a conversation -->
    <div id="msg_messages"></div>

    <!-- special parameters stored for the widget message -->
    <input type="hidden" id="msg_id_conv" value="{{$mv->vis ? 'conv' : '0'}}">
    <!-- 0 if the widget is not opened or if the participants div is opened / 'conv' if the conversation list is opened / id_conv if a conv is opened -->
</div>
