<?php
/** @var \App\Helper $helper */
/** @var \App\Conversation $conv */
/** @var \App\User $user */
/** @var boolean $allow_save_ld */
/** @var int $id_report_context */

$current_report = $id_report_context ? App\Report::find($id_report_context) : null;
?>
@foreach($conv->messages as $msg)
    @php
        if ($msg->id_user_sender == $user->id_user) {
            $sender = __("Moi : " );
        } elseif ($msg->id_user_sender == 1 || !$msg->id_user_sender) {
            $sender = "";
        } else {
            $sender = mb_substr($msg->user->first_name, 0, 1).mb_substr($msg->user->user_name, 0, 1).' : ';
        }
    @endphp
    <div class="msg_msg" x-data="messageView()">
        <span class="msg_time"
              title="{{$helper->relativeDate($msg->msg_time, true)}}">{{$helper->relativeDate($msg->msg_time)}}</span>
        <span :class="{'msg_msg_save': displayAttachedLd}" x-on:click.outside="displayAttachedLd=false">
            <span class="msg_sender"
                  title="{{$msg->id_user_sender ? $msg->user->first_name.' '.$msg->user->user_name : ''}}">{!! $sender !!}</span>
            @if($msg->id_labdoc)
                <i class="fa fa-paperclip"></i>
                @if($allow_save_ld)
                    <span class="attached_ld" x-on:click="toggleDownloadLD($el)"
                          title="{{__("Afficher / enregistrer le labdoc")}}">
                        {!! $msg->msg_content !!}
                    </span>
                @else
                    <a class="lb-a" href="/labdoc/{{$msg->id_labdoc}}" target="_blank">{!! $msg->msg_content !!}</a>
                @endif
                @if($allow_save_ld && $current_report)
                    <div id="save_ld_{{$msg->id_labdoc}}"  class="allow_save_ld" x-show="displayAttachedLd"
                         x-init="loadRPAndLDInfo(`{{json_encode($current_report->mission->reportParts()->get(['id_report_part','text','drawing','dataset','procedure','code']))}}`, {{App\Labdoc::find($msg->id_labdoc)}})">
                        <div class="widget-message-save-ld">
                            <span>{{__('Enregistrer le labdoc dans')}}</span>
                            <span style="white-space:nowrap;">
                                <select class="widget-message-select" x-model="idReportPart">
                                    <option value="" label="{{__('Choisissez une partie')}}"></option>
                                    @foreach($current_report->mission->reportParts as $rp)
                                        <option value="{{$rp->id_report_part}}" label="{{$rp->title}}"
                                                :selected="'{{$rp->id_report_part}}' === idReportPart"></option>
                                    @endforeach
                                </select>
                                <button class="widget-message-btn" x-on:click="saveAttachedLD({{$msg->id_labdoc}})">
                                    <i class="fas fa-check"></i>
                                </button>
                            </span>
                        </div>
                        <a class="lb-a" href="/labdoc/{{$msg->id_labdoc}}" target="_blank">{{__('Afficher le labdoc')}}</a>
                    </div>
                @endif
            @else
                <span class="msg_text">{!! $msg->msg_content !!}</span>
            @endif
        </span>
    </div>
@endforeach
