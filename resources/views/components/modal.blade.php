<div class="modal-container {{$headerClass}}" id="{{ $id }}" x-data="{{ $xData }}" x-show="displayedModal" 
     x-init="$el.style.height=window.outerHeight+'px'; $el.style.width=window.innerWidth+'px';">
    <div class="modal-content">
        <div class="modal-header">
            <h2 class="modal-title">{{ __($title) }}</h2>
        </div>
        @if($includeAlert)
            <div class="modal-alert-message" x-on:modal-alert-message.window="$el.innerText = $event.detail.message"></div>
        @endif
        <div class="modal-main">
            {{ $slot }}
        </div>
        <div class="modal-buttons">
            <button x-on:click="{{ $clickSubmitAction }}" title="{{ __("Valider") }}" class="modal-submit"><i class="fa fa-check disabled"></i></button>
            <button x-on:click="{{ $clickCancelAction }}" title="{{ __("Annuler") }}" class="modal-cancel"><i class="fa fa-times"></i></button>
        </div>
    </div>
</div>
