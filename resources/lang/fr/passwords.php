<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Votre mot de passe doit contenir au moins 8 caractères et correspondre à la confirmation.',
    'reset' => 'Votre mot de passe à été réinitialisé !',
    'sent' => 'Nous vous avons envoyé un lien de réinitialisation de mot de passe !',
    'token' => 'Ce token de réinitialisation de mot de passe est invalide.',
    'user' => "Nous ne trouvons pas d'utilisateurs correspondant à cet identifiant.",

];
