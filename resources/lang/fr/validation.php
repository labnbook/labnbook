<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => ':attribute doit être accepté.',
    'active_url' => ':attribute n\'est pas une URL valide.',
    'after' => ':attribute doit être une date après :date.',
    'after_or_equal' => ':attribute doit être une date après ou égale à :date.',
    'alpha' => ':attribute doit contenir uniquement des lettres.',
    'alpha_dash' => ':attribute doit contenir uniquement des : lettres, chiffres, tirets et underscores.',
    'alpha_num' => ':attribute doit contenir uniquement des lettres et des chiffres.',
    'array' => ':attribute doit être un tableau.',
    'before' => ':attribute doit être une date avant :date.',
    'before_or_equal' => ':attribute doit être une date avant ou égale à :date.',
    'between' => [
        'numeric' => ':attribute doit être entre :min et :max.',
        'file' => ':attribute doit avoir une taille entre :min et :max kilooctets.',
        'string' => ':attribute doit contenir entre :min et :max caractères.',
        'array' => ':attribute doit avoir entre :min et :max entrées.',
    ],
    'boolean' => ':attribute doit être "true" ou "false".',
    'confirmed' => 'la confirmation de :attribute ne correspond pas.',
    'date' => ':attribute n\'est pas une date valide.',
    'date_equals' => ':attribute doit être une date égale à :date.',
    'date_format' => ':attribute n\'est pas au format : :format.',
    'different' => ':attribute et :other doivent être différents.',
    'digits' => ':attribute doit contenir :digits chiffres.',
    'digits_between' => ':attribute doit contenir entre :min et :max chiffres.',
    'dimensions' => 'Les dimensions de l\'image :attribute sont invalides.',
    'distinct' => ':attribute doit être unique.',
    'email' => ':attribute doit être une adresse email valide.',
    'ends_with' => ':attribute doit finir par :values',
    'exists' => 'La section :attribute est invalide.',
    'file' => ':attribute doit être un fichier.',
    'filled' => ':attribute doit être rempli.',
    'gt' => [
        'numeric' => ':attribute doit être supérieur à value.',
        'file' => ':attribute doit être supérieur à value kilooctets.',
        'string' => ':attribute doit être supérieur à value caractères.',
        'array' => ':attribute doit contenir plus de :value entrées.',
    ],
    'gte' => [
        'numeric' => ':attribute doit être supérieur ou égale à  à :value.',
        'file' => ':attribute doit être supérieur ou égale à  à :value kilooctets.',
        'string' => ':attribute doit être supérieur ou égale à  à :value caractères.',
        'array' => ':attribute doit avoir :value entrées ou plus.',
    ],
    'image' => ':attribute doit être une image.',
    'in' => 'selected :attribute est invalide.',
    'in_array' => ':attribute n\'existe pas dans :other.',
    'integer' => ':attribute doit être un entier.',
    'ip' => ':attribute doit être une adresse IP valide.',
    'ipv4' => ':attribute doit être une adresse IPV4 valide.',
    'ipv6' => ':attribute doit être une adresse IPV6 valide.',
    'json' => ':attribute doit être une chaine JSON valide.',
    'lt' => [
        'numeric' => ':attribute doit être inférieur à value.',
        'file' => ':attribute doit être inférieur à value kilooctets.',
        'string' => ':attribute doit être inférieur à value caractères.',
        'array' => ':attribute doit contenir moins de :value entrées.',
    ],
    'lte' => [
        'numeric' => ':attribute doit être inférieur ou égale à  à :value.',
        'file' => ':attribute doit être inférieur ou égale à  à :value kilooctets.',
        'string' => ':attribute doit être inférieur ou égale à  à :value caractères.',
        'array' => ':attribute doit contenir moins de :value entrées.',
    ],
    'max' => [
        'numeric' => ':attribute ne doit pas être plus grand que :max.',
        'file' => ':attribute ne doit pas être plus grand que :max kilooctets.',
        'string' => ':attribute ne doit pas être plus grand que :max caractères.',
        'array' => ':attribute net doit pas contenir plus de :max entrées.',
    ],
    'mimes' => ':attribute doit être un fichier de type : :values.',
    'mimetypes' => ':attribute doit être un fichier de type : :values.',
    'min' => [
        'numeric' => ':attribute doit être au moins :min.',
        'file' => ':attribute doit être au moins :min kilooctets.',
        'string' => ':attribute doit être au moins :min caractères.',
        'array' => ':attribute doit contenir au moins :min entrées.',
    ],
    'not_in' => 'L\':attribute selection est invalide.',
    'not_regex' => 'Le format de:attribute est invalide.',
    'numeric' => ':attribute doit être un nombre.',
    'present' => ':attribute doit être présent.',
    'regex' => 'Le format de :attribute est invalide.',
    'required' => ':attribute est requis.',
    'required_if' => ':attribute est requis quand :other est :value.',
    'required_unless' => ':attribute est requis sauf si :other est dans :values.',
    'required_with' => ':attribute est requis quand :values est présent.',
    'required_with_all' => ':attribute est requis quand :values sont présents.',
    'required_without' => ':attribute est requis quand :values sont absent.',
    'required_without_all' => ':attribute est requis si aucun de :values n\'est présent.',
    'same' => ':attribute et :other doivent correspondrent.',
    'size' => [
        'numeric' => ':attribute doit être :size.',
        'file' => ':attribute doit être :size kilooctets.',
        'string' => ':attribute doit être :size caractères.',
        'array' => ':attribute doit contenir :size entrées.',
    ],
    'starts_with' => ':attribute doit commencer par l\'une des valeurs suivantes : :values',
    'string' => ':attribute doit être une chaine de caractères.',
    'timezone' => ':attribute doit être une zone valide.',
    'unique' => ':attribute est déjà utilisé.',
    'uploaded' => 'Erreur lors du téléversement  de :attribute .',
    'url' => 'Le format de :attribute est invalide.',
    'uuid' => ':attribute doit être un UUID valide.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail adresse" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
