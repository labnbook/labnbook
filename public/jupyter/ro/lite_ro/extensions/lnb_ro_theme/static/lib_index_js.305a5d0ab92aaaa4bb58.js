"use strict";
(self["webpackChunklnb_ro_theme"] = self["webpackChunklnb_ro_theme"] || []).push([["lib_index_js"],{

/***/ "./lib/index.js":
/*!**********************!*\
  !*** ./lib/index.js ***!
  \**********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _jupyterlab_apputils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @jupyterlab/apputils */ "webpack/sharing/consume/default/@jupyterlab/apputils");
/* harmony import */ var _jupyterlab_apputils__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_jupyterlab_apputils__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _jupyterlab_settingregistry__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @jupyterlab/settingregistry */ "webpack/sharing/consume/default/@jupyterlab/settingregistry");
/* harmony import */ var _jupyterlab_settingregistry__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_jupyterlab_settingregistry__WEBPACK_IMPORTED_MODULE_1__);


/**
 * Initialization data for the lnb_ro_theme extension.
 */
const plugin = {
    id: 'lnb_ro_theme:plugin',
    autoStart: true,
    requires: [_jupyterlab_apputils__WEBPACK_IMPORTED_MODULE_0__.IThemeManager],
    optional: [_jupyterlab_settingregistry__WEBPACK_IMPORTED_MODULE_1__.ISettingRegistry],
    activate: (app, manager, settingRegistry) => {
        const style = 'lnb_ro_theme/index.css';
        manager.register({
            name: 'lnb_ro_theme',
            isLight: true,
            load: () => manager.loadCSS(style),
            unload: () => Promise.resolve(undefined)
        });
        const id_ld = window.frameElement.id.split('ld_code_iframe_').pop();
        if (settingRegistry) {
            settingRegistry
                .load(plugin.id)
                .then(settings => {
                // console.log('lnb_ro_theme: settings loaded [id_labdoc=' + id_ld + '].', settings.composite);
            })
                .catch(reason => {
                console.error('lnb_ro_theme: failed to load settings [id_labdoc=' + id_ld + '].', reason);
            });
        }
        // console.log("lnb_ro_theme -> lnb: lnb_ro_theme is registered [id_labdoc=" + id_ld + "]");
    }
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (plugin);


/***/ })

}]);
//# sourceMappingURL=lib_index_js.305a5d0ab92aaaa4bb58.js.map
