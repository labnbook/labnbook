"use strict";
(self["webpackChunklnb_rw_save"] = self["webpackChunklnb_rw_save"] || []).push([["lib_index_js"],{

/***/ "./lib/index.js":
/*!**********************!*\
  !*** ./lib/index.js ***!
  \**********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _jupyterlab_notebook__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @jupyterlab/notebook */ "webpack/sharing/consume/default/@jupyterlab/notebook");
/* harmony import */ var _jupyterlab_notebook__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_jupyterlab_notebook__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _jupyterlab_settingregistry__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @jupyterlab/settingregistry */ "webpack/sharing/consume/default/@jupyterlab/settingregistry");
/* harmony import */ var _jupyterlab_settingregistry__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_jupyterlab_settingregistry__WEBPACK_IMPORTED_MODULE_1__);


/**
 * Initialization data for the lnb_rw_save extension.
 */
const plugin = {
    id: 'lnb_rw_save:plugin',
    autoStart: true,
    optional: [_jupyterlab_settingregistry__WEBPACK_IMPORTED_MODULE_1__.ISettingRegistry],
    requires: [_jupyterlab_notebook__WEBPACK_IMPORTED_MODULE_0__.INotebookTracker],
    activate: (app, notebookTracker, settingRegistry) => {
        const id_ld = window.frameElement.id.split('ld_code_iframe_').pop();
        if (settingRegistry) {
            settingRegistry
                .load(plugin.id)
                .then(settings => {
                console.log('lnb_rw_save: settings loaded [id_labdoc=' + id_ld + '].', settings.composite);
            })
                .catch(reason => {
                console.error('lnb_rw_save: failed to load settings [id_labdoc=' + id_ld + '].', reason);
            });
        }
        notebookTracker.currentChanged.connect((tracker, panel) => {
            window.addEventListener('message', event => {
                if (event.data.type === 'save_notebook_content') {
                    console.log('lnb_rw_save -> lnb: save_notebook_content [id_labdoc=' + event.data.id_ld + ']');
                    if (panel !== null) {
                        let ld_content;
                        if (panel.content.model !== null) {
                            ld_content = panel.content.model.toJSON();
                            window.parent.postMessage({
                                type: 'save_notebook_content',
                                ld_content: ld_content,
                                id_ld: event.data.id_ld
                            }, '*');
                        }
                        else {
                            console.error('lnb_rw_save -> lnb: error while saving the notebook, content is null [id_labdoc=' + event.data.id_ld + ']');
                        }
                    }
                    else {
                        console.error('lnb_rw_save -> lnb: error while saving the notebook [id_labdoc=' + event.data.id_ld + ']');
                    }
                }
            }, { once: false });
        });
    }
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (plugin);


/***/ })

}]);
//# sourceMappingURL=lib_index_js.111aef3779d27165d3a0.js.map