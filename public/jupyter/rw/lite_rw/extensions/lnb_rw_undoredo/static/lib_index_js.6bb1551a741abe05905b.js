"use strict";
(self["webpackChunklnb_rw_undoredo"] = self["webpackChunklnb_rw_undoredo"] || []).push([["lib_index_js"],{

/***/ "./lib/index.js":
/*!**********************!*\
  !*** ./lib/index.js ***!
  \**********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _jupyterlab_settingregistry__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @jupyterlab/settingregistry */ "webpack/sharing/consume/default/@jupyterlab/settingregistry");
/* harmony import */ var _jupyterlab_settingregistry__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_jupyterlab_settingregistry__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _jupyterlab_notebook__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @jupyterlab/notebook */ "webpack/sharing/consume/default/@jupyterlab/notebook");
/* harmony import */ var _jupyterlab_notebook__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_jupyterlab_notebook__WEBPACK_IMPORTED_MODULE_1__);


/**
 * Initialization data for the lnb_rw_undoredo extension.
 */
const plugin = {
    id: 'lnb_rw_undoredo:plugin',
    autoStart: true,
    optional: [_jupyterlab_settingregistry__WEBPACK_IMPORTED_MODULE_0__.ISettingRegistry],
    requires: [_jupyterlab_notebook__WEBPACK_IMPORTED_MODULE_1__.INotebookTracker],
    activate: (app, notebookTracker, settingRegistry) => {
        //const id_ld: any = window.frameElement.id.split('ld_code_iframe_').pop();
        if (settingRegistry) {
            settingRegistry
                .load(plugin.id)
                .then(settings => {
                console.log('lnb_rw_undoredo settings loaded:', settings.composite);
            })
                .catch(reason => {
                console.error('Failed to load settings for lnb_rw_undoredo.', reason);
            });
        }
        notebookTracker.currentChanged.connect((tracker, panel) => {
            function undo_redo(event) {
                if (event.data.type === 'undo_redo_notebook') {
                    console.log('lnb_rw_undoredo -> lnb: update_content [id_labdoc=' + event.data.id_ld + ']');
                    if (panel !== null) {
                        // 1 - Update content
                        const model = new _jupyterlab_notebook__WEBPACK_IMPORTED_MODULE_1__.NotebookModel();
                        model.fromJSON(JSON.parse(event.data.ld_content));
                        panel.content.model = model;
                        // 2 - Hide utils_function cell
                        const tag = "utils_functions";
                        panel.content.widgets.forEach((cell, index) => {
                            let arrayOfTags = cell.model.metadata.get("tags");
                            if (Array.isArray(arrayOfTags) && arrayOfTags != null && arrayOfTags != undefined) {
                                if (arrayOfTags.includes(tag)) {
                                    let layout = panel.content.widgets[index].layout;
                                    for (var i = 0; i < 5; i++) {
                                        layout.widgets[i].hide();
                                    }
                                    if (typeof document.querySelectorAll('div.jp-Cell-inputWrapper[aria-hidden="true"]')[0] !== 'undefined') {
                                        document.querySelectorAll('div.jp-Cell-inputWrapper[aria-hidden="true"]')[0]
                                            .closest('div.jp-Cell')
                                            .style.display = 'none';
                                    }
                                }
                            }
                        });
                    }
                    else {
                        console.error('lnb_rw_undoredo -> lnb: error while undo redo the notebook [id_labdoc=' + event.data.id_ld + ']');
                    }
                }
            }
            window.addEventListener('message', undo_redo, { once: false });
        });
    }
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (plugin);


/***/ })

}]);
//# sourceMappingURL=lib_index_js.6bb1551a741abe05905b.js.map