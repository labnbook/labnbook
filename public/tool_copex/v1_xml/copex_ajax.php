<?php
require (dirname(__DIR__).'/common/boot_laravel.php');


// on reçoit une demande du code l'image envoyé
if(isset($_POST["req"]) && $_POST["req"]=="getCodeImage"){
	$reponse = 'nok';
	$message = 'Erreur aucune image envoyé';

	//Si on a bien la path temporaire de l'image
	if(!empty($_FILES['image']['tmp_name'])){
		$reponse = 'ok';
		$message = base64_encode(file_get_contents($_FILES['image']['tmp_name']));
		$type = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
	}

	echo $reponse . "@" . $message . "@" . $type;
}
else{
	echo 'nok';
}
