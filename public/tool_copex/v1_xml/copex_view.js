// **********************************************************************************
//  Initialisation du chargement des fichiers XML/XSL, nécessaire aux fonctions
// **********************************************************************************
var copex_conf; // contient le contenu du fichier copex_conf.xml = configuration de copex
$.ajax({
	type: "GET",
	url: "/tool_copex/v1_xml/copex_conf.xml",
	dataType: 'xml'
})
	.fail(function (msg) { alert( __("Erreur : impossible de charger le fichier de configuration de copex") ); })
	.done(function (data) {	copex_conf = data; });

// *******************************************************************
//           Fonction initiale d'affichage
// *******************************************************************
function displayProtocol(id_ld, edition) {
	// ajout d'informations dans le tableau général des LD
	global_tab_ld[id_ld].ML = getMaterialListfromXML(id_ld) ; // tableau contenant tous les matériels du protocole (objets JS)
	global_tab_ld[id_ld].Actions = getActionListfromXML(id_ld) ; // tableau contenant toutes les actions accessibles (objets JS)
	global_tab_ld[id_ld].ld_locker = 0 ;
		// 0 : Rien en modification, pas de blocage
    	// "QHP_idld_idqhp" : id de la QHP en modification. Selon le cas, affichage d'une alerte ou validation de la QHP
		// "ml" : la liste de matériel est en modification => validation
    	// "proc" : Un élément du mode opératoire est en modification, on affiche l'alerte (Ancien comportement avec valeur 1)
	
	// affichages
	// création des 4 div pour les parties QHP, ML, procedure_root et procedure
	$('#labdoc_content_'+id_ld).html('<div id="QHP_'+id_ld+'"></div><div id="ML_'+id_ld+'" class="QHPM"></div><div id="proc_root_'+id_ld+'" class="cx_proc_root"></div><div id="procedure_'+id_ld+'"></div>') ; 
	displayQHP(id_ld, edition) ;  // affichage des parties QHP
	displayML(id_ld, edition) ;  // affichage de la liste de matériel
	displayProcedureRoot(id_ld, edition) ; // Affichage de la racine de l'arbre
	displayProcedure(id_ld, edition); // affichage de la partie procédure
}

// *******************************************************************
//           Fonction pour afficher les parties QHPM
// *******************************************************************

/**
 * Affiche les parties QHP d'un protocole d'idLD donné en mode affichage ou édition
 * @param {number} idLD - Identifiant du labdoc
 * @param {boolean} edition - Représente si on est en mode édition
 */
function displayQHP(idLD, edition) {
	var xml = $(global_tab_ld[idLD].ld_content); //On parcours le XML en jQuery pour générer le HTML
	var QPHs_HTML = '' ;
	var QHP_indice = 0; //Indice qui définit un identifiant unique pour chaque QHP du labdoc

	//On boucle sur les QHP présent dans le XML (quelque soit leurs type)
	xml.find("QHP").each(function(index){
		//On récupère les attributs dans le XML pour les ré-insérer en HTML
		var attribut_edit = $(this).attr('edit');
		var attribut_type = $(this).attr('type');
		//La variable qui va contenir le QHP généré (HTML)
		var QHP_html = '<div data-type="' + attribut_type + '" id="QHP_' + idLD + '_' + QHP_indice + '" class="QHPM">';
		//L'attribut classe si on peut éditer le QHP
		var class_edit = '';
		if(edition && attribut_edit == 'true') { class_edit = ' QHPMEditable'; }
		QHP_html += '<span class="' + class_edit + '">';
		/*QHP_html += '<span class="QHPM_icon">';
		//L'image en fonction du type de QHP
		if(attribut_type.indexOf("question") >= 0){
			// QHP_html += '<img src="/tool_copex/v1_xml/images/protocol_question.png" alt="Question" align="absmiddle"/>';
			QHP_html += '<i class="fa fa-bullseye" aria-hidden="true"></i>' ;
		}
		else if (attribut_type.indexOf("hypothesis") >= 0){
			//QHP_html += '<img src="/tool_copex/v1_xml/images/protocol_hypothesis.png" alt="Hypotheses" align="absmiddle"/>';
			QHP_html += '<i class="far fa-lightbulb" aria-hidden="true"></i>' ;
		}
		else if (attribut_type.indexOf("principle") >= 0){
			//QHP_html += '<img src="/tool_copex/v1_xml/images/protocol_principle.png" alt="Principe" align="absmiddle"/>';
			QHP_html += '<i class="fa fa-cogs" aria-hidden="true"></i>' ;
		}
		else {
			//QHP_html += '<img src="/tool_copex/v1_xml/images/protocol_default_qhp.png" alt="" align="absmiddle"/>';
			QHP_html += '<i class="far fa-circle" aria-hidden="true"></i>' ;
		}
		QHP_html += '</span>';
		*/
		//Le label
		var label = $(this).find('label').text();
		QHP_html += '<span class="protocol_part_title">' + label + '</span>';
		QHP_html += '</span>';
		//On récupère le contenu dans le XML
		var content = $(this).find('content').html();
		var default_content = $(this).find('defaultContent').html();
		//On récupère le commentaire et le libellé du commentaire (si existant)
		var comment = $(this).find('comment').html();
		var comment_label = __("Commentaire");
		if($(this).find('commentLabel').length > 0){ comment_label = $(this).find('commentLabel').text(); }
		
		//Le style en fonction du contenu (on n'affiche pas le defaut content si on a un contenu)
		style_default_content = '';
		if(content != ""){ style_default_content = ' style="display:none;"' }

		//On crée le bloc QHPContent et on insère le contenu récupéré
		QHP_html += '<div id="content_'+attribut_type+'_'+idLD+'"' + ' class="QHPMContent">';
			QHP_html += '<div ' + style_default_content + ' class="defaultQHPContent ' + class_edit + '">' + default_content + ' </div>';
			QHP_html += '<div class="QHPContent ' + class_edit + '">'+ content +'</div>';
			//Le commentaire s'il existe
			if($(this).find('comment').length > 0){
				QHP_html += '<div class="commentLabel">' + comment_label + ' :</div>';
				QHP_html += '<div class="comment ' + class_edit + '">'+ comment +' </div>';
			}
			//Les boutons (statiques)
			QHP_html += '<div class="divButtons">'
				QHP_html += '<span class="cx_button QHPValidate">'+__('Valider')+'</span>'
				QHP_html += '<span class="cx_button QHPCancel">'+__('Annuler')+'</span>'
			QHP_html += '</div>';
		QHP_html += '</div>';
		QHP_html += '</div>';

		//On ajout le html du QHP à la variable contenant tous les QHP
		QPHs_HTML += QHP_html; 

		//On incrémente l'indice
		QHP_indice++;
	});	
	
	// affichage de la structure HTML du protocole (les 3 parties QHP sont complètement affichées)
	$("#QHP_"+idLD).html(QPHs_HTML);
	
	//On abonne les QHP editable du labdoc à la fonction 'editQHP'
	$("#QHP_"+idLD+" .QHPMEditable").click(function(e){
		//On stoppe la propagation de l'évènement : ne valide pas le commentaire ou la description au passage
		if(e.stopPropagation) {e.stopPropagation();}
		var QHP_div = $(this).closest(".QHPM");
		editQHP(idLD, QHP_div.data('type'), QHP_div.attr('id'));
		//Si on clique pas sur un élément déjà en cours d'édition (champ contenant la description & commentaire)
		/*if(!$(this).hasClass('txtEdited') && !$(this).hasClass('commentEdited')){
			//On récupère la QHP div qui contient l'information sur le type de QHP
			var QHP_div = $(this).parent();
			if(!QHP_div.hasClass('QHPM')){
				QHP_div = $(this).parent().parent();
			}
			//Si on a bien le type de la QHP (sécurité) on peut lancer l'edition
			if(QHP_div.data('type')){
				var QHP_type = QHP_div.data('type');
				editQHP(idLD,QHP_type,QHP_div.attr('id'));
			}
		}*/
	});
}


// *******************************************************************
//             Fonctions pour afficher le matériel
// *******************************************************************

function displayML(idLD, edition) { // affichage des matériels
	var xml = $(global_tab_ld[idLD].ld_content);
	if (xml.find('materialList').length > 0 ) {
		// variables 
		var add_ml = xml.find('materialList').attr('add') ; 
		if (add_ml == "true" ) { var defaultSearchAddMaterial = __("Ajouter ou rechercher un matériel") ; }
		else if (add_ml == "false") { var defaultSearchAddMaterial = __("Rechercher un matériel") ; }
		var select_ml = xml.find('materialList').attr('select') ; 
		
		// titre
		var ml_html = '<span ' ;
		if (edition) {  ml_html += 'class="QHPMEditable"' } ;
		ml_html += '>';
		//ml_html += '<span class="QHPM_icon"><i class="fa fa-flask" aria-hidden="true"></i></span>' ;
		ml_html += '<span class="protocol_part_title">'+xml.find('materialList').find('label').text()+'</span></span>' ;
		// contenu
		ml_html += '<div class="QHPMContent" id="contentML_'+idLD+'">' ;
			ml_html += '<div class="display_ML ' ;// la liste sur 4 colonnes
			if (edition) {  ml_html += 'QHPMEditable' ; } 
			ml_html += '"></div>' ; 
			ml_html += '<div class="editML">' ;  // la liste du matériel en mode édition
				ml_html += '<div class="searchAddMaterial">' ; // la fenêtre de recherche / ajout de matériel
					ml_html += '<input type="text" name="search" placeholder="'+defaultSearchAddMaterial+'" id="searchAddMaterial_'+idLD+'" onclick="enterSearchAddMaterial('+idLD+')" onkeyup="keyUpSearchAddMaterial('+idLD+')"></input>' ;
					ml_html += '<i class="fa fa-times" aria-hidden="true" title="'+__('Effacer la recherche')+'" onclick="deleteSearchAddMaterial('+idLD+')"></i>' ;
				ml_html += '</div>' ;
				ml_html += '<div class="selectMaterial" id="selectMaterial_'+idLD+'">' ; // la liste du matériel sélectionnable (gauche)
					ml_html += '<div class="addMaterial" id="addMaterial_'+idLD+'">' ;
						ml_html += '<span class="btn_pointer" onclick="addMaterial('+idLD+')" title="'+__('Ajouter un nouveau matériel à la liste')+'"> ' + __('Ajouter le matériel') + ' <span style="font-weight:bold" id="newMaterial_'+idLD+'"></span> ' + __('à la liste') + '<i class="fa fa-check" aria-hidden="true"></i></span>' ;
				ml_html += '</div><div id="selectableMaterialList_'+idLD+'"></div></div>' ;
				ml_html += '<div class="displayMaterial" id="displayMaterial_'+idLD+'">' ; // la div d'affichage d'un seul matériel (droite)
					ml_html += '<input type="hidden" value="" id="idMaterial_'+idLD+'"></input> ' ;
					// ml_html += '<div class="imageMaterial" id="imageMat_'+idLD+'">Ici l\'image du matériel si elle existe</div>' ;
					ml_html += '<div><span class="displayMaterialLabel">'+__('Matériel') + __("&nbsp;:")+' </span></div>' ;
					ml_html += '<div><i class="fas fa-trash-alt" aria-hidden="true" title="'+__('Supprimer ce matériel de la liste')+'" id="deleteMat_'+idLD+'"></i><div id="nameMat_'+idLD+'"></div></div>' ;
					if (select_ml == "true") {
						ml_html += '<div class="displayMaterialLabel">'+__('Quantité sélectionnée');
							ml_html += ' <input type="text" name="iteration" id="selectedMat_'+idLD+'" class="cx_int_field" value="">' ;
							ml_html += '<div class="cx_int_increment">' ;
								ml_html += '<i class="fa fa-caret-up cx_int_plus" id="buttonPlusMat_'+idLD+'"></i>';
								ml_html += '<i class="fa fa-caret-down cx_int_minus" id="buttonMinusMat_'+idLD+'"></i>';
							ml_html += '</div>' ;
							ml_html += '<span class="txtAfterButtonsIncrementing" id="quantityMat_'+idLD+'"></span>' ;
						ml_html += '</div>' ;
						ml_html += '<div class="displayMaterialLabel">' ;
							ml_html += __('Sélectionné')+' <span class="isSelectedMat" id="isSelectedMat_'+idLD+'"></span>' ;
						ml_html += '</div>' ;
					}
					ml_html += '<div class="displayMaterialLabel">'+__('Description') + __("&nbsp;:") +'</div>' ;
					ml_html += '<div class="commentEdited" id="descriptionMat_'+idLD+'"></div>' ;
					ml_html += '<div class="displayMaterialLabel">'+__('Commentaire') + __("&nbsp;:")+'</div>' ;
					ml_html += '<div class="commentEdited" contenteditable="true" id="commentMat_'+idLD+'"></div>' ;
				ml_html += '</div>' ;
				ml_html += '<div class="spacer"></div>' ;
				ml_html += '<div class="divButtonsML">' ; // la div des boutons
					ml_html += '<span class="cx_button" onclick="closeML('+idLD+',\'validate\')">'+__('Valider')+'</span>' ;
					ml_html += '<span class="cx_button" onclick="closeML('+idLD+',\'cancel\')">'+__('Annuler')+'</span>' ;
		ml_html += '</div></div></div>' ;
		$("#ML_"+idLD).html(ml_html) ;
		
		$("#ML_"+idLD+" .QHPMEditable").click(function(e){
			//On stoppe la propagation de l'évènement : ne valide pas le commentaire ou la description au passage
			if(e.stopPropagation) {e.stopPropagation();}
			editML(idLD) ;
		}) ;
	
		$("#contentML_"+idLD).children(".display_ML").html(displayMaterialList(idLD));
		$("#selectableMaterialList_"+idLD).html(displaySelectableMaterialList(idLD, null));
	}
	else { $("#ML_"+idLD).remove() ; }
}

/**
 * code html de la liste du materiel en mode compact
 */
function displayMaterialList(idLD) {
	// liste à afficher
	if ($(global_tab_ld[idLD].ld_content).find("materialList").attr("select")==='true') {
		var ML = getMaterialList(idLD,null,'selected',null,0);
	} else {
		var ML = getMaterialList(idLD,null,'all',null,0);
	}
	var HTMLCode = '' ;
	for (var i=0 ; i< ML.length ; i++) {
        if (ML[i].hidden) {
            HTMLCode += '<span style="display:none">';
        }
        HTMLCode += '&#8226;&nbsp;'+ML[i].name ;
        if (ML[i].selected !== 1) { // affichage du nombre d'items sélectionnés
            HTMLCode += '&nbsp;('+ML[i].selected+')' ;
        } 
        HTMLCode += '' ;
        if (ML[i].comment) HTMLCode += '<span class="comment"> - '+ML[i].comment+'</span>' ; // affichage du comment
        HTMLCode += '<br/>' ;
        if (ML[i].hidden) {
            HTMLCode += '</span>';
        }
	}
	return HTMLCode ;
}

/**
 * Afiche la liste du matériel en mode édition
 * @param {number} idLD - L'identifiant du labdoc
 * @param {string} searchText - Le texte cherchée
 * @param {boolean} init - Si c'est l'initialisation ou non
 */
function displaySelectableMaterialList(idLD, searchText) {
	// récupération de la liste à afficher
	if ($(global_tab_ld[idLD].ld_content).find("materialList").attr("select")==='true') {
		selected_ML =  getMaterialList(idLD,searchText,'selected',null,0).concat(getMaterialList(idLD,searchText,'unselected',null,0)) ;// récupération de 2 tableaux ordonnés + concaténation
	} else {
		selected_ML = getMaterialList(idLD, searchText, 'all', null, 0);// récupération d'un seul tableau ordonné sans différencier les select ou non
	}
    HTMLCode = '';
	for (var i=0 ; i<selected_ML.length ; i++) { // affichage des différents matériels
        if (selected_ML[i].hidden) {
            HTMLCode += '<div style="display:none">';
        } else {
            HTMLCode += '<div>';
        }
        if ($(global_tab_ld[idLD].ld_content).find("materialList").attr("select")==='true') { // checkbox
            HTMLCode += '<input type="checkbox" ' ;
            HTMLCode += 'onclick="selectMaterial('+idLD+', '+selected_ML[i].id+')"' ; // permet de sélectionner / déselectionner un matériel
            if (selected_ML[i].selected > 0) HTMLCode += ' checked="checked"' ;
            HTMLCode += '></input>' ; 
        }
        else {  // bullet
            HTMLCode += '&#8226; ' ;
        }
        HTMLCode += '<span class="btn_pointer" onclick="displayMaterial('+idLD+', '+selected_ML[i].id+')">'+selected_ML[i].name ;
        if (selected_ML[i].selected > 1) HTMLCode += ' ('+selected_ML[i].selected+')' ; // affichage du nombre d'items sélectionnés
        HTMLCode += '<span class="comment">' ;
        if (selected_ML[i].description) HTMLCode += ' - '+selected_ML[i].description ; // affichage de la description
        if (selected_ML[i].comment) HTMLCode += '<i> - '+selected_ML[i].comment+'</i>' ; // affichage du comment
        HTMLCode += '</span></span></div>' ;
	}
	return HTMLCode ;
}

/**
 * Affiche le cadre de modification d'un matériel
 * @param {number} idLD - L'identifiant du labdoc
 * @param {number} idMat - L'identifiant du materiel
 */
function displayMaterial(idLD, idMat) { 
	// d'abord on vérifie si il n'y a pas un matériel à sauver (seulement si on change de matériel)
	if (document.getElementById('idMaterial_'+idLD).value != idMat) {
		if (!saveMaterial(idLD)) return ;
	}
	// on remet à jour la liste du matériel (au cas où un changement ait été opéré sur un matériel)
	document.getElementById('selectableMaterialList_'+idLD).innerHTML =displaySelectableMaterialList(idLD, null) ;
	
	document.getElementById('displayMaterial_'+idLD).style.display = 'block'; // affiche la div du matérial
	$('#idMaterial_'+idLD).val(idMat);
	//document.getElementById('idMaterial_'+idLD).value = idMat ; // l'id en balise cachée
	
	// Surlignage de l'élément qui est sélectionné dans la liste de matériel
	$("#selectableMaterialList_"+idLD+" > span.btn_pointer").filter(function() {
		// test si le texte de l'élément (en enlevant celui des descendants) correspond au nom du matériel
		if (global_tab_ld[idLD].ML[idMat].selected > 1) {
			return $(this).clone()    //clone the element
					  .children() //select all the children
					  .remove()   //remove all the children
					  .end()  //again go back to selected element
					  .html() == global_tab_ld[idLD].ML[idMat].name+" ("+global_tab_ld[idLD].ML[idMat].selected+")";
		} else {
			return $(this).clone()    //clone the element
					  .children() //select all the children
					  .remove()   //remove all the children
					  .end()  //again go back to selected element
					  .html() == global_tab_ld[idLD].ML[idMat].name;
		}
	}).addClass( "selectedMaterialSpan" );
	
	// l'image
	/*if (global_tab_ld[idLD].ML[idMat].image) {
		document.getElementById('imageMat_'+idLD).style.display = 'block' ;
		document.getElementById('imageMat_'+idLD).innerHTML = "ici l'image "+global_tab_ld[idLD].ML[idMat].image ;
	} else {document.getElementById('imageMat_'+idLD).style.display = 'none' ; }*/
	
	// le nom, éditable ou pas
	$('#nameMat_'+idLD).text(global_tab_ld[idLD].ML[idMat].name) ;
	if (global_tab_ld[idLD].ML[idMat].definedBy == 'user') {
		document.getElementById('nameMat_'+idLD).contentEditable = true;
		document.getElementById('nameMat_'+idLD).className = 'materialNameEdited';
	}
	else {
		document.getElementById('nameMat_'+idLD).contentEditable = false;
		document.getElementById('nameMat_'+idLD).className = '';
	}
	
	// quantité sélectionnée, avec un maximum ou pas
	if ($(global_tab_ld[idLD].ld_content).find("materialList").attr("select")==='true') {
		$('#selectedMat_'+idLD).val(global_tab_ld[idLD].ML[idMat].selected) ;
		if (global_tab_ld[idLD].ML[idMat].quantity == 1 || isNaN(global_tab_ld[idLD].ML[idMat].quantity)) { // si quantity = 1 ou null (=NaN), on ne peut pas changer la quantité (on enlève la div correspondante qui ne sert plus)
			$("div.displayMaterialLabel:contains('"+__("Quantité sélectionnée") +"')").hide();
			$("div.displayMaterialLabel:contains('"+__("Sélectionné")+"')").show();
			if (global_tab_ld[idLD].ML[idMat].selected > 0){ // si le matériel est sélectionné
				$('#isSelectedMat_'+idLD).html(__(' oui'));
			} else {
				$('#isSelectedMat_'+idLD).html(__(' non'));
			}
		} else if (global_tab_ld[idLD].ML[idMat].quantity == 100000){ // si la quantité est infinie (=100000)
			$("div.displayMaterialLabel:contains('"+__("Sélectionné")+"')").hide();
			$("div.displayMaterialLabel:contains('"+__("Quantité sélectionnée") +"')").show();
			$('#quantityMat_'+idLD).html("");
		} else { // si il y a une quantité >1 qui est précisée
			$("div.displayMaterialLabel:contains('"+__("Sélectionné")+"')").hide();
			$("div.displayMaterialLabel:contains('"+__("Quantité sélectionnée") +"')").show();
			$('#quantityMat_'+idLD).html(" (max : "+global_tab_ld[idLD].ML[idMat].quantity+")");
		}
	}

	// description, editable ou pas
	$('#descriptionMat_'+idLD).html(global_tab_ld[idLD].ML[idMat].description) ;
	$('#descriptionMat_'+idLD).show();
	$("div.displayMaterialLabel:contains('"+__("Description") + __("&nbsp;:") + "')").show();
	if (global_tab_ld[idLD].ML[idMat].definedBy == 'user') {
		document.getElementById('descriptionMat_'+idLD).contentEditable = true;
		document.getElementById('descriptionMat_'+idLD).className = 'commentEdited';
	}
	else {
		if (global_tab_ld[idLD].ML[idMat].description != "") {
			document.getElementById('descriptionMat_'+idLD).contentEditable = false;
			document.getElementById('descriptionMat_'+idLD).className = 'comment';
		} else {
			$('#descriptionMat_'+idLD).hide();
			$("div.displayMaterialLabel:contains('"+__("Description") + __("&nbsp;:") + "')").hide();
		}
	}
	
	// le commentaire
    if (global_tab_ld[idLD].ML[idMat].comment) {
        document.getElementById('commentMat_'+idLD).innerHTML = global_tab_ld[idLD].ML[idMat].comment ;
    }
	
	// attribution de la fonction au bouton delete
	if (global_tab_ld[idLD].ML[idMat].definedBy==="user") {
		document.getElementById('deleteMat_'+idLD).onclick=function(){
			if (confirm(__('Ce matériel va être supprimé définitivement de votre liste.'))) { deleteMaterial(idLD, idMat); }
		};
		document.getElementById('deleteMat_'+idLD).style.display = 'inline';
	}
	else document.getElementById('deleteMat_'+idLD).style.display = 'none';
}


// *******************************************************************
//           Fonctions pour afficher l'arbre de la procédure
// *******************************************************************

/**
 * Affiche l'arbre de la procédure
 * @param {number} idLD - L'identifiant du labdoc
 * @param {boolean} edition - Boolean représentant le mode édition ou non
 */
function displayProcedureRoot(idLD, edition) {
	var procedureName = $(global_tab_ld[idLD].ld_content).find("procedure > label").text() ;
	var procedureRootContent = '<a class="jstree-drop"><span class="protocol_part_title">'+procedureName+'</span></a></div>' ;
	procedureRootContent += '<div id="JSTreeRootCM_'+idLD+'" class="JSTreeRootContextMenu"></div> ' ;
	$("#proc_root_"+idLD).html(procedureRootContent) ;
	if (edition && !$(global_tab_ld[idLD].ld_content).find("procedure > label").attr("no_menu")) {
		$("#proc_root_"+idLD).addClass("JSTreeRoot") ;
		$("#proc_root_"+idLD).find('a').click(function(e) { e.stopPropagation() ; openJSTreeRootCM(idLD) ; }); 
	}
}

/**
 * Permet d'afficher les procedures
 * @param {number} idLD - L'identifiant du labdoc
 * @param {boolean} edition - Boolean représentant le mode édition ou non
 */
function displayProcedure(idLD, edition) {
	// la div dans laquelle on met l'arbre
	var divProcedure = $("#procedure_"+idLD) ;
	// Créer une copie du Xml Client qui permet de remplacer les balises <parameter> par des balises <span> sans modifier le XML client
	var xml = global_tab_ld[idLD].ld_content.cloneNode(true) ;
	replaceParameterIntoSpan($(xml).find('ul li[rel="action"] > a > content'));
	var ul = $(xml).find("ul").first();
	var HTMLJSTree;
	if (ul.length === 0) {
		logError("displayProcedure() has no list to display in LD " + idLD, "error", divProcedure.html());
		HTMLJSTree = '<ul></ul>';
	} else {
		HTMLJSTree = XMLToString(ul) ; // le contenu HTML du JSTree (à partir de la copie du XML client

		// On gère les commentLabel on les transformant en span caché, ils vont uniquement servir en mode édition
		HTMLJSTree = HTMLJSTree.replace('<commentLabel>','<span class="commentLabelEdit">');
		HTMLJSTree = HTMLJSTree.replace('</commentLabel>','</span>');
	}

	if (!edition) {  // affichage de l'arbre hors édition
		divProcedure.jstree({
			"plugins" : [ "html_data", "types",	"themes" ],
			"html_data" : { "data" : HTMLJSTree } ,
			"types" : { "types" : {
					"step" : { "icon" : { "image" : "/tool_copex/v1_xml/images/AdT_step.png" }},
					"step_iter" : { "icon" : { "image" : "/tool_copex/v1_xml/images/AdT_step_iter.png" }},
					"action" : { "icon" : { "image" : "/tool_copex/v1_xml/images/AdT_action.png" }}
			}}
		});

		//Une fois chargé on ajuste le HTML
		divProcedure.bind("loaded.jstree", function () { 
			// supprime les pointillés
			divProcedure.jstree("hide_dots") ; 
			//NT on ajoute la classe cx_image_uploaded aux images incorporés
			divProcedure.find('img').addClass('cx_image_uploaded');
		});
	}
	
	// Si on est en mode édition
	if (edition) {
		// Construit de manière dynamique le menu présentant les actions structurées disponibles pour la mission
		var subMenu = {};
		var subMenuAfter = {};
		var onlyFreeActions = true ;
		// le tableau des actions
		var AL = global_tab_ld[idLD].Actions ; 
		for (var idA = 0; idA < AL.length; idA++) {
			subMenu[idA] = { "label" : AL[idA].name, "action" : function (obj) { 
				addNode(idLD,"action",$("#vakata-contextmenu ul li ul li a:hover").attr("rel"),obj); }
			};
			subMenuAfter[idA] = { "label" : AL[idA].name, "action" : function (obj) { 
				addNode(idLD,"action",$("#vakata-contextmenu ul li ul li a:hover").attr("rel"),obj.parent().parent(),obj.index() + 1); }
			};
			if (AL[idA].structured != "none"){ onlyFreeActions = false ;}
		}
		if (onlyFreeActions){ subMenu = subMenuAfter = null ;} // pas de sous menu action si il n'y a que l'action libre

		divProcedure.jstree({
			"plugins" : [ "html_data", 		// convertit les listes à puces imbriquée HTML en arbres interactifs JSTree
				"types", 			// affiche les icônes adaptées au type de noeud
				"themes", 			// controle l'apparence de l'arbre --> theme default
				"contextmenu",  	// gère le menu contextuel
				"crrm", 			// gère la création, le renommage, la suppression et le déplacement de nœuds par l'utilisateur.
				"ui",				// permet de surligner les noeuds
				"dnd"] ,		    // active le drag'ndrop
			"html_data" : { "data" : HTMLJSTree } , // le contenu de l'arbre
			"core" : {
				"animation" : 80, // pour que la fermeture/ouverture se fasse rapidement
				"html_titles" : true // pour que les noeuds puissent contenir de l'html
			} ,
			"types" : {
				"types" : {
					"root" : { "icon" : { "image" : "/tool_copex/v1_xml/images/protocol_procedure.png" }},
					"step" : { "icon" : { "image" : "/tool_copex/v1_xml/images/AdT_step.png" }},
					"step_iter" : { "icon" : { "image" : "/tool_copex/v1_xml/images/AdT_step_iter.png" }},
					"action" : {
						"icon" : { "image" : "/tool_copex/v1_xml/images/AdT_action.png" },
						"max_children" : 0 // empêche le déplacement d'un noeud dans une action
					}
				}
			},
			"dnd" : {
				"drop_check" : function(data) { // affiche la croix si interdit de dropper l'objet draggé (ne tient pas compte de son parent)
					if ( data.o.attr("move")=="false" ) return false ;
					else return true ;
				},
				"drop_finish" : function (data) { // permet de dropper un noeud sur la racine
					divProcedure.jstree("move_node",data.o, -1,"last",false) ;
				}},
			"crrm" : { "move" :  {
				"check_move" : function (m) { // l'utilisateur a-t-il les droits pour faire son déplacement ? o : node being moved ; op : former parent ; np : new parent ; cy ; copy
					if ( !m.cy && (m.o.attr("move")=="false" || m.op.attr("modify")=="false")) return false ;
					else if (m.np.attr("modify")=="false") return false ;
					else return true ;
				}
			}},
			"contextmenu" : {
				"select_node" : true ,
				"items" : function(node) { return customMenu(node,idLD, subMenu, subMenuAfter) ; } // ça c'est un peu tricky !! pour passer les parametres supplémentaires (idLD) tout en gardant le node
			}
		});

		//Une fois chargé on ajuste le HTML
		divProcedure.bind("loaded.jstree", function () { 
			//On ajoute la classe cx_image_uploaded aux images incorporés
			divProcedure.find('img').addClass('cx_image_uploaded');
		});

		// met à jour le contenu du menu de la racine
		var rootMenuContent ='<ul><li><ins><img align="absmiddle" src="/tool_copex/v1_xml/images/AdT_addStep.png"></ins><a onclick="addNode('+idLD+',\'step\',\'\',-1)" rel="addStepIn">'+__('Ajouter une étape')+'</a></li>';
		rootMenuContent += '<li class="action-item"><ins><img align="absmiddle" src="/tool_copex/v1_xml/images/AdT_addAction.png"></ins><a rel="addActionIn"  ';
		if (!subMenu) rootMenuContent += 'onclick="addNode('+idLD+',\'action\',0,-1)">' ;
		else rootMenuContent += '><span style="float:right;">»</span>';
		rootMenuContent += __('Ajouter une action')+'</a>' ;
		// on construit le sous-menu
		if (subMenu) { 
			rootMenuContent += '<ul class="action-sub-nav">';
			for (var idA = 0; idA < global_tab_ld[idLD].Actions.length; idA++) {
				rootMenuContent += '<li><ins></ins><a onclick="addNode('+idLD+',\'action\','+idA+',-1)">'+global_tab_ld[idLD].Actions[idA].name+'</a></li>' ;
			}
			rootMenuContent += '</ul>' ;
		}
		rootMenuContent += '</li>' ;	
		rootMenuContent +='<li><ins></ins><a onclick="pasteInRoot('+idLD+')" rel="paste">'+__('Coller une étape ou action')+'</a></li></ul>' ;

		//alert('on met le rootmenu '+idLD+'  '+rootMenuContent) ;
		$("#JSTreeRootCM_"+idLD).html(rootMenuContent) ;
		
		// met le curseur comme pointer sur les éléments du JSTree
		$(divProcedure, 'a').css('cursor', 'pointer');
		
		// permet d'ouvrir les menus contextuels au clic gauche
		divProcedure.bind("select_node.jstree", function (event, data) {
  			setTimeout(function() {if (!data.rslt.obj.children('a').hasClass("jstree-contextmenu-disabled")) {data.rslt.obj.children('a').trigger('contextmenu')}}, 100);
		});
		
		// permet d'ouvrir l'édition d'un noeud avec un double-clique
		divProcedure.dblclick("select_node.jstree", function () {
			var a = $.jstree._focused().get_selected(); // récupère le noeud sélectionné
			if (!a.children('a').hasClass("jstree-contextmenu-disabled") && $(a).attr("edit") !== 'false') {
				editNode(a, idLD, false);
			}
		});
		
		//trigger de sauvegarde qui est déclenché lors du déplacement d'un noeud et autres modifs de l'arbre
		divProcedure.bind("move_node.jstree",function(){
			saveProcedure(idLD) ;
		});
	}
	
	//Lorsque le JSTree de la procédure à fini de charger
	divProcedure.bind("loaded.jstree", function () {
		// ouvrir tous les noeuds
    	divProcedure.jstree("open_all",-1);

		//En scope PDF on a pour objectif d'imprimer donc on change les bg-image par des images
		if(typeof global_scope !== 'undefined' && global_scope == 'pdf'){
			//On enleve le CSS des jstree icone comme on va les remplacer par de vrais images
			$('#labdoc_content_' + idLD + ' .jstree-icon').css('background-image','none');
			//On remplace les icones par des images pour l'impression
			$('#labdoc_content_' + idLD + ' .jstree li[rel="action"] > a > .jstree-icon').html('<img src="/tool_copex/v1_xml/images/AdT_action.png">');
			$('#labdoc_content_' + idLD + ' .jstree li[rel="step"] > a > .jstree-icon').html('<img src="/tool_copex/v1_xml/images/AdT_step.png">');
			$('#labdoc_content_' + idLD + ' .jstree li[rel="step_iter"] > a > .jstree-icon').html('<img src="/tool_copex/v1_xml/images/AdT_step_iter.png">');
		}
    }) ;
}

/**
 * Défini le menu contextuel, les actions possibles sur les étapes/actions
 * @param {*} node - La node en question
 * @param {*} idLD - L'identifiant du labdoc
 * @param {*} subMenu - Le sous-menu pour les actions qui s'ajoute à la fin d'une étape
 * @param {*} subMenuAfter - Le sous-menu pour les actions "après" (traitement différent)
 */
function customMenu(node, idLD, subMenu, subMenuAfter) {
	var addActionInObj ; // cet objet est différent suivant si on a que des actions libres ou non
	if (subMenu) {
		addActionInObj = {
			"label" : __("Ajouter une action (dans)"),
			"icon" : "/tool_copex/v1_xml/images/AdT_addAction.png",
			"submenu" : subMenu
		}
		addActionAfterObj = {
			"label" : __("Ajouter une action (après)"),
			"icon" : "/tool_copex/v1_xml/images/AdT_action.png" ,
			"submenu" : subMenuAfter
		}
	}
	else { // actions libres
		addActionInObj = {
			"label" : __("Ajouter une action (dans)"),
			"icon" : "/tool_copex/v1_xml/images/AdT_addAction.png",
			"action" : function (obj) { addNode(idLD,"action",0,obj) ; }
		}
		addActionAfterObj = {
			"label" : __("Ajouter une action (après)"),
			"icon" : "/tool_copex/v1_xml/images/AdT_action.png" ,
			"action" : function (obj) {
				//On envoie obj.parent car il faut envoyer ici l'etape dans laquelle on veut insérer l'action or on a cliqué sur une action
				//Le dernier parametre est la position
				addNode(idLD,"action",0,obj.parent().parent(),obj.index() + 1) ; 
			}
		}
	}
	var items = {
		// ajoute tous les éléments qui seront ou non supprimés ensuite
		addActionIn : addActionInObj, // défini précédemment
		addStepIn : {
			"label" : __("Ajouter une étape (dans)"),
			"icon" : "/tool_copex/v1_xml/images/AdT_addStep.png",
			"action" : function (obj) { addNode(idLD,'step','',obj) ; }
		},
		//On ajoute une ation après l'élément ciblé (clic)
		addActionAfter : addActionAfterObj,
		addStepAfter : {
			"label" : __("Ajouter une étape (après)"),
			"icon" : "/tool_copex/v1_xml/images/AdT_step.png",
			"separator_after" : true, 
			"action" : function (obj) { 
				//On envoie obj.parent , node dans laquelle on veut insérer l'étape
				//Le dernier parametre est la position
				addNode(idLD,"step",0,obj.parent(),obj.index() + 1) ; 
			 }
		},
		edit : {
			"label" : __("Modifier"),
			"separator_before" : true, 
			"action" : function (obj){ editNode(obj, idLD, false); }
		},
		//On transforme une étape en action
		changeToAction : {
			"label" : __("Transformer en action"),
			"action" : function (obj) {
				 $(obj).attr("rel", "action"); 
				 saveProcedure(idLD);
			}
		},
		change : {
			"label" : __("Transformer en étape"),
			"action" : function (obj) {
				$(obj).attr("rel", "step");
				//Pas d'images liés aux étapes
				$(obj).find('.cx_image_uploaded').remove();
				saveProcedure(idLD) ;
			}
		},
		suppress : {
			"label" : __("Supprimer"),
			"separator_after" : true, 
			"action" : function (obj) { 
				if (obj.attr("rel") != "step" || confirm(__("Souhaitez vous supprimer cette étape et tous les éléments qu'elle contient ?"))) { // confirmation que dans le cas des étapes
					this.remove(obj);
					saveProcedure(idLD) ;
					setTimeout( "$('#vakata-contextmenu').hide();", 100) ;// hack pour fermer le menu qui se réouvre sur le noeud supérieur (why ????) après une suppression
				}
			}
		},
		copy : {
			"label" : __("Copier"),
			"separator_before" : true, 
			"action": function (obj) { this.copy(obj); }
		},
		paste : {
			"label" : __("Coller"),
			"action": function (obj) {
				this.paste(obj);
				saveProcedure(idLD) ;
			}
		}
	};
	
	//Si c'est une action ou une étape qu'on ne peut pas modifier
	if ($(node).attr("rel") == "action" || $(node).attr("modify") == "false") {
		delete items.addActionIn ;
		delete items.addStepIn ;
		delete items.addStepAfter ;
		delete items.paste ;
	}
	//Si c'est une action ou une étape avec des actions ou une étape non modifiable ou itérable on ne propose pas l'action de : "Trasnformer en action"
	if ($(node).attr("rel") == "action" || ($(node).attr("rel")=="step" && $(node).find('ul li').length > 0) || $(node).attr("edit") == "false" || $(node).attr("rel") == "step_iter"){
		delete items.changeToAction ;
	}
	//Si c'est une étape OU qu'on est sur une étape itérable 
	if ($(node).attr("rel")=="step" || $(node).attr("rel")=="step_iter") {
		delete items.addActionAfter ;
		delete items.change;
	}
	//Si on est sur une action structuée ou non editable
	if( $(node).attr("structured") != "none" || $(node).attr("edit") == "false"){
		delete items.change;
	}
	// le premier parent de type LI
	if ($(node.parents('li:eq(0)')).attr("modify") == "false") { 
		delete items.addActionAfter ;
		delete items.addStepAfter ;
	}
	//Si pas editable on enlève l'option "modifier"
	if ($(node).attr("edit") == "false") {
		delete items.edit ;
	}
	//Si pas supprimable ou que c'est la racine
	if ($(node).attr("suppr") == "false" || $(node.parents('li:eq(0)')).attr("modify") == "false") {
		delete items.suppress ;
	}
	//Si on ne peut pas copier l'élément
	if ($(node).attr("copy") == "false") {
		delete items.copy ;
	}
	return items;
}





/**
 * Cette fonction remplace les balises <span> correspondant aux paramètres dans le contenu des actions structurées lorsqu'on passe en mode édition
 * @param {*} content - Le contenu HTML à transformer
 * @param {*} idLD - L'identifiant du labdoc
 * @return - Transforme directement le HTML dans le DOM
 */
function replaceSpanForEdition(content, idLD){
	//pour chaque balise <span> contenu dans la balise de classe 'contentStructured' (correspond aux parameters dans XML)
	$(content).children("span").each(function() {
		// on récupère les attributs génériques de la balise <span> 
		var id_param = $(this).attr('id');
		var name_param = $(this).attr('name'); 
		var length_param = $(this).attr('length');

		// si c'est un paramètre libre => Transformation en input type texte
		if($(this).attr('type') == "free") { 
			 // récupère le contenu de la balise <span>
			var content_param = $(this).html();
			// On modifie uniquement si le contenu n'est pas un 'X' : sélectionné en tant que paramètre dans le tableau itératif
			if (content_param != 'X') { 
				// remplace balises <span> par un input avec les valeurs correspondantes
				$(this).replaceWith('<input id="'+id_param+'" type="text" name="'+name_param+'" size="'+length_param+'" value="'+content_param+'" type_param="free" />');
			}
		}
		
		// si c'est un paramètre libre de liste déroulante  => Transformation en select
		else if($(this).attr('type') == "freeSelect") {
			 // récupère le contenu de la balise <span>
			var content_param = $(this).html();
			var selected_value = $(this).text();
			var new_content = '';
			if (content_param != '') {
				new_content += '<select type="'+$(this).attr('type')+'" id="'+id_param+'" name="'+name_param+'">';
				//On recupère le tableau d'option
				var array_option = $($(this).attr('value').split('|@|'));
				//On supprime les cases vides du tableau
				array_option = array_option.filter( function(indice,valeur){return valeur != ''} );
				//On parcours le talbeau pour re-créer la balise <select>
				array_option.each(function(indice,valeur){
					// création option (en la sélectionnant si besoin)
					if(selected_value==valeur){
						new_content += '<option selected>' + valeur + '</option>'; 
					} else {
						new_content += '<option>' + valeur + '</option>';
					}
				});
				new_content += '</select>';
			}
			$(this).replaceWith(new_content);
		} 
		
		//si c'est un paramètre avec un matériel
		else if ($(this).attr('type') == "material") {
			// récupère le contenu de la balise <span>
			var content_param = $(this).html(); 
			// On modifie uniquement si le contenu n'est pas un 'X' : sélectionné en tant que paramètre dans le tableau itératif
			if (content_param != 'X') {
				var mat_type_param = $(this).attr('mat_type');
				var new_content = '<select id="'+id_param+'" name="'+name_param+'" type="material"';
				// si l'attribut 'mat_type' n'est pas présent
				if(!(mat_type_param == 'undefined' || typeof mat_type_param == 'undefined')) {
                    new_content += 'mat_type="'+mat_type_param+'"';
                } 
				new_content += '><option/>';
				// récupère la liste de matériel sélectionné
				var ML = getMaterialList(idLD,null,'selected',null,0);
				// si il n'y a pas de type de matériel (affiche tout le matériel sélectionné)
				if (mat_type_param == 'undefined' || typeof mat_type_param == 'undefined') { 
					 // parcours de la liste de matériel
					for (var i=0; i < ML.length; i++) {
						// récupère le nom du matériel
						var material = ML[i].name ; 
						// et son nombre le cas échéant
						if (ML[i].selected !== 1) material += ' ('+ML[i].selected+')' ; 
						// Sélectionne à l'affichage le matériel déjà sélectionné (contenu balise <span>).
						if(content_param == material) { 
							new_content += '<option selected>'+material+'</option>';
						} else {
							var usable = isMaterialUsable(material, idLD);
							if(usable) new_content += '<option>'+material+'</option>';
						}
					}
					// Ajoute des options si il y a des matériels créés.
					$(global_tab_ld[idLD].ld_content).find("materialCreated").each(function(){
						var param_id = $(this).children("name").attr("paramId");
						var name_param = $(this).parent().children("content").children("parameter[id='"+param_id+"']").attr("name");
						$(global_tab_ld[idLD].ld_content).find("ul").find("parameter[id='"+param_id+"'][name='"+name_param+"']").each(function(){
							var valueCreatedMat = $(this).html();
							if (valueCreatedMat == 'X') valueCreatedMat = name_param;
							// Sélectionne à l'affichage le matériel déjà sélectionné (contenu balise <span>).
							if(content_param == valueCreatedMat) { 
								new_content += '<option selected>'+valueCreatedMat+'</option>';
							} else {
								var usable = isMaterialUsable(valueCreatedMat, idLD);
								if(usable) new_content += '<option>'+valueCreatedMat+'</option>';
							}
						});
					});
				}
				else { // sinon on regarde le type de chaque matériel pour savoir si on l'affiche ou pas.
					for (var i=0 ; i < ML.length ; i++) {
						// parcours les différents types de chaque matériel
						for (var j=0; j < ML[i].type.length; j++) {
							// Si le type de matériel correspond on effectue un traitement sur le matériel
							if (ML[i].type[j] == mat_type_param) { 
								// récupère le nom du matériel
								var material = ML[i].name;
								// et son nombre le cas échéant
								if (ML[i].selected !== 1) material += ' ('+ML[i].selected+')' ;
								// Sélectionne à l'affichage le matériel déjà sélectionné (contenu balise <span>).
								if(content_param == material) { 
									new_content += '<option selected>'+material+'</option>';
								} else {
									var usable = isMaterialUsable(material, idLD);
									if(usable) new_content += '<option>'+material+'</option>';
								}
							}
						}
					}
					// Ajoute des options si il y a des matériels créés.
					$(global_tab_ld[idLD].ld_content).find("materialCreated").each(function(){
						var param_id = $(this).children("name").attr("paramId");
						var name_param = $(this).parent().children("content").children("parameter[id='"+param_id+"']").attr("name");
						$(this).children("type").each(function(){
							if ($(this).html() == mat_type_param) {
								$(global_tab_ld[idLD].ld_content).find("ul").find("parameter[id='"+param_id+"'][name='"+name_param+"']").each(function(){
									let valueCreatedMat = $(this).html();
									if (valueCreatedMat == 'X') {
										valueCreatedMat = name_param;
									}
									// Sélectionne à l'affichage le matériel déjà sélectionné (contenu balise <span>).
									if(content_param == valueCreatedMat) { 
										new_content += '<option selected>'+valueCreatedMat+'</option>';
									} else if (isMaterialUsable(valueCreatedMat, idLD)) {
										new_content += '<option>'+valueCreatedMat+'</option>';
									}
								});
							}
						});
					});
				}
				new_content += '</select>';
				$(this).replaceWith(new_content);
			}
		}
		
		// si c'est un paramètre avec une quantité
		else if ($(this).attr('type') == "quantity") { 
			// Récupération des attributs et contenus de la balise <span>
			var content_param = $(this).children('value').html();
			var quant = $(this).attr('quant_type');
			var new_content = '';
			// On modifie uniquement si le contenu n'est pas une 'X' : sélectionné en tant que paramètre dans le tableau itératif
			if (content_param != 'X') { 
				new_content += '<input id="'+id_param+'" type="text" name="'+name_param+'" size="'+length_param+'" value="'+content_param+'" type_param="quantity" quant_type="'+quant+'" />'; // création variable de remplacement et insertion input avec valeurs
			} else {
				new_content += '<span id="'+id_param+'" type="quantity" name="'+name_param+'" length="'+length_param+'" quant_type="'+quant+'" >'+content_param+'</span>';
			}
			var contentUnit = $(this).children('unit').html(); 
			// charge le fichier xml des unités
			var uniteXML = copex_conf;
			// si l'attribut 'choose' de l'unité est true, on permet le choix à l'utilisteur avec un select
			if ($(this).children('unit').attr('choose') == 'true'){  
				// on créé une balise <select>
				new_content += '<select name="'+name_param+'">';
				// pour chaque quantité
				$(uniteXML).find("quantity").each(function(){ 
					// si elle correspond à notre quantité
					if($(this).attr("quant_type") == quant) { 
						// pour chaque unité
						$(this).find("unit").each(function(){
							//si une langue est spécifiée pour le symbole
							if($(this).find("symbol[lang]").length){ 
								// récupère le symbole avec la langue spécifiée
								var symbole = $(this).find("symbol[lang='" + labnbook.lang + "']").html(); 
							} else {
								// récupère le symbole sans langue spécifiée
								var symbole = $(this).find("symbol").html(); 
							}
							// définition de la valeur par défaut..
							if (contentUnit == "") contentUnit = symbole; 
							// si il y a des préfixes, on les parcours
							if ($(this).find("prefix").length){ 
								$(this).find("prefix").each(function(){
									// La valeur du prefixe
									var prefixe = $(this).html();

									// création option (en la sélectionnant si besoin)
									if(contentUnit==prefixe+symbole){
										new_content += '<option selected>'+prefixe+symbole+'</option>'; 
									} else {
										new_content += '<option>'+prefixe+symbole+'</option>';
									}
								});
							} 
							// si on n'a pas de préfixes
							else { 
								// option avec juste le symbole et sélectionnée si besoin
								if(contentUnit==symbole){
									new_content += '<option selected>'+symbole+'</option>';
								} else {
									new_content += '<option>'+symbole+'</option>';
								}
							}
						});
					}
				});
				new_content += '</select>';
			} else {
				new_content += '<unit name="'+name_param+'">'+contentUnit+'</unit>'; //mettre une balise pour CSS (mettre en gras par exemple) à voir pour le nom...
			}
			// Remplace l'ancien contenu par le nouveau contenu dans le content d'une action structurée
			$(this).replaceWith(new_content);	
		}
	});
}

/**
 * Vérifie en fonction de la valeur d'un paramètre, si celui-ci est utilisable ou non (pour l'afficher dans le select d'un tableau itératif)
 * @param {string} valueParam - Le paramètre
 * @param {number} idLD - L'identifiant du labdoc
 * @return {Boolean} - TRUE si utilisable, FALSE si inutilisable
 */
function isMaterialUsable(valueParam, idLD){
	// par défaut paramètre utilisable
	var usable = true; 
	// pour chaque matériel inutilisable trouvé dans le fichier XML client
	$(global_tab_ld[idLD].ld_content).find("materialUnusable").each(function(){ 
		var param_id = $(this).attr("paramId");
		var name_paramUnusable = $(this).parent().children("content").children("parameter[id='"+param_id+"']").attr("name");
		// on vérifie chaque occurence de ce paramètre dans l'arbre
		$(global_tab_ld[idLD].ld_content).find("ul").find("parameter[id='"+param_id+"'][name='"+name_paramUnusable+"']").each(function(){ 
			// Si sa valeur est égale à celle de notre paramètre, alors ce dernier devient inutilisable
			if (valueParam == $(this).html()) usable = false; 
		});
	});
	return usable; // renvoie la valeur booléenne
}

/**
 * Remplace les balises <input>, <select> par des balises <span> et leurs attributs dans le contenu des actions structurées lorsqu'on ferme le mode édition
 * @param {html} content - Contenu HTML à transformer
 * @return - Transforme directement le HTML dans le DOM
 */
function replaceInputIntoSpan(content){
	//pour chaque balise <span> contenu dans la balise de classe 'contentStructured' (cas ou il y 'X' et donc pas d'input mais un span : on traite uniquement type 'quantity')
	$(content).children("span").each(function() {
		var name_span = $(this).attr('name');
		var valeur_span = $(this).html(); 
		var type_param_span = $(this).attr('type'); 
		// Si le span correspond à un paramètre de type "quantity"
		if (type_param_span == "quantity") { 
			var new_content = '<value>'+valeur_span+'</value>';
			// Si choose == true (on récupère l'unité depuis le select)
			if ($(this).next('select[name="'+name_span+'"]').length) { 
				new_content += '<unit choose="true">'+$(this).next('select[name="'+name_span+'"]').val()+'</unit>';
				$(this).next('select[name="'+name_span+'"]').remove();
			} 
			// Sinon choose == false (on récupère l'unité depuis la balise unit)
			else { 
				new_content += '<unit choose="false">'+$(this).next('unit[name="'+name_span+'"]').html()+'</unit>';
				$(this).next('unit[name="'+name_span+'"]').remove();
			}
			$(this).html(new_content);
		}
	});

	//pour chaque balise <input> contenu dans la balise de classe 'contentStructured'
	$(content).children("input").each(function() {
		//On récupère les attribut de l'input 
		var idInput = $(this).attr('id');
		var nameInput = $(this).attr('name');
		var valeurInput = $(this).val();
		var sizeInput = $(this).attr('size'); 
		var type_param_span = $(this).attr('type_param');

		// Si l'input correspond à un paramètre de type "free"
		if (type_param_span == "free") { //remplace balises <input> par un <span> avec les valeurs correspondantes
			$(this).replaceWith('<span id="'+idInput+'" type="free" name="'+nameInput+'" length="'+sizeInput+'">'+valeurInput+'</span>'); 
		} 

		// Si l'input correspond à un paramètre de type "quantity"
		else if (type_param_span == "quantity") { 
			var new_content = '<span id ="'+idInput+'" type="quantity" name="'+nameInput+'" length="'+sizeInput+'" quant_type="'+ $(this).attr('quant_type')+'" ><value>'+valeurInput+'</value>';
			// Si choose == true (on récupère l'unité depuis le select)
			if ($(this).next('select[name="'+nameInput+'"]').length) { 
				new_content += '<unit choose="true">'+$(this).next('select[name="'+nameInput+'"]').val()+'</unit>';
				$(this).next('select[name="'+nameInput+'"]').remove();
			} 
			// Sinon choose == false (on récupère l'unité depuis la balise unit)
			else { 
				new_content += '<unit choose="false">'+$(this).next('unit[name="'+nameInput+'"]').html()+'</unit>';
				$(this).next('unit[name="'+nameInput+'"]').remove();
			}
			new_content += '</span>';
			$(this).replaceWith(new_content);
		}
	});
	
	//pour chaque balise <select> contenu dans la balise de classe 'contentStructured'
	$(content).children("select").each(function() {

		//On gere le cas des select libres
		if ($(this).attr('type') == "freeSelect" ) { 
			//On serialize les valeurs dans le span avec le séparateur "|@|"
			var valueSerialized = "|@|";
			$(this).find('option').each(function(){
				valueSerialized += ($(this).text().replace('|@|','')) + '|@|';
			});
			var content_span = '<span value="' + valueSerialized + '" id ="'+$(this).attr('id')+'" type="freeSelect" name="'+$(this).attr('name')+'"';
			content_span += '>' + $(this).val() + '</span>'
			//remplace balises <select> par un <span> avec les valeurs correspondantes
			$(this).replaceWith(content_span); 
		}
		// Si le select correspond à un type "material" (a un attribut 'type')
		else if ($(this).attr('type') !== undefined ) { 
			var content_span = '<span id ="'+$(this).attr('id')+'" type="material" name="'+$(this).attr('name')+'"';
			if(typeof $(this).attr('mat_type') != 'undefined') content_span += 'mat_type="'+$(this).attr('mat_type')+'"'; ;
			content_span += '>' + $(this).val() + '</span>'
			//remplace balises <select> par un <span> avec les valeurs correspondantes
			$(this).replaceWith(content_span); 
		}
	});
}

/**
 * Remplace les balises <parameter> avec tous ses attributs et son contenu par des balises <span> identiques
 * @param {html} content - Le contenu HTML à transformer
 * @return - Transforme directement le HTML dans le DOM
 */
function replaceParameterIntoSpan(content){
	//pour chaque balise <parameter> contenu dans la balise de classe 'contentStructured'
	$(content).find("parameter").each(function() {
		// Récupération des attributs de la balise <parameter>
		var name_param = $(this).attr('name');
		var id_param = $(this).attr('id'); 
		var type_param = $(this).attr('type');
		var content_param = $(this).html();
		// si c'est un paramète libre
		if(type_param == "free") { 
			// on récupère l'attribut longueur de la balise <parameter>
			var length_param = $(this).attr('length'); 
			//remplace balises <parameter> par un span avec les valeurs correspondantes
			$(this).replaceWith('<span id ="'+id_param+'" type="'+type_param+'" name="'+name_param+'" length="'+length_param+'">'+content_param+'</span>'); 
		} 
		// Nouveau by N.T. - Les liste déroulante libres
		else if(type_param == "freeSelect") { 
			var content_select = $(this).text();
			var value_serialized = $(this).attr('value');

			// On remplace la balise <parameter> par un span avec les valeurs correspondantes
			if(content_select != ""){
				$(this).replaceWith('<span value="'+ value_serialized +'" id ="'+id_param+'" type="'+type_param+'" name="'+name_param+'">'+content_select+'</span>'); 
			}
		} 
		// si c'est un paramète avec une quantité
		else if (type_param == "quantity") { 
			// on récupère l'attribut longueur de la balise <parameter>
			var length_param = $(this).attr('length'); 
			// on récupère l'attribut du type de quantité de la balise <parameter>
			var quanttype_param = $(this).attr('quant_type'); 
			//remplace balises <parameter> par un span avec les valeurs correspondantes
			$(this).replaceWith('<span id ="'+id_param+'" type="'+type_param+'" name="'+name_param+'" quant_type="'+quanttype_param+'" length="'+length_param+'">'+content_param+'</span>'); 
		}
		// si c'est un paramète avec un matériel 
		else if (type_param == "material") { 
			// on récupère l'attribut du type de matériel de la balise <parameter>
			var mat_type_param = $(this).attr('mat_type'); 
			//remplace balises <parameter> par un span avec les valeurs correspondantes
			$(this).replaceWith('<span id ="'+id_param+'" type="'+type_param+'" name="'+name_param+'" mat_type="'+mat_type_param+'">'+content_param+'</span>'); 
		}
	});
}
