// Tableau des langages pour la partie JS
var loc = 0 ; // 0 : français, 1 : anglais
var lang = {
	"uniqueEd":[
		"Un élément de votre mode opératoire est en cours d'édition - merci de le valider ou de l'annuler",
		"An element of your protocol is being edited - please, validate or cancel"],
	"maximum":[
	   "maximum",
	   "maximum"],
	"badMaterialName":[
	   "Le nouveau nom de votre matériel ne convient pas",
	   "The new name of your material is not adapted"],
	"tooManyMaterial":[
	   "Vous avez sélectionné plus de matériel que vous n'en disposez",
	   "You have selected more material than available"],
	"cmAddActionIn":[
	   "Ajouter une action (dans)",
	   "Add an action (in)"],
	"cmAddActionAfter":[
	   "Ajouter une action (après)",
	   "Add an action (after)"],
	"cmAddStepAfter":[
	   "Ajouter une étape (après)",
	   "Add a step (after)"],
	"cmAddStepIn":[
	   "Ajouter une étape (dans)",
	   "Add a step (in)"],
	"cmFreeAction":[
	   "Action libre",
	   "Free action"],
	"cmEdit":[
	   "Modifier",
	   "Edit"],
	"cmChange":[
		"Transformer en étape",
		"Transforming into step"],
	"cmChangeToAction":[
		"Transformer en action",
		"Transforming into action"],
	"cmSuppress":[
	   "Supprimer",
	   "Delete"],
	"cmCopy":[
	   "Copier",
	   "Copy"],
	"cmPaste":[
	   "Coller",
	   "Paste"],
	"comment":[
		"Commentaire :",
		"Comment:"],
}