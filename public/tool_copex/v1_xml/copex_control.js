
//***************************************************************************
/**
 * Get a html text and clean it : remove html tags and replace special chars & and < for xml compatibility
 * the line breaks are replaced by something specified (<br/> or - or whatever)
 * @param {string} html_string
 * @param {string} br : tag or content that replaces the line breaks
 * @return {string}
 */
function cleanHTMLInputs (html_string, br) {
	// keep the line breaks
	var cleaned_str = html_string.replace(/(?:\r\n|\r|\n)/g, 's#d#l') // Word line breaks
		.replace(/<(div|p)>/gi, 's#d#l').replace(/<\/(div|p)>/gi, '') // <div> or <p> tags (for Chrome)
		.replace(/<br\s*\/?>/gi, 's#d#l'); // <br> tags
	// If there are other html tags to keep : do it here...
	
	// clean the HTML
	cleaned_str = jQuery('<span>' + cleaned_str + '</span>').text(); // clean all the html tags
	cleaned_str = cleaned_str.replace(/&nbsp;/g, " ") ;
	cleaned_str = cleaned_str.trim(); // remove extra blank spaces 
	cleaned_str = cleaned_str.replace(/&/g, '&amp;').replace(/</g, '&lt;') ; // replace the problematic < and &
	
	// Management of line breaks
	cleaned_str = cleaned_str.replace(/(\s*s#d#l\s*)+/g, 's#d#l'); // avoid the insert of multi line breaks
	if (cleaned_str.substr(-5) == 's#d#l') { // Remove the last br
		cleaned_str = cleaned_str.substr(0, cleaned_str.length - 5); 
	}
	if (cleaned_str.substr(0,5) == 's#d#l') { // Remove the first br
		cleaned_str = cleaned_str.substr(5); 
	}
	cleaned_str = cleaned_str.replace(/s#d#l/g, br) ; // Replace the line breaks by the chosen text
	// If there are other html tags to keep : do it here...

	return cleaned_str;
}


/*
 * Parse a text : if an error is detected, stop the process et display again the current LD
 * @param {string} string - text to parse
 * @param {int} id_ld - labdoc
 * @returns {XML-doc}
 */
function procXMLParse(string, id_ld) {
	var xml_doc ;
	try { // uses the native parsing function of the browser to create a valid XML Document can be traversed and manipulated
		xml_doc = $.parseXML(string) ;
	}
	catch(e) { 
		console.log("Error in procXMLParse", string) ;
		alert( __("Une erreur XML a été détectée dans le contenu de votre protocole. L'enregistrement est impossible.\nLa dernière action est annulée.") );
		displayLDContent(id_ld, 1);
		return false ;
	}
	return xml_doc;
}

// ******************************************************
//        Fonction de fermeture des éléments ouverts
// ******************************************************
/**
 * valide si possible l'élément ouvert et return true
 * si l'élément ne peut pas être fermé, envoie un msg d'erreur et return false
 */

function checkProcLDLocker(id_ld) {
	if(typeof global_tab_ld[id_ld] != "undefined" && typeof global_tab_ld[id_ld].ld_locker != "undefined"){
		if(global_tab_ld[id_ld].ld_locker == 'proc') {//proc : Un élément du mode op est en edition => on affiche l'alerte
			alert( __("Un élément de votre mode opératoire est en cours d'édition - merci de le valider ou de l'annuler") );
			return false;
		} 
		else if(global_tab_ld[id_ld].ld_locker == 'ml'){ //ml : la liste de matériel en modification => validation de la ML
			closeML(id_ld, 'validate') ;
			global_tab_ld[id_ld].ld_locker = 0 ;
		}
		else if(global_tab_ld[id_ld].ld_locker != 0){ //id du type QHP_idld_idqhp : Une QHP en modification => validation de la QHP
			var QHP_div_blocked = $('#'+global_tab_ld[id_ld].ld_locker);
			if(QHP_div_blocked.length > 0){
				//On recupère les attribut nécéssaire a la fermeture et validation de la QHP ouverte
				var QHP_div_blocked_content = QHP_div_blocked.children('.QHPMEdited');
				var QHP_blocked_type = QHP_div_blocked.data('type');
				//On ferme et enregistre la QHP ouverte en question 
				if(QHP_div_blocked_content.length > 0){
					closeQHP(id_ld, QHP_div_blocked_content, QHP_blocked_type);
					global_tab_ld[id_ld].ld_locker = 0;	
				}
			}
		}
	}
	return true ;
}


// ******************************************************
//              Parties QHP
// ******************************************************

/**
 * Ouvre en édition les parties QHP
 * @param {number} idLD - Identifiant du labdoc
 * @param {string} QHP_type - Type du QHP
 * @param {string} QHP_id - ID de le DIV portant la QHP
 */
function editQHP(idLD,QHP_type,QHP_id){
	if (checkProcLDLocker(idLD)) {
		global_tab_ld[idLD].ld_locker = QHP_id; // locker
		var QHP_div_content = $("#content_"+QHP_type+"_"+idLD);//On retrouve l'ID du QHPM en fonction du type et de l'ID labdoc
		QHP_div_content.find(".QHPMEditable").unbind() ; // empêche la fonction d'edition d'être appelée à chaque fois

		// affichage du cadre d'édition
		QHP_div_content.toggleClass("QHPMContent QHPMEdited");
		// affichage des différentes parties des QHP en mode édition
		QHP_div_content.children(".defaultQHPContent").show();
		QHP_div_content.children(".QHPContent").addClass("txtEdited");
		QHP_div_content.children(".QHPContent").attr("contentEditable", "true");
		QHP_div_content.children(".QHPContent").focus() ;
		QHP_div_content.children(".commentLabel").show();
		QHP_div_content.children(".comment").addClass("commentEdited");
		QHP_div_content.children(".comment").attr("contentEditable", "true");
		QHP_div_content.children(".divButtons").show();
	
		// définition des fonctions des boutons de fermeture du mode édition
		QHP_div_content.find(".QHPValidate").click(function(){
			closeQHP(idLD, QHP_div_content, QHP_type);
			global_tab_ld[idLD].ld_locker = 0;
		});
		QHP_div_content.children().children(".QHPCancel").click(function(){
			displayQHP(idLD, 1);
			global_tab_ld[idLD].ld_locker = 0;
		});
	}
}


/**
 * Ferme les parties QHP
 */
function closeQHP(idLD, QHP_div_content, QHP_type){
	// MaJ des variables dans le XML
	var QHP = $(global_tab_ld[idLD].ld_content).find('QHP[type="'+QHP_type+'"]');
	var xml_new = procXMLParse("<content>" + cleanHTMLInputs(QHP_div_content.find(".txtEdited").html(),'<br/>') + "</content>", idLD) ;
	if (xml_new) {
		$(QHP).find("content").replaceWith($(xml_new).find("content")); // il faut créer un fragment par find 
	} else {
		return;
	}
	if(QHP_div_content.children(".commentEdited").length > 0){ //Si le commentaire existe (optionnel)
		xml_new = procXMLParse("<comment>" + cleanHTMLInputs(QHP_div_content.find(".commentEdited").html(),'<br/>') + "</comment>", idLD);
		if (xml_new) {
			$(QHP).find("comment").replaceWith($(xml_new).find("comment"));
		} else {
			return;
		}
	}
	
	saveLocalLDVersion(idLD) ; // Sauvegarder la version
	displayQHP(idLD, 1); // afficher
}

// ******************************************************
//              Partie Material List
// ******************************************************

/**
 * Ouvre en édition la partie ML
 * @param {number} idLD - Identifiant du labdoc
 */
function editML(idLD){
	if (checkProcLDLocker(idLD)) {
		//ml - Code qui défini que ce n'est pas un ID de QHP mais qu'i faut bloquer l'edition
		global_tab_ld[idLD].ld_locker = 'ml';
		var MD_div= $("#contentML_"+idLD);
		
		// empêche la fonction d'edition d'être appelée à chaque fois
		MD_div.parent().attr("onclick", "");
		MD_div.parent().css('cursor', 'default');
		
		MD_div.toggleClass("QHPMContent QHPMEdited"); //MD_div.className = 'QHPMEdited';
		
		// affiche le cadre de modification de la liste éditable du matériel
		MD_div.children(".display_ML").hide();
		MD_div.children(".editML").show();
	}
	
	// Traitement des boutons d'incrémentation et décrémentation pour la quantité du matériel sélectionné
	//bouton "+"
	$("#buttonPlusMat_"+idLD).on("click", function() {
		var old_value = $("#selectedMat_"+idLD).val(); //on récupère la valeur du span contenant le nombre actuel de matériel
		var nb_mat = parseInt(old_value) ;
		if(old_value < global_tab_ld[idLD].ML[$('#idMaterial_'+idLD).val()].quantity){ //si l'ancienne valeur est inférieur à la quantité maximum alors on affiche la sélection des paramètres et le tableau
			nb_mat += 1; // on ajoute 1
		} else if(isNaN(old_value)){ // si la valeur n'est pas un nombre (incorrect)
			nb_mat = 1; // on réinitialise la valeur à 1
		} else {
			alert( __("Vous ne pouvez sélectionner plus de matériel que vous n'en disposez") );
		}
		$("#selectedMat_"+idLD).val(nb_mat);
	});
	//bouton "-"
	$("#buttonMinusMat_"+idLD).on("click", function() {
		var old_value = $("#selectedMat_"+idLD).val();  //on récupère la valeur du span contenant le nombre actuel de matériel
		var nb_mat = parseInt(old_value);
		if (old_value != 0){ 
			if(isNaN(old_value)){ nb_mat = 1; }// on réinitialise la valeur à 1
		 	else { nb_mat -= 1; }
		}
		$("#selectedMat_"+idLD).val(nb_mat);
	});
}

/**
 * Quand l'utilisateur entre dans le cadre de recherche / ajout de matériel
 * @param {number} idLD - Identifiant du labdoc
 */
function enterSearchAddMaterial(idLD) { 
	//cache la liste du matériel précédent & son surlignage
	$("#displayMaterial_"+idLD).hide();
	$("#selectableMaterialList_"+idLD+" > span.btn_pointer").removeClass( "selectedMaterialSpan" );
}

/**
 * Quand l'utilisateur fait une recherche de matériel
 * @param {number} idLD - Identifiant du labdoc
 */
function keyUpSearchAddMaterial(idLD) {
	// empêche d'entrer des caractères problématiques tels < et &
	var search_text = document.getElementById('searchAddMaterial_'+idLD).value ;
	var car = search_text.substring(search_text.length-1,search_text.length) ; //le dernier caractère rentré
	if (car=='<' || car=='>' || car=='&' ||car=='"') {
		search_text = search_text.substring(0,search_text.length-1) ;
		document.getElementById('searchAddMaterial_'+idLD).value = search_text ;
	}
	var ML = displaySelectableMaterialList(idLD, search_text) ;
	document.getElementById('selectableMaterialList_'+idLD).innerHTML = ML ;
	if (ML === '' && $(global_tab_ld[idLD].ld_content).find("materialList").attr("add")==='true') { // si on n'a trouvé aucun matériel selon la clé entrée
		document.getElementById('addMaterial_'+idLD).style.display = 'block' ;
		document.getElementById('newMaterial_'+idLD).innerHTML = document.getElementById('searchAddMaterial_'+idLD).value ;
	}
	else {
		document.getElementById('addMaterial_'+idLD).style.display = 'none' ;
	}
}

/**
 * Quand l'utilisateur efface le cadre de recherche
 * @param {number} idLD - Identifiant du labdoc
 */
function deleteSearchAddMaterial(idLD) { 
	$('#searchAddMaterial_'+idLD).val('') ;
	$('#selectableMaterialList_'+idLD).html(displaySelectableMaterialList(idLD, null)) ;
	$("#displayMaterial_"+idLD).hide();
	$('#addMaterial_'+idLD).hide() ;
}

/**
 * Quand l'utilisateur clique sur une case de sélection d'un matériel on change son status de sélect
 * @param {number} idLD - Identifiant du labdoc
 * @param {number} idMat - Identifiant du materiel
 */
function selectMaterial(idLD, idMat) { 
	if (global_tab_ld[idLD].ML[idMat].selected === 0) global_tab_ld[idLD].ML[idMat].selected = 1 ;
	else global_tab_ld[idLD].ML[idMat].selected = 0 ;
	// displayMaterial(idLD, idMat) ; // affiche le matériel modifié
}

/**
 * Quand l'utilisateur ajoute un materiel
 * @param {number} idLD - Identifiant du labdoc
 */
function addMaterial(idLD) { 
	// d'abord on vérifie si il n'y a pas un matériel à sauver
	if (!saveMaterial(idLD)) return ; 
	// crée l'objet 
	var types = new Array("none"); 
	
	// déterminer le nouvel id
	// 9999 le plus grand id des matériels de la liste générale
	var idMax =9999 ; 
	for (var id in global_tab_ld[idLD].ML) {
		if (global_tab_ld[idLD].ML[id] && (parseInt(global_tab_ld[idLD].ML[id].id) > idMax)) {
			idMax = parseInt(global_tab_ld[idLD].ML[id].id) ; 
		}
	}
	var obj = new Material('user',idMax+1,100000,document.getElementById('searchAddMaterial_'+idLD).value,'','',types,null,1,0) ; // création de l'objet
	// ajout de l'objet au tableau ML
	global_tab_ld[idLD].ML[idMax+1] = obj ; 
	// remet à jour le cadre de recherche et la liste
	deleteSearchAddMaterial(idLD) ; 
	// affiche le nouveau matériel
	displayMaterial(idLD, idMax+1) ; 
}

/**
 * Appelée depuis le bouton "valider" ou lors du changement d'affichage d'un matériel
 * @param {number} idLD 
 */
function saveMaterial(idLD) {
	// si il n'y a pas d'objet affiché, on ne fait rien et ça va bien
	if (document.getElementById('idMaterial_'+idLD).value == '') {
		return true ;
	} else { var idMat = document.getElementById('idMaterial_'+idLD).value ; }
	// le nom
	if ($('#nameMat_'+idLD).text()=='') {
		alert( __("Le nouveau nom de votre matériel ne convient pas") ); 
		return false ; 
	}
	else global_tab_ld[idLD].ML[idMat].name = cleanHTMLInputs($('#nameMat_'+idLD).html(),' - ') ;
	// le nombre de matériels sélectionnés
	if (document.getElementById('selectedMat_'+idLD)) {
		if (parseInt($('#selectedMat_'+idLD).val()) > global_tab_ld[idLD].ML[idMat].quantity) {
			alert( __("Vous avez sélectionné plus de matériel que vous n'en disposez") ); 
			$("#selectedMat_"+idLD).val(global_tab_ld[idLD].ML[idMat].quantity);
			return false ; 
		}
		else global_tab_ld[idLD].ML[idMat].selected = parseInt($('#selectedMat_'+idLD).val()) ;
	}
	global_tab_ld[idLD].ML[idMat].description = cleanHTMLInputs($('#descriptionMat_'+idLD).html(),' - ') ;
	global_tab_ld[idLD].ML[idMat].comment = cleanHTMLInputs($('#commentMat_'+idLD).html(),' - ') ;
	return true ;
}

/**
 * supression d'un matériel
 * @param {number} idLD - Identifiant du labdoc
 * @param {number} idMat - Identifiant du materiel
 */
function deleteMaterial(idLD,idMat) {
	global_tab_ld[idLD].ML[idMat] = null ; 
	// met à jour la liste des matériels
	document.getElementById('selectableMaterialList_'+idLD).innerHTML = displaySelectableMaterialList(idLD, null) ;
	// supprime l'id caché du cadre de matériel
	document.getElementById('idMaterial_'+idLD).value = '' ; 
	// cache le cadre de matériel
	document.getElementById('displayMaterial_'+idLD).style.display = 'none'; 
}

/**
 * Validation ou annulation des modifs sur la liste du materiel
 * @param {number} idLD - Identifiant du labdoc
 * @param {*} option
 */
function closeML(idLD, option){
	if(option==="validate") {
		if (!saveMaterial(idLD)) return ; // puis on sauvegarde le dernier matériel affiché
		
		// MaJ de la Material List
		// obtention du nouveau contenu [format texte] à partir de la methode ld_content des objets Materiel
		var XML_text = '<materialList select="'+$(global_tab_ld[idLD].ld_content).find("materialList").attr("select")+'" add="'+$(global_tab_ld[idLD].ld_content).find("materialList").attr("add")+'" copexGeneralMaterial="'+$(global_tab_ld[idLD].ld_content).find("materialList").attr("copexGeneralMaterial")+'">\n' ;
		XML_text += '<label>'+$(global_tab_ld[idLD].ld_content).find("materialList").find("label").text()+'</label>\n';
		for (var id in global_tab_ld[idLD].ML) {
			if ( global_tab_ld[idLD].ML[id] && (typeof global_tab_ld[idLD].ML[id] == "object") ){ // on teste l'existence du matériel (a-t-il été supprimé ??) - le typeof est un espèce de hack pour la page auteur : le fait de passer par le DOM pour parser le XML interfère avec un prototype de la page auteur. Bizarre !
                XML_text += global_tab_ld[idLD].ML[id].getXML();
            } 
		}
		XML_text += '</materialList>' ;

		// MaJ du ld_content du LD
		var new_XML = procXMLParse(XML_text, idLD) ;
		if (new_XML) {
			global_tab_ld[idLD].ld_content.documentElement.replaceChild(new_XML.documentElement, global_tab_ld[idLD].ld_content.getElementsByTagName("materialList")[0]) ;
			saveLocalLDVersion(idLD) ; // Sauvegarder dans la table JS
		} 
	}
	global_tab_ld[idLD].ML =  getMaterialListfromXML(idLD) ; // on remet à jour le tableau des matériels à partir du XMLDoc pour s'assurer de la cohérence
	global_tab_ld[idLD].ld_locker = 0;
	displayML(idLD, 1);
}

// ******************************************************
//              JSTree
// ******************************************************

/**
 * Sauvegarde la procédure
 * @param {number} idLD - Identifiant du labdoc
 */
function saveProcedure(idLD) { 
	// Normalize the DOM of the HTML procedure
	var jsTreeDom = $('#procedure_' + idLD + ' > ul').first().clone();
	jsTreeDom.find('ins, br').remove();
	jsTreeDom.find('span.commentLabelEdit').replaceWith(function () {
		return $('<commentLabel />').html($(this).html());
	});
	jsTreeDom.find('span').replaceWith(function () {
		var source = this;
		var copy = document.createElement('parameter');
		if (source.hasAttributes) {
			for (var i = 0; i < source.attributes.length; i++) {
				if (source.attributes[i].name !== "class" && source.attributes[i].name !== "style" && source.attributes[i].name !== "xmlns") {
					copy.setAttribute(source.attributes[i].name, source.attributes[i].value);
				}
			}
		}
		if (source.childNodes && source.childNodes.length > 0) {
			// move the sub-nodes under this new node (to avoid moving, see cloneNode())
			for (var i = 0; i < source.childNodes.length; i++) {
				copy.appendChild(source.childNodes[i].cloneNode(true));
			}
		}
		return copy;
	});
	jsTreeDom.find('[class]').removeAttr('class');
	jsTreeDom.find('[style]').removeAttr('style');

	// convert HTML into XML, or undo if it fails
	var new_XML = procXMLParse(XMLToString(jsTreeDom.wrap('<root />')), idLD);

	if (new_XML) {
		// MaJ du contenu XML du LD
		var old_ul = global_tab_ld[idLD].ld_content.querySelector("procedure > ul");
		var new_ul = new_XML.querySelector("ul");
		new_ul.removeAttribute('xmlns'); // ne fonctionne pas !
		old_ul.parentNode.replaceChild(new_ul, old_ul);
		saveLocalLDVersion(idLD);
    }
}

/**
 * Gestion de l'affichage du Menu Racine du labdoc Procedure JSTree
 * @param {number} idLD - Identifiant du labdoc
 */
function openJSTreeRootCM(idLD) {
	$('#JSTreeRootCM_'+idLD).show() ;
	// fonction de fermeture
	$('#labdoc_content_'+idLD).on("click",function() {
		$('#JSTreeRootCM_'+idLD).hide() ;
		$('#labdoc_content_'+idLD).off("click") ; // on supprime la fonction de fermeture
	}) ;
}

/**
 * Fonctions de modifications de l'arbre
 * @param {number} idLD - Identifiant du labdoc
 */
function pasteInRoot(idLD) {
	var div_procedure = document.getElementById('labdoc_content_'+idLD).lastChild ; // la div dans laquelle on met l'arbre
	$(div_procedure).jstree("paste",-1, true) ;
	saveProcedure(idLD) ;
}

/**
 * ajoute un noeud (step ou action) dans jstree
 * @param {number} idLD - Identifiant du labdoc
 * @param {string} task - La tâche : step / action /step_iter ...
 * @param {number} idA - Identifiant de l'action
 * @param {HTMLElement} targetNode - La node qu'on crée
 * @param {string} position - la position : Valeurs possibles : Entier ou "before", "after", "inside", "first", "last".
 */
function addNode(idLD, task, idA, targetNode, position) { 
	//Position parametre optionnel défini à "last" par défaut
	if(typeof position == 'undefined'){
		position = "last";
	}
	if (checkProcLDLocker(idLD)) {
		// la div dans laquelle on met l'arbre
		var div_procedure = document.getElementById('labdoc_content_'+idLD).lastChild ; 
		if (task==="step") {
			var node = $(div_procedure).jstree("create",targetNode,position,{attr: {'rel': 'step', 'edit': 'true', 'suppr': 'true', 'copy': 'true', 'move': 'true', 'modify': 'true', 'iteration':'1', 'parameters':'1'},data: ' '},false,true);
		}
		else if (task==="action") {
			var structured = global_tab_ld[idLD].Actions[idA].structured ;
			if (structured != 'none') var html = '<name>'+global_tab_ld[idLD].Actions[idA].name+'</name>' ;
			else var html = '' ;
			html += '<content>'+global_tab_ld[idLD].Actions[idA].content+'</content>' ;
			//Gestion du commentLabel
			if(typeof global_tab_ld[idLD].Actions[idA].commentLabel != 'undefined' && global_tab_ld[idLD].Actions[idA].commentLabel != null){
				html += '<commentLabel>'+global_tab_ld[idLD].Actions[idA].commentLabel+'</commentLabel>' ;
			}
			if(typeof global_tab_ld[idLD].Actions[idA].comment != 'undefined'){
				html += '<comment>'+global_tab_ld[idLD].Actions[idA].comment+'</comment>' ;
			}
			
			var node = $(div_procedure).jstree("create",targetNode,position,{attr: {'rel': 'action', 'edit': 'true', 'suppr': 'true', 'copy': 'true', 'move': 'true', 'structured': structured},data: html},false,true) ;
		}
		// supprimer un attribut href qui vient se mettre ici, on ne sait pourquoi...
		node.children("a:eq(0)").removeAttr("href") ;
		editNode(node, idLD, true) ;
	}
}

/**
 * éditer un node de JSTree
 * @param {*} node - Le node que l'on veut editer
 * @param {number} idLD - Identifiant du labdoc
 * @param {*} creation - booléen (True si c'est une création de node) 
 */
function editNode(node, idLD, creation) {
	// variable booléenne qui détermine si le noeud contient au moins une action structurée avec un paramètre
	var action_structured = false; 
	if ($(node).attr('rel') == 'step' || $(node).attr('rel') == 'step_iter') {
		$(node).children('ul').find('li').each(function(){
			var rel = $(this).attr('rel');
			var structured = $(this).attr('structured');
			if (rel == 'action' && structured == 'content')
				action_structured = true; 
		});
	}
	//S'il y a un blocage enregistré, c'est qu'un élément est en cours d'édition, affichage du message d'erreur
	if (checkProcLDLocker(idLD)) { 
		//On récupère la procédure (dernier élément du labdoc)
		var div_procedure = document.getElementById('labdoc_content_'+idLD).lastChild ;
		// tricks pour permettre de rentrer dans les champs
		node.addClass("inEdition") ; 
		
		//On met le locker à proc pour empecher l'edition multiple
		global_tab_ld[idLD].ld_locker = 'proc';
		//Désactiver le context-menu quand le noeud est ouvert
		node.children("a:eq(0)").addClass("jstree-contextmenu-disabled") ;
		
		// Recuperation des paramètres de la tâche
		var action = node.attr("rel") === "action" ;
		var structure = node.attr("structured") ;
		var iteration = node.attr("iteration") !== undefined ? node.attr("iteration") : 1;
		var parameters = node.attr("parameters") !== undefined ? node.attr("parameters") : 1;
		
		// Recuperation des champs de la tâche
		if (action && structure !== "none") {
            var name = node.children("a:eq(0)").children("name:eq(0)").html() ;
        }
		// On transforme les actions structurés pour l'édition
		if (structure == 'content') { // Avant de récupérer le contenu, si c'est un contenu structuré, on remplace les balises <param>
            replaceSpanForEdition(node.children("a:eq(0)").children("content:eq(0)"), idLD);
        }
		var iContent = node.children("a:eq(0)").children("content:eq(0)").html() ;
		var iComment = node.children("a:eq(0)").children("comment:eq(0)").html() ;
		var iTable = node.children("a:eq(0)").children("table:eq(0)").html();
		if(node.attr("rel") === "step_iter"){
			var iContentActionAfterIteration = $(node).children('ul').html();
		}

		//Le libelle du commentaire s'il est personnalisé
		var comment_Label = __("Commentaire") + __("&nbsp;:");
		var node_comment_Label = node.children("a:eq(0)").children(".commentLabelEdit:eq(0)") ;
		if (node_comment_Label.length > 0) {
			comment_Label = node_comment_Label.text();
		}
		//Si c'est pour une action structuré
		else if(node.children("a:eq(0)").children("commentLabel:eq(0)").length > 0) {
			comment_Label = node.children("a:eq(0)").children("commentLabel:eq(0)").text();
		}

		// Recuperations des images de la tâche
		var iImages = "";
		var image_indice = 0;
		node.find("a:eq(0) > img").each(function(){
			//Timestamp + indice pour être sur que ce soit différent
			var timestamp = new Date().getTime(); 
			var img_id = "img_" + idLD + "_" + timestamp + "_" + image_indice;
			//On re-crée la balise contenant la balise Image
			iImages +=  "<span id=\"" + img_id + "\" class=\"cx_bloc_uploaded\"><img class=\"cx_image_uploaded\" src=\"" + $(this).attr('src') + "\">";
			//On ajoute la possibilité de supprimer l'image en question
			iImages += '<i class="fas fa-trash-alt" title="' + __('Supprimer l\'image') + '" onclick="$(\'#' + img_id + '\').remove();"></i></span>';
			image_indice++;
		});
		
		// construction du formulaire html (uniquement pour les étapes et les actions libres)
		html = '<div id="editedNode_'+idLD+'" class="nodeEdited">';
		if (name) html += '<span class="structured_action_name">'+name+'</span>' ;
		var value_content = "";
		if (iContent) { value_content = iContent ;}
		if (structure == 'content') {
			html += ' : <span class="contentStructured">';
			if (iContent) {
				html+= iContent ;
			}
			html += '</span>';
		} else { 
			html += '<div class="txtEdited" contenteditable="true">' + value_content + '</div>' ;
		}

		var value_comment = "";
		//Si on a un commentaire ou que l'on est sur une création d'action avec commentaire ou une création d'étape
		if(((node.children("a:eq(0)").children("comment:eq(0)").length > 0) || (creation && !action)) && !(creation && action && iComment == "null")){
			//Si on est sur une création d'action structuré on vérifie que l'on met un commentaire
			if (iComment) { value_comment = iComment;}
			html += '<div class="commentLabel" style="display: block;">' + comment_Label + '</div><div class="commentEdited" contenteditable="true" >' + value_comment + '</div>' ;
		}
		
		//On ajoute un formulaire independant qui va permettre d'upload une image en Ajax dans XML
		//Si le champ d'upload de fichier n'existe pas on va le créer
		var Labdoc_id = "#labdoc_" + idLD;
		//Cas enseignant
		if($(Labdoc_id).length <= 0){
			var Labdoc_id = "#ld_edit";
		}
		if(!$(Labdoc_id + " .formUpload").length > 0){
			var form_upload = '<form class="formUpload" method="post" enctype="multipart/form-data" action="process_form.php">';
			form_upload += '<input style="display:none" class="fileUpload" type="file" name="image" accept="image/*">';
			form_upload += '</form>';

			//On ajoute le formulaire à la fin du labdoc
			$(Labdoc_id).append(form_upload);

			//On abonne le champ du formulaire qu'on vien d'ajouter pour lancer l'upload des que le fichier est choisi
			$(Labdoc_id + " .fileUpload").change(function(){
				//Il faut soumettre le formulaire a la fin du choix de l'image
				$(Labdoc_id + ' .formUpload').submit();
			});

			//On abonne le formulaire pour récupérer le code ajax sur la soumission du formulaire
			$(Labdoc_id + " .formUpload").on('submit',function(e){
				//On empeche le naviguateur de soumettre le formulaire
				e.preventDefault();

				const fileInput = $(e.target).find('input[type="file"]')[0];
				uploadImage(fileInput).resizeImageToEncoded()
					.then(function(encodedImg) {
						// Ajoute l'image dans le div des images
						const timestamp = new Date().getTime();
						const imgId = "img_" + idLD + "_" + timestamp;
						var imageHtml = '<span id="' + imgId + '"  class="cx_bloc_uploaded">' +
							'<img class="cx_image_uploaded" src="' + encodedImg + '" />' +
							'<i class="fas fa-trash-alt" title="' + __('Supprimer l\'image') + '" onclick="$(\'#' + imgId + '\').remove();"></i>' +
							'</span>';
						$('#editedNode_' + idLD + ' .cx_action_img').append(imageHtml);
					}).catch(function(errorMessage) {
						if (errorMessage) {
							console.error(errorMessage);
						}
					});
			});
		}

		if (action==false){ // Si c'est une étape : affichage de la partie concernant les itérations
			html += '<div class="stepIteration">' +
			 			'<br><span>' + __('Réaliser cette étape') + '</span>' +
						'<input type="text" disabled name="iteration" id="nb_iter" class="cx_int_field" value="'+iteration+'">' +
						'<div class="cx_int_increment">' +
							'<i class="fa fa-caret-up cx_int_plus" id="buttonPlusIter"></i>' +
							'<i class="fa fa-caret-down cx_int_minus" id="buttonMinusIter"></i>' +
						'</div>' +
						'<span class="txtStepAfterInput">' + __('fois') + '</span>' +
						'<div class="stepParameters">' +
							'<span> ' + __('en modifiant') + '</span>' +
							'<input type="text" disabled name="parameters" id="nb_param" class="cx_int_field" value="'+parameters+'">' +
							'<div class="cx_int_increment">' +
								'<i class="fa fa-caret-up cx_int_plus" id="buttonPlusParam"></i>' +
								'<i class="fa fa-caret-down cx_int_minus" id="buttonMinusParam"></i>' +
							'</div>' +
							'<span class="txtStepAfterInput">' + __('paramètre(s)') + '</span>' +
						'</div>' +
					'</div>' +
				'<table id="tableIter">';
			if (iTable) { // si un tableau est déjà présent on le récupère et on l'affiche
				html += iTable;
			} else if (parameters == 0){ // si le nombre de paramètres est égale à 0 on affiche juste l'entête du tableau
				var countIter = 1;
				html += '<tr><th></th>';
				while (countIter <= iteration) { 
					html += '<th>'+countIter+'</th>';
					countIter++;
				}
				html += '</tr>';
			} else if (iteration == 1 && parameters > 1) { // si il n'y a pas d'itérations mais que le nombre de paramètres est supérieur à 1, on affiche le bon nombre de ligne
				html += '<tr><th></th><th>'+1+'</th></tr>';
				var countParam = 1;
				while (countParam <= parameters) {
						html += '<tr><td contenteditable="true" placeholder="' + __("Paramètre") + ' ' + countParam + '">' + __("Paramètre") + ' ' + countParam+'</td><td contenteditable="true"></td></tr>';
					countParam++;
				}
			} else { //sinon on affiche le tableau par défaut
					html += '<tr><th></th><th>1</th><th>2</th></tr><tr><td contenteditable="true" placeholder="' + __("Paramètre") + ' ' + '1">'+__('Paramètre')+ ' ' + '1</td><td contenteditable="true"></td><td contenteditable="true"></td></tr>';
			}
			html += '</table>';
		}

		var Labdoc_id = "#labdoc_" + idLD;
		//Cas enseignant
		if($(Labdoc_id).length <= 0){
			var Labdoc_id = "#ld_edit";
		}

		//On ajoute le bouton  déclenchant l'action du formulaire => ainsi l'upload de l'image => l'ajout du code de l'image dans le XML
		if (action==true){
			html += '<div class="cx_action_img"><span title="' + __('Ajouter une image') + '" class="cx_button_upload_image" onclick="$(\'' + Labdoc_id + ' .fileUpload\').click();"></span>';
			//On ajoute les images déjà présentes avant les boutons de validations
			if(iImages != '' && action==true){ html += iImages; }
			html += '</div>' ; 
		}

		// Pour l'affichage des boutons
		html += '<div class="divButtons" style="display: block;"><span id="validateTask_' + idLD + '" class="cx_button">' + __('Valider') + '</span><span id="cancelTask_'+idLD+'" class="cx_button">' + __('Annuler') + '</span></div></div>' ;
		//node.children("a:eq(0)").html(html) ;
		$(div_procedure).jstree("rename_node", node, html) ;
		$(".txtEdited").focus() ;
		/*
		Ce code est supprimé car il pose problème sur le choix du paramètre sélectionné dans les actions paramétrées
		if (action_structured) { // si l'action contient des actions structurées (affichage de select)
			var numTd = 1; // ligne du tableau
			$("#tableIter tr td:first-child").each(function() { // pour chaque première cellule de chaque ligne
				var isNotPlaceholder = true;
				var contentFistTd = $(this).html();
				if(contentFistTd == $(this).attr('placeholder')) isNotPlaceholder = false;
				var selectHtml = selectIntoTableForParameter(node, contentFistTd, numTd, isNotPlaceholder); // appel fonction qui construit le select (renvoie code html)
				$(this).html(selectHtml); //Ajout du select à la cellule
				numTd++;
			});	
		}*/
		
		$(document).ready(function(){ // quand le traitement de l'affichage par défaut en édition est terminé, on traite les cas particuliers
			if (iteration > 1) {
				$(".stepParameters").css("display", "inline"); // affichage des paramètres si on est dans le cas d'une étape itérative
				if (parameters > 0) $("#tableIter").show(); // affichage du tableau si c'est une étape itérative et qu'il y au moins 1 paramètre
			}
			// permettre la modification des cellules du tableau en édition
			node.children("a").children("div").children("table").children("tbody").children("tr").children("td").attr("contenteditable", "true");
			node.children("a").children("div").children("table").children("tbody").children("tr").children("td").children().children('select').each(function(){ // Sauf si le td contient un select la cellule n'est pas éditable
				$(this).parent('div').parent('td').attr("contenteditable", "false");
			});
		});
		
		//bouton itération "+"
		$("#buttonPlusIter").on("click", function() {
			var old_value = $("#nb_iter").val(); //on récupère la valeur de l'input contenant le nombre d'itération de l'étape
			var nbIter = parseFloat(old_value) + 1; // puis on ajoute 1
			if(old_value == 1){ //si l'ancienne valeur est de 1 alors on affiche la sélection des paramètres et le tableau
				$(".stepParameters").css("display", "inline");
				if ($("#nb_param").val() > 0){
					$("#tableIter").show();
				}
			}
			$("#nb_iter").val(nbIter);
			var nbParam = $("#nb_param").val();
			//on ajoute une colonne (en faisant attention que si iteration = 1, le tableau n'est que 2 colonnes sinon il y en a 3 (tableau par défaut et dans ce cas on n'ajoute pas de nouvelle colonne)
			if ((nbIter == 2 && $("#tableIter > tbody").find("> tr:first > th").length == 2) || nbIter > 2) { 
				$("#tableIter tr:first th:last").after('<th>'+nbIter+'</th>');
				while (nbParam > 0) {
					$('#tableIter tr:eq('+nbParam+') td:last').after('<td contenteditable="true"></td>');
					nbParam--;
				}
			}
		});
		//bouton itération "-"
		$("#buttonMinusIter").on("click", function() {
			var old_value = $("#nb_iter").val(); //récupération valeur
			if (old_value == 2){ // si l'ancienne valeur est de 2 alors on cache la sélection des paramètres et le tableau
				$(".stepParameters").hide();
				$("#tableIter").hide();
			}
			if (old_value > 1) { // on enlève uniquement si le nombre est supérieur à 0
			  $("#nb_iter").val(parseFloat(old_value) - 1);
			} else {
			  $("#nb_iter").val(1);
			}
			var nbParam = $("#nb_param").val();
			if (old_value > 2){ // on enlève une colonne que s'il en reste plus de 2 (sinon le tableau est caché = pas utile)
				$("#tableIter tr:first th:last").remove();
				while (nbParam >0) {
					$('#tableIter tr:eq('+nbParam+') td:last').remove();
					nbParam--;
				}
			}
		});
		
		//bouton paramètres "+"
		$("#buttonPlusParam").on("click", function() {
			var old_value = $("#nb_param").val(); //on récupère la valeur de l'input contenant le nombre de paramètres de l'étape
			var nbParam = parseFloat(old_value) + 1; // puis on ajoute 1
			if (old_value == 0){ //si l'ancienne valeur est de 0 alors on doit afficher le tableau
				$("#tableIter").show();
			}
			$("#nb_param").val(nbParam);
			var nbIter = $("#nb_iter").val();
			// on ajoute une nouvelle ligne pour un nouveau paramètre
			var newRow = '';
			/*
			Ce code est supprimé car il pose problème sur le choix du paramètre sélectionné dans les actions paramétrées
			if (action_structured) { // si l'action contient des actions structurées (affichage de select)
				newRow += '<tr><td contenteditable="false" placeholder="' + __("Paramètre") + ' ' + nbParam+'">'+selectIntoTableForParameter(node, __("Paramètre") + ' ' + nbParam, nbParam)+'</td>';
			} else {
				newRow += newRow = '<tr><td contenteditable="true" placeholder="' + __("Paramètre") + ' ' + nbParam+'">' + __("Paramètre") + ' ' + nbParam+'</td>';
			}*/
			newRow += newRow = '<tr><td contenteditable="true" placeholder="' + __("Paramètre") + ' ' + nbParam+'">' + __("Paramètre") + ' ' + nbParam+'</td>';
			while (nbIter > 0) { 
				newRow += '<td contenteditable="true"></td>';
				nbIter--;
			}
			newRow += '</tr>';
			$("#tableIter").append(newRow);
			// Appel de la fonction placeholderTable() qui gère un placeholder prennant en compte les nouvelles lignes créées
			placeholderTable();
			// Appel de la fonction changeOnSelect(node) qui gère les changements concernant les Select des tableaux itératifs (pour les nouvelles lignes créées )
			changeOnSelect(node);
		});
		
		//bouton paramètres "-"
		$("#buttonMinusParam").on("click", function() {
			var old_value = $("#nb_param").val(); //récupération valeur
			if (old_value == 1){ // si l'ancienne valeur est de 1 alors on doit cacher le tableau
				$("#tableIter").hide();
			}
			if (old_value > 0) { // on enlève 1 uniquement si le nombre est supérieur à 0
			  $("#nb_param").val(parseFloat(old_value) - 1);
			} else {
			  $("#nb_param").val(0);
			}
			if (old_value > 0) { // on enlève une ligne uniquement si l'ancienne valeur est supérieure à 0 (pour ne pas supprimer l'entête)
				$('#tableIter tr:last').remove();
			}
		});
		
		// Appel de la fonction placeholderTable() qui gère un placeholder prennant en compte les lignes déjà créées au sein du tableau en cours d'édition
		placeholderTable();
		// Appel de la fonction changeOnSelect(node) qui gère les changements concernant les Select des tableaux itératifs (pour les lignes déjà créées au sein du tableau en cours d'édition)
		changeOnSelect(node);
		
		// bouton valider
		$('#validateTask_'+idLD).bind('click', function() {
			var checkInput = true;
			var checkTd = true;
			//Si on traite un contenu structuré
			if (structure == 'content') { 
				node.find(".contentStructured:eq(0)").find("input").each(function(){ // Pour chaque input présent dans le contenu d'une action structurée
					if ($(this).attr('type_param') == 'quantity'){ // Si c'est un input correspondant à un paramète de type 'quantity'
						var dataInput = $(this).val(); // récupération de sa valeur
						// Remplace les '.' par des ',' (séparateur décimal français) et les exposants '^' par 'E'
						dataInput = dataInput.replace(/\./g, ',').replace(/e/g, 'E').replace(/\^/g, 'E');
						$(this).val(dataInput);
						checkInput = isNumberFormat(dataInput); // vérification si le format est bien de type numérique
						if (!checkInput){ // si la valeur de l'input ne correspond pas à un nombre décimal (avec exposant possible)
							alert( __('Vous devez renseigner un nombre valide pour le paramètre')+' "'+$(this).attr('name')+'"\n'+__('Exemples') + ":" +' 12,75 ; 1,425E-6');
							$(this).addClass('errorInput');
						} else {
							$(this).removeClass('errorInput');
						}
					}
				});
				//On remplace les input par des <spans> (on enlève le mode édition)
				if (checkInput) { // Si le format d'un input n'est pas valide : on ne change pas l'affichage des inputs
                    replaceInputIntoSpan(node.find(".contentStructured:eq(0)"));
                }
				var content = node.find(".contentStructured:eq(0)").html();
			} else {
				var content = cleanHTMLInputs(node.find(".txtEdited:eq(0)").html(),' - ') ;
				node.find(".txtEdited:eq(0)").html(content) ; // apply the modification to JSTree - useless
			}

			var comment = null;
			//Si on a un commentaire edité
			if(node.find(".commentEdited:eq(0)").length > 0){
				comment = cleanHTMLInputs(node.find(".commentEdited:eq(0)").html(),' - ') ;
				node.find(".commentEdited:eq(0)").html(comment); // apply the modification to JSTree - useless
			}

			//On récupères les images insérés
			var iImages_tab = new Array;
			var image_indice = 0;
			if(action==true){
				node.find("a:eq(0) img.cx_image_uploaded").each(function(){
					iImages_tab[image_indice] = $(this).attr('src');
					image_indice++;
				});
			}

			var table ="";
			node.attr("iteration", $("#nb_iter").val());
			node.attr("parameters", $("#nb_param").val());
			// On récupère les données de la table que si Parameter > 0 et Iteration > 1 (sinon la table n'a aucun d'intérêt)
			if (($("#nb_param").val() > 0) && ($("#nb_iter").val() > 1)) { 
				// Vérification si le format des cellules du tableau sont correctes dans le cas où un paramètre sélectionné est de type 'quantity'
				$("#tableIter td:first-child").each(function(){
					if($(this).children().children('select').attr('type_param') == 'quantity'){ // si paramètre sélectionné de type 'quantity'
						$(this).siblings('td').each(function(){ // pour chaque cellule de la ligne qui suit
							var dataTd = $(this).text();
							// Remplace les '.' par des ',' (spérateur décimal français) et les exposants '^' par 'E'
							dataTd = dataTd.replace(/\./g, ',').replace(/e/g, 'E').replace(/\^/g, 'E');
							$(this).text(dataTd);
							// Mise-à-jour de notre variable qui indique si le format des cellules est correcte
							checkTd = isNumberFormat(dataTd); // vérifiaction si le format est bien de type numérique
							if (!checkTd){ // si la valeur de la cellule ne correspond pas à un nombre décimal (avec exposant possible)
								alert(__('Vous devez renseigner un nombre valide pour le paramètre')+' "'+$(this).prevAll('td:first-child').children('select').val()+'"\n'+__('Exemples') + __("&nbsp;:") +' 12,75 ; 1,425E-6');
								$(this).addClass('errorTd');
							} else {
								$(this).removeClass('errorTd');
							}
							
						});
					}
				});
				if (checkTd) { // Si les cellules du tableau sont au bon format
					$("#tableIter").find('select').each(function() { // récupère la valeur du select si il y en a dans le tableau et supprime le select et l'input pour l'affichage hors édition et l'enregistrement XML
						var valueSelect = $(this).val();
						$(this).parent().parent().html(valueSelect);
						
					});
					table = $("#tableIter").html();
				}
			}
			if (checkInput && checkTd) { // Si les input du content et les cellules du tableau sont au bon format
				//On ferme l'édition de la node et on enregistre dans le XML
				closeNode(node, idLD, name, content, comment, table, iImages_tab, comment_Label) ; // on ferme le mode édition
				placeholderTable(); // s'assure que le css est correcte pour le texte du tableau itératif
			}
		});
        
		// bouton annuler
		$('#cancelTask_'+idLD).bind('click', function() {
			if (creation) { //supprimer le node
				$(div_procedure).jstree("remove", node) ;
				global_tab_ld[idLD].ld_locker = 0;
			} 
			else { // revenir à l'affichage antérieur
				if (structure == 'content') { 
					node.find(".contentStructured:eq(0)").html(iContent); //récupère les données initiales
					replaceInputIntoSpan(node.find(".contentStructured:eq(0)")); // change les balises input, select en parameter
					iContent = node.find(".contentStructured:eq(0)").html(); // récupération de ce contenu
				}
				if(node.attr("rel") === "step_iter"){
					$(node).children('ul').html(iContentActionAfterIteration);
				}

				//On supprime les images dans le node
				node.find("img.cx_image_uploaded").remove();

				//On ré-insères l'html des images provenant du XML
				node.find(".divButtons").before(iImages);

				//Maintenant on peut traiter l'enregistrement dans le XML des bonnes images
				var iImages_tab = new Array;
				var image_indice = 0;
				if(action==true){
					node.find("img.cx_image_uploaded").each(function(){
						iImages_tab[image_indice] = $(this).attr('src');
						image_indice++;
					});
				}

				//On ferme l'édition de la node et on enregistre dans le XML
				closeNode(node, idLD, name, iContent, iComment, iTable, iImages_tab, comment_Label) ;
			}
		});
	}
}

/**
 * Test si une donnée correspond à un format numérique considéré comme valide ou non
 * @param {string} data - Le texte testé
 */
function isNumberFormat(data) {
	var rgx = /^[+-]?\d*(,\d+)?(E[-]?\d+)?$/g ; // valeurs possibles : 12 ; 4562,23 ; 12,145E7 ; 5,7E-3 etc.
	var bool = rgx.test(data); // booléen qui test si les données correspondent bien à un nombre valide.
	return bool;
}

/**
 * Gère des placeholders dans les <td> de la première colonne des tableaux des actions itératives. De type "paramètre X".
 * Agit directement sur le DOM
 */
function placeholderTable() {
	$('.jstree td:first-child').each(function(){
		var defaultValue = $(this).attr('placeholder'); // valeur du placeholder
		if ($(this).html() == defaultValue) { // Si c'est la valeur du placeholder on affiche un style gris et italique
			$(this).css('color', '#7f7f7f');
			$(this).css('font-style', 'italic');
		} else {
			$(this).css('color', '#000000');
			$(this).css('font-style', 'normal');
		}
		$(this).focus(function(){ // Au moment du focus dans la cellule du tableau
			if($(this).html() == defaultValue) { // Si la valeur est celle du placeholder
				$(this).html('');
				$(this).css('color', '#000000');
				$(this).css('font-style', 'normal');
			}
		});
		$(this).blur(function(){ // Au moment où l'on sort de la cellule du tableau
			if($(this).text() == '') { // Si le texte est vide on remet l'information du placeholder
				$(this).html(defaultValue);
				$(this).css('color', '#7f7f7f');
				$(this).css('font-style', 'italic');
			}
		});
	});
}

/**
 * Appel un ensemble de fonctions qui traitent des changements liés aux Select à l'intérieur du tableau des étapes itératives
 * @param {HTMLElement} node - l'objet HTML node sur lequel on va intéragir directement (ajouts de classes)
 */
function changeOnSelect(node) {
	// Quand on passe sur une option : met en valeur le paramètre correspondant dans la liste des actions de l'étape (ajout classe CSS)
	$('.non').mouseenter(function(){ 
		var index = $(this).index();
		var parameter = $(node).children('ul').find('content').find('span')[index-1];
		$(parameter).addClass('hoverSelectParameter');
	});
	// Quand on quitte une option : enlève la classe CSS pour le paramètre correspondant dans la liste des actions de l'étape
	$('.non').mouseleave(function() { 
		var valueOption = $(this).html();
		$(node).children('ul').find('content').find('span').each(function(){ // Pour chaque paramètre trouvé
			if ($(this).attr('name') == valueOption){ // Si le nom du paramètre corespond au texte de l'option
				$(this).removeClass('hoverSelectParameter');
			}
		});
	});
	
	(function () {
		var number_select;
		var previous_index_selected_option;
		var attribut_previous_selected_option;
		// Lorsqu'on entre dans un Select
		$('#tableIter select').focus(function () {
			number_select = $(this).attr('id').substring(17); // récupération du Select par son nombre
			// Récupère l'index de l'option courante du Select avant un changement
			previous_index_selected_option = $('#selectParameters_'+number_select)[0].selectedIndex;
			attribut_previous_selected_option = $('#selectParameters_'+number_select).children('option:selected').attr('class');
		// Pour chaque changement dans un Select
		}).change(function(){
			var initial_text = $('#selectParameters_'+number_select+' .editable').text(); // texte de l'option editable
			if(initial_text != $('#inputEditOption_'+number_select).attr('placeholder')) { // Si le texte de l'option est différent du placeholder de l'input
				$('#inputEditOption_'+number_select).val(initial_text); // ajoute le texte à l'input qui sert d'édition
			} else {
				$('#inputEditOption_'+number_select).val(''); // sinon texte vide pour faire apparaître le placeholder dans l'input
			}
			var selected = $('option:selected', this).attr('class'); // Récupère la classe de l'option sélectionnée
			if(selected == "editable"){ // Si la classe est éditable
				$('#inputEditOption_'+number_select).css('display','block'); // On montre l'input
			}else{
				$('#inputEditOption_'+number_select).hide(); // sinon on cache l'input
			}
			/*************DEBUT GESTION CROIX : plus possible de modifier la valeur d'un paramètre dans son action ************/
			var index_selected_option = $('#selectParameters_'+number_select)[0].selectedIndex;
			var parameter;
			// Si la nouvelle valeur a été modifiée par rappot à l'ancienne valeur
			if (index_selected_option != previous_index_selected_option) {
				// Test : si la valeur du select n'est pas sélectionnée dans un autre Select -> Si test_selection == true ce n'est pas le cas, si test_selection == false c'est le cas donc on enlève pas la croix
				var test_selection = true;
				var position_select = 1;
				var piso = previous_index_selected_option; // copie variable pour ne pas que la valeur soit modifiée avec le each()
				var apso = attribut_previous_selected_option; // copie variable pour ne pas que la valeur soit modifiée avec le each()
				$('#tableIter select').each(function(){
					if (position_select != number_select) { // si les Select qu'on compare ont un numéro différent
						if (apso == 'non') { // Si l'ancienne valeur du Select est un paramètre du action structurée (il y avait une Croix)
							if ($(this)[0].selectedIndex == piso) test_selection = false; // Si les 2 select ont le même index on enlève pas la croix (le paramètre est tjrs sélectionné 1 fois)
						} else {
							test_selection = false;
						}
					}
					position_select++;
				});
				previous_index_selected_option --; // soustrait 1 car la première option est éditable
				if(test_selection){ // Si la sélection n'est pas également sélectionnée par un autre select..
					// Enlève la croix du précédent paramètre qui été sélectionné
					parameter = $(node).children('ul').find('content').find('span')[previous_index_selected_option];
					if ($(parameter).attr('type') == 'quantity'){
						$(parameter).children('value').html('')
					} else {
						$(parameter).html('');
					}
				}
				// Condition If : si la sélection du Select correspond à un paramètre (classe 'non'), alors on récupère le paramètre dans son action structurée pour modifier son contenu par une croix.
				if ( $('#selectParameters_'+number_select+' option:eq('+index_selected_option+')').attr('class') == 'non') {
					parameter = $(node).children('ul').find('content').find('span')[index_selected_option-1];
					if ($(parameter).attr('type') == 'quantity'){
						$(parameter).children('value').html('X')
					} else {
						$(parameter).html('X');
					}
					$('#selectParameters_'+number_select).attr('type_param', $(parameter).attr('type')); // change la valeur de l'attribut 'type_param' en fonction du type du paramètre
				} else {
					$('#selectParameters_'+number_select).attr('type_param', 'none'); // si ce n'est pas un paramètre défini, l'attribut 'type_param' prend valeur 'none'
				}
				previous_index_selected_option = index_selected_option; //maj de l'ancienne valeur
				attribut_previous_selected_option = $('#selectParameters_'+number_select).children('option:selected').attr('class');
			}
			/************FIN GESTION CROIX****************/
		});
	})();
	
	// Pour chaque caractère ajouté / enlevé de l'input
	$('.editOption').keyup(function(){
		var number_input = $(this).attr('id').substring(16); // Récupère l'input exact par son numéro
		var edit_text = $('#inputEditOption_'+number_input).val(); // puis son contenu
		if(edit_text != "") { // Si le contenu n'est pas vide
			$('#selectParameters_'+number_input+' .editable').html(edit_text); // On donne le même nom à l'option éditable
		} else {
			$('#selectParameters_'+number_input+' .editable').html($('#inputEditOption_'+number_input).attr('placeholder')); // sinon (= contenu vide) on redonne le nom par défaut à l'option éditable : celui du placeholder de l'input
		}
	});
}

/**
 * Renvoi le code Html pour un Select et un Input contenant les paramètres des actions structurées au sein d'une étape itérative
 * @param {HTMLElement} node 
 * @param {number} idLD 
 * @param {string} value - La valeur selectionnée
 * @param {number} parameter 
 * @param {boolean} isNotPlaceholder 
 */
function selectIntoTableForParameter(node, value, parameter, isNotPlaceholder) {
	var optionSelected = false;
	var occurenceParam = 1;
	// Vérifie si le paramètre n'est pas déjà présent dans les Select précédent pour compter son nombre d'occurence
	if (parameter == 2){
		var optionValueSelected = $('#selectParameters_'+1).children('option:selected').html();
		if (value == optionValueSelected) occurenceParam ++;
	} else if ( parameter > 2){
		for (var i = 1 ; i < parameter ; i++){
			var optionValueSelected = $('#selectParameters_'+i).children('option:selected').html();
			if (value == optionValueSelected) occurenceParam ++;
		}
	}
	var inputHtml = '';
	var optionParamHtml = '';
	var optionEditableHtml = '';
	var typeParamSelected = 'none';
	if(isNotPlaceholder) { // true : value != placeholder
		$(node).children('ul').find('content').find('span').each(function(){ // Pour chaque paramètre trouvé on ajoute une option au Select
			var nameParameter = $(this).attr("name");
			if (nameParameter == value && ($(this).html() == 'X' || $(this).children("value").html() == 'X')) { // si le nom du paramètre est égale à la valeur de l'option et que le texte du paramètre est 'X' alors c'est le bon paramètre
				if (occurenceParam == 1) { // Si occurence==1 alors c'est que c'est le bon paramètre sinon c'est une occurence.
					optionParamHtml += '<option class="non" selected>'+nameParameter+'</option>';
					typeParamSelected = $(this).attr("type");
					optionSelected = true;
					occurenceParam--;
				} else {
					optionParamHtml += '<option class="non">'+nameParameter+'</option>';
					occurenceParam--;
				}
			} else {
				optionParamHtml += '<option class="non">'+nameParameter+'</option>';
			}
		});
		if (optionSelected) { // si une option a été sélectionnée on ajoute la valeur par défaut dans le champs éditable
			optionEditableHtml += '<option class="editable">Paramètre '+parameter+'</option>';
			inputHtml = '<input type="text" id="inputEditOption_'+parameter+'" class="editOption" placeholder="' + __("Paramètre") + ' ' + parameter+'" style="display:none;" />';
		} else { // sinon c'est une valeur éditée et on la sélectionne
			optionEditableHtml += '<option class="editable" selected>'+value+'</option>';
			inputHtml = '<input type="text" id="inputEditOption_'+parameter+'" class="editOption" value="'+value+'" placeholder="' + __("Paramètre") + ' ' + parameter+'" style="display:block;" />';
		}
	} else { // false : value == placeholder
		optionEditableHtml += '<option class="editable">'+value+'</option>';
		$(node).children('ul').find('content').find('span').each(function(){ // Pour chaque paramètre trouvé on ajoute une option au Select
			optionParamHtml += '<option class="non">'+$(this).attr("name")+'</option>';
		});
		inputHtml = '<input type="text" id="inputEditOption_'+parameter+'" class="editOption" placeholder="' + __("Paramètre") + ' ' + parameter+'" style="display:block;" />';
	}
	var selectHtml = '<div class="divInTd"><select id="selectParameters_'+parameter+'" type_param="'+typeParamSelected+'">'+optionEditableHtml+optionParamHtml+'</select>'+inputHtml+'</div>';
	return selectHtml;
}

/**
 * Cette fonction permet de fermer le mode edition de la node, et d'enregistrer dans le XML les différents contenus passés en paramètres
 * @param {*} node - La node en cours
 * @param {*} idLD - L'identifiant du labdoc
 * @param {*} name - Le nom de la node
 * @param {*} content - Le contenu
 * @param {*} comment - Le commentaire
 * @param {*} table - La table (itération de l'exp)
 * @param {*} images - Les images ajoutés (Nouveau 05/2017 by N.T)
 * @param {*} commentLabel - libellé du commentaire (optionnel)
 */
function closeNode(node, idLD, name, content, comment, table, images, commentLabel) {
	var div_procedure = document.getElementById('labdoc_content_'+idLD).lastChild ;
	// reconstruire le contenu de la balise a 
	var html = '' ; //'<ins class="jstree-icon"> </ins>' ;
	if (name) html += '<name>'+name+'</name> ';
	html += '<content>'+content+'</content>';

	//Si on a un commentLabel et qu'il est personalisé (paramètre optionnel)
	if(typeof commentLabel != 'undefined' && commentLabel != "Commentaire :"){
		html += '<span class="commentLabelEdit">' + commentLabel + '</span>' ;
	}

	//Si on a un commentaire
	if(comment != null){
		html += '<comment>'+comment+'</comment>' ;
	}

	//Si on doit enregistrer une ou plusieurs images
	if(typeof images != 'undefined' && images.length > 0){
		for(var i=0; i < images.length;i++){
			if(images){
				html += '<img class="cx_image_uploaded"  src="' + images[i] + '">';
			}
		}
	}

	if (node.attr("rel") !== "action") { // insertion balise table si ce n'est pas une action
		// Affichage d'une phrase précisant le nombre d'itération quand nécessaire
		if (node.attr("iteration") > 1) {
			html += '<step>' + __('Réaliser cette étape {{nbi}} fois', {nbi:node.attr("iteration")}) + '</step>';
			node.attr("rel", "step_iter"); //change le type de noeud pour metre à jour l'icône (cas itération)
		} else { // Sinon pas d'itération on remet (ou laisse) le type à étape simple
			node.attr("rel", "step");
		}
		html += '<table>'+table+'</table>' ;
	}
	//node.children("a:eq(0)").html(html) ;
	$(div_procedure).jstree("rename_node", node, html) ;
	node.removeClass("inEdition") ;
	
	// changer l'attribut contenteditable des cellules du tableau à false pour ne plus les modifier hors édition
	node.children("a").children("table").children("tbody").children("tr").children("td").attr("contenteditable", "false");
	
	// remettre en route le menu contextuel
	node.children("a:eq(0)").removeClass("jstree-contextmenu-disabled") ;
	global_tab_ld[idLD].ld_locker = 0;
	saveProcedure(idLD) ;
}
