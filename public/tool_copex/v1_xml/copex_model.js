//**********************************************************
//                   Partie Matériel
//**********************************************************

// crée un objet matériel
function Material(definedBy, id, quantity, name, description, comment, type, image, selected, used, hidden){ 
	this.definedBy = definedBy ;
	this.id = parseInt(id) ; 
	this.quantity = parseInt(quantity) ;
	this.name = name ;
	this.description = description ;
	this.comment = comment ;
	this.type = type ;
	this.image = image ;
	this.selected = parseInt(selected) ;
	this.used = parseInt(used) ;
    this.hidden = parseInt(hidden);
	
	this.getXML = function() { // Renvoie la description de l'objet au format texte (XML)
	    var xml_content = '<material definedBy="'+this.definedBy ;
		xml_content += '" id="'+this.id ;
		if (this.quantity) { xml_content += '" quantity="'+this.quantity ; }// on ne spécifie pas la quantité si elle est indéfinie
        if (this.hidden) { xml_content += '" hidden="'+this.hidden ; }
		xml_content += '">\n' ;
		xml_content += '   <name>'+this.name+'</name>\n' ;
		xml_content += '   <description>'+this.description+'</description>\n' ;
        if (this.comment) {
            xml_content += '   <comment>'+this.comment+'</comment>\n' ;
        }
		for (var i=0; i<this.type.length; i++) { xml_content += '   <type>'+this.type[i]+'</type>\n' }
		if (this.image) xml_content += '   <image>'+this.image+'</image>\n' ;
		xml_content += '   <selected>'+this.selected+'</selected>\n' ;
		xml_content += '   <used>'+this.selected+'</used>\n' ;
		xml_content += '</material>\n' ;
		return xml_content ;
    } 
}

// peuple le tableau des matériels d'un protocole à partir du XMLDoc
function getMaterialListfromXML(id_ld){ 
	var ML = [] ;
	$(global_tab_ld[id_ld].ld_content).find("material").each(function() { // pour chaque materiel du XMLDoc
		
		// création du tableau des types
		var types = new Array() ;
		var i=0;
		$(this).find("type").each(function() { // pour chaque type du matériel
			types.push($(this).text()) ;
			i++;
		});
		if (i===0) types.push('none') ; // si on ne trouve pas de type, on attribue un type générique 'none'
		
		// création de l'objet matériel
		var obj = new Material($(this).attr("definedBy"), $(this).attr("id"), $(this).attr("quantity"), $(this).find("name").html(), $(this).find("description").html(), $(this).find("comment").html(), types, $(this).find("image").html(), $(this).find("selected").html(), $(this).find("used").html(), $(this).attr("hidden")) ;
		ML[obj.id] = obj ; // ajout de l'objet au tableau ML
	});
	// si copexGeneralMaterial est true on récupère aussi tous les materiels decrits dans copex_conf.xml : TODO
	// Attention à la mise à jour : les materiels utilisés devront être copiés dans le fichier perso (??)
	return ML;
}

// renvoie une liste de matériel d'un protocole sous forme d'un tableau selon les critère spécifiés
function getMaterialList(idLD, searchText, selected, type, ordered) { 
	var ML = [] ;
	for (var id in global_tab_ld[idLD].ML) {
		if ( global_tab_ld[idLD].ML[id] && (typeof global_tab_ld[idLD].ML[id] == "object") ) { // le matériel doit exister (il a pu être effacé) - le typeof est un espèce de hack pour la page auteur : le fait de passer par le DOM pour parser le XML interfère avec un prototype de la page auteur. Bizarre !
			if (!searchText || (searchText && global_tab_ld[idLD].ML[id].name.toLowerCase().indexOf(searchText.toLowerCase()) !== -1)) { // si il y a une recherche de texte
				if (selected==='all' || (selected==='selected' && global_tab_ld[idLD].ML[id].selected !== 0) || (selected==='unselected' && global_tab_ld[idLD].ML[id].selected === 0)) { // si sélection ou non sélection
					if (!type || (type && global_tab_ld[idLD].ML[id].type.indexOf(type) !== -1)) { // si il y a spécification d'un type
						ML.push(global_tab_ld[idLD].ML[id]) ;
					}
				}
			}
		}
	}
	if (ordered) ML.sort(function(a,b){return (a.name > b.name)?1:-1}) ; // tri par ordre alphabétique selon le nom des matériels
	return ML ;
}

//**********************************************************
//                   Partie Action
//**********************************************************

// crée un objet action
function Action(name, content, comment, structured, commentLabel){ 
	this.name = name;
	this.content = content;
	this.comment = comment;
	this.structured = structured;
	this.commentLabel = commentLabel;
}

// peuple le tableau des actions d'un protocole
function getActionListfromXML(id_ld){
	var AL = [] ;
	$(global_tab_ld[id_ld].ld_content).find("action").each(function() { // pour chaque action du XMLDoc
		var copyAction = $(this).clone();
		var struct = $(this).attr("structured") ;
		if (struct == "none") { // on donne le name par défaut à l'action libre si elle n'en a pas
			var name = ( $(this).find("name").html() ? $(this).find("name").html() : __("Action libre") ) ; 
		}
		else {
			var name = $(this).find("name").html() ;
			replaceParameterIntoSpan(copyAction.find("content")); // On remplace à partir d'une copie du XML Client les balises <parameter> par des balises <span> pour ne pas avoir de problèmes d'affichages (liés aux navigateurs et fonction JQuery .html())
		}
		var comment = null;
		if($(this).find("comment").length > 0){
			comment = $(this).find("comment").html();
		}
		var commentLabel = null;
		if($(this).find("commentLabel").length > 0){
			commentLabel = $(this).find("commentLabel").html();
		}
		var obj = new Action(name,copyAction.find("content").html(),comment,struct,commentLabel) ;
		AL.push(obj) ; 
	});
	AL.sort(function(a,b){return (a.name > b.name)?1:-1}) ; // tri par ordre alphabétique
	return AL;
}