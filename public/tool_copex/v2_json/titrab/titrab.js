const titrabCtrl = function (simulationObject) {

	function init() {
		// Init specific values
		this.globalOffsetX = 255;
		this.globalOffsetY = 25;
		this.baseUnits = { "molar concentration" : "mol.L-1", volume: "mL" };
		this.beakerVolumeStep = this.simulationObject.unitConvertToBaseUnit(0.0001, 'L');
		this.simulationObject.replaceLdResponse = true;
		this.simulationObject.replaceLdData = true;
	}

	function initSimulationDrawing () {
		this.simulationObject.simulationDisplayDiv.style.display = 'none';
		this.beaker1 = new Image();
		this.beaker1.src = '/tool_copex/v2_json/titrab/images/beaker1.png';
		this.beaker1.onload = () => {
			this.drawSimulation();
		};
		this.beaker2 = new Image();
		this.beaker2.src = '/tool_copex/v2_json/titrab/images/beaker2.png';
		this.beaker2.onload = () => {
			this.drawSimulation();
		};
		this.burette = new Image();
		this.burette.src = '/tool_copex/v2_json/titrab/images/burette.png';
		this.burette.onload = () => {
			this.drawSimulation();
		};
	}

	function checkUndefinedVariables (undefinedVariables) {
		return undefinedVariables.length > 0 && !(undefinedVariables.length === 1 && undefinedVariables[0].variable === 'colorizeIndicator')
	}

	function setSpecificVariables () {
	}
	
	function convertRemainingInputs(simulationVariables, actionTextPart) {
		if (['dropLiquidTotalVolumeAndUnit', 'dropLiquidMinVolumeAndUnitFraction', 'dropLiquidMaxVolumeAndUnitFraction', 'addLiquidTotalVolumeAndUnit', 'diluteVolume']
			.includes(actionTextPart.simulationVariable)) {
			simulationVariables[actionTextPart.simulationVariable][0] = this.simulationObject.unitConvertToBaseUnit(parseFloat(simulationVariables[actionTextPart.simulationVariable]), "mL");
		}
		return simulationVariables;
	}

	function computeRemainingValues(simulationVariables, structuredAction) {
		if (structuredAction.simulationVariable === 'dilute' 
			&& !Object.keys(simulationVariables).some(key => 
				key !== 'stepSimulationVariable' 
				&& (typeof(simulationVariables[key]) === "undefined" || simulationVariables[key] === null)
			)) {
			if(!simulationVariables.diluteSolution.materialNotCreated && !simulationVariables.diluteSolution.materialUsedBeforeCreation) {
				for (let key of Object.keys(simulationVariables.diluteSolution).filter(k => k !== "name" && k !== "id" && k !== "position")) {
					simulationVariables.diluteCreated[key] = JSON.parse(JSON.stringify(simulationVariables.diluteSolution[key]));
				}
				if (!simulationVariables.diluteCreated.properties.find(p => p.name === 'concentration')) {
					this.simulationObject.copexObject.addPropertyToMaterial(simulationVariables.diluteCreated, {
						name: 'concentration', physicalQuantityId: null, value: null, unitId: null, hidden: true
					});
				} else {
					simulationVariables.diluteCreated.properties.find(p => p.name === 'concentration').value =
						this.simulationObject.unitConvertToBaseUnit(simulationVariables.diluteVolumeAndUnit[0], simulationVariables.diluteVolumeAndUnit[1]) /
						this.simulationObject.propertyValueConvertInbaseUnit(simulationVariables.diluteContainer, 'volume')
						* this.simulationObject.propertyValueConvertInbaseUnit(simulationVariables.diluteSolution, 'concentration');
				}
				let baseSolutionShortName = simulationVariables.diluteSolution.properties.find(p => p.name === 'shortName') || {value: 'default'};
				if (baseSolutionShortName) {
					this.simulationObject.copexObject.addPropertyToMaterial(simulationVariables.diluteCreated, {
						name: 'originShortName',
						physicalQuantityId: null,
						value: baseSolutionShortName.value,
						unitId: null, hidden: true
					});
				}
			}
		}
		return simulationVariables;
	}

	function fillBuretteParameters(droppedVolume=0) {
		let parameters = {};
		const titratorAction = this.simulationObject.protocol.findLast(a => a.structuredAction === 'fill');
		if (titratorAction) {
			parameters.color = titratorAction.fillSolution ? this.simulationObject.getPropertyValueByName(titratorAction.fillSolution, 'color') : '#ffffff';
			parameters.color += '80';
			const buretVolume = this.simulationObject.propertyValueConvertInbaseUnit(this.simulationObject.getMaterialFromShortName('burette'), 'volume');
			parameters.height = (1 - droppedVolume / buretVolume) * 236;
			return parameters;
		} else {
			return {
				height: 0,
				color: '#ffffff'
			};
		}
	}

	function fillBeaker(droppedVolume=0, indicatorParameters={}) {
		droppedVolume = parseFloat(droppedVolume);
		const beakerFillingActions = this.simulationObject.protocol.filter(a => a.structuredAction === 'addLiquid');
		let totalVolume = 0;
		let parameters = {};
		parameters.color = '#ffffff';
		for (let beakerFillingAction of beakerFillingActions) {
			let addedVolume = this.simulationObject.unitConvertToBaseUnit(beakerFillingAction.addLiquidTotalVolumeAndUnit[0], beakerFillingAction.addLiquidTotalVolumeAndUnit[1]) || 0;
			totalVolume += addedVolume;
			parameters.color = this.simulationObject.blendColors(
				parameters.color, 
				this.simulationObject.getPropertyValueByName(beakerFillingAction.addLiquidSolution, 'color'),
				totalVolume, 
				addedVolume
            );
		}
		totalVolume += droppedVolume;
		const titratorAction = this.simulationObject.protocol.findLast(a => a.structuredAction === 'fill');
		if (titratorAction) {
			parameters.color = this.simulationObject.blendColors(
				parameters.color,
				this.simulationObject.getPropertyValueByName(titratorAction.fillSolution, 'color'),
				totalVolume,
				droppedVolume
			);
		}
		const colorizeAction = this.simulationObject.protocol.findLast(a => a.structuredAction === 'colorize');
		if (colorizeAction && colorizeAction.colorizeIndicator) {
			if (indicatorParameters.volume1 && droppedVolume < indicatorParameters.volume1) {
                parameters.color = indicatorParameters.color1;
			} else if (indicatorParameters.volume1 && indicatorParameters.volume2 && droppedVolume > indicatorParameters.volume1 && droppedVolume < indicatorParameters.volume2) {
                parameters.color = indicatorParameters.color2;
			} else if (indicatorParameters.volume2 && droppedVolume > indicatorParameters.volume2) {
                parameters.color = indicatorParameters.color3;
			}
		}
		parameters.color += '80';
		const maxVolume = parseFloat(this.simulationObject.propertyValueConvertInbaseUnit(this.simulationObject.getMaterialFromShortName('beaker'), 'maxVolume'));
		const currentVolume = Math.min(maxVolume, totalVolume);
		parameters.offsetY = currentVolume / maxVolume * 85;
		return parameters;
	}
	
	function cleanSimulation() {
		this.drawSimulation();
	}

	function drawSimulation(droppedVolume=0, currentPH=null, indicatorParameters={}, fillBuret=false, fillBeaker=false) {
		this.simulationObject.protocol = this.simulationObject.retreiveParameterValues();
		let ctx = this.simulationObject.canvas.getContext("2d");
		ctx.clearRect(0, 0, this.simulationObject.canvas.width, this.simulationObject.canvas.height);
		ctx.fillStyle = "#d3d3d3";
		ctx.fillRect(0, 0, this.simulationObject.canvas.width, this.simulationObject.canvas.height);
		if(fillBuret || fillBeaker) {
			ctx.fillStyle = "black";
			ctx.font = "bold 18px arial, verdana, sans-serif";
			let actionTitle = (fillBuret && !fillBeaker && !droppedVolume)?'Remplissage de la burette':(!droppedVolume)?'Remplissage du bécher':'Ajouts successifs de titrant';
			let textWidth = ctx.measureText(actionTitle).width;
			ctx.fillText(actionTitle, this.simulationObject.canvas.width/2-textWidth/2, 20);
		}
		ctx.translate(this.globalOffsetX, this.globalOffsetY);
		if(this.beaker1 && this.beaker2 && this.burette) {
			// Buret filling
			const buretteParameters = this.fillBuretteParameters(droppedVolume);
			if(fillBuret) {
				ctx.fillStyle = buretteParameters.color;
				ctx.fillRect(20, 255 - buretteParameters.height, 25, buretteParameters.height);
			}
			ctx.fillStyle = "black";
			ctx.font = "11px arial, verdana, sans-serif";
			ctx.fillText(droppedVolume.toFixed(2) + ' ' + this.baseUnits['volume'], -30, 255 - buretteParameters.height);
            ctx.drawImage(this.beaker1, 0, 0, this.beaker1.naturalWidth, this.beaker1.naturalHeight, 0, 252, this.beaker1.naturalWidth, this.beaker1.naturalHeight);
            ctx.drawImage(this.burette, 0, 0, this.burette.naturalWidth, this.burette.naturalHeight, 20, 0, this.burette.naturalWidth, this.burette.naturalHeight);
            // Beaker filling
			const beakerParameters = this.fillBeaker(droppedVolume, indicatorParameters);
			if(fillBeaker) {
				ctx.fillStyle = beakerParameters.color;
				ctx.fillRect(20, 375 - beakerParameters.offsetY, 75, beakerParameters.offsetY );
			}
			// Beaker
            ctx.drawImage(this.beaker2, 0, 0, this.beaker2.naturalWidth, this.beaker2.naturalHeight, 0, 252, this.beaker2.naturalWidth, this.beaker2.naturalHeight);
			ctx.fillStyle = buretteParameters.color;
			if(droppedVolume && !this.simulationObject.simulationPaused) {
				ctx.fillRect(36, 255, 3, 375 - beakerParameters.offsetY - 255);
			}
		}
		const phMetreOffsetX = 192;
		const phMetreOffsetY = 332;
		ctx.translate(phMetreOffsetX, phMetreOffsetY);
		// pHmètre
		ctx.strokeStyle = "black";
		ctx.lineWidth = 2;
		ctx.strokeRect(0, 0, 143, 88);
		ctx.fillStyle = "#aaa";
		ctx.fillRect(1, 1, 140, 85);
		// radio
		ctx.beginPath();
		ctx.lineWidth = 1;
		ctx.fillStyle = "black";
		ctx.arc(13, 26, 7, 0, 2 * Math.PI);
		ctx.fill();
		ctx.closePath();
		ctx.beginPath();
		ctx.fillStyle = "white";
		ctx.arc(13, 26, 5, 0, 2 * Math.PI);
		ctx.fill();
		ctx.closePath();
		ctx.beginPath();
		ctx.fillStyle = "blue";
		ctx.arc(13, 26, 3, 0, 2 * Math.PI);
		ctx.fill();
		ctx.closePath();
		// pH text
		ctx.fillStyle = "black";
		ctx.font = "11px arial, verdana, sans-serif";
		ctx.fillText("pHmètre : ", 23, 29);
		ctx.fillStyle = "black";
		ctx.fillRect(22, 40, 52, 22);
		ctx.fillStyle = "#eee";
		ctx.fillRect(23, 41, 50, 20);
		ctx.fillStyle = "black";
		ctx.fillText(currentPH ? currentPH : 'mesure', 28, 55);
		ctx.translate(-phMetreOffsetX, -phMetreOffsetY);
		ctx.translate(-this.globalOffsetX, -this.globalOffsetY);
	}

	async function runSimulation () {
		window.scrollTo({
			'behavior': 'smooth',
			'left': 0,
			'top': this.simulationObject.feedbacksDiv.offsetTop - 80
		});
		document.getElementById(this.simulationObject.copexObject.id_ld+'_protocol_submission').getElementsByTagName('span')[0].style.visibility = 'hidden';
		return new Promise(async (resolve, reject) => {
			// Fill buret
			this.drawSimulation(0, null, {}, true, false);
			await new Promise(r => setTimeout(r, 1000));
			// Fill beaker
			this.drawSimulation(0, null, {}, true, true);
			await new Promise(r => setTimeout(r, 1000));

			let i=0;
			while(i<this.simulationData.points.length) {
				const droppedVolume = parseFloat(this.simulationData.points[i][0]);
				this.drawSimulation(droppedVolume, parseFloat(this.simulationData.points[i][1]).toFixed(2), this.simulationData.indicatorParameters, true, true);
				await new Promise(r => setTimeout(r, 100));
				if(!this.simulationObject.simulationPaused){
					let newI = this.simulationData.points.findIndex(p => parseFloat(p[0]) > (droppedVolume + this.beakerVolumeStep));
					i=newI!==-1?newI:this.simulationData.points.length;
				}
				if(!this.simulationObject.isRunning) {
					return;
				}
			}
			resolve(true);
		});
	}

	function displayFeedbackAfterSimulation() {
		if(!this.simulationObject.previous_error) {
			this.simulationObject.feedbackSimulation =
				__('Félicitations ! votre protocole est optimisé.')
				+ "<br />"
				+ __('En appliquant la méthode des tangentes à la courbe obtenue ') 
				+ "<a class='lb-a' href='#labdoc_"+this.simulationObject.idDataset+"'> ci dessous </a>"
				+ __(', on obtient un volume équivalent de : ')
				+ Math.round(100 * this.simulationData.equivalentVolume) / 100 + ' mL.';
		}
	}
	
	function restoreSimulationData (simulationData) {
		this.simulationData = simulationData;
	}
	
	function stopSimulation () {
		this.simulationObject.isRunning = false;
		this.simulationObject.simulationPaused = true;
		document.getElementById(this.simulationObject.copexObject.id_ld+'_protocol_submission').getElementsByTagName('span')[0].style.visibility = 'visible';
		this.cleanSimulation();
	}

	function getSimulationTextPartVariableNames () {
		return [
			"dropLiquidMinVolumeAndUnitFraction", "dropLiquidMaxVolumeAndUnitFraction",
			"addLiquidTotalVolumeAndUnit", "addLiquidSolution", "addLiquidMeasurer",
			"diluteContainer", "diluteVolumeAndUnit", "diluteSolution", "diluteMeasurer", "diluteSolvent", "diluteCreated", "diluteFullVolumeAndUnit",
			"conditioningContainer", "conditioningSolution",
			"measureP1", "measureP2", "measureP3", "measureP4",
			"colorizeIndicator",
			"fillSolution"
		]
	}

	function getSimulationActionVariableNames () {
		return ["addLiquid", "dropLiquid", "dilute", "conditioning", "measure", "colorize", "fill"];
	}
	
	function getSimulationStepVariableNames () {
		return ["buretteSetup", "beakerSetup", "titrationProcess"];
	}
	

	return {
		init,
		initSimulationDrawing,
		checkUndefinedVariables,
		setSpecificVariables,
		fillBuretteParameters,
		fillBeaker,
		cleanSimulation,
		drawSimulation,
		runSimulation,
		displayFeedbackAfterSimulation,
		stopSimulation,
		restoreSimulationData,
		getSimulationTextPartVariableNames,
		getSimulationActionVariableNames,
		getSimulationStepVariableNames,
		computeRemainingValues,
		convertRemainingInputs,
		simulationObject: simulationObject
	}
}
