const protocolCtrl = function (id_ld, isTeacher, isSuperManager, ldEdition, quantitiesData, simulationCode, simulationMaterial) {

    function init () {
		this.LD = JSON.parse(getLDContentFromLocalHistory(id_ld));
		this.undoRedoDone = false;
		if(this.LD.protocol.isSubStep === undefined) {
			this.LD.protocol.isSubStep = true;
		}
		if(this.LD.protocol.isInsideAction === undefined) {
			this.LD.protocol.isInsideAction = true;
		}
		this.LD.materialList.listItems
			.forEach((elt, idx) => elt.position = idx);
		this.LD.actionList.forEach((action, actionIdx) => {
			action.position = actionIdx;
			action.actionTextParts.forEach((actionTextPart, splitIdx) => {
				actionTextPart.position = splitIdx;
			});
		});
		this.currentMaxId = this.findCurrentMaxId();
		this.setProtocolAttributes();
		this.initOverallConcerns();
		this.quantitiesData.forEach(function (quantity, quantityIdx, quantityArray) {
			quantityArray[quantityIdx].unit = quantityArray[quantityIdx].unit.map((unit, index) => {
				return {
					value: index.toString(),
					id: index.toString(),
					symbol: unit.symbol,
					name: unit.symbol[i18next.language],
					label: unit.symbol[i18next.language],
					factor: unit.factor
				}
			})
		});
		if (this.simulationCode) {
			this.simulation = simulationCtrl(this, this.simulationCode, JSON.parse(this.simulationMaterial || '{}'));
			this.simulation.init();
		}
		this.quantitiesData.sort((a,b) => a.name[i18next.language].localeCompare(b.name[i18next.language]));
		this.history = [JSON.stringify(this.LD)];
		this.currentHistoryIndex = 0;
		this.userModif = true;
    }
	function extractModelChanges(oldLDModel, LDModel) {
		const isDeepEqual = (object1, object2) => {

			const objKeys1 = Object.keys(object1);
			const objKeys2 = Object.keys(object2);

			if (objKeys1.length !== objKeys2.length) return [object1, object2];

			let response = true;
			
			for (let key of objKeys1) {
				const value1 = object1[key];
				const value2 = object2[key];

				const isObjects = isObject(value1) && isObject(value2);
				if(isObjects) {
					response = isDeepEqual(value1, value2);
					if(response !== true) {
						break;
					}
				} else if(value1 !== value2) {
					response = [key, value1, value2, object1, object2];
					break;
				}
			}
			return response;
		};
		const isObject = (object) => {
			return object != null && typeof object === "object";
		};
		return isDeepEqual(JSON.parse(oldLDModel), JSON.parse(LDModel));
	}
	function updateUndoRedo(lastChange=false) {
		if(!this.upateHistoryTimeout && this.userModif) {
			let modelChanges = this.extractModelChanges(this.history[this.currentHistoryIndex], JSON.stringify(this.LD));
			if( modelChanges !== true
				&& (modelChanges.length !== 5 || !['iterationNumber',  'variableNumber'].includes(modelChanges[0]))) {
				if(!lastChange) {
					// Update changes with a timeout of 1 sec
					this.upateHistoryTimeout = true;
					setTimeout(()=> {
						this.upateHistoryTimeout=false;
						// Save last changes
						this.updateUndoRedo(true);
					},1000);
				}
				if(this.history.length>global_ld_max_local_versions) {
					this.history.shift();
				}
				this.history = this.history.slice(0,this.currentHistoryIndex+1);
				this.history.push(JSON.stringify(this.LD));
				this.currentHistoryIndex = this.history.length-1;
				window.dispatchEvent(new CustomEvent('procedure-update-undo-redo', {detail : {id_ld: this.id_ld, histo_index:this.currentHistoryIndex, histo_length:this.history.length}}));
			}
		}
	}
	function undoRedo(detail) {
		if(parseInt(detail.id_ld) === parseInt(this.id_ld)) {
			if(detail.undo) {
				this.currentHistoryIndex = Math.max(0,this.currentHistoryIndex-1);
			} else {
				this.currentHistoryIndex = Math.min(this.history.length-1,this.currentHistoryIndex+1);
			}
			const lastModel = this.history[this.currentHistoryIndex];
			// In order to not save the change when we restore the previous model version
			this.userModif = false;
			setTimeout(()=> {
				this.userModif=true;
			},1000);
			this.LD = null;
			this.LD = JSON.parse(lastModel);
			this.undoRedoDone = !this.undoRedoDone;
			window.dispatchEvent(new CustomEvent('prons-restore-action-img-height', {}));
			window.dispatchEvent(new CustomEvent('prons-update-object-clicked', {}));
			window.dispatchEvent(new CustomEvent('procedure-update-undo-redo', {detail : {id_ld: this.id_ld, histo_index:this.currentHistoryIndex, histo_length:this.history.length}}));
		}
	}
	
	function setProtocolAttributes (nodeArray=this.LD.protocol.nodes) {
		nodeArray.forEach((n, idx) => {
			n.position = idx;
			n.inEdition = false;
			if(n.nodeType === 'step') {
				n.isActive = false;
				if(n.isSubStep === undefined) {
					n.isSubStep = true;
				}
				if(n.isInsideAction === undefined) {
					n.isInsideAction = true;
				}
				if(n.iterationVariables.length > 0) {
					n.iterationNumber = !n.iterationNumber ? n.iterationVariables[0].values.length : n.iterationNumber;
					n.variableNumber = !n.variableNumber ? n.iterationVariables.length : n.variableNumber;
				} else {
					n.iterationNumber = !n.iterationNumber ? 1 : n.iterationNumber;
					n.variableNumber = !n.variableNumber ? 1 : n.variableNumber;
				}
			}
			if (n.nodes && n.nodes.length > 0) {
				this.setProtocolAttributes(n.nodes);
			}
		});
	}
	
	function initOverallConcerns () {
		this.LD.overallConcerns.forEach(elt => {
			if (elt.title_1 === null && elt.title_2 === null) {
				switch (elt.overallConcernsType) {
					case 'question':
						elt.title_1 = __('Question de recherche');
						elt.title_2 = __('Objectif');
						elt.selectedTitle = 'title_1';
						break;
					case 'hypothese':
						elt.title_1 = __('Hypothèses');
						elt.title_2 = __('Résultats attendus');
						elt.selectedTitle = 'title_2';
						break;
					case 'principle':
						elt.title_1 = __('Principe de la manipulation');
						elt.title_2 = "";
						elt.selectedTitle = 'title_1';
						break;
				}
			}
			if (elt.assignment === null) {
				switch (elt.overallConcernsType) {
					case 'question':
						elt.assignment = __('Décrivez l’objectif de votre expérimentation : la question à laquelle vous voulez répondre et/ou les objets que vous voulez produire.');
						break;
					case 'hypothese':
						elt.assignment = __('Lister les hypothèses que vous souhaitez tester au cours de votre expérimentation et/ou les résultats que vous pensez obtenir.');
						break;
					case 'principle':
						elt.assignment =  __('Décrivez rapidement la stratégie, les moyens que vous allez mettre en place. Le principe de manipulation ressemble à un mode opératoire succinct ne contenant pas les paramètres de la manipulation.');
						break;
				}
			}
		})
	}
	
	function sendContentBack(idLD) {
		if(idLD.toString() === this.id_ld.toString()) {
			window.dispatchEvent(new CustomEvent('procedure-content-sent', { detail: 
					{ 
						id_ld: this.id_ld, 
						ld_content: JSON.stringify(this.LD)
					} 
			}));
		}
	}
	function closeAllEditContent(nodeId=null) {
		window.dispatchEvent(new CustomEvent('close-edition', { detail: {id_node:nodeId}}));
	}
	function resizeInput() {
		window.dispatchEvent(new CustomEvent('resize-input', { detail: ''}));
	}
	
	function reInitializeSorting() {
		window.dispatchEvent(new CustomEvent('re-initialize-sorting', { detail: ''}));
	}
	function updatePositions (list) {
		list.sort((a, b) => a.position - b.position);
		for (let i = 0; i < list.length; i++) {
			list[i].position = i;
		}
	}
	function getTextWidth(domElt, desiredText=null) {
		const text = desiredText || domElt.innerText || domElt.value;
		const font= window.getComputedStyle(domElt, null).font;
		// re-use canvas object for better performance
		const canvas = getTextWidth.canvas || (getTextWidth.canvas = document.createElement("canvas"));
		const context = canvas.getContext("2d");
		context.font = font;
		const metrics = context.measureText(text);
		return metrics.width;
	}
	function displayAndFocusEditElement(el, editWidth, editHeight, offsetWidth, offsetHeight) {
		el.style.width = (editWidth + offsetWidth) + 'px';
		el.style.height = (editHeight + offsetHeight) + 'px';
		el.focus();
	}
	function findCurrentMaxId (maxId=0, nodeArray=this.LD.protocol.nodes) {
		if (!nodeArray) {
			return maxId;
		} else {
			let maxIdInGroup = (nodeArray || []).reduce((acc, cur) => Math.max(acc, cur.id), maxId);
			for (let n of nodeArray) {
				if (n.nodes) {
					maxIdInGroup = this.findCurrentMaxId(maxIdInGroup, n.nodes);
				}
			}
			return maxIdInGroup;
		}
	}
	function findNode (nodeId, node=this.LD.protocol) {
		if(nodeId === 'root') { return this.LD.protocol; }
		if (node.id === nodeId) {
			return node;
		} else {
			if (node.nodes) {
				let nFound;
				for (const n of node.nodes) {
					nFound = this.findNode(nodeId, n);
					if (nFound) {
						break;
					}
				}
				return nFound;
			}
		}
	}
	function findSiblingList(nodeId, node=this.LD.protocol) {
		if(node.nodes && node.nodes.find(n=>parseInt(n.id)===parseInt(nodeId))) {
			return node.nodes;
		} else {
			if(node.nodes) {
				let nFound;
				for( const n of node.nodes ) {
					nFound = this.findSiblingList(nodeId, n);
					if (nFound) {
						break;
					}
				}
				return nFound;
			}
		}
	}
	function findParentNode (nodeId, node=this.LD.protocol) {
		if(node.nodes && node.nodes.find(n=>parseInt(n.id)===parseInt(nodeId))) {
			return node;
		} else {
			if(node.nodes) {
				let nFound;
				for( const n of node.nodes ) {
					nFound = this.findParentNode(nodeId, n);
					if (nFound) {
						break;
					}
				}
				return nFound;
			}
		}
	}
	function findNodeByMaterialCreated(material, node=this.LD.protocol) {
		let structuredAction = node.nodeType === 'action'?this.LD.actionList.find(a => parseInt(node.idStructuredAction) === parseInt(a.id)):null;
		if(structuredAction
			&& structuredAction.actionTextParts.find(atp => atp.materialCreation 
				&& parseInt(node.parameterValues.find(p=> parseInt(p.parameterId) === parseInt(atp.id)).value) === parseInt(material.id) )
		){
			return node;
		} else {
			if(node.nodes) {
				let nFound;
				for( const n of node.nodes ) {
					nFound = this.findNodeByMaterialCreated(material, n);
					if (nFound) {
						break;
					}
				}
				return nFound;
			}
		}
	}
	
	function findPreviousNode(nodeId, node=this.LD.protocol) {
		if(node.nodes && node.nodes.findIndex(n=>parseInt(n.id)===parseInt(nodeId))) {
			const previousNodeIndex = node.nodes.findIndex(n=>parseInt(n.id)===parseInt(nodeId)) - 1;
			if(previousNodeIndex>=0) {
				return node.nodes[previousNodeIndex];
			} else {
				return null;
			}
		} else {
			if(node.nodes) {
				let nFound;
				for( const n of node.nodes ) {
					nFound = this.findPreviousNode(nodeId, n);
					if (nFound) {
						break;
					}
				}
				return nFound;
			}
		}
	}
	function findDepth (nodeId, node=this.LD.protocol, depth=0) {
		if(nodeId === 'root') { return 0; }
		if (node.id === nodeId) {
			return depth;
		} else {
			let newDepth = depth + 1;
			let depthFound;
			if (node.nodes) {
				for (let n of node.nodes) {
					depthFound = this.findDepth(nodeId, n, newDepth);
					if (depthFound) {
						break;
					}
				}
			}
			return depthFound;
		}
	}
	function findHeight(node=this.LD.protocol, height=0) {
		if (!node.nodes) {
			return height;
		} else {
			let newHeight = height + 1;
			if (node.nodes) {
				for (let n of node.nodes) {
					newHeight = Math.max(newHeight, this.findHeight(n, newHeight));
				}
			}
			return newHeight;
		}
	}
	function isSomeNodeInEdition(node=this.LD.protocol) {
		if(node.nodes && node.nodes.some(n=>n.inEdition)) {
			return true;
		} else {
			if(node.nodes) {
				let isNodeInEdition = false;
				for (let n of node.nodes) {
					isNodeInEdition = this.isSomeNodeInEdition(n);
					if(isNodeInEdition) {
						break;
					}
				}
				return isNodeInEdition;
			}
		}
	}
	function getParentStepIterationVariables(node, filterType=null, actionTextPart = null) {
		let parentNode = this.findParentNode(node.id);
		let iterationVariables = [];
		while(parentNode) {
			if(parentNode.nodeType && parentNode.nodeType === 'step' && parentNode.iterationVariables.length>0) {
				if(filterType === 'notMaterial') {
					iterationVariables = iterationVariables.concat(parentNode.iterationVariables.filter(variable => !variable.typeMaterial));
				} else if (filterType === 'material') {
					iterationVariables = iterationVariables.concat(parentNode.iterationVariables.filter(variable => variable.typeMaterial));
				} else if (filterType === 'physicalQuantity') {
					iterationVariables = iterationVariables.concat(parentNode.iterationVariables
						.filter(variable => !variable.typeMaterial && variable.unit && actionTextPart && actionTextPart.listId === variable.unit.listId));
				} else {
					iterationVariables = iterationVariables.concat(parentNode.iterationVariables);
				}
			}
			parentNode = this.findParentNode(parentNode.id);
		} 
		return iterationVariables;
	}
	function getAllMaterialCreatedAtThisStep (node, materialIterationVariables, materialsCreatedList = []) {
		if(node.nodes) {
			for(let n of node.nodes) {
				if(n.nodes) {
					this.getAllMaterialCreatedAtThisStep(n, materialIterationVariables, materialsCreatedList);
				} else {
					for( let iterationVariable of materialIterationVariables ) {
						let relatedAction = this.LD.actionList.find(a => parseInt(a.id) === parseInt(n.idStructuredAction));
						for ( let parameter of n.parameterValues) {
							if ( relatedAction.actionTextParts.find(atp => parseInt(atp.id) === parseInt(parameter.parameterId)).materialCreation 
								&& parameter.valueIterationVariable 
								&& parseInt(parameter.valueIterationVariable.id) === parseInt(iterationVariable.id)) {
								materialsCreatedList.push(iterationVariable.values);
							}
						}
					}
				}
			}
		}
		return materialsCreatedList.flatMap(v=>v);
	}
	function getAllMaterialsCreatedByIterativeStep(node, materialsCreatedList=[]) {
		if(node.nodeType === 'step' && node.iterationVariables.length>0) {
			let materialsCreatedAtThisStep = this.getAllMaterialCreatedAtThisStep(node,node.iterationVariables
				.filter(v => v.typeMaterial)
				.flatMap(v => {return {id:parseInt(v.id), values:v.values}}));
			materialsCreatedList.push(materialsCreatedAtThisStep);
		}
		if(node.nodes) {
			for(let n of node.nodes) {
				this.getAllMaterialsCreatedByIterativeStep(n, materialsCreatedList);
			}
		}
		return materialsCreatedList.flatMap(v=>v);
	}
	function retreiveParameterValues (node=this.copexObject.LD.protocol, currentProtocolActions=[]) {
		if (node.nodes) {
			node.iterationNumber = Math.max(1, node.iterationNumber || 0);
			for (let iterationIdx=0; iterationIdx < node.iterationNumber; iterationIdx++) {
				for (let n of node.nodes) {
					if (n.nodeType === 'action') {
						currentProtocolActions.push(this.extractParameterValues(n, node, iterationIdx));
					} else {
						this.retreiveParameterValues(n, currentProtocolActions);
					}
				}
			}
		}
		return currentProtocolActions;
	}
	function getObjectList (listId) {
		return this.LD.customLists
			.find(list => parseInt(list.id) === parseInt(listId));
	}
	
	function getObjectListItems (listId) {
		return this.getObjectList(listId).listItems;
	}
	
	function getUnitList(listId) {
		return listId ? this.quantitiesData.find(quantity => parseInt(quantity.id) === parseInt(listId)).unit : [];
	}
	
	function listFullUnit() {
		return this.quantitiesData
			.flatMap(quantity => quantity.unit
			.map((unit, index) => { return {
				listId: quantity.id.toString(),
				unitId: index.toString(),
				name: unit.symbol[i18next.language],
				factor: unit.factor
			}}));
	}
	
	function searchUnit(unitName) {
		unitName = unitName.replace(/\n|\s/gm,'');
		return this.listFullUnit().filter(u => u.name.includes(unitName));
	}
	
	function findUnit(unitName) {
		unitName = unitName.replace(/\n|\s/gm,'');
		return this.listFullUnit().find(u => u.name===unitName);
	}
	
	function getHTMLUnitName(unitName) {
		// Add a sup tag for number or -number in a unit name
		const pattern = /-?\d+/g;
		return unitName.replace(pattern, match => `<sup>${match}</sup>`);
	}

	function removeList (listId) {
		const idx = this.LD.customLists.findIndex(list => parseInt(list.id) === parseInt(listId));
		this.LD.customLists.splice(idx, idx >= 0 ? 1 : 0);
		return true;
	}
	
	function removeListItem (list, item) {
		const idx = list.findIndex(i => parseInt(i.id) === parseInt(item.id));
		list.splice(idx, idx >= 0 ? 1 : 0);
		return true;
	}

	function initializeSortingMaterialList (domElt) {
		if(this.ldEdition) {
			Sortable.create(domElt, {
				animation: 150,
				draggable: '.sortable-material-item',
				group: 'sortable-material',
				onUpdate: (event) => { this.sortUpdateMaterial(event); }
			});
		}
    }
	function sortUpdateMaterial (event) {
        this.LD.materialList.listItems
			.filter(material => !material.createdByProtocol)[event.oldDraggableIndex].position = event.newDraggableIndex - 0.5 + Number(event.oldDraggableIndex < event.newDraggableIndex);
        this.updatePositions(this.LD.materialList.listItems);
    }
	function initializeSortingStructuredActionList (domElt) {
		if(this.ldEdition) {
			Sortable.create(domElt, {
				animation: 150,
				draggable: '.sortable-structured-action-item',
				group: 'sortable-structured-action',
				onUpdate: (event) => { this.sortUpdateStructuredActionList(event); }
			});
		}
	}
	function sortUpdateStructuredActionList (event) {
		this.LD.actionList
			.find(a=>a.position===event.oldDraggableIndex)
			.position = event.newDraggableIndex - 0.5 + Number(event.oldDraggableIndex < event.newDraggableIndex);
		this.updatePositions(this.LD.actionList);
	}
	function isNodeDroppable(dragNode, targetNode) {
		return ( dragNode.nodeType === 'action' && targetNode.isInsideAction ) || ( dragNode.nodeType === 'step' && targetNode.isSubStep );
	} 
	/* Sortable object for protocol */
	function initializeSortingNodes (domElt) {
		if(this.ldEdition) {
			Sortable.create(domElt, {
				animation: 150,
				group: {
					name: 'nodes',
					put: (to, from, dragElt) => {
						const dragNode = this.findNode(dragElt.dataset.nodeId);
						const targetNode = this.findNode(to.el.dataset.nodeId);
						return dragElt.classList.contains('node-item') && (this.isTeacher || this.isNodeDroppable(dragNode, targetNode))
							&& (this.findHeight(dragNode) + this.findDepth(to.el.dataset.nodeId)) < 5;
					}
				},
				draggable: '.node-item',
				handle: '.node-handle',
				emptyInsertThreshold: 20, // Allow dropping into an empty list
				ghostClass: 'node-ghost',
				fallbackOnBody: true,
				swapThreshold: 1,
				invertSwap: true,
				onStart: (event) => {
					this.dragNode=true;
				},
				onUpdate: (event) => {
					this.sortUpdateProtocol(event);
				},
				onAdd: (event) => {
					this.sortAddProtocol(event);
				},
				onEnd: (event) => {
					let targetNode = this.findNode(event.item.dataset.nodeId);
					if(targetNode) {
						targetNode.inEdition = false;
					}
					this.dragNode=false;
				},
			});
		}
	}
	function sortUpdateProtocol(event) {
		let nodeList = this.findNode(event.from.dataset.nodeId).nodes;
		const oldi = event.oldDraggableIndex;
		const newi = event.newDraggableIndex;
		let nodeObject = nodeList[oldi];
		nodeObject.position = newi - 0.5 + Number(oldi < newi);
		this.updatePositions(nodeList);
		this.traceUserAction('student_move_' + nodeObject.nodeType, nodeObject);
	}
	function sortAddProtocol(event) {
		let newNodeList = this.findNode(event.to.dataset.nodeId).nodes;
		let oldNodeList = this.findNode(event.from.dataset.nodeId).nodes;
		const oldi = event.oldDraggableIndex;
		const newi = event.newDraggableIndex;
		let nodeObject = oldNodeList.splice(oldi,1);
		newNodeList.push(nodeObject[0]);
		newNodeList[newNodeList.length-1].position = newi - 0.5;
		this.updatePositions(newNodeList);
		this.updatePositions(oldNodeList);
		window.dispatchEvent(new CustomEvent('prons-update-action-text-part', {detail:{nodeId:event.item.dataset.nodeId}}));
		this.traceUserAction('student_move_' + nodeObject[0].nodeType, nodeObject[0]);
	}
	/* Material list functions */
	function getMaterialColumns (onlySelectedItems=false) {
		const materialList = this.LD.materialList.listItems
			.filter(material => 
				!material.createdByProtocol
				&& (!onlySelectedItems || material.name)
				&& (!onlySelectedItems || material.selected)
				&& !material.createdBySimulation)
			.sort((a,b)=>a.position-b.position);
		return materialList;
	}
	
	function setMaterialUsedInProtocol (nodeArray = this.LD.protocol.nodes) {
		nodeArray.filter(node => node.nodeType === 'action')
			.flatMap(a => a.parameterValues.flatMap(pm => { return { "idStructuredAction": a.idStructuredAction, "parameterId": pm.parameterId, "value": pm.value }; }))
			.map(paramValue => { return {
				"value": paramValue.value, 
				"parameter": this.LD.actionList
					.find(al => parseInt(al.id) === parseInt(paramValue.idStructuredAction)).actionTextParts
					.find(atp => parseInt(atp.id) === parseInt(paramValue.parameterId)) 
			}; })
			.filter(elt => elt.parameter && elt.parameter.isMaterialList && !isNaN(parseInt(elt.value)))
			.map(elt => parseInt(elt.value))
			.forEach(materialId => {
				this.LD.materialList.listItems.find(m => parseInt(m.id) === parseInt(materialId)).used = true;
			});
		if(!nodeArray.some(n => n.nodes)) {
			return true;
		} else {
			let isLeaf = false;
			for(let n of nodeArray) {
				if(n.nodes) {
					isLeaf = this.setMaterialUsedInProtocol(n.nodes);
					if(isLeaf) {
						break;
					}
				}
			}
			return isLeaf;
		}
	}
	function updateMaterialUsedInProtocol () {
		this.LD.materialList.listItems.forEach(material => {
			material.used = false;
		})
		this.setMaterialUsedInProtocol();
	}
	
	function removeMaterial (material) {
		if (material.used && !material.createdByProtocol && !material.createdBySimulation) {
			if (!confirm(__('Ce matériel est utilisé dans votre protocole. \nSouhaitez vous le supprimer ?'))) {
				return false;
			}
		}
		let materialList = this.LD.materialList.listItems;
		const indexMaterial = materialList.findIndex(mat => parseInt(mat.id) === parseInt(material.id));
		if(indexMaterial>=0) {
			window.dispatchEvent(new CustomEvent('prons-remove-selected-material-from-protocol', {detail:{materialId:material.id}}));
			materialList.splice(indexMaterial,1);
			this.updatePositions(materialList);
			return true;
		} else {
			return false;
		}
	}
	
	function isNewMaterial (searchText) {
		const materialList = this.LD.materialList.listItems.map(material => material.name.toLowerCase());
		return materialList.includes(searchText.toLowerCase());
	}
	function addMaterial (materialName='', types=[], isDeletable=true, createdByProtocol=false) {
		let materialList = this.LD.materialList.listItems;
		const ids = [...materialList.map(material => parseInt(material.id))];
		let id = ids.length === 0 ? 0 : (Math.max(...ids) + 1);
		let position =  Math.max(...materialList.map(material => material.position)) + 0.5;
		let material = {
			"id": id.toString(),
			"position": position,
			"name": materialName,
			"description": "",
			"quantity": null,
			"selected": true,
			"used": false,
			"types": types,
			"properties": [],
			"urlImg": null,
			isDeletable: isDeletable
		}
		if (createdByProtocol) {
			material.createdByProtocol = true;
		}
		materialList.push(material);
		this.updatePositions(materialList);
		this.traceUserAction('student_create_material', material);
		return material;
	}

	function duplicateMaterial (material) {
		let materialList = this.LD.materialList.listItems;
		const ids = [...materialList.map(material => parseInt(material.id))];
		let id = ids.length === 0 ? 0 : (Math.max(...ids) + 1);
		let duplicatedMaterial = JSON.parse(JSON.stringify(material));
		duplicatedMaterial.id = id.toString();
		materialList.push(duplicatedMaterial);
		this.updatePositions(materialList);
		return duplicatedMaterial;
	}

	function recreateMaterialInProtocol (node=this.LD.protocol) {
		if(node.nodes) {
			for( let n of node.nodes ) {
				if(n.nodeType === 'action') {
					const structuredAction = this.LD.actionList.find(a => parseInt(a.id) === parseInt(n.idStructuredAction));
					for ( let p of n.parameterValues ) {
						const actionTextPart = structuredAction.actionTextParts.find(atp => parseInt(atp.id) === parseInt(p.parameterId));
						if ( actionTextPart.materialCreation ) {
							let newMat = this.addMaterial('', actionTextPart.types, false, true);
							p.value = newMat.id;
						}
					}
				}
				if(n.nodes) {
					this.recreateMaterialInProtocol(n);
				}
			}
		}
	}
	function importMaterial (levelExercise) {
		this.removeAllNodeParameters();
		this.LD.materialList = this.simulation.simulationMaterial[levelExercise];
		this.recreateMaterialInProtocol();
	}
	
	function getLevelLabel(key) {
		switch(key) {
			case 'easy':
				return __('Niveau facile');
			case 'average':
				return __('Niveau intermédiaire');
			case 'advanced':
				return __('Niveau avancé');
			case 'expert':
				return __('Niveau expert');
		}
	}

	function addCustomList(newListName='') {
		let id = 0;
		if (this.LD.customLists.length > 0) {
			id = Math.max(...this.LD.customLists.map(i => parseInt(i.id))) + 1;
		}
		let list = 
			{
				id: id.toString(),
				listName: newListName,
				listItems: []
			};
		this.LD.customLists.push(list);
		return list;
	}
	function addItemToCustomList(listItems, itemName='') {
		let id = 0;
		if (listItems.length > 0) {
			id = Math.max(...listItems.map(i => parseInt(i.id))) + 1;
		}
		let item =
			{
				id: id.toString(),
				name: itemName
			};
		listItems.push(item);
	}

	function duplicateNode (node, updatePos=true, siblingList=null) {
		if(!siblingList) {
			siblingList = this.findSiblingList(node.id);
		}
		this.currentMaxId = parseInt(this.currentMaxId) + 1;
		let newId = this.currentMaxId.toString();
		let { nodes, parameterValues, iterationVariables, ...newNode } = node;
		newNode.isDuplicable = true;
		newNode.isDeletable = true;
		newNode.isMovable = true;
		newNode.isEditable = true;
		newNode.position = node.position + 0.5 * Number(updatePos);
		if(updatePos) {
			node.inEdition=false;
			newNode.inEdition=true;
		}
		newNode.id = newId;
		if(node.nodeType === 'action') {
			newNode.parameterValues = this.getDuplicateParameterValues(node);
		} else if(node.nodeType === 'step') {
			newNode.iterationVariables = JSON.parse(JSON.stringify(iterationVariables));
			newNode.isIterable = true;
		}
		if (node.nodes) {
			newNode.nodes = [];
			for (const n of node.nodes) {
				this.duplicateNode(n,false, newNode.nodes);
			}
		}
		siblingList.push(newNode);
		this.updatePositions(siblingList);
		this.traceUserAction('student_add_' + newNode.nodeType, newNode);
	}
	function getDuplicateParameterValues(node) {
		let parameterValues = [];
		const structuredAction = this.LD.actionList.find(a => parseInt(a.id) === parseInt(node.idStructuredAction));
		for(const structuredParameter of structuredAction.actionTextParts) {
			if(structuredParameter.domElement !== 'text') {
				if (structuredParameter.materialCreation) {
					const newMaterial = this.addMaterial('', structuredParameter.types, false, true);
					parameterValues.push({ parameterId:structuredParameter.id, value:newMaterial.id });
				} else {
					const duplicateParameter = node.parameterValues.find(p => parseInt(p.parameterId) === parseInt(structuredParameter.id));
					if(duplicateParameter) {
						parameterValues.push(JSON.parse(JSON.stringify(duplicateParameter)));
					}
				}
			}
		}
		return parameterValues;
	}
	function addNewNode(node, parentNode, addInside) {
		this.closeAllEditContent();
		let siblingList, position;
		if(!parentNode) {
			parentNode = this.LD.protocol;
			siblingList = parentNode.nodes;
			position = siblingList.length;
		} else {
			siblingList = addInside?parentNode.nodes:this.findParentNode(parentNode.id).nodes;
			position = addInside?siblingList.length:parentNode.position;
		}
		this.currentMaxId = this.currentMaxId + 1;
		node.id = this.currentMaxId.toString();
		node.inEdition = true;
		node.isEditable = true;
		node.isMovable = true;
		node.isDeletable = true;
		node.isDuplicable = true;
		node.position = addInside?position-0.5:position+0.5;
		if(node.nodeType === 'step') {
			node.iterationVariables = node.iterationVariables ? node.iterationVariables : [];
			node.iterationNumber = !node.iterationNumber ? 1 : node.iterationNumber;
			node.variableNumber = !node.variableNumber ? 1 : node.variableNumber;
			node.isIterable = true;
			node.isSubStep = true;
			node.isInsideAction = true;
			node.nodes = [];
		} else if(node.nodeType === 'action') {
			this.updateActionTextPartsInProtocol(node);
		}
		siblingList.splice(position,0,node);
		this.updatePositions(siblingList);
		this.traceUserAction('student_add_' + node.nodeType, node);
	}
	function isActionButton(node) {
		return this.isTeacher || (!node && this.LD.protocol.isInsideAction) || (node && node.isInsideAction);
	}
	function isStepButton(node) {
		return this.isTeacher || (!node && this.LD.protocol.isSubStep) || (node && node.isSubStep);
	}
	function updateParameterValue(parameter) {
		if(this.simulation) {
			if(this.isSomeNodeInEdition() && parameter.highlighted){
				parameter.highlighted = false;
			}
		}
	}
	function checkSwitchableNode(node) {
		if(node.nodeType==='action') {
			const structuredAction = this.LD.actionList.find(a=>parseInt(a.id)===parseInt(node.idStructuredAction));
			if(structuredAction && structuredAction.isFreeAction && this.findDepth(node.id) <= 3) {
				return true;
			}
		} else if(node.nodeType==='step') {
			if(node.iterationVariables.length===0 && node.nodes.length===0) {
				return true;
			}
		}
		return false;
	}
	
	function checkNewNodeButtons(node) {
		return !node || node.isActive || node.inEdition || (node.nodes && node.nodes.some(n => n.nodeType === 'action' && n.inEdition));
	}
	function switchStepAction(node) {
		if(node.nodeType === 'step'){
			node.nodeType = 'action';
			node.idStructuredAction = this.LD.actionList.find(a=>a.isFreeAction).id;
			this.updateActionTextPartsInProtocol(node);
			node.parameterValues[0].value = node.name;
		}else{
			node.nodeType = 'step';
			node.name = node.parameterValues[0].value;
			node.iterationVariables = [];
			node.nodes = [];
			node.iterationNumber = 1;
			node.variableNumber = 1;
		}
		this.traceUserAction('student_switch_to_' + node.nodeType , node);
	}
	
	function isFreeActionAllowed() {
		const freeAction = this.LD.actionList.find(a => a.isFreeAction);
		return freeAction && freeAction.isUsable;
	}
	
	function getReadOnlyTextValueIterationNumberSelect(iterationVariable) {
		return iterationVariable?(iterationVariable.name+(iterationVariable.unit?' ['+iterationVariable.unit.name+']':'')):'';
	}

	function getReadOnlyTextParameter (actionTextPart, node=null) {
		if(actionTextPart.domElement==='inputText') {
			return actionTextPart.nodeParameter.valueIterationVariable?actionTextPart.nodeParameter.valueIterationVariable.name:(actionTextPart.nodeParameter.value || '___________');
		} else if (actionTextPart.domElement === 'materialInputText') {
			return actionTextPart.nodeParameter.valueIterationVariable?actionTextPart.nodeParameter.valueIterationVariable.name:((actionTextPart.materialCreated && actionTextPart.materialCreated.name)?actionTextPart.materialCreated.name:'___________');
		} else if(actionTextPart.domElement === 'select') {
			return actionTextPart.nodeParameter.valueNameMaterialCreated?actionTextPart.nodeParameter.valueNameMaterialCreated:actionTextPart.nodeParameter.valueIterationVariable?actionTextPart.nodeParameter.valueIterationVariable.name:(this.getSelectValue(node, actionTextPart, 'select') || '___________');
		} else if(actionTextPart.domElement === 'numberSelect') {
			let value = actionTextPart.nodeParameter.valueIterationVariable?
				this.getReadOnlyTextValueIterationNumberSelect(actionTextPart.nodeParameter.valueIterationVariable)
				:(actionTextPart.nodeParameter.valueQuantity || '__');
			let unit = actionTextPart.nodeParameter.valueIterationVariable?''
				:this.getSelectValue(node, actionTextPart, 'numberSelect');
			return  value+ ' ' +(unit==='___________'?'__':unit);
		}
		
	}
	function removeMaterialCreatedByProtocol (node) {
		if (node.nodeType === 'action') {
			let structuredAction = this.LD.actionList.find(a => parseInt(a.id) === parseInt(node.idStructuredAction));
			if(structuredAction) {
				structuredAction.actionTextParts.filter(atp => atp.materialCreation)
					.forEach((atp) => {
						const idMaterial = node.parameterValues.find(p => parseInt(p.parameterId) === parseInt(atp.id)).value;
						this.removeMaterial(this.LD.materialList.listItems.find(m => parseInt(m.id) === parseInt(idMaterial)));
					});
			}
		}
		if(node.nodes) {
			for(let n of node.nodes) {
				this.removeMaterialCreatedByProtocol(n);
			}
		}
	}
	function removeAllNodeParameters (node=this.LD.protocol) {
		if(node.nodes) {
			for( let n of node.nodes ) {
				if(n.nodeType === 'action') {
					for ( let p of n.parameterValues ) {
						p.value = null;
						p.valueIterationVariable = null;
						p.valueNameMaterialCreated = null;
						p.valueUnit = null;
					}
				}
				if(n.nodes) {
					this.removeAllNodeParameters(n);
				}
			}
		}
	}
	function removeNode(node) {
		this.removeMaterialCreatedByProtocol(node);
		let siblingList = this.findSiblingList(node.id);
		const idx = siblingList.findIndex(n => parseInt(n.id) === parseInt(node.id));
		siblingList.splice(idx, 1);
		this.updatePositions(siblingList);
		this.traceUserAction('student_remove_' + node.nodeType, node);
	}
	function getSwitchText(node) {
		const type = node.nodeType==='step'?'action':'étape';
		return __('Transformer en '+type)
	}
	function updateIterationTable(node) {
		if(isNaN(parseInt(node.iterationNumber)) 
		|| isNaN(parseInt(node.variableNumber))) {
			return;
		}
		// Remove old variable
		let newVariableNumber = parseInt(node.iterationNumber)===1?0:parseInt(node.variableNumber);
		let newIterationNumber = parseInt(node.iterationNumber);
		if(newVariableNumber >= 0 && newIterationNumber >= 0) {
			for(let i=node.iterationVariables.length-1; i>=newVariableNumber;i--) {
				node.iterationVariables.pop();
			}
			for(let i=node.iterationVariables.length; i<newVariableNumber;i++) {
				node.iterationVariables.push(
					{
						id:node.iterationVariables.reduce((a,c)=>Math.max(a,parseInt(c.id)),0)+1,
						name:'', values:[], typeMaterial: false
					});
			}
			for(let iterVar of node.iterationVariables) {
				for(let i=iterVar.values.length-1; i>=newIterationNumber;i--) {
					iterVar.values.pop();
				}
			}
			for(let iterVar of node.iterationVariables) {
				for(let i=iterVar.values.length; i<newIterationNumber;i++) {
					iterVar.values.push('');
				}
			}
		}
		node.iterationNumber = newIterationNumber.toString();
		node.variableNumber = newVariableNumber.toString();
	}
	function isVariableUsedInProtocol(node, variable) {
		if(node.nodes) {
			for(let n of node.nodes) {
				if(n.nodeType === 'action' && n.parameterValues.some(p => p.valueIterationVariable && p.valueIterationVariable.id === variable.id)){
					return true;
				}
			}
			let returnValue = false;
			for(let n of node.nodes) {
				if(n.nodes) {
					returnValue = isVariableUsedInProtocol(n, variable);
					if(returnValue){break;}
				}
			}
			return returnValue;
		}
	}
	
	function resizeDiv() {
		window.dispatchEvent(new CustomEvent('prons-save-img-height',{}));
	}
	
	function getPositionFromRoot(node) {
		let positions = [node.position];
		let parent = this.findParentNode(node.id);
		while (parent) {
			positions.splice(0,0, parent.position);
			parent = this.findParentNode(parent.id);
		}
		return positions;
	}
	function isMaterialUsedAfterCreation(material, currentNode) {
		let creationNode = this.findNodeByMaterialCreated(material);
		if(creationNode) {
			let positionsCreationNode = this.getPositionFromRoot(creationNode);
			let positionsCurrentNode = this.getPositionFromRoot(currentNode);
			let i = 0;
			while(positionsCreationNode[i] === positionsCurrentNode[i] && i < Math.min(positionsCreationNode.length-1, positionsCurrentNode.length-1)) {
				i++;
			}
			return positionsCreationNode[i] < positionsCurrentNode[i];
		} else {
			return true;
		}
	}
	function getPreviouslyCreatedMaterials(parameter, node) {
		if (parameter.isMaterialList) {
			return this.LD.materialList.listItems
				.filter(item =>
					item.name
					&& item.createdByProtocol
					&& this.isMaterialUsedAfterCreation(item, node)
				)
				.map(option => { return {
					value: option.id,
					label: option.name
				}
			});
		} else {
			return [];
		}
	}
	function getPreviouslyCreatedMaterialsByIteration(node) {
		let materialsPreviouslyCreated= []; 
		let previousNode = this.findParentNode(node.id);
		if(!previousNode) {
			return materialsPreviouslyCreated;
		}else if(!previousNode.nodeType) {
			previousNode = node;
		} else {
			while (this.findParentNode(previousNode.id).nodeType) {
				previousNode = this.findParentNode(previousNode.id);
			}
		}
		while(this.findPreviousNode(previousNode.id)) {
			previousNode = this.findPreviousNode(previousNode.id);
			materialsPreviouslyCreated = materialsPreviouslyCreated.concat(this.getAllMaterialsCreatedByIterativeStep(previousNode));
		}
		return materialsPreviouslyCreated.filter((value, index, array) => array.indexOf(value) === index);
	}
	function getSelectOptions (parameter, node) {
		if (parameter.isMaterialList) {
			return this.LD.materialList.listItems
				.filter(item =>
					item.selected
					&& item.name
					&& !item.createdByProtocol
					&& (parameter.filterTypes === null 
						|| (item.types && item.types.some(e => parameter.filterTypes.map(f => f.name).includes(e.name))))
					&& this.isMaterialUsedAfterCreation(item, node)
				)
				.map(option => { return {
					value: option.id,
					label: option.name
				}
			});
		} else if(parameter.domElement === 'select' && this.LD.customLists.some(list => list.id === parameter.listId)) {
			return this.LD.customLists.find(list => parseInt(list.id) === parseInt(parameter.listId)).listItems
				.map(option => { return {
					value: option.id,
					label: option.name
				}
				});
		} else if (parameter.domElement === 'numberSelect' && this.quantitiesData.some(list => list.id === parameter.listId)) {
			return this.getUnitList(parameter.listId);
		}
		return [];
	}
	
	function getSelectValue (action, actionTextPart, selectType) {
		let itemList;
		let id;
		let item;
		let nodeParameter = actionTextPart.nodeParameter;
		if(!nodeParameter) {
			return '';
		}
		if (selectType === 'select' || selectType === 'numberSelect') {
			if(actionTextPart.isMaterialList) {
				itemList = this.LD.materialList.listItems;
			} else if(selectType === 'numberSelect'){
				itemList = actionTextPart.listId ? this.getUnitList(actionTextPart.listId) : [];
			} else if(selectType === 'select') {
				itemList = ( actionTextPart.listId && this.LD.customLists.some(l=>parseInt(l.id)===parseInt(actionTextPart.listId)) )? 
					this.getObjectListItems(actionTextPart.listId) : [];
			}
			if (selectType === 'select') {
				id = nodeParameter.value;
			} else if (selectType === 'numberSelect') {
				id = nodeParameter.valueUnit;
			}
			if (id && id.startsWith('iterative')) {
				return action.parameterValues.find(p => parseInt(p.parameterId) === parseInt(actionTextPart.id)).value.split('_')[1];
			}
			item = itemList.find(i => parseInt(i.id) === parseInt(id));
			if(actionTextPart.isMaterialList && item) {
				if(item.selected) {
					this.updateMaterialUsedInProtocol();
				}else {
					item = undefined;
				}
			}
			return (item === undefined) ? "___________" : item.name;
		}
	}
	function removeRelatedActionInProtocol (structuredActionId, node=this.LD.protocol) {
		if(node.nodes) {
			for(let i=node.nodes.length-1; i>=0; i--) {
				let n = node.nodes[i]; 
				if(n.nodeType === 'action' && parseInt(n.idStructuredAction) === parseInt(structuredActionId)) {
					this.removeNode(n);
				}
			}
			for(let i=node.nodes.length-1; i>=0; i--) {
				let n = node.nodes[i]; 
				if(n.nodes) {
					this.removeRelatedActionInProtocol(structuredActionId, n);
				}
			}
		}
	}
	function removeStructuredAction (structuredAction) {
		this.removeRelatedActionInProtocol(structuredAction.id);
		this.LD.actionList.splice(structuredAction.position, 1);
		this.updatePositions(this.LD.actionList);
		return true;
	}

	function addStructuredAction (newActionName = '') {
		let id = 0;
		let position = 0;
		if(this.LD.actionList.length > 0) {
			id = Math.max(...this.LD.actionList.map(act => parseInt(act.id))) + 1;
			position = Math.max(...this.LD.actionList.map(act => parseInt(act.position))) + 1;
		}
		let newAction = {
			id: id.toString(),
			position: position,
			name: newActionName,
			actionTextParts: [],
			comment: null,
			isUsable: true,
			simulationVariable: null,
		};
		newAction.actionTextParts.push({
			description: null,
			domElement: "text",
			filterTypes: null,
			id: '0',
			lengthMax: null,
			position: 0,
			value: ''
		});
		this.LD.actionList.push(newAction);
		return newAction;
	}
	
	function addElementToStructuredAction (structuredAction, elementType) {
		let id = 0;
		let position = 0;
		if(structuredAction.actionTextParts.length > 0) {
			id = Math.max(...structuredAction.actionTextParts.map(elt => parseInt(elt.id))) + 1;
			position = Math.max(...structuredAction.actionTextParts.map(elt => parseInt(elt.position))) + 1;
		}
		let actionTextPart = {};
		actionTextPart.id = id.toString();
		if(elementType === 'numberSelect') {
			actionTextPart.listId = null;
			actionTextPart.valueUnit = null;
			actionTextPart.valueQuantity = null;
		} else if (elementType === 'selectMaterial') {
			elementType = 'select';
			actionTextPart.isMaterialList = true;
		} else if (elementType === 'selectCustom') {
			elementType = 'select';
			actionTextPart.listId = null;
		} else if (elementType === 'materialInputText') {
			actionTextPart.types = [];
			actionTextPart.materialCreation = true;
		}
		elementType === 'text' ? actionTextPart.value = '': actionTextPart.value = null;
		actionTextPart.simulationVariable = null;
		actionTextPart.description = null;
		actionTextPart.filterTypes = null;
		actionTextPart.domElement = elementType;
		actionTextPart.lengthMax = actionTextPart.domElement==='inputText'?10:null;
		actionTextPart.position = position;
		structuredAction.actionTextParts.push(actionTextPart);
		this.updateTextParts(structuredAction);
		this.updateProtocolAction(structuredAction);
		return actionTextPart;
	}
	
	function removeActionTextPart (actionTextPart, structuredAction) {
		structuredAction.actionTextParts.splice(actionTextPart.position, 1);
		this.updateTextParts(structuredAction);
		this.updateProtocolAction(structuredAction);
		return true;
	}
	
	function updateActionTextPartsInProtocol (node) {
		const structuredAction = this.LD.actionList.find(a => parseInt(a.id) === parseInt(node.idStructuredAction));
		if(!node.parameterValues) {
			node.parameterValues = [];
		} else {
			// Remove deleted parameters
			for(let i=node.parameterValues.length-1; i>=0;i--) {
				if(!structuredAction.actionTextParts.some(atp => parseInt(atp.id) === parseInt(node.parameterValues[i].parameterId))) {
					node.parameterValues.splice(i,1);
				}
			}
		}
		// Add new parameters
		for(const structuredParameter of structuredAction.actionTextParts) {
			if( structuredParameter.domElement !== 'text' 
				&& !node.parameterValues.some(parameter => parseInt(parameter.parameterId) === parseInt(structuredParameter.id)) ) {
				if (structuredParameter.domElement === 'numberSelect') {
					node.parameterValues.push({parameterId:structuredParameter.id,valueQuantity: structuredParameter.valueQuantity,valueUnit: structuredParameter.valueUnit});
				} else {
					if (structuredParameter.materialCreation) {
						const newMaterial = this.addMaterial('', structuredParameter.types, false, true);
						node.parameterValues.push({ parameterId:structuredParameter.id, value:newMaterial.id });
					} else {
						node.parameterValues.push({ parameterId:structuredParameter.id, value:structuredParameter.value });
					}
				}
			}
		}
	}
	function updateProtocolAction ( structuredAction, node=this.LD.protocol ) {
		if(node.nodeType === 'action' && parseInt(node.idStructuredAction) === parseInt(structuredAction.id)) {
			this.updateActionTextPartsInProtocol(node);
		} else {
			if(node.nodes) {
				for(let n of node.nodes) {
					this.updateProtocolAction(structuredAction,n);
				}
			}
		}
	}
	function getActionTextParts(node) {
		if(!this.findNode(node.id)){
			return [];
		}
		let action = this.LD.actionList.find(a => parseInt(a.id) === parseInt(node.idStructuredAction));
		if(action) {
			// To duplicate step or action we need to force the creation of a new object
			let atps = JSON.parse(JSON.stringify(action.actionTextParts));
			return atps.map(actionTextPart => {
				if(actionTextPart.domElement !== 'text') {
					actionTextPart.nodeParameter = node.parameterValues.find(p => parseInt(p.parameterId) === parseInt(actionTextPart.id));
					if (actionTextPart.materialCreation) {
						actionTextPart.materialCreated = this.LD.materialList.listItems.find(m => parseInt(m.id) === parseInt(actionTextPart.nodeParameter.value));
					}
					if(actionTextPart.nodeParameter.valueIterationVariable) {
						actionTextPart.nodeParameter.valueIterationVariable = this.getParentStepIterationVariables(node).find(variable => parseInt(variable.id) === parseInt(actionTextPart.nodeParameter.valueIterationVariable.id));
					}
				}
				return actionTextPart;
			});
		} else {
			return [];
		}
	}
	function initializeSortingStructuredActionTextParts (domElt, structuredAction) {
		if(this.ldEdition) {
			return Sortable.create(domElt, {
				animation: 150,
				draggable: '.sortable-structured-action-text-part-item',
				handle: '.sortable-structured-action-text-part-handle',
				onStart: (event) => { this.prepareActionTextPartsBeforeDrag(event, structuredAction); },
				onEnd: (event) => { this.sortUpdateStructuredActionTextPart(event, structuredAction); },
			});
		} else {
			return null;
		}
	}
	
	function sortUpdateStructuredActionTextPart (event, structuredAction) {
		const newi = event.newDraggableIndex;
		const oldi = structuredAction.actionTextParts.findIndex(atp => atp.id === structuredAction.selectedDraggedTextPart.id);
		structuredAction.selectedDraggedTextPart.position = newi - 0.5 + Number(oldi < newi);
		this.updateTextParts(structuredAction);
	}
	
	function prepareActionTextPartsBeforeDrag(event, structuredAction) {
		let i = 0;
		let currNewId = Math.max(...structuredAction.actionTextParts.map(elt => parseInt(elt.id))) + 1;
		structuredAction.selectedDraggedTextPart = structuredAction.actionTextParts[event.oldDraggableIndex];
		while(i < structuredAction.actionTextParts.length) {
			if(structuredAction.actionTextParts[i].domElement === 'text' 
				&& structuredAction.actionTextParts[i].value.trim().split(' ').length>1) {
				let actionTextPart = structuredAction.actionTextParts.splice(i,1)[0];
				for(let part of actionTextPart.value.trim().split(' ')) {
					let newTextPart = {
						domElement: "text",
						id: currNewId.toString(),
						position: i - 0.5,
						value: part
					};
					structuredAction.actionTextParts.splice(i,0, newTextPart);
					this.updatePositions(structuredAction.actionTextParts);
					currNewId += 1;
					i+=1;
				}
			} else {
				i+=1;
			}
		}
	}
	function updateTextParts(structuredAction) {
		this.updatePositions(structuredAction.actionTextParts);
		let currTextPart = null;
		let removeValFromIndex = [];
		for(let i=0; i<structuredAction.actionTextParts.length; i++) {
			// Remove empty text
			if(structuredAction.actionTextParts[i].domElement === 'text' && !structuredAction.actionTextParts[i].value) {
				structuredAction.actionTextParts.splice(i,1);
			}
		}
		for(let i=0; i<structuredAction.actionTextParts.length; i++) {
			let atp = structuredAction.actionTextParts[i];
			// Merge all fixed text part
			if(atp.domElement === 'text' && currTextPart && i>0) {
				currTextPart.value += ' '+atp.value;
				removeValFromIndex.push(i);
			}
			if(atp.domElement === 'text' && !currTextPart) {
				currTextPart = atp;
			}
			if(atp.domElement !== 'text') {
				currTextPart = null;
			}
		}
		for (let i = removeValFromIndex.length -1; i >= 0; i--) {
			structuredAction.actionTextParts.splice(removeValFromIndex[i],1);
		}
		this.updatePositions(structuredAction.actionTextParts);
		currTextPart = null;
		let currNewId = Math.max(...structuredAction.actionTextParts.map(elt => parseInt(elt.id))) + 1;
		let addTextFromIndex = [];
		for(let i=0; i<structuredAction.actionTextParts.length; i++) {
			let atp = structuredAction.actionTextParts[i];
			if(i>0 && currTextPart && currTextPart.domElement !== 'text' && atp.domElement !== 'text') {
				addTextFromIndex.push(i);
			}
			currTextPart = atp;
		}
		for (let i = addTextFromIndex.length -1; i >= 0; i--) {
			let newTextPart = {
				description: null,
				domElement: "text",
				filterTypes: null,
				id: (currNewId+1).toString(),
				lengthMax: null,
				position: addTextFromIndex[i]-0.5,
				value: ''
			};
			currNewId += 1;
			structuredAction.actionTextParts.splice(addTextFromIndex[i],0, newTextPart);
		}
		// Add text part add start and end of the action on purpose
		if(structuredAction.actionTextParts.length>0) {
			if(structuredAction.actionTextParts[0].domElement !== 'text') {
				let newTextPart = {
					description: null,
					domElement: "text",
					filterTypes: null,
					id: (currNewId+1).toString(),
					lengthMax: null,
					position: -0.5,
					value: ''
				};
				currNewId += 1;
				structuredAction.actionTextParts.splice(0,0, newTextPart);
			}
			if(structuredAction.actionTextParts[structuredAction.actionTextParts.length-1].domElement !== 'text') {
				let newTextPart = {
					description: null,
					domElement: "text",
					filterTypes: null,
					id: (currNewId+1).toString(),
					lengthMax: null,
					position: structuredAction.actionTextParts.length,
					value: ''
				};
				structuredAction.actionTextParts.push(newTextPart);
			}
		}
		this.updatePositions(structuredAction.actionTextParts);
		structuredAction.update = !structuredAction.update;
	}
	function getPlaceholder(domElement, isMaterialList) {
		let placeholder;
		switch (domElement) {
			case 'inputText':
				placeholder =  __('Champ libre');
				break;
			case 'select':
				placeholder = isMaterialList?__('Matériel utilisé'):__('Item de liste');
				break;
			case 'numberSelect':
				placeholder = __('Grandeur');
				break;
			case 'materialInputText':
				placeholder = __('Matériel créé');
				break;
			default:
				placeholder =  '___________';
				break;
		}
		return placeholder;
	}
	
	function getOverallConcernLabelForTeacher (overallConcern) {
		let title = overallConcern.title_1 || '';
		if (overallConcern.title_2) {
			title += (title ? __(' ou ') : '') + overallConcern.title_2;
		}
		return title;
	}
	
	function getOverallConcernNextLabel (overallConcern) {
		if (overallConcern.title_1 && overallConcern.title_2) {
			if (overallConcern.selectedTitle === 'title_1') {
				return 'title_2';
			} else {
				return 'title_1';
			}
		} else {
			if (overallConcern.title_1) {
				return 'title_1';
			} else {
				return 'title_2';
			}
		}
	}

	
	function getAllMaterialTypes (excludedTypes) {
		let actionTypes = this.LD.actionList
				.filter(a => a.actionTextParts.some(atp => atp.materialCreation))
				.flatMap(a => a.actionTextParts.filter(atp => atp.materialCreation && atp.types.length >0 )
					.flatMap(atp => atp.types));
		let arrayOfTypes;
		arrayOfTypes = this.LD.materialList.listItems
			.reduce((carry, current) => carry.concat(current.types), []).concat(actionTypes);
		arrayOfTypes = arrayOfTypes.filter(
			(value, index, array) => 
				array.map(elt => elt.name).indexOf(value.name) === index 
				&& (!excludedTypes || !excludedTypes.map(elt => elt.name).includes(value.name))
		).sort((a,b) => a.name.localeCompare(b.name));
		return arrayOfTypes;
	}
	
	function addType (type, object, key) {
		if (type && type.trim() && (object[key] === null || !object[key].map(i => i.name.toLowerCase()).includes(type.toLowerCase()))) {
			object[key] = object[key] === null ? [] : object[key];
			const id = object[key].length === 0 ? "0" : (Math.max(...object[key].map(elt => parseInt(elt.id))) + 1).toString();
			object[key].push({id: id, name: type});
		}
	}
	
	function removeType (type, types) {
		const idx = types.findIndex(elt => elt.name === type);
		types.splice(idx, idx >= 0 ? 1 : 0);
		return true;
	}
	
	function initImageResizeEvent(handler, image) {
		document.addEventListener('mousedown', function(e) {
			this.currHandler = handler;
			this.currImage = image;
			// If mousedown event is fired from .handler, toggle flag to true
			if (e.target === this.currHandler) {
				this.isHandlerDragging = true;
				this.previousMouseY = e.clientY;
				document.body.style.cursor = 'row-resize';
			}
		});
		document.addEventListener('mousemove', function(e) {
			// Don't do anything if dragging flag is false
			if (!this.isHandlerDragging) {
				return false;
			}
			e.preventDefault();
			const offsetMouseY = this.previousMouseY-e.clientY;
			this.previousMouseY = e.clientY;
			this.currImage.style.height = this.currImage.offsetHeight-offsetMouseY+'px';
		});
		document.addEventListener('mouseup', function(e) {
			// Turn off dragging flag when user mouse is up
			this.isHandlerDragging = false;
			document.body.style.cursor = 'auto';
		});
	}

	function setEndOfContenteditable(contentEditableElement)
	{
		let range,selection;
		if(document.createRange)//Firefox, Chrome, Opera, Safari, IE 9+
		{
			range = document.createRange();//Create a range (a range is a like the selection but invisible)
			range.selectNodeContents(contentEditableElement);//Select the entire contents of the element with the range
			range.collapse(false);//collapse the range to the end point. false means collapse to end rather than the start
			selection = window.getSelection();//get the selection object (allows you to change selection)
			selection.removeAllRanges();//remove any selections already made
			selection.addRange(range);//make the range you have just created the visible selection
		}
		else if(document.selection)//IE 8 and lower
		{
			range = document.body.createTextRange();//Create a range (a range is a like the selection but invisible)
			range.moveToElementText(contentEditableElement);//Select the entire contents of the element with the range
			range.collapse(false);//collapse the range to the end point. false means collapse to end rather than the start
			range.select();//Select the range (make it the visible selection
		}
	}
	
	function removeImage (object) {
		object.urlImg = null;
		window.dispatchEvent(new CustomEvent('prons-clean-material-details',{}));
		window.dispatchEvent(new CustomEvent('prons-restore-action-img-height',{}));
		return true
	}
	
	function getSimulationTextPartVariableNames () {
		return this.simulation.simulatedActivity.getSimulationTextPartVariableNames();
	}
	
	function getSimulationActionVariableNames () {
		return this.simulation.simulatedActivity.getSimulationActionVariableNames();
	}
	function getSimulationStepVariableNames () {
		return this.simulation.simulatedActivity.getSimulationStepVariableNames();
	}
	
	function isSimulationVariableUsedinList (name, flatList) {
			return flatList.some(elt => elt.simulationVariable === name);
	}
	
	function isSimulationVariableUsedInProtocol (name, node=this.LD.protocol) {
		if (node.nodes) {
			if (node.nodes.some(n => n.simulationVariable === name)) {
				return true;
			} else {
				let isVariableUsed = false;
				for (let n of node.nodes) {
					isVariableUsed = this.isSimulationVariableUsedInProtocol(name, n);
					if (isVariableUsed) {
						break;
					}
				}
				return isVariableUsed;
			}
		} else {
			return false;
		}
	}
	
	function getMaterialProperties (materialObject) {
		return materialObject.properties.sort((a, b) => a.id - b.id);
	}

	function addPropertyToMaterial (materialObject, property={ name: null, physicalQuantityId: null, value: null, unitId: null, hidden: false } ) {
		const ids = [...materialObject.properties.map(property => parseInt(property.id))];
		let id = ids.length === 0 ? 0 : (Math.max(...ids) + 1);
		property.id = id.toString();
		materialObject.properties.push(property);
	}
	
	function getPhysicalQuantityName (physicalQuantityId) {
		return physicalQuantityId ?
			'(' + this.quantitiesData
				.find(quantity => parseInt(quantity.id) === parseInt(physicalQuantityId))
				.name[i18next.language] + ')'
			: '';
	}
	
	function getPhysicalUnitLabel (property) {
		return (property.physicalQuantityId && property.unitId) ?
			this.getUnitList(property.physicalQuantityId)
				.find(unit => parseInt(unit.id) === parseInt(property.unitId))
				.label
			: '';
	}
	
	function traceUserAction (traceAction, node)
	{
		if (this.simulation && !this.isTeacher) {
			this.simulation.traceUserAction(traceAction, node);
		}
	}
	
	function saveCurrentNodeEditedState (node) {
		if (this.simulation && !this.isTeacher) {
			let {inEdition, ...nodeToSave} = node; 
			this.simulation.currentEditedNode = JSON.stringify(nodeToSave);
		}
	}

	function nameAlreadyExists (element, list, key='name') {
		return list.length > 0 
			&& list
				.filter(elt => parseInt(elt.id) !== parseInt(element.id))
				.map(elt => elt[key].toLowerCase())
				.includes(element[key].toLowerCase());
	}
	
	function selectProtocolMaterialOption (actionTextPart, event) {
		switch(event.target.options[event.target.selectedIndex].dataset.materialOrigin) {
			case 'materialList':
				actionTextPart.nodeParameter.valueIterationVariable = null; 
				actionTextPart.nodeParameter.valueNameMaterialCreated = null;
				break;
			case 'iterativeVariable':
				actionTextPart.nodeParameter.value = null;
				actionTextPart.nodeParameter.valueNameMaterialCreated = null; 
				actionTextPart.nodeParameter.valueIterationVariable = event.target.options[event.target.selectedIndex].variable;
				break;
			case 'materialCreated':
				actionTextPart.nodeParameter.value = null;
				actionTextPart.nodeParameter.valueIterationVariable = null; 
				actionTextPart.nodeParameter.valueNameMaterialCreated = event.target.options[event.target.selectedIndex].label;
				break;
			case 'none':
				actionTextPart.nodeParameter.value = null;
				actionTextPart.nodeParameter.valueIterationVariable = null; 
				actionTextPart.nodeParameter.valueNameMaterialCreated = null;
				break;
		}
	}

    return {
        init,
		updateUndoRedo,
		extractModelChanges,
		undoRedo,
		sendContentBack,
		closeAllEditContent,
		resizeInput,
		reInitializeSorting,
		findCurrentMaxId,
        setProtocolAttributes,
		initOverallConcerns,
		findNode,
		getTextWidth,
		displayAndFocusEditElement,
		getObjectList,
		getObjectListItems,
		getUnitList,
		listFullUnit,
		searchUnit,
		findUnit,
		getHTMLUnitName,
		removeList,
		removeListItem,
		getParentStepIterationVariables,
		getAllMaterialCreatedAtThisStep,
		getAllMaterialsCreatedByIterativeStep,
		findSiblingList,
		findParentNode,
		getPositionFromRoot,
		findNodeByMaterialCreated,
		findPreviousNode,
        findDepth,
        findHeight,
		isSomeNodeInEdition,
		initializeSortingMaterialList,
		sortUpdateMaterial,
		initializeSortingStructuredActionList,
		sortUpdateStructuredActionList,
		sortUpdateProtocol,
		sortAddProtocol,
		isNodeDroppable,
		initializeSortingNodes,
		isMaterialUsedAfterCreation,
		getPreviouslyCreatedMaterials,
		getPreviouslyCreatedMaterialsByIteration,
		getSelectOptions,
		getSelectValue,
		getMaterialColumns,
		updateParameterValue,
		getDuplicateParameterValues,
		duplicateNode,
		addNewNode,
		isActionButton,
		isStepButton,
		resizeDiv,
		switchStepAction,
		isFreeActionAllowed,
		checkSwitchableNode,
		checkNewNodeButtons,
		removeAllNodeParameters,
		removeMaterialCreatedByProtocol,
		removeNode,
		getReadOnlyTextValueIterationNumberSelect,
		getReadOnlyTextParameter,
		updateIterationTable,
		isVariableUsedInProtocol,
		getSwitchText,
		removeMaterial,
		setMaterialUsedInProtocol,
		updateMaterialUsedInProtocol,
		isNewMaterial,
		addMaterial,
		duplicateMaterial,
		recreateMaterialInProtocol,
		importMaterial,
		getLevelLabel,
		addCustomList,
		addItemToCustomList,
		updatePositions,
		removeRelatedActionInProtocol,
		removeStructuredAction,
		addStructuredAction,
		addElementToStructuredAction,
		removeActionTextPart,
		updateActionTextPartsInProtocol,
		updateProtocolAction,
		getActionTextParts,
		initializeSortingStructuredActionTextParts,
		sortUpdateStructuredActionTextPart,
		prepareActionTextPartsBeforeDrag,
		updateTextParts,
		getPlaceholder,
		getOverallConcernLabelForTeacher,
		getOverallConcernNextLabel,
		getAllMaterialTypes,
		addType,
		removeType,
		setEndOfContenteditable,
		initImageResizeEvent,
		removeImage,
		getSimulationTextPartVariableNames,
		getSimulationActionVariableNames,
		getSimulationStepVariableNames,
		isSimulationVariableUsedinList,
		isSimulationVariableUsedInProtocol,
		getMaterialProperties,
		addPropertyToMaterial,
		getPhysicalQuantityName,
		getPhysicalUnitLabel,
		traceUserAction,
		saveCurrentNodeEditedState,
		simulationMaterial: simulationMaterial,
		nameAlreadyExists,
		selectProtocolMaterialOption,
        LD: null,
		sortableStepsGroup: [],
		sortableActionsGroup: [],
		sortableMaterial:null,
		quantitiesData: quantitiesData,
        id_ld: id_ld,
        ldEdition: ldEdition===1,
		isTeacher: isTeacher===1,
		isSuperManager: isSuperManager===1,
		simulationCode: simulationCode,
		constraints: [],
		taskTypes: [],
		feedbacks: [],
		constraintsHaveChanged: false,
		taskTypesHaveChanged: false,
		feedbacksHaveChanged: false,
		currentMaxId: 0
    }
};
