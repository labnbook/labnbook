const simulationCtrl = function (copexObject, simulationCode, simulationMaterial) {

	function init() {
		// Define the specific activity
		if (this.simulationCode === 'titrab') {
			this.simulatedActivity = titrabCtrl(this);
		} else if (this.simulationCode === 'dosage-spectro') {
			this.simulatedActivity = dosageSpectroCtrl(this);
		}
		if (this.simulatedActivity) {
			this.simulatedActivity.init();
		}
		// log simulation trace
		if (this.copexObject.ldEdition) {
			this.traceUserInit();
		}
	}
	
	function initSimulation () {
		if(!this.copexObject.isTeacher) {
			// Define containers
			this.feedbacksDiv = document.getElementById(this.copexObject.id_ld+"_simulation_feedbacks");
			this.ldContentDiv = document.getElementById('labdoc_content_' + this.copexObject.id_ld);
			this.simulationDisplayDiv = document.getElementById(this.copexObject.id_ld+"_simulation_display_div");
			// Set the simulation canvas parameters, callbacks and events
			this.initSimulationDrawing();
			// Remove the highlighting for all the action parameters
			this.removeHighlighting();
		}
	}

	function initSimulationDrawing () {
		this.canvas = document.getElementById(this.copexObject.id_ld+"_simulation_canvas");
		this.canvas.width = this.canvas.offsetWidth;
		this.canvas.height = this.canvas.offsetHeight;
		new ResizeObserver((entries) => {
			this.canvas.width = entries[0].target.offsetWidth;
			this.canvas.height = entries[0].target.offsetHeight;
			if(!this.isRunning) {
				this.simulatedActivity.drawSimulation();
			}
		}).observe(this.canvas);
		this.ldContentDiv.addEventListener('input', (event)=> {
			this.simulationPaused = true;
			if(this.isRunning) {
				this.simulatedActivity.stopSimulation();
				this.simulatedActivity.drawSimulation();
			}
		});
		this.ldContentDiv.addEventListener('change', (event)=> {
			this.simulationPaused = true;
			if(this.isRunning) {
				this.simulatedActivity.stopSimulation();
				this.simulatedActivity.drawSimulation();
			}
		});
		this.simulatedActivity.initSimulationDrawing();
	}

	async function checkProtocol (onlySimulation, lastActionId=null) {
		this.copexObject.closeAllEditContent();
		this.removeHighlighting();
		this.protocol = this.retreiveParameterValues();
		let variableList;
		if (lastActionId) {
			// Slice protocol to the selected action ID in order to simulate only a part of the protocol
			let lastIndex = this.protocol.findLastIndex(a => parseInt(a.nodeId) === parseInt(lastActionId));
			this.protocol = this.protocol.slice(0, lastIndex + 1);
			variableList = this.simulatedActivity.getSimulationTextPartVariableNames().filter(
				v => this.protocol.flatMap(a => Object.keys(a)).includes(v)
			)
		}
		// Handle undefined variables
		if (this.checkUndefinedVariables(this.getUndefinedVariables())) {
			this.removeMaterialCreatedBySimulation();
			return null;
		}
		this.simulatedActivity.setSpecificVariables();
		this.checkProtocolInProcess = true;
		this.simulatedActivity.cleanSimulation();
		this.resetFeedbacks();
		// Send to back-end
		let self = this;
		[...document.getElementsByClassName('labdoc')].forEach(
			(el)=> {
				if(this.replaceLdResponse && el.children[0] && el.children[0].getAttribute('ld_type') === 'text' && el.innerHTML.includes('verificationOfTheRequiredConcentration')) {
					el.remove();
				} else if(this.replaceLdData && el.children[0] && el.children[0].getAttribute('ld_type') === 'dataset') {
					el.remove();
				}
			})
		await $.ajax({
			type: "POST",
			url: "/simulation/checkProtocolAndGetFeedback",
			datatype: "json",
			data: {
				id_labdoc: self.copexObject.id_ld,
				isLdInEdition: self.copexObject.ldEdition?1:0,
				onlySimulation: onlySimulation?1:0,
				protocol: JSON.stringify(self.protocol),
				states: self.constraintStates,
				materialList: JSON.stringify(self.copexObject.LD.materialList.listItems),
				previous_error: self.previous_error ? self.previous_error.id_constraint : null,
				base_unit: self.simulatedActivity.baseUnits,
				scope: window.global_scope,
				quantities: JSON.stringify(self.copexObject.quantitiesData),
				variableList: variableList,
				replaceLdResponse : self.replaceLdResponse?1:0,
				replaceLdData : self.replaceLdData?1:0
			},
			error: function (jqXHR, textStatus) {
				self.checkProtocolInProcess = false;
				alertGently(__("Une erreur est survenue : ") + textStatus, 'danger');
				self.removeMaterialCreatedBySimulation();
			},
			success: function (responseData) {
				self.checkProtocolInProcess = false;
				self.lastProtocolChecked = JSON.stringify(self.cleanProtocolInEdition());
				let isSameError = (!self.isPreviousSimulation && self.previous_error && responseData.previous_error) ? parseInt(self.previous_error.id_constraint) === parseInt(responseData.previous_error.id_constraint) : false;
				self.previous_error = responseData.previous_error;
				self.simulation_enabled = responseData.simulation_enabled;
				self.simulatedActivity.simulationData = responseData.simulation_data;
				self.constraintStates = responseData.constraintStates;
				self.skillsChanges = responseData.skillsChanges;
				if(onlySimulation) {
					self.feedbackSimulation = self.simulation_enabled?
						__('Le protocole actuel permet de simuler les résultats ci-dessous.'):
						__('Le protocole actuel ne permet pas de simuler de résultats.');
					if(self.simulation_enabled && self.previous_error) {
						self.feedbackSimulation += __('Des erreurs dans le protocole empêchent la simulation de certaines mesures.');
					}
				} else {
					self.setFeedbackError(responseData.feedback, isSameError);
					self.setFeedbackIndications(responseData.feedback);
					self.highlightProtocol(responseData.feedback);
				}
				self.displayGlobalKnowledgesEvolution(responseData.globalKnowledges);
				self.progression = responseData.progression;
				self.datasetView = responseData.datasetView;
				self.datasetAssignment = responseData.datasetAssignment;
				self.responseView = responseData.responseView;
				self.responseAssignment = responseData.responseAssignment;
				self.datasetData = responseData.datasetData;
				self.responseData = responseData.responseData;
				self.idDataset = responseData.idDataset;
				self.idResponse = responseData.idResponse;
				self.idReportPart = responseData.idReportPart;
				self.consoleLogChanges(self.constraintStates, self.skillsChanges);
			}
		});
		this.removeMaterialCreatedBySimulation();
		if ((onlySimulation && this.simulation_enabled) || !this.previous_error) {
			this.simulationPaused=false;
			this.isRunning = true;
			await this.simulatedActivity.runSimulation();
			this.simulatedActivity.displayFeedbackAfterSimulation();
			this.simulatedActivity.stopSimulation();
			this.insertLd(this.idDataset, this.datasetAssignment, this.datasetView, this.datasetData, 'dataset');
			this.insertLd(this.idResponse, this.responseAssignment, this.responseView, this.responseData, 'text');
		} else {
			if(this.simulation_enabled) {
				this.feedbackSimulationEnabled = __('Votre protocole permet de simuler des mesures. Vous pouvez lancer la simulation.');
			}
		}
		this.isPreviousSimulation = onlySimulation;
	}
	
	function synchronizeProtocol() {
		if(!this.copexObject.ldEdition) {
			this.resetFeedbacks();
			let self = this;
			$.ajax({
				type: "GET",
				url: "/simulation/getSynchronizeData",
				datatype: "json",
				data: {
					idLd: self.copexObject.id_ld,
					simulationCode: self.simulationCode
				}
			}).done((responseData) => {
				self.progression = responseData.progression;
				self.failedConstraints = responseData.failedConstraints;
				self.displayGlobalKnowledgesEvolution(responseData.globalKnowledges);
			}).fail();
		}	
	}
	
	function resetFeedbacks() {
		this.feedbackError = '';
		this.feedbackIndications = '';
		this.feedbackSimulationEnabled = '';
		this.feedbackSimulation = '';
	}
	
	function setFeedbackError(feedbackData, isSameError) {
		this.feedbackError = '';
		if(feedbackData.message_parts) {
			feedbackData.message_parts = feedbackData.message_parts.filter(m => m!=='');
			if(feedbackData.message_parts.length>0) {
				if (isSameError) {
					this.feedbackError += __(" (identique à la précédente)");
				}
				this.feedbackError += ' : ' + feedbackData.message_parts[0];
			}
		}
	}
	
	function setFeedbackIndications(feedbackData) {
		this.feedbackIndications = '';
		if(feedbackData.message_parts) {
			feedbackData.message_parts = feedbackData.message_parts.filter(m => m!=='');
			if(feedbackData.message_parts.length>1){
				this.feedbackIndications = feedbackData.message_parts.slice(1).join(' - ');
			}
		}
	}
	
	function highlightProtocol(feedbackData) {
		let highlighted = feedbackData.highlighted ? feedbackData.highlighted : null;
		if(highlighted && highlighted.simulationVariables) {
			for (let simulationVariable of highlighted.simulationVariables) {
				if (highlighted.nodesId) {
					for (let highlightedNodeId of highlighted.nodesId) {
						let action = this.protocol.find(node => parseInt(node.nodeId) === parseInt(highlightedNodeId));
						if(action) {
							let parameter = action.relatedIndexes.find(i => i.simulationVariable === simulationVariable);
							if (parameter) {
								let parameterId = parameter.index;
								this.copexObject.findNode(action.nodeId).parameterValues.at(parameterId).highlighted = true;
							}
						}
					}
				}
			}
		}
	}

	function displayGlobalKnowledgesEvolution(globalKnowledges) {
		this.globalKnowledges = globalKnowledges.map(globalKnowledge => {
			globalKnowledge.evolution = globalKnowledge.evolution?(globalKnowledge.evolution>0?'up':'down'):'steady';
			if(globalKnowledge.relatedTaskTypesKnowledge) {
				globalKnowledge.title = globalKnowledge.relatedTaskTypesKnowledge.map((tt) => {
					return tt.description + ' ' + (tt.knowledge!==null?Math.round(globalKnowledge.knowledge*100):'-')+'%';
				}).join(', ');
			} else {
				globalKnowledge.title = (globalKnowledge.knowledge!==null?Math.round(globalKnowledge.knowledge*100):'-')+'%';
			}
			return globalKnowledge;
		});
	}
	
	function insertLd (idLd, ldAssignment, ldView, ldData, ldType) {
		if ((ldType === 'text' && this.replaceLdResponse) || (ldType === 'dataset' && this.replaceLdData)) {
			let ldDiv = document.getElementById("labdoc_" + idLd);
			let reportPartList = document.getElementById("rp_sortable_" + this.idReportPart);
			if (ldDiv) {
				ldDiv.innerHTML = ldView;
			} else {
				ldDiv = document.createElement('div');
				ldDiv.id = 'labdoc_' + idLd;
				ldDiv.classList.add('labdoc');
				ldDiv.dataset.id = idLd;
				ldDiv.style.height = 'auto';
				ldDiv.innerHTML = ldView;
				reportPartList.appendChild(ldDiv);
			}
			global_tab_ld[idLd] = {
				current_i: 0,
				ld_assignment_history: [ldAssignment || ''],
				ld_history: [ldData],
				ld_type: ldType,
				recovering: false,
				send_in_db: false,
				ld_content: ldData
			}
			if (ldAssignment) {
				document.getElementById('labdoc_assignment_' + idLd).innerHTML = ldAssignment || '';
				document.getElementById('labdoc_assignment_container_' + idLd).style.display = 'block';
			}
			showHideLD(idLd, 'show');
			if (ldType === 'dataset') {
				displayDataSet(idLd, false);
			} else if (ldType === 'text') {
				displayText(idLd, false);
			}
			ldDiv.classList.remove('labdoc-content-loading');
		}
	}
	
	function retreiveParameterValues (node=this.copexObject.LD.protocol, currentProtocolActions=[]) {
		if (node.nodes) {
			node.iterationNumber = Math.max(1, node.iterationNumber || 0);
			for (let iterationIdx=0; iterationIdx < node.iterationNumber; iterationIdx++) {
				for (let n of node.nodes) {
					if (n.nodeType === 'action') {
						currentProtocolActions.push(this.extractParameterValues(n, node, iterationIdx));
					} else {
						this.retreiveParameterValues(n, currentProtocolActions);
					}
				}
			}
		}
		return currentProtocolActions;
	}
	function findClosestSimulationVariable(node) {
		if(node.simulationVariable) {
			return node.simulationVariable;
		} else {
			let parentNode = this.copexObject.findParentNode(node.id);
			while(parentNode) {
				if(parentNode.simulationVariable) {
					return parentNode.simulationVariable;
				} else {
					parentNode = this.copexObject.findParentNode(parentNode.id);
				}
			}
		}
	}
	function extractParameterValues(node, parentNode, iterationIdx) {
		let structuredAction = this.copexObject.LD.actionList.find(a => parseInt(a.id) === parseInt(node.idStructuredAction));
		let simulationVariables = { 
			"structuredAction": structuredAction.simulationVariable, 
			"stepId": parentNode.id || 'root', 
			"stepName": parentNode.name,
			"stepSimulationVariable": this.findClosestSimulationVariable(parentNode),
			"nodeId": node.id, 
			"relatedIndexes" : [],
			"position": node.position,
			"textparts": this.getTextPartsForTrace(node)
		};
		let parameterIndex = 0;
		for (let parameter of node.parameterValues) {
			let actionTextPart = structuredAction.actionTextParts.find(atp => parseInt(atp.id) === parseInt(parameter.parameterId));
			simulationVariables[actionTextPart.simulationVariable] = this.getObjectFromParameter(actionTextPart, parameter, node, iterationIdx);
			simulationVariables.relatedIndexes.push({
				"simulationVariable": actionTextPart.simulationVariable,
				"index": parameterIndex
			});
			// Convert quantities to baseUnit
			if (actionTextPart.domElement === 'numberSelect') {
				simulationVariables[actionTextPart.simulationVariable][0] = this.unitConvertToBaseUnit(simulationVariables[actionTextPart.simulationVariable][0], simulationVariables[actionTextPart.simulationVariable][1]);
				if (this.copexObject.quantitiesData) {
					let quantityObject = this.copexObject.quantitiesData
						.find(quantity => quantity.unit.some(unit => Object.values(unit.symbol).includes(simulationVariables[actionTextPart.simulationVariable][1])));
					if (quantityObject) {
						simulationVariables[actionTextPart.simulationVariable][1] = this.simulatedActivity.baseUnits[quantityObject.name["en"]];
					}
				}
			}
			simulationVariables = this.simulatedActivity.convertRemainingInputs(simulationVariables, actionTextPart);
			parameterIndex+=1;
		}
		simulationVariables = this.simulatedActivity.computeRemainingValues(simulationVariables, structuredAction);
		return simulationVariables;
	}
	
	function removeMaterialCreatedBySimulation() {
		for (let material of this.copexObject.LD.materialList.listItems.filter(m => m.createdBySimulation)) {
			this.copexObject.removeMaterial(material);
		}
	}

	function getUndefinedVariables () {
		return this.protocol.flatMap(node => Object.keys(node).map(key => { return { key: key, value: node[key] } })
			.filter(variable => !['stepName','stepSimulationVariable','position', 'relatedIndexes', 'textparts'].includes(variable.key) 
				&& (!variable.value
					|| variable.value.materialNotCreated
					|| variable.value.materialUsedBeforeCreation
					|| (Array.isArray(variable.value) && (isNaN(variable.value[0]) || !variable.value[1]))
					|| (variable.value.name !== undefined && variable.value.name === '') )
			)
			.map(variable => { return { action: node.structuredAction, variable: variable.key,
				materialNotCreated: variable.value?(variable.value.materialNotCreated || null):null,
				materialUsedBeforeCreation: variable.value?(variable.value.materialUsedBeforeCreation || null):null} }));
	}
	
	function checkUndefinedVariables(undefinedVariables) {
		let isUndefinedVariables = false;
		// Alert gently when the name of a material is not found in the material list or the material is used before is creation
		if(undefinedVariables.some(undefinedVariable => undefinedVariable.materialNotCreated)) {
			if(this.copexObject.ldEdition) {
				alertGently(__('Au moins une action utilise un matériel qui n\'existe pas. Le protocole n\'est pas évaluable.'), 'warning');
			}
			isUndefinedVariables = true;
		}
		// Alert gently when the material is used before is creation
		if(undefinedVariables.some(undefinedVariable => undefinedVariable.materialUsedBeforeCreation)) {
			if(this.copexObject.ldEdition) {
				alertGently(__('Au moins une action utilise un matériel avant qu\'il n\'ait été créé. Le protocole n\'est pas évaluable.'), 'warning');
			}
			isUndefinedVariables = true;
		}
		let filteredUndefinedVariables = undefinedVariables.filter(undefinedVariable => 
			!undefinedVariable.materialUsedBeforeCreation && !undefinedVariable.materialNotCreated);
		if(this.simulatedActivity.checkUndefinedVariables(filteredUndefinedVariables)) {
			if(this.copexObject.ldEdition) {
				alertGently(__('Les champs ne sont pas tous remplis !'), 'warning');
			}
			isUndefinedVariables = true;
		}
		return isUndefinedVariables;
	}

	function removeHighlighting (node=this.copexObject.LD.protocol) {
		if(!node.nodes) {
			return true;
		} else {
			for (let n of node.nodes) {
				if (n.parameterValues) {
					for (let parameter of n.parameterValues) {
						parameter.highlighted = false;
					}
				}
				this.removeHighlighting(n);
			}
		}
	}

	function resetFeedbackKnowledge() {
		const self = this;
		$.ajax({
			type: "POST",
			url: "/simulation/resetUserSkills",
			datatype: "json",
			data: {
				id_labdoc: self.copexObject.id_ld
			},
			error: function (jqXHR, textStatus) {
				alertGently(__("Une erreur est survenue : ") + textStatus, 'danger');
			},
			success: function (responseData) {
				self.previous_error = null;
				self.constraintStates = [];
				alertGently(__('Le niveau des rétroactions a été réinitialisé'),'info');
			}
		});
	}

	function unitConvertToBaseUnit (value, sourceUnit) {
		if (this.copexObject.quantitiesData) {
			let quantityObject = this.copexObject.quantitiesData
				.find(quantity => quantity.unit.some(unit => Object.values(unit.symbol).includes(sourceUnit)));
			if (quantityObject) {
				const targetUnit = this.simulatedActivity.baseUnits[quantityObject.name["en"]];
				const targetFactor = this.copexObject.findUnit(targetUnit).factor;
				const sourceFactor = this.copexObject.findUnit(sourceUnit).factor;
				return parseFloat(value) * parseFloat(sourceFactor[0]) / parseFloat(targetFactor[0]) + (parseFloat(sourceFactor[1]) - parseFloat(targetFactor[1])) / parseFloat(targetFactor[0]);
			}
		}
	}

	function getVariableValue ( variableKey ) {
		let variable = this.protocol.flatMap(action => {
			return Object.keys(action).map(key => {
				return { key: key, value: action[key] }
			})
		}).find(variable => variable.key === variableKey);
		return variable ? variable.value : null;
	}

	function getMaterialFromShortName (shortName) {
		return this.copexObject.LD.materialList.listItems.find(material => material.properties.some(p=>p.name==='shortName' && p.value === shortName));
	}

	function getPropertyValueByName (material, propertyName) {
		if (!material) {
			return null;
		}
		const property = material.properties.find(p => p.name === propertyName);
		return property && property.value;
	}

	function getPropertyUnitNameByPropertyName (material, propertyName) {
		return this.copexObject.getPhysicalUnitLabel(material.properties.find(p => p.name === propertyName));
	}

	function propertyValueConvertInbaseUnit(material, propertyName) {
		return this.unitConvertToBaseUnit(
			this.getPropertyValueByName(material, propertyName),
			this.getPropertyUnitNameByPropertyName(material, propertyName)
		);
	}

	function blendColors(colorA, colorB, qA, qB) {
		if (colorA && colorB) {
			const [rA, gA, bA] = colorA.match(/\w\w/g).map((c) => parseInt(c, 16));
			const [rB, gB, bB] = colorB.match(/\w\w/g).map((c) => parseInt(c, 16));
			const r = Math.round((rA * qA + rB * qB) / (qA + qB)).toString(16).padStart(2, '0');
			const g = Math.round((gA * qA + gB * qB) / (qA + qB)).toString(16).padStart(2, '0');
			const b = Math.round((bA * qA + bB * qB) / (qA + qB)).toString(16).padStart(2, '0');
			return '#' + r + g + b;
		} else {
			return '#ffffff';
		}
	}

	function getObjectFromParameter (actionTextPart, parameter, node=null, iterationIdx=0) {
		switch (actionTextPart.domElement) {
			case 'inputText':
				if (parameter.valueIterationVariable) {
					return parameter.valueIterationVariable.values[iterationIdx];
				} else {
					return parameter.value;
				}
			case 'select':
				if (parameter.valueNameMaterialCreated) {
					return this.copexObject.LD.materialList.listItems.find(material =>
						material.name === parameter.valueNameMaterialCreated);
				}
				else if (parameter.valueIterationVariable) {
					const mat = this.copexObject.LD.materialList.listItems.find(material =>
						material.name === parameter.valueIterationVariable.values[iterationIdx]);
					return mat || {materialNotCreated:parameter.valueIterationVariable.values[iterationIdx]};
				} else {
					if (actionTextPart.isMaterialList) {
						if (parameter.value && parameter.value.startsWith('iterative')) {
							return this.copexObject.LD.materialList.listItems.find(material =>
							material.name === parameter.value.split('_')[1]);
						} else {
							const material = this.copexObject.LD.materialList.listItems.find(material =>
								parseInt(material.id) === parseInt(parameter.value));
							if(material && this.copexObject.isMaterialUsedAfterCreation(material, node)) {
								return material;
							} else if(material) {
								return {materialUsedBeforeCreation:material};
							} else {
								return null;
							}
						}
					} else {
						return this.copexObject.LD.customLists.find(list => parseInt(list.id) === parseInt(parameter.listId)).listItems.find(obj =>
							parseInt(obj.id) === parseInt(parameter.value));
					}
				}
			case 'numberSelect':
				if (parameter.valueIterationVariable) {
					return [
						parseFloat(parameter.valueIterationVariable.values[iterationIdx]),
						parameter.valueIterationVariable.unit.name
					];
				} else {
					let unitList = this.copexObject.getUnitList(actionTextPart.listId);
					let unit = unitList ? unitList.find(unit => parseInt(unit.id) === parseInt(parameter.valueUnit)) : null;
					let unitName = unit ? unit.name : '';
					return [
						parseFloat(parameter.valueQuantity),
						unitName
					];
				}
			case 'materialInputText':
				if (parameter.valueIterationVariable) {
					let material = this.copexObject.addMaterial(parameter.valueIterationVariable.values[iterationIdx]);
					material.createdBySimulation = true;
					return material;
				} else {
					return this.copexObject.LD.materialList.listItems.find(material =>
						parseInt(material.id) === parseInt(parameter.value));
				}
		}
	}
	
	function traceUserInit () {
		if (window.global_scope === 'default' && this.copexObject.ldEdition) {
			let self = this;
			this.removeMaterialCreatedBySimulation();
			$.ajax({
				type: "POST",
				url: "/simulation/traceSimulationInit",
				datatype: "json",
				data: {
					traceAction: 'student_edit_labdoc',
					idLabdoc: self.copexObject.id_ld,
					materialList: JSON.stringify(self.copexObject.LD.materialList),
					protocol: JSON.stringify(self.retreiveParameterValues())
				},
				error: function (jqXHR, textStatus) {
					alertGently(__("Une erreur est survenue : ") + textStatus, 'danger');
					self.removeMaterialCreatedBySimulation();
				},
				success: function (responseData) {
					self.removeMaterialCreatedBySimulation();
				}
			});
		}
	}	
	function traceUserNodeAction (traceAction, node) {
		let path = [];
		let action = null;
		let step = null;
		let parentNode = this.copexObject.findParentNode(node.id);
		while (parentNode) {
			path.push({
				stepId: parentNode.id,
				stepName: parentNode.name,
				stepSimulationVariable: parentNode.simulationVariable
			});
			parentNode = this.copexObject.findParentNode(parentNode.id);
		}
		if (node.nodeType === 'action') {
			action = {};
			action.actionId = node.id;
			action.comment = node.comment;
			action.simulationVariable = this.copexObject.LD.actionList.find(al => parseInt(al.id) === parseInt(node.idStructuredAction)).simulationVariable;
			action.textParts = this.getTextPartsForTrace(node);
		} else if (node.nodeType === 'step') {
			step = {};
			step.stepId = node.id;
			step.name = node.name;
			step.comment = node.comment;
			step.simulationVariable = node.simulationVariable;
			step.iterationVariables = node.iterationVariables;
		}
		let simulationVariable;
		if (action) {
			simulationVariable = action.simulationVariable;
		} else {
			simulationVariable = step ? step.simulationVariable : null;
		}
		if (window.global_scope === 'default') {
			let self = this;
			$.ajax({
				type: "POST",
				url: "/simulation/traceSimulationNodeAction",
				datatype: "json",
				data: {
					traceAction: traceAction,
					idLabdoc: self.copexObject.id_ld,
					path: path,
					position: node.position,
					simulationVariable: simulationVariable,
					action: traceAction.startsWith('student_modify') ? action : null,
					step: traceAction.startsWith('student_modify') ? step : null,
				},
				error: function (jqXHR, textStatus) {
					alertGently(__("Une erreur est survenue : ") + textStatus, 'danger');
				},
				success: function (responseData) {
				}
			});
		}
	}
	function traceSimulationUserAction(traceAction, object) {
		if (window.global_scope === 'default') {
			let self = this;
			let attributes = {};
			if(object.overallConcernsType) {
				// context trace
				attributes = {type:object.overallConcernsType, content:object.content};
			} else if(object.types || object.url) {
				// material or link trace
				attributes = object;
			}
			$.ajax({
				type: "POST",
				url: "/simulation/traceSimulationUserAction",
				datatype: "json",
				data: {
					traceAction: traceAction,
					idLabdoc: self.copexObject.id_ld,
					attributes: JSON.stringify(attributes),
				},
				error: function (jqXHR, textStatus) {
					alertGently(__("Une erreur est survenue : ") + textStatus, 'danger');
				},
				success: function (responseData) {
				}
			});
		}
	}
	function traceUserAction (traceAction, object) {
		if(object.nodeType) {
			let {inEdition, ...nodeWithoutEditionKey} = object; 
			if(!traceAction.startsWith('student_modify') 
				||( this.currentEditedNode 
				&& this.copexObject.extractModelChanges(this.currentEditedNode, JSON.stringify(nodeWithoutEditionKey)) !== true)) {
				this.traceUserNodeAction (traceAction, object);
			}
		} else {
			this.traceSimulationUserAction(traceAction, object);
		} 
	}
	
	function cleanProtocolInEdition (node=JSON.parse(JSON.stringify(this.copexObject.LD.protocol))) {
		if (node.nodes) {
			for (let n of node.nodes) {
				delete n.inEdition;
				this.cleanProtocolInEdition(n);
			}
		}
		return node;
	}
	
	function getTextPartsForTrace (node) {
		return this.copexObject.getActionTextParts(node).map(actionTextPart => { 
			let parameter = node.parameterValues.find(p => parseInt(p.parameterId) === parseInt(actionTextPart.id));
			let returnedValue;
			if (actionTextPart.domElement === 'text') {
				returnedValue = actionTextPart.value;
			} else if (actionTextPart.domElement === 'inputText' && parameter) {
				returnedValue = parameter.valueIterationVariable ? parameter.valueIterationVariable.name : (parameter.value || '___________');
			} else if (actionTextPart.domElement === 'materialInputText' && parameter) {
				let materialCreated = this.copexObject.LD.materialList.listItems.find(m => parseInt(m.id) === parseInt(parameter.value))
				returnedValue = parameter.valueIterationVariable ? parameter.valueIterationVariable.name : ((materialCreated && materialCreated.name) ? materialCreated.name : '___________');
			} else if (actionTextPart.domElement === 'select' && parameter) {
				returnedValue = parameter.valueIterationVariable ? parameter.valueIterationVariable.name : (this.copexObject.getSelectValue(node, actionTextPart, 'select') || '___________');
			} else if (actionTextPart.domElement === 'selectIterationVariable' && parameter) {
				returnedValue = this.copexObject.getSelectValue(node, actionTextPart, 'selectIterationVariable') || '___________';
			} else if (actionTextPart.domElement === 'numberSelect' && parameter) {
				returnedValue = (parameter.valueIterationVariable ? parameter.valueIterationVariable.name : (parameter.valueQuantity || '__'))
				+ ((parameter.valueIterationVariable && parameter.valueIterationVariable.unit) ? parameter.valueIterationVariable.unit.name : (this.copexObject.getSelectValue(node, actionTextPart, 'numberSelect') || '__'));
			}
			return {
				type: actionTextPart.domElement,
				value: returnedValue
			}; 
		} )
	}
	function canActionBeSimulated (node) {
		let structuredAction = this.copexObject.LD.actionList.find(a => parseInt(a.id) === parseInt(node.idStructuredAction));
		return structuredAction.canBeSimulated;
	}
	
	function consoleLogChanges (constraints, skillsChanges) {
		for (let constraint of constraints.filter(c => c.previous_state !== c.current_state)) {
			console.log('------------- Constraint ', constraint.id_constraint, '-------------');
			console.log(constraint.description);
			console.log('state : ', constraint.previous_state, ' -> ', constraint.current_state,
				', last_not_null_state : ', constraint.last_not_null_state, ' -> ',(constraint.current_state!==null)?constraint.current_state:constraint.last_not_null_state);
		}
		for (let skill of skillsChanges) {
			console.log('------------- TaskType ', skill.id_task_type, '-------------');
			console.log(skill.description);
			console.log('knowledge : ', skill.old_knowledge, ' -> ', skill.new_knowledge,
				', feedback_level : ', skill.old_feedback_level, ' -> ', skill.new_feedback_level);
		}
	}
	
	function getTitleSimulationButton() {
		return this.simulationPaused?__("Simuler les mesures telles qu'elles sont spécifiées dans le protocole"):__("Mettre en pause la simulation");
	}

	return {
		init,
		initSimulation,
		initSimulationDrawing,
		checkProtocol,
		synchronizeProtocol,
		resetFeedbacks,
		setFeedbackError,
		setFeedbackIndications,
		highlightProtocol,
		displayGlobalKnowledgesEvolution,
		retreiveParameterValues,
		extractParameterValues,
		findClosestSimulationVariable,
		getUndefinedVariables,
		checkUndefinedVariables,
		removeHighlighting,
		resetFeedbackKnowledge,
		unitConvertToBaseUnit,
		getVariableValue,
		getMaterialFromShortName,
		getPropertyValueByName,
		propertyValueConvertInbaseUnit,
		getPropertyUnitNameByPropertyName,
		blendColors,
		getObjectFromParameter,
		removeMaterialCreatedBySimulation,
		traceUserNodeAction,
		traceSimulationUserAction,
		traceUserAction,
		traceUserInit,
		getTextPartsForTrace,
		canActionBeSimulated,
		insertLd,
		consoleLogChanges,
		cleanProtocolInEdition,
		getTitleSimulationButton,
		canvas: {},
		simulationPaused: true,
		protocol: [],
		constraintStates: [],
		feedbacks: [],
		previous_error: null,
		simulationMaterial: simulationMaterial,
		copexObject: copexObject, 
		simulationCode: simulationCode,
		constraints: [],
		taskTypes: [],
		progression:null,
		simulatedActivity: null
	}
}

document.addEventListener("keyup", (event) => {
	if (event.target.dataset.key === 'verificationOfTheRequiredConcentration') {
		let hash = event.target.dataset.simulation;
		const converter = {'A':'.', 'Z':'0', 'C':'1', 'W':'2', 'E':'3', 'V':'4', 'T':'5', 'J':'6', 'R':'7', 'M':'8', 'O':'9'};
		const solution = parseFloat(hash.split('').map(l => converter[l]).join('')) / 100151;
		const guess = parseFloat(event.target.textContent.trim().replace(',', '.'));
		if (Math.abs(guess / solution - 1) < 0.01) {
			event.target.style.color = 'green';
			const ld = event.target.closest('.labdoc');
			if(ld && window.global_scope === 'default') {
				const idLd = ld.dataset.id;
				if(idLd) {
					$.ajax({
						type: "POST",
						url: "/simulation/traceSimulationCorrectAnswer",
						datatype: "json",
						data: {
							idLabdoc: idLd,
						},
						error: function (jqXHR, textStatus) {
							alertGently(__("Une erreur est survenue : ") + textStatus, 'danger');
						},
						success: function (responseData) {
						}
					});
				}
			}
		} else {
			event.target.style.color = 'red';
		}
	}
});
