const dosageSpectroCtrl = function (simulationObject) {

	function init() {
		// Init specific values
		//this.globalOffsetX = 255;
		//this.globalOffsetY = 25;
		this.baseUnits = { "molar concentration" : "mol.L-1", volume: "mL", "length": "nm" };
		this.simulationObject.replaceLdResponse = true;
		this.simulationObject.replaceLdData = false;
	}

	function initSimulationDrawing () {
		if (this.simulationObject.canvas) {
			this.simulationObject.canvas.style.display = 'none';
		}
		this.simulationObject.simulationDisplayDiv.classList.add('dosage-spectro');
		this.drawSimulation();
		if(!this.simulationObject.copexObject.ldEdition && window.global_scope!=='follow') {
			this.simulationObject.checkProtocol(true);
		} 
	}

	function checkUndefinedVariables (undefinedVariables) {
		const authorizedUndefined = ['spectrumProductionMin', 'spectrumProductionMax', 'absorbanceMeasurementWaveLength'];
		// return true if a material is not properly used or a variable is not filled
		return undefinedVariables.filter(u => !authorizedUndefined.includes(u.variable)).length !== 0;
	}

	function setSpecificVariables () {
	}

	function convertRemainingInputs(simulationVariables, actionTextPart) {
		if (['spectrumProductionMin', 'spectrumProductionMax', 'absorbanceMeasurementWaveLength'].includes(actionTextPart.simulationVariable)) {
			simulationVariables[actionTextPart.simulationVariable] = this.simulationObject.unitConvertToBaseUnit(parseFloat(simulationVariables[actionTextPart.simulationVariable]), "nm");
		}
		return simulationVariables;
	}

	function computeRemainingValues(simulationVariables, structuredAction) {
		if (structuredAction.simulationVariable === 'dilution' && !Object.keys(simulationVariables).some(key => key !== 'stepSimulationVariable' && (typeof(simulationVariables[key]) === "undefined" || simulationVariables[key] === null))) {
			if(!simulationVariables.dilutionBaseSolution.materialNotCreated && !simulationVariables.dilutionBaseSolution.materialUsedBeforeCreation) {
				for (let key of Object.keys(simulationVariables.dilutionBaseSolution).filter(k => k !== "name" && k !== "id" && k !== "position" && k !== 'materialNotCreated')) {
					simulationVariables.dilutionSolutionCreated[key] = JSON.parse(JSON.stringify(simulationVariables.dilutionBaseSolution[key]));
				}
				if (!simulationVariables.dilutionSolutionCreated.properties.find(p => p.name === 'concentration')) {
					this.simulationObject.copexObject.addPropertyToMaterial(simulationVariables.dilutionSolutionCreated, {
						name: 'concentration', physicalQuantityId: null, value: null, unitId: null, hidden: true
					});
				} else {
					simulationVariables.dilutionSolutionCreated.properties.find(p => p.name === 'concentration').value =
						this.simulationObject.unitConvertToBaseUnit(simulationVariables.dilutionBaseSolutionVolumeAndUnit[0], simulationVariables.dilutionBaseSolutionVolumeAndUnit[1]) /
						this.simulationObject.unitConvertToBaseUnit(simulationVariables.dilutionSolutionCreatedVolumeAndUnit[0],simulationVariables.dilutionSolutionCreatedVolumeAndUnit[1])
						* this.simulationObject.propertyValueConvertInbaseUnit(simulationVariables.dilutionBaseSolution, 'concentration');
				}
				let baseSolutionShortName = simulationVariables.dilutionBaseSolution.properties.find(p => p.name === 'shortName') || {value:'default'};
				if (baseSolutionShortName) {
					this.simulationObject.copexObject.addPropertyToMaterial(simulationVariables.dilutionSolutionCreated, {
						name: 'originShortName',
						physicalQuantityId: null,
						value: baseSolutionShortName.value,
						unitId: null, hidden: true
					});
				}
			}
		}
		return simulationVariables;
	}
	
	function cleanSimulation() {
		this.simulationObject.simulationDisplayDiv = document.getElementById(this.simulationObject.copexObject.id_ld+"_simulation_display_div");
		this.simulationObject.simulationDisplayDiv.innerHTML = '';
		let divSpectrum = document.createElement('div');
		divSpectrum.classList.add('bg-spectro');
		this.simulationObject.simulationDisplayDiv.prepend(divSpectrum);
	}

	async function drawSimulation() {
		this.simulationObject.simulationDisplayDiv = document.getElementById(this.simulationObject.copexObject.id_ld+"_simulation_display_div");
		this.simulationObject.simulationDisplayDiv.innerHTML = '';
		if (this.simulationData && this.simulationData.length>0) {
			let simulation = this.simulationData[this.simulationIndex];
			
			let controlButtonsDiv = document.createElement('div');
			controlButtonsDiv.classList.add('control-buttons');
			let previousButton = document.createElement('button');
			previousButton.classList.add('control-button');
			previousButton.disabled = this.simulationIndex === 0;
			let previousIcon = document.createElement('i');
			previousIcon.classList.add('fas', 'fa-backward-step');
			previousButton.onclick = () => { this.simulationIndex = Math.max(0, this.simulationIndex - 1); this.drawSimulation() };
			previousButton.appendChild(previousIcon);
			
			let nextButton = document.createElement('button');
			nextButton.classList.add('control-button');
			nextButton.disabled = this.simulationIndex === this.simulationData.length - 1;
			let nextIcon = document.createElement('i');
			nextIcon.classList.add('fas', 'fa-forward-step');
			nextButton.onclick =() => { this.simulationIndex = Math.min(this.simulationData.length - 1, this.simulationIndex + 1); this.drawSimulation() };
			nextButton.appendChild(nextIcon);
			
			controlButtonsDiv.appendChild(previousButton);
			controlButtonsDiv.appendChild(nextButton);
			this.simulationObject.simulationDisplayDiv.appendChild(controlButtonsDiv);

			if (simulation.spectrum) {
				let divSpectrum = document.createElement('div');
				divSpectrum.setAttribute('id', 'spectrum_dosage_' + this.simulationObject.copexObject.id_ld);
				divSpectrum.classList.add('bg-spectro');
				let spectrumTrace = {
					x: simulation.spectrum.data.map(x => x.lambda),
					y: simulation.spectrum.data.map(x => x.absorbance),
					hovertemplate: 'Lambda: %{x:.0f} nm<extra></extra>',
					mode: 'lines',
					name: 'Scatter + Lines',
					line: {dash: '0px,5200px'}
				};
				let layout = {
					title: {
						text: __('Spectre d\'absorbance') + ' : ' + simulation.spectrum.solutionName,
						xanchor: 'center',
						yanchor: 'top',
						y: 0.83
					},
					margin: {l: 130, r: 93, t: 109, b: 120},
					pad: {l: 0, r: 0, t: 0, b: 0},
					paper_bgcolor: 'transparent',
					plot_bgcolor: 'transparent',
					xaxis: {
						title: 'Lambda (nm)',
						position: 0.1,
						fixedrange: true
					},
					yaxis: {
						title: 'Absorbance',
						tickformat: '.3',
						position: 0.05,
						fixedrange: true
					}
				};
				this.simulationObject.simulationDisplayDiv.appendChild(divSpectrum);
				let duration = 5000;
				let self = this;
				await Plotly.newPlot(divSpectrum.id, [spectrumTrace], layout)
					.then(function () {
						return Plotly.animate(divSpectrum.id,
							[{data: [{'line.dash': document.getElementById(divSpectrum.id).offsetWidth * 1.2 + 'px,0px'}]}],
							{
								frame: {duration: duration, redraw: false},
								transition: {duration: duration, easing: 'linear'}
							}
						);
					});
			} else if (simulation.absorbance) {
				let divAbsorbance = document.createElement('div');
				divAbsorbance.setAttribute('id', 'absorbance_dosage_' + this.simulationObject.copexObject.id_ld);
				divAbsorbance.classList.add('bg-spectro');
				let spanSolutionName = document.createElement('span');
				spanSolutionName.innerText = __('Solution : ') + simulation.absorbance.solutionName;
				let spanLambda = document.createElement('span');
				spanLambda.innerText = __('Lambda : ') + Math.round(simulation.absorbance.lambdaValue) + ' ' + this.baseUnits.length;
				let spanAbsorbance = document.createElement('span');
				spanAbsorbance.innerText = __('Absorbance : ') + Math.round(simulation.absorbance.absorbanceValue * 1000) / 1000;
				divAbsorbance.appendChild(spanSolutionName);
				divAbsorbance.appendChild(spanLambda);
				divAbsorbance.appendChild(spanAbsorbance);
				this.simulationObject.simulationDisplayDiv.appendChild(divAbsorbance);
			}
		} else {
			let divSpectrum = document.createElement('div');
			divSpectrum.classList.add('bg-spectro');
			this.simulationObject.simulationDisplayDiv.prepend(divSpectrum);
		}
	}

	function runSimulation () {
		window.scrollTo({
			'behavior': 'smooth',
			'left': 0,
			'top': this.simulationObject.feedbacksDiv.offsetTop - 80
		});
		this.simulationIndex = 0;
		this.drawSimulation();
	}
	
	function displayFeedbackAfterSimulation() {
		if(!this.simulationObject.previous_error) {
			this.simulationObject.feedbackSimulation =
				__('Félicitations ! votre protocole est optimisé.')
				+ "<br />"
				+ __('Utilisez les données simulées pour déterminer la concentration recherchée');
		}
	}
	
	function stopSimulation() {
		this.simulationObject.isRunning = false;
		this.simulationObject.simulationPaused = true;
	}

	function getSimulationTextPartVariableNames () {
		return [
			"conditioningContainer", "conditioningSolution",
			"dilutionBaseSolutionVolumeAndUnit", "dilutionBaseSolution", "dilutionBaseSolutionMeasurer", "dilutionContainer", "dilutionSolvent", "dilutionSolutionCreatedVolumeAndUnit", "dilutionSolutionCreated",
			"transferSolution", "transferContainer",
			"referenceSpectrumRecordingSolution",
			"spectrumProductionSolution", "spectrumProductionMin", "spectrumProductionMax",
			"absorbanceMeasurementSolution", "absorbanceMeasurementWaveLength"
		];
	}

	function getSimulationActionVariableNames () {
		return ["conditioning", "dilution", "transfer", "referenceSpectrumRecording", "spectrumProduction", "absorbanceMeasurement"];
	}

	function getSimulationStepVariableNames () {
		return ["preparationOfReferenceSolutions", "referenceCurveElaboration", "absorbanceMeasures", "E124ConcentrationComputation"];
	}

	function getSimulationVariableFromConstraint(satisfactionConditionKey) {
		switch(satisfactionConditionKey) {
			case 'titratorOrTitrandShouldBeOppositeNature':
				return true;
		}
	}

	function restoreSimulationData (simulationData) {
		this.simulationData = simulationData;
		this.simulationIndex = 0;
		this.drawSimulation();
	}

	return {
		init,
		initSimulationDrawing,
		checkUndefinedVariables,
		setSpecificVariables,
		cleanSimulation,
		drawSimulation,
		runSimulation,
		displayFeedbackAfterSimulation,
		stopSimulation,
		getSimulationTextPartVariableNames,
		getSimulationActionVariableNames,
		getSimulationStepVariableNames,
		getSimulationVariableFromConstraint,
		computeRemainingValues,
		convertRemainingInputs,
		restoreSimulationData,
		simulationObject: simulationObject
	}
}
