/* global code */

// code is required

function setNewPwd() {
    // verification qu'un code de modification existe
    var pwd = $("#new_pwd").val() ; 
    if (!token) {
        alert ( __("Utilisez le lien fourni dans votre e-mail pour accéder à cette page") ) ;
        return ;
    }
    // verification que tous les champs sont remplis
    if ($("#login").val()=='' || pwd=='' || $("#new_pwd2").val()=='') {
        alert ( __("Tous les champs doivent être remplis") ) ;
        return ;
    }
    // vérification que les 2 mdp sont identiques
    if ( pwd != $("#new_pwd2").val()) {
        alert ( __("Vos deux mots de passe ne sont pas cohérents") ) ;
        return ;
    }
    // vérification que le pwd est suffisamment compliqué
    if (pwd.length<8 || pwd.length>20 || pwd == pwd.toUpperCase() || pwd == pwd.toLowerCase()) {
        alert ( __("Votre mot de passe doit comprendre entre 8 et 20 caractères avec des lettre minuscules et majuscules") ) ;
        return ;
    }
    $.ajax({
        type: "POST",
        url: "/password/reset",
        data: "token="+token+"&login="+$("#login").val()+"&password="+$("#new_pwd").val()+"&password_confirmation="+$("#new_pwd2").val(),
        success: function(msg){ alert(msg); },
        fail: function() { alert(__("La mise à jour n'a pas pu être effectuée") ); }
    });
}
