/* global __, alertGently, $ */
/*
 * @license http://www.gnu.org/licenses/gpl-3.0.html  GNU GPL v3
 */
window.TeamsCtrl = (function () {
	let state = null;
	const refreshSeconds = 2;

	/*
	 * Initialize the data and start synchronize process
	 * @param {obj} data -- AlpineJS initial data
	 */
	function init (data) {
		state = data;
		setTimeout(synchronize, refreshSeconds * 1000);
		addEmptyTeamIfAllowed();
	}

	/**
	 * Adds an empty team If allowed
	 */
	function addEmptyTeamIfAllowed () {
		if (state.canAdd) {
			state.teams.push({
				id_report: 0,
				name: __('Nouvelle équipe'),
				extensible: true,
				members: [
				]
			});
		}
	}

	/**
	 * Refresh team list for database
	 */
	async function synchronize () {
		$.ajax({
			url: '/teamconfig/' + state.settings.id_team_config + '/getTeams'
		}).done(function (data) {
			updateTeams(data);
			setTimeout(synchronize, refreshSeconds * 1000);
		});
	}

	/**
	 * Update the local list of team from the given list of team
	 * @param {array} teams
	 */
	function updateTeams (newTeams) {
		state.teams = newTeams.teams;
		addEmptyTeamIfAllowed();
		join(currentTeam());
	}

	/**
	 * Can the user join the given team
	 * @param {obj} team
	 * @return {boolean}
	 */
	function canJoin (team) {
		if (team === undefined) {
			return false;
		}
		return team.extensible;
	}

	/**
	 * Leave the current team
	 */
	function leaveTeam () {
		const team = currentTeam();
		if (team === undefined) {
			return;
		}
		team.members = team.members.filter(m => m.id_user !== state.user.id_user);
		state.current_team_id = null;
	}

	/**
	 * Join the given team, if allowed
	 */
	function join (team) {
		if (!canJoin(team)) {
			return;
		}
		leaveTeam();
		team.members.push(state.user);
		state.current_team_id = team.id_report;
	}

	/**
	 * Return the team currently containning the logged user
	 * @return {obj}
	 */
	function currentTeam () {
		return state.teams.find(t => t.id_report === parseInt(state.current_team_id));
	}

	/**
	 * Is the given team a new team ?
	 * @return {boolean}
	 */
	function isNew (team) {
		if (team === undefined) {
			return false;
		}
		return team.id_report === 0;
	}

	/**
	 * Format a number with a leading 0 if < 10
	 * @return {string}
	 */
	function format (number) {
		return String(number).padStart(2, 0);
	}

	/**
	 * Returns the next free team number formated
	 * @return {string}
	 */
	function getNextTeamNum () {
		return format(
			state.teams.map(t => parseInt(t.name.replace(state.settings.name_prefix, '')))
				.reduce((a, b) => Math.max(a, b), 0) + 1
		);
	}

	/**
	 * Returns the next free team name
	 * @return {string}
	 */
	function nextName () {
		return state.settings.name_prefix + getNextTeamNum();
	}

	/**
	 * Submit the user choice
	 */
	function submit () {
		const myTeam = currentTeam();
		if (myTeam === undefined) {
			alertGently(__('Veuillez choisir une équipe'));
			// Wait for sync code to end before resolving
			return new Promise(resolve => setTimeout(resolve, 0));
		}
		const dfd = $.Deferred();
		$.ajax({
			method: 'POST',
			url: '/teamconfig/' + state.settings.id_team_config + '/selectTeam',
			data: {
				id_report: myTeam.id_report
			}
		}).done(function (data) {
			window.location = '/report/' + data.id_report;
		}).fail(function (jqXHR, textStatus, errorThrown) {
			alertGently(jqXHR.responseJSON.message, jqXHR.responseJSON.status);
		}).always(function () {
			dfd.resolve();
		});
		return dfd.promise();
	}

	/**
	 * Is the given user the logged user ?
	 * @return {boolean}
	 */
	function isMe (u) {
		return u.id_user === state.user.id_user;
	}

	/**
	 * Return a formated user name
	 * @return {string}
	 */
	function displayName (u) {
		return u.first_name + ' ' + u.user_name;
	}

	return {
		init,
		submit,
		join,
		canJoin,
		nextName,
		isMe,
		displayName
	}
})();
