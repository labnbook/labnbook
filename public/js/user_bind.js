function highlight(selector) {
	$(selector).addClass('highlight');
	setTimeout(() => {
		$(selector).removeClass('highlight');
	}, 4000);
}

function prelogin(internal, login, dohighlight) {
	if (internal) {
		$('#user_login').val(login);
		if (dohighlight) {
			highlight('#user_pwd');
		}
	} else {
		$('#frm-login-cas').submit()
	}
}
function checkFormHomonymes() {
	let data = $('form#create-account').serializeArray();
	// Remove password field
	data = data.splice(0, data.length -1);
	$.get({
		url: '/user/getHomonymes',
		method: 'POST',
		data: data
	}).done(function(accounts) {
		if (accounts.length) {
			console.log(accounts);
			if(accounts.length === 1) {
				let details = accounts[0].first_name + ' ' + accounts[0].user_name + ' - ' + accounts[0].login;
				alertGently(__("Nous détectons un compte pouvant vous appartenir : {{details}}. Nous vous conseillons de réutiliser tout compte existant en vous connectant à ce compte.", {'details': details}), 'warning');
			} else {
				let details = accounts.reduce( ( accu, account)  => accu + '<li>' + account.first_name + ' ' + account.user_name + ' - ' + account.login + '</li>', '<ul>');
				details += "</ul>";
				console.log(details);
				alertGently(__("Nous détectons des comptes pouvant vous appartenir : {{- details}}. Nous vous conseillons de réutiliser tout compte existant en vous à ce compte", {'details': details}), 'warning');
			}
		}
	});
}

function validate_form() {
	let valid = true;
	$('input:invalid').each(
		function(i, e) {
			if ($(e).data('error')) {
				alertGently($(e).data('error'));
			}
			valid = false;
		}
	);
	let email = $('#create-account-email').val().toLowerCase();
	if (email && !email.match(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
		alertGently(__("Votre e-mail est mal formé"));
		valid = false;
	}
	if ($('#create-account-pwd').val() !== $('#create-account-pwd2').val()) {
		alertGently(__("Mot de passe : vos deux entrées ne sont pas identiques"));
		valid = false;
	}
	return valid;
}

function createAccount() {
	if (validate_form()) {
		$('#create-account').submit();
	}
}

function handleCasContinue() {
	let login=$('input.account-checkbox[name=login]:checked').val()
	if (typeof(login) === 'undefined') {
		alertGently(__("Veuillez selectionner une option"));
		return;
	}
	$('form#login-form-'+login).submit();
}

function showRadioForm(login) {
	$('.login').hide();
	$('form#login-form-'+login).show();
	$('#reset-password-'+login).show();
}
