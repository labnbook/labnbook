/* global i18next, i18nextXHRBackend */

// Constants for LNB widgets 
const WIDGET ={
	COMMENTS : "comments",
	RESOURCES : "ressource",
	MESSAGING : "messagerie",
	TRASH : "trash",
	SCORING : "scoring",
	TASK: 'task',
	RECOVER_PASSWORD : "recoverpassword",
	CALENDAR : "calendar",
}

const RESPONSIVE_WIDTH ={
	SM : 768,
	MD : 1024,
	LG : 1200,
	XL : 1440,
}

/**
 * Wrapper for translation.
 *
 * When i18next is loaded, this will be overwritten.
 * Meanwhile it just returns the text received.
 *
 * @param {String} sentence
 * @param {object} {key: string}
 * @return {string}
 */
function __() {
	if (arguments.length === 1) {
		return arguments[0];
	} else {
		var interpolated = arguments[0];
		for (var x in arguments[1]) {
			if (arguments[1].hasOwnProperty(x)) {
				interpolated = interpolated.replace("{{" + x + "}}", arguments[1][x]);
			}
		}
		return interpolated;
	}
}

function capitalize(string) {
    // Inspired from https://stackoverflow.com/a/3291856
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}

/**
 * Use this function to escape special char like ' in __() trad.
 * Example : 
 * editor = "Bo'b";
 * __("{{- editor}}", {editor : hTMLEscapeNoConv (editor)});
 * Notice the - before the parameter editor. https://www.i18next.com/translation-function/interpolation
 * @param unsafe
 * @returns {string} safe
 */
function hTMLEscapeNoConv(unsafe){
	var p = $('<p>');
	p.text(unsafe);
	return p.html();
}

function langLong() {
	function longform(lang) {
		if (lang.length ==5) {
			// Already in long form
			return lang.replace('-', '_');
		}
		switch(lang) {
			case 'fr':
				return 'fr_FR';
			case 'en':
				return 'en_US';
		}
		return 'fr_FR';
	}
	if (navigator.language && navigator.language.includes(window.labnbook.lang)) {
		return longform(navigator.language);
	}
	if (navigator.userLanguage && navigator.userLanguage.includes(window.labnbook.lang)) {
		return longform(navigator.userLanguage);
	}
	return longform(window.labnbook.lang);
}

/**
 * Initialize i18next translator.
 *
 * Use this promise to wait for i18next initialization before acting.
 * If not used and i18next is slow to load, the translation will not work.
 */
if (window.labnbook.i18n) {
	window.i18next_Promise = i18next
		.use(i18nextXHRBackend)
		.init({
			lng: window.labnbook.lang, // Selected language (defined : default.php)
			// namespace and path
			ns: 'translation',
			defaultNS: 'translation',
			backend: {
				loadPath: window.labnbook.langFile
			},

			// Allow keys to be phrases having `:`, `.` and return key if his value is an empty string
			nsSeparator: false,
			keySeparator: false,
			returnEmptyString: false,

			// do not load a fallback, use the key
			fallbackLng: false,
			debug: 'debug' in window.labnbook ? window.labnbook.debug : false, // display on the console
		}, function(err, t) {
		}).then(function(){
			// overwrite __()
			window.__ = function() {
				return i18next.t(arguments[0], arguments[1]);
			}
		});
} else {
	window.i18next_Promise = Promise.resolve();
}

/*
 * Global error listener that logs errors on the server.
 */
window.onerror = function(msg, file, line, col, error) {
	try {
		StackTrace.fromError(error, {offline: true}).then(function(stackframes) {
			logError(msg, 'error', stackframes);
		});
	} catch (e) {
		logError(msg, 'error', file + ":" + line);
	}
};

/**
 * Log a message into the JS console and the server log files.
 *
 * @param {String} message
 * @param {String} category "error", "warning", "info"
 * @param {object} trace
 * @return {undefined}
 */
function logError(message, category, trace) {
	$.ajax({
		method: "POST",
		url: "/log/jserror",
		data: {
			message: message,
			category: category,
			time: (new Date()).getTime() / 1000,
			trace: JSON.stringify(trace, null, 2)
		},
		success: function(data) {
			if (data === 'abort') {
				// The server told us to abord because we are in test mode
				window.alert("JS ERROR IN TEST MODE : " + message);
			}
		}
	});
	if (category === "error") {
		console.error(message);
	} else if (category === "warning") {
		console.warn(message);
	} else {
		console.log(message, category, trace);
	}
}

/**
 * Désactive le retour à la page précédente par simple appui sur "backspace",
 * si le navigateur web a encore ce fonctionnement.
 */
function disableBackspaceNavigation() {
	var pressed = false;
	$(document).keydown(function(event){
		if (event.which === 8) {
			pressed = true;
		}
	});
	$(document).keyup(function(event){
		if (event.which === 8) {
			pressed = false;
		}
	});
	window.addEventListener("beforeunload", function (e) {
		if (pressed) {
			pressed = false;
			e.preventDefault();
			return __("Confirmer ?");
		}
		else { // TRACE
			if (window.global_scope == "default" || window.global_scope == "view") {
				$.ajax({
					method: "POST",
					url: "/report/"+window.global_id_report+"/trace",
					async: false, // Keep async false for Firefox. Otherwise error : empty response
					data: {
						trace: 'LeaveReport',
					},
					global: false,
					error: function() {}
				});
			}
		}
	});
}


/**
 *
 * @param {string} message insert multi-line messages with <br>
 * @param {string} category : "success"|"info"|"maintenance"|"warning"|"danger"|"error"  danger is default
 * @param {number} duration default is 5000 (5 seconds), minimum is 1000 and 0 if the message doesn't disappear
 * @return {undefined}
 */
// TODO use constants for AlertGently
function alertGently(message, category = 'danger', duration = 5000) {
	if (category === 'error') {
		category = 'danger';
	}
	if (duration > 0 && duration < 1000) {
		duration = 5000;
	}

	let alertBlock = document.createElement('div');
	alertBlock.className = "alert alert-" + category;
	alertBlock.innerHTML = '<span onclick="this.parentNode.remove()" class="alert-close">&#10006;</span>' + message;
	let container = document.querySelector("#page-alerts");
	if (!container) {
		container = document.body;
	}
	container.appendChild(alertBlock);
	if (duration > 0) {
		setTimeout(
			function() {
				if (alertBlock && alertBlock.parentNode) {
					alertBlock.parentNode.removeChild(alertBlock);
				}
			},
			duration
		);
	}
}

/**
 * Call a function with a delay in millisecond
 * @param func
 * @param delay
 * @returns {(function(...[*]): void)|*}
 */
function debounce(func, delay) {
	let timer;
	return function (...args) {
		const context = this;
		clearTimeout(timer);
		timer = setTimeout(() => func.apply(context, args), delay);
	};
}

function modalConfirm(message, category, buttons) {
	if (typeof category === 'undefined' || category === '') {
		category = 'info';
	}
	if (category === 'error') {
		category = 'danger';
	}
	if ($("#modal-confirm").length === 0) {
		$('body').after('<div id="modal-confirm" title="' + __('Confirmation') + '"><div class="alert"></div></div>');
	}
	$("#modal-confirm > .alert")[0].className = 'alert alert-' + category;
	$("#modal-confirm > .alert").html(message);
	$("#modal-confirm").dialog({
		draggable: false,
		resizable: false,
		position: { my: "center", at: "center top+20%", of: window },
		height: "auto",
		minHeight: "40%",
		width: "50%",
		maxWidth: "60ex",
		modal: true,
		buttons: buttons
	});
}


function getBrowserName(){
	var nAgt = navigator.userAgent;
	var browserName  = navigator.appName;
	var nameOffset,verOffset,ix;

	// In Opera, the true version is after "Opera" or after "Version"
	if ((verOffset=nAgt.indexOf("Opera"))!=-1) {
		browserName = "Opera";
	}
	// In MSIE, the true version is after "MSIE" in userAgent
	else if ((verOffset=nAgt.indexOf("MSIE"))!=-1) {
		browserName = "Microsoft Internet Explorer";
	}
	// In Chrome, the true version is after "Chrome"
	else if ((verOffset=nAgt.indexOf("Chrome"))!=-1) {
		browserName = "Chrome";
	}
	// In Safari, the true version is after "Safari" or after "Version"
	else if ((verOffset=nAgt.indexOf("Safari"))!=-1) {
		browserName = "Safari";
	}
	// In Firefox, the true version is after "Firefox"
	else if ((verOffset=nAgt.indexOf("Firefox"))!=-1) {
		browserName = "Firefox";
	}
	// In most other browsers, "name/version" is at the end of userAgent
	else if ( (nameOffset=nAgt.lastIndexOf(' ')+1) <
		(verOffset=nAgt.lastIndexOf('/')) )
	{
		browserName = nAgt.substring(nameOffset,verOffset);
		fullVersion = nAgt.substring(verOffset+1);
		if (browserName.toLowerCase()==browserName.toUpperCase()) {
			browserName = navigator.appName;
		}
	}
	return browserName;
}

/**
 * Returns the Csrf token
 * @param {string} type 'raw'|'input'
 */
function getCsrfToken(type) {
	var token = document.head.querySelector('meta[name="csrf-token"]').content;
	if(type === 'input'){
		return "<input type='hidden' name='_token' value='"+token+"'>";
	}
	return token;
}

function isNetworkError(jqXHR, thrownError) {
	return jqXHR.statusText != 'abort' && (thrownError === 'timeout' || jqXHR.statusText === 'timeout' || jqXHR.status === 0);
}

/**
 * A default error handler for AJAX errors.
 * See http://api.jquery.com/ajaxError/ but the documentation is incomplete (e.g. nothing about timeout)
 *
 * @param {Event} event
 * @param {jqXHR} jqXHR
 * @param {object} settings
 * @param {string} thrownError
 * @return {undefined}
 */
function defaultAjaxErrorHandler(event, jqXHR, settings, thrownError){
	console.error("Erreur AJAX: ", thrownError, " - HTTP ", jqXHR.status, " request: ", settings.url, ' + ', settings.data);
	if (jqXHR.status === 401) {
		alertGently( __("Votre session a expiré. Merci de vous reconnecter.") , 'danger');
		logout();
	} else if (isNetworkError(jqXHR, thrownError)) {
		alertGently( __("Le serveur n'a pas répondu. Erreur réseau ?") );
	}
}
if (typeof $ !== 'undefined') {
	$(document).ajaxError(defaultAjaxErrorHandler);
}

/**
 * Sends a fetch request with a timout and  killing duplicates requests (before
 * sending the fetch) and waits for it
 * Inspired by https://dmitripavlutin.com/timeout-fetch-request/
 * @return {response}
 */
async function fetchWitoutDuplicates(url, options) {
	const { timeout = 8000 } = options
	if (window.fetchController === undefined) {
		window.fetchController = [];
	}
	if (window.fetchController[url]) {
		window.fetchController[url].abort();
	}
	const controller = new AbortController();
	window.fetchController[url] = controller
	const id = setTimeout(() => controller.abort(), timeout);
	const response = await fetch(url, {
		...options,
		signal: controller.signal
	});
	clearTimeout(id);
	window.fetchController[url] = null;
	return response;

}

/**
 * Disables a dom and show a contained i.spinner
 * @param {DOM} dom
 */
function disableAndShowSpinner(dom) {
	$(dom).prop('disabled', true);
	$(dom).find('i.fa-spinner').show()
}

/**
 * Enables a dom and show a contained i.spinner
 * @param {DOM} dom
 */
function enableAndHideSpinner(dom) {
	$(dom).prop('disabled', false);
	$(dom).find('i.fa-spinner').hide()
}

/**
 * Protect a string for inclusion in HTML, either in text node or in an attribute (wrapped in " or ').
 *
 * @param {string} s
 * @return {string}
 */
function escapeHTML(s) {
	if (!s) {
		return '';
	}
	return $('<div>').text(s).html();
}

/**
 * Protect a string for inclusion in an HTML/XML text node.
 *
 * @param {string} s
 * @return {string} s where &<> are protected
 */
function escapeXML(s) {
	return s
		.replace(/&/g, '&amp;')
		.replace(/</g, '&lt;')
		.replace(/>/g, '&gt;');
}

/**
 * Unprotect a string from an HTML/XML text node
 * @param {string} s
 * @return {string} s with &<> characters
 */
function unEscapeXML(s) {
	return s
		.replace(/&amp;/g, '&')
		.replace(/&lt;/g, '<')
		.replace(/&gt;/g, '>');
}

// ********************************************************************************************************
//             Functions TinyMCE & texts with formulas
// ********************************************************************************************************

/**
 * Render EW formulas with Katex
 * @param {string} selector
 */
function katexRenderFormulas(selector) {
	// console.log('Start Katex Renderer ' + selector);
	if (typeof selector === 'undefined' || !selector) {
		selector = "";
	}
	let spans = window.document.querySelectorAll(selector + ' .ew_ltxformula');

	for (let i = 0; i < spans.length; i++) {
		let span = spans[i];
		let equation = span.getAttribute("data-katex");
		let epsidata = span.getAttribute("data-eplw");
		let display_true = false;
		if (epsidata.lastIndexOf("% display") > 0) {
			display_true = true;
		}
		try {
			katex.render(equation, span, {
				displayMode: display_true,
				throwOnError: false,
			});
		} catch (error) {
			katex.render(equation, span, {
				displayMode: false,
				throwOnError: false,
			});
		}
	}
}

/**
 * Typeset the formulas that are inside the target
 *
 * @param {String} selector
 * @return {undefined}
 */
function typesetHtmlWithFormulas(selector) {
	try {
		katexRenderFormulas(selector);
	} catch (e) {
		StackTrace.fromError(e).then(function (stackframes) {
			logError("Katex failed", "error", stackframes);
		});
	}
}

/**
 * Set the HTML content then typeset the math formulas in the text
 *
 * @param {String} selector
 * @param {String} data
 * @param {Function} callback
 * @return {undefined}
 */
function updateHtmlWithFormulas(selector, data, callback=()=>void(0)) {
	$(selector).html(data);
	requestAnimationFrame(() => requestAnimationFrame(function(){ // wait for the DOM being updated
		typesetHtmlWithFormulas(selector);
		callback();
	}));
}

// ********************************************************************************************************
//             Fonctions de formatage de texte
// ********************************************************************************************************

// renvoie un objet XML en string
function XMLToString(xmlData)  {
	var xmlString;
    if (window.ActiveXObject){ //IE
        xmlString = xmlData.xml;
    }
	else { // code for Mozilla, Firefox, Opera, etc.
        xmlString = (new XMLSerializer()).serializeToString(xmlData[0]);
    }
    return xmlString;
}


// ********************************************************************************************************
//             Fonctions des widgets
// ********************************************************************************************************

function widgFocus(objId) {  // passe le widget au dessus des autres : 200 est considéré comme le haut de pile
	$('.widget').css("z-index", "+=-1") ; // on baisse tout le monde d'un cran
	// et on remet notre focus sur le dessus
	/*var widg = document.getElementById(objId);
	widg.style.zIndex = 200 ;*/
	$("#"+objId).css("z-index", "200") ;
}

/*
 * Open or close the widget window
 * @param {comments, ressource, messagerie, scoring} objId : widget id
 * @param {type} id_ld : if the widget is the comment widget, the ld commented
 * @param {type} ld_name : if the widget is the comment widget, the name of the ld commented
 */
function toggleWidget(objId, id_ld, ld_name) {
	let widg = document.getElementById(objId);
	if (!widg) {
		return;
	}
	if (widg.style.display !== 'block'){ // on ouvre le widget
		// on vérifie que notre widget est bien dans la fenêtre sinon on le replace à gauche et en haut
		let leftmax = $(window).width()-20;
		let topmax = $(window).height()-20;
		if (parseInt(widg.style.left) >= leftmax) { widg.style.left = String(leftmax)+'px' ; }
		if (parseInt(widg.style.top) >= topmax) { widg.style.top = String(topmax)+'px' ; }
		widg.style.display = 'block';
		if (objId === WIDGET.MESSAGING) {
			displayConversations();
		}
		if (objId === WIDGET.TASK) {
			sendTaskOpenWidgetTrace(window.global_id_report);
		}
    } else { // on ferme le widget
		widg.style.display = 'none';
		if (objId === WIDGET.MESSAGING) {
			$("#msg_id_conv").val(0);
		}
		if (objId === WIDGET.TASK) {
			sendTaskCloseWidgetTrace(window.global_id_report);
		}
	}
	let evt = new CustomEvent(widg.style.display === 'none' ? 'widgetHidden': 'widgetShown');
	if (objId !== WIDGET.RECOVER_PASSWORD) {
		widg.dispatchEvent(evt);
		widgSaveParam(objId) ;
	}
}

/*
 * permet de sauvegarder les paramètres des ressources : taille, position, ouvert ou non
 * @param {comments, ressource, messagerie, scoring} objId : widget id
 */
function widgSaveParam(objId) {
	console.log("widgSaveParam", objId);
	let widg = $(".widget#"+objId) ;
	let vis;
	if (widg.css("display") == "none") {
		vis = 0 ;
	} else if (objId == WIDGET.COMMENTS) {
		vis = $("#com_id_ld").val() ;
	} else {
		vis = 1 ;
	}
	$.ajax({
		type: "POST",
		url: "/widget/saveParams",
		data: {
			widg: objId,
			x: widg.offset().left,
			y: (widg.offset().top-$(window).scrollTop()),
			h: widg.height(),
			w: widg.width(),
			vis: vis
		},
		error:function(msg){ alert( "Erreur 26: " + msg) ; },
		success:function(data){ if (data) { alert(data) ; } }
	});
}

/**
 * Add messages to the widget and scroll or add an unread arrow
 * @param {html} content
 * @param {string} type
 * @param {boolean} scroll
 */
function updateWidgetComMsg (content, type, scroll){
	if (type === "msg") {
		var elem = $('#msg_msg_messages');
		var prefix = 'msg_msg';
	} else if (type === "com") {
		var elem = $('#comment_messages');
		var prefix = 'comment';
	}	
	if (!scroll) {
		scroll = elem.scrollTop() == elem[0].scrollTopMax;
	}
	elem.html(content);
	if (scroll){
		elem.scrollTop(2000000) ; // aller en bas de la liste des msg
		$("#messagerie.student").closest('.widget-wrapper').scrollTop(2000000) ;
	} else {
		var arrow = '<i id="'+prefix+'_new_arrow" class="fa fa-arrow-circle-down new_com_msg" title="Nouveau(x) Message(s)" onclick="$(\'#'+prefix+'_messages\').scrollTop(2000000)"></i>';
		elem.append(arrow);
		setTimeout(
			"$('#"+prefix+"_new_arrow').remove();",
			10000
		);
	}
}

// ********************************************************************************************************
//             Fonctions de view
// ********************************************************************************************************

function lbModal(display) {
	if (display) {
		var position = $("body").position();
		$("#lb_modal_cover").css('top', position.top);
		$("#lb_modal_cover").css('left', position.left);
		$("#lb_modal_cover").height('100vh') ;
		$("#lb_modal_cover").width('100vw') ;
		$("#lb_modal_cover").show() ;
	}
	else {
		$("#lb_modal_cover").hide() ;
	}
}

/**
 * makes sure that the block is displayed at its best in screen
 * @param {string}selector
 */
function scrollToDisplay(selector) {
	if ($(selector).length === 0) {
		return;
	}
	let overflow = (10 + $(selector).offset().top + $(selector).outerHeight()) - ($(document).scrollTop() + $(window).height()) ; // 10 for nicer rendering
	// console.log($(selector).offset().top , $(selector).outerHeight(), $(document).scrollTop(), $(window).height()) ;
	if (overflow > 0) {
		$(document).scrollTop($(document).scrollTop()+overflow);
	}
	overflow = $(document).scrollTop() + 40 - $(selector).offset().top ; // the 40 is for the menu on top of the page
	if (overflow > 0) {
		$(document).scrollTop($(selector).offset().top - 40);
	}
}

// ********************************************************************************************************
//             Fonctions Gestions compte utilisateur
// ********************************************************************************************************

/**
* Affiche une fenêtre modale permettant de visualiser les informations du compte (identifiant, nom etc.) et mettre à jour l'email, le mdp et l'identifiant.
* @param id int user id
**/
function openAccountManagement(id){
	// remove previous account modal
	document.querySelectorAll('#modal-account').forEach(el => el.remove());
	$.ajax({
		type: 'GET',
		url: '/user/'+id+'/edit/',
		data: {},
		error: function(msg){
			alert("Erreur d'initialisation : " + msg);
			},
		success: function(data){
			if (!data.success) {
				alertGently(data.msg, 'danger');
			} else {
				let div = document.createElement('div');
				div.innerHTML = data.html.trim();
				document.body.appendChild(div.firstChild);
			}
		}
	});
}

/**
* Envois les mise à jour effectué par l'utilisateur sur son compte en AJAX pour traitement
* Le résultat (succès ou echec) des MAJ est affiché dans un alert
* On récupère les informations par l'ID des input associé dans la div généré par openAccountManagement
**/
function updateUser( id=-1, token="", login_edition=false, password_edition=false){

	// Get inputs
	const user_name = document.getElementById('c_user_name').value;
	const first_name = document.getElementById('c_first_name').value;
	const inst_number = document.getElementById('c_inst_number');                   // non présent si le numéro d'étudiant est vide
	const email = document.getElementById('c_email');                         // peut être vide si l'étudiant décide d'effacer son adresse mail
	let lang = document.getElementById('c_lang').value;
	if (window.labnbook.i18n && lang !== "" && lang !== i18next.language) {
		i18next.changeLanguage(lang);
	}
	const choose_cas_checkbox = document.getElementById('c_choose_cas_authentication');   // non présent si l'utilisateur utilise déjà le CAS ou s'il n'y a pas de CAS dans l'institution
	let choose_cas_authentication = choose_cas_checkbox ? choose_cas_checkbox.checked : false;   // non présent si l'utilisateur utilise déjà le CAS ou s'il n'y a pas de CAS dans l'institution
	const login = document.getElementById('c_login');                         // non présent si l'utilisateur utilise déjà le CAS
	const password =  document.getElementById('prev_pwd');                    // non présent si l'utilisateur utilise déjà le CAS
	const new_password = document.getElementById('c_pwd');                    // non présent si l'utilisateur utilise déjà le CAS
	const new_confirm_password = document.getElementById('c_pwd_2');          // non présent si l'utilisateur utilise déjà le CAS
	const code = document.getElementById('c_code').value;
	console.log(email.dataset);

	if (email) {
		if(!email.dataset.instLogin && email.dataset.previousEmail !== '' && email.value === '') {
			if(!confirm(__("Sans adresse e-mail vous ne pourrez plus récupérer un mot de passe oublié"))){
				return ;
			}
		} 
		if(email.value !== '' && !email.value.toLowerCase().match(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)){
			window.dispatchEvent(new CustomEvent('modal-alert-message', {detail: {message: __('Votre e-mail est mal formé')}}));
			return ;
		}
	}
	if (!choose_cas_authentication && login) {
		if (login.value === '') {
			window.dispatchEvent(new CustomEvent('modal-alert-message', {detail: {message: __('Vous devez définir un identifiant (login)')}}));
			return ;
		}
		if(login_edition && password.value === '') {
			window.dispatchEvent(new CustomEvent('modal-alert-message', {detail: {message: __("Mot de passe requis pour changer l'identifiant")}}));
			return ;
		}
		if(password_edition) {
			if( password.value === '') {
				window.dispatchEvent(new CustomEvent('modal-alert-message', {detail: {message: __('Pour modifier votre mot de passe, vous devez indiquer votre ancien mot de passe')}}));
				return ;
			}
			if ( new_password.value === '' || new_confirm_password.value === '') {
				window.dispatchEvent(new CustomEvent('modal-alert-message', {detail: {message: __("Pour définir un nouveau mot de passe, entrez le deux fois dans les champs adéquats")}}));
				return ;
			}
			if ( new_password.value !== new_confirm_password.value ) {
				window.dispatchEvent(new CustomEvent('modal-alert-message', {detail: {message: __("Définition d'un nouveau mot de passe : vos deux entrées ne correspondent pas")}}));
				return ;
			}
			if ( new_password && (new_password.value.length<8 || new_password.value.length>20
				|| new_password.value === new_password.value.toUpperCase()
				|| new_password.value === new_password.value.toLowerCase())) {
				window.dispatchEvent(new CustomEvent('modal-alert-message', {detail: {message: __("Votre mot de passe doit comprendre entre 8 et 20 caractères avec des lettre minuscules et majuscules")}}));
				return ;
			}
		}
	}
	$.ajax({
		type: 'POST',
		url: '/user/'+id+'/update',
		data: {
			user_name: user_name,
			first_name: first_name,
			inst_number: inst_number?inst_number.value:'',
			email: email?email.value:'',
			login_edition: (!choose_cas_authentication && login_edition)?1:0,
			password_edition: (!choose_cas_authentication && password_edition)?1:0,
			login: login?login.value:'',
			password: password?password.value:'',
			new_password: new_password?new_password.value:'',
			new_confirm_password: new_confirm_password?new_confirm_password.value:'',
			code: code,
			lang: lang,
			_token: token,
		},
		success: function(msg){
			let resp = JSON.parse(msg);
			if (resp.success) {
				alertGently(__("Mise à jour effectuée : ") + '<br> - ' + resp.success, "success");
				if (window.labnbook.lang !== resp.lang) {
					setTimeout("location.reload();", 2000);
				}
			}
			if (resp.error) {
				alertGently(__("Problèmes de mise à jour : ") + '<br> - ' + resp.error, "danger", 10000);
			}
			if (!resp.error && choose_cas_authentication) {
				alert(__("Vous allez être redirigé vers la page de connexion de votre institution."));
				window.location='/auth/cas/confirm';
			}
		},
		error: function (jqXHR) {
			if (jqXHR.status === 422 && 'email' in jqXHR.responseJSON.errors) {
				alertGently(__("Adresse email incorrecte"), "danger");
			} else {
				alertGently(jqXHR.responseJSON.message);
			}
		}
	});
	return true;
}


function openToolTipEditUser(){
	var msg_title, msg_content;
	msg_title    =  __('Compte institutionnel');
	msg_content  = __('En activant cette option, vous vous authentifierez dorénavant avec votre compte institutionnel.');
	global_tooltip_popup.openTooltipPopup(msg_title, msg_content,"#c_inst_account_tooltip");
}

/*******************************************
 *  Common pop up
 ********************************************/

var global_confirmation_popup = function () {
	var submitCallback;
	var cancelCallback;
	
	return {
		/**
		 * @param modal_title header text
		 * @param modal_label text to accept
		 * @param modal_callback callback to execute when submit
		 */
		openConfirmationPopup  : function (modal_title, modal_label, modal_submit_callback, modal_cancel_callback, cssClass=null) {
			$("#confirmation-popup-confirm-ok").prop('checked', false);
			if (cssClass) {
				$("#confirmation-popup").addClass(cssClass);
			}
			var modal = $("#confirmation-popup");

			modal.prop('title', modal_title);
			modal.children().find('.popup-title').text(modal_title);
			modal.children().find('span.message').html(modal_label);

			submitCallback = modal_submit_callback;
			cancelCallback = modal_cancel_callback;
			
			modal.dialog("open");
		},
		closeConfirmationPopup : function (){
			var modal = $("#confirmation-popup");
			modal.dialog("close");
		},
		executeSubmitCallBack : function (){
			global_confirmation_popup.closeConfirmationPopup();
			return submitCallback($("#confirmation-popup-id-user").val());
		},
		executeCancelCallBack : function (){
			if(cancelCallback !== undefined){
				return cancelCallback($("#confirmation-popup-id-user").val());
			}
		}
	}
}();

/*/**
 * réinitialise les différent inputs d'un popup
 * @param dialog_id id d'un popup
 */
function resetDialogInputs(dialog_id){
	let inputs = document.getElementById(dialog_id).getElementsByTagName('input');
	for (let i = 0; i < inputs.length; i++) {
		inputs[i].value = "";
	}
}

/*
 * Définit une popup Jquery-UI modale en fonction d'un id et de l'id d'un bouton associé à son ouverture
 * @param dialog_id : id de la popup, suffixe des actions de validation et annulation
 * @param button_id : id du bouton associé à l'ouverture de la popup
 * @param reset_input : boolean
 * @param rel_pos_id : id of the relative container
 */
function defineModal(dialog_id, button_id, reset_input, rel_pos_id){
	let d_id = "#"+dialog_id;
	$(d_id).dialog({
		autoOpen: false,
		modal: true,
		resizable: false,
		fluid: true
	});
	$(d_id).draggable({
		handle:'header',
		containment: 'document'
	});
	if (button_id) {
		$("#"+button_id).click(function() {
			$(d_id).dialog("open");
		});
	}
	$(d_id+"-cancel").click(function() {
		if (reset_input) {
			resetDialogInputs(dialog_id);
		}
		$(d_id).dialog("close");
	});

	// positioning			console.log($tooltip_popup.position());

	$(d_id).dialog('option','width', 'auto');
	if (rel_pos_id) {
		$(d_id).dialog("option", "position", { my: "right top", at: "right+50 top+30", of: "#"+rel_pos_id } );
	} else {
		$(d_id).dialog("option", "position", { my: "center top", at: "center top+100", of: window } );
	}

}

/*******************************************
 *  Document ready
 ********************************************/

$(document).ready(function() {
	defineModal("confirmation-popup", null, true, null);
	$("#confirmation-popup").on("click", ".popup-submit", function(event) {
		if ($("#confirmation-popup-confirm-ok").prop('checked')) {
			event.stopPropagation();
			global_confirmation_popup.executeSubmitCallBack();
		}
	});
	$("#confirmation-popup").on("click", ".popup-cancel", function(event) {
		$("#confirmation-popup i.fa-check").addClass("disabled");
		global_confirmation_popup.executeCancelCallBack();
	});
	$("#confirmation-popup-confirm-ok").on("click", function() {
		$("#confirmation-popup i.fa-check").toggleClass("disabled");
	});

	// on window resize run function
	$(window).resize(function () {
		fluidDialog();
	});
	
	// catch dialog if opened within a viewport smaller than the dialog width
	$(document).on("dialogopen", ".ui-dialog", function (event, ui) {
		fluidDialog();
	});

});
function fluidDialog() {
	var $visible = $(".ui-dialog:visible .cgu").closest("div.ui-dialog");
	// each open dialog
	$visible.each(function () {
		var $this = $(this);
		var dialog = $this.find(".ui-dialog-content").data("ui-dialog");
		// if fluid option == true
		if (dialog.options.fluid) {
			var wWidth = $(window).width();
			// check window width against dialog width
			if (wWidth < (parseInt(dialog.options.maxWidth) + 50))  {
				// keep dialog from filling entire screen
				$this.css("max-width", "75%");
			} else {
				// fix maxWidth bug
				$this.css("max-width", dialog.options.maxWidth + "px");
			}
			//reposition dialog
			dialog.option("position", dialog.options.position);
		}
	});
}

/*************************************************
 * 				Tooltip partagé
 **************************************************/

/**
 * Affiche la popup d'un tooltip au bon endroit pour qu'il ne dépasse pas de l'écran
 * @param $tooltip_popup : Popup créée à partir du tooltip
 * @param $teachers_tooltip : Tooltip à partir duquel est affiché la popup
 * @param id_positon : Element utilisé pour placer le tooltip
 */

function positionPopup($tooltip_popup, $tooltip, id_positon) {

	let pos_viewport_tooltip = $tooltip[0].getBoundingClientRect();
	let os = $(id_positon).offset();
	let viewportWidth = $(window).width();
	let viewportHeight = $(window).height();
	let tt_height = $("#tooltip_popup").height();
	let tt_width = $("#tooltip_popup").width();
	let tt_top, tt_left;

	if (viewportHeight - pos_viewport_tooltip.bottom > tt_height + 50) {
		tt_top = os.top + 20;
	} else {
		tt_top = os.top - tt_height - 20;
	}
	if (pos_viewport_tooltip.left < viewportWidth / 3) {
		tt_left = os.left - 10;
	} else if (pos_viewport_tooltip.left > 2*viewportWidth / 3) {
		tt_left = os.left - tt_width;
	} else {
		tt_left = os.left - tt_width / 2;
	}
	$tooltip_popup.css({top: tt_top, left: tt_left});
}

/**
 * Affiche un tooltip d"information proche du bouton de déclenchement
 * @param title string : chaine qui sera placee en Titre
 * @param comment string : chaine (code html) qui contitue le message affiche
 * @param id_positon : Element utilisé pour placer le tooltip
 */
var global_tooltip_popup = function () {
	return {
		/**
		 * Affiche un tooltip d"information proche du bouton de déclenchement
		 * @param title string : chaine qui sera placee en Titre
		 * @param comment string : chaine (code html) qui contitue le message affiche
		 * @param id_positon string : Element utilisé pour placer le tooltip
		 */
		openTooltipPopup: function (title, comment, id_positon) {
			var $tooltip_popup = $("#tooltip_popup");
			$("#tooltip_popup_title").html(title);
			$("#tooltip_popup_comment").html(comment);
			var $tooltip = $(id_positon);
						
			positionPopup($tooltip_popup, $tooltip, id_positon);

			$tooltip_popup.show();
			
			var openMethod=event.type
			// Used to stop current click
			event.stopPropagation();
			
			if(openMethod=="click"){
				// hide when clicking anywhere (tablet / phone)
				document.addEventListener(
					"click",
					function myListener(event) {
						$("#tooltip_popup").hide();
						// Used named function to remove listener
						document.removeEventListener("click", myListener);
					},
					false
				);
			} else {
				// openMethod = mouseover
				// hide when mouse over (mouse)
				document.addEventListener(
					"mouseout",
					function myListener(event) {
						$("#tooltip_popup").hide();
						// Used named function to remove listener
						document.removeEventListener("mouseout", myListener);
					},
					false
				);
			}
		}
	}
}();

/**
 * Store the fact that the user just accepted the CGUs
 */
function acceptCGUs() {
	$.post("/user/acceptConditions");
}

/**
 * Retrieve the list of users that we can see for the given filters and adds them
 * as options in the selector and transform it in a select2
 * @param {string} selector : selector in which we should add the options, should be a select
 * @param {string} parent_selector : the parent of the selector, useful if in a modal
 * @param {string} placeholder : the placeholder for the select
 * @param {string} type : student|teacher|both
 * @param {int} include_my_teams : should we add teacher teams
 * @param {int} id_inst : the id_inst to use if not set, use the default of the user
 * @param {array} exclude_ids : user ids to exclude
 * @param id_report : report id
 */
function load_add_users (selector, parent_selector, placeholder, type, include_my_teams, id_inst = 0, exclude_ids = [], id_report = null) {
	const configSelect2 = {
		url: '/user',
		data: {
			exclude_ids: exclude_ids,
			type: type,
			include_my_teams: include_my_teams,
			id_inst: id_inst,
			id_report: id_report
		},
		groups: [
			{
				text: __('Equipes'),
				childrenLabel: 'teams',
				paginated: false
			},
			{
				text: __('Utilisateurs'),
				childrenLabel: 'users',
				paginated: true
			}
		],
		placeholder: placeholder,
		parentSelector: parent_selector,
		clearOnSelect: true,
	}
	window.dispatchEvent(new CustomEvent('lnb-select2-load', {
		detail:{
			selector: selector,
			configSelect2: configSelect2
		}}));
}

/**
 * Select2 alpine component, it can be configure with an url, 
 * a data object to filter the query, 
 * different groups to process the results
 * a placeholder and a parent selector
 * @param selector
 * @returns {{selector, configSelect2: {}, selectedElement: null, initSelect2(*): Promise<void>}}
 */
function select2Component(selector) {
	return {
		selector:selector,
		configSelect2: {},
		selectedElement: null,
		initialized: false,
		select2Loaded: false,
		getDataFromAlpineStore() {
			if(this.configSelect2.alpineStore) {
				for(let storeObject of this.configSelect2.alpineStore) {
					this.configSelect2.data[storeObject.storeAttributeName] = Alpine.store(storeObject.storeIdentifier)[storeObject.storeAttributeName];
				}
			}
			return this.configSelect2.data;
		},
		initSelect2(configSelect2){
			this.configSelect2 = configSelect2;
			this.select2Loaded = false;
			let self = this;
			$(this.$refs.select2).select2({
				ajax: {
					url: self.configSelect2.url,
					dataType: 'json',
					delay: 250,
					data: function(params){
						let data = self.getDataFromAlpineStore(); 
						data.search = params.term;
						data.page = params.page || 1;
						data.pageSize = 15;
						return data;
					},
					processResults: function (data) {
						let results = [];
						for(let group of self.configSelect2.groups) {
							if(data[group.childrenLabel].length>0 && parseInt(data.pagination.page) === 1) {
								// first page we display the group
								results.push({
									text: group.text,
									children: data[group.childrenLabel]
								});
							} else if(group.paginated && parseInt(data.pagination.page) > 1) {
								// next pages we just add the new results 
								results = data[group.childrenLabel];
							}
						}
						self.select2Loaded = !self.select2Loaded;
						return {
							results: results,
							pagination: {
								more: data.pagination.more
							}
						};
					},
					cache: true
				},
				language: {
					searching : () => "<i class='fa fa-spinner fa-pulse'></i> "+__("Chargement en cours"),
					loadingMore : () => "<i class='fa fa-spinner fa-pulse'></i> "+__("Chargement de plus de résultats"),
					noResults : () => __("Aucun résultats trouvés"),
					removeAllItems : () => __("Déselectionner tout"),
				},
				escapeMarkup: function (markup) {
                    return markup; // Allows HTML rendering
                },
				allowClear: true,
				placeholder: self.configSelect2.placeholder,
				dropdownParent: self.configSelect2.parentSelector?$(self.configSelect2.parentSelector):null,
				dropdownAutoWidth: true,
				width: self.configSelect2.width?self.configSelect2.width:'auto'
			}).on("select2:clear", () => {
				self.clearing = true;
			}).on('select2:open', () => {
				if(self.clearing) {
					// Not open the dropdown if it's from a clearing button
					self.clearing = false;
					$(self.$refs.select2).select2('close');
				}
			}).on('select2:select', function (event) {
				self.selectedElement = event.params.data; // Bind selected value to Alpine state
				self.addId(self.selectedElement.id);
			}).on('change.select2', function(event) {
				if(event.target.value && !self.selectedElement) {
					const selectedOption = Array.from(event.target.options).find((option) => option.value === event.target.value);
					self.selectedElement = {
						id: selectedOption.value,
						text: selectedOption.label
					}
				} else {
					self.selectedElement = null;
				}
			});
			this.initialized = true;
			this.$watch('select2Loaded', () => {
				setTimeout(() => {
					const searchInput = document.querySelector('.select2-search__field');
					// Ensure focus when select2 data is loaded
					if(searchInput) {
						searchInput.focus();
					}
				}, 100);
			});
			window.dispatchEvent(new CustomEvent('lnb-select2-component-initialized', { detail : {selector:this.selector}}));
		},
		addId(idElement) {
			if(this.configSelect2.data && this.configSelect2.data.exclude_ids) {
				this.configSelect2.data.exclude_ids.push(idElement);
			}
		},
		removeId(event) {
			if(event.detail.selector === this.selector 
				&& this.configSelect2.data && this.configSelect2.data.exclude_ids) {
				const idx = this.configSelect2.data.exclude_ids.findIndex((id) => parseInt(id) === parseInt(event.detail.idElement));
				if(idx>=0) {
					this.configSelect2.data.exclude_ids.splice(idx,1);
				}
			}
		}
	}
}



/**
 * Creates a span which a delete button to add the user in a list before validation
 * @param {string} id : the user id
 * @param {string} name : the user display name
 * @param {string} staging_selector : selector of the staging area where we should append the span
 * @param {string} data_name : name of the data holding the id i.e id-teacher or id-user
 * @param {string} delete_callback : javascript to put in onclick on the trash icon
 * @param {string} title : html title for the span
 */
function add_user_in_staging_from_id_and_name(id, name, staging_selector, data_name, delete_callback, title) {
	let data_attr = 'data-' + escapeHTML(data_name);
	if ($('span[' + data_attr + '=' + escapeHTML(id) +']').length == 0) {
		var html = '<span ' + data_attr + '="' + escapeHTML(id) + '"> - ' + escapeHTML(name.replace(/ \(.*\)/, ''));
		html += '&nbsp;<i class="far fa-trash-alt" onclick="' + delete_callback + '" title="'+ escapeHTML(title) + '"></i></span>';
		$(staging_selector).append(html);
	}
}

/**
 * Creates a span which a delete button to add the user in a list before validation
 * @param {string} selector : selector of the select containing the selected user to add
 * @param {string} staging_selector : selector of the staging area where we should append the span
 * @param {string} data_name : name of the data holding the id i.e id-teacher or id-user
 * @param {string} delete_callback : javascript to put in onclick on the trash icon
 * @param {string} title : html title for the span
 */
function add_user_in_staging (selector, staging_selector, data_name, delete_callback, title='') {
	const id = $(selector).val();
	var name = $(selector + ' option:selected').text();
	var pos = name.indexOf('(');
	if (pos > 0) {
		name = name.substr(0, pos).trim();
	}
	if (id) {
		add_user_in_staging_from_id_and_name(id, name, staging_selector, data_name, delete_callback, title);
	}
}

/**
 * Returns alpine x-data for a givent element from id
 * @param {string} id
 * @return {object}
 */
function getAlpineData(id) {
	return Alpine.$data(document.getElementById(id));
}

/**
 * Handle the resizing of a div vertically or horizontally.
 * When the mouse is  pressed down, we record its position. As it moves, we increase or
 * decrease the zize up to the limit. When the button is released, we stop dragging.
 * The sizing bar can be inside or beside the element to resize.
 * @param element : dom element to resize
 * @param selector : string selector for retrieving the dom element if not available
 * @param direction : "horizontal", "vertical"
*/
/* function initSizingBar(element, selector = "", direction = "vertical") {

	if (selector) {
		element = $(selector);
	}
	console.log(element);

	const min_size = 100;
	let resizing = false;
	let move = 0, original_size = 0;

	let sizing_bar = $(element).parent().find(".sizing-bar"); // the sizing bar is in or beside the element
	if (!sizing_bar) {
		return;
	}

	function lnb_mousedown(e) {
		resizing = true;
		if (direction === "horizontal") {
			move = e.pageX;
			original_size = $(element).width();
		} else {
			move = e.pageY;
			original_size = $(element).height();
		}
	}

	function lnb_mousemove(e) {
		if (resizing) {
			console.log("resizing");
			let delta;
			if (direction === "horizontal") {
				delta = e.pageX - move;
				let new_size = Math.max(original_size + delta, min_size);
				$(element).css('max-width', 'none');
				$(element).width(new_size + "px");
			} else {
				delta = e.pageY - move;
				let new_size = Math.max(original_size + delta, min_size);
				$(element).css('max-height', 'none');
				$(element).height(new_size + "px");
			}
			// ctx.resize(); // only for Zwibbler
		}
	}

	function lnb_mouseup(e) {
		resizing = false;
	}

	// The move and up events must be placed on the window, because when
	// the mouse moves it may briefly leave the sizing bar.
	$(sizing_bar).on("mousedown", lnb_mousedown);
	$(document).on("mousemove", lnb_mousemove);
	$(document).on("mouseup", lnb_mouseup);

	$(sizing_bar).on("remove", function () {
		document.removeEventListener("mousemove", lnb_mousemove);
		document.removeEventListener("mouseup", lnb_mouseup);
	})
} */
