// ********************************************************************************************************
//             Fonction lançant l'export PDF et étudiant le succès de cette création
// ********************************************************************************************************

// Affiche le PDF si il a été créé avec succès par Puppeteer (exécuté dans pdf_prepare.php par AJAX) sinon 
// affiche un message d'erreur demandant à l'utilisateur d'utiliser l'outil d'impression de son navigateur.

/*
 */
function prepare_pdf(params) {
	console.log(params);
    $.ajax({
        method: 'POST',
        url: '/prepare_PDF.php',
        data: params,
    })
    .done(function(response) {
		// Suppression du message indiquant que le chargement du PDF est en cours
		let alertLoading = document.querySelector('.alert-info');
		if (alertLoading !== null) {
			alertLoading.remove();
		}

		if (response.success) {
            let filepath = encodeURIComponent(response.filepath);
            let filename = encodeURIComponent(response.filename);
            window.location.href = "/report/displayPDF?filepath="+filepath;
        } else {
            alertGently("Une erreur s'est produite à la création de votre PDF. <br/> Pour imprimer le rapport ou créer une version PDF, utilisez la fonction \"Imprimer\" de votre navigateur.",'danger',500000);
        }
    })
    .fail(function(xhr) {
        console.log('error', xhr);
        alertGently("Une erreur s'est produite à la création de votre PDF. <br/> Pour imprimer le rapport ou créer une version PDF, utilisez la fonction \"Imprimer\" de votre navigateur.",'danger',500000);
    });
};
