// ********************************************************************************************************
//             Fichier nodejs/serveur exécutant Puppeteer afin de créer le PDF d'un rapport
// ********************************************************************************************************

const puppeteer = require('puppeteer');

// Erreur si il manque les paramètres utilisés pour la création du PDF :
if (process.argv.length <= 2) {
    console.error("2 arguments expected, " + process.argv.length + " received.");
    process.exit(1);
}

// Les paramètres sont un tableau JSON passé en argument dans la ligne de commande exécutant ce fichier, on
// les récupère dans une variable pour simplifier les choses :
const [bin, sourcePath, ...args] = process.argv;
var params = JSON.parse(args[0]);
if (!params.hasOwnProperty('filepath') || !params.filepath) {
    console.error("Parameter 'filepath' is empty or missing. Aborting.");
    process.exit(2);
}

// Définition d'un timer au cas où une erreur a lieu afin de stopper l'exécution de la commande depuis PHP
// et ne pas avoir de boucle infinie :
let timeoutChecker;

// Lancement de Puppeteer en mode Headless avec le chemin vers la version officielle de Chromium (résout des
// problèmes de droit d'écriture et de lecture des PDF) :
(async () => {
    const browser = await puppeteer.launch({
        executablePath: '/usr/bin/chromium',
        args: ['--no-sandbox', '--disable-web-security', '--disable-dev-profile', '--headless=new'],
    });
    const context = await browser.createIncognitoBrowserContext();

    const page = await context.newPage();
    page.on('error', error => {
        console.warn(params);
        console.error('Chromium Tab CRASHED', error);
    });
    // Catch all failed requests like 4xx..5xx status codes
    page.on('requestfailed', request => {
        console.log(`url: ${request.url()}, errText: ${request.failure().errorText}, method: ${request.method()}`)
    });
    // Catch console log errors
    page.on("pageerror", err => {
        console.log(`Page error: ${err.toString()}`);
    });
    // Catch all console messages
    page.on('console', msg => {
        console.log('Logger:', msg.type());
        console.log('Logger:', msg.text());
        console.log('Logger:', msg.location());

    });


    try {
        await page.goto(params.url, {waitUntil: 'networkidle2'});
        page.addStyleTag({path: __dirname + '/../../css/puppeteer.css'})

		let frames = await page.frames().filter(frame => frame.name().startsWith('jupyter_iframe'));
		for (let frame of frames) {
			// Wait for a first cell
			await frame.waitForSelector('#main .jp-MainAreaWidget div.jp-Cell');
			await page.waitFor(1000);
		};
		await page.waitFor(500);

		await page.pdf({
            path: params.filepath,
            printBackground: true,
            format: 'A4',
            displayHeaderFooter: true,
            headerTemplate: '<div id="header-template" style="font-size:7px; color:#808080; display: flex; justify-content: space-between; align-items: center; border-bottom: 1px solid black; margin: 15px 15px 0 15px; width: 19cm; padding: 0 10px 5px 10px;"><div><span>' + params.date + '</span></div><span>' + params.nom + '</span></div>',
            footerTemplate: '<div id="footer-template" style="font-size:7px; color:#808080; display: flex; justify-content: space-between; align-items: center; border-top: 1px solid black; margin: 0 15px 15px 15px; width: 19cm;  padding: 5px 10px 0 10px;"><div></div><div><span class="pageNumber"></span><span>/</span><span class="totalPages"></span></div></div>',
            margin: {
                top: '80px',
                bottom: '80px',
                right: '20px',
                left: '20px',
            }
        });
    } catch (e) {
        console.error("Chromium failed with the error: ", e);
        process.exit(3);
    }
    await browser.close();
    await killProcess(browser);

    // Arrête le timer si l'export PDF est un succès :
    stopTimeoutCheck();
})();

// zombie process
// see https://github.com/GoogleChrome/puppeteer/issues/1825
async function killProcess(browser) {
    const process = browser.process();
    if (process.pid > 0) {
        process.kill('SIGTERM');
        process.kill('SIGKILL');
    }
}

async function timeoutCheck(browser) {
    timeoutChecker = setTimeout(
        async function () {
            await browser.close();
            await killProcess(browser);
        },
        200
    );
}

function stopTimeoutCheck() {
    if (timeoutChecker) {
        clearTimeout(timeoutChecker);
    }
}
