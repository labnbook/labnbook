
$(document).ready(function() {
	// generate auto login
	$("#t-add-name").on('input', function() { 
		autoLogin("user_name", "#t-add-first-name", "#t-add-name", "#t-add-login"); 
	});
	
	// generate auto login
	$("#t-add-first-name").on('input', function() {
		autoLogin("first_name", "#t-add-first-name", "#t-add-name", "#t-add-login");
	});


	$("#t-popup-valid").on("click", function(){
			addTeacher();
	});
});

function getIdInst() {
	if (typeof global_current_id_inst !== 'undefined') {
		return global_current_id_inst;
	}
	return $('#institutions .lba-context-icons').data('params').inst.id;
}

function casEnabled() {
	if (typeof global_ldap !== 'undefined') {
		return global_ldap;
	}
	return parseInt($('#institutions .lba-context-icons').data('params').inst.cas);
}

function openAddTeacher(){
	$("#t-add-first-name").focusout(function() { 
		searchUserBy('name') ; 
	});
	$("#t-add-name").focusout(function() { 
		searchUserBy('name') ; 
	});
	$("#t-add-email").focusout(function() { 
		searchUserBy('email') ; 
	});
	$("#t-popup").dialog("open");
	if (casEnabled()) {
		$("#t-add-ldap").prop('disabled', false);
		$("#t-add-ldap").click();
		$("#t-popup-method").show();
	} else {
		$("#t-add-ldap").prop('disabled', true);
		$("#t-add-form").click();
		$("#t-popup-method").hide();
	}
	$('#t-popup .t-fieldset input').not(':hidden').first().focus();
}

/**
 * fonction searchUserBy
 * cherche un teacher correspondant aux infos saisies
 * si c'est le cas => propose de charger le profil en tant qu'enseigant l'institution
 */
function searchUserBy(what, var1, var2){
	var var1 = "" ;
	var var2 = "" ;
	if (what == "name") {
		var1 = $.trim($('#t-add-first-name').val()) ;
		var2 = $.trim($('#t-add-name').val()) ;
	}
	else if (what == "email") {
		var1 = $.trim($('#t-add-email').val()) ;
	} 
	if ( var1 != "" && (what == "email" || (what == "name" && var2 != "" ))) {
		commonSearchUserBy(what, var1, var2, "all", getIdInst(), function (users) {
			if (users.length === 0) {
				return;
			}
			// affichage d'une fenetre avec des propositions d'étudiant
			var user_html = __("Il existe des utilisateurs avec des informations similaires dans la base LabNBook.") + "<br/>";
			user_html += __("Validez pour ajouter l'utilisateur existant comme enseignant à LabNBook") + __("&nbsp;:") + "<ul>";
			for (var i = 0; i < users.length; i++) {
				/**
				 * @todo Secure this HTML production
				 */
				user_html += "<li>" + users[i].first_name + " " + users[i].user_name + " (" + users[i].login + ") - " + users[i].name;
				if (users[i].inst_number) {
					user_html += " n°" + users[i].inst_number;
				}
				if (users[i].email) {
					user_html += " - " + users[i].email;
				}
				user_html += "&nbsp;&nbsp;<i class='fa fa-check' title='" + __("Ajouter cet utilisateur à LabNBook") + "' onclick='grantTeacherRights(" + users[i].id_user + ")'></i></li>";
			}
			user_html += "</ul>";
			$("#t-add-popup").find("section").html(user_html);
			// To avoid opening modal after the adding
			if($("#t-popup").dialog( "isOpen" )){
				$("#t-add-popup").dialog("open");
			}
		});
	}
}

/**
 * Configure la methode d'ajout d'un enseignant
 */
function queryLdapForTeacher() {
	var query = $('#t-login-ldap').val();
	if (query) {
		$.ajax({
			method: "POST",
			url: "/manager/getUserFromLdap",
			data: {
				query: query,
			}
		}).done(function (response) {
			if (Object.keys(response).length == 0 ) {
				alertGently(__("Aucun utilisateur trouvé dans l'annuaire"), "danger");
			} else {
				if (response.user_name === undefined) {
					alertGently(response);
				} else {
					// Show result and ask for adding user
					user_html = response.first_name + " " + response.user_name + " ("+ response.login +") - " + response.email;
					// Add an input with login for addTeacherFromLdap
					user_html += "<input type='hidden' name='t-add-ldap-login' value='"+ response.login+ "' >";
					$("#t-add-ldap-popup").find("section").html(user_html);
					$("#t-add-ldap-popup").dialog("open");
				}
			}
		});
	} else {
		alertGently(__("Veuillez entrer un identifiant ou email à chercher"), "warning");
	}
}

/**
 * Ajoute l'utilisateur depuis le ldap
 */
function addTeacherFromLdap() {
	var login = $("#t-add-ldap-popup").find("section").find('input[name=t-add-ldap-login]').val();
	$.ajax({
		method: "POST",
		url: "/manager/addTeacherFromLdap",
		data: {
			login: login,
			sendMail: $('input[name=t-add-send-email]').prop('checked'),
			id_inst: getIdInst()
		}
	}).done(function (response) {
		if (response.msg) {
			alertGently(response.msg);
		} else if (response.create) {
			alertGently(__("Compte enseignant crée"), 'success');
		} else {
			alertGently(__("Droits enseignants accordés à l'utilisateur"), 'success');
		}
		cleanAddTeacherPopup();
	}).fail(function (jqXHR, textStatus, errorThrown) {
		alertGently(__("Echec lors de l'ajout "+textStatus));
	});
}

/**
 * Configure la methode d'ajout d'un enseignant
 */
function setAddTeacherMethod(method) {
	if (method === 'ldap' && casEnabled()) {
		$('.t-fieldset.t-add-form').hide()
		$('.t-fieldset.t-add-ldap').show()
		$('#t-popup-valid').hide()
	} else {
		$('.t-fieldset.t-add-form').show()
		$('.t-fieldset.t-add-ldap').hide()
		$('#t-popup-valid').show()
	}
}

function cleanAddTeacherPopup (){
	$("#t-add-ldap-popup").dialog("close");
	$("#t-popup").dialog("close");
	$("#t-add-name").val("");
	$("#t-add-first-name").val(""); 
	$("#t-add-email").val("");
	$("#t-add-login").val("");
	$("#t-add-password").val("");
	if (typeof $('#u-table') !== 'undefined') {
		$('#u-table').DataTable().ajax.reload();
	}
}


/**
 * Create a new teacher 
 */
function addTeacher () {
	if(validateTeacherPopup()){
		$.ajax({
			method: "POST",
			url: "/manager/addTeacher",
			data: {
				user_name: $("#t-add-name").val(),
				first_name: $("#t-add-first-name").val(),
				email: $("#t-add-email").val(),
				login: $("#t-add-login").val(),
				password: $("#t-add-password").val(),
				send_mail: $('#t-add-send-email').prop("checked"),
				id_inst: getIdInst()
			}
		}).done(function(data) {
			if (isNaN(data)) {
				alertGently(data, 'error');
			} else {
				alertGently( __("L'utilisateur a bien été ajouté en tant qu'enseignant."), "info" ) ;
				cleanAddTeacherPopup();
			}

		}).fail(function(xhr) {
			if (xhr.responseJSON) {
				alertGently(xhr.responseJSON.message);
			} else {
				logError("AJAX addTeacher failed: " + JSON.stringify(xhr), 'error');
				alert("Erreur interne à l'enregistrement. Le problème est signalé, mais contactez les administrateurs système si le besoin est urgent.");
			}
		});
	}
	
}

/**
 * Vérifie les entrées pour l'ajout d'un enseignant
 * @returns vrai si les entrées du popup d'ajout d'enseignants sont valides e
 */
function validateTeacherPopup(){
	if (! ATControlString($('#t-add-first-name'), "Prénom", 1, 32, true)) { return false ; }
	if (! ATControlString($('#t-add-name'), "Nom", 1, 64, true)) { return false ; }
	if (! ATControlString($('#t-add-login'), "Intitulé du compte", 1, 20, true)) { return false ; }

	if (! $('#t-add-email').val() && ! $('#t-add-password').val()) {
		alertGently (  __("Vous devez définir un mot de passe (et le fournir à l'enseignant)\nou indiquer une adresse e-mail pour qu'il soit généré aléatoirement et envoyé par e-mail.") ,
			'error' ) ;
		return false ;
	}
	if ($('#t-add-send-email').prop("checked") && ! $('#t-add-email').val()) {
		alertGently (  __("Pour envoyer un e-mail récapitulatif, définissez une adresse mail") ,
			'error' ) ;
		return false ;
	}
	if (! $('#t-add-password').val() && ! $('#t-add-send-email').prop("checked")) {
		alertGently (  __("Si le mot de passe est généré aléatoirement, il faut envoyer un e-mail récapitulatif à l'utilisateur pour le lui faire passer") ,
			'error' ) ;
		return false ;
	}
	
	var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/ ;
	if ($('#t-add-email').val() && !regex.test($('#t-add-email').val())) {
		alertGently ( __("Le format de l'adresse e-mail n'est pas reconnu"),
			'error') ;
		return false ;
	}
	if ($('#t-add-password').val() && ! ATControlString($('#t-add-password'), __("Mot de passe"), 8, 64, false)) {
		alertGently(__('Le mot de passe doit comporter au moins 8 caractères dont une majuscule et une minuscule'));
		return false;
	}
	return true ;
}

/**
 * Grant teacher right for id_user user
 * @param {int} id_user
 */
function grantTeacherRights (id_user){
	
	$.ajax({
		method: "POST",
		url: "/manager/grantTeacherRights",
		data: {
			id_user: id_user,
		},
		success: function(content){
			// content can be true, false or "string"
			if(content == true){
				$("#t-add-popup").dialog("close");
				$("#t-popup").dialog("close");
				alertGently( __("L'utilisateur a bien été ajouté en tant qu'enseignant."), "info" );
				// For admin page
				$("#u-table").DataTable().ajax.reload()
			} else if (content == false) {
				$("#t-add-popup").dialog("close");
				$("#t-popup").dialog("close");
				alertGently( __("L'utilisateur était déjà present en tant que teacher et n'a pas été modifié."), "info" ) ;
			}
			else {
				$("#t-add-popup").dialog("close");
				$("#t-popup").dialog("close");
				alertGently(content, 'error');
			}
		}
	});
}


