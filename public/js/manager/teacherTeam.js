var teacherTeamActions = {
	/**
	 * Open a popup to add a new class.
	 *
	 * @return {undefined}
	 */
	add: function() {
		openEditTeacherTeamPopup(null);
	},
	delete: function(teacherTeamAttributes) {
		if(confirm(__('ATTENTION : La suppression est irréversible'))) {
			$.ajax({
				method: "POST",
				url: "/manager/"+teacherTeamAttributes.id_teacher_team + "/delete",
			})
			.done(function(content){
				$("#teacherTeams .lba-context-icons").addClass('actions-inactive');
				$("#tt-table").DataTable().ajax.reload(null, false);
			});
			return true;
		} else {
			return false;
		}
	},
	edit: function(idTeacherTeam, teacherTeamAttributes) {
		openEditTeacherTeamPopup(parseInt(idTeacherTeam), teacherTeamAttributes);
	},
	archive: function(teacherTeamAttributes) {
		if (teacherTeamAttributes.status === 'normal') {
			$.ajax({
				method: "POST",
				url: "/manager/"+teacherTeamAttributes.id_teacher_team+"/archive",
			})
				.done(function (content) {
					$("#teacherTeams .lba-context-icons").addClass('actions-inactive');
					$("#tt-table").DataTable().ajax.reload(null, false);
				})
				.fail(function (xhr) {
					logError("fail on archive teacher team", "error", { "id_teacher_team": teacherTeamAttributes.id_teacher_team });
				})
		} else {
			$.ajax({
				method: "POST",
				url: "/manager/"+teacherTeamAttributes.id_teacher_team+"/unarchive",
			})
				.done(function (content) {
					$("#teacherTeams .lba-context-icons").addClass('actions-active');
					$("#tt-table").DataTable().ajax.reload(null, false);
				})
				.fail(function (xhr) {
					logError("fail on unarchive teacher team", "error", { "id_teacher_team": teacherTeamAttributes.id_teacher_team });
				})
		}
	},
	duplicate: function(idTeacherTeam, teacherTeamAttributes) {
		$.ajax({
			method: "POST",
			url: "/manager/"+idTeacherTeam+"/duplicate",
		})
		.done(function (content) {
			$("#tt-table").DataTable().ajax.reload(null, false);
		})
		.fail(function (xhr) {
			logError("fail on archive teacher team", "error", { "id_teacher_team": idTeacherTeam });
		})
	},
	getParamsArchivePopup: function (){
		const attributes = $('#tt-actions .lba-context-icons').data().params.teacherTeamAttributes;
		let deleteLabel =  __('Supprimer l’équipe pédagogique "{{teacherTeam}}"', {teacherTeam : attributes.name});
		let deleteDetailsLabel =  __('ATTENTION : cette action est définitive', {teacherTeam : attributes.name});
		let archiveLabel = attributes.status==='archive'?
			__('Désarchiver l’équipe "{{teacherTeam}}"', { teacherTeam: attributes.name })
			:__('Archiver l’équipe "{{teacherTeam}}"', { teacherTeam: attributes.name });
		let archiveDetailsLabel = attributes.status==='archive'?'':__('Cette équipe peut être retrouvée en cochant la case "archives" du tableau', { teacherTeam: attributes.name });
		return {
			attributes: attributes,
			deleteLabel: deleteLabel,
			deleteDetailsLabel: deleteDetailsLabel,
			archiveLabel: archiveLabel,
			archiveDetailsLabel: archiveDetailsLabel,
			unsubscribeLabel: '',
			unsubscribeDetailsLabel: ''
		};
	}
	
};

var teacherTeams;

// Rename in global_tt_add_state
var global_tt_add_state = function () {
	var newTeachersName = [];
	var newTeachersId = [];
	var deletedTeachersName = [];
	var deletedTeachersId = [];
	var initialTeacherInHTML = "";

	return {
		addNewTeacher : function (newTeacherName, newTeacherId) {
			var positionInDeleted=deletedTeachersId.indexOf(newTeacherId)
			if(positionInDeleted==-1){
				newTeachersName.push(newTeacherName);
				newTeachersId.push(newTeacherId);
			} else{
				// On essaie d'ajouter un teacher qu'on vient de supprimer, donc on le ne supprime plus 
				deletedTeachersName.splice(positionInDeleted,1);
				deletedTeachersId.splice(positionInDeleted,1);
			}
			
		},
		addDeletedTeacher : function (deletedTeacherName, deletedTeacherId) {
			var positionInAdded=newTeachersId.indexOf(String(deletedTeacherId))
			if(positionInAdded==-1){
				deletedTeachersName.push(deletedTeacherName);
				deletedTeachersId.push(deletedTeacherId);
			} else{
				// On essaie de supprimer un teacher qu'on vient d'ajouter, donc on ne l'ajoute plus 
				newTeachersName.splice(positionInAdded,1);
				newTeachersId.splice(positionInAdded,1);
			}
		},
		getMessage : function (){
			let msg1 = __("Voulez vous y inscrire les enseignants ajoutés") + __("&nbsp;: ") + newTeachersName.join(", ") + " ?";
			let msg2 = __("Les enseignants supprimés restent toujours associés à leurs classes et missions.");
			if(newTeachersName != "" && deletedTeachersName == "" ){
				return msg1;
			}
			if(newTeachersName == "" && deletedTeachersName != "" ){
				$("#tt-add-popup-ok").removeClass("hide");
				$("#tt-add-popup-valid").addClass("hide");
				$("#tt-add-popup-cancel").addClass("hide");
				return msg2 ;
			}
			return msg1 + "<br>" + msg2;
		},
		getNewTeachersId : function () {
			return newTeachersId;
		},
		getDeletedTeachersId : function () {
			return deletedTeachersId;
		},
		teacherListHasChanged : function () {
			return  (deletedTeachersName.length + newTeachersName.length)>0;
		},
		setInitialTeacherList : function (param) {
			initialTeacherInHTML=param;
		},
		getInitialTeacherList : function (param) {
			return initialTeacherInHTML;
		},
		resetState : function (){
			newTeachersName = [];
			deletedTeachersName = [];
			newTeachersId = [];
			deletedTeachersId = [];
			initialTeacherInHTML = "";
		}
	};
}();

/**
 * selecting a table row (from any table)
 *
 * @param {Element} target DOM Element
 * @param {function} selectCallback
 * @param {function} unselectCallback
 * @return {undefined}
 */
function highlightRow(target, selectCallback, unselectCallback) {
	var tr = $(target);
	if (tr.hasClass('description-row')) {
		return;
	}
	var htmlTable = tr.closest('table.dataTable');
	var data = htmlTable.DataTable().row(tr).data();
	if (!data) {
		console.error("no data on this target, aborting: ", target);
		return;
	}
	var actionsBlock = $(htmlTable).closest('section').find(".lba-context-icons");
	$(".description-row", htmlTable).remove();
	if (tr.hasClass('selected')) {
		tr.removeClass('selected');
		actionsBlock.data('params', {});
		if (unselectCallback) {
			unselectCallback.call(this, htmlTable, tr);
		}
	} else {
		htmlTable.find("tr.selected").removeClass("selected");
		tr.addClass('selected');
		if (selectCallback) {
			selectCallback.call(this, htmlTable, tr, data);
		}
	}
}

function highlightTeacherTeamByRow(target) {
	/**
	 * Select a teacher team row, display its description
	 *
	 * @param {Element} htmlTable
	 * @param {Element} tr
	 * @param {Number} idTeacherTeam
	 * @param {object} teacherTeamAttributes (optional)
	 * @return {undefined}
	 */
	function highlightTeacherTeam(htmlTable, tr, idTeacherTeam, teacherTeamAttributes) {
		if (typeof teacherTeamAttributes === 'undefined' || !teacherTeamAttributes) {
			teacherTeamAttributes = htmlTable.DataTable().row(tr).data();
		}
			
		var status = "";
		if (teacherTeamAttributes.status === 'archive') {
			status = "<div><strong>" + __("Statut") + "</strong> : " + __("archive") + "</div>";
		}
		
		var html = 
			"<div><strong>" + __("Institution") + __("&nbsp;:") + " </strong>" + $('<span />').text(teacherTeamAttributes.inst_name).html()+  "</div>" +
			"<div><strong>" + __("Enseignants") + __("&nbsp;:") + " </strong>" + listUsers(teacherTeamAttributes.teachers) + "</div>" +
			"<div><strong>" + __("Classes ") + " </strong><i id='classes_tooltip' class='fa fa-info-circle lba_icon_default' onclick='messageTeacherTeamClasses(\"click\")' ></i> : " + listClasses(teacherTeamAttributes.classes) + "</div>" +
			"<div><strong >" + __("Missions ") + " </strong><i id='missions_tooltip' class='fa fa-info-circle lba_icon_default' onclick='messageTeacherTeamMissions(\"click\")' ></i> : " +listMissions(teacherTeamAttributes.missions) + "</div>";

		var descriptionCell = $('<td colspan="' + tr.children('td').length + '"></td>')
			.html(html);
	
	
		tr.after($('<tr class="description-row"></tr>').append(descriptionCell));
		var actionsBlock = $("#teacherTeams .lba-context-icons");
		actionsBlock.data('params', {idTeacherTeam: idTeacherTeam, teacherTeamAttributes: teacherTeamAttributes});
	
		actionsBlock.removeClass('actions-inactive');
		
		function listUsers(users) {
			return users.map(escapeUserName).join(" - ");
		}

		function listClasses(classes) {
			return classes.map(escapeClassName).join(" - ");
		}

		function listMissions(missions) {
			return missions.map(escapeMissionName).join(" - ");
		}

		function escapeClassName(classe) {
			return $('<span />').text(classe.class_name).html();
		}
		
		function escapeMissionName(mission) {
			return $('<span />').text(mission.name).html();
		}

		$("#classes_tooltip").on("mouseover", function (){
			messageTeacherTeamClasses();
		})

		$("#missions_tooltip").on("mouseover", function (){
			messageTeacherTeamMissions();
		})
	}

	highlightRow(
		this instanceof Element ? this : target,
		function(htmlTable, tr, data) {
			highlightTeacherTeam(htmlTable, tr, data.id_teacher_team, data);
		},
		function(htmlTable, tr) {
			$("#teacherTeams .lba-context-icons").addClass("actions-inactive");
		}
	);
}

function escapeUserName(user) {
	return $('<span />').text(user.user_name + " " + user.first_name).html();
}

function highlightTeacherTeamById(idTeacherTeam) {
	$("#tt-table tr.selected").removeClass("selected");
	highlightTeacherTeamByRow($('#teacherTeam-' + idTeacherTeam));
}

function searchInTable(el) {
	var dataTable = $(el).closest("section").find("table.dataTable").DataTable();
	if (dataTable) {
		dataTable.search(unaccent($(el).val())).draw();
	}
}


$(document).ready(function() {
	"use strict";

	initDataTables();
	$('#tt-table tbody').on("click", "tr", highlightTeacherTeamByRow);
	

	// search
	$(".search-input").on('input', function() {
		searchInTable(this);
	});
	$("#teacherTeams .tb-search").on('click', 'i', function(){
		var searchPopup = $('#tb-search-class');
		if (searchPopup.css('display') === 'inline-block') {
			searchPopup.find('input').val('');
			searchInTable(this);
			searchPopup.css('display', 'none');
		} else {
			searchPopup.css('display', 'inline-block').find('input').focus();
		}
	});
	// pagination
	$(".pagination-size").on('change', function(){
		var dataTable = $(this).closest("section").find("table.dataTable").DataTable();
		if (dataTable) {
			dataTable.page.len($(this).val()).draw();
		}
	});
		
	// apply archive filter
	$("#display-archives").on('change', function() {
		$('#tt-table').DataTable().draw();
	});

	
	// clicking on a link with attribute "data-action" will launch this action
	$(".actions").on("click", "[data-action]", function(event) {
		event.stopPropagation();
		if ($(this).closest(".actions-inactive").length > 0) {
			return false;
		}
		var params = $(this).closest(".lba-context-icons").data('params');
		
		// Block action on archive item if archive items is not display
		if(!$("#display-archives").prop('checked') && params && params.teacherTeamAttributes.status =="archive"){
			return false;
		}
		
		var action = $(this).attr('data-action');
		if(action === 'archive') {
			window.dispatchEvent(new CustomEvent('display-archive-popup'));
		} else if (teacherTeamActions[action]) {
			if (params) {
				teacherTeamActions[action].call(event, params.idTeacherTeam, params.teacherTeamAttributes);
			} else {
				teacherTeamActions[action].call(event);
			}
		} else {
			console.error("unknown action: ", action, ", parameters: ", params);
		}
		return false;
	});
	
		
	function initDataTables() {
		$.fn.dataTable.ext.order.intl();
		$.fn.dataTable.ext.search.push(
			// filter out #cl-table on archive teachingTeam
			function(settings, viewData, rowIndex, rawData) {
				if (settings.sTableId !== "tt-table") {
					return true;
				}
				var includeOld = $("#display-archives").is(":checked");
				return (includeOld || rawData.status == 'normal');
			}
		);
		$.extend(true, $.fn.dataTable.defaults, {
			dom: "rtp", // https://datatables.net/reference/option/dom
			autoWidth: false,
			paging: true,
			pageLength: 20,
			lengthMenu: [20, 50, -1],
			conditionalPaging: true,
			order: [],
			language: { url: "/libraries/datatables/locale/" + window.labnbook.lang + ".json" }
		});

		var italicsArchives = function(value, cellType, row) {
			var escapedValue = $('<span>').text(value).html();
			var classAttr = (row.status === 'archive' ? ' class="status-archive"' : '');
			return '<span' + classAttr + '>' + escapedValue + '</span>';
		};

		const teacherTeamConfig = {
			ajax: "/manager/getTeacherTeams",
			columns: [
				{
					data: "name",
					render: italicsArchives
				},
				{
					data: "nb_teacher",
					orderDataType: "num"
				}
			],
			rowId: function(d){ return "teacherTeam-" + d.id_teacher_team; }
		};
		const teacher_team = $('#tt-table').DataTable(teacherTeamConfig);
	}
});


/***********************************
 *        Initialisation     	   *
 ***********************************/

$(document).ready(function() {
	defineModal("tt-popup", "tt-add", true, "tt-actions");
	defineModal("t-popup", null, true, "t-add-teacher");
	defineModal("tt-add-popup", "tt-add", true, "tt-table");
	defineModal("t-add-popup", null, true, "tt-table");
	defineModal("t-add-ldap-popup", null, true, "tt-table");
	defineModal("archive-popup", null, false, "tt-actions");

	$("#tt-popup-valid").on("click", function(){
		// Vérifications des champs 
		if(validateAllTeacherTeamPopup()){
			// On affiche seulement si on modifie
			if($("#tt-popup-valid").data('id_teacher_team') && global_tt_add_state.teacherListHasChanged() && ($("#tt-add-classes-list").text() != "" || $("#tt-add-missions-list").text() != "")) {
				$("#tt-add-teachers-message").html(global_tt_add_state.getMessage());
				$("#tt-add-popup").dialog("open");
				$("#tt-popup").dialog("close");
				
			} else {
				$("#tt-add-teacher").val(null).trigger('change');
				addOrUpdateTeacherTeam(parseInt($("#tt-popup-valid").data('id_teacher_team')),true);
			}
		}
	});

	$('#t-search-teacher').on('keydown', function () {
		// TODO ajouter un spinner de recherche pourrait être intéressant 
		delay(function () {
			searchTeacher()
		}, 400);
	});
});


/**************************************************************************************************
 *                                            LES ÉQUIPES PÉDAGOGIQUES		                                   *
 **************************************************************************************************/

/*********************************************
 *   Ajouter/modifier/supprimer une équipe pédagogique
 ********************************************/

/**
 * Affiche la popup d'édition d'équipe pédagogique.
 * Récupère en AJAX les informations concernant cette équipe pédagogique et rempli les différents champs du popup.
 * Si my_id est défini : création de l'équipe pédagogique par l'enseignant d'id my_id
 */
function openEditTeacherTeamPopup(id_teacher_team, teacherTeamAttributes=null){
	global_tt_add_state.resetState();
	$('#tt-teachers-list').html('');
	if (typeof id_teacher_team === 'undefined' || !id_teacher_team) { // création de l'équipe pédagogique
		$("#tt-popup-title").text( __("Ajouter une équipe pédagogique") );
		$("#tt-popup-valid").data('id_teacher_team', null);
		$("#tt-add-inst").attr("disabled", false);
	}
	else { // modification de l'équipe pédagogique
		$("#tt-popup-title").text( __("Modifier l'équipe pédagogique") );
		$("#tt-popup-valid").data('id_teacher_team', parseInt(id_teacher_team));
		$("#tt-add-inst").attr("disabled", true);
	}
	$.ajax({ // récupération des infos
		method: "GET",
		url: "/manager/initEdit",
		data: {
			id_teacher_team: id_teacher_team
		},
		success: function(data) {
			$("#tt-add-name").val(data.teacher_team_name);
			$("#tt-add-inst").html(data.institutionsHTML);
			global_tt_add_state.setInitialTeacherList(data.teachersHTML);
			if(data.teacher_team_name === null && data.currentUser){
				// We create a new team so we need to add current user to newTeacherArray
				global_tt_add_state.addNewTeacher(escapeUserName(data.currentUser), data.currentUser.id_user);
				add_user_in_staging_from_id_and_name(data.currentUser.id_user,
					escapeUserName(data.currentUser),
					'#tt-teachers-list', 
					'id-teacher', 
					'deleteTeacherFromTeacherTeam(' + data.currentUser.id_user + ')', 
					'');
			}else if(teacherTeamAttributes) {
				teacherTeamAttributes.teachers.forEach((teacher) => {
					add_user_in_staging_from_id_and_name(teacher.id_user,
						escapeUserName(teacher),
						'#tt-teachers-list', 
						'id-teacher', 
						'deleteTeacherFromTeacherTeam(' + teacher.id_user + ')', 
						'');
				});
			}
			$("#tt-add-classes-list").html(data.classesHTML) ;
			$("#tt-add-missions-list").html(data.missionsHTML);
			if ($('#tt-add-inst').val()>= 1) {
				load_add_users('tt-add-teacher', '#tt-popup', __("Nom ou prénom d'un enseignant"), 'teacher', 0, $("#tt-add-inst").val(), (teacherTeamAttributes?teacherTeamAttributes.teachers.map((u) => u.id_user):[]));
			}
		}
	});
	$("#tt-popup").dialog("open");
}

/**
 * remplit la data-list correspondant aux enseignants de l'institution sélectionnée
 */
function selectInstForTeacherTeam(){
	// the selection is not possible anymore
	$("#tt-add-inst").prop('disabled', 'disabled');
	// Get teacher datalist
	load_add_users('tt-add-teacher', '#tt-popup', __("Nom ou prénom d'un enseignant"), 'teacher', 0, $("#tt-add-inst").val());
}

/**
 * fonction addTeacherInTeacherTeam
 * récupère le nom d'un enseignant et tente de l'ajouter à la liste des enseignants associés à une équipe pédagogique
 * ne fais rien si l'enseignant n'a pas été trouvé dans l'institution sélectionnée ou si l'enseignant est déjà dans la liste
 */
function addTeacherInTeacherTeam(selectedTeacher){
	if (!selectedTeacher) {
		alertGently(__('Veuillez selectionner un utilisateur'));
		return;
	}
	const new_teacher_name = escapeUserName({
		first_name: selectedTeacher.firstName,
		user_name: selectedTeacher.userName
	});
	add_user_in_staging_from_id_and_name(selectedTeacher.id, 
		new_teacher_name, 
		'#tt-teachers-list',
		'id-teacher',
		'deleteTeacherFromTeacherTeam(' + selectedTeacher.id + ')',
		'');
	global_tt_add_state.addNewTeacher(new_teacher_name,selectedTeacher.id);
}

/**
 * supprime un enseignant de la liste des enseignant associé à une équipe pédagogique. Ces modifications ne seront valides qu'une fois validées
 * @param id_teacher un id d'enseignant à dé-associé de l'équipe pédagogique selectionnée
 */
function deleteTeacherFromTeacherTeam(id_teacher){
	var deleteTeacherName = $("#tt-teachers-list").find('span[data-id-teacher="'+id_teacher+'"]').text();
	global_tt_add_state.addDeletedTeacher(deleteTeacherName,id_teacher);
	window.dispatchEvent(new CustomEvent('lnb-select2-remove-id', {
		detail:{
			selector: "tt-add-teacher",
			idElement: id_teacher
		}}));
	$("#tt-teachers-list").find('span[data-id-teacher="'+id_teacher+'"]').remove() ;
}

/**
 * Vérifie les entrées pour le popup de création ou de modification de l'équipe pédagogique
 * Vérifie également que le nombre d'enseignant associé est non nul
 * Affiche une alert descriptive en cas d'erreur
 * @returns vrai si tout les champs d'édition/ajout de l'équipe pédagogique sont valide, faux sinon
 */
function validateAllTeacherTeamPopup(){
	if (!ATControlString($('#tt-add-name'), "Nom de l'équipe pédagogique", 1, 64, true)) {
		return false ; 
	}
	if ($("#tt-add-inst").val() == 0) {
		alertGently (  __("Veuillez sélectionner une institution") ,
			'error' ) ;
		return false ;
	}
	
	if ($('#tt-teachers-list').find("span").length == 0) {
		if ($("#tt-add-teacher").val() > 0) {
			alertGently (  __("Veuillez valider l'enseignant à associer avant de valider l'équipe") ,
				'error' ) ;
		}else {
			alertGently (  __("Au moins un enseignant doit être associé à cette équipe") ,
				'error' ) ;
		}
		return false ;
	}
	if ($("#tt-add-teacher").val() > 0) {
		return confirm ( __('Attention : du texte est contenu dans le champ "Ajouter" sans avoir été validé.') + "\n" + __("Souhaitez vous confirmer les modifications effectuées ?") );
	}
	return true ;
}

/**
 * fonction addOrUpdateTeacherTeam
 * Vérifie les entrées du popup ajout de class
 * Envoie en ajax les informations pour ajouter / mettre à jour une équipe
 * En cas de succès affiche une message de succés et met à jour le tableau des équipes
 * En cas d'échec affiche un alert décrivant le problème
 */
function addOrUpdateTeacherTeam(id_teacher_team, updateLink){
	if (validateAllTeacherTeamPopup()) {
		
		$.ajax({
			method: "POST",
			url: "/manager/addOrUpdate",
			data: {
				id_teacher_team: isNaN(id_teacher_team) ? null : id_teacher_team,
				name: $("#tt-add-name").val(),
				inst: $("#tt-add-inst").val(),
				newTeachers: JSON.stringify(global_tt_add_state.getNewTeachersId()),
				deletedTeachers :  JSON.stringify(global_tt_add_state.getDeletedTeachersId()),
				updateLink : updateLink
			}
		}).done(function(id) {
			$("#tt-table").DataTable().ajax.reload(function(){
				if (id) {
					highlightTeacherTeamById(id);
				}
			});
			global_tt_add_state.resetState();
			$("#tt-add-popup").dialog("close");
			$("#tt-popup").dialog("close");
		}).fail(function(xhr) {
			if (xhr.responseText) {
				alert(JSON.parse(xhr.responseText));
			} else {
				logError("AJAX addOrUpdateTeacherTeam failed: " + JSON.stringify(xhr), 'error');
				alert("Erreur interne à l'enregistrement. Le problème est signalé, mais contactez les administrateurs système si le besoin est urgent.");
			}
		});
	}
}

function acceptConfirmationModal(){
	$("#tt-add-teacher").val(null).trigger('change'); //to prevent modal confirmation spam when the field is not empty
	addOrUpdateTeacherTeam(parseInt($("#tt-popup-valid").data('id_teacher_team')),true);
}
function closeConfirmationModal(){
	// On update la teacher team sans changer les teachers 
	$("#tt-teachers-list").html(global_tt_add_state.getInitialTeacherList());
	$("#tt-add-popup").dialog("close");
	$("#tt-popup").dialog("close");
	$("#tt-add-popup-ok").addClass("hide");
	$("#tt-add-popup-valid").removeClass("hide");
	$("#tt-add-popup-cancel").removeClass("hide");
	$("#tt-add-teacher").val(null).trigger('change'); //to prevent modal confirmation spam when the field is not empty
	addOrUpdateTeacherTeam(parseInt($("#tt-popup-valid").data('id_teacher_team')),false);
	global_tt_add_state.resetState();
}

/**************************************************************************************************
 *                                          Gestion enseignant                               *
 **************************************************************************************************/

// Voir aussi public/js/manager/addTeacher.js

/**
 * message d information sur les missions 
 */
function messageTeacherTeamMissions(){
	let msg_title, msg_content;
	msg_title    =  __('Rapport(s) associé(s)');
	msg_content  = __('Vous pouvez associer des rapports à vos équipes pédagogiques. Pour cela allez dans l\'onglet rapports');
	global_tooltip_popup.openTooltipPopup(msg_title, msg_content,"#missions_tooltip");
}

/**
 * message d information sur les classes
 */
function messageTeacherTeamClasses(){
	let msg_title, msg_content;
	msg_title    =  __('Classe(s) associée(s)');
	msg_content  = __('Vous pouvez associer des classes à vos équipes pédagogiques. Pour cela allez dans l\'onglet étudiant');
	global_tooltip_popup.openTooltipPopup(msg_title, msg_content,"#classes_tooltip");
}

/**
 * fonction searchTeacher
 * fonction searchTeacher
 * cherche un teacher correspondant aux infos saisies
 */
function searchTeacher(){
	let search = $.trim($('#t-search-teacher').val());
	if ( search != "" && search.length >= 2) {
		$.ajax({
			method: "POST",
			url: "/manager/getTeacherWithInstAndLinkBy",
			data: {
				search: search,
				id_inst: global_current_id_inst
			}
		}).done(function(users) {
			users.sort((a,b) => a.user_name.toLowerCase() > b.user_name.toLowerCase());
			let $result_panel = $('#t-teacher-results');
			$result_panel.empty();
			let usersHTML;
			if (users.length == 0) {
				usersHTML = __("Pas de résultat pour votre recherche")
			} else if (users.length == 1) {
				usersHTML = "1 " + __("résultat") + __("&nbsp;:");
			} else {
				usersHTML = users.length + " " + __( "résultats") + __("&nbsp;:");
			}
			usersHTML += `<i id='t-teacher-close-result' class='fa fa-times' onClick='closeTeacherResults()'/><div><ul>`;
			for (let i = 0; i < users.length; i++) {
				usersHTML += "<li data-id-user='" + users[i].id_user + "'>";
				if (users[i].hasLink !== 1) {
					usersHTML += ' <i onClick="removeTeacherStatus(' + users[i].id_user + ')" title="' + __("Supprimer le statut enseigant de cet utilisateur") + '" class="far fa-trash-alt remove-status-enable"></i> '
				}
				usersHTML += users[i].user_name + " " + users[i].first_name;
				if (users[i].email !== "") {
					usersHTML += ", " + "<a href='mailto:" + users[i].email + "'>" + users[i].email + "</a>";
				}
				usersHTML += ", login = " + users[i].login;
				usersHTML += "</li>";
			}
			usersHTML += "</ul></div>";
			$result_panel.append(usersHTML);
			$result_panel.removeClass("hide");
		});
	} else {
		$('#t-teacher-results').addClass("hide")
	}
}

function removeTeacherStatus(id_teacher) {
	if (id_teacher !== null){
		if(confirm( __("Voulez vous vraiment retirer le statut enseignant ?"))){
			$.ajax({
				method: "POST",
				url: "/manager/removeTeacherRights",
				data: {
					id_teacher : id_teacher
				}
			}).done(function (data) {
				alertGently(__("Le statut enseignant de cet utilisateur a bien été retiré"), 'info' );
				searchTeacher();
			})
		}
	} else {
		alertGently(__("Le statut enseignant de cet utilisateur ne peut pas être supprimé car cet enseignant est lié à au moins une classe ou une mission"), 'warning' );
	}
}

function closeTeacherResults(){
	$('#t-teacher-results').toggleClass("hide");
	$('#t-search-teacher').val("");
}

var delay = (function () {
	let timer = 0;
	return function (callback, ms) {
		clearTimeout(timer);
		timer = setTimeout(callback, ms);
	};
})()
