/* global i18next_Promise, report_env */

/**
 * Display the content of every (.labdoc-content-loading) LD from the content set in global_tab_ld
 * then return a jQuery.Deferred.
 * Used only in report_init.js at the first opening of the report - Caution : doesn't wait the next-tick
 * @return {Deferred}
 */
function displayLoadingLDs() {
    let loadingLDs = $(".labdoc-content-loading").map(function() {
        let a = $(this).data("attributes");
        return displayLDContent(a.id_labdoc, a.edition, a.extend);
    });
    // wait until all the LD have been attended
    return $.when.apply($, loadingLDs);
}

/**
 * initialization of the report
 */
i18next_Promise.then(function() {
    $(function () {
		if(report_env.freeze_lds_on_init) {
			freezeSyncLD();
		}
		
        function openAllRPAssignments() {
            $(".lb_report_part").each(function () {
                // console.log($(this));
                let idReportPart = $(this).data('id_rp');
                // console.log(idReportPart);
                if (idReportPart) {
                    let part_assignement = $(this).find('.lb_div_assignment');
                    if (part_assignement.text().trim() !== "") {
                        part_assignement.show();
                    }
                    toggleAllLdAssignment(idReportPart, true);
                }
            });
        }

        console.log("Open report " + report_env.id_report_js + " - scope " + global_scope);
        displayLoadingLDs().then(function () { // wait until all LD are displayed

            if (global_scope !== "test" && global_scope !== "pdf") {
                window.annotatorActions = initAnnotator(report_env.scope_js, report_env.id_user_js, report_env.id_report_js);
            }

            if (global_scope === "default" || global_scope === "view" || global_scope === "follow") {
                synchronize(report_env.id_report_js);
                setInterval(function() {
                    synchronize(report_env.id_report_js);
                    }, report_env.refresh_period * 1000);
            }

			// readytoprint();
            if (global_scope === 'pdf') {
                // gestion de l'impression des parties de rapport ne comprenant pas de LD
                if (!report_env.print_empty_rp) {
                    $(".lb_report_part").each(function () {
						if ($(this).find(".part_content").find("ul").text().trim() == "") {
							$(this).hide();
						}
                    });
                }
                if (report_env.print_assignment) {
                    openAllRPAssignments();
                }
            }
            // Manage the opening of the assignments
            if (((global_scope === 'default' || global_scope === 'view') && report_env.show_assignments) || global_external_test_report)  {
                openAllRPAssignments();
            } else if (global_scope === 'test') {
                openAllRPAssignments();
            }
        });

        // Alerts when the due date is close
        var report_end = ((report_env.scope_js === 'default' && report_env.report_end_datetime_js) ? (new Date(report_env.report_end_datetime_js)).getTime() : "null" );
        var now = (new Date).getTime();
        // in some browsers, timeout is a 32b int, so we ignore dates more than 24 days ahead
        if (report_end && report_end > now && report_end - now < 2147483647) {
            var report_end_date = new Date(report_end);
            var report_end_fr = report_end_date.getHours() + "h" + report_end_date.getMinutes();
            var millisecondsRemaining = report_end - now;
            if (millisecondsRemaining > 3600000) {
                setTimeout(
                    function() {
                        alertGently("<strong>" + __("Enregistrez vos modifications !") + "</strong> " + __("La phase d'ouverture du rapport se termine à {{time_end}}. Ce rapport sera automatiquement",{time_end:report_end_fr}) + "<strong>" + __("rendu dans une heure") + "</strong>",'warning', 30000);
                    },
                    millisecondsRemaining - 3600000
                );
            }
            if (millisecondsRemaining > 900000) {
                setTimeout(
                    function() {
                        alertGently("<strong>" + __("Enregistrez vos modifications !") + "</strong> " + __("La phase d'ouverture du rapport se termine à {{time_end}}. Ce rapport sera automatiquement",{time_end:report_end_fr}) + "<strong>" + __("rendu dans un quart d'heure") + "</strong>", 'warning', 30000);
                    },
                    millisecondsRemaining - 900000
                );
            }
            if (millisecondsRemaining > 60000) {
                setTimeout(
                    function() {
                        alertGently("<strong>" + __("Enregistrez vos modifications !") + "</strong> " + __("La phase d'ouverture du rapport se termine à {{time_end}}. Ce rapport sera automatiquement",{time_end:report_end_fr}) + "<strong>" + __("rendu dans une minute") + "</strong>", 'warning', 30000);
                    },
                    millisecondsRemaining - 60000
                );
            }
        }

        // Formulas renditions
        typesetHtmlWithFormulas("#widg-res-short-assignment");
        typesetHtmlWithFormulas(".lb_div_assignment");

        // icon toggle all LD
        updateIconForToggleAllLD();		
    });
});

/**
 * dynamically set the behaviours of buttons
 */
$(document).ready(function() {

    disableBackspaceNavigation();

    // toggle the RP assignments & trace
	$(".lb_show_assignment").on('click', (function(){
		// Nested function for static anonymous function variable
		var odd_even = true;
		return function () {
			var assignment = $(this).closest(".lb_report_part").find(".lb_div_assignment");
			var idReportPart = $(this).closest('[data-id-report-part]').attr('data-id-report-part');
			var is_visible_before = assignment.is(':visible');
			assignment.toggle();
			var is_visible_after = assignment.is(':visible');
			// Either there is a report part assignment and we follow it's visibility after toggle, or we use a
			// odd_even bit to decide if we should show or hide all ld assignments
			var show = (is_visible_after || is_visible_before) ? is_visible_after : odd_even;
			odd_even = ! odd_even;
			toggleAllLdAssignment(idReportPart, show);
			updateIconForToggleAllLD();
			if ((window.global_scope == "default" || window.global_scope == "view") && assignment.is(":visible")) { // trace
				$.ajax({
					method: 'POST',
					url: "/report/"+window.global_id_report+"/trace",
					data: {
						trace: 'viewPartAssignment',
						id_report_part: idReportPart
					}
				});
			}
		};
	})());

    // Initialize the double click for opening LD
    if (global_scope === 'default' || global_scope === 'test') {
        $("#lb_report").on("dblclick", ".labdoc_name, .labdoc_content", async function(){
            let id = $(this).attr('id');
            let m = id.match(/_(\d+)$/);
            if (m) {
                let id_ld = m[1];
                if (!$(this).closest(".labdoc").hasClass("editable-no")) {
                    await editLD(id_ld);
                }
            } else {
                logError("No valid id_labdoc in the HTML id: " + id, 'danger');
            }
        });
    }
	
	let rp_sortable = document.getElementsByClassName('report-part-sortable');
	for(let rpElem of rp_sortable) {
		Sortable.create(rpElem, {
			animation: 150,
			handle: '.ld-draggable',
			draggable: '.sortable-labdoc',
			group: {
				name: 'sortable-report-part',
				put: (to, from, dragElt) => {
					// Test if the type of the dragged labdoc is allowed in the report part target
					const rp_div = document.getElementById('report_part_'+to.el.dataset.id_rp);
					const allowed_types = rp_div.getAttribute('ld_type');
					const type_drag_ld = dragElt.querySelector('.ld_name_actions').getAttribute('ld_type');
					return allowed_types.includes(type_drag_ld);
				}
			},
			onEnd: (event) => {
				const id_ld = event.item.dataset.id_ld;
				const id_rp = event.to.dataset.id_rp;
				const ld_in_rp = Array.from(event.to.children).map((ld) => ld.dataset.id_ld);
				// Labdoc position : 1.. in LabNBook vs 0.. in sortable
				const new_position = event.newDraggableIndex + 1;
				// Move the labdoc in the db
				$.ajax({
					type: "POST",
					url: "/labdoc/"+id_ld+"/move",
					data: {
						id_rp: id_rp,
						position: new_position,
						ld_in_rp: ld_in_rp
					}
				}).done(function(responseData) {
					if(responseData.status === 'ok') {
						Alpine.store('reportLabdocs').updateLdPosition(id_ld, responseData.ld_position, responseData.rp_position);
					}
				}).fail(function(data) {
					if (data.message) {
						alertGently(data.message);
					} else {
						alertGently( __("Le déplacement est entré en conflit avec une action réalisée par un étudiant de l'équipe. Merci de recharger la page (touche F5) et de réessayer.") );
					}
				});
			}
		});
	}
});

// ********************************************************************************************************
//             Function synchronize
// ********************************************************************************************************

/**
 * Iterating function sent every $cfg['refresh_period'] secondes to update the page with server info
 * @param {int} id_report
 * @param {boolean} sub_synchro
 */
async function synchronize(id_report, sub_synchro = false) {

    // *******************************************************************
    // Saving in DB
    // *******************************************************************

    if (global_scope === "default"){
        // TinyMCE autosave
        if (
            typeof global_edited_ld !== 'undefined'
            && global_edited_ld > 0
            && global_tab_ld.hasOwnProperty(global_edited_ld)
            && (
                global_tab_ld[global_edited_ld].ld_type === "text" 
                || global_tab_ld[global_edited_ld].ld_type === "code" 
                || isProcedure(global_edited_ld, 'json')
            )
            && !global_tab_ld[global_edited_ld].loading
            && !global_tab_ld[global_edited_ld].recovering
        ) {
            await saveLocalLDVersion(global_edited_ld);
        }
        // Save all the modified LD in DB
        autoUpdateLDinDB(!sub_synchro);
    }

    // *******************************************************************
    //  Option de gel du rapport (scope follow uniquement)
    // *******************************************************************
    let do_sync;
    if (typeof window.global_stop_sync !== 'undefined' && window.global_stop_sync) {
        do_sync = false;
    } else {
        do_sync = true;
    }

    let sync_msg = "Sync report " + id_report;
    if (sub_synchro) { sync_msg += " HF"; }
    if (!do_sync) { sync_msg += " freeze"; }
    sync_msg += " - edited LD " + global_edited_ld;
    console.log(sync_msg);

    // *******************************************************************
    // Récupération des nouveautés sur la BD et mise à jour sur le client
    // *******************************************************************
    $.ajax({
        timeout: 5000,
        type: "POST",
        url: "/report/"+id_report+"/synchronize",
        data: {
            id_report: id_report,
            scope : global_scope,
            edited_ld: global_edited_ld,
            message: window.global_msg
        },
        error: function (jqXHR, textStatus) {
            if (jqXHR.status === 403) {
                if (jqXHR.responseText) {
                    let msg = decodeURIComponent(JSON.parse(jqXHR.responseText));
                    alertGently(msg);
                }
                window.location.href = '/report/';
                return;
            } else if (jqXHR.status === 409) {
                if (jqXHR.responseJSON) {
                    alertGently(jqXHR.responseJSON.message);
                }
                window.location.href = '/report/';
                return;
            } else if (jqXHR.status === 404) {
                alert(__("Le rapport a été supprimé, vous allez être redirigé"));
                window.location.href = '/report/';
                return;
            }
            const isNetworkErr = isNetworkError(jqXHR, textStatus);
            if (isNetworkErr) {
                alertGently( __("L'enregistrement périodique n'a pas fonctionné. Perte de connexion au réseau ?") , "warning");
            } else if (jqXHR.status !== 401) {
                logError("Erreur inconnue lors de la synchronisation : " + textStatus + " code "+jqXHR.status + " Message "+jqXHR.responseText, 'danger');
                alertGently( __("Erreur inconnue lors de la synchronisation"), "warning");
            }
        },
        success: function (data) {
            // console.log("sync", data) ;

            // ANNOTATED LD - first position for possibly avoiding some errors when refreshing LD
            if ($(data).find('annotations annotation').length) {
                if (window.annotatorActions) {
                    window.annotatorActions.refreshAllAnnotations();
                    // console.log('refresh all annotations after synchro');
                }
            }

            if (do_sync) {
                // DELETED LD
                $(data).find('deleted').children('ld').each(function () {
                    let id_ld = $(this).attr('id_ld');
                    let shared = $(this).attr('shared');
                    if (id_ld != global_edited_ld) { // check if the LD is not edited - shouldn't happen anymore !
                        // console.log("delete ld", id_ld);
                        let ld_name = getLDNameFromDom(id_ld);
                        if (ld_name) {
                            alertGently(__("Le labdoc '{{ld_name}}' a été placé dans la corbeille par un étudiant de l'équipe", {ld_name: ld_name}), "info");
                        }
                        removeLDFromDom(id_ld, shared);
                        // Nouvelle requête pour changer le statut deleted à 0 pour le user
                        $.ajax({
                            type: "POST",
                            url: "/labdoc/" + id_ld + "/removedStatus",
                            data: {
                                status: 'deleted',
                            }
                        });
                    }
                });

                // MODIFIED OR NEW LD -> refresh the content
                $(data).find('modified ld').each(function () {
                    const id_ld = $(this).attr('id_ld');
                    const id_rp = $(this).attr('id_rp');
                    const shared = $(this).attr('shared');
                    // console.log("modify ld", id_ld);
                    let domLabdoc = $("#labdoc_" + id_ld);
                    if (domLabdoc.length === 0) {
						// creation of the LD in the DOM
						const dest_selector_prefix = shared === "1" ? 'rp_shared_ld_' : 'rp_sortable_';
						$("#" + dest_selector_prefix + id_rp).append('<div id="labdoc_' + id_ld + '" class="sortable-labdoc labdoc" data-id_ld="' + id_ld + '"></div>');
						getAndDisplayLD(id_ld, 0, 1, false);
                    } else if (id_ld != global_edited_ld) {
						let stored_ld = Alpine.store('reportLabdocs').getLd(id_ld);
						const new_name = $(this).find("name").text();
						if(stored_ld) {
							stored_ld.name = new_name;
							stored_ld.last_id_version_before_freeze = $(this).find('last_id_version_before_freeze').text();
						}
                        try {
							global_tab_ld[id_ld].ld_history[0] = $(this).find("content").text();
							const ld_toggle_diff = document.getElementById('lb-toggle-modifications-'+id_ld);
							const ld_teacher_in_edit = document.getElementById("ld_test_without_saving_" + id_ld);
							if((!ld_toggle_diff || !ld_toggle_diff.classList.contains('display-diff') 
								&& (!ld_teacher_in_edit || ld_teacher_in_edit.dataset.inEdit !== 'true'))) {
								// display new content only if not diff mode
								displayLDContent(id_ld, 0, "unchanged", 0, 0, 0, true, true, false);
							}
						} catch (error) {
                            console.log('Labdoc ' + id_ld + ' is modified by synchronize but not in global_tab_ld');
                            console.log(error);
                        }
                    }
                });

                // MOVED LD
                var modified_rp = new Set();
                $(data).find('moved ld').each(function () {
                    var id_ld = $(this).attr('id_ld');
                    var id_rp = $(this).attr('id_rp');
                    const shared = $(this).attr('shared');
                    var new_pos = parseInt($(this).text()) - 1; // position DOM [0..] du LD
                    var current_ld = $("#labdoc_" + id_ld);
                    const dest_selector_prefix = shared === "1" ? 'rp_shared_ld_' : 'rp_sortable_';
                    var current_id_rp = parseInt(current_ld.closest(".lb_report_part").data("id_rp"));
                    var current_pos = parseInt($("#" + dest_selector_prefix + current_id_rp + " .labdoc").index(current_ld));
                    if (id_ld != global_edited_ld || (current_id_rp == id_rp && current_pos == new_pos)) { // Move the LD only if the LD is not edited
                        // console.log("move ld",id_ld,"new rp",id_rp, "insert after pos", new_pos);
                        modified_rp.add(id_rp);
                        if (current_id_rp != id_rp || current_pos != new_pos) { // a real need to move the LD
                            if (current_id_rp == id_rp && current_pos < new_pos) {
                                new_pos++;
                            }
                            if (new_pos == 0) {
                                $("#" + dest_selector_prefix + id_rp).prepend($("#labdoc_" + id_ld));
                            } else {
                                $("#labdoc_" + id_ld).insertAfter($("#" + dest_selector_prefix + id_rp + " .labdoc").eq(new_pos - 1));
                            }
                        }
                        $.ajax({
                            type: "POST",
                            url: "/labdoc/" + id_ld + "/removedStatus",
                            data: {
                                status: "moved"
                            }
                        });
                    }
                });
                // After a move LD, check the sequence of the LDs in the RP. If not, inform
                var sequence_ok = true;
                for (let id_rp of modified_rp) {
                    var dom_ld = $("#part_content_" + id_rp + " .labdoc");
                    var sync_ld = $(data).find('ld_list ld[id_rp="' + id_rp + '"]');
                    if (dom_ld.length != sync_ld.length) {
                        sequence_ok = false;
                        break;
                    } else {
                        for (var i = 0; i < dom_ld.length; i++) {
                            if (dom_ld.eq(i).data("id_ld") != sync_ld.eq(i).attr("id_ld")) {
                                //console.log('Ld in the wrong place at '+i+' Got ld '+dom_ld.eq(i).data("id_ld")+' expected '+sync_ld.eq(i).attr("id_ld"));
                                sequence_ok = false;
                                break;
                            }
                        }
                    }
                }
                if (!sequence_ok) {
                    alertGently(__("Une modification faite par un de vos collaborateurs a été mal synchronisée. Nous vous conseillons de recharger la page (touche F5)."));
                }
            }

            // LD STATUS CHECK -> free / new / locked = name of the locker
            // This part is used to update header icon on labdoc ( none / locked / modified )
			if (typeof(window.global_stop_sync) == 'undefined' || !window.global_stop_sync) {
				$(data).find('ld_list ld').each(function () {
					let store_labdoc = Alpine.store('reportLabdocs').getLd($(this).attr('id_ld'));
					if(store_labdoc) {
						store_labdoc.status = $(this).find('status').text();
						store_labdoc.last_editor = hTMLEscapeNoConv($(this).find("editor").text());
						store_labdoc.last_edition = hTMLEscapeNoConv($(this).find("last_edition").text());
						store_labdoc.last_id_version_before_freeze = $(this).find('last_id_version_before_freeze').text();
					}
				});
			}

            // DRAFTED LD -> change the "drafts" elements
            $(data).find('drafted ld').each(function () {
                const id_ld = $(this).attr('id_ld');
				const draft = parseInt($(this).text());
                changeDraftDisplays(id_ld, draft);
            });

            // COMMENTS
            // Maj des icones commentaires pour chaque labdoc
			Alpine.store('reportLabdocs').resetLdNewCom();
            $(data).find('comment ld').each(function () {
				let id_ld = $(this).attr('id_ld');
				Alpine.store('reportLabdocs').addLdNewCom(parseInt(id_ld));
				if(parseInt(Alpine.store('commentWidgetIdLd')) === parseInt(id_ld)) {
					window.dispatchEvent(new CustomEvent('report-display-comment', {detail:{id_ld:id_ld}}));
				}
            });

            // CONNECTED USERS
            let title_on_name = $(".user_in_team").length > 3;
            if (title_on_name) {
                $(".user_in_team").removeClass("connected_user").each(function () {
                    let name = $(this).attr("name");
                    $(this).attr("title", name);
                });
            } else {
                $(".user_in_team").removeClass("connected_user").attr("title","");
            }
            var nb_connected_users = 0;
            $(data).find('connected_user user').each(function () {
                var id_u = $(this).attr("id_user");
                let name = $("#user_in_team_" + id_u).attr("name");
                $("#user_in_team_" + id_u).addClass("connected_user").attr("title",name + " " + __("est actuellement connecté(e)"));
                nb_connected_users++;
            });
            nb_connected_users += $(data).find('connected_user teacher').length;
            if (nb_connected_users > 1 && !sub_synchro) { // add five sub_synchro between 2 synchro
                for (let i = 1; i <= 5; i++) {
                    setTimeout(function(){synchronize(report_env.id_report_js, true)}, report_env.refresh_period * 160 * i);
                }
            }


            // MESSAGES
            var ids_conversation = [];
            var nb_conv = 0;
            $(data).find('message conv').each(function(){
                ids_conversation["key_"+$(this).attr("id_conv")]=$(this).text();
                nb_conv++;
            });
            ids_conversation.length = nb_conv;
            if (Number(window.global_msg)) {
                try {
                    refreshMessage(ids_conversation);
                }
                catch(e) {
                    logError(e.message + " - problem in refreshMessage", 'error', 'synchronize() in report_control.js');
                }
            }
			
			//RESSOURCES
			let ressources_list = [];
			$(data).find('lru').each(function(){
				ressources_list.push($(this).attr("id_lru"));
			})
			displayRessources(ressources_list);
			
			// TASKS
			const event = new Event('tasksynchro');
			document.dispatchEvent(event);
			
			// SIMULATION
			if(window.global_scope === 'follow') {
				window.dispatchEvent(new CustomEvent('synchronize-simulation'));
			}
        }
    });
}
document.addEventListener('alpine:init', () => {
	Alpine.store('reportLabdocs', {
		labdocs: [],
		storeOrUpdateLabdoc(id_ld, ld_name, is_shared, ld_position, rp_position, last_id_version_before_freeze, is_draft) {
			let labdoc = this.labdocs.find((ld)=>parseInt(ld.id_ld) === parseInt(id_ld));
			if(labdoc) {
				labdoc.name = ld_name;
				labdoc.is_shared = is_shared;
				labdoc.ld_position = ld_position;
				labdoc.rp_position = rp_position;			
				labdoc.is_draft = is_draft;			
				labdoc.last_id_version_before_freeze = last_id_version_before_freeze;
			} else {
				this.labdocs.push({
					id_ld:id_ld,
					name:ld_name,
					new_com:0,
					is_shared:is_shared,
					ld_position: ld_position,
					rp_position: rp_position,
					status: 'free_ld',
					last_edition: null,
					last_editor: null,
					last_id_version_before_freeze: last_id_version_before_freeze,
					is_draft:is_draft,
					is_new_ld:false
				});
			}
		},
		deleteLd(id_ld) {
			const ldIndex = this.labdocs.findIndex((ld) => parseInt(ld.id_ld) === parseInt(id_ld));
			if(ldIndex) {
				this.labdocs.splice(ldIndex, 1);
			}
		},
		getSortedLabdocs() {
			return this.labdocs.sort((a, b) => a.rp_position - b.rp_position || a.is_shared - b.is_shared || a.ld_position - b.ld_position);
		},
		getLd(id_ld) {
			return this.labdocs.find((ld)=>parseInt(ld.id_ld) === parseInt(id_ld));
		},
		getLdName(id_ld) {
			const labdoc = this.labdocs.find((ld)=>parseInt(ld.id_ld) === parseInt(id_ld));
			return labdoc?(labdoc.name!==''?labdoc.name:__('Nom de labdoc à indiquer')):null;
		},
		getLdStatus(id_ld) {
			const labdoc = this.labdocs.find((ld)=>parseInt(ld.id_ld) === parseInt(id_ld));
			return labdoc?labdoc.status:'';
		},
		getModifiedTitle(id_ld) {
			const labdoc = this.labdocs.find((ld)=>parseInt(ld.id_ld) === parseInt(id_ld));
			if(labdoc) {
				return __("Dernière modification par {{- editor}} le {{- last_edition}} (cliquez pour éteindre)", {
					editor: labdoc.last_editor,
					last_edition: labdoc.last_edition
				});
			} else {
				return '';
			}
		},
		getLockedTitle(id_ld){
			const labdoc = this.labdocs.find((ld)=>parseInt(ld.id_ld) === parseInt(id_ld));
			if(labdoc) {
				return __("Labdoc en cours de modification par {{- editor}}", {editor: labdoc.last_editor});
			} else {
				return '';
			}
		},
		getDraftTitle(id_ld) {
			const labdoc = this.labdocs.find((ld)=>parseInt(ld.id_ld) === parseInt(id_ld));
			if(labdoc) {
				return labdoc.is_draft?__('Montrer le labdoc à votre enseignant'):__('Cacher le labdoc à votre enseignant');
			} else {
				return '';
			}
		},
		isLdName(id_ld) {
			const labdoc = this.labdocs.find((ld)=>parseInt(ld.id_ld) === parseInt(id_ld));
			return labdoc && labdoc.name!=='';
		},
		isDraft(id_ld) {
			const labdoc = this.labdocs.find((ld)=>parseInt(ld.id_ld) === parseInt(id_ld));
			return labdoc && labdoc.is_draft;
		},
		isNewLd(id_ld) {
			const labdoc = this.labdocs.find((ld)=>parseInt(ld.id_ld) === parseInt(id_ld));
			return labdoc && labdoc.is_new_ld;
		},
		getFirstLdWithNewCom() {
			return this.labdocs.find((ld) => ld.new_com);	
		},
		resetLdNewCom() {
			for(let ld of this.labdocs) {
				ld.new_com = 0;
			}
		},
		addLdNewCom(id_ld) {
			let labdoc = this.labdocs.find((ld)=>parseInt(ld.id_ld) === parseInt(id_ld));
			if(labdoc) {
				labdoc.new_com += 1;
			}
		},
		updateLdPosition(id_ld, new_ld_position , new_rp_position) {
			let labdoc = this.labdocs.find((ld)=>parseInt(ld.id_ld) === parseInt(id_ld));
			if(labdoc) {
				labdoc.ld_position = new_ld_position - 0.5 + Number(labdoc.ld_position < new_ld_position);
				labdoc.rp_position = new_rp_position;
				let relatedList = this.labdocs.filter((ld) => ld.is_shared === labdoc.is_shared && ld.rp_position === labdoc.rp_position).sort((a,b)=>a.ld_position - b.ld_position);
				for (let i = 0; i < relatedList.length; i++) {
					relatedList[i].ld_position = i+1;
				}
			}			
		},
		isCommentOnLd(id_ld) {
			const labdoc = this.labdocs.find((ld)=>parseInt(ld.id_ld) === parseInt(id_ld));
			return labdoc?labdoc.new_com:false;
		},
		getTotalNewComment() {
			return this.labdocs.filter((ld) => ld.new_com).length;
		}
	});
});
