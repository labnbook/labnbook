$(document).ready(function() {
	"use strict";

	initDataTables();
	selfTest();

	function initDataTables() {
		$.fn.dataTable.ext.order.intl();
		$.fn.dataTable.ext.search.push(
			// filter out #cl-table on archive teachingTeam
			function(settings, viewData, rowIndex, rawData) {
				if (settings.sTableId !== "tt-table") {
					return true;
				}
				var includeOld = $("#display-archives").is(":checked");
				return (includeOld || rawData.status == 'normal');
			}
		);
		$.extend(true, $.fn.dataTable.defaults, {
			dom: "rtp", // https://datatables.net/reference/option/dom
			autoWidth: false,
			paging: true,
			pageLength: 20,
			lengthMenu: [20, 50, -1],
			conditionalPaging: true,
			order: [[0, 'asc']],
			language: { url: "/libraries/datatables/locale/" + window.labnbook.lang + ".json" },
		});

		var italicsArchives = function(value, cellType, row) {
			var escapedValue = $('<span>').text(value).html();
			var classAttr = (row.status === 'archive' ? ' class="status-archive"' : '');
			return '<span' + classAttr + '>' + escapedValue + '</span>';
		};
		
		const usersConfig = {
			columns: [
				{data: "user_name"},
				{data: "first_name"},	
				{data: "email"},	
				{data: "roleName"},
			],
			rowId: function(d) { return "user-" + d.id; }
		};

		const institutionsConfig = {
			ajax: "/admin/institutions",
			columns: [
				{data: "name"},
				{
					data: null,
					render: function ( data, type, row ) {
						if(data.classe){
							return data.classe;
						} else {
							return __('NA');
						}
					}
				},
				{
					data: null,
					render: function ( data, type, row ) {
						if(data.cas=="1"){
							return __('Activé');
						} else {
							return __('Désactivé');
						}
					}
				},
			],
			rowId: function(d) { return "institution-" + d.id; }
		};

		const extplatformsConfig = {
			ajax: "/admin/extplatforms",
			columns: [
				{data: "inst"},
				{data: "name"},
				{data: "url"},
				{data: "platform_version"},
				{data: "plugin_version"},
				{data: "contacts"},
			],
			rowId: function(d) { return "extplatform-" + d.id; }
		};

		const users = $('#u-table').DataTable(usersConfig);
		const institutions = $('#i-table').DataTable(institutionsConfig);
		const extplatforms = $('#e-table').DataTable(extplatformsConfig);

		users.on('search.dt', function() {
			$('#num-visible-users').text(users.rows({search: 'applied'}).count());
		} );
		institutions.on('search.dt', function() {
			$('#num-visible-institutions').text(institutions.rows({search: 'applied'}).count());
		} );
		extplatforms.on('search.dt', function() {
			$('#num-visible-institutions').text(extplatforms.rows({search: 'applied'}).count());
		} );
	}

	function showUsers() {
		$('#u-table td.dataTables_empty').text(__('Chargement en cours'));
		$('#u-table').DataTable().ajax.url("/admin/users/?id_inst="+$('#institutions .lba-context-icons').data('params').idInst).load();
		$('#users .lba-context-icons').addClass('actions-inactive');
		$('#users').show();
	}

	$('#i-table tbody').on("click", "tr[role=row]", function() {
		var tr = $(this);
		if (tr.hasClass('description-row')) {
			return;
		}
		var htmlTable = tr.closest('table.dataTable');
		var actionsBlock = $(this).closest('section').find(".lba-context-icons");
		var data = htmlTable.DataTable().row(tr).data();
		$(".description-row", htmlTable).remove();
		if (tr.hasClass('selected')) {
			tr.removeClass('selected');
			actionsBlock.addClass('actions-inactive');
			actionsBlock.data('params', {});
			$('#users').hide();
		} else if (data) {
			$("tr.selected", htmlTable).removeClass("selected");
			tr.addClass('selected');

			var descriptionContent = data.description;
			var descriptionCell = $('<td colspan="' + tr.children('td').length + '" ></td>').html(descriptionContent);
			var extraRow = tr.after($('<tr class="description-row"></tr>').append(descriptionCell)).next();
			actionsBlock.data('params', {idInst: data.id, inst: data});
			actionsBlock.removeClass('actions-inactive');
			typesetHtmlWithFormulas(".description-row");
			showUsers(data.id)
		}
	});
	
	$('#u-table tbody').on("click", "tr[role=row]", function() {
		var tr = $(this);
		if (tr.hasClass('description-row')) {
			return;
		}
		var htmlTable = tr.closest('table.dataTable');
		var actionsBlock = $(this).closest('section').find(".lba-context-icons");
		var data = htmlTable.DataTable().row(tr).data();
		$(".description-row", htmlTable).remove();
		if (tr.hasClass('selected')) {
			tr.removeClass('selected');
			actionsBlock.addClass('actions-inactive');
			actionsBlock.data('params', {});
		} else if (data) {
			$("tr.selected", htmlTable).removeClass("selected");
			tr.addClass('selected');

			var descriptionContent = "<i class='fa fa-spinner fa-pulse'></i> "+__("Chargement en cours");
			var descriptionCell = $('<td colspan="' + tr.children('td').length + '" ></td>').html(descriptionContent);
			var extraRow = tr.after($('<tr class="description-row large-row"></tr>').append(descriptionCell)).next();
			$.ajax({
				url: "/admin/user/"+data.id+"/getDescr"
			}).done(function(html) {
				var descriptionContent = html;
				var descriptionCell = $('<td colspan="' + tr.children('td').length + '" ></td>').html(descriptionContent);
				extraRow.html(descriptionCell);
			});
			actionsBlock.data('params', {idUser: data.id, user: data});
			actionsBlock.removeClass('actions-inactive');
			typesetHtmlWithFormulas(".description-row");
		}
	});

	$('#e-table tbody').on("click", "tr[role=row]", function() {
		var tr = $(this);
		if (tr.hasClass('description-row')) {
			return;
		}
		var htmlTable = tr.closest('table.dataTable');
		var actionsBlock = $(this).closest('section').find(".lba-context-icons");
		var data = htmlTable.DataTable().row(tr).data();
		$(".description-row", htmlTable).remove();
		if (tr.hasClass('selected')) {
			tr.removeClass('selected');
			actionsBlock.addClass('actions-inactive');
			actionsBlock.data('params', {});
		} else if (data) {
			$("tr.selected", htmlTable).removeClass("selected");
			tr.addClass('selected');

			var descriptionContent = data.description;
			var descriptionCell = $('<td colspan="' + tr.children('td').length + '" ></td>').html(descriptionContent);
			var extraRow = tr.after($('<tr class="description-row"></tr>').append(descriptionCell)).next();
			actionsBlock.data('params', {idExtp: data.id, extp: data});
			actionsBlock.removeClass('actions-inactive');
			typesetHtmlWithFormulas(".description-row");
		}
	});
	
	defineModal("modal-inst-add", "", true, "i-table");
	defineModal("modal-inst-edit", "", true, "i-table");
	defineModal("modal-extp-add", "", true, "e-table");
	defineModal("modal-extp-edit", "", true, "e-table");
	defineModal("t-popup", null, true, "u-table")
	defineModal("t-add-popup", null, true, "u-table")
	defineModal("t-edit-popup", null, true, "u-table")
	defineModal("t-add-ldap-popup", null, true, "u-table")
	$("#t-edit-popup-valid").on("click", function(){
		editTeacher();
	});


	$(".popup").on("click", ".popup-cancel", function(event) {
		event.stopPropagation();
		var modal = $(this).closest(".popup");
		modal.dialog("close");
		if (modal.data('reset-input')) {
			$("form", modal)[0].reset();
		}
	});
	$("#modal-inst-add").on("click", ".popup-submit", function(event) {
		event.stopPropagation();
		addInstitution(
			$(this).closest(".popup"),
			$("#in-add-name").val().trim(),
			$("#in-add-cas").prop('checked')
		);
	});
	$("#modal-inst-edit").on("click", ".popup-submit", function(event) {
		event.stopPropagation();
		var cas = $("#in-edit-cas").prop('checked') ? 1 : 0;
		updateInstitution(
			$(this).closest(".popup"),
			$('#institutions .lba-context-icons').data('params').idInst,
			$("#in-edit-name").val().trim(),
			cas,
			$("#in-edit-class").val()
		);
	});

	$("#modal-extp-add").on("click", ".popup-submit", function(event) {
		event.stopPropagation();
		addExtp(
			$(this).closest(".popup"),
			$("#extp-add-name").val().trim(),
			$("#extp-add-url").val().trim(),
			$("#extp-add-inst").val(),
			$("#extp-add-contacts").val().trim()
		);
	});
	$("#modal-extp-edit").on("click", ".popup-submit", function(event) {
		event.stopPropagation();
		var cas = $("#in-edit-cas").prop('checked') ? 1 : 0;
		updateExtplatform(
			$(this).closest(".popup"),
			$('#extplatforms .lba-context-icons').data('params').idExtp,
			$("#extp-edit-name").val().trim(),
			$("#extp-edit-url").val().trim(),
			$("#extp-edit-secret").val().trim(),
			$("#extp-edit-inst").val().trim(),
			$("#extp-edit-contacts").val().trim()
		);
	});

	function searchInTable(el) {
		var dataTable = $(el).closest("section").find("table.dataTable").DataTable();
		if (dataTable) {
			dataTable.search(unaccent($(el).val())).draw();
		}
		refreshIcons($(el).closest('section'));
	}
	
	// search
	$(".search-input").on('input', function() {
		searchInTable(this);
	});
	// pagination
	$(".pagination-size").on('change', function(){
		var dataTable = $(this).closest("section").find("table.dataTable").DataTable();
		if (dataTable) {
			dataTable.page.len($(this).val()).draw();
		}
	});
});

/**
 * @return {object}
 */
function getSelectedAttributes(section) {
	var htmlTable = $(section).find('table');
	console.log(htmlTable);
	var higlightedTr = $(htmlTable).find(" tr.selected");
	console.log(higlightedTr);
	if (higlightedTr.length === 1) {
		return htmlTable.DataTable().row(higlightedTr).data();
	} else {
		return null;
	}
}
function refreshIcons(section) {
	console.log(section);
	var hasHighlight = getSelectedAttributes(section) !== null;
	console.log(hasHighlight);
	if (hasHighlight) {
		$(section).find(".lba-context-icons")
			.removeClass('actions-inactive')
	} else {
		$(section).find(".lba-context-icons")
			.addClass('actions-inactive')
	}
}

/**
 * Send an ajax request to update the user
 * @param object user
 */
function editTeacherAjax(user) {
	$.ajax({
		type: "POST",
		url: "/admin/user/"+user.id,
		data: {
			user_name: $('#t-edit-name').val(),
			first_name: $('#t-edit-first-name').val(),
			email: $('#t-edit-email').val(),
			role: $('input[name="t-edit-role"]:checked').attr('id'),
			login: $('#t-edit-login').val()
		}
	}).fail(function(jqXHR) {
		let msg = decodeURIComponent(JSON.parse(jqXHR.responseText));
		if (jqXHR.status === 409) { // conflict
			alert( __("Echec lors de la modification de l'utilisateur : {{task}}", { task : msg } ) );
		} else {
			alert(msg);
		}
	}).done(function(data) {
		$('#t-edit-popup').dialog("close");
		$("#u-table").DataTable().ajax.reload()
	});
}

/**
 * Try to update the selected user, sends ajax or ask for confirmation
 */
function editTeacher() {
	var user = $('#users .lba-context-icons').data('params').user
	var role = $('input[name="t-edit-role"]:checked').attr('id')
	if (role != user.role) {
		var roleName = $('input[name="t-edit-role"]:checked').data('display-name');
		var message = __(
			"Passer le role de {{first_name}} {{last_name}} ({{login}}) de '{{oldRole}}' a '{{newRole}}'",
			{
				first_name: user.first_name,
				last_name: user.last_name,
				login: user.login,
				oldRole: user.roleName,
				newRole: roleName
			}
		);
		if (role == 'learner') {
			message += "\n\n"+__("Attention : cette action est irréversible et peut entrainer une perte de donnée");
		}
		global_confirmation_popup.openConfirmationPopup(
			__('Changement de rôle utilisateur'),
			message,
			function(){
				editTeacherAjax(user);
				global_confirmation_popup.closeConfirmationPopup();
			}
		);
	} else {
		editTeacherAjax(user);
	}
}

/**
 * Show/hide the superpower part of the user edit dialog
 */
function showHideSuperPowerSection(role)
{
	var short_role = role === 'learner' ? 'learner' : 'teacher';
	$('#t-edit-short-role').val(short_role);
	var power=$('#superpower');
	power.find("input[name='t-edit-role'][id='"+role+"']").prop('checked', true);
	if (role == 'learner') {
		power.hide();
	} else {
		power.show();
	}
}

/**
 * Open the user edition popu
 */
function openEditUserDialog() {
	$('#t-edit-popup').dialog("open");
	var user = $('#users .lba-context-icons').data('params').user
	$('#t-edit-name').val(user.user_name);
	$('#t-edit-first-name').val(user.first_name);
	$('#t-edit-email').val(user.email);
	showHideSuperPowerSection(user.role);
	$('#t-edit-login').val(user.login);
	$('#t-edit-account-type').val(user.login_type);
}

/*
 * Starts to impersonate a user
 */
function impersonateUser() {
	var user = $('#users .lba-context-icons').data('params').user
	$.ajax({
		type: "POST",
		url: "/admin/user/"+user.id+"/impersonate",
	}).fail(function(jqXHR) {
		let msg = decodeURIComponent(JSON.parse(jqXHR.responseText));
		if (jqXHR.status === 409) { // conflict
			alert( __("Echec lors de la prise de l'identité de l'utilisateur : {{task}}", { task : msg} ) );
		} else {
			alert(msg);
		}
	}).done(function(data) {
		window.location = "/";
	});
}

/**
 * Sends an ajax request to delete the user
 * @param object user
 */
function deleteUserAjax(user) {
	$.ajax({
		type: "POST",
		url: "/admin/user/"+user.id+"/delete",
	}).fail(function(jqXHR) {
		let msg = decodeURIComponent(JSON.parse(jqXHR.responseText));
		if (jqXHR.status === 409) { // conflict
			alert( __("Echec lors de la suppression de l'utilisateur : {{task}}", { task : msg} ) );
		} else {
			alert(msg);
		}
	}).done(function(data) {
		$("#u-table").DataTable().ajax.reload()
	});
}

/**
 * Ask for confirmation then launch the delete user ajax
 */
function deleteUser() {
	var user = $('#users .lba-context-icons').data('params').user
	global_confirmation_popup.openConfirmationPopup(
		__("Suppression d'un utilisateur"),
		__("En cochant cette case, vous confirmez la suppression définitive de l'utilisateur {{login}} ({{email}}) et de toutes ses données", {'login': user.login, 'email': user.email}),
		function(){
			deleteUserAjax(user);
			global_confirmation_popup.closeConfirmationPopup();
		}
	);
}

/**
 * Dowload the GDPR dump for the selected user
 */
function getUserDump() {
	var user = $('#users .lba-context-icons').data('params').user
	window.open('/admin/user/'+user.id+'/GDPRDump', '_blank');
}

/**
 * Sends an ajax request to delete the user
 * @param object user
 */
function anonymizeUserAjax(user) {
	$.ajax({
		type: "POST",
		url: "/admin/user/"+user.id+"/anonymize",
	}).fail(function(jqXHR) {
		let msg = decodeURIComponent(JSON.parse(jqXHR.responseText));
		if (jqXHR.status === 409) { // conflict
			alert( __("Echec lors de l'anonymisation de l'utilisateur : {{task}}", { task : msg} ) );
		} else {
			alert(msg);
		}
	}).done(function(data) {
		$("#u-table").DataTable().ajax.reload()
	});
}

/**
 * Asks for confirmation then launch the ajax to anonymize the selected user
 */
function anonymizeUser() {
	let user = $('#users .lba-context-icons').data('params').user
	global_confirmation_popup.openConfirmationPopup(
		__("Anonymisation d'un utilisateur"),
		__("En cochant cette case, vous confirmez l'anonymisation définitive de l'utilisateur {{login}} ({{email}}) et de toutes ses données", {'login': user.login, 'email': user.email}),
		function(){
			anonymizeUserAjax(user);
			global_confirmation_popup.closeConfirmationPopup();
		}
	);
}

// Opens the dialog to add institution
function openAddInstDialog() {
	$("#modal-inst-add").dialog("open");
}

/*
 * Creates an institution
 */
function addInstitution(popup, name, cas) {
	$.ajax({
		type: "POST",
		url: "/admin/institutions/create",
		data: {
			name:name,
			cas: cas
		}
	}).fail(function(jqXHR) {
		let msg = decodeURIComponent(JSON.parse(jqXHR.responseText));
		if (jqXHR.status === 409) { // conflict
			alert( __("Echec lors de la création de l'institution : {{task}}", { task : msg} ) );
		} else {
			alert(msg);
		}
	}).done(function(data) {
		popup.dialog("close");
		$("#i-table").DataTable().ajax.reload(function () {
			$("#i-table tr#institution-" + data).click();
			selfTest();
		});
	});
}

/*
 * Opens the dialog to add institution
 */
function openEditInstDialog() {
	var params = $('#institutions .lba-context-icons').data('params')
	if (typeof(params) === 'undefined') {
		return;
	}
	$.ajax({
		type: "GET",
		url: "/admin/institution/"+params.idInst+"/getClasses"
	}).fail(function(jqXHR) {
		let msg = decodeURIComponent(JSON.parse(jqXHR.responseText));
		if (jqXHR.status === 409) { // conflict
			alert( __("Echec lors de la création de l'institution : {{task}}", { task : msg} ) );
		} else {
			alert(msg);
		}
	}).done(function(data) {
		var modal = $("#modal-inst-edit");
		var html = ""
		for(var id in data) {
			html += `<option value="${id}">${data[id]}</option>`
		}
		modal.find('#in-edit-class').html(html);
		modal.find('#in-edit-class').val(params.inst.id_class_teacher);
		modal.find('#in-edit-name').val(params.inst.name);
		modal.find('#in-edit-cas').prop('checked', params.inst.cas === 1);
		modal.dialog("open");
	});
}

/*
 * updates an institution
 */
function updateInstitution(popup, inst, name, cas, classe) {
	$.ajax({
		type: "POST",
		url: "/admin/institution/"+inst,
		data: {
			name:name,
			cas: cas,
			classe: classe
		}
	}).fail(function(jqXHR) {
		let msg = decodeURIComponent(JSON.parse(jqXHR.responseText));
		if (jqXHR.status === 409) { // conflict
			alert( __("Echec lors de la création de l'institution : {{task}}", { task : msg} ) );
		} else {
			alert(msg);
		}
	}).done(function(data) {
		if (data.msg) {
			alertGently(data.msg);
		}
		popup.dialog("close");
		$("#i-table").DataTable().ajax.reload(function () {
			$("#i-table tr#institution-" + inst).click();
			selfTest();
		});
	});
}

/**
 * Deletes an institution
 */
function deleteInstitution() {
	var params = $('#institutions .lba-context-icons').data('params')
	$.ajax({
		type: "POST",
		url: "/admin/institution/"+params.idInst+"/delete",
	}).fail(function(jqXHR) {
		if (jqXHR.status === 409) { // conflict
			alertGently(jqXHR.responseJSON.msg);
		} else {
			let msg = decodeURIComponent(JSON.parse(jqXHR.responseText));
			alertGently(msg);
		}
	}).done(function(data) {
		$("#i-table").DataTable().ajax.reload(function () { selfTest(); });
	});
}

// Opens the dialog to add extplatform
function openAddExtpDialog() {
	let html = "";
	let insts = $('#i-table').DataTable().data().toArray();
	insts.sort((a, b) => a.name.localeCompare(b.name));
	for (let i in insts) {
		let e = insts[i];
		html += `<option value="${e.id}">${e.name}</option>`;
	}
		let modal = $("#modal-extp-add");
	modal.find('#extp-add-inst').html(html);
	modal.dialog("open");
}

/*
 * Creates an extplatform
 */
function addExtp(popup, name, url, inst, contacts) {
	$.ajax({
		type: "POST",
		url: "/admin/extplatforms/create",
		data: {
			name: name,
			url: url,
			id_inst: inst,
			contacts: contacts
		}
	}).fail(function(jqXHR) {
		let msg = decodeURIComponent(JSON.parse(jqXHR.responseText));
		if (jqXHR.status === 409) { // conflict
			alert( __("Echec lors de la création de la plateform externe : {{task}}", { task : msg} ) );
		} else {
			alert(msg);
		}
	}).done(function(data) {
		popup.dialog("close");
		$("#e-table").DataTable().ajax.reload(function () {
			$("#e-table tr#extplatform-" + data).click();
			selfTest();
		});
	});
}

/*
 * Opens the dialog to add an ext platform
 */
function openEditExtpDialog() {
	let params = $('#extplatforms .lba-context-icons').data('params')
	if (typeof(params) === 'undefined') {
		return;
	}
	let modal = $("#modal-extp-edit");
	let html = "";
	let insts = $('#i-table').DataTable().data().toArray();
	insts.sort((a, b) => a.name.localeCompare(b.name));
	for (let i in insts) {
		let e = insts[i];
		html += `<option value="${e.id}">${e.name}</option>`;
	}
	modal.find('#extp-edit-inst').html(html);
	modal.find('#extp-edit-name').val(params.extp.name);
	modal.find('#extp-edit-url').val(params.extp.url);
	modal.find('#extp-edit-secret').val(params.extp.secret);
	modal.find('#extp-edit-inst').val(params.extp.instId);
	modal.find('#extp-edit-contacts').val(params.extp.contacts);
	modal.dialog("open");
}

/*
 * updates an extplatform
 */
function updateExtplatform(popup, extp, name, url, secret, inst, contacts) {
	$.ajax({
		type: "POST",
		url: "/admin/extplatform/"+extp,
		data: {
			name:name,
			url: url,
			secret: secret,
			id_inst: inst,
			contacts: contacts
		}
	}).fail(function(jqXHR) {
		let msg = decodeURIComponent(JSON.parse(jqXHR.responseText));
		if (jqXHR.status === 409) { // conflict
			alert( __("Echec lors de la modification de la plateforme externe : {{task}}", { task : msg} ) );
		} else {
			alert(msg);
		}
	}).done(function(data) {
		if (data.msg) {
			alertGently(data.msg);
		}
		popup.dialog("close");
		$("#e-table").DataTable().ajax.reload(function () {
			$("#e-table tr#extplatform-" + data).click();
			selfTest();
		});
	});
}

/**
 * Deletes an institution
 */
function deleteExtp() {
	var params = $('#extplatforms .lba-context-icons').data('params')
	$.ajax({
		type: "POST",
		url: "/admin/extplatform/"+params.idExtp+"/delete",
	}).fail(function(jqXHR) {
		if (jqXHR.status === 409) { // conflict
			alertGently(jqXHR.responseJSON.msg);
		} else {
			let msg = decodeURIComponent(JSON.parse(jqXHR.responseText));
			alertGently(msg);
		}
	}).done(function(data) {
		$("#e-table").DataTable().ajax.reload(function () { selfTest(); });
	});
}

/**
 * Runs self test
 */
function selfTest() {
	var html = '<i class="wait fa fa-spinner fa-pulse"></i>&nbsp;'+__("Verification de votre configuration en cours");
	$('#adminstatus').html(html);
	$.ajax({
		type: "POST",
		url: "/admin/selfTest"
	}).done(function(messages) {
		if (messages['warn'].length == 0 &&  messages['err'].length ==0) {
			var msg = __("Votre installation est à jour et bien configurée");
			var html = '<li class="success">'+msg+'</li>'
		} else {
			var html="";
			for(var type in messages) {
				for (var id in messages[type]) {
					html += '<li class="'+type+'">'+messages[type][id]+'</li>';
				}
			}
		}
		console.log(html);
		$('#adminstatus').html(html);
	});
}

/**
 * Show hide debug infos
 */
function toggleDebugInfos()
{
	var section=$('#debug');
	if (section.is(':hidden')) {
		var new_class = 'fa-caret-down'
		var old_class = 'fa-caret-right'
	} else {
		var new_class = 'fa-caret-right'
		var old_class = 'fa-caret-right'
	}
	var icon = $('#toggle-debug');
	icon.removeClass(old_class);
	icon.addClass(new_class);
	section.toggle();
}

/**
 * Sends a test emal
 */
function sendTestMail() {
	$.ajax({
		type: "post",
		url: "/admin/sendTestMail"
	}).done(function(message) {
		alertGently(message, 'info')
	});
}
