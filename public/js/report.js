/**
 * @return {boolean} Is allowed to quit?
 */
async function lockedByEditing() {
	if (global_edited_ld) {
		const isValid = await validateLD(global_edited_ld);
		if (!isValid) {
			alertGently(__("Pensez à vérifier le contenu de votre labdoc."), "danger", 10000);
			return false;
		} else {
			alertGently(__("Un labdoc a été validé automatiquement."), "info", 5000);
		}
	}
	return true;
}

/**
 *  hide or show all LD of the report
 */
function toggleAllLD() {
	let extend;
	if ($(".labdoc_content").is(":visible") || $(".lb_div_assignment").is(":visible") || $(".labdoc_assignment").is(":visible")) {
		extend = 0;
		$(".lb_div_assignment").hide();
		$(".labdoc").each(function () {
			let ld_content = $(this).find(".labdoc_content");
			const ld_assignment = $(this).find(".labdoc_assignment");
			ld_assignment.hide();
			if (ld_content.is(":visible")) {
				ld_content.hide();
				let icon = $(this).find(".lb_show_hide_ld i");
				icon.removeClass("fa-chevron-right").addClass("fa-chevron-right");
				icon.attr('title', "Déplier le labdoc");
			}
		});
		updateIconForToggleAllLD("right");
		
		// Change general chevron
		$('#lb-toggle').find('i').removeClass('fa-chevron-down').addClass('fa-chevron-right');
		
	} else {
		extend = 1;
		$(".labdoc").each(function () {
			let ld_content = $(this).find(".labdoc_content");
			if (ld_content.is(":hidden")) {
				ld_content.show();
				let icon = $(this).find(".lb_show_hide_ld i");
				icon.removeClass("fa-chevron-right").addClass("fa-chevron-down");
				icon.attr('title', __("Replier le labdoc"));
			}
		});
		updateIconForToggleAllLD("down");

		// Change general chevron
		$('#lb-toggle').find('i').removeClass('fa-chevron-right').addClass('fa-chevron-down');
	}
	if (window.global_scope !== "follow") { // do not update in DB in scope follow
		$.ajax({
			method: 'POST',
			url: "/report/" + window.global_id_report + "/toggleAllLD",
			data: {
				"extend": extend
			}
		});
	}
}

/*
 * Set the global variable that may stop the synchro of LD
 */
function toggleFreezeSyncLD() {
	if (typeof window.global_stop_sync !== 'undefined' && window.global_stop_sync) {
		window.location.replace("/report/" + window.global_id_report + "?sc=follow");
	} else {
		freezeSyncLD();
	}
}

function getTitleFreezeBtn(stop_sync) {
	return stop_sync?__('Réactiver la synchronisation du rapport avec celui des étudiants'):__("Geler le rapport : tant que le rapport est gelé, les labdocs ne sont plus synchronisés avec ceux des étudiants");
}


/**
 * Freeze labdoc synchronization
 */
function freezeSyncLD(){
	if (typeof window.global_stop_sync == 'undefined' || !window.global_stop_sync) {
		window.global_stop_sync = true;
		alertGently(__("Mode gelé : les modifications apportées aux labdocs par les étudiants ne sont plus synchronisées sur cette page"), "info", 5000);
		$.ajax({
			type: "POST",
			url: "/report/" + global_id_report + "/freezeReportTrace"
		})
	}
}


/**
 * Update the state of the icon that toggles all the LD of the report
 * @param {string} state : "up"|"right"|null
 */
function updateIconForToggleAllLD(new_state) {
	let icon = $("#lb_toggle_all_ld");
	if (!new_state) {
		if ($(".labdoc_content").is(":visible") || $(".lb_div_assignment").is(":visible")) {
			new_state = "down";
		} else {
			new_state = "up";
		}
	}
	if (new_state == "down") {
		icon.removeClass("fa-chevron-right").addClass("fa-chevron-down");
		icon.attr('title', __("Replier tous les labdocs"));
	} else if (new_state == "up") {
		icon.removeClass("fa-chevron-down").addClass("fa-chevron-right");
		icon.attr('title', __("Déplier tous les labdocs"));
	}
}

async function openDialogPrintPage(role, id_report){ // open a dialog to print the report
	const isOpenedLdValidated = await lockedByEditing();
	if (isOpenedLdValidated) {
		// Remove old pdf modal
		document.querySelectorAll('#lb_pdf_dialog').forEach(el => el.remove());
		$.ajax({
			type: "POST",
			url: "/report/" + id_report + "/listLdToPrint",
			data: {
				scope: global_scope,
				role: role,
				id_report: id_report
			},
			success: function (html) {
				let div = document.createElement('div');
				div.innerHTML = html.trim();
				document.body.appendChild(div.firstChild);
			},
			error: function (jqXHR) {
				alertGently(jqXHR.responseJSON.message);
			}
		});
	}
}

/**
 * Passe le rapport au status 'wait'
 */
async function submitReport() {
	const isOpenedLdValidated = await lockedByEditing();
	if (isOpenedLdValidated) {
		if (confirm(__("ATTENTION !") + "\n\n" + __("Si vous rendez votre rapport à l'enseignant, VOUS NE POURREZ PLUS LE MODIFIER.") + "\n" + __("Êtes-vous sûr de vouloir rendre votre rapport ?"))) {
			// on passe le status de la mission de on à wait et on retourne à la page de choix des missions
			$.ajax({
				type: "POST",
				url: "/report/" + global_id_report + "/submit",
				success: function (data) {
					if (!data.success) {
						alertGently(data.message);
					} else {
						window.location = '/reports';
					}
				},
				error: function (jqXHR) {
					alertGently(jqXHR.responseJSON.message);
				}
			});
		}
	}
}


// ********************************************************************************************************
//             Functions of the RP
// ********************************************************************************************************

/**
* @param {int} id_rp
* @param {boolean} ld_txt
* @param {boolean} ld_drw
* @param {boolean} ld_pro
* @param {boolean} ld_ds
* @param {boolean} ld_cde
* @returns {undefined}
*/
function displayLDToImport (id_rp, id_m, ld_txt, ld_drw, ld_pro, ld_ds, ld_cde) {
	if (global_scope === 'test') {
		alertGently(__("L'import de labdoc n′est pas fonctionnel en mode test"), "warning", 2500);
		return;
	}
	$.ajax({
		method: 'POST',
		url: '/report/' + global_id_report + '/displayLDToImport',
		data: {
			id_rp: id_rp,
			id_m: id_m,
			ld_txt: ld_txt,
			ld_drw: ld_drw,
			ld_pro: ld_pro,
			ld_ds: ld_ds,
			ld_cde: ld_cde,
			role: global_scope === 'follow' ? 'teacher' : 'learner'
		},
		success:function(response){
			$('#report_part_' + id_rp + ' .ld_import_menu').html(response);
		},
		error: function (jqXHR, textStatus) {
			$('#report_part_' + id_rp + ' .ld_import_menu').hide();
			handleLDAjaxError(jqXHR, textStatus);
		}
	});
};
function getMsgNoLdsToImport(is_follow, m_code){
	return (is_follow || !m_code)?__('Il n y aucun labdoc à importer dans cette partie')
     :__('Il n y a aucun labdoc du rapport ({{m_code}}) à importer dans cette partie',{m_code:m_code});
}
function importLD (id_ld , id_rp) {
	if (id_ld !== '-1') {
		duplicateLD(id_ld, id_rp, 0, 0);
	}
}

// ********************************************************************************************************
//             Functions of the LD
// ********************************************************************************************************

/**
 * Handle common AjaxErrors
 * @param jqXHR
 * @param textStatus
 */
function handleLDAjaxError(jqXHR, textStatus) {
	if (jqXHR.status === 404) {
        alertGently( __("Action impossible, le labdoc n'existe plus. Merci de recharger la page (touche F5)."));
	} else {
		alertGently(jqXHR.responseJSON.message);
	}
}

/**
 * toggle the display of the LD - can be forced with show_hide
 * @param {int} id_ld
 * @param {string} show_hide : "show"|"hide"|null
 */
function showHideLD(id_ld, show_hide = null){ // affiche ou cache le LD
	let icon = $('#lb_show_hide_ld_'+id_ld+' i');
	let extend = -1;
	let selection = window.getSelection();
	if((selection.toString().length >0)
		&& (window.global_scope == "view" || window.global_scope == "follow")
		&& ("ld_name_"+id_ld===selection.anchorNode.id)) {
		return;
	}
	if($("#labdoc_content_"+id_ld).is(":hidden") && show_hide != "hide"){
		$("#labdoc_content_"+id_ld).show();
		icon.removeClass("fa-chevron-right").addClass("fa-chevron-down");
		icon.attr('title', __("Replier le labdoc"));
		extend = 1;
		updateIconForToggleAllLD("down");
	}
	else if (show_hide != "show") {
		$("#labdoc_assignment_container_"+id_ld).hide();
		$("#labdoc_content_"+id_ld).hide();
		icon.removeClass("fa-chevron-down").addClass("fa-chevron-right");
		icon.attr('title', __("Déplier le labdoc"));
		extend = 0;
		$('#labdoc_'+id_ld).css('min-height', '') ; //if user is hiding LD remove min-height
		updateIconForToggleAllLD();
		if ($("#labdoc_content_"+id_ld).hasClass('ld_code')) {
			let spinner = $("#labdoc_content_"+id_ld).parent().find('i.fa-spinner');
			if (spinner.length) {
				spinner.hide();
			}
		};
	}
	if (extend != -1 && window.global_scope !== "follow") {
		$.ajax({
			method: "POST",
			url: "/labdoc/" + id_ld + "/extend",
			data: {
				"extend": extend
			},
			error: function (jqXHR, textStatus) {
				handleLDAjaxError(jqXHR, textStatus);
			}
		});
	}
}


/**
 * Hide the little star at the left of a labdoc (which indicates something new happened since last visit)
 * And update last id version reviewed
 * @param {int} id_ld
 */
function hideStar(id_ld) {
	let stored_labdoc = Alpine.store('reportLabdocs').getLd(id_ld);
	if (stored_labdoc && stored_labdoc.status==='modified_ld') {
		stored_labdoc.status = 'free_ld';
		$.ajax({
			method: 'POST',
			url: '/labdoc/'+id_ld+'/hideStar',
			data: {
				last_id_version_reviewed: stored_labdoc.last_id_version_before_freeze,
			},
			error: function (jqXHR, textStatus) {
				handleLDAjaxError(jqXHR, textStatus);
			}
		});
	}
}

/**
 * Toggle the draft (hidden) status of the LD
 * @param {int} id_ld
 */
function toggleDraftStatus(id_ld) {
	let store_labdoc = Alpine.store('reportLabdocs').getLd(id_ld);
	const to_draft = (store_labdoc && !store_labdoc.is_draft)?1:0;
	if(!to_draft || confirm( __("Attention ! Seul le titre du labdoc sera visible par votre enseignant"))) {
		$.ajax({
			type: "POST",
			url: "/labdoc/"+id_ld+"/toggleDraftStatus",
			data: {
				draft: to_draft,
				id_report: global_id_report
			},
		}).done(function(data) {
			if (data) {
				alertGently(data);
			}
			else {
				changeDraftDisplays (id_ld, to_draft);
			}
		}).fail(function (jqXHR, textStatus) {
			handleLDAjaxError(jqXHR, textStatus);
		});
	}
}

/**
 *
 * @param {int} id_ld
 * @param {boolean} to_draft
 */
function changeDraftDisplays(id_ld, to_draft) {
	let store_labdoc = Alpine.store('reportLabdocs').getLd(id_ld);
	if(store_labdoc) {
		store_labdoc.is_draft = to_draft;
	}
	// content display
	if (global_scope === 'follow') {
		if (to_draft) {
			$("#labdoc_content_"+id_ld).remove();
			$("#lb_show_hide_ld_"+id_ld).html("");
		} else {
			getAndDisplayLD(id_ld, 0, "unchanged", false);
		}
	}
}

/**
 * add a LD at the end of the report part
 * @param {int} id_rp
 * @param {string} type : "text","dataset","drawing","procedure","code"
 * @return {boolean}
 */
async function addLD(id_rp, type){
	const isOpenedLdValidated = await lockedByEditing();
	if (!isOpenedLdValidated) {
		return false;
	} else {
		$.ajax({
			type: "POST",
			url: "/report/" + global_id_report + "/addLD",
			data: {
				id_rp: id_rp,
				type: type
			},
			success: function (id_ld) {
				if (id_ld > 0) {
					$("#rp_sortable_" + id_rp).append('<div id="labdoc_' + id_ld + '" class="sortable-labdoc labdoc" data-id_ld="' + id_ld + '"></div>');
					getAndDisplayLD(id_ld, 1, 1, 1, 1);
				} else {
					logError("addLD(): AJAX response with an empty id_ld", 'danger', id_ld);
					alertGently(__("Erreur lors de la création."));
				}
				return true;
			},
			error: function (jqXHR) {
				alertGently(jqXHR.responseJSON.message);
				return false;
			}
		});
	}
}

/**
 *
 * @param {int} id_ld  to be duplicated
 * @param {int} id_rp  destination RP
 * @param {boolean} edition  to be opened in edition
 * @param {boolean} incr_name  name to be changed with a number
 * @returns {boolean}
 */
function duplicateLD(id_ld, id_rp, edition, incr_name) { // duplicate le LD en fin de RP
	if (global_edited_ld && edition) {
		alert ( __("Vous ne pouvez pas dupliquer de labdoc tant que vous avez un labdoc en modification.") );
		return false;
	}
	$.ajax({
		type: "POST",
		url: "/report/"+global_id_report+"/duplicateLD",
		data: {
			id_ld: id_ld,
			edition: edition,
			id_rp: id_rp,
			incr_name: incr_name,
			scope: window.global_scope
		},
		success:function(data){
			var new_id_ld = data ;
			if (id_ld > 0) {
				if (!id_rp) {
					id_rp = "0";
				}
				$("#rp_sortable_"+id_rp).append('<div id="labdoc_'+new_id_ld+'" class="sortable-labdoc labdoc" data-id_ld="' + new_id_ld + '"></div>');
				getAndDisplayLD(new_id_ld, edition, 1, 1, 1);
				return true;
			} else {
				logError("duplicateLD(): AJAX response with an empty id_ld", 'danger', data);
				alertGently(__("Erreur lors de la duplication."));
			}
		},
		error: function (jqXHR, textStatus) {
			handleLDAjaxError(jqXHR, textStatus);
		}
	});
	return false;
}

function removeLDFromDom(id_ld, shared = "0") {
	if(global_scope != "follow" && $('#trash').css('display') != "none" && shared === "0" ){ // Rafraichissement de la poubelle
		displayDeletedLDinTrash(global_id_report, false);
	}
	$('#labdoc_'+id_ld).remove() ;
	Alpine.store('reportLabdocs').deleteLd(parseInt(id_ld));
	if(parseInt(Alpine.store('commentWidgetIdLd')) === parseInt(id_ld)){
		Alpine.store('commentWidgetIdLd', '');
	}
}

/**
 * delete a LD in the DB
 * @param {int} id_ld
 */
function deleteLD(id_ld){
	$.ajax({
		type: "POST",
		url: "/report/"+global_id_report+"/deleteLD",
		data: {
			id_ld: id_ld,
		},
		success:function(data){
			if (data.status == "ok" || data.status == "deleted") { // suppression de la div
				removeLDFromDom(id_ld);
				if (global_tab_ld[id_ld] && global_tab_ld[id_ld].ld_history) {
					global_tab_ld[id_ld].ld_history.length = 0; // initialize the array
				}
			} else if (data.status == "locked") {
				alertGently(data.message, "warning");
				$("#ld_name_actions_"+id_ld).find(".locked_ld").show();
				getAndDisplayLDContent(id_ld,0,"unchanged",0);
			}
			else {
				alertGently(data.message);
			}
		},
		error: function (jqXHR, textStatus) {
			handleLDAjaxError(jqXHR, textStatus);
		}
	});
}

/**
 * cancel the creation of a new LD (add or duplicate)
 * @param {int} id_ld
 */
async function cancelLD(id_ld) {
	if (global_tab_ld[id_ld].ld_type == "text" || global_tab_ld[id_ld].ld_type == "code" || isProcedure(id_ld, 'json')) {
		// TinyMCE : save the content that is not saved at each click
		await saveLocalLDVersion(id_ld, 1);
	}
	global_tab_ld[id_ld].send_in_db = false; // prevents future auto-save
	let ld_content = getLDContentFromLocalHistory(id_ld);

	if (global_tab_ld[id_ld].ld_history.length < 2 || !ld_content) {
		// The LD has not been modified by the user or its content is empty => delete definitely the LD
		// Extracts of code from the deleteLD function with async false
		$.ajax({
			type: "POST",
			async: false,
			url: "/report/"+global_id_report+"/deleteLD",
			data: {
				id_ld: id_ld,
			},
			success:function(data){
				if (data.status == "ok" || data.status == "deleted") {
					removeLDFromDom(id_ld);
					if (global_tab_ld[id_ld] && global_tab_ld[id_ld].ld_history) {
						global_tab_ld[id_ld].ld_history.length = 0;
					}
				}
				else {
					alertGently(data.message);
				}
			},
			error: function (jqXHR, textStatus) {
				handleLDAjaxError(jqXHR, textStatus);
			}
		});
		window.global_edited_ld = 0;
		realDeleteLD(global_id_report, id_ld, true);
	} else {
		// Put the LD in the trash can
		// Extracts of code from the validateLD function
		if (global_tab_ld[id_ld].ld_type === "text") {
			ld_content = removeEquationsDisplays(ld_content);
		}
		let ld_name = getLDNameFromDom(id_ld);
		if (!ld_name) {
			ld_name = __("Labdoc annulé");
		}
		$.ajax({
			type: "POST",
			async: false, // mandatory to know if the LD has really been saved
			url: "/report/" + global_id_report + "/validateLD",
			data: {
				id_ld: id_ld,
				name: ld_name,
				content: ld_content,
				recovering: 0,
				new_version: 1
			},
			error: function (xhr) {
				logError("validateLD() error", "error", {id_ld: id_ld, xhr: xhr});
				var message = typeof xhr.responseJSON === 'undefined' ? "" : xhr.responseJSON.message;
				alertGently(__("Le labdoc n'a pas pu être enregistré sur le serveur. " + message));
			}
		});
		deleteLD(id_ld);
		window.global_edited_ld = 0;
		alertGently(__("Le labdoc a été placé dans la corbeille"), "info");
	}
}

/**
 * Checks that no labdoc is being edited and start the edition mode for the
 * given labdoc
 * @param {int} id_ld
 * @returns {boolean}
 */
async function checkAndStartEdition(id_ld) {
	const isOpenedLdValidated = await lockedByEditing();
	if (!isOpenedLdValidated) {
		return false;
	} else {
		showHideLD(id_ld, "show");
		return true;
	}
}

/**
 * Edit a labdoc in the report
 * @param {int} id_ld
 * @returns {boolean}
 */
async function editLD(id_ld){
	const isValid = await checkAndStartEdition(id_ld);
	if (!isValid) {
		return false;
	}
	$.ajax({
		type: "POST",
		url: "/report/"+global_id_report+"/editLD",
		data: {
			id_ld: id_ld,
		},
		success:function(data){
			if (data.status == "ok") {
				displayLDHeader(id_ld, data.html, 1);
			} else if (data.status == "locked") {
				alertGently(data.message, "warning");
				displayLDHeader(id_ld, data.html, 0, 1, 0);
			} else if (data.status == "forbidden") {
				alertGently(data.message, "warning");
			} else if (data.status == "deleted") {
				alertGently(data.message);
				removeLDFromDom(id_ld);
			}
			return true;
		},
		error: function (jqXHR, textStatus) {
			handleLDAjaxError(jqXHR, textStatus);
		}
	});
}

function checkLDname(name) {
	if(name.length == 0){
		alertGently( __("Le labdoc ne peut pas être validé car son titre n'est pas renseigné.") );
		return false;
	}
	return true ;
}

/**
 * Check everything before sending the LD name and content for update in DB
 * Used when validating an edited LD or when recovering a LD version
 * @param {int} id_ld
 * @param {boolean} preventSavingIfEmptyTitle whether to save or not if the title is not filled
 * @return {boolean}
 */
async function validateLD(id_ld, preventSavingIfEmptyTitle=false){

	let ld_name;
	let ld_content;
	let recovering = global_tab_ld[id_ld].recovering; // validation of the LD from the recovering interface
	let recovering_id_version = null;

	if (recovering) {
		ld_name = $("#ld_versions_name").text();
		ld_content = getLDContentFromLocalHistory(id_ld);
		recovering_id_version = $("#ld_versions_restore").attr("data-id_version");
	} else {
		// pre-treatment when validating an edited LD
		ld_name = getLDNameFromDom(id_ld);
		if (preventSavingIfEmptyTitle && !checkLDname(ld_name)) {
			return false;
		}
		// copex: close the edited elements if possible, else error message
		if (isProcedure(id_ld, 'xml') && !checkProcLDLocker(id_ld)) {
			return false;
		} else if (global_tab_ld[id_ld].ld_type == "dataset") {
			// © Cédric d'Ham (in order to save the current cell value)
			$("#labdoc_content_"+id_ld+" .fx_table_wrapper").click();
			setTimeout(function(){},100);
		} else if (global_tab_ld[id_ld].ld_type == "text" 
			|| global_tab_ld[id_ld].ld_type == "code"
			|| isProcedure(id_ld, 'json')) {
			// TinyMCE : save the content that is not saved at each click
			await saveLocalLDVersion(id_ld, 1);
		} else if (global_tab_ld[id_ld].ld_type == "drawing") {
            let drawingResponsePromise = new Promise((resolve, reject) => {
                window.addEventListener('drawing-content-saved', event => {
                    if (event.detail.id_ld == id_ld) {
                        resolve(event.detail.acknowledgement);
                    }
                }, { once: true });
            });
            let timeOutPromise = new Promise((resolve, reject) => {
                setTimeout(function() {
					// do not reject because it stops the flow and prevent saving
                    resolve(true);
					console.log('Drawing : timeout error while saving')
                }, 1000);
            });
            window.dispatchEvent(new CustomEvent('drawing-save-required', { detail: { id_ld: id_ld } }));
            await Promise.race([drawingResponsePromise, timeOutPromise]);
		}
		ld_content = getLDContentFromLocalHistory(id_ld);
		// management of an empty content for the LD
		if (!ld_content) {
			if (confirm(__("Vous êtes sur le point de valider un labdoc vide. Est-ce bien ce que vous souhaitez faire ?"))) {
				ld_content = "";
			} else {
				return false;
			}
		}
	}

	// save the LD in DB and display its structure in view mode
	global_tab_ld[id_ld].send_in_db = false; // prevents future auto-save
	if (global_tab_ld[id_ld].ld_type === "text") {
		ld_content = removeEquationsDisplays(ld_content);
	}
	// check if a new version is necessary : only if the name or the content of the LD did change
	let new_version = 0;
	if ((ld_name != global_tab_ld[id_ld].ld_initial_name || global_tab_ld[id_ld].current_i > 0) && !recovering) {
		new_version = 1;
	}
	$.ajax({
		type: "POST",
		async: false, // mandatory to know if the LD has really been saved in order to close it and to open another one
		url: "/report/" + global_id_report + "/validateLD",
		data: {
			id_ld: id_ld,
			name: ld_name,
			content: ld_content,
			recovering: Number(recovering),
			new_version: new_version,
			recovering_id_version: recovering_id_version
		},
		error: function (xhr) {
			if (xhr.status != 422) {
				logError("validateLD() error", "error", {id_ld: id_ld, xhr: xhr});
			}
			var message = typeof xhr.responseJSON === 'undefined' ? "" : xhr.responseJSON.message;
			alertGently(__("Le labdoc n'a pas pu être enregistré sur le serveur. " + message));
		},
		success: function (data) {
			displayLDHeader(id_ld, data.html);
			if (data.error) {
				alertGently(__("Nous avons détecté un problème dans le code du labdoc enregistré - Vérifiez l'intégrité de votre labdoc - Si besoin, vous pouvez revenir à une version antérieure avec la fonction \"Récupérer\" accessible dans le menu du labdoc."), 'warning', 5000);
			}
		}
	});

	window.global_edited_ld = 0;
	global_tab_ld[id_ld].recovering = false;
	return true ;
}

/**
 * Open the version history for a LD in the report
 * @param {int} id_ld
 * @returns {boolean}
 */
async function openLDVersioning(id_ld) {
	const isValid = await checkAndStartEdition(id_ld);
	if (!isValid) {
		return false;
	}
	$.ajax({
		type: "POST",
		url: "/report/"+global_id_report+"/openLDVersioning",
		data: {
			id_ld: id_ld,
		},
		success:function(data){
			if (data.status == "ok") {
				global_tab_ld[id_ld].recovering = true;
				global_edited_ld = id_ld ;
				$('#labdoc_'+id_ld).html(data.html);
				requestAnimationFrame(() => requestAnimationFrame(function(){ // wait for the DOM being updated
					$('#ld_versions .ld_version_current').click();
					$('#ld_versions').scrollTop($('.ld_version_current').offset().top - $('#ld_versions').offset().top - 30);
					scrollToDisplay("#labdoc_"+id_ld) ;
				}));
			} else if (data.status == "locked") {
				alertGently(data.message, "warning");
				displayLDHeader(id_ld, data.html, 0, 1, 0);
			} else if (data.status == "deleted") {
				alertGently(data.message);
				removeLDFromDom(id_ld);
			}
			return true;
		},
		error: function (jqXHR, textStatus) {
			handleLDAjaxError(jqXHR, textStatus);
		}
	});
}


/**
 * Display a version of a LD
 * @param {int} id_ld
 * @param {int} id_vers n° de la trace qui correspond à la version du LD
 */
function displayLDVersion(id_ld, id_vers) {
	// highlight version
	$("#ld_versions li").removeClass("selected");
	// id_vers may be null (in case of pre-filled version)
	var id_vers = ((typeof id_vers === 'undefined') ? '' : id_vers);
	$("#labdoc_" + id_ld + " #ld_version_" + id_vers).addClass("selected");
	// show LD
	global_tab_ld[id_ld].ld_history = [$('#labdoc_' + id_ld + ' #ld_version_content_' + id_vers).text()];
	displayLDContent(id_ld, 0, 1, 0, 1);
	$("#labdoc_" + id_ld + " #ld_versions_name").text($("#labdoc_" + id_ld + " #ld_version_name_"+id_vers).text());
	$("#labdoc_" + id_ld + " #ld_versions_name").data("id_vers", id_vers);
	// restore button
	if ($("#labdoc_" + id_ld + " #ld_version_"+id_vers).hasClass("ld_version_current")) {
		$("#ld_versions_restore").hide();
	} else {
		$("#ld_versions_restore").show();
	}
	// show code
	$("#ld_versions_code").text($("#labdoc_" + id_ld + " #ld_version_content_"+id_vers).text());
	$("#ld_versions_restore").attr("data-id_version", id_vers);
}


/**
 * Closes the version history for a LD in the report
 * @param {int} id_ld
 * @returns {boolean}
 */
function closeLDVersioning(id_ld) {
	$.ajax({
		type: "POST",
		url: "/report/"+global_id_report+"/closeLDVersioning",
		data: {
			id_ld: id_ld,
		},
		success:function(data){
			displayLDHeader(id_ld, data.html, 0, 1, 0);
			global_tab_ld[id_ld].recovering = false;
			window.global_edited_ld = 0;
		},
		error: function (jqXHR, textStatus) {
			handleLDAjaxError(jqXHR, textStatus);
		}
	});
}


/**
 * Open a rw editor (for code or dataset labdoc)
 * The user can modify but it is not saved neither locally nor in database
 * @param {int} id_ld
 * @returns {boolean}
 */
function teacherEditLD(id_ld) {
	if (["code", "dataset", "procedure"].includes(window.global_tab_ld[id_ld].ld_type)) {
		document.getElementById("ld_test_without_saving_" + id_ld).dataset.inEdit = 'true';
		displayLDContent(id_ld, true);
		showHideLD(id_ld, "show");
	}
	$("#ld_test_without_saving_" + id_ld).hide();
	$("#ld_exit_test_without_saving_" + id_ld).show();
	alertGently(__("Test du labdoc") + " '" + $("#ld_name_" + id_ld).text() + "'. " + __("Aucune modification ne sera enregistrée."), "warning", 8000);
	return true;
}


/**
 * Go back to read only mode (after a teacherEditLD)
 * @param {int} id_ld
 * @returns {boolean}
 */
function teacherUnEditLD(id_ld) {
	if (["code", "dataset", "procedure"].includes(window.global_tab_ld[id_ld].ld_type)) {
		getAndDisplayLDContent(id_ld, false, "unchanged");
		document.getElementById("ld_test_without_saving_" + id_ld).dataset.inEdit = 'false';
	}
	$("#ld_test_without_saving_" + id_ld).show();
	$("#ld_exit_test_without_saving_" + id_ld).hide();
	if (window.annotatorActions) {
		window.annotatorActions.refreshAllAnnotations();
	}
	return true;
}
