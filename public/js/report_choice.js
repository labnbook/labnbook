/* global i18next_Promise */

function displayMissionDescription(id_report, id_team_config, id_mission) {
	$.ajax({
		method: 'POST',
		url: '/reports/getRecordDescription',
		data: {
			id_report:id_report,
			id_team_config:id_team_config,
			id_mission:id_mission
		},
		success:function(response){
			$cadre_mission = $('#cadre_mission');
			updateHtmlWithFormulas('#cadre_mission', response);
			$('#cadre_mission .block-radio').show();
			let offset = $cadre_mission.offset().top - $(window).scrollTop();
			if(offset > (window.innerHeight-200)){
				$('html, body').animate({
					scrollTop: offset
				}, 200);
			}
		},
		error: function (jqXHR) {
			alertGently(jqXHR.responseJSON.message);
		}
	});
}

/**
 * Toggle the folding of a category of missions
 * @param dom_element - .menu_mission_title
 */
function toggleMissionsCategory(dom_element) {
	let parent = dom_element.parent('.menu_mission');
	let category_id = parent.attr('id');
	let missions_list = parent.find('.menu_mission_contents');
	if (missions_list.is(':hidden')) {
		showHideMissionsCategory(category_id, true);
	} else {
		showHideMissionsCategory(category_id, false);
	}
}

/**
 * Toggle the folding of a category of missions
 * @param category_id
 * @param {boolean} show
 */
function showHideMissionsCategory(category_id, show){
	let category = $('#'+category_id);
	let missions_list = category.find('.menu_mission_contents');
	if (missions_list.find('.menu_mission_item').length === 0) {
		return;
	}
	let icon = category.find('.menu_mission_title .fa');
	let old_class, new_class, title;
	if (show) {
		old_class='fa-chevron-right';
		new_class='fa-chevron-down';
		title=__('Replier les missions');
		missions_list.show();
	} else {
		old_class='fa-chevron-down';
		new_class='fa-chevron-right';
		title=__('Déplier les missions');
		missions_list.hide();
	}
	icon.removeClass(old_class).addClass(new_class).prop('title', title);
}

$(window).bind("unload", function() {
	enableAndHideSpinner($('#btn_form_submit'));
});

$(document).ready(function() {
	typesetHtmlWithFormulas('.mission_description')
	i18next_Promise.then(function() {
		var confirmButtons = [
			{
				text: __("Continuer malgré les erreurs"),
				"class": "btn btn-danger",
				click: function() {
					$(this).dialog('close');
				}
			},
			{
				text: __("Se déconnecter"),
				"class": "btn btn-info",
				click: function() {
					logout();
				}
			}
		];
		if (navigator.appName === 'Microsoft Internet Explorer' || navigator.userAgent.match(/Trident/)) {
		// if (navigator.appName === 'Netscape' || navigator.userAgent.match(/Trident/)) {
			modalConfirm(
				__("Vous utilisez Internet Explorer. Ce navigateur n'a pas été testé avec LabNBook, et il provoquera certainement des <strong>bugs</strong> sur certaines pages.")
				+ "<p style=\"margin-top:1ex\"><strong>" + __("Firefox ou Chrome sont recommandés.") + "</strong></p>",
				'danger',
				confirmButtons
			);
		} else if (!Array.prototype.includes) {
			modalConfirm(
				__("Votre navigateur web semble ancien, et LabNBook n'a pas été testé avec celui-ci.")
				+ "<div><strong>" + __("Firefox ≥ 45 ou Chrome ≥ 47 sont recommandés.") + "</strong></div>",
				'danger',
				confirmButtons
			);
		}
	});

	$(".mission-search input.search-input").on('input', function(val) {
		let open_categories_ids = [];
		// find the categories to open
		if(val.currentTarget.value.length > 0) {
			let num_results = 0;
			let current_item;
			$(".menu_mission_item").each(function() {
				let regExp = new RegExp(val.currentTarget.value, 'i')
				if (this.textContent.match(regExp)) {
					let id = $(this).parents(".menu_mission").attr("id");
					if(id && !open_categories_ids.includes(id)) {
						open_categories_ids.push(id);
					}
					num_results++;
					$(this).show();
					current_item = $(this);
				} else {
					$(this).hide();
				}
			});
			if (num_results === 1) {
				current_item.click(); // Not so easy to improve it (the selector of displayMissionDescription is too complex)
			}
		} else {
			$(".menu_mission_item").show();
			open_categories_ids.push("new_missions", "current_missions", "tutorial_missions");
		}
		// fold / unfold the categories
		$(".menu_mission").each(function () {
			let id = $(this).attr("id");
			if(open_categories_ids.includes(id) ){
				showHideMissionsCategory(id, true);
			} else {
				showHideMissionsCategory(id, false);
			}
		})
	});
});
