/* 
 * @license http://www.gnu.org/licenses/gpl-3.0.html  GNU GPL v3
 */

"use strict";

/**
 * From an input[type=file] that uploads an image, return several functions to transform it.
 * https://developer.mozilla.org/en-US/docs/Web/API/File
 *
 * @param {object} uploadInput DOM element (from jQuery) for the input[type=file]
 * @returns {object} Functions "resizeImageToEncoded" and "uploadImage".
 */
function uploadImage(uploadInput) {
	const maxWidth = 844;
	const maxSizeMo = (window.location.pathname.indexOf("/report") == 0) ? 2 : 8;
	const maxSize = maxSizeMo*1024*1024;
 
	const defaultMimeType = 'image/jpeg';
	const fileExtensions = {
		'image/jpeg': '.jpg',
		'image/png': '.png'
	};
	const jpegQuality = 0.8; // 0=lowest to 1=best
	const resizingOptions = {
		unsharpAmount: 80,
		unsharpRadius: 0.6,
		unsharpThreshold: 2,
		alpha: true
	};

	var mimeType = uploadInput.files[0].type;
	if (!(mimeType in fileExtensions)) {
		mimeType = 'image/jpeg';
	}

	/**
	 * Create an < img src="data-uri:..." /> from the first uploaded file of the input.
	 *
	 * @return {Promise} Wraps the DOM Element of the image uploaded.
	 */
	function createImage() {
		return new Promise(function(resolve, reject) {
			if (typeof uploadInput !== "object") {
				return reject("resizeImageOnUpload() was called on a strange value.");
			}
			if (uploadInput.files.length === 0) {
				return reject(""); // resizeImageOnUpload() sees no uploaded file.
			}
			const upForm = uploadInput.closest('form');
			if ($(upForm).find('.upload-image-source').length === 0) {
				$(upForm).append('<div class="upload-image-process"><img src="" class="upload-image-source" /><canvas class="upload-image-dest"></canvas></div>');
			}
			const srcImg = upForm.querySelector('img.upload-image-source');
			$(srcImg).on('load', function() {
				resolve(srcImg);
			});
			srcImg.src = window.URL.createObjectURL(uploadInput.files[0]);
		});
	}

	/**
	 * From an image, return a promise on the canvas with the (reduced) image.
	 *
	 * @param {Element} srcImg DOM Element
	 * @returns {Promise} Wraps {reduced: boolean, src: string, filename: string}.
	 */
	function createCanevas(srcImg) {
		const filename = uploadInput.files[0].name;
		const upForm = uploadInput.closest('form');
		const dstCanvas = upForm.querySelector('.upload-image-dest');
		if (srcImg.naturalWidth <= maxWidth && uploadInput.files[0].size < maxSize) {
			console.info("resizeImageOnUpload(): no resizing required.");
			dstCanvas.width = srcImg.naturalWidth;
			dstCanvas.height = srcImg.naturalHeight;
			dstCanvas.getContext('2d').drawImage(srcImg, 0, 0);
			srcImg.remove();
			return Promise.resolve({reduced: false, canvas: dstCanvas, filename: filename});
		}

		dstCanvas.width = maxWidth;
		dstCanvas.height = Math.round(srcImg.naturalHeight * maxWidth / srcImg.naturalWidth);
		console.info("resizeImageOnUpload(): resizing to ", dstCanvas.width, "x", dstCanvas.height);
		return window.pica()
			.resize(srcImg, dstCanvas, resizingOptions)
			.then(function(canvas){
				srcImg.remove();
				return {reduced: true, canvas: canvas, filename: filename};
			});
	}

	/**
	 * From the input[type=file] that uploads an image, return promise on the canvas.
	 *
	 * @returns {Promise} Wraps {reduced: boolean, canvas: DOMElement, filename: string}.
	 */
	function resizeImageToCanvas() {
		return createImage().then(createCanevas);
	}

	return {
		/**
		 * Return the src value (base64 encoded) after reducing the image.
		 *
		 * @returns {Promise} Wraps the src value (base64 encoded image)
		 */
		resizeImageToEncoded: function() {
			return createImage()
				.then(createCanevas)
				.then(function(result) {
					const encoded = result.canvas.toDataURL(mimeType, jpegQuality);
					result.canvas.remove();
					console.info("Image size after encoding: ", encoded.length);
					return encoded;
				});
		},

		/**
		 * Return a FormData without resizing.
		 *
		 * @returns {Promise} Wraps a FormData instance
		 */
		toFormData: function() {
			const form = uploadInput.closest('form');
			if (uploadInput.files[0].size > maxSize) {
				return Promise.reject(
					__("La taille maximale autorisée pour un fichier est {{size}} Mo.", {size : maxSizeMo} )
				);
			} else {
				return Promise.resolve(new FormData(form));
			}
		},

		/**
		 * Return a FormData where the field containing the blob of the (reduced) image.
		 *
		 * @returns {Promise} Wraps a FormData instance
		 */
		resizeImageToFormData: function() {
			const form = uploadInput.closest('form');
			const fieldName = uploadInput.name;
			if (!uploadInput.files || !uploadInput.files[0].type.startsWith('image/')) {
				console.info("No image uploaded, nothing to resize");
				return Promise.resolve(new FormData(form));
			}
			return resizeImageToCanvas().then(function(result) {
				const filename = result.filename.replace(/\.(jpe?g|png)$/i, '') + fileExtensions[mimeType];
				return new Promise(function(resolve) {
					result.canvas.toBlob(
						function(blob) {
							var formData = new FormData(form);
							formData.set(fieldName, blob, filename);
							result.canvas.remove();
							console.info("Image size before upload: ", blob.size);
							return resolve(formData);
						},
						mimeType,
						jpegQuality
					);
				});

			});
		},

		/**
		 * Uploads to the URL of form.action.
		 *
		 * @return {Promise} fetch() Promise
		 */
		resizeImageAndUpload: function() {
			const form = uploadInput.closest('form');
			return resizeImageToFormData()
				.then(function(uploadData) {
					return fetch(
						form.action,
						{
							method: "POST",
							body: uploadData,
							credentials: "include",
							cache: "no-cache"
						}
					);
				});
		}
	};
}
