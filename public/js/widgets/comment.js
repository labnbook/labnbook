// *********************************************************************************************
//                                    VIEW FUNCTIONS
// *********************************************************************************************
// LIST OF MESSAGES
function commentWidget() {
	return {
		scrollToLastComment() {
			this.$nextTick(() => {
				let parentDiv = document.getElementById('comment_wrapper').closest('.widget-wrapper');
				parentDiv.scrollTop = parentDiv.scrollHeight;
			});
		},
		displayComments(id_ld) {
			if(id_ld) {
				let self = this;
				this.commentLdView = '<i class="fa fa-spinner fa-pulse"></i>';
				$.ajax({
					type: "POST",
					url: "/comment/displayComments",
					data: {
						id_ld: parseInt(id_ld)
					},
					success: function(data){
						self.commentLdView = data;
						self.scrollToLastComment();
					},
					error: function (jqXHR, textStatus) {
						handleLDAjaxError(jqXHR, textStatus);
					}
				});
			} else {
				this.commentLdView = __('Aucun labdoc sélectionné');
			}
		},
		addComment() {
			if (window.global_scope === "test") {
				alert(__("Il n'est pas possible d'ajouter de commentaire en mode test"));
				return;
			}
			let self = this;
			let comment = this.currentComment.trim();
			if (comment === "") {
				return;
			}
			this.addCommentDisabled = true;
			comment = comment.replace(/</g, "&lt;");
			$.ajax({
				type: "POST",
				url: "/comment/addComment",
				data: {
					comment: comment,
					id_ld: Alpine.store('commentWidgetIdLd')
				},
				success: function (data) {
					self.commentLdView = data;
					self.currentComment = '';
					self.addCommentDisabled = false;
					self.scrollToLastComment();
				},
				error: function (jqXHR, textStatus) {
					handleLDAjaxError(jqXHR, textStatus);
					self.addCommentDisabled = false;
				}
			});
		},
		deleteComment(id_comment, id_ld) {
			if (confirm(__('Souhaitez-vous supprimer définitivement ce commentaire ?'))) {
				let self = this;
				$.ajax({
					type: "POST",
					url: "/comment/"+id_comment+"/delete",
					data: {
						id_ld: id_ld
					},
					success: function(data){
						self.commentLdView = data;
					}
				});
			}
		},
		commentLdView: __('Aucun labdoc sélectionné'),
		currentComment:'',
		addCommentDisabled:false
	}
}
