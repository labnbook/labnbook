/*
 // display the previous annotations corresponding to the selected colour (category)
 // the selector is a jQuery object =
 // - the parent tag of the annotation (if the user is changing the color of the annotation)
 // - or the editor itself (if the editor is opening)
 */
/* global Annotator */

function displayPreviousAnnotations(selector, id_user = null, id_report = null) {
	var target = $('.previous-annotations', selector);
	annotationsByCategory = target.data('bycategory');
	target.hide(0);
	target.html('');
	if (typeof annotationsByCategory !== 'undefined' && annotationsByCategory) {
		for (var cat = 0; cat < annotationsByCategory.length; cat++) {
			for (var i = 0; i < annotationsByCategory[cat].length && i < 12; i++) {
				if (annotationsByCategory[cat][i].visible) {
					var text 			= annotationsByCategory[cat][i].text;
					let annotationId 	= annotationsByCategory[cat][i].id;

					// Création du container de l'ancienne annotation
					let divPreviousAnnotationContainer = $('<div></div>');
					divPreviousAnnotationContainer.attr("class", "previous-annotation-container category-" + cat);

					// Création de l'ancienne annotation
					var divPreviousAnnotation = $('<div title="'+__('Ajouter ce texte à votre annotation')+'"></div>');
					divPreviousAnnotation.attr("data-text", text);
					divPreviousAnnotation.attr("data-id", annotationId);
					divPreviousAnnotation.attr("class", "previous-annotation category-" + cat);

					// On limit de le texte à afficher afin de mettre 3 lignes au maximum
					let textToShowInDiv;

					// Dans le cas où on a du texte sans espace.
					if(text.indexOf(" ") == -1){
						textToShowInDiv = text.length > 30 ? text.substring(0, 30)+'...' : text;
					}else{
						textToShowInDiv = text.length > 100 ? text.substring(0, 100)+'...' : text;
					}

					divPreviousAnnotation.append(textToShowInDiv);

					divPreviousAnnotationContainer.append(divPreviousAnnotation);

					// Ajout de l'icone pour faire redescendre une annotation dans l'historique
					divPreviousAnnotationContainer.append('<i class="previous-annotation-back-down fa fa-arrow-down" title="Redescendre dans la liste" aria-hidden="true"></i>');

					target.append(divPreviousAnnotationContainer);	
				}
			}
		}
		
		if(id_user != null && id_report != null){
			// Création de l'event click sur le bouton flèche du bas dans l'historique des annotations
			createEventPreviousAnnotationBackDown(id_user, id_report);
		}

	}
	target.show(0);

    // harmonize the selector based on the class "annotator-wrapper" that contains the editor
	if (selector.closest(".annotator-wrapper").length > 0) {
        selector = selector.closest(".annotator-wrapper");
    } else if (selector.find(".annotator-wrapper").length > 0) {
        selector = selector.find(".annotator-wrapper");
    }

    var category = $(selector).find('.annotator-category-selected').text();
    if (category == "" || typeof category == "undefined") {
        category = 0;
    }
    if (selector.find(".previous-annotations .category-" + category).length > 0) {
        selector.find(".previous-annotations").show();
        //On affiche uniquement les commentaires de la catégorie actuellement selectionné
        selector.find('.previous-annotations .previous-annotation-container').hide();
        selector.find(".previous-annotations .category-" + category).css("display","flex");
        //On ajoute la classe pour le fond des commentaire
        selector.find('.previous-annotations').attr('class', "previous-annotations");
        selector.find('.previous-annotations').addClass('annotator-category-' + category);
    } else {
        selector.find('.previous-annotations').hide();
    }
	return category;
}

// Permet de connaitre l'ID de la partie du rapport selectionné
let currentIdRp;

/**
 * Création de l'event click sur le bouton flèche du bas dans l'historique des annotations
 * 
 * @param {int/string} id_user 
 * @param {int/string} id_report 
 */
function createEventPreviousAnnotationBackDown(id_user, id_report){

	$('.previous-annotation-back-down').unbind('click');

	// Ajout du click sur le bouton
	$('.previous-annotation-back-down').on('click',function(e){

		e.stopPropagation();

		// Récupération de l'id de l'annotation
		let id_annotation = $(this).parent().find('.previous-annotation').attr('data-id');

		// On mets à 0 la popularité de l'annotation 
		updatePopularityPreviousAnnotation(id_annotation, 1)

		let currentHtmlPreviousAnnotationClicked = $(this).parent().get(0);

		// On la place à la fin de la liste
		$(this).parent().parent().append(currentHtmlPreviousAnnotationClicked);
		
	})
}

/**
 * Mise à jour de la popularité de l'annotation
 * @param {int/string} id_annotation 
 * @param {string} popularity 
 */

function updatePopularityPreviousAnnotation(id_annotation, reset = 0){
	$.ajax({// load old annotations made by the teacher for this rp
		type: "POST",
		url: "/annotation/"+id_annotation+"/updatePopularity",
		data: {
			reset:reset
		},
		error: function (msg) {
			alertGently(__("Erreur : impossible d'augmenter la popularité de l'annotation sélectionnée"));
		},
	});
}

/**
 * Init annotator for a report and display the annotations in DB
 */
function initAnnotator(scope, id_user, id_report) {
	var annotatorInstance;

	var annHistory = {
		filter: "",
		categories: [[], [], [], []]
	};

	var annotationsByLabdoc = {}; // { ldID1: [annotationID1, ...] }

	var annotator_color_options = {// Options of the color plugin
		// categories : yellow, green, blue, red
		category: ["0", "1", "2", "3"],
		// categories class (css)
		categoryColorClasses: {"0": "annotator-category-0", "1": "annotator-category-1", "2": "annotator-category-2", "3": "annotator-category-3"},
		categoryClass: "annotator-category",
		classForSelectedCategory: "annotator-category-selected",
		emptyCategory: "0",
		annotatorHighlight: 'span.annotator-hl',
		id_teacher:id_user,
		id_report:id_report
	};

	var readOnly = (scope !== 'follow');

	let initialCurrentUser = "";
	$.ajax({// load old annotations made by the teacher for this rp
		type: "POST",
		url: "/user/getInitials",
		data: {
			id_user: id_user
		},
		error: function (msg) {
			console.error("Impossible to load the initials of the teacher");
		},
		success: function (initial_user) {
			initialCurrentUser = initial_user.initial;
		}
	});

	$(function() {
		// on document ready

		if (typeof Annotator === 'undefined') {
			console.error('Annotator is not loaded');
			return;
		}
		/* @var Annotator Annotator */

		// éventuelle instance de Annotator
		if (readOnly) {
			setupAnnotatorRO();
			annotatorInstance = $("#lb_report").annotator({readOnly: true});
		} else {
			initEvents();
			setupAnnotatorRW();
			annotatorInstance = $("#lb_report").annotator();
			annotatorInstance.annotator('addPlugin', 'Categories', annotator_color_options);
			annotatorInstance.annotator('addPlugin', 'Header', {});
			
		}

		if (annotatorInstance) {
			annotatorInstance.annotator("addPlugin", "PluginLnB");
			loadAnnotationByAjax(id_user);

			if(!readOnly){
				// Appel de l'extension Resise ici car conflit avec PluginLnB
				annotatorInstance.annotator('addPlugin', 'Resize', {});
			}
		} else {
			logError("Erreur en chargeant le moteur d'annotation", 'danger');
		}
	});

	return {
		refreshLDsAnnotations: function(ids) {
			requestAnimationFrame(() =>
			requestAnimationFrame(function(){ // wait for the DOM being updated
				if (typeof Array.from !== 'undefined') {
					ids = Array.from(new Set(ids)); // unicity
				}
				for (var lid of ids) {
					// remove the possible highlights
					$("#labdoc_" + lid + " span.annotator-hl").replaceWith(function() {
						return $(this).html();
					});
					// skip unless there are annotations
					if (!annotationsByLabdoc[lid] || !annotationsByLabdoc[lid].length) {
						continue;
					}
					// add the annotations again
					for (var ann of annotationsByLabdoc[lid]) {
						// Copy the annotation to preserve annotationsByLabdoc from modifications done by annotator
						var ann = Object.assign({}, ann);
						annotatorInstance.annotator('setupAnnotation', ann);
						colorHighlights(ann);
					}
				}
			}));
		},
		refreshAllAnnotations: function() {
			$("span.annotator-hl").contents().unwrap();
			loadAnnotationByAjax(id_user);
		},
		hideAnnotatorAdder: function(){
			window.Annotator._instances[0].adder.detach();
		},
		showAnnotatorAdder: function(){
			window.Annotator._instances[0].adder.appendTo(window.Annotator._instances[0].wrapper);
			window.annotatorActions.refreshAllAnnotations();
		},
	};

	function initEvents() {
		$("body").on('click', '.previous-annotation', function (e) {
			// function for replacing or appending the previous annotation to the current one
			e.stopPropagation();
			var rawText = $.isNumeric($(this).data('text')) ? $(this).data('text').toString() : $(this).data('text');
			var selectedText = rawText ? rawText.replace(/&quot;/g, '"') : "";
			var textarea = $(this).closest(".annotator-editor").find('textarea.annotator-ed-textarea');
			var valueTextarea = textarea.val().trim();
			var newTextArea;
			if (selectedText.includes(valueTextarea)) {
				// if current text is included in the previous annotation, discard it
				newTextArea = selectedText;
			} else {
				// append the previous annotation to the current text
				newTextArea = valueTextarea + " " + selectedText;
			}
			textarea.val(newTextArea.trim());

			// Récupération de l'id de l'annotation			
			let id_annotation 		= $(this).data('id');

			updatePopularityPreviousAnnotation(id_annotation);
			
		});

		// automatic filtering of the annotations history
		$("body").on("input", "textarea.annotator-ed-textarea", function() {
			var content = $(this).val().trim();
			if (annHistory.filter === content) {
				return;
			}
			var changed = false;
			if (content === "") {
				// remove the history filter
				annHistory.filter = '';
				for (var i = 0; i <= 3; i++) {
					for (var j = 0; j < annHistory.categories[i].length; j++) {
						if (!annHistory.categories[i][j].visible) {
							annHistory.categories[i][j].visible = true;
							changed = true;
						}
					}
				}
			} else {
				// update the history filter
				annHistory.filter = content;
				for (var i = 0; i <= 3; i++) {
					for (var j = 0; j < annHistory.categories[i].length; j++) {
						var expected = annHistory.categories[i][j].text.includes(content);
						if (annHistory.categories[i][j].visible !== expected) {
							annHistory.categories[i][j].visible = expected;
							changed = true;
						}
					}
				}
			}
			if (changed) {
				var selector = $("#lb_report");
				$('.previous-annotations', selector).data('bycategory', annHistory.categories);
				displayPreviousAnnotations(selector, id_user, id_report);
				annotatorInstance.trigger('resizeAfterDisplayPreviousAnnotations');
			}
		});
	}

	function setupAnnotatorRO() {
		// Plugin for managing commentaries in a LabNBook report
		Annotator.Plugin.PluginLnB = function (element) {
			return {
				pluginInit: function () {
					this.annotator
						// An annotation was displayed
						.subscribe("annotationViewerTextField", function(field, annotation) {
							let initialTeacher = "";
							if( annotation.initial_teacher != undefined && annotation.initial_teacher != "" ){
								initialTeacher = ' ('+ annotation.initial_teacher + ')'
							}
							var content = Annotator.Util.escape(annotation.text || "") + initialTeacher;

							//var content = Annotator.Util.escape(annotation.text || "");
							$(field).html(content.replace(/\n/g, '<br />'));
						});

					/**
					 * @todo Check which scopes should be included.
					 *     We need a list of scopes with descriptions, somewhere in the source code
					 */
					if (window.global_scope === "default" || window.global_scope === "view") {
						var handlers = buildTracerAnnotationView();
						// maybe use "annotationViewerShown" or "annotationViewerTextField"?
						//annotatorInstance.annotator('subscribe', "annotationViewerShown", handlers.show);
						this.annotator.viewer.subscribe("show", handlers.show);
						this.annotator.viewer.subscribe("hide", handlers.hide);
					}
				}
			};
		};
		// End of the plugin
	}

	function setupAnnotatorRW() {
		// hack! override the default key process so that Enter doesn't submit the annotation
		Annotator.Editor.prototype.processKeypress = function (event) {
			if (event.key === "Escape") {
				event.stopPropagation();
				return this.hide(); // The Editor constructor will bind this
			}
		};

		// hack to interactively confirm deletion (patched Annotator code)
		Annotator.prototype.confirmDeleteAnnotation = function () {
			return $(".annotator-ed-textarea").val() === ""
				|| confirm(__("Souhaitez-vous supprimer définitivement cette annotation ?"));
		};

		// Plugin for managing commentaries in a LabNBook report
		Annotator.Plugin.PluginLnB = function (element) {
			return {
				pluginInit: function () {
					// Access the adder directly
					const adder = this.annotator.adder;
					if (adder) {
						// Hook into the `show` method of the adder
						const originalShow = adder.show.bind(adder);
						adder.show = (position, annotation) => {
							// Don't show adder when a labdoc diff is displayed
							const relatedLabdoc = this.annotator.selectedRanges[0].commonAncestor.closest('.labdoc');
							if (relatedLabdoc && relatedLabdoc.getElementsByClassName('display-diff').length>0) {
								alertGently(__("Il n'est pas possible d'annoter ce labdoc. Fermez l'affichage des modifications pour annoter."), "warning", 3000);
								return; // Prevent adder from being shown
							}
							// Call the original show method
							originalShow(position, annotation);
						};
					}
						
					this.annotator
						// An annotation was displayed
						.subscribe("annotationViewerTextField", function(field, annotation) {
							let initialTeacher = "";
							if( annotation.initial_teacher != undefined && annotation.initial_teacher != "" ){
								initialTeacher = ' ('+ annotation.initial_teacher + ')'
							}
							var content = Annotator.Util.escape(annotation.text || "") + initialTeacher;
							$(field).html(content.replace(/\n/g, '<br />'));

							// Hide/display edit and delete button
							if (typeof annotation.display_controls == "undefined" || annotation.display_controls) {
								$(field).closest("div.annotator-viewer").find(".annotator-controls").show();
							} else {
								$(field).closest("div.annotator-viewer").find(".annotator-controls").hide();
							}
						})

						// show the annotator editor
						.subscribe("annotationEditorShown", annotationEditorShown)

						// hide the annotator editor
						.subscribe("annotationEditorHidden", annotationEditorHidden)

						// Ajax request to store an annotation
						.subscribe("annotationCreated", annotationCreated)

						// Ajax request to Update an annotation
						.subscribe("annotationUpdated", annotationUpdated)
						
						// Ajax request to delete an annotation
						.subscribe("annotationDeleted", annotationDeleted);
				}
			};
		};
		// End of the plugin
	}

	function loadAnnotationByAjax(id_user) {
		// loads annotations from the DB and display them
		$.ajax({
			type: "POST",
			url: "/annotation/get",
			data: {
				id_report: id_report
			},
			success: function (annotations) {
				if (!annotations) {
					return;
				}
				for (var i = 0; i < annotations.length; i++) { // boucle sur les annotations
					try {
						var ann = JSON.parse(annotations[i].json);
					} catch (e) {
						logError("Problème de parsing d'une annotation", 'danger', [ id_report, annotations[i], e]);
						continue;
					}
					
					ann 				= prefixOldXpath(ann, annotations[i]);
					ann.id 				= annotations[i].id_annotation;   		// inclusion de l'id dans l'annotation pour son édition ou sa suppression
					ann.id_rp 			= annotations[i].id_report_part;  		// inclusion de l'id_rp dans l'annotation pour l'appel aux anciennes annotations
					ann.initial_teacher = annotations[i].initial; 		// inclusion des initiales de l'enseignant de l'annotation
					ann.display_controls = annotations[i].id_teacher == id_user;
					
					// add to list for later refreshing of a labdoc's content
					if (annotations[i].id_labdoc) {
						var id_labdoc = annotations[i].id_labdoc;
						if (annotationsByLabdoc[id_labdoc]) {
							annotationsByLabdoc[id_labdoc].push(ann);
						} else {
							annotationsByLabdoc[id_labdoc] = [ann];
						}
					}
					// Copy the annotation to preserve annotationsByLabdoc from modifications done by annotator
					var ann = Object.assign({}, ann);
					// affiche l'annotation
					
					try {
						annotatorInstance.annotator('setupAnnotation', ann);
					} catch (e) {
						logError("Problème d'affichage d'une annotation", 'danger', [annotations[i], e]);
					}
					colorHighlights(ann);
				}
			}
		});
	}

	function colorHighlights(annotation) {
		// gestion de la couleur de l'annotation
		var highlights = annotation.highlights;
		for (var j = 0; j < highlights.length; j++) {
			highlight = highlights[j];
			$(highlight).addClass(annotator_color_options.categoryColorClasses[annotation.category]);
		}
	}

	/**
	 **  Nettoie et stringify le texte avant un envoi Ajax
	 **/
	function stringifyAnnotation(annotation) { // modification of the annotation text to be compliant with the Ajax transfert
		var objectSeen = [];
		return JSON.stringify(annotation, function (key, val) { // stringify annotation
			if (key === 'highlights') {
				return undefined; // do not serialize this field
			}
			if (val !== null && typeof val === "object") {
				if (objectSeen.indexOf(val) >= 0) {
					return undefined;
				}
				objectSeen.push(val);
			}
			return val;
		});
	}

	/**
	 **  Get the informations of an annotation (type, id_ld, id_rp) from the informations of the adequate ancestor tag
	 **/
	function getAnnotationParams(annotation) {
		var element = annotation.highlights[0].parentElement;
		var type = '';
		// determination of the annotation type
		var id_ld = $(element).closest('.labdoc_content').attr('id');
		if (typeof id_ld !== 'undefined') {
			type = 'ld_content';
		} else {
			id_ld = $(element).closest('.labdoc_name').attr('id');
			if (typeof id_ld !== 'undefined') {
				type = 'ld_title';
			}
		}
		var id_rp = $(element).closest('.lb_report_part').attr('id');
		if (typeof id_rp !== 'undefined' && typeof id_ld === 'undefined') {
			type = 'rp_title';
		}
		// get the id_ld
		if (typeof id_ld !== 'undefined') {
			id_ld = id_ld.split("_")[2];
		} else {
			id_ld = null;
		}
		// get the id_rp
		if (typeof id_rp !== 'undefined') {
			id_rp = id_rp.split("_")[2];
		} else {
			id_rp = null;
		}

		return {id_ld: id_ld, id_rp: id_rp, type: type};
	}

	function annotationEditorShown(editor, annotation) {
		// Changes for the edition interface of an annotation by redefining the <li> items of the editor
		var ul_list = $(editor.element).find("form").find("ul");
		$(ul_list).find("span.annotator-resize").remove(); // get rid of the handler for textarea resize (disabled)
		//var items = $(ul_list).find("li");
		var list, textArea, colors;
		$($(ul_list).find("li")).each(function () {
			if ($(this).find("textarea").length == 1) {
				textArea = this;
			}
			// else if ($(items[i]).find("select").length == 1) { list = items[i]; }  // inutile ??
			else {
				colors = this;
			}
		});
		$(ul_list).empty();
		// items in the right order
		$(ul_list).append(colors);
		//$(ul_list).append(list);  // inutile ?
		$(ul_list).append(textArea);
		// class gestion
		$(ul_list).find("li").addClass("annotator-marge");
		$(ul_list).find("textarea").addClass("annotator-ed-textarea");

		// replace the controls at the bottom of the editor
		var controls = $(editor.element).find("form").find(".annotator-controls").remove();
		$(editor.element).find("form").append(controls);

		// display the previous annotations in the select
		var selector = $("#lb_report");
		var id_rp = annotation.id_rp; // edit an existing annotation
		if (typeof id_rp === 'undefined') { // new annotation
			id_rp = getAnnotationParams(annotation)['id_rp'];
		}

		// On met à jour l'Id partie rapport pour incrémenter la popularité des annotations historiques
		currentIdRp = id_rp;
		
		var initial_text = annotation.text; //Le texte de l'annotation qui existait deja
		selector.find('.previous-annotations').html("");
		selector.find("textarea.annotator-ed-textarea").focus();

		$.ajax({// load old annotations made by the teacher for this rp
			type: "POST",
			url: "/annotation/get",
			data: {
				id_teacher: id_user,
				id_report_part: id_rp
			},
			error: function (msg) {
				alertGently(__("Erreur : impossible de charger les anciennes annotations"));
			},
			success: function (annotations) {
				annHistory.categories = [[], [], [], []];
				if (annotations.length > 0) { //on boucle sur les annotation pour les afficher en fonction de la catégorie
					var previousByCategory = [new Set, new Set, new Set, new Set];
					for (var i = 0; i < annotations.length; i++) {
						try {
							var ann = JSON.parse(annotations[i].json);
							let annId 	= annotations[i].id_annotation;
							var annText = ann.text.trim();
							if (initial_text !== annText) { //On n'affiche pas si c'est la même annotation en cours
								if (!previousByCategory[ann.category].has(annText)) {
									previousByCategory[ann.category].add(annText);
									annHistory.categories[ann.category].push({id:annId,  text: annText, visible: true });
								}
							}
						} catch (e) {
							console.error("Problème de parsing de l'annotation n°" + i + " : ", annotations[i].id_annotation, e);
						}
					}
					$('.previous-annotations', selector).data('bycategory', annHistory.categories);
					displayPreviousAnnotations(selector, id_user, id_report); // display the previous annotations corresponding to the selected colour
					annotatorInstance.trigger('resizeAfterDisplayPreviousAnnotations');
				} else {
					selector.find('.previous-annotations').hide();
				}
			}
		});
	}

	function annotationEditorHidden(editor) { // On re-affiche les blocs commentaire (comportement de base)
		$('.previous-annotations').show();
	}

	/**
	 * Convert an xpath string so that it starts at the closest tag with an "id" attribute.
	 *
	 * @param {string} xp
	 * @return {string}
	 */
	function rebaseXpathOnAncestor(xp) {
		var annotationsRoot = document.querySelector("#lb_report > .annotator-wrapper");
		var target = Annotator.Range.nodeFromXPath(xp, annotationsRoot);
		if (target === null) {
			// the node is missing, probably because the content was deleted
			return xp;
		}
		var closestAnchor = target.closest("[id]");
		if (closestAnchor === null) {
			return xp;
		}
		var pathFromAnchor = Annotator.Util.xpathFromNode($(target), closestAnchor);
		pathFromAnchor.unshift("//" + closestAnchor.tagName.toLowerCase() + "[@id='" + closestAnchor.getAttribute('id') + "']");
		return pathFromAnchor.join('');
	}

	function convertAnnotationXpath(annotation) {
		// convert the xpath range so that it starts from the closest tag with an "id" attribute,
		// instead of starting at the root element of Annotator
		for (var i = 0; i < annotation.ranges.length; i++) {
			annotation.ranges[i].start = rebaseXpathOnAncestor(annotation.ranges[i].start);
			annotation.ranges[i].end = rebaseXpathOnAncestor(annotation.ranges[i].end);
		}
	}

	function annotationCreated(annotation) {
		if (!annotation.highlights[0] || !id_report) {
			return;
		}

		convertAnnotationXpath(annotation);
		var params = getAnnotationParams(annotation); // get the parameters useful for storing the annotation (id_ld, id_rp, type)
		$.ajax({
			type: "POST",
			url: "/annotation/store",
			data: {
				id_teacher: id_user,
				id_report: id_report,
				id_report_part: params.id_rp,
				id_labdoc: params.id_ld,
				json: stringifyAnnotation(annotation),
				type: params.type
			},
			success: function(newId) {
				if (newId > 0) {
					annotation.id = newId; // inclusion de l'id dans l'annotation pour son édition ou sa suppression
					annotation.id_rp = params.id_rp; // inclusion de l'id_rp dans l'annotation pour l'appel aux anciennes annotations
				} else {
					logError("Inserting this annotation failed:", "danger", [id_report, params]);
				}
			}
		});
		annotation.initial_teacher = initialCurrentUser
	}

	function annotationUpdated(annotation) {
		if (!annotation.id) {
			return;
		}
		convertAnnotationXpath(annotation);
		$.ajax({
			type: "POST",
			url: "/annotation/"+annotation.id+"/update",
			data: {
				json: stringifyAnnotation(annotation)
			}
		});
	}

	function annotationDeleted(annotation) {
		if (!annotation.id) {
			return; // no ID, so not in the DB
		}
		$.ajax({
			type: "POST",
			url: "/annotation/"+annotation.id+"/delete",
		});
	}

	function buildTracerAnnotationView() {
		"use strict";
		const min_duration = 500; // (ms) Ignored if smaller
		const max_duration = 30000; // (ms) Truncated if greater

		var last_annotation_id = null;
		var last_annotation_theoricDuration = 0;
		var last_annotation_start = null;
		var monitor = null;

		function saveTrace(annotationId, duration) {
			if (annotationId <= 0 || duration <= 0) {
				console.error("Invalid annotation trace: ", annotationId, duration);
				return;
			}
			//console.log("Tracing annotation view : " + annotationId + " for " + duration + " ms.");
			$.ajax({
				type: "POST",
				url: "/annotation/"+annotationId+"/trace",
				data: {
					duration: duration
				}
			});
		}

		function resetMonitoring() {
			if (monitor !== null) {
				clearTimeout(monitor);
				monitor = null;
			}
			last_annotation_id = null;
			last_annotation_start = null;
			last_annotation_theoricDuration = 0;
		}

		function startMonitoring(annotations) {
			if (!annotations || annotations.length !== 1) {
				return;
			}
			var annotation = annotations[0];
			if (monitor !== null) {
				console.warn("Warning, opening an annotation while another one is active ", annotation);
				stopMonitoring();
			}
			// Ne pas envoyer de requete AJAX si l'annotation a été tracée lors du précédent appel AJAX traceAnnotation
			if (annotation.id === last_annotation_id) {
				return;
			}
			//console.log("Showing annotation " + annotation.id);
			last_annotation_id = annotation.id;
			last_annotation_start = new Date();
			last_annotation_theoricDuration = getTheoricTimeOfReading(annotation.text);
			monitor = setTimeout(stopMonitoring, max_duration);
		}

		function stopMonitoring() {
			if (monitor === null) {
				return;
			}
			var duration = (new Date) - last_annotation_start;
            // console.log("durée de lecture", duration, "pour un minimum calculé de ", last_annotation_theoricDuration);
			if (duration < last_annotation_theoricDuration) {
				//console.log("Aborting view of annotation " + last_annotation_id);
				resetMonitoring();
				return;
			}
			if (duration > max_duration) {
				duration = max_duration;
			}
			//console.log("Hiding annotation " + last_annotation_id + " after " + duration + " ms.");
			saveTrace(last_annotation_id, duration);
			resetMonitoring();
		}

		return {show: startMonitoring, hide: stopMonitoring};
	}

	function prefixOldXpath(a, annotation) {
		if (a.ranges[0].start.startsWith('//')) {
			return a; // recent XPath, nothing to change
		}
		var prefix = '';
		switch (annotation.type) {
			case 'ld_content':
				prefix = "//div[@id='labdoc_content_" + annotation.id_labdoc + "']";
				break;
			case 'ld_title':
				prefix = "//span[@id='ld_name_" + annotation.id_labdoc + "']";
				break;
			case 'rp_title':
				prefix = "//div[@id='part_title_actions_" + annotation.id_report_part + "']";
				break;
		}
		for (var i = 0; i < a.ranges.length; i++) {
			a.ranges[i].start = prefix + a.ranges[i].start;
			a.ranges[i].end = prefix + a.ranges[i].end;
		}
		return a;
	}
	
	function countWordInAnnotationText(annotationText) {
		var matches = annotationText.match(/[\w\d\’\'-]+/gi);
		return matches ? matches.length : 0;
	}
	
	// Get theoric time of reading in ms
	function getTheoricTimeOfReading(annotationText) {
		// Theoric reading speed (very high value) : 600 words/min => 10 words/sec 
		const theoricSpeedOfReading = 10;
		return (0.1 + countWordInAnnotationText(annotationText)/theoricSpeedOfReading)*1000;
	} 
}
