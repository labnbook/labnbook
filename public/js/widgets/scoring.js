// Widget
const DESCROBS_TYPE = {
	OBSERVABLE: 'ob',
	DESCRIPTOR: 'descr'
}
// Controller for scoring assesment
window.assessmentCtrl = function () {
	let assessment = {};
	let deletes = [];
	let isDirty = false;
	const MAX_DESCROB_NUMBER = 100;
	let studentViewConfig = {
		tablePercentWidth: 100,
		criterionTitlePercentWidth: 14,
		criterionScorePercentWidth: 12,
	}
	let correctorViewConfig = {
		tablePercentWidth: 100,
		criterionTitlePercentWidth: 16,
		criterionScorePercentWidth: 12,
	}

	/* --------------------------*/
	/* Score computing functions */
	/* --------------------------*/
	
	/**
	 * Computes the sum of the descrobs values
	 */
	function _get_descrobs_sum (descrobs) {
		return descrobs.reduce(
			(acc, cur) => acc + parseFloat(cur.value),
			0
		);
	}

	/**
	 * Computes the max of the descrobs values
	 */
	function _get_descrobs_max (descrobs) {
		return descrobs.reduce(
			(acc, cur) => Math.max(acc, parseFloat(cur.value)),
			0
		);
	}

	/**
	 * This function computes the field computed_relative_weight for each criteria
	 * and criteria_group
	 */
	function _computeAllWeights () {
		// Compute criteria weight
		assessment.criteria.forEach(function (c) {
			if (!isActive(c)) {
				// neutralized
				c.computed_relative_weight = 0;
			} else if (c.relative_weight !== null) {
				// teacher defined
				c.computed_relative_weight = c.relative_weight;
			} else if (c.type === DESCROBS_TYPE.OBSERVABLE) {
				// Observable sum values
				c.computed_relative_weight = _get_descrobs_sum(c.descrobs);
			} else {
				// descriptor max of values
				c.computed_relative_weight = _get_descrobs_max(c.descrobs);
			}
		});
		// Compute group weights
		assessment.criteria_group.forEach(function (g) {
			const inner_computed_weight_sum = getCriteriaFromGroup(g).reduce(
				(acc, cur) => acc + parseFloat(cur.computed_relative_weight),
				0
			);
			if (inner_computed_weight_sum === 0) {
				// All criteria with non null weight are neutralized
				// this group weights nothing
				// This may replace the teacher choice
				g.computed_relative_weight = 0;
			} else if (g.relative_weight === null) {
				// Teacher has not defined the group weight, use sum of
				// computed criteria weights
				g.computed_relative_weight = inner_computed_weight_sum;
			} else {
				// Keep the teacher choice
				g.computed_relative_weight = g.relative_weight;
			}
		});
	}

	/**
	 * Requirements all computed_relative_weight on criteria and groups should
	 * be recomputed, i.e call _computeAllWeights before this function
	 * This functions computes the relative score of each criteria as a percentage
	 * of the criteria max possible value this is called \tilde{S_{ci}} in #999
	 */
	function _computeCriteriaRelativeScores () {
		assessment.criteria.forEach(function (c) {
			if (c.computed_relative_weight === 0) {
				// No weight, no score
				c.computed_relative_score = 0;
			} else {
				const selected_descrobs = c.descrobs.filter(d => d.selected === true);
				const selected_descrob_sum = _get_descrobs_sum(selected_descrobs);
				const num_selected = selected_descrobs.length;
				const descrob_sum = _get_descrobs_sum(c.descrobs);
				if (num_selected === 0 || descrob_sum === 0) {
					// No value, no score
					c.computed_relative_score = 0;
				} else if (c.type === DESCROBS_TYPE.OBSERVABLE) {
					c.computed_relative_score = selected_descrob_sum / descrob_sum;
				} else {
					// max cannot be 0 as sum is not
					const descrob_max = _get_descrobs_max(c.descrobs);
					c.computed_relative_score = selected_descrob_sum / (descrob_max * num_selected)
				}
			}
		});
	}

	/**
	 * Requirements computed_relative_score must have been computed first on
	 * all criteria i.e call _computeCriteriaRelativeScores first
	 * This function computes the maximum reachable score of each criteria
	 * i.e M_{ci} in #999
	 */
	function _computeCriteriaMaxAndActualScores () {
		const sum_groups_weights = assessment.criteria_group.reduce(
			(acc, cur) => acc + parseFloat(cur.computed_relative_weight),
			0
		);
		assessment.criteria.forEach(function (c) {
			const my_group_weight = assessment.criteria_group.find(
				e => e.id_criteria_group === c.id_criteria_group
			).computed_relative_weight;

			// If a criterion is neutralized, it's computed_relative_weight is 0
			const sum_criteria_weight_of_my_group = assessment.criteria.filter(
				e => e.id_criteria_group === c.id_criteria_group).reduce(
				(acc, cur) => acc + cur.computed_relative_weight,
				0
			);

			if (c.computed_relative_weight === 0 ||
				my_group_weight === 0 ||
				sum_criteria_weight_of_my_group === 0 ||
				sum_groups_weights === 0
			) {
				// No score
				c.computed_maximum_reachable_score = 0;
			} else {
				c.computed_maximum_reachable_score = my_group_weight / sum_groups_weights * c.computed_relative_weight / sum_criteria_weight_of_my_group * assessment.grading;
			}
			c.computed_score = c.computed_relative_score * c.computed_maximum_reachable_score;
		});
	}

	/**
	 * Parse float, return 0 instead of None
	 */
	function _safeParseFloat (num) {
		const value = parseFloat(num);
		if (isNaN(value)) {
			return 0;
		}
		return value;
	}

	function _getCapedScoreWithBonus (score, bonus) {
		 return Math.max(
			 Math.min(
				 score + bonus,
				 assessment.max_grade
			 ),
			 assessment.min_grade
		 );
	}
	/**
	 * Requirement : computed_score must have been computed on each criteria
	 * This function computes the global score
	 */
	function _computeGlobalScore () {
		// compute criteria_group computed_score
		assessment.criteria_group.forEach(function (g) {
			g.computed_maximum_reachable_score = getCriteriaFromGroup(g).reduce(
				(acc, cur) => acc + cur.computed_maximum_reachable_score,
				0
			);
			g.computed_score = getCriteriaFromGroup(g).reduce(
				(acc, cur) => acc + cur.computed_score,
				0
			);
		});
		// Compute total score
		assessment.score = assessment.criteria_group.reduce(
			(acc, cur) => acc + cur.computed_score,
			0
		);
		assessment.score = _getCapedScoreWithBonus(assessment.score, assessment.bonus);
	}

	/**
	 * Fetch assessment from backend
	 * @param id_production
	 * @param is_view_as_teacher
	 * @returns {Promise<null|any>}
	 */
	async function get (id_production, is_view_as_teacher=null) {
		if(is_view_as_teacher) {
			this.is_view_as_teacher = is_view_as_teacher===1;
		}
		const options = {
			headers: {
				'X-Requested-With': 'XMLHttpRequest'
			},
		};
		const query = '?view_as_student=' + (this.is_view_as_teacher ? 0 : 1);
		const response = await fetch('/scoring/assessment/' + id_production + query, options);
		if (response.status === 200) {
			const data = await response.json();
			_castBooleans(data);
			data.editable = !data.published;
			return data;
		}
		if ([403, 404].includes(response.status)) {
			return null;
		}
	}

	/**
	 * Iterate over data and cast fields that should be boolean
	 * @param {assessment} data
	 */
	function _castBooleans (data) {
		data.criteria.forEach(function (c) {
			c.descrobs.forEach(function (d) {
				d.selected = Boolean(d.selected);
			});
		});
	}

	/**
	 * Controller for score computing (issue #999)
	 */
	function computeAssessmentScore () {
		// 1. Compute weights
		_computeAllWeights();
		// 2. compute criteria relatives scores
		_computeCriteriaRelativeScores();
		// 3. Compute criteria max scores and scores
		_computeCriteriaMaxAndActualScores();
		// 4. Dispatch and compute global score
		_computeGlobalScore();
	}

	/**
	 * Save the edited assessment to the backend
	 * @param {boolean} warn : whether to warn or not for not corrected descriptors
	 * @returns {Promise<*>}
	 */
	async function save (warn=true) {
		if (warn && !_warnMissingDescriptors()) {
			await new Promise(resolve => setTimeout(resolve, 100));
			return;
		}
		const response = await fetch('/scoring/assessment/' + assessment.id_production, {
			method: 'POST',
			headers: {
				'X-CSRF-TOKEN': document.querySelector('meta[name=csrf-token]').content,
				'Content-Type': 'application/json',
				Accept: 'application/json'
			},
			body: JSON.stringify({
				assessment,
				deletes
			})
		});
		const data = await response.json();
		if (response.ok) {
			_do_reload(data.assessment);
		}
		return data.message;
	}

	/**
	 * Notify the corrector if a descriptor is not corrected
	 * @returns {boolean}
	 * @private
	 */
	function _warnMissingDescriptors() {
		const activeDescriptorCriteria = assessment.criteria.filter(c => c.type === 'descr' && isActive(c));
		if (activeDescriptorCriteria.length > 0) {
			const missingDesciptorChoice = activeDescriptorCriteria
				.map(c => c.descrobs.reduce((acc, cur) => acc && !cur.selected, true))
				.filter(Boolean).length;
			if (missingDesciptorChoice > 0) {
				if (!confirm(
					__('Attention : {{missingDesciptorChoice}} critères avec descripteurs ne sont pas évalués. Souhaitez vous rendre l\'évaluation malgré tout ?',
						{missingDesciptorChoice: missingDesciptorChoice}))
				) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Switch the state of an assessment (editable <-> non-editable)
	 * @param {boolean} warn : whether to warn or not for not corrected descriptors
	 * @returns {Promise<string|*>}
	 */
	async function toggleEditable (warn=false) {
		isDirty = true;
		if (!assessment.published && !confirm(__('Les étudiants  pourront voir leur évaluation une fois publiée'))) {
			await new Promise(resolve => setTimeout(resolve, 100));
			return;
		}
		if (warn && !_warnMissingDescriptors()) {
			await new Promise(resolve => setTimeout(resolve, 100));
			return;
		}
		if (!assessment.editable && assessment.published) {
			fetch('/scoring/assessment/' + assessment.id_production + '/traceEdit', {
				method: 'POST',
				headers: {
					'X-CSRF-TOKEN': document.querySelector('meta[name=csrf-token]').content
				}
			});
		}
		assessment.editable = !assessment.editable;
		if (!assessment.published) {
			assessment.published = true;
			return save(false, false);
		}
		// Wait for synchronous tasks to end before returning true to avoid alpinejs not seeing wait is done
		await new Promise(resolve => setTimeout(resolve, 100));
	}

	/**
	 * Re-init an assessment to match the original grid
	 * @returns {Promise<void>}
	 */
	async function reinit () {
		if (!confirm(__('Réinitialiser l\'évaluation va entraîner la perte de l\'évualuation'))) {
			// Wait for synchronous tasks to end before returning true to avoid alpinejs not seeing wait is done
			await new Promise(resolve => setTimeout(resolve, 100));
			return;
		}
		await _deleteAssessment();
		const response = await get(assessment.id_production, 1);
		return _do_reload(response);
	}

	/**
	 * Returns all the crieria of a given group
	 * @param group
	 * @returns {T[]|*[]}
	 */
	function getCriteriaFromGroup (group) {
		if (group === undefined) {
			return [];
		}
		return assessment.criteria.filter(e => e.id_criteria_group === group.id_criteria_group);
	}
	

	/**
	 * Select two descriptors
	 * @param criterion
	 * @param descriptor
	 * @returns {Promise<[]|*>}
	 */
	async function selectAdjacentDescr (criterion, descriptor) {
		const list = criterion.descrobs;
		isDirty = true;
		if (assessment.editable === false || !isActive(criterion)) {
			return criterion.descrobs;
		}
		let found = false;
		const to_select = [];
		for (const i in list) {
			if (list[i].id_descrob === descriptor.id_descrob) {
				found = true;
				to_select.push(list[i]);
			} else if (found) {
				found = false;
				to_select.push(list[i]);
			}
		}
		_unSelectDescrobs(list);
		to_select.forEach(e => _selectSingleDescrob(e));
		computeAssessmentScore();
		// Wait for synchronous tasks to end before returning true to avoid alpinejs not seeing wait is done
		await new Promise(resolve => setTimeout(resolve, 100));
		return criterion.descrobs;
	}

	/**
	 * Create a group for non grouped criteria
	 * @private
	 */
	function _addEmptyGroup () {
		if (assessment.criteria.find(e => e.id_criteria_group === null) !== undefined &&
			assessment.criteria_group.find(e => e.id_criteria_group === null) === undefined
		) {
			assessment.criteria_group.push({
				id_criteria_group: null,
				description: __('Critères non groupés'),
				relative_weight: assessment.criteria.filter(e => e.id_criteria_group === null).map(e => e.relative_weight).reduce((a, b) => a + b)
			});
		}
	}

	/**
	 * Alpinejs creates a proxy from the object returned by get in x-init
	 * we need to store the proxy instead of the object returned by get
	 * so that when we modify it it triggers alpine's callbacks
	 * @param {Proxy} proxy - the proxy object containning the assessment
	 */
	function setAssessmentProxy (proxy) {
		assessment = proxy;
		if (assessment === null) {
			return;
		}
		_addEmptyGroup();
		computeAssessmentScore();
	}

	/**
	 * Marks a single descrob as selected
	 * @param {descrob} descrob
	 */
	function _selectSingleDescrob (descrob) {
		descrob.selected = true;
	}

	/**
	 * unselected all descrob in a list
	 * @param list
	 */
	function _unSelectDescrobs (list) {
		list.forEach(function (descrob) {
			descrob.selected = false;
		});
	}

	/**
	 * Reload the data to be used by the assessment blades
	 * @param data
	 * @private
	 */
	function _do_reload (data) {
		_castBooleans(data);
		deletes = [];
		assessment.id_assessment = data.id_assessment;
		assessment.id_rubric_origin = data.id_rubric_origin;
		assessment.id_production = data.id_production;
		assessment.bonus = data.bonus;
		assessment.bonus_comment = data.bonus_comment;
		assessment.score = data.score;
		assessment.score_comment = data.score_comment;
		assessment.published = data.published;
		assessment.editable = !assessment.published;

		assessment.criteria.splice(0, assessment.criteria.length);
		data.criteria.forEach(function (e) { assessment.criteria.push(e); });

		assessment.criteria_group.splice(0, assessment.criteria_group.length);
		data.criteria_group.forEach(function (e) { assessment.criteria_group.push(e); });

		assessment.assessed_students.splice(0, assessment.assessed_students.length);
		data.assessed_students.forEach(function (e) { assessment.assessed_students.push(e); });

		assessment.grading = data.grading;
		assessment.min_grade = data.min_grade;
		assessment.max_grade = data.max_grade;
		assessment.min_bonus = data.min_bonus;
		assessment.max_bonus = data.max_bonus;
		_addEmptyGroup();
		computeAssessmentScore();
		isDirty = false;
	}

	/**
	 * Delete an assessment
	 * @returns {Promise<Response<any, Record<string, any>, number>>}
	 * @private
	 */
	async function _deleteAssessment () {
		return await fetch('/scoring/assessment/' + assessment.id_production, {
			method: 'DELETE',
			headers: {
				'X-CSRF-TOKEN': document.querySelector('meta[name=csrf-token]').content,
				'Content-Type': 'application/json',
				Accept: 'application/json'
			}
		});
	}

	/**
	 * Returns the view configuration
	 * @param who ('student' or 'corrector')
	 * @param assessment
	 * @returns {{tablePercentWidth: number, criterionScorePercentWidth: number, criterionTitlePercentWidth: number}}
	 */
	function getViewConfig(who, assessment) {
		if (assessment === null) {
			return;
		}
		const gcd = (a, b) => a ? gcd(b % a, a) : b;
		const lcm = (a, b) => a * b / gcd(a, b);
		const descrobsLCM = assessment.criteria.map((elt) => elt.descrobs.length).reduce(lcm);
		
		let objectConfig = {};
		if (who === 'corrector') {
			objectConfig = correctorViewConfig;
		} else if (who === 'student') {
			objectConfig = studentViewConfig;
		}

		objectConfig.columnsNumber = descrobsLCM + 2;
		objectConfig.descrobColumnPercentWidth = (
			objectConfig.tablePercentWidth - objectConfig.criterionTitlePercentWidth - objectConfig.criterionScorePercentWidth
			) / (objectConfig.columnsNumber - 2);
		return objectConfig;
	}

	/**
	 * Check if group is active
	 * @param group
	 * @returns {boolean}
	 */
	function isGroupActive(group) {
		return getCriteriaFromGroup(group).filter(c => isActive(c)).length > 0
	}

	/**
	 * Is the given criterion active ?
	 * @pram {criterion} criterion
	 */
	function isActive (criterion) {
		return criterion.activated;
	}

	/**
	 * Check if a variable is null, undefined or empty
	 * @param value
	 * @returns {boolean}
	 */
	function isEmpty (value) {
		if (typeof value == 'number') {
			return false;
		} else {
			return (value == null || value.trim().length === 0);
		}
	}

	/**
	 * Modify the criterion status (active <-> not active)
	 * @param criterion
	 */
	function toggleActive (criterion) {
		isDirty = true;
		if (assessment.editable === false) {
			return;
		}
		criterion.activated = !criterion.activated;
		computeAssessmentScore();
	}

	/**
	 * Format a decimal, rounding to the indicated number of digits
	 * @param number
	 * @param digits
	 * @returns {string}
	 */
	function _format(number, digits=2) {
		if (typeof number === 'undefined' || isNaN(number) || number === null) {
			return '';
		}
		return number.toLocaleString(window.langLong().split('_')[0], { maximumFractionDigits: digits });
	}

	/**
	 * Display the score a / b
	 * @param score
	 * @param maximum_reachable_score
	 * @param digits
	 * @returns {string}
	 */
	function displayScore(score, maximum_reachable_score, digits) {
		if (maximum_reachable_score === 0) {
			return __('Non\xa0noté');
		} else {
			return _format(score, digits)
				+ '\xa0/\xa0'
				+ _format(maximum_reachable_score, digits);
		}
	}

	/**
	 * Display the weight of a descrob, criterion or group
	 * @param {number} weight
	 * @param {number} computedWeight
	 * @param {number} digits
	 * @returns {string}
	 */
	function displayWeight(weight, computedWeight, digits=2) {
		if (weight === undefined || weight == null) {
			return _format(computedWeight, digits) + ' (auto)';
		} else {
			return _format(weight, digits);
		}
	}

	/**
	 * Validate bonus score
	 * @param bonusScore
	 * @returns {null|*|null}
	 * @private
	 */
	function _validateBonusScore(bonusScore) {
		if (bonusScore == null) {
			assessment.bonus = null;
			return null; 
		}
		const bonusValue = parseFloat(bonusScore.toString().replace(',', '.').replace('+', ''));
		if (assessment.bonus == null || isNaN(bonusValue)) {
			assessment.bonus = null;
			return null;
		}
		if (bonusValue > assessment.max_bonus || bonusValue < assessment.min_bonus) {
			assessment.bonus = Math.max(Math.min(bonusValue, assessment.max_bonus), assessment.min_bonus);
			alert(__("Le bonus doit être compris entre {{min_bonus}} et {{max_bonus}}.\nLa valeur que vous avez renseignée est remplacée par {{bonus}}.", 
				{min_bonus: assessment.min_bonus, max_bonus: assessment.max_bonus, bonus: assessment.bonus}));
		} else {
			assessment.bonus = Math.round(100 * bonusValue) / 100;
		}
		return assessment.bonus;
	}

	/**
	 * Update bonus score and assessment scores
	 * @param bonusScore
	 * @returns {boolean}
	 */
	function updateBonusScore(bonusScore) {
		_validateBonusScore(bonusScore);
		computeAssessmentScore();
		return assessment.bonus == null;
	}

	/**
	 * Add a + to the bonus score when it's positive
	 * @param bonusScore
	 * @returns {string|null|*}
	 */
	function displayBonusScore(bonusScore) {
		if (bonusScore != null) {
			bonusScore = parseFloat(bonusScore.toString());
			if (!isNaN(bonusScore) && bonusScore > 0) {
				return '+' + _format(bonusScore);
			} else {
				return _format(bonusScore);
			}
		} else {
			bonusScore = '';
		}
		return bonusScore;
	}

	/**
	 * Print an individual bonus
	 * @param bonusScore
	 * @returns {string|null|*}
	 */
	function displayIndividualBonusScore(bonusScore, mode='both') {
		let bonus = parseFloat(bonusScore);
		bonus = isNaN(bonus) ? 0 : bonus;
		const score = _getCapedScoreWithBonus(assessment.score, bonus);
		const bonusDisplay = displayBonusScore(bonusScore);
		const scoreDisplay = displayScore(score, assessment.grading, 1);
		if (mode === 'bonus') {
				return bonusDisplay;
		}
		if (mode === 'score') {
				return scoreDisplay;
		}
		return bonusDisplay + '<br />(' + scoreDisplay + ')';
	}

	/**
	 * Check if the missing student label has to be shown
	 * @param assessment
	 * @returns {*|boolean}
	 */
	function isStudentMissingLabelShown(assessment) {
		return assessment.assessed_students 
			&& (assessment.assessed_students.filter(student => student.missing).length > 0)
	}

	/**
	 * Main function to select descrobs
	 * @param criterion
	 * @param descrob
	 * @returns {Promise<*>}
	 */
	async function selectOrToggleDescrob(criterion, descrob) {
		if (isActive(criterion) && assessment.editable ) {
			isDirty = true;
			if (criterion.type === 'descr') {
				if ((descrob.selected && criterion.descrobs.filter(d => d.selected).length > 1) || !descrob.selected) {
					_unSelectDescrobs(criterion.descrobs);
					_selectSingleDescrob(descrob);
				} else {
					_unSelectDescrobs([descrob]);
				}
			} else {
				if (descrob.selected) {
					_unSelectDescrobs([descrob]);
				} else {
					_selectSingleDescrob(descrob);
				}
			}
			computeAssessmentScore();
			// Wait for synchronous tasks to end before returning true to avoid alpinejs not seeing wait is done
			await new Promise(resolve => setTimeout(resolve, 100));
			return descrob.selected
		}
		return descrob.selected;
	}

	function assessmentShown (who) {
		if (who === 'student' && assessment !== null) {
			fetch('/scoring/assessment/' + assessment.id_production + '/traceView', {
				method: 'POST',
				headers: {
					'X-CSRF-TOKEN': document.querySelector('meta[name=csrf-token]').content,
					'Content-Type': 'application/json',
					Accept: 'application/json'
				},
				body: JSON.stringify({
					who
				})
			});
		}
	}

	async function addIndividualBonus (id_assessed_student) {
		if (id_assessed_student != 0) {
			const student = assessment.assessed_students.find(s => s.id_assessed_student == id_assessed_student);
			student.bonus_comment = "";
		}
	}

	function getIndividualBonus (id_student) {
		return assessment.assessed_students.find(s => !s.missing && ( s.bonus !== 0 || s.bonus_comment !== null) && s.id_student === id_student);
	}

	function getIndividualScore (id_student, is_corrector) {
		let individual_bonus = 0;
		if (!is_corrector) {
			const student = getIndividualBonus(id_student);
			if (student) {
				individual_bonus = student.bonus;
			}
		}
		return assessment.score + individual_bonus;
	}

	return {
		// Call this function when something changed (settings or students)
		init: get,
		update: computeAssessmentScore,
		save,
		reinit,
		getCriteriaFromGroup,
		selectAdjacentDescr,
		toggleEditable,
		setAssessmentProxy,
		displayScore,
		displayBonusScore,
		displayIndividualBonusScore,
		displayWeight,
		updateBonusScore,
		isActive,
		isEmpty,
		toggleActive,
		getViewConfig,
		isGroupActive,
		isStudentMissingLabelShown,
		selectOrToggleDescrob,
		addIndividualBonus,
		getIndividualBonus,
		getIndividualScore,
		assessmentShown
	}
}();
