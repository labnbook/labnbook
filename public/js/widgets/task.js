// Widget

/***************************
 Controller for the tasks list
 ***************************/
window.tasksCtrl = function () {
	this.initialized = false; // Flag to know if the tasks list has been initialized
	this.tasks = []; // List of tasks
	this.report_members = []; // List of report members that can be added to a task
	this.hasChanged = false; // Flag to know if the tasks list has been modified
	this.inDragg = false; // Flag to know if a task is being dragged
	this.draggedItemId = null; // Id of the task being dragged
	this.newTask = {}; // New task to be created
	this.selectedReports = []; // Reports for which we want to show tasks
	this.reports = [];
	this.sortByCriteria = null;
	
	/** **************************/
	/** Drag and drop functions **/
	/** **************************/

	function onDragStart(evt) {
		this.inDragg = true;
		$(evt.item).addClass('dragging');
	}

	function onDragEnd(evt) {
		let draggedItemId = $(evt.item).data("taskid");
		$(evt.item).removeClass('dragging');
		this.inDragg = false;

		let task = this.searchTask(draggedItemId);
		let oldParentTask = this.searchTask(task.id_parent_task);
		let newParentTask = this.searchTask($(evt.to).data("taskid"));
		let reportTasks = getReportTasks(this.id_report);

		// Move task to task
		if (task && !oldParentTask && !newParentTask) {
			this.tasks.splice(evt.oldIndex, 1);
			this.tasks.splice(evt.newIndex, 0, task);
			this.tasks.forEach(t => {
				t.position = this.tasks.indexOf(t);
			});
		}

		// Move task to subtask
		if (task && !oldParentTask && newParentTask) {
			if(task.children.length > 0) {
				console.error('Task has children, cannot move it to a subtask');
				// From https://github.com/SortableJS/Sortable/issues/837
				const items = evt.from.querySelectorAll(":scope > .task-container") 
				evt.from.insertBefore(evt.item, items[evt.oldIndex]);
				return false;
			}
			
			task.id_parent_task = newParentTask.id_task;
			task.position = evt.newIndex;
			newParentTask.hasSubtasksUnfolded = true;
			newParentTask.children.splice(evt.newIndex, 0, task);
			newParentTask.children.forEach(t => {
				t.position = this.tasks.indexOf(t);
			});
			this.tasks.splice(evt.oldIndex, 1);
			this.tasks.forEach(t => {
				t.position = this.tasks.indexOf(t);
			});
		}

		// Move subtask to task
		if (task && oldParentTask && !newParentTask) {
			task.id_parent_task = null;
			task.position = evt.newIndex;
			// Add the task to the main tasks list and update positions
			this.tasks.splice(evt.newIndex, 0, task);
			this.tasks.forEach(t => {
				t.position = this.tasks.indexOf(t);
			});
			// Remove the task from the old parent and update positions
			oldParentTask.children.splice(evt.oldIndex, 1);
			oldParentTask.children.forEach(t => {
				t.position = this.tasks.indexOf(t);
			});
		}

		// Move subtask to subtask (potentially to same parent)
		if (task && oldParentTask && newParentTask) {
			task.id_parent_task = newParentTask.id_task;
			task.position = evt.newIndex;
			// Remove the task from the old parent and update positions
			oldParentTask.children.splice(evt.oldIndex, 1);
			oldParentTask.children.forEach(t => {
				t.position = this.tasks.indexOf(t);
			});
			// Add the task to the new parent and update positions
			newParentTask.children.splice(evt.newIndex, 0, task);
			newParentTask.children.forEach(t => {
				t.position = this.tasks.indexOf(t);
			});
			newParentTask.hasSubtasksUnfolded = true;
		}

		this.hasChanged = true;
		this.changeTasksPosition();
	}
		
	function addDragAndDropListener() {
		// Sortable for tasks first level
		const sortable_main_elem = document.getElementById('sortable-tasks');
		if (sortable_main_elem) {
			Sortable.create(sortable_main_elem, {
				group: 'nested', animation: 150, handle: '.task-sortable-handle',
				fallbackOnBody: true,
				swapThreshold: 0.2,
				onStart: (evt) => {
					this.onDragStart.bind(this)(evt);
				},
				onEnd: (evt) => {
					this.onDragEnd.bind(this)(evt);
				}
			});
		}

		// Sortable for subtasks inside task
		let nested = document.querySelectorAll('.nested');
		for (let i = 0; i < nested.length; i++) {
			Sortable.create(nested[i], {
				group: 'nested',
				handle: '.subtask-sortable-handle',
				animation: 150,
				swapThreshold: 0.2,
				fallbackOnBody: true,
				onStart: (evt) => {
					this.onDragStart.bind(this)(evt);
				},
				onEnd: (evt) => {
					this.onDragEnd.bind(this)(evt);
				},
			});
		}
	}

	/** **************************/
	/** Init and refresh  **/
	/** **************************/
	
	async function refresh () {
		if (this.initialized) {
			await this.init(window.global_id_report, null);
		}
	}

	function updateReports () {
		if (this.reports !== undefined) {
			this.reports.splice(0);
		}
		this.report_names.forEach((r) => {
			let weight = this.getReportTasks(r.id_report).reduce((acc, task) => acc + this.getWeight(task), 0);
			reports.push({
				'name': r.name,
				'weight': weight,
				'id_report': r.id_report ? r.id_report : -1,
				'start_datetime': r.start_datetime,
				'end_datetime': r.end_datetime,
			});
		});
		this.reports = reports.sort((a, b) => a.id_report === -1 ? -1 : (b.id_report === null ? 1 : b.weight - a.weight));
	}

	async function init(id_report, report_members, report_names) {
		this.id_report = id_report > 0 ? parseInt(id_report) : -1;
		if (report_names) {
			this.report_names = report_names;
		}
		if(!this.initialized){
			this.updateReports();
			this.selectReports([this.id_report]);
			this.resetNewTask();
			this.taskToggleState = TASK_TOGGLE_STATES.ALL;
		}
		if(!this.initialized && report_members) {
			this.report_members = report_members.map(member => {
				return {
					value: member.id_user,
					selectValue: member.first_name + ' ' + member.user_name,
					displayValue: getDisplayName(member)
				}
			}) || [];
		}
		const params = new URLSearchParams({
			idr: id_report ? id_report : -1,
		});
		await this.handleRequest('/task?' + params.toString(), 'GET', null);

		this.addDragAndDropListener();
		this.initialized = true;
	}
	
	/** **************************/
	/** New task functions **/
	/** **************************/

	function getLastPos() {
		return this.tasks.reduce((acc, value) => {
			return (acc = acc > value.position ? acc : value.position);
		}, 0);
	}

	function resetNewTask() {
		this.newTask.title = "";
		this.newTask.assignees = [];
		this.newTask.isCompleted = false;
		this.newTask.priority = PRIORITIES.NONE;
		this.newTask.dueDate = null;
		this.newTask.estimated_duration = null;
	}

	async function createTask() {
		// In case to move in todo doing done list
		this.newTask.state = this.newTask.isCompleted ? TASK_STATES.DONE : TASK_STATES.IN_PROGRESS;
		// Tell the backend that the task should be linked to the report
		this.newTask.id_report = window.global_id_report;
		this.newTask.position = this.getLastPos() + 1;
		this.newTask.children = [];
		return await this.handleRequest('/task', 'POST', this.newTask).then(() => {
			this.resetNewTask();
			this.addDragAndDropListener();
			let scroll_div = document.getElementById('tasks').closest('.widget-wrapper');
			if(scroll_div) {
				scroll_div.scrollTop = scroll_div.scrollHeight;
			}
		});
	}

	/** **************************/
	/** Update task functions **/
	/** **************************/

	/**
	 * Get the field that have chnaged since last refresh
	 * @param task task to compare
	 * @returns {*[]}
	 */
	function getChanges(task) {
		const ignoredFields = ['assignees', 'children', 'isCompleted'];
		let changes = [];
		const orig = this.origTasks.find(t => t.id_task === task.id_task);
		for (let field in task) {
			if (!ignoredFields.includes(field)) {
				if (orig[field] != task[field]) {
					changes.push(field);
				}
			}
		}
		return changes;
	}

	function getDisplayName(member) {
		return '<span class="small-avatar" title="' + member.first_name + ' ' + member.user_name + '">' + member.first_name[0] + member.user_name[0] + '</span>'
	}

	function setTasks(data) {
		var editedTask = this.tasks.find((t) => t.openInput !== null);
		if (!editedTask) {
			// Search in subtasks
			this.tasks.some((t) => {
				editedTask = t.children.find((c) => c.openInput !== null);
				if (editedTask) {
					return true;
				}
			});
		}
		if (editedTask && ['due_date', 'priority'].includes(editedTask.openInput)) {
			// Abort if openInput is a field for which data is no held by input while typing (datepicker / select)
			return;
		}
		if (editedTask && !data.find((t) => t.id_task === editedTask.id_task)) {
			alertGently(__('La tâche {{title}} à été supprimée', {'title': editedTask.title}), 'danger');
		}
		// Save the original tasks to compare changes (copy by value)
		this.origTasks = JSON.parse(JSON.stringify(data));
		data.forEach((t) => {
			t.assignees.forEach((a) => {
				let member = this.report_members.find(m => m.value === a.id_user);
				if (member) {
					a.displayValue = this.report_members.find(m => m.value === a.id_user).displayValue;
				} else {
					a.displayValue = this.getDisplayName(a);
				}
				a.value = a.id_user;
			});
			t.isCompleted = t.state === TASK_STATES.DONE;
			if (editedTask && editedTask.id_task == t.id_task) {
				t[editedTask.openInput] = editedTask[editedTask.openInput];
				t.openInput = editedTask.openInput;
			} else {
				t.openInput = null;
			}
			t.children = [];
		});
		data.forEach((t) => {
			t.children = data.filter(sub => sub.id_parent_task === t.id_task);
			t.children = t.children.sort((a, b) => a.position - b.position);
		});
		data = data.filter(t => t.id_parent_task === null);
		data = data.sort((a, b) => a.position - b.position);
		// Replace tasks with data
		let tmptasks = data;
		if (this.id_report === -1) {
			tmptasks = [];
			// Force task order to match report order
			this.reports.forEach((r) => {
				const id_r = r.id_report === -1 ? null : r.id_report;
				tmptasks = tmptasks.concat(data.filter((t) => t.id_report === id_r));
			});
		}
		this.tasks.splice(0, this.tasks.length, ...tmptasks);

		this.updateReports();
		this.hasChanged = false;
		window.dispatchEvent(new CustomEvent('tasks_ready', {detail: {taskNS: this}}));
	}


	function getReportTasks (id_r = null) {
		id_r = id_r > 0 ? id_r : null;
		return this.tasks.filter(t => t.id_report === id_r);
	}

	function selectReports (selectedIds) {
		this.selectedReports = selectedIds;
		sendTaskFilterReportTrace(selectedIds);
	}

	function isSelectedReport (id_report) {
		return this.selectedReports.includes(id_report);
	}

	function toggleReport (id_report) {
		if (this.isSelectedReport(id_report)) {
			this.selectedReports = this.selectedReports.filter(i => i !== id_report);
		} else {
			this.selectedReports.push(id_report);
		}
	}
	
	function getWeight(task) {
		if (task.isCompleted) {
			return 0;
		}
		switch (task.priority) {
			case PRIORITIES.LOW:
				return 1;
			case PRIORITIES.MEDIUM:
				return 2;
			case PRIORITIES.HIGH:
				return 3;
			default:
				return 0;
		}
	}

	function getReports() {
		return this.reports;
	}

	async function updateTask(task) {
		task.openInput = null;
		this.hasChanged = true;
		// In case to move in todo doing done list
		task.state = task.isCompleted ? TASK_STATES.DONE : TASK_STATES.IN_PROGRESS;
		let changes = this.getChanges(task)
		if (changes.length > 0) {
			data = {};
			changes.forEach((f) => {
				data[f] = task[f];
			});
			let result = await this.handleRequest('/task/' + task.id_task, 'PATCH', data);
			return result;
		}
	}
	
	async function toggleIsCompleted(id_task) {
		const task = this.searchTask(id_task);
		task.isCompleted = !task.isCompleted;
		this.updateTask(task);
	}

	async function toggleSubTask(id_task) {
		const task = this.searchTask(id_task);
		task.hasSubtasksUnfolded = !task.hasSubtasksUnfolded;
		this.updateTask(task);
	}

	async function updateAssignees(data) {
		return await this.handleRequest('/task/' + data.changeData, 'PATCH', {assignees: data.selected});
	}

	/**
	 * Save the positions of all the tasks
	 * @returns {Promise<void>}
	 */
	async function changeTasksPosition() {
		// Change the order of task (at the same level)
		const positions = {};
		const id_r = this.id_report > 0 ? this.id_report : null;
		let send = false;
		this.tasks.filter(t => t.id_report === id_r).forEach(function (t, i) {
			send = true;
			positions[t.id_task] = {
				id_task: t.id_task, position: i, id_parent_task: t.id_parent_task
			}
			if (t.children) {
				t.children.forEach(function (c, j) {
					positions[c.id_task] = {
						id_task: c.id_task, position: j, id_parent_task: t.id_task
					}
				});
			}
		});
		if (send) {
			await this.handleRequest('/tasks/updatePositions', 'PATCH', {tasks: positions});
			this.addDragAndDropListener()
		}
	}

	/** **************************/
	/** Getters on task information **/
	/** **************************/

	function isInSelectedReport(task) {
		return this.selectedReports.includes(task.id_report ? task.id_report : -1)
	}
	
	function getNumberOfTasksCompleted() {
		return this.tasks.filter(t => t.state === TASK_STATES.DONE && this.isInSelectedReport(t)).length;
	}

	function switchToNextState() {
		switch (this.taskToggleState) {
			case TASK_TOGGLE_STATES.IN_PROGRESS:
				this.taskToggleState = TASK_TOGGLE_STATES.DONE;
				break;
			case TASK_TOGGLE_STATES.DONE:
				this.taskToggleState =  TASK_TOGGLE_STATES.ALL;
				break;
			case TASK_TOGGLE_STATES.ALL:
				this.taskToggleState = TASK_TOGGLE_STATES.IN_PROGRESS;
				break;
		}
		sendTaskFilterTrace('state:: ' + this.taskToggleState);
	}

	function getTaskToggleState() {
		return this.taskToggleState;
	}

	function getNumberOfTasks() {
		return this.tasks.filter(t => this.isInSelectedReport(t)).length;
	}

	function getNumberOfSubtasksCompleted(taskId) {
		return this.tasks.find(t => t.id_task === taskId).children.filter(t => t.state === TASK_STATES.DONE).length;
	}

	function getNumberOfSubtasks(taskId) {
		return this.tasks.find(t => t.id_task === taskId).children.length;
	}
	
	/** **************************/
	/** Task search function **/
	/** **************************/

	function isInFilter(task) {
		if (this.taskToggleState !== TASK_TOGGLE_STATES.ALL) {
			if (this.taskToggleState !== task.state) {
				return false;
			}
		}
		if(this.filter === undefined || this.filter === ''){
			return true;
		}
		if(task.title.toLowerCase().includes(this.filter.toLowerCase())){
			return true;
		}
		if(task.children){
			for(let child of task.children){
				if(child.title.toLowerCase().includes(this.filter.toLowerCase())){
					return true;
				}
			}
		}
		return false;
	}

	/** **************************/
	/** Backend communication **/
	/** **************************/
	async function handleRequest(path, verb, reqData) {
		const options = {
			method: verb, headers: {
				'X-CSRF-TOKEN': document.querySelector('meta[name=csrf-token]').content,
				'Content-Type': 'application/json',
				'Accept': 'application/json',
				'X-Requested-With': 'XMLHttpRequest'
			},
		}
		if (reqData) {
			options['body'] = JSON.stringify(reqData);
		}
		let response = await fetch(path, options);
		let respData = await response.json();
		if (response.ok) {
			if (verb === 'PATCH') {
				const id = parseInt(path.split('/').slice(-1)[0]);
				if (!isNaN(id)) {
					const myTask = respData.tasks.find((t) => t.id_task === id);
					var conflict = false;
					Object.keys(reqData).forEach((f) => {
						switch(f) {
							case 'isCompleted':
								break; // Ignore
							case 'due_date':
								// Servers removes 00:00:00 from date
								let dateConflict = myTask[f]?myTask[f].split(' ')[0]:'';
								conflict |= reqData[f] != dateConflict;
								break;
							case 'assignees':
								// Check assignee list contains same ids
								myTask[f].forEach((a) => {
									conflict |= !reqData[f].find((st) => st.value === a.id_user);
								});
								reqData[f].forEach((st) => {
									conflict |= !myTask[f].find((a) => st.value === a.id_user);
								});
								break;
							default:
								conflict |= (myTask[f] != reqData[f]);
								break;
						}
					});
					param = {title: myTask.title};
					if (conflict) {
						alertGently(__('La tâche {{title}} à déjà été modifiée', param), 'warning');
					}
				}
			}
			this.setTasks(respData.tasks);
		} else {
			for (let msg in respData.errors) {
				alertGently(respData.errors[msg][0]);
			}
		}
	}

	/** **************************/
	/** Task delete function **/
	/** **************************/
	
	async function deleteTask(id_task) {
		const task = this.tasks.find(t => t.id_task === id_task);
		return await this.handleRequest('/task/' + id_task, 'DELETE', {});
	}

	/** **************************/
	/** Task sorting function **/
	/** **************************/
	
	function sortByDueDate(a, b) {
		if (a.due_date === null && b.due_date === null) {
			return 0;
		}
		if (a.due_date === null) {
			return 1;
		}
		if (b.due_date === null) {
			return -1;
		}
		return a.due_date < b.due_date
	}
	
	function sortByPriority(a, b) {
		return PRIORITIES_LIST.indexOf(a.priority) - PRIORITIES_LIST.indexOf(b.priority);
	}

	function sortByEstimatedTime(a, b) {
		return a.estimated_duration - b.estimated_duration;
	}
	
	function sortBy (criteria) {
		let sortFunction;
		let sortDirection;
		
		this.sortByCriteria = criteria;
		switch (criteria) {
			case SORT_BY.DUE_DATE_ASC:
				sortFunction = sortByDueDate;
				sortDirection = SORT_ASC;
				break;
			case SORT_BY.DUE_DATE_DESC:
				sortFunction = sortByDueDate;
				sortDirection = SORT_DESC;
				break;
			case SORT_BY.PRIORITY_ASC:
				sortFunction = sortByPriority;
				sortDirection = SORT_ASC;
				break;
			case SORT_BY.PRIORITY_DESC:
				sortFunction = sortByPriority;
				sortDirection = SORT_DESC;
				break;
			case SORT_BY.ESTIMATED_TIME_ASC:
				sortFunction = sortByEstimatedTime;
				sortDirection = SORT_ASC;
				break;
			case SORT_BY.ESTIMATED_TIME_DESC:
				sortFunction = sortByEstimatedTime;
				sortDirection = SORT_DESC;
				break;
		}

		// Sort the tasks
		this.tasks.sort(sortFunction);
		if(sortDirection === SORT_DESC) {
			this.tasks.reverse();
		}
		this.tasks.forEach(t => {
			t.position = this.tasks.indexOf(t);
		});
		
		// Sort the subtasks
		for (let task of this.tasks) {
			if (task.children) {
				task.children.sort(sortFunction);
				if(sortDirection === SORT_DESC) {
					task.children.reverse();
				}
				task.children.forEach(c => {
					c.position = task.children.indexOf(c);
				});
			}
		}
		
		this.changeTasksPosition();
		sendTaskSortTrace(criteria);
	}

	/** **************************/
	/** Utils **/
	/** **************************/

	function searchTask(id_task) {
		if (!id_task) {
			return null;
		}
		let task = this.tasks.find(t => t.id_task === id_task);
		if (task == undefined) {
			task = this.tasks.map(t => t.children).reduce((acc, val) => acc.concat(val), []).find(c => c.id_task === id_task)
		}
		return task;
	}

	// This function is a workaround for the translate tool to detect these constants
	function getPriorityTrans (task) {
		switch (task.priority) {
			case PRIORITIES.LOW:
				return __('low');
			case PRIORITIES.MEDIUM:
				return __('medium');
			case PRIORITIES.HIGH:
				return __('high');
		}
		return '';
	}

	return {
		addDragAndDropListener,
		changeTasksPosition,
		createTask,
		deleteTask,
		getChanges,
		getDisplayName,
		getLastPos,
		getNumberOfSubtasks,
		getNumberOfSubtasksCompleted,
		getNumberOfTasks,
		getNumberOfTasksCompleted,
		getReportTasks,
		getReports,
		getPriorityTrans,
		getTaskToggleState,
		getWeight,
		handleRequest,
		hasChanged,
		inDragg,
		init: init,
		initialized,
		isInFilter,
		isInSelectedReport,
		isSelectedReport,
		newTask,
		onDragEnd,
		onDragStart,
		refresh,
		report_members,
		resetNewTask,
		searchTask,
		selectReports,
		selectedReports,
		setTasks,
		sortBy,
		sortByCriteria,
		switchToNextState,
		tasks,
		toggleReport,
		toggleIsCompleted,
		toggleSubTask,
		updateAssignees,
		updateReports,
		updateTask,
	}

}();

function sendTraceTask(action, id_task,params) {
	$.ajax({
		url: "/task/trace",
		type: 'POST',
		data: {
			trace: action,
			id_task: id_task,
			id_report: window.global_id_report,
			...params
		},
	});
}

function sendTaskSortTrace(criteria) {
	sendTraceTask(TRACE_TASK_SORT, null, {criteria});
}

function sendTaskFilterTrace(filter) {
	sendTraceTask(TRACE_TASK_FILTER, null, {filter});
}

function sendTaskFilterReportTrace(reports) {
	sendTraceTask(TRACE_TASK_FILTER_REPORTS, null, {reports});
}

function sendTaskOpenWidgetTrace(id_report)
{
	sendTraceTask(TRACE_TASK_OPEN_WIDGET, id_report);
}

function sendTaskCloseWidgetTrace(id_report)
{
	sendTraceTask(TRACE_TASK_CLOSE_WIDGET, id_report);
}

function sendCalendarOpenWidgetTrace()
{
	sendTraceTask(TRACE_CALENDAR_OPEN_WIDGET)
}
function sendCalendarCloseWidgetTrace()
{
	sendTraceTask(TRACE_CALENDAR_CLOSE_WIDGET)
}
function sendCalendarChangeViewTrace(view)
{
	sendTraceTask(TRACE_CALENDAR_CHANGE_VIEW, null, {view})
}
function sendCalendarClickTaskTrace(id_task, id_report)
{
	sendTraceTask(TRACE_CALENDAR_OPEN_TASK, id_task, {id_report})
}
