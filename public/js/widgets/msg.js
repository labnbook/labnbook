// ************************************************************************************************
// WIDGET INITIALIZATION

$(function(){ // fonctions de resize / drag / focus sur le widget
	
	window.global_msg = 1;

	$('#messagerie.teacher').resizable({ // resize
		handles: "all",
		start: function() { widgFocus('messagerie'); },
		stop: function() { widgSaveParam('messagerie') ; }					  
	});
	$('#messagerie.teacher').draggable({ // drag
		handle : "#drag_msg",
		cancel:".tab",
		start: function() { widgFocus('messagerie'); },
		stop: function() { widgSaveParam('messagerie') ; },
		containment: [0,0],
		scroll : false
	});
	$("#messagerie.teacher ").click(function(e){ widgFocus('messagerie'); });
	// maj du containeur avent le drag (pour empêcher de dragger sous la barre du navigateur)
	$('#messagerie.teacher').mouseenter(function(){
		$(this).draggable( "option", "containment", [0,$(window).scrollTop()] );
	});
});




// ************************************************************************************************
// CONVERSATIONS

/*/
 * 
 * @param {boolean} refresh : for refreshing or not the conversation list
 */
function displayConversations(refresh = true) {
	
	$("#msg_id_conv").val("conv") ;
	$("#msg_participants").hide() ;
	// $("#msg_messages").hide() ;
	$("#msg_messages").css('display', 'none') ;
	$("#widget-message-back-btn.fa-arrow-left").remove();
	hideLD2Send() ;
	$("#msg_menu_conversations").show() ;
	$("#msg_conversations").show() ;
	$("#messagerie").css("background-color","white") ;
	if (refresh) {
		displayConversationList();
	}
}

function displayConversationList() { // permet d'obtenir les aperçus de toutes les conversations auxquelles l'utilisateur participe
	var filter = $("#search_conversation input").val() ; // le filtre des conversations
	var id_report_context = 0;
	if (typeof(window.global_id_report) != 'undefined') {
		id_report_context = window.global_id_report;
	}
	$.ajax({ 
		type: "POST",
		url: "/conversations",
		data: {
			filter: filter,
			id_report_context: id_report_context
		},
		success: function(data){ $('#msg_conversations').html(data); }
	});
}

function deleteConversation(id_conv) {
	if (confirm(__('Souhaitez vous quitter définitivement de cette conversation ?'))) {
		let filter = $("#search_conversation input").val() ; // le filtre des conversations
		let id_report_context = 0;
		if (typeof(window.global_id_report) != 'undefined') {
			id_report_context = window.global_id_report;
		}
		$.ajax({ 
			type: "POST",
			url: "/conversation/"+id_conv+"/delete",
			data: {
				id_conv: id_conv,
				filter: filter,
				id_report_context: id_report_context
			},
			success: function(data){
				$('#msg_conversations').html(data);
				displayConversations(false);
			}
		});
	}
}


// ***********************************************************************************************
// PARTICIPANTS & TITLE

/*
 * ouvre le menu de choix des participants à une conversation
 * @param {int} id_conv : 0 if a new conversation is created
 * @param {int} id_report_context : 0 if there is no report_context : TODO replace by id_mission
 * @param {array of int} id_users : the ids of users to add
 * @param {string} users : html to display user names
 */
function editConvParticipants(id_conv, id_report_conv, title, users) {
	new_part_ids = [];
	$("#widget-message-back-btn.fa-arrow-left").remove();
	$("#msg_id_conv").val(0) ;
	// displays
	$("#msg_menu_conversations").hide();
	$("#msg_conversations").hide();
	$("#msg_messages").css('display', 'none');
	hideLD2Send() ;
	$("#msg_participants").css('display', 'flex');
	$('#msg_participants').html(__('Construction de votre liste de destinataires...'));
	if (!id_conv) {
		id_conv = 0 ;
	}
	const scope = typeof(window.global_scope) == 'undefined' ? null : window.global_scope;
	const is_select_from_class = !scope || !id_report_conv;
	$.ajax({ // récupération des participants possibles
		type: "POST",
		url: "/conversation/editParticipants",
		data: {
			id_conv: id_conv,
			id_report_conv: id_report_conv,
			title: title,
			id_report_context: typeof(window.global_id_report) == 'undefined' ? null : window.global_id_report, // TODO : envoyer le id_mission plutôt
			scope: scope
		},
		success: function(data){
			$('#msg_participants').html(data);
			let type = '';
			if (scope !== null) {
				type = scope !== 'follow' ? 'student' : 'follow';
			} else {
				type = 'both';
			}
			// We exclude from the query the current participants
			let exclude_ids = [];
			const participant_list = document.querySelectorAll('.msg_participant');
			if(participant_list && participant_list.length>0) {
				exclude_ids = Array.from(participant_list)
					.filter(el => el.dataset && el.dataset.idUser)
					.map(el => parseInt(el.dataset.idUser));
			}
			requestAnimationFrame(() => requestAnimationFrame(function(){ // wait for the DOM being updated
				load_add_users('msg_add_participants_select', '',
					scope?__('Prénom Nom'):__('Nom Prénom'),
					type, 0, 0, exclude_ids, window.global_id_report );
				if (users) { stageParticipants(users); }
				if(is_select_from_class) {
					// We load a select2 for teachers to select a class
					const configClassSelect = {
						url: '/conversation/getClasses',
						data: {
							id_conv: id_conv
						},
						groups: [
							{
								text: __('Classes'),
								childrenLabel: 'classes',
								paginated: true
							},
						],
						placeholder: __('Classes'),
						clearOnSelect: true,
						width: 'auto',
					};
					window.dispatchEvent(new CustomEvent('lnb-select2-load', {
						detail:{
							selector: 'msg_add_participants_class',
							configSelect2: configClassSelect
						}}));
				}
			}));
		}
	});
	
}

function unStageUser(id) {
	$('#msg_staging_user_'+id).remove();
	new_part_ids.splice(new_part_ids.indexOf(id), 1);
	window.dispatchEvent(new CustomEvent('lnb-select2-remove-id', {
		detail:{
			selector: 'msg_add_participants_select',
			idElement: id
		}}));
	var last_span = $('#msg_participants_list .msg_participant').last();
	if (typeof(last_span.html()) != "undefined" && last_span.html().slice(-1) == ',') {
		last_span.html(last_span.html().slice(0,-1));
	}
}

function addParticipantsByRole (role, id_conversation, selectedClass=null) {
	$.ajax({
			type: "GET",
			url: "/conversation/getParticipantsByRole",
			data: {
				id_report: window.global_id_report,
				id_class: selectedClass?selectedClass.id_class:0,
				id_conversation: id_conversation,
				role: role,
				for_teacher: Number(typeof(window.global_scope) == 'undefined')
			}
		})
		.done(function(responseData) {
			if(responseData.userList) {
				stageParticipants(responseData.userList);
			}
		});
}

function stageUsers(users) {
	if (users.length) {
		var html = "";
		for (var i=0; i<users.length; i++) {
			if (new_part_ids.indexOf(users[i].id_user) >=0 ) {
				continue;
			}
			html += ' <span class="msg_participant';
			if (users[i].connected == 1) {
				html += ' connected_user';
			}
			html += '" id ="msg_staging_user_' + users[i].id_user + '">' + users[i].name ;
			html += '&nbsp;<i class="fas fa-trash-alt" title="' + __("Supprimer le participant") + '" onclick="unStageUser(' + users[i].id_user + ')"></i>,</span>';
			new_part_ids.push(users[i].id_user);
		}
		$('#msg_participants_list .msg_participant').last().append(",");
		$('#msg_participants_list').append(html);
		var last_span = $('#msg_participants_list .msg_participant').last();
		if (last_span.length > 0) {
			last_span.html(last_span.html().slice(0, -1));
		}
		$('#msg_participants_list').animate({scrollTop: 20000},100);
	}
}

function stageParticipant(selectedUser) {
	if(selectedUser) {
		stageUsers([{
			'connected': 0,
			'id_user': selectedUser.id,
			'name': selectedUser.text
		}]);
	}
}

function stageParticipants(users) {
	if (typeof(new_part_ids) == "undefined") {
		new_part_ids = [];
	}
	if (typeof(users) != "undefined") {
		stageUsers(users);
	}
}

/*
 * récupère les id des nouveaux participants depuis les checkboxes
 * @param {int} id_conv = 0 if a new conversation is being created
 */
function addParticipants(id_conv, id_report_conv) {
	
	if ($("#msg_conv_title").val()) {
		var title = $("#msg_conv_title").val();
	} else {
		var title = "";
	}

	if ($('input[name=msg_add_participants_input]').length && $('input[name=msg_add_participants_input]').val() != "") {
		alertGently(__("Un utilisateur est selectionné, cliquez le + pour l'ajouter"));
		return;
	}

	if ($('select[name=msg_add_participants_select]').val()) {
		alertGently(__("Un participant a été sélectionné mais n'a pas été ajouté. Cliquez sur <i class='fa fa-check'></i> pour l'ajouter"));
		return;
	}

	if (typeof (new_part_ids) == 'undefined') {
		new_part_ids = [];
	}

	// cas de la création d'une nouvelle conversation
	if (!id_conv && new_part_ids.length == 0) {
		alert(__("Pour créer une nouvelle conversation, vous devez indiquer au moins un participant."));
		return;
	}

	// cas de la modification d'une conversation existante
	if (id_conv && new_part_ids.length) {
		if (confirm(__("Un nouveau participant pourra consulter tous les anciens messages et ne pourra plus être supprimé de la conversation.\nSouhaitez vous ajouter de nouveaux participants à la conversation ?"))) {
			displayMessages(id_conv, title, id_report_conv, new_part_ids);
		}
	} else {
		displayMessages(id_conv, title, id_report_conv, new_part_ids);
	}
	new_part_ids = [];
}


// *************************************************************************************************
// LIST OF MESSAGES

/*
 * Affiche les participants et les messages d'une conversation 
 * @param {int} id_conv : conversation = 0 en cas de nouvelle conversation
 * @param {string} title : titre à mettre à jour si non null
 * @param {array} new_part_ids : id des participants à ajouter (null si rien à ajouter)
 * @param {int} id_report_context : 0 if there is no report_context
 */
function displayMessages(id_conv, title, id_report_conv, new_part_ids) {
	$("#msg_menu_conversations").hide() ;
	$("#msg_conversations").hide() ;
	$("#msg_participants").hide() ;
	$("#msg_messages").css('display', 'flex') ;
	$("#messagerie").css("background-color","white") ;
	$('#msg_messages').html("");
	
	let id_report_context = 0;
	let scope = null;
	if (typeof(window.global_id_report) != 'undefined') {
		id_report_context = window.global_id_report;
		scope = window.global_scope;
	}
	
	$.ajax({ 
		type: "POST",
		url: "/conversation/show",
		data: {
			id_conv: id_conv,
			title: title,
			id_report_conv: id_report_conv,
			new_part_ids: new_part_ids.join(','),
			id_report_context: id_report_context,
			scope: scope
		},
		success: function(data){
			let conversation= $("#conv_"+id_conv);
			//let layer = $("#lb_menubar_msg .fa-layers-counter");
			let layer = $("#lb_menubar_messagerie .fa-layers-counter");
			$('#msg_messages').html(data);
			if (id_conv) {
				$("#msg_id_conv").val(id_conv) ;
				if(conversation[0] && conversation[0].className === "msg_conversation has-new-msg" ){
					layer[0].innerHTML--;
				}
			} else {
				// New conversation
				$("#msg_id_conv").val($("#msg_new_conv_id").val()) ;
			}
			$("#msg_msg_messages").scrollTop(2000000) ; // aller en bas de la liste des msg
			if($("#messagerie.student").length>0) {
				$(".widget-message-header")[0].appendChild($('#msg_menu_messages .widget-message-btn')[0]);
				$("#messagerie.student #msg_menu_messages #widget-message-back-btn").remove();
			}
			$("#messagerie.student").closest('.widget-wrapper').scrollTop(2000000) ;
			conversation.removeClass("has-new-msg");
			if (!$("#msg_conversations .msg_conversation").hasClass("has-new-msg")) {
				layer.hide();
			}
		}
	});
}

/**
 * récupère et  affiche la liste des messages de la conversation avec éventuellement envoi d'un nouveau msg
 * @param {string} new_msg : contenu du message : vide si il n'y a pas de nouveau message
 * @param {int} id_ld : labdoc attaché : 0 si il n'y en a pas
 * @param {boolean} reset_send_msg : savoir si la fenetre d'écriture de msg doit être réinitialisée
 * @param {boolean} scroll : go to the bottom of the msg panel
 * @returns {undefined}
 */
function updateMessages(new_msg, id_ld, scroll) { 
	if (typeof(window.global_id_report) != 'undefined') {
		var id_report_context = window.global_id_report;
		var scope = window.global_scope;
	} else {
        var id_report_context = 0;
        var scope = null;
    }
	var id_conv = $("#msg_id_conv").val();
	$.ajax({ 
		type: "POST",
		url: "/conversation/"+id_conv+"/addMessage",
		data: {
			id_report_context: id_report_context, // report context is needed if it's a new conversation
			scope: scope,
			new_msg: new_msg,
			id_ld: id_ld
		},
		success: function(data){
			 updateWidgetComMsg(data, "msg", scroll);	
		}
	});
}

function displayLD2Send(){ // affiche la fenetre de choix du LD a joindre au message
	$("#msg_send_wrapper_right").hide() ;
	$("#msg_send_wrapper_left").hide() ;
	$("#ld_send_wrapper_right").show() ;
	$("#ld_send_wrapper_left").show() ;
	
	var id_report_context = 0;
	if (typeof(window.global_id_report) != 'undefined') {
		id_report_context = window.global_id_report;
	}
	$.ajax({ 
		type: "POST",
		url: "/conversation/displayLD2Send",
		data: {
			id_report_context: id_report_context,
		},
		success: function(data){ $("#ld_send_wrapper_left").html(data); }
	});
}

function hideLD2Send() {
	$("#msg_send_wrapper_right").show() ;
	$("#msg_send_wrapper_left").show() ;
	$("#ld_send_wrapper_right").hide() ;
	$("#ld_send_wrapper_left").hide() ;
}

function sendMsg() { // Envoi d'un message
	var msg = $('#text_msg_send').val() ;
	if(msg){
        $('#text_msg_send').val("");
		msg = msg.replace(/</g,"&lt;");
		updateMessages(msg, 0, true);
	}
}

function sendLD() { // Envoi d'un LD - crée la conversation si elle n'existe pas
	if ($("#ld_to_attach").val()) {
		updateMessages("", $("#ld_to_attach").val(), true) ;
	}
	hideLD2Send() ;
}

function messageView() {
	return {
		displayAttachedLd:false,
		idReportPart:null,
		reportParts:null,
		attachedLabdoc:null,
		saveAttachedLD(id_ld) {
			if(this.idReportPart) {
				const selectedRp = this.reportParts.find((rp) => parseInt(this.idReportPart) === parseInt(rp.id_report_part));
				if(selectedRp && selectedRp[this.attachedLabdoc.type_labdoc]) {
					duplicateLD(id_ld, this.idReportPart,0,0);
					this.displayAttachedLd = false;
				} else {
					alertGently(__('Cette partie de rapport n\'autorise pas de labdoc de ce type'), 'warning');
				}
			} else {
				alertGently(__('Sélectionner une partie'), 'warning');
			}
		},
		toggleDownloadLD(el){ 
			this.displayAttachedLd = !this.displayAttachedLd;
			if(this.displayAttachedLd) {
				this.$nextTick(() => {
					// scrol in order to display the new option panel
					let $msg_panel = $("#messagerie.student").closest('.widget-wrapper');
					if(!$msg_panel) {
						$msg_panel = $("#msg_msg_messages");
					}
					$msg_panel.scrollTop($msg_panel.scrollTop() + 70);
				});
			}
		},
		loadRPAndLDInfo(report_parts, attached_ld) {
			this.reportParts = JSON.parse(report_parts);
			this.attachedLabdoc = attached_ld;
		}
	}
}

/**
 * 
 * @param {array} new_msg : associative array containing id_convs and the last msg unseen
 * @returns {undefined}
 */
function refreshMessage(new_msg) {
	//let layer = $("#lb_menubar_msg .fa-layers-counter");
	let layer = $("#lb_menubar_messagerie .fa-layers-counter");
	layer[0].innerHTML=new_msg.length;
	if (new_msg.length == 0) { // pas de nouveau msg
		layer.hide();
	} else {
		if ("key_"+$("#msg_id_conv").val() in new_msg) { // la fenêtre de message est ouverte sur une conversation à rafraichir
			updateMessages("", 0, false);
			if (new_msg.length > 1) {
				layer.show();
			} else {
				layer.hide();
			}
		} else if ($("#msg_id_conv").val() == "conv") { // the conv div of the widget is displayed
			layer.show();
			var refreshed = false;
			// vérifier s'il y a une nouvelle conv et faire un refresh si besoin
			for (var key in new_msg) {
				var id_conv = key.substr(4);
				if ($("#msg_conversations #conv_"+id_conv).length == 0) {
					displayConversationList();
					refreshed = true;
					break;
				} 
			}
			if (!refreshed) {
				$("#msg_conversations .no-new-msg").show();
				$("#msg_conversations .new-msg").hide();
				$("#msg_conversations .msg_conversation").removeClass("has-new-msg");
				for (var key in new_msg) {
					var id_conv = key.substr(4);
					$("#conv_"+id_conv+" .no-new-msg").hide();
					$("#conv_"+id_conv+" .new-msg").show();
					$("#conv_"+id_conv+" .conv_snapshot").html(new_msg[key]);
					$("#conv_"+id_conv).addClass("has-new-msg");
				}
			}
		} else {
			layer.show();
		}
	}
}
