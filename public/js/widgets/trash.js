// Widget

$(function(){ // fonctions de resize / drag / focus sur le widget
    if ($("#trash").is(":visible")) {
        displayDeletedLDinTrash(global_id_report);
    }
});

function displayDeletedLDinTrash(id_report) {
	$.ajax({ 
		type: "POST",
		url: "/trash/listDeletedLabdoc",
		data: {
			id_report: id_report
		}
	})
	.done(function(response) {
		$("#trash_content").html(response);
	})
	.fail(function(xhr) {
		console.log('error', xhr);
	});
}

function restoreLD(id_report, id_report_part, id_ld) {
	$.ajax({ 
		type: "POST",
		url: "/trash/restoreLabdoc",
		data: {
			id_report: id_report,
			id_report_part: id_report_part,
			id_labdoc: id_ld,
		},
	})
	.done(function(response) {
		$("#trash_ld_"+id_ld).remove();
		$("#rp_sortable_" + id_report_part).append('<div id="labdoc_' + id_ld + '" class="sortable-labdoc labdoc" data-id_ld="' + id_ld + '"></div>');
		getAndDisplayLD(id_ld);
	})
	.fail(function(xhr) {
		console.log('error', xhr);
	});
}

/**
 * Hard Delete of the LD with suppression from the DB
 * @param {int} id_report
 * @param {int} id_ld
 * @param {boolean} no_confirm
 */
function realDeleteLD(id_report, id_ld, no_confirm = false) {
	if(no_confirm || confirm(__("Ce labdoc va être définitivement supprimé, sa récupération sera impossible !"))){
		$('#trash_ld_' + id_ld + ' i.far').removeClass('fa-trash-alt').removeClass('far').addClass('fa-spinner fa-pulse').addClass('fa').removeAttr('onclick')
		$.ajax({ 
			type: "POST",
			url: "/trash/deleteLabdoc",
			data: {
				id_labdoc: id_ld,
				id_report: id_report
			}
		})
		.done(function(response) {
			$("#trash_content").html(response);
		})
		.fail(function(xhr) {
			console.log('error', xhr);
		});
	}
}
