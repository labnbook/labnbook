/* global FullCalendar */

window.calendarCtrl = function () {
	function fetchEvents() {
		return this.getTasks().concat(this.getReports());
	}
	/**
	 * Initialize the calendar
	 */
	function init (event, domElement, container, tz) {
		this.MonthViewName = 'dayGridMonth';
		if (!this.initialized) {
			this.events = [];
			this.tasksNS = window.tasksCtrl;
			this.calendarEl = domElement;
			this.tz = tz;
			this.container = container;
			this.loadCalendar();
		}
		this.events = this.fetchEvents();
		this.dirty = true; // events have changed
		this.render();
		this.initialized = true;
	}

	/**
	 * Updates calendar view
	 */
	function render () {
		if (!this.calendarEl || !this.calendarEl.checkVisibility()) {
			return; // Nothing to do
		}
		if (this.dirty) {
			// Refresh events only if they have changed
			this.calendar.refetchEvents();
			this.dirty = false;
		}
		//update height
		const widgetHeaderHeight = this.container.getElementsByClassName('widget-header')[0].offsetHeight;
		this.container.getElementsByClassName('widget-slot')[0].style.height = 'calc(100% - '+widgetHeaderHeight+'px)';
		this.calendar.render()
	}

	/**
	 * Converts a task object into an event object like
	 */
	function taskToEvent (task) {
		const idr = task.id_report === null ? -1 : task.id_report;
		const title = task.title + ' (' + task.report_name + ')';
		return {
			id: 'task_' + task.id_task,
			title,
			start: task.due_date + ' 00:00:00', // Convert datetime to date
			extendedProps: {
				context: 'report_' + idr,
				users: task.assignees,
				id_report: idr,
				id_task: task.id_task
			}
		};
	}

	/**
	 * Converts a report object into an event object like
	 */
	function reportToEvent (report) {
		const idr = report.id_report === null ? -1 : report.id_report;
		return {
			id: 'report_' + idr,
			title: report.name,
			start: report.start_datetime,
			end: report.end_datetime,
			extendedProps: {
				id_report: idr,
				id_task: -1,
			}
		}
	}

	/**
	 * Returns tasks that should be added as events
	 */
	function getTasks () {
		return this.tasksNS
			.tasks
			.filter((t) => t.due_date)
			.map((t) => this.taskToEvent(t));
	}

	/**
	 * Returns reports that should be added as events
	 */
	function getReports () {
		return this.tasksNS
			.getReports()
			.filter((r) => r.start_datetime || r.end_datetime)
			.map((r) => this.reportToEvent(r));
	}

	/**
	 * Handles a fullcalendar dateset event
	 */
	function handleDateSet(dateInfo)
	{
		if (this.view != dateInfo.view.type) {
			sendCalendarChangeViewTrace(dateInfo.view.type);
			this.view = dateInfo.view.type;
		}
	}

	/**
	 * Handles a click on an event
	 */
	function handleEventClick (info) {
		if (!info.event.end && info.view.type === this.MonthViewName) {
			sendCalendarClickTaskTrace(info.event.extendedProps.id_task, info.event.extendedProps.id_report === -1 ? null : info.event.extendedProps.id_report);
			this.calendar.changeView('listDay', info.event.start);
		}
	}

	function getEventDescr (event) {
		// Use temporary element to escape html
		const tempDomElement = document.createTextNode(event.title);
		let text = tempDomElement.textContent;
		if (event.extendedProps.users && event.extendedProps.users.length > 0) {
			event.extendedProps.users.forEach(function (u) {
				text += '&nbsp;' + u.displayValue;
			});
		}
		return text;
	}

	/**
	 * Loads the calendar
	 */
	function loadCalendar () {
		this.view = this.MonthViewName;
		this.calendar = new FullCalendar.Calendar(this.calendarEl, {
			timeZone: this.tz,
			locale: window.labnbook.lang,
			defaultTimedEventDuration: '00:00',
			dayMaxEventRows: true,
			initialView: this.view,
			headerToolbar: {
				left: 'prev,next',
				center: 'title',
				right: 'dayGridMonth,listWeek'
			},
			displayEventTime: false,
			eventContent: function (arg) {
				const descr = document.createElement('div');
				descr.innerHTML = getEventDescr(arg.event);
				descr.setAttribute('title', descr.textContent);
				descr.classList.add('ellipsis');
				let nodes = [descr];
				if (arg.view.type === this.MonthViewName && !arg.event.end) {
					const dot = document.createElement('i')
					dot.classList.add('fc-daygrid-event-dot');
					nodes = [dot, ...nodes];
				}
				return { domNodes: nodes }
			}.bind(this),
			viewDidMount: function (arg) {
				this.events.forEach(function (e) {
					if (e.id.startsWith('report_')) {
						if (arg.view.type !== this.MonthViewName && e.end) {
							e.old_start = e.start;
							e.start = e.end;
						} else if (arg.view.type === this.MonthViewName && typeof(e.old_start) !== 'undefined') {
							e.start = e.old_start;
						}
					}
				}.bind(this));
				this.calendar.refetchEvents();
			}.bind(this),
			height: '100%',
			datesSet: this.handleDateSet.bind(this),
			eventClick: this.handleEventClick.bind(this),
			events: function (info, successCallback, failureCallback) {
				return Promise.resolve(this.events);
			}.bind(this),
			moreLinkContent: function (arg) {
				return arg.shortText;
			}
		});
	}

	return {
		fetchEvents,
		getReports,
		getTasks,
		handleDateSet,
		handleEventClick,
		init,
		loadCalendar,
		render,
		reportToEvent,
		taskToEvent
	}
}();
