// *********************************************************************************************
//                                    VIEW FUNCTIONS
// *********************************************************************************************

// Widget initialization
$(function(){
    // Initial display if the widget is opened
    if ($("#ressource").is(":visible")) {
        updateLearnerDocs();
    }
    
    // Trace
	if (window.global_scope == "default" || window.global_scope == "view") {
		$("#widg_res_assignment").on("click", ".resource-link", function() {
			$.ajax({
				method: 'POST',
				url: "/report/"+window.global_id_report+"/trace",
				data: {
					trace: 'viewDetailedAssignment'
				}
			});
		});
        $("#widg-res-given-docs").on("click", ".resource-link", function () {
            var url = $(this).attr('href');
            $.ajax({
                method: 'POST',
                url: "/report/"+window.global_id_report+"/trace",
                data: {
                    trace: 'viewDoc',
                    url: url
                }
            });
        });
	}
});

function traceOpenAssignment() {
    $.ajax({
        method: 'POST',
        url: "/report/"+window.global_id_report+"/trace",
        data: {
            trace: 'viewAssignment'
        }
    });
}

/**
 * Met à jour de contenu depuis le serveur
 */
function updateLearnerDocs(){
    $("#widg-res-docs-list").html("<i class='fa fa-spinner fa-pulse'></i>");
	$.ajax({ 
		type: "POST",
		url: "/resource/updateLearnerDocs",
		data: {
			id_report : window.global_id_report
		},
		success: function(data){
			$("#widg-res-docs-list").html(data);
		}
	});
}

function toggleSpinner(waiting) {
	if (waiting) {
		$("#add_document_btn").hide();
		$("#add_document_spinner").show();
	} else {
		$("#add_document_btn").show();
		$("#add_document_spinner").hide();
	}
}

function toggleAddDoc(inactive=false) {
	if(inactive) {
		alertGently(__('Désactivé dans ce mode'), 'warning');
		return;
	}
    if (window.global_scope === "test") {
		alertGently(__("Il n'est pas possible d'ajouter de documents en mode test"), 'warning');
        return;
    }
	toggleSpinner(0);
    // efface les champs
    $('#name_resource').val('');
    $('#url_document_address').val('');
    if ($('#add_document').css('display') == 'none') {
        $('#add_document').show();
        $('#add_doc_link').hide();
        $("#widg-res-content").scrollTop($("#widg-res-content")[0].scrollHeight);
    } else {
        $('#add_document').hide();
        $('#add_doc_link').show();
    }
}

function urlOrFile(){
	var type = document.getElementsByName("type_document");
	if(type[0].checked){
		$("#url_document").show();
		$("#res-file").hide();
	} else {
		$("#url_document").hide();
		$("#res-file").show();
	}
    $("#widg-res-content").scrollTop($("#widg-res-content")[0].scrollHeight);
}


// *********************************************************************************************
//                                    CONTROL FUNCTIONS
// *********************************************************************************************

// Tab Documents

function addDoc() { // ajoute un document : url ou fichier
	var name = $('#name_resource').val() ;
	if ($('input:radio[name=type_document]:checked').val() == 'link') { // on est dans le cas d'un ajout d'url : on fait une requete ajax
		var url = $('#url_document_address').val() ;
		if (!name || !url) { alert(__("Veuillez indiquer le nom et une adresse internet")) ; }
		else {
			$.ajax({
				type: "POST",
				url: "/resource/addUrl",
				data: {
                    id_report: window.global_id_report,
                    name: name,
                    url: url
                },
				success: function(){
					updateLearnerDocs() ;
					toggleAddDoc() ;
				}
			});
		}
	}
	else { // on est dans le cas d'un ajout de fichier - on passe par le formulaire 
		if (!name) {
            alert(__("Veuillez indiquer le nom de votre document")) ;
            return;
        } else if (typeof $("#res_file_document")[0].files[0] === "undefined") {
			alert(__("Veuillez sélectionner un fichier"));
			return;
		} else if ($('#res_file_document')[0].files[0].size > 2*1024*1024) {
            alert(__("Votre fichier est trop volumineux - la taille maximale acceptées est de 2Mo"));
            return;
        } else  {
			toggleSpinner(1);
            $('#add_document').submit() ;
        }
	}
}


$(document).ready(function() {
	var options = { 
		target: '#response_ajax_file', 
		success: function(data) { 
			if(data){
				alertGently(data);
			}else{
				updateLearnerDocs() ;
				toggleAddDoc() ;
				$('#add_document').resetForm();
				urlOrFile();
			}
		},
		error: function (response) {
			if (response.status == 413) {
				alertGently(__("L'upload du fichier n'a pas fonctionné. La taille maximum pour les fichiers est de 2 Mo."));
			}else{
				alertGently(response.responseJSON);
			}
			toggleSpinner(0);
		},
	};
	
	// bind formulaire pour l'ajout d'un document avec une fonction de callback en Ajax
	$('#add_document').ajaxForm(options); 
});

function learnerDoc(docName) {
	return {
		editDoc: false,
		docName: docName,
		delDoc(id) {
			if( confirm(__('Voulez-vous supprimer définitivement le document ?')) ){
				let self = this;
				$.ajax({
					type: "POST",
					url: "/resource/"+id+"/delDoc",
					data: {
						id_report: window.global_id_report
					},
					success: function(){
						$('#learner_doc_'+id).remove();
						self.editDoc = false;
					}
				});
			}
		},
		renameDoc(id){
			if (!this.docName) {
				alertGently(__("Veuillez indiquer un nom pour le document"));
				return;
			}
			let self = this;
			$.ajax({
				type: "POST",
				url: "/resource/"+id+"/renameDoc",
				data: {
					id_report: window.global_id_report,
					name: this.docName
				},
				success: function(){
					self.editDoc = false;
				}
			});
		}
	}
}


/**
 * Use an XML (<ressource><lru></lru></ressource>) file 
 * to update visual information about resources
 * @param new_list_ressource
 */
function displayRessources(new_list_ressource){
	let layer = $("#lb_menubar_ressource .fa-layers-counter");
	layer.hide();
	if(layer[0].innerHTML !== new_list_ressource.length.toString()){
		layer[0].innerHTML = new_list_ressource.length;
	}
	if(layer[0].innerHTML !== "0"){
		layer.show();
	}
}

/**
 * Use updateOrInsert(Request $request) from ResourceController.php
 * @param $resource
 */
function updateOrInsert($resource) {
	let layer = $("#lb_menubar_ressource .fa-layers-counter");
	$resource = parseInt($resource,10);
	if(isNaN($resource)) {
		alertGently(__("L'id de votre ressource est erroné"));
	}
	let learner_id = '#learner_doc_' + $resource + ' .fa-circle';
	let learner_edit_id = '#learner_doc_edit_' + $resource + ' .fa-circle';
	if($(learner_id)[0].classList.contains('new')){
		layer[0].innerHTML--;
	}
	if(layer[0].innerHTML === "0"){
		layer.hide();
	}
	$(learner_id)[0].classList.remove('new');
	if($(learner_edit_id).length !== 0){
		$('#learner_doc_edit_' + $resource + ' .fa-circle')[0].classList.remove('new');

	}
	$.ajax({
		type: "POST",
		url: "/resource/"+ $resource+"/updateOrInsertLinkResourceUser",
	})
}
