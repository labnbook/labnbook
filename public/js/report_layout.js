const reportViewCtrl = function(report, scope, widget_config) {
	
	function init(allowed_widget_types) {
		this.allowed_widget_types = JSON.parse(allowed_widget_types);
		this.is_default = this.scope === 'default';
		this.is_pdf = this.scope === 'pdf';
		this.is_view = this.scope === 'view';
		this.is_test = this.scope === 'test';
		this.is_follow = this.scope === 'follow';
		if (this.report) {
			this.is_solution = this.report.status === 'solution';
		}
		
		this.reduced = this.widget_config?(this.widget_config.widget_reduced===1):true;
		if(this.is_test && this.widget_config && this.widget_config.widget_type === WIDGET.MESSAGING) {
			this.reduced = true;
		}
		this.fixed_to_bottom = this.widget_config?(this.widget_config.widget_position === 'bottom'):false;
				
		this.setMinColumnSize();
		this.reduced_width = 80;
		this.responsive_small_size = window.innerWidth < RESPONSIVE_WIDTH.SM;

		this.report_content_div = document.getElementById('report_content');
		if (this.report) {
			this.header_wrapper = document.getElementById('headers_wrapper');
			this.report_column_id = 'report_column';
		} else {
			this.header_wrapper = document.getElementById('welcome_report');
			this.report_column_id = 'cadre_mission';
		}
		this.widgets_column_div = document.getElementById('widgets_column');
		this.widgets_content_div = document.getElementById('widgets_column').getElementsByClassName('widget-content')[0];

		this.width_expanded_column = 380;
		this.height_expanded_column = 380;
		if (this.widget_config && this.widget_config.widget_size_ratio) {
			this.active_widget_type = this.widget_config.widget_type;
			const active_layout_config = this.getWigetConfigLayout();
			document.body.style.maxWidth = active_layout_config.body_max_width;
			if(this.fixed_to_bottom) {
				this.height_expanded_column = Math.min(Math.max(active_layout_config.min_height,Math.round(window.innerHeight * this.widget_config.widget_size_ratio / 100)),active_layout_config.max_height);
			} else {
				this.width_expanded_column = Math.min(Math.max(active_layout_config.min_width,Math.round(window.innerWidth * this.widget_config.widget_size_ratio / 100)),active_layout_config.max_width);
			}
			this.moveColumnPosition();
		} 
		if (!window.report_env) {
			// Report index, widget is reduced only on small screen
			this.reduced = this.responsive_small_size;
			this.active_widget_type = WIDGET.CALENDAR;
		} else if(window.report_env.open_scoring_on_init) {
			// Open scoring at the opening if needed
			this.reduced = false;
			this.active_widget_type = WIDGET.SCORING;
		} else if(window.report_env.print_assignment) {
			// Open assignment at the opening if needed
			this.reduced = false;
			this.active_widget_type = WIDGET.RESOURCES;
		} else {
			if(this.widget_config && this.widget_config.widget_type 
				&& !(this.is_test && this.widget_config.widget_type === WIDGET.MESSAGING)) {
				this.toggleWidgetItem(this.widget_config.widget_type);
			}
		}
		// display report content to adjust widget column size
		const wait_for_init = document.getElementById('report-wait-for-init');
		if(wait_for_init) {
			wait_for_init.remove();
		}
		this.report_content_div.classList.add('initialized');
		this.reducedExpandColumn();

		window.addEventListener("resize", () => {
			this.setMinColumnSize();
			this.reducedExpandColumn();
		});

		this.initColumnResizeEvent();
	}
	
	function reducedExpandColumn() {
		this.responsive_small_size = window.innerWidth < RESPONSIVE_WIDTH.SM;
		if(this.reduced) {
			document.body.style.maxWidth = '';
			if (this.active_widget_type === WIDGET.TASK) {
				sendTaskCloseWidgetTrace(window.global_id_report);
			}else if (this.active_widget_type === WIDGET.CALENDAR) {
				sendCalendarCloseWidgetTrace();
			}
			this.active_widget_type = null;
			if(!this.responsive_small_size) {
				this.report_content_div.style.gridTemplateRows = 'auto';
				this.report_content_div.style.gridTemplateColumns = '1fr '+this.reduced_width+'px';
				this.report_content_div.style.gap = 'calc(1rem + 3px)';
				this.widgets_column_div.style.width = this.reduced_width+'px';
				this.widgets_column_div.style.top = this.report_content_div.offsetTop+'px';
				this.widgets_column_div.style.bottom = 'auto';
			} else {
				this.setWidgetSizeSmallScreen();
			}
			this.adjustAndSave();
		} else {
			this.moveColumnPosition();
		}
	}

	function setWidgetSizeSmallScreen() {
		this.report_content_div.style.gridTemplateColumns = 'auto';
		this.report_content_div.style.gridTemplateRows = '1fr '+this.reduced_width+'px';
		this.report_content_div.style.gap = '1.5rem';
		this.widgets_column_div.style.width = 'calc(100% - '+(2*this.header_wrapper.getBoundingClientRect().left)+'px';
		this.widgets_column_div.style.height = '';
		this.widgets_column_div.style.top = 'auto';
		this.widgets_column_div.style.bottom = '0';
		this.widgets_column_div.style.right = 'auto';
	}
	
	function adjustWidgetColumnSize() {
		this.responsive_small_size = window.innerWidth < RESPONSIVE_WIDTH.SM;
		if(this.responsive_small_size) {
			this.widgets_column_div.style.width = 'calc(100% - '+(2*this.header_wrapper.getBoundingClientRect().left)+'px';
			this.widgets_content_div.style.height = (window.innerHeight - (this.header_wrapper.offsetHeight+65))+'px';
		} else {
			if(this.fixed_to_bottom && !this.reduced) {
				this.widgets_column_div.style.top = 'auto';
				this.widgets_column_div.style.bottom = '65px';
				if(!this.reduced) {
					this.widgets_column_div.style.height = this.height_expanded_column+'px';
					this.widgets_column_div.style.width = this.header_wrapper.offsetWidth+'px';
					this.widgets_content_div.style.height = (this.height_expanded_column-5)+'px';
				}
			} else {
				this.widgets_column_div.style.top = this.report_content_div.offsetTop+'px';
				this.widgets_column_div.style.bottom = 'auto';
				if(!this.reduced) {
					this.widgets_column_div.style.width = this.width_expanded_column + 'px';
					if(!this.report) {
						let footer = document.getElementById('footer');
						const footer_full_height = footer?footer.offsetHeight + parseInt(window.getComputedStyle(footer).marginTop) + parseInt(window.getComputedStyle(footer).marginBottom):0;
						this.widgets_content_div.style.height = document.documentElement.clientHeight - (this.widgets_column_div.offsetTop + 55 + footer_full_height)+'px';
					} else {
						this.widgets_content_div.style.height = window.visualViewport.height - (this.widgets_column_div.offsetTop + 55)+'px';
					}
				}
			}
		}
		this.adjustAndSave();
		window.dispatchEvent(new CustomEvent('widget_resized'));
	}
	
	function adjustAndSave() {
		this.widgets_column_div.style.right = this.header_wrapper.getBoundingClientRect().left+'px';
		this.saveWidgetConfig();
		this.adjustDatasetLabdocsWidth();
	}
	
	function saveWidgetConfig(last_change=false) {
		if (!this.report) {
			return;
		}
		if(!this.wait_for_next_save) {
			if(!last_change) {
				// Update changes with a timeout of 1 sec
				this.wait_for_next_save = true;
				setTimeout(()=> {
					this.wait_for_next_save=false;
					// Save last changes
					this.saveWidgetConfig(true);
				},2000);
			}
			const widget_current_size_ratio = this.fixed_to_bottom?
				(Math.round(this.height_expanded_column/window.innerHeight*100))
				:(Math.round(this.width_expanded_column/window.innerWidth*100));
			// If it's comment we save resource
			const widget_type = this.active_widget_type;
			$.ajax({
				type: "POST",
				url: "/widgetconfig/saveUserWidgetConfig",
				data: {
					id_report: this.report.id_report,
					widget_type: widget_type,
					widget_reduced: this.reduced?1:0,
					widget_position: this.fixed_to_bottom?'bottom':'right',
					widget_size_ratio: widget_current_size_ratio,
				},
				error:function(msg){ console.log('Erreur 26', msg); },
				success:function(data){  }
			});
		}
	}
	
	function adjustDatasetLabdocsWidth() {
		if (!window.global_tab_ld) {
			return;
		}
		Object.entries(window.global_tab_ld).map(ld => {ld[1].id_ld = ld[0]; return ld[1]})
			.filter(ld => ld.ld_type === 'dataset').forEach((ld) => {
				const ld_div = document.getElementById('labdoc_content_'+ld.id_ld)
				if(ld_div && ld_div.checkVisibility() && ld.view) {
					ld.view.displayGraphs();
				}
			});
	}

	/**
	 * Used to dynamically toggle widget item content
	 * @param widgetType
	 * @param id_ld
	 * @param ld_name
	 */
	function toggleWidgetItem(widgetType, id_ld=null) {
		if(!this.allowed_widget_types.includes(widgetType)) {
			this.reduced = true;
			this.active_widget_type = null;
			return;
		}
		const previous_reduced = this.reduced; 
		this.reduced = false;
		const previous_type = this.active_widget_type; 
		this.active_widget_type = widgetType;
		this.setMinColumnSize(true);
		
		// Specific cases
		if (widgetType === WIDGET.RESOURCES) {
			updateLearnerDocs();
		}
		else if (widgetType === WIDGET.COMMENTS) {
			const first_ld_with_new_com = Alpine.store('reportLabdocs').getFirstLdWithNewCom();
			if(first_ld_with_new_com) {
			 	Alpine.store('commentWidgetIdLd', first_ld_with_new_com.id_ld);
			}
		}
		else if (widgetType === WIDGET.MESSAGING) {
			if(this.is_test) {
				alertGently(__('La messagerie n’est pas fonctionnelle en mode test'), 'warning', 1000);
				this.toggleWidgetItem(previous_type);
			} else {
				displayConversations();
			}
		}
		else if (widgetType === WIDGET.TRASH) {
			displayDeletedLDinTrash(global_id_report);
		} else if (widgetType === WIDGET.TASK) {
			if (previous_reduced || previous_type != widgetType) {
				sendTaskOpenWidgetTrace(window.global_id_report);
			}
		} else if (widgetType === WIDGET.CALENDAR) {
			if (previous_reduced || previous_type != widgetType) {
				sendCalendarOpenWidgetTrace();
			}
			window.dispatchEvent(new CustomEvent('calendar_toggle'));
		}
		if (previous_type === WIDGET.TASK && widgetType !== WIDGET.TASK) {
			sendTaskCloseWidgetTrace(window.global_id_report);
		} else if (previous_type === WIDGET.CALENDAR && widgetType !== WIDGET.CALENDAR) {
			sendCalendarCloseWidgetTrace();
		}
		if(previous_reduced === this.reduced) {
			this.moveColumnPosition();
		}
	}
	
	function moveColumnPosition() {
		this.responsive_small_size = window.innerWidth < RESPONSIVE_WIDTH.SM;
		if(this.responsive_small_size) {
			this.setWidgetSizeSmallScreen();
		} else {
			if(this.fixed_to_bottom) {
				this.report_content_div.style.gridTemplateRows = '1fr '+(this.height_expanded_column + 65)+'px';
				this.report_content_div.style.gridTemplateColumns = 'auto';
				this.report_content_div.style.gap = '0';
			} else {
				this.report_content_div.style.gridTemplateRows = 'auto';
				this.report_content_div.style.gridTemplateColumns = '1fr '+this.width_expanded_column+'px';
				this.report_content_div.style.gap = '0';
			}
		}
		this.adjustWidgetColumnSize();
	}

	function initColumnResizeEvent() {
		// Resize widget column events for desktop and mobile
		const eventStart = (e) => {
			if (e.target.classList.contains('widget-column-handle')) {
				for(let iframeElement of document.getElementsByTagName('iframe')) {
					iframeElement.style.pointerEvents = 'none';
				}
				this.is_handler_dragging = true;
				if(e instanceof MouseEvent) {
					this.previous_mouse_pose = this.fixed_to_bottom?e.clientY:e.clientX;
				} else {
					this.previous_mouse_pose = this.fixed_to_bottom?e.touches[0].clientY:e.touches[0].clientX; 
				}
				document.body.style.cursor = this.fixed_to_bottom?'row-resize':'col-resize';
			}
		};
		const eventMove = (e) => {
			// Don't do anything if dragging flag is false
			if (this.is_handler_dragging) {
				e.preventDefault();
				let offset_mouse_pos=0;
				if(e instanceof MouseEvent) {
					offset_mouse_pos = this.previous_mouse_pose-(this.fixed_to_bottom?e.clientY:e.clientX);
					this.previous_mouse_pose = this.fixed_to_bottom?e.clientY:e.clientX;
				} else {
					offset_mouse_pos = this.previous_mouse_pose-(this.fixed_to_bottom?e.touches[0].clientY:e.touches[0].clientX);
					this.previous_mouse_pose = this.fixed_to_bottom?e.touches[0].clientY:e.touches[0].clientX;
				}
				const active_layout_config = this.getWigetConfigLayout();
				if(this.fixed_to_bottom) {
					this.height_expanded_column = Math.min(Math.max(active_layout_config.min_height,this.widgets_column_div.offsetHeight+offset_mouse_pos),active_layout_config.max_height);
				} else {
					this.width_expanded_column = Math.min(Math.max(active_layout_config.min_width,this.widgets_column_div.offsetWidth+offset_mouse_pos),active_layout_config.max_width);
				}
				this.moveColumnPosition();
			}
		};
		const eventEnd = (e)=> {
			if(this.is_handler_dragging) {
				for(let iframeElement of document.getElementsByTagName('iframe')) {
					iframeElement.style.pointerEvents = 'auto';
				}
				// Turn off dragging flag when user mouse is up
				this.is_handler_dragging = false;
				document.body.style.cursor = 'auto';
			}
		};
		document.addEventListener('mousedown', eventStart);
		document.addEventListener('touchstart', eventStart);
		document.addEventListener('mousemove', eventMove);
		document.addEventListener('touchmove', eventMove);
		document.addEventListener('mouseup', eventEnd);
		document.addEventListener('touchend', eventEnd);
	}
	
	function getWigetConfigLayout() {
		let layout_config = {
			max_width:(document.body.offsetWidth/2-10),
			min_width:350,
			max_height:window.innerHeight/2,
			min_height:window.innerHeight/8,
			body_max_width:''
		}
		switch (this.active_widget_type) {
			case WIDGET.TASK:
			case WIDGET.SCORING:
				layout_config.min_width = 581;
				layout_config.max_width = (window.innerWidth-(2*window.innerWidth/100));
				layout_config.body_max_width = 'unset';
				break;
			case WIDGET.RESOURCES:
			case WIDGET.MESSAGING:
			case WIDGET.COMMENTS:
			case WIDGET.TRASH:
			case WIDGET.CALENDAR:
				layout_config.max_width = 580;
				break;
		}
		return layout_config;
	}
	
	function setMinColumnSize(toggle=false) {
		const active_layout_config = this.getWigetConfigLayout();
		document.body.style.maxWidth = active_layout_config.body_max_width;
		this.min_column_width = active_layout_config.min_width;
		if(toggle && this.width_expanded_column > active_layout_config.max_width) {
			this.width_expanded_column = this.min_column_width;
		} else {
			this.width_expanded_column = Math.min(Math.max(this.min_column_width,this.width_expanded_column),active_layout_config.max_width);
		}
		this.min_column_height = active_layout_config.min_height;
		this.height_expanded_column = Math.min(Math.max(this.min_column_height,this.height_expanded_column),active_layout_config.max_height);
	}
	
	return {
		init,
		reducedExpandColumn,
		moveColumnPosition,
		toggleWidgetItem,
		adjustWidgetColumnSize,
		setWidgetSizeSmallScreen,
		adjustDatasetLabdocsWidth,
		initColumnResizeEvent,
		saveWidgetConfig,
		adjustAndSave,
		setMinColumnSize,
		getWigetConfigLayout,
		reduced:true,
		fixed_to_bottom:false,
		report:report,
		scope:scope,
		widget_config: widget_config,
		allowed_widget_types: []
	}
}
