/* Sommaire
   - Fonctions générales
   - Gestion de TinyMCE
   - Gestion de l'upload des fichiers
   - Fonctions de gestion des paramètres généraux de la mission
   - Gestion du contenu du widget Ressource
   - Gestion des partie de rapport
   - Gestion des labdocs
   - Gestion des menus contextuels + fonction générale de déplacement des items (RD, RP & LD)
*/

/*======================================================================================*/
/*	                                                                                    */
/*          Fonctions générales                                                         */
/*                                                                                      */
/*======================================================================================*/


/**
 * Prepends http:// to a path if required
 */
function resUrl(rd) {
	console.log(rd);
	if (rd.res_type === 'url') {
		return rd.href.match(/^https?:\/\//i) ? rd.href : 'http://' + rd.href;
	}
	return rd.href;
}

// *******************************************************************
// Sauvegarde des LD
// *******************************************************************
async function autoSaveLD() { // cette fonction est lancée toutes les 10 secondes
	console.log("LD auto save");
	// Autosave de TinyMCE
	if (global_edited_ld 
		&& global_tab_ld.hasOwnProperty(global_edited_ld) 
		&& (
			global_tab_ld[global_edited_ld].ld_type == "text" 
			|| global_tab_ld[global_edited_ld].ld_type == "code"
			|| isProcedure(global_edited_ld, 'json')
		)) {
		await saveLocalLDVersion(global_edited_ld);
	}
	// Sauvegarde automatique de tous les LD modifiés
	autoUpdateLDinDB(false);
}

/**
 * Sends a message in order to save a valid rubric in edit mode.
 * @returns {Promise<void>}
 */
async function autoSaveRubric() { // cette fonction est lancée toutes les 10 secondes
	window.dispatchEvent(new CustomEvent('lnb-custom-event', { detail: 'save-rubric' }));
}

/*=================================================================================
/*
/*      Gestion de TinyMCE
/*
/*================================================================================= */

/**
 * Callback pour la gestion des images
 */
function tmce_teacher_file_picker_cb(callback, value, meta) {
	/*if (meta.filetype == 'file') { // Provide file and text for the link dialog
	// put a reference to the callback function
				document.getElementById("lba_upload_form").callback_fn = callback ;
				setUploadForm("tmce_pdf","") ;
			}*/
	if (meta.filetype == 'image') { // Provide image and alt text for the image dialog
		// put a reference to the callback function
		document.getElementById("lba_upload_form").callback_fn = callback ;
		setUploadForm("tmce_img","") ;
	}
}

const tmce_teacher_params = {
	toolbar1: "undo redo | pastetext | formatselect  fontselect fontsizeselect | forecolor bold italic underline strikethrough subscript superscript",
	resize: true,
	height: 400
};

/**
 * Selectionne depuis la div "read" le texte et le renvoie à tinyMCE pour edition
 * @param target : [description|assignment] chaine précisant les champs concernés
 */
function editContent(target){
	$("#lba_"+target+"_edit").html($("#lba_"+target+"_read").html());
	$("#bloc_lba_"+target+"_read").hide();
	$("#bloc_lba_"+target+"_edit").show();
	init_tinyMCE("#lba_"+target+"_edit", null, tmce_teacher_file_picker_cb, tmce_teacher_params );
}

/**
 * Si le target concerné est description|assignment, sauvegarde le contenu de tinyMCE dans la BD et l'affiche en lecture dans une DIV selon le bloc.
 * Si le target concerné est auto_fit, sauvegarde simplement le contenu en BD.
 * @param target : [description|assignment|auto_fit|rubric_broadcast] chaine précisant les champs concernés
 * @param {boolean} update : whether to POST data to the backend or not
 */
function validContent(target, update=true){
	var is_tinymce = !["auto_fit", "rubric_broadcast"].includes(target);
	var str_new_content = "";
	if (is_tinymce) {
		str_new_content = window.tinymce.get("lba_" + target + "_edit").getContent();
		//$("#lba_"+target+"_read").html(str_new_content);
		updateHtmlWithFormulas("#lba_" + target + "_read", str_new_content);
	} else {
		const identifier = "#" + target + ":checked";
		str_new_content = $(identifier).val() === "on" ? "1" : "0";
	}
	if (update) {
		str_new_content = removeEquationsDisplays(str_new_content);
		$.ajax({
			type: "POST",
			url: "/teacher/mission/"+global_id_mission+"/updateField",
			data: {
				field: target,
				value: str_new_content
			}
		})
		.fail(function(msg){ alert("Erreur 2 : "+msg); });
	}
	if (is_tinymce) {
		$("#bloc_lba_" + target + "_edit").hide();
		$("#bloc_lba_" + target + "_read").show();
		$("#lba_" + target + "_edit").html("");
		tinymce.get("lba_" + target + "_edit").remove();
		$("#lba_" + target + "_edit_parent").remove();
	}
	return str_new_content;
}

/**
 * Revient à l'affichage en lecture sur la DIV et efface les éléments générés par tinyMCE
 * @param target : [description|assignment] chaine précisant les champs concernés
 */
function cancelContent(target){
	$("#bloc_lba_"+target+"_edit").hide();
	$("#bloc_lba_"+target+"_read").show();
	$("#lba_"+target+"_edit").html("");
	tinymce.get("lba_"+target+"_edit").remove();
	$("#lba_"+target+"_edit_parent").remove();
}

/*======================================================================================*/
/*	                                                                                    */
/*         Gestion de l'upload des fichiers                                             */
/*                                                                                      */
/*======================================================================================*/

/**
 * Gestion des envois de fichiers en AJAX avec le plugin [libraries/formJQuery/jquery.form.js]
 */
$(document).ready(function(){
	const form = $('#lba_upload_form');

	typesetHtmlWithFormulas('#lba_description_read');
	typesetHtmlWithFormulas('#lba_assignment_read');
	form.on('submit', function(e) {
		e.preventDefault();

		const base = uploadImage(this.querySelector('input[type=file]'));
		console.log(base);
		const processed = (this.querySelector('input[id="lba_res_type"]').value === 'res_doc'
			? base.toFormData()
			: base.resizeImageToFormData());
		processed
			.then(function(formData){
				console.log(formData);
				$.ajax({
					url: "/storage/upload",
					method: "POST",
					data: formData,
					contentType: false,
					processData: false,
					success: function(response){ // traitement de la réponse
						if(!response.res_type) {
							// Ajout d'un fichier dans les ressources
							$('#bloc_rd .item_list').append(response);
						}
						console.log("Fichier déposé :", response) ;
						if (response.res_type === "assignment") {
							$("#lba_assignment_link").children("a").attr("href","/storage/missions/" + global_id_mission + "/resources/" + response.file_name);
							$("#lba_assignment_link").children("a").html("<i>" + response.file_name + "</i>");
							$("#lba_assignment_link").children("i").removeAttr('onclick').off('click').on('click', () => removeLongAssi(response.id_res));
							$('#lba_assignment_link').show();
							$('#lba_add_assignment').hide();
						}
						else if (response.res_type === "tmce_img" || response.res_type === "tmce_pdf") {
							var cb_fn = document.getElementById("lba_upload_form").callback_fn ;
							if (response.res_type === "tmce_img") {
								cb_fn(
									'/storage/missions/' + global_id_mission + '/tinymce/' + response.file_name,
									{alt: response.name.substr(0,response.name.length-4)}
								);
							}
							cb_fn = null;
						}
					},
					error: function(response) {
						console.log("Erreur de transfert :", response);
						if (typeof response.responseJSON !== 'undefined') {
							alert(response.responseJSON);
						} else if (response.status === 413) {
							alert( __("Ce fichier est de trop grande taille pour être accepté.") );
						} else {
							alert( __("Une erreur a eu lieu lors du transfert de votre fichier.") );
						}
					},
					complete: function(){ // important d'effacer tous les champs du formulaire
						$('#lba_file').prop('disabled', false);
						var res_type = $("#lba_res_type").val();
						$('#lba_add_'+res_type).find('i.fa-spinner').hide();
						form.resetForm();
					}
				});
			})
			.catch(function(error){
				alert(error);
			});
	});
});

/**
 * prepare le formulaire d'upload mais ne déclenche rien
 */
function prepareUploadForm(res_type, res_name) {
	console.log(res_type);
	$("#lba_res_type").val(res_type) ;
	$("#lba_res_name").val(res_name) ;
	var extensions = getAllowedUploadType(res_type);
	if (extensions) {
		$("#lba_file").attr('accept', extensions.map(function(x) { return "." + x; }).join(','));
	}
}

/**
 * met à jour les variables envoyées lors d'un envoi de fichier
 * choisit l'action à faire ensuite
 */
function setUploadForm(res_type, res_name){
	prepareUploadForm(res_type, res_name);
	if (res_type === "res_doc") {
		if (uploadFormTreatment()) {
            $("#add_rd_buttons").hide();
            $("#bloc_rd_form_fa_spinner").show();
        }
	}
	else {
        $("#lba_file").trigger("click");
    }
}

// submit le form si besoin
function uploadFormChange() {
	if ($("#lba_res_type").val() === "res_doc") {
		var $file = $("#lba_file")[0];
		if ($("#lba_res_id").val()) {
			$("#rd_add_feedback_upload_" + $("#lba_res_id").val()).html($file.value);
		} else {
			$("#rd_add_feedback_upload").html($file.value);
			if ($('#rd_name').val() === '') {
				$('#rd_name').val($file.value.replace(/^.+[\\\/]/, ''));
			}
		}
	}
	else {
        uploadFormTreatment();
    }
}
function onUpdateSortable(event, table_name) {
	let items;
	if(table_name === 'labdoc') {
		const class_name = event.item.className.split(' ').filter(
			(name) => name.indexOf('ld_item_draggable') === 0
		)[0];
		const toRpNS = getAlpineData('rp_' + event.to.parentElement.dataset.idRp).rpNS;
		items = Array.from(event.to.getElementsByClassName(class_name)).map(((e,index) => {
			let ldNS = getAlpineData('ld_' + e.dataset.idLd).ldNS;
			ldNS.ld.position = index+1;
			return {id: parseInt(e.dataset.idLd), pos: index+1}
		}));
		toRpNS.sortLabdocListByPosition();
	} else if (table_name === 'report_part') {
		items = Array.from(event.to.getElementsByClassName('lba_rp')).map(((e,index) => {
			return {id: parseInt(e.dataset.idRp), pos: index+1}
		}));
		// Update the display of the rp position in rp title
		Array.from(event.to.getElementsByClassName('lba_rp')).forEach((e,index) => {
			document.getElementById('rp_position_'+e.dataset.idRp).innerText = index+1;
		});
	} else if(table_name === 'ressource') {
		items = Array.from(event.to.getElementsByClassName('rd_item_draggable')).map(((e,index) => {
			return {id: parseInt(e.dataset.idRd), pos: index}
		}));
	}
	if(items) {
		$.ajax({
			type: "POST",
			url: "/teacher/mission/"+global_id_mission+"/updateAllItemPos",
			data: {
				table: table_name,
				items_array: items
			}
		})
			.done(function(){ console.log("position updated"); })
			.fail(function(msg){ alert("Erreur 25: "+msg); });
	}

}
function onAddSortable(event, table_name) {
	let items;
	if(table_name === 'labdoc') {
		const class_name = event.item.className.split(' ').filter(
			(name) => name.indexOf('ld_item_draggable') === 0
		)[0];
		const from_rp_id = parseInt(event.from.parentElement.dataset.idRp);
		const to_rp_id = parseInt(event.to.parentElement.dataset.idRp);
		const fromRpNS = getAlpineData('rp_' + from_rp_id).rpNS;
		const toRpNS = getAlpineData('rp_' + to_rp_id).rpNS;
		if (from_rp_id !== to_rp_id) {
			const id_ld = parseInt(event.item.dataset.idLd);
			const shared = (event.item.className.indexOf('_shared') >= 0)
			if (shared) {
				fromRpNS.labdocs_shared = fromRpNS.labdocs_shared.filter((l) => l.id_labdoc !== id_ld);
			} else {
				fromRpNS.labdocs_report = fromRpNS.labdocs_report.filter((l) => l.id_labdoc !== id_ld);
			}
			let ldNS = getAlpineData('ld_' + event.item.dataset.idLd).ldNS;
			toRpNS.reInsertLabdoc(ldNS.ld);
		}
		items = Array.from(event.to.getElementsByClassName(class_name)).map(((e,index) => {
			let ldNS = getAlpineData('ld_' + e.dataset.idLd).ldNS;
			ldNS.ld.position = index+1;
			return {id: parseInt(e.dataset.idLd), pos: index+1, idRp:e.closest('.lba_rp').dataset.idRp}
		})).concat(Array.from(event.from.getElementsByClassName(class_name)).map((e,index) => {
			let ldNS = getAlpineData('ld_' + e.dataset.idLd).ldNS;
			ldNS.ld.position = index+1;
			return {id: parseInt(e.dataset.idLd), pos: index+1, idRp:e.closest('.lba_rp').dataset.idRp}
		}));
		toRpNS.sortLabdocListByPosition();
	}
	if(items) {
		$.ajax({
			type: "POST",
			url: "/teacher/mission/"+global_id_mission+"/updateAllItemPos",
			data: {
				table: table_name,
				items_array: items
			}
		})
		.done(function(){ console.log("position updated"); })
		.fail(function(msg){ alert("Erreur 25: "+msg); });
	}
}
/*
* Initialize sortable resource document
*
* */
function initializeSortingResource(){
	for(const item of document.getElementsByClassName('rd_draggable')) {
		new Sortable(item.parentElement, {
			group: 'rd_nested',
			handle: '.item_handle',
			draggable: '.rd_item_draggable',
			animation: 150,
			fallbackOnBody: true,
			onUpdate: (event) => { onUpdateSortable(event, 'ressource'); },
			swapThreshold: 1
		});
	}
}
/*
* Initialize sortable report part
*
* */
function initializeSortingPart(){
	for(const item of document.getElementsByClassName('part_draggable')) {
		new Sortable(item.parentElement, {
			group: 'part_nested',
			handle: '.item_handle',
			draggable: '.item_draggable',
			animation: 150,
			fallbackOnBody: true,
			onUpdate: (event) => { onUpdateSortable(event, 'report_part'); },
			swapThreshold: 1
		});
	}
}
/*
* Initialize sortable labdoc
*
* */
function initializeSortingLabdoc() {
	for(const type_share of ['shared', 'report']) {
		const suffix = '_' + type_share;
		for(const item of document.getElementsByClassName('ld_draggable' + suffix)) {
			new Sortable(item.parentElement, {
				group: 'nested_ld' + suffix,
				handle: '.item_handle',
				draggable: '.ld_item_draggable'+ suffix,
				animation: 150,
				fallbackOnBody: true,
				onAdd: (event) => { onAddSortable(event, 'labdoc'); },
				onUpdate: (event) => { onUpdateSortable(event, 'labdoc'); },
				swapThreshold: 0.4
			});
		}
	}
}
$(document).ready(
	function(){
		initializeSortingResource();
		initializeSortingPart();
		initializeSortingLabdoc();
	});

function getAllowedUploadType(resourceType) {
	var allow_extension;
	if (resourceType === "assignment" || resourceType === "tmce_pdf") {
		allow_extension = ["pdf"];
	} else if (resourceType === "tmce_img") {
		allow_extension = ["gif", "jpeg", "jpg", "png"];
	} else if (resourceType === "xml_proc") {
		allow_extension = ["xml"];
	} else {
		allow_extension = [];
	}
	return allow_extension;
}

/**
 * Vérifie l'extension et la taille du fichier puis fait l'upload
 * @return: bool
 */
function uploadFormTreatment() {
	var $file = $("#lba_file")[0];
	if (typeof $file.files[0] === "undefined") {
		alert( __("Sélectionnez un fichier en cliquant sur l'icône en forme de trombone.") );
		return false;
	}
	var res_type = $("#lba_res_type").val();
	var file_size = $file.files[0].size;
	var file_name = $file.value;
	var file_parts = file_name.split(".");
	var file_extension = file_parts[(file_parts.length - 1)];
	file_extension = file_extension.toLowerCase();
	var allow_extension = getAllowedUploadType(res_type);
	if (allow_extension.length > 0 && !(allow_extension.includes(file_extension))) {
		var alert_text = "";
		for (var l = allow_extension.length, i = 0; i < l; i++) {
			alert_text += " " + allow_extension[i];
		}
		alert( __("Le format du fichier fourni n'est pas reconnu.\nLes formats autorisés sont : {{format}}", { format: alert_text }) );
		return false;
	}
	if (!$file.files[0].type.startsWith('image/') && file_size >= 8*1024*1024) {
		alert( __("Votre fichier ne peut pas faire plus de 8 Mo.") );
		return false;
	}
	if (res_type === "xml_proc") {
		var xml_file = document.getElementById("lba_file").files[0];
		uploadLD($("#lba_res_name").val(), xml_file); // $("#lba_res_name").val() est l'id du LD (shame !)
	} else {
		$('#lba_add_'+res_type).find('i.fa-spinner').show();
		$("#lba_upload_form").submit();
		$('#lba_file').prop('disabled', true);
        return true;
	}
}


/*======================================================================================*/
/*	                                                                                    */
/*      Fonctions de gestion des paramètres généraux de la mission
/*                                                                                      */
/*======================================================================================*/

// Met à jour dans la base les valeurs de code et de nom de la mission
function updateInput(type, name, min_size, max_size) {
	if (!ATControlString($("#m_"+type), name, min_size, max_size, true)) {
		$("#m_"+type).val($("#m_"+type+"_save").text());
		return ;
	}
	var text = $("#m_"+type).val();
	$.ajax({
		type: "POST",
		url: "/teacher/mission/"+global_id_mission+"/updateField",
		data: {
			field: type,
			value: text,
		}
	})
	.done(function(msg) {
		if (msg) {
			alert(msg) ;
			$("#m_"+type).val($("#m_"+type+"_save").text()); // récupère la valeur de la variable de sauvegarde
		}
		else { $("#m_"+type+"_save").text(text); } // met à jour la variable de sauvegarde
	})
	.fail(function(msg){ alert("Erreur 3 : "+msg); });
}


/**
 * Mise à jour du statut des mission
 */
function updateStatus(){
	var m_status = $("#m_status option:selected").val();
	$.ajax({
        type: "POST",
         url: "/teacher/mission/"+global_id_mission+"/updateField",
        data: {
			field: 'status',
			value: m_status,
		}
    })
    .done(function(msg){if (msg) {alert(msg); } })
    .fail(function(msg){ alert("Erreur 4 : "+msg); });
}

/**
 * message d information sur les enseignants associes
 */
function messageLinkedTeacher(openMethod){
	let msg_title, msg_content;
	msg_title    =  __('Enseignant(s) associé(s)');
	msg_content  = __('Vous pouvez associer des enseignants à vos missions. Attribuez leur un des deux statuts suivants') + __("&nbsp;:") + '<br/><br/>';
	msg_content += "<b>" + __("Tuteur") + __("&nbsp;:") + "</b> " + __("peut affecter la mission à des équipes d'étudiants et suivre les rapports des étudiants de ses classes pour cette mission.") + "<br/><br/>";
	msg_content += "<b>" + __("Concepteur") + __("&nbsp;:") + "</b> " + __("peut modifier la mission (en plus des droits du tuteur).") + "<br>";
	global_tooltip_popup.openTooltipPopup(msg_title, msg_content,"#teachers_tooltip",openMethod);
}

/**
 * fonction linkTeacherFromName
 * récupère le nom d'un enseignant et tente de l'ajouter à la liste des enseignants associés à une mission
 * ne fais rien si l'enseignant n'a pas été trouvé ou si l'enseignant est déjà dans la liste
 */

function linkTeacher(selectedUserOrTeam){
	if(selectedUserOrTeam) {
		if(selectedUserOrTeam.type === "user"){
			$.ajax({
				type: "POST",
				url: "/teacher/mission/"+global_id_mission+"/linkTeacher",
				data: {
					id_teacher: selectedUserOrTeam.id
				},
			}).done(function(data) {
				if(data !== ""){
					$("#linked_teachers").html(data);
				}
				else { 
					alertGently( __("L'enseignant n'a pas pu être ajouté à la mission"), 'error' );
				}
			}).fail(function(msg) {
				alert("Erreur 12 : "+msg);
			});
		} else {
			$.ajax({
				type: "POST",
				url: "/teacher/mission/"+global_id_mission+"/linkTeacherTeamFromId",
				data: {
					id_teacher_team: selectedUserOrTeam.id
				},
			}).done(function(data) {
				if(data !== ""){
					$("#linked_teachers").html(data);
				}
				else {
					alertGently( __("L'équipe pédagogique n'a pas pu être ajouté à la mission"), 'error' );
				}
			}).fail(function(msg) {
				alert("Erreur 12 : "+msg);
			});
		}
	}
}


/**
 * Mise a jour du type d association [enseignant|concepteur] au changement dans la liste deroulante
 * @param id_teacher int (id de l enseignant a modifier)
 * @param allow_self_delete boolean
 */
function updateLinkedTeacher(id_teacher, allow_self_delete=false){
	var teacher_type = $("#tr_"+ id_teacher+" select option:selected").val();
	var switch_self_to_tutor = id_teacher == global_current_id_user && teacher_type == "teacher";
	if (switch_self_to_tutor && !allow_self_delete) {
		openUnlinkSelfPopup('change');
		return ;
	}
	$.ajax({ // Mise à jour de la table link_mission_teacher, suppression de l'ID teacher récupéré
		type: "POST",
		url: "/teacher/mission/"+global_id_mission+"/updateLinkedTeacher",
		data: {
			id_teacher: id_teacher,
			teacher_type: teacher_type
		},
		error: function(jqXHR) {
			alertGently(jqXHR.responseJSON.message, 'danger');
		}
	})
	.done(function(data){
		if (data) {
			$("#linked_teachers").html(data);
			if (switch_self_to_tutor) {
				window.location = '/teacher/missions';
			}
		}
	});
}

/**
 * Suprime l'association d'un enseigant depuis la liste
 * @param id_teacher int (id de l'enseignant)
 * @param id_mission int (id de la mission)
 * @param allow_self_delete boolean
 */
function removeLinkedTeacher(id_teacher, id_mission, allow_self_delete=false){
	var self_delete = id_teacher == global_current_id_user;
	if (self_delete && !allow_self_delete) {
		openUnlinkSelfPopup('unlink');
		return ;
	}
	$.ajax({ // Mise à jour de la table link_mission_teacher, suppression de l'ID teacher récupéré
		type: "POST",
		url: "/teacher/mission/"+id_mission+"/removeLinkedTeacher",
		data: {
			id_teacher: id_teacher
		},
		error: function(jqXHR) {
			alertGently(jqXHR.responseJSON.message, 'danger');
		}
	})
	.done(function(data){
		if (data) {
			$("#linked_teachers").html(data);
			window.dispatchEvent(new CustomEvent('lnb-select2-remove-id', {
				detail:{
					selector: "mi-add-teacher",
					idElement: id_teacher
				}}));
			if (self_delete) {
				window.location = '/teacher/missions';
			}
		}
	});
}

/*======================================================================================*/
/*	                                                                                    */
/*              Gestion du contenu du widget Ressource                                  */
/*                                                                                      */
/*======================================================================================*/

/**
 * Suppression de l enregistrement de la consigne détaillée
 */
function removeLongAssi(id_ressource){
	if( confirm( __('Voulez-vous supprimer définitivement la consigne détaillée ?') ) ){
		$.ajax({
			type: "POST",
			url: "/resource/"+id_ressource+"/removeFromMission",
		})
		.fail(function(msg){ alert("Erreur 10 : "+msg); })
		.done(function(data){
			$('#lba_assignment_link').hide();
			$('#lba_add_assignment').show();
		});
	}
}
// Ouvre la fenetre de sélection du fichier
function selectRDFile(id_rd) {
	prepareUploadForm('res_doc', '');
	$("#lba_res_type").val("res_doc") ; // nécessaire pour lancer le retour sur le nom de fichier sélectionné
	$("#lba_res_id").val(id_rd) ; // nécessaire pour lancer le retour sur le nom de fichier sélectionné
	$('#lba_file').trigger('click') ;
}

/**
 * Ajoute un nouveau document ressource fichier ou lien et met a jour la liste
 */
function addRD(){
	if (!ATControlString($('#rd_name'), "Nom", 1, 64, true)) {
        return true;
    }
	var rd_name = $('#rd_name').val();
	if ( $('input[name=rd_type]:checked').val() == "link" ){ // cas de l envoie d une URL
		if (!ATControlString($('#rd_url'), "Adresse", 1, 255, true)) {
            return true;
        }
		var rd_url = $('#rd_url').val();
		$.ajax({
			type: "POST",
			url: "/teacher/mission/"+global_id_mission+"/addURLRD",
			data: {
				name: rd_name,
				res_path: rd_url
			}
		})
		.fail( function(msg){ alert("Erreur 11: "+msg); } )
		.done( function(data){
			$('#bloc_rd .item_list').append(data);
		});
		return false;
	} else { // on est dans le cas d'un ajout de fichier - on passe par le formulaire
		setUploadForm('res_doc',rd_name);
		return false;
	}
}
/**
 * Mise a jour de la ressource selectionne
 * @param rd : la ressource a mettre a jour
 * @return boolean : should we keep the rd edition mode
 */
async function updateRD(rd){
	if (!ATControlString($("#rd_edit_name_"+rd.id_ressource), "Nom", 1, 64, true)) { return true; }
	if (rd.res_type === 'url') {
		if (!ATControlString($("#rd_edit_url_"+rd.id_ressource), "Adresse", 1, 255, true)) { return true; }
	} else if (typeof $("#lba_file")[0].files[0] != "undefined") {
		uploadFormTreatment();
		$(`#rd_${rd.id_ressource}`).remove();
		return false;
	}
	return new Promise((resolve, reject) => {
		$.ajax({
			type: "POST",
			url: "/resource/"+rd.id_ressource+"/updateFromMission",
			data: {
				id_ressource: rd.id_ressource,
				name: rd.name,
				url: rd.res_path,
			}
		}).done(function(data) {
			rd.name = data.name;
			if (rd.res_type === 'url') {
				rd.href = data.res_path;
			}
			resolve(false);
		}).fail(function(error) {
			alertGently(error.responseJSON.message, 'danger');
			resolve(true);
		});
	});
}

/**
 * Suppression d un ressource avec gestion du positionnement des ressources suivantes
 * @param rd : selected rd
 */
function removeRD(rd){
	if ( confirm( __("Souhaitez-vous supprimer ce document ?") ) ){
		$.ajax({
			type: "POST",
			url: "/resource/"+rd.id_ressource+"/removeFromMission",
		})
		.fail(function(msg){ alert("Erreur 14: "+msg); })
		.done(function(data){
			$("#rd_"+rd.id_ressource).remove();
		});
	}
}

/*======================================================================================*/
/*	                                                                                    */
/*        Gestion des parties de rapport                                                 */
/*                                                                                      */
/*======================================================================================*/

/**
 * Avoid to open 2 different actions on RP or LD
 */
function isActionOn() {
	if ($("#ld_import").is(":visible")) {
		alert("Un labdoc est en cours d'ajout.\nMerci de valider ou annuler cet ajout avant de faire une autre action.") ;
		return true ;
	}
	if ($(".ld_edit").is(":visible")) {
		alert( __("Un labdoc est en cours de modification.\nMerci de valider ces modifications avant de faire une autre action.") ) ;
		return true ;
	}
	if ($(".rp_edit").is(":visible")) {
		alert( __("Une partie de rapport est en cours de modification.\nMerci de valider ou annuler ces modifications avant de faire une autre action.") ) ;
		return true ;
	}
	return false ;
}

window.reportPartCtrl = function (rp, is_edit_mode, is_show_assignment, is_show_import) {
	function init () {
		if(rp.id_report_part===0) {
			// Initialize a new report part
			this.rp.title='';
			this.rp.assignment='';
			this.rp.text=1;
			this.rp.drawing=1;
			this.rp.dataset=1;
			this.rp.procedure=1;
			this.rp.code=1;
		}
		this.labdocs_shared = [];
		this.labdocs_report = [];
	}

	function initLabdocs(shared, report) {
		this.labdocs_shared = shared;
		this.labdocs_report = report;
	}

	function reInsertLabdoc(ld) {
		// inset the labdoc in the other list if necessary
		if((ld.shared && !this.labdocs_shared.some((l) => parseInt(l.id_labdoc) === parseInt(ld.id_labdoc)))
			|| (!ld.shared && !this.labdocs_report.some((l) => parseInt(l.id_labdoc) === parseInt(ld.id_labdoc)))) {
			this.labdocs_shared = this.labdocs_shared.filter((l) => parseInt(l.id_labdoc) !== parseInt(ld.id_labdoc));
			this.labdocs_report = this.labdocs_report.filter((l) => parseInt(l.id_labdoc) !== parseInt(ld.id_labdoc));
			let dest = this.labdocs_report;
			if (ld.shared) {
				dest = this.labdocs_shared;
			}
			ld.position = dest.length+1;
			dest.push(ld);
		}
	}
	
	function sortLabdocListByPosition(){
		this.labdocs_shared = this.labdocs_shared.sort((a,b) => {return a.position - b.position});
		this.labdocs_report = this.labdocs_report.sort((a,b) => {return a.position - b.position});
	}

	function savePreviousVersion() {
		this.previous_rp = JSON.parse(JSON.stringify(this.rp));
	}
	
	function getBackPreviousVersion() {
		this.rp = JSON.parse(JSON.stringify(this.previous_rp));
		this.previous_rp = null;
	}
	/**
	 * Valid the current edited rp
	 */
	function validCurrentEditRp(new_rp) {
		this.is_show_import = false;
		if(this.is_edit_mode && !new_rp){
			this.validateEditRP(this.rp);
			this.is_edit_mode=false;
		}
	}
	function switchToEditMode() {
		if (!this.is_edit_mode ) { 
			validCurrentEditedLD(); 
			validCurrentEditedRP(); 
			validCurrentEditedLDAssignment();
			this.savePreviousVersion();
			this.is_edit_mode = true; 
			this.is_show_assignment=false;
		}
	}
	function checkIfRPIsValid() {
		if(this.rp.title.trim().length < 3 || this.rp.title.trim().length > 255){
			alertGently(this.rp.title.trim().length < 3?__('Le titre d’une partie doit être renseigné et d’au moins 3 caractères.'):__('Le titre d’une partie ne doit pas excéder 255 caractères')
				,'warning',2000);
			document.getElementById(`rp_edit_title_${this.rp.id_report_part}`).focus();
			return false;
		} else {
			return true;
		}
	}
	/**
	 * Ajout ou modifie une partie de rapport
	 * rp.id_report_part = 0 si c'est un ajout from scratch et > 0 si c'est une modif
	 * Mise a jour de la BD et de la liste affiche
	 */
	function validateEditRP(rp){
		rp.title = rp.title.trim();
		if(rp.title.length > 255) {
			rp.title = rp.title.substring(0,255);
			alertGently(__('Le titre d’une partie a été réduit automatiquement'),'warning',2000);
		}
		let rp_assi;
		try {
			rp_assi = window.tinymce.get(`rp_edit_assi_${rp.id_report_part}`).getContent().trim();
			rp_assi = removeEquationsDisplays(rp_assi);
		} catch (ex) {
			rp_assi = null;
			let err_msg = __("Erreur de récupération du texte de la consigne");
			alertGently(err_msg, "danger");
			logError(err_msg, 'danger', ex.message);
		}
		if (rp_assi === null) {
			return;
		} 
		rp.assignment = rp_assi;
		let is_new_rp = rp.id_report_part === 0;
		this.closeEditAssignment();
		$.ajax({
			type: "POST",
			url: "/teacher/mission/"+global_id_mission+"/editRP",
			data: {
				id_rp: rp.id_report_part || 0,
				title: rp.title,
				text: rp.text?1:0,
				drawing: rp.drawing?1:0,
				dataset: rp.dataset?1:0,
				procedure: rp.procedure?1:0,
				code: rp.code?1:0,
				assignment: rp_assi
			}
		})
		.fail( function(msg){ alert("Erreur 15: "+msg); } )
		.done( function(data){
			if(is_new_rp) {
				// Ajout d'une report part
				$('#rp_list').append(data);
				// Append can take a few milisecods, if we init the sortable to soon it won't work
				setTimeout(function() {
					initializeSortingPart();
					initializeSortingLabdoc();
				}, 100);
			}
		})
	}

	/**
	 * Importe un labdoc dans la BD et met à jour l'affichage depuis les données transmises.
	 */
	function validateImportLD(id_ld){
		this.is_show_import = false;
		this.duplicateLD(id_ld);
	}

	/**
	 * Duplique le labdoc dans la Partie
	 * @param int : id du labdoc source pour la duplication
	 */
	function duplicateLD(id_ld){
		validCurrentEditedLD();
		validCurrentEditedRP();
		validCurrentEditedLDAssignment();
		$.ajax({
			type: "POST",
			url: "/teacher/mission/"+global_id_mission+"/duplicateLD",
			data: {
				id_labdoc: id_ld,
				id_rp: this.rp.id_report_part,
			}
		})
			.fail(function(msg){ alertGently(msg.responseJSON.message); })
			.done((function(data){
				this.reInsertLabdoc(data);
			}).bind(this));
	}

	
	/**
	 * Créer une instance tinymce pour editer la consigne
	 */
	function initEditAssignment(id_report_part){
		init_tinyMCE(`#rp_edit_assi_${id_report_part}`, null, tmce_teacher_file_picker_cb, tmce_teacher_params);
	}

	/**
	 * Annule l operation d ajout d'une partie de rapport et restaure l affichage
	 */
	function closeEditAssignment(){
		tinymce.get("rp_edit_assi_" + this.rp.id_report_part).remove();
	}
	
	/**
	 * Suppression d une RP et de ses LD
	 */
	function removeRP(rp){
		var msg = __("ATTENTION !") +"\n\n" + __("La suppression d'une partie de rapport, entraine la suppression des labdocs inclus dans cette partie") ;
		if (global_nb_reports>0) { msg +=",\n" + __("y compris ceux créés par les étudiants") ; }
		msg +=".\n" + __("Souhaitez-vous supprimer la partie de rapport et son contenu ?") ;
		if (confirm(msg)){
			$("#rp_"+rp.id_report_part).hide();
			$.ajax({
				type: "POST",
				url: "/teacher/mission/"+global_id_mission+"/removeRP",
				data: {
					id_rp: rp.id_report_part,
				}
			})
				.fail(function(msg){ alert("Erreur 16: "+msg);$("#rp_"+rp.id_report_part).show(); })
				.done(function(data){
					$("#rp_"+rp.id_report_part).remove();
					// Update the rp positions
					Array.from(document.getElementsByClassName('lba_rp')).forEach((e,index) => {
						document.getElementById(`rp_position_${e.dataset.idRp}`).innerText = index+1;
						e.dataset.position = index;
					});
				});
		}
	}

	/**
	 * Suppression d un labdoc de partie et gestion de la maj du positionnement des LD suivants
	 */
	function contextMenuRemoveLD(ld, is_new_ld){
		if (isActionOn()) { return ; }
		var txt_confirm = "";
		if (global_nb_reports>0) { txt_confirm="\n" +__("NB : ce labdoc ne sera supprimé des {{tot_nb_reports}} rapports étudiants en cours que si il n'a pas été modifié par les étudiants", { tot_nb_reports : global_nb_reports}) ; }
		if ( confirm( __("Souhaitez-vous réellement supprimer ce labdoc ?") +txt_confirm) ){
			this.removeLD(ld, is_new_ld);
		}
	}
	function removeLD(ld, is_new_ld) {
		if (ld.shared) {
			this.labdocs_shared = this.labdocs_shared.filter((l) => l.id_labdoc != ld.id_labdoc);
		} else {
			this.labdocs_report = this.labdocs_report.filter((l) => l.id_labdoc != ld.id_labdoc);
		}
		$.ajax({
			type: "POST",
			url: "/teacher/mission/"+global_id_mission+"/removeLD",
			data: {
				id_labdoc: ld.id_labdoc,
			}
		})
			.fail(function(msg){ alert("Erreur 24: "+msg); })
			.done(function(data){
				if (global_nb_reports>0 && !is_new_ld) { 
					alertGently ( __("Le labdoc a été supprimé dans {{nb_report}} des {{tot_nb_report}} rapports d'étudiants en cours",
						{ nb_report : data, tot_nb_report : global_nb_reports}), 'warning' ); 
				}
			});
	}

	/**
	 * Ajoute un labdoc dans la BD et met à jour l'affichage depuis les données transmises.
	 */
	function validateAddLD(rp, ld_type=''){
		validCurrentEditedLD(); 
		validCurrentEditedRP();
		validCurrentEditedLDAssignment();
		$.ajax({
			type: "POST",
			url: "/teacher/mission/"+global_id_mission+"/validateAddLD",
			data: {
				id_rp: rp.id_report_part,
				type_labdoc: ld_type,
				name: '',
				position: 'last',
				id_user: global_current_id_user,
			}
		})
		.fail(function(msg){ alert("Erreur 17: "+msg); })
		.done((data) => {
			data.ld.shared_in_db = 0;
			this.labdocs_report.push(data.ld);
			initializeSortingLabdoc();
		});
	}
	/**
	 * Import LD dans un RP
	 */

	function getImportLD(id_rp){
		closeImportLdMenu();
		$.ajax({
			type: "GET",
			url: "/teacher/mission/"+global_id_mission+"/getImportLDHTML/"+id_rp,
		})
			.done(function(html){
				$("#import_ld_"+id_rp).html(html);
				scrollToDisplay("#import_ld_"+id_rp);
			});
	}
	
	return {
		init,
		savePreviousVersion,
		getBackPreviousVersion,
		checkIfRPIsValid,
		validCurrentEditRp,
		switchToEditMode,
		initEditAssignment,
		initLabdocs,
		closeEditAssignment,
		validateEditRP,
		validateImportLD,
		removeRP,
		validateAddLD,
		getImportLD,
		removeLD,
		duplicateLD,
		contextMenuRemoveLD,
		reInsertLabdoc,
		sortLabdocListByPosition,
		rp: rp,
		previous_rp:null,
		is_edit_mode: is_edit_mode,
		is_show_assignment: is_show_assignment,
		is_show_import: is_show_import,
	}
};
/*======================================================================================*/
/*	                                                                                    */
/*         Gestion des labdocs                                                          */
/*                                                                                      */
/*======================================================================================*/
/*
	Fonctions de download et d'upload du code XML pour les procedures
*/
function uploadLD(id_ld, xml_file){
	// récupération du fichier sélectionné au format XML
	var reader = new FileReader();
	reader.onload = function() {
		var xml_content = reader.result;
		if (xml_content.substr(0,5) == "<?xml"){
			$.ajax({
				type: "POST",
				url: "/teacher/mission/"+global_id_mission+"/updateLDXMLCode",
				data: {
					id_labdoc: id_ld,
					labdoc_data: xml_content,
				}
			})
				.error(function(msg){
					alert("Erreur 20: "+msg.responseJSON);
				})
				.done(function() {
					getAndDisplayLDContent(id_ld, 1, 0, 1, 1);
				});
		}
	};
	reader.readAsText(xml_file, 'UTF-8');
}

window.labdocCtrl = function (ld, is_new_ld, is_edit_mode, is_displayed, is_edit_mode_assignment) {
	function init () {
		if(this.ld.name===null) {
			this.ld.name='';
		}
	}

	/**
	 * Insere un bloc d'edition d'un labdoc a la place de celui-ci dans la liste.
	 */
	function contextMenuEditLD(){
		validCurrentEditedLD();
		validCurrentEditedRP();
		validCurrentEditedLDAssignment();
		this.is_displayed = true;
		this.is_edit_mode = true;
		this.is_edit_mode_assignment = false;
		getAndDisplayLDContent(this.ld.id_labdoc, 1, 0, 1, 1);
		global_edited_ld = this.ld.id_labdoc ;
	}

	function closeTinyMce(id_ld, is_assignment) {
		const suffix = is_assignment ? '_assignment' : '';
		const id = 'ld_txt_' + ld.id_labdoc + suffix;
		const tmce = window.tinymce.get(id)
		if (tmce) {
			tmce.remove();
			$(id+" .mce_container").remove();
		}
	}

	function editLDAssignment() {
		this.is_edit_mode_assignment = true;
		displayText(this.ld.id_labdoc, 1, 1);
	}

	async function validateEditLDAssignment() {
		await saveLocalLDVersion(this.ld.id_labdoc)
		closeTinyMce(this.ld.id_labdoc, true);
		this.ld.assignment = global_tab_ld[this.ld.id_labdoc].ld_assignment_history[0];
	}
	
	function checkIfLDIsValid() {
		if(this.ld.name.trim().length < 3 || this.ld.name.trim().length > 127){
			alertGently(this.ld.name.trim().length < 3?__('Le titre d\'un labdoc doit être renseigné et d’au moins 3 caractères.'):__('Le titre d’une partie ne doit pas excéder 127 caractères')
				,'warning',2000);
			document.getElementById(`ld_name_${this.ld.id_labdoc}`).focus();
			return false;
		} else {
			return true;
		}
	}
	/**
	 * Send to DB the attributes of the modified LD
	 * @param {labdoc} ld
	 */
	async function validateEditLD(rpNS){
		this.ld.name = this.ld.name.trim();
		if(this.ld.name.length > 127) {
			this.ld.name = this.ld.name.substring(0,127);
			alertGently(__('Le titre d\'un labdoc a été réduit automatiquement'),'warning',2000);
		}
		if (this.ld.type_labdoc === 'text' || this.ld.type_labdoc === 'code' || isProcedure(this.ld.id_labdoc, 'json')) {
			await saveLocalLDVersion(this.ld.id_labdoc) ; // sauvegarde le contenu (qui n'est pas sauvegardé à chaque clic)
			if (this.ld.type_labdoc === 'text'){
				closeTinyMce(this.ld.id_labdoc, false);
			}
		}
		// recuperation du contenu
		let ld_content = getLDContentFromLocalHistory(this.ld.id_labdoc) ;
		if (!ld_content) {
			ld_content = "";
		} else if(ld.type_labdoc === 'text') {
			ld_content = removeEquationsDisplays(ld_content);
		}
		await this.validateEditLDAssignment();
		let ld_assignment = getLDContentFromLocalHistory(this.ld.id_labdoc, 0, 1) ;
		if (!ld_assignment) {
			ld_assignment = "";
		}
		closeTinyMce(this.ld.id_labdoc, true);
		this.is_edit_mode=false;
		this.is_edit_mode_assignment=false;
		this.is_new_ld = false;
		this.is_displayed = false;
		let self = this;
		$.ajax({
			type: "POST",
			url: "/teacher/mission/"+global_id_mission+"/validateEditLD",
			data: {
				id_labdoc: this.ld.id_labdoc,
				name: this.ld.name,
				editable: this.ld.editable?1:0,
				editable_name: this.ld.editable_name?1:0,
				duplicatable: this.ld.duplicatable?1:0,
				deleteable: this.ld.deleteable?1:0,
				draggable: this.ld.draggable?1:0,
				shared: this.ld.shared?1:0,
				last_editor: global_current_id_user,
				labdoc_data: ld_content,
				assignment: ld_assignment,
			}
		}).done((data) => {
			if (global_nb_reports>0) {
				alertGently( __("{{nb_reports}} rapport(s) d'étudiants sur {{tot_nb_reports}} ont été mis à jour",
					{ nb_reports : data.nb_modified_reports, tot_nb_reports : global_nb_reports }), "info");
			}
			global_edited_ld = 0 ;
			self.ld.shared_in_db = self.ld.shared;
			self.ld.is_new_ld = 0;
			global_tab_ld[self.ld.id_labdoc].send_in_db = false;
			rpNS.reInsertLabdoc(self.ld);
		}).fail(function(msg) {
			alert("Erreur 19: "+msg);
		});
	}
	function displayLDContent(id_labdoc=null) {
		if(this.is_displayed){
			getAndDisplayLDContent(id_labdoc ?? this.ld.id_labdoc, 0, 1, 0, 0);
		}
	}

	function downloadLD(id_ld){
		$.ajax({
			type: "POST",
			url: "/teacher/mission/"+global_id_mission+"/getLDXMLCode",
			data: {
				id_labdoc: id_ld
			}
		})
			.fail(function(msg){ alert("Erreur 21: "+msg); })
			.done(function(data) {
				if (data) {
					$('<a></a>', {
						"download": "labdoc_"+ id_ld + ".xml",
						"href": "data:text/plain;charset=UTF-8," + encodeURIComponent(data),
						"id": "ld_export"
					}).appendTo("body")[0].click();
					$("#ld_export").remove();
				}
			});
	}
	
	return {
		init,
		contextMenuEditLD,
		editLDAssignment,
		checkIfLDIsValid,
		validateEditLD,
		validateEditLDAssignment,
		displayLDContent,
		downloadLD,
		ld: ld,
		is_new_ld: is_new_ld,
		is_edit_mode: is_edit_mode,
		is_edit_mode_assignment: is_edit_mode_assignment,
		is_displayed: false,
	}
};
function hideAllLds() {
	window.dispatchEvent(new CustomEvent('lnb-hide-ld-event', { detail: 'hide_ld' }));
}

function validCurrentEditedLD(){
	window.dispatchEvent(new CustomEvent('lnb-valid-current-edited-ld', { detail: 'valid_ld' }));
}

function validCurrentEditedRP() {
	window.dispatchEvent(new CustomEvent('lnb-valid-current-edited-rp',{ detail: 'valid_rp' }));
}

function validCurrentEditedLDAssignment() {
	window.dispatchEvent(new CustomEvent('lnb-valid-current-edited-ld-assignment',{ detail: 'valid_ld_assignment' }));
}

function closeImportLdMenu() {
	window.dispatchEvent(new CustomEvent('lnb-close-ld-import-menu',{ detail: 'close_import_menu' }));
}

/**
 * Suppression d un labdoc du menu et maj de l affichage
 */
function contextMenuRemoveLDm(event){
	if ( confirm( __("Souhaitez-vous réellement supprimer ce labdoc ?") ) ){
		var id_ld = $(event.target).closest("span").attr("id_target"); // id du labdoc selectionne
		var $ld_selected = $("#ld_"+id_ld);
		$.ajax({
			type: "POST",
			url: "/teacher/mission/"+global_id_mission+"/removeLD",
			data: {
				id_labdoc: id_ld,
			}
		})
		.fail(function(msg){ alert("Erreur 23: "+msg); })
		.done(function(data){ $ld_selected.remove(); });
	}
}



/*==============================================================================================*/
/*	                                                                                            */
/*     Gestion des menus contextuels + fonction générale de déplacement des items (RD, RP & LD)
/*                                                                                              */
/*==============================================================================================*/

/**
 * Fermeture du menu contextuel au click dans la page
 */
$(document).ready(function(){
	defineModal("modal-mission-unlink-self", "", true, null);
	$(".popup").on("click", ".popup-cancel", function(event) {
		event.stopPropagation();
		var modal = $(this).closest(".popup");
		modal.dialog("close");
		if (modal.data('reset-input')) {
			$("form", modal)[0].reset();
			//TODO bizarre cette ligne
			$("#modal-mission-unlink-self i.fa-check").addClass("disabled");
		}
	});
	$("#teachers_tooltip").on("mouseover", function (){
		messageLinkedTeacher("mouseover");
	})
 let teachers = [];
	$('#linked_teachers div.teacher_line').each(function(){
		teachers.push(this.id.replace('tr_', ''))
	})
	load_add_users ('mi-add-teacher', '', __("Nom d'utilisateur ou équipe péda"), 'teacher', 1, -1, teachers);
});

/**
 * Ouvre le popup de confirmation avant de rennoncer à ces droits sur la mission
 * @param action string : 'unlink'|'change' dans le premier cas on se désassocie complétement de la mission dans le second on deviens tuteur
 */
function openUnlinkSelfPopup(action) {
	if (action == 'unlink') {
		global_confirmation_popup.openConfirmationPopup( __("Ne plus être associé à cette mission"), __("En cochant cette case je confirme ne plus vouloir être associé à cette mission"), function(id_user){
			removeLinkedTeacher(id_user, global_id_mission, true);
		});
	} else {
		global_confirmation_popup.openConfirmationPopup(  __("Ne plus être concepteur de cette mission"), __("En cochant cette case je confirme ne plus vouloir être concepteur de cette mission"), function (id_user){
			updateLinkedTeacher(id_user, true);
		});
	}
}
/*==============================================================================================*/
/*	                                                                                            */
/*     Gestion des Evaluations                                                                  */
/*                                                                                              */
/*==============================================================================================*/
function toggleSection(selectorParent, selectorChild) {
	$(selectorParent + ' .toggle-section').toggleClass('fa-caret-right').toggleClass('fa-caret-down');
	$(selectorChild).toggle();
}
