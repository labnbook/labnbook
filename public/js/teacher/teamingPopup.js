// Constants
const TEAM_CHOICE_METHOD = {
	RANDOM : 3,
	ONLY_TEACHER : 2,
	STUDENT_CHOICE : 1,
	NO_METHOD: 0
}

const REPORT_STATUS = {
	NEW : "new",
	STARTED : "on",
	WAIT : "wait",
	ARC : "arc",
	TEST : "test"
}

const MAX_NB_TEAMS = 50000;
const SAVED = "SAVED";

var teamConfigPopupActions = function() {
	var _teamConfig;
	var _selectedClass;
	var _selectedMissionId;
	var _teamConfigHasChanged = false;
	var _postHook

	// build datetime from date + time (start and end)
	function cleanDateTime(){
		var start_date = normalizeDate($("#tc-start-date").val());
		var start_time = $("#tc-start-time").val();

		var end_date = normalizeDate($("#tc-end-date").val());
		var end_time = $("#tc-end-time").val();

		if (!start_date) {
			start_time = null;
		} else if (!start_time) {
			start_time = "00:00";
		}

		if (!end_date) {
			end_time = null;
		} else if (!end_time) {
			end_time = "23:59";
		}

		_teamConfig.start_datetime = start_date ? start_date + " " + start_time + ":00" : null;
		_teamConfig.end_datetime = end_date ? end_date + " " + end_time + ":59" : null;
	}
	// Split datetime from BDD in date + time
	function splitDateTime(){
		if (_teamConfig.start_datetime) {
			_teamConfig.start_date = _teamConfig.start_datetime.substring(0, 10);
			_teamConfig.start_time = _teamConfig.start_datetime.substring(11, 16);
		} else {
			_teamConfig.start_date = '';
			_teamConfig.start_time = '00:00';
		}

		if (_teamConfig.end_datetime) {
			_teamConfig.end_date = _teamConfig.end_datetime.substring(0, 10);
			_teamConfig.end_time = _teamConfig.end_datetime.substring(11, 16);
		} else {
			_teamConfig.end_date = '';
			_teamConfig.end_time = '23:59';
		}
	}
	// Clean the team config popup
	function cleanPopup(){
		$("#tc-mission").val(0);
		$("#tc-reuse-team-choice-mission").val(0);
		$("#tc-options").hide();
		$("#tc-students").hide();
		$("#tc-future-students").hide();
		$("#tc-reuse").hide();
		$("#tc-students-random-panel").hide();
		$("#tc-popup .popup-submit i").addClass("disabled");
		$("#tc-students-number").text("");
		$("#tc-delete-teaming").hide();
		$("#tc-add-external-learner").val("");
		displayDefaultSettings();
	}
	// Display loaded settings on the interface
	function displaySettingsFromData(){
		splitDateTime();

		$("#tc-start-date").val(_teamConfig.start_date);
		$("#tc-start-time").val(_teamConfig.start_time);
		$("#tc-end-date").val(_teamConfig.end_date);
		$("#tc-end-time").val(_teamConfig.end_time);

		$("#tc-report-prefix").val(_teamConfig.name_prefix);

		if(_teamConfig.allow_import && _teamConfig.allow_import_id_mission == null){
			$("#tc-import-ld").val(LABDOC_IMPORT.ALL_MISSIONS_ALLOWED);
		}else{
			if(_teamConfig.allow_import_id_mission == null){
				$("#tc-import-ld").val(LABDOC_IMPORT.NOT_ALLOWED);
			} else {
				$("#tc-import-ld").val(_teamConfig.allow_import_id_mission);
			}
		}

		if(_teamConfig.allow_msg_id_class!=null || _teamConfig.allow_msg_id_mission != null || _teamConfig.allow_msg_team || _teamConfig.allow_msg_teacher) {
			$("#tc-messaging-options").show();
			$('input[name="tc-allow-msg"][value="1"]').prop("checked", true);
		} else {
			$("#tc-messaging-options").hide();
			$('input[name="tc-allow-msg"][value="0"]').prop("checked", true);
		}

		if(_teamConfig.allow_msg_id_class!=null){
			$("#tc-msg-class").val(_teamConfig.allow_msg_id_class);
		}else {
			$("#tc-msg-class").val(0);
		}
		if(_teamConfig.allow_msg_team){
			$('input[name="tc-msg-team"][value="1"]').prop("checked", true);
		} else{
			$('input[name="tc-msg-team"][value="0"]').prop("checked", true);
		}
		if(_teamConfig.allow_msg_teacher){
			$('input[name="tc-msg-teacher"][value="1"]').prop("checked", true);
		}else{
			$('input[name="tc-msg-teacher"][value="0"]').prop("checked", true);
		}

		if(_teamConfig.allow_save_ld){
			$('input[name="tc-msg-save-ld"][value="1"]').prop("checked", true);
		}else {
			$('input[name="tc-msg-save-ld"][value="0"]').prop("checked", true);
		}
		if(_teamConfig.allow_attach_ld){
			$('input[name="tc-msg-send-ld"][value="1"]').prop("checked", true);
		}else {
			$('input[name="tc-msg-send-ld"][value="0"]').prop("checked", true);
		}
		
		$("#tc-teaming-method").val(_teamConfig.method);
		updateTeamingPanel(_teamConfig.method);
		if(_teamConfig.method!=TEAM_CHOICE_METHOD.ONLY_TEACHER){
			$("#tc-future-team-size-minimal").val(_teamConfig.size_min);
			$("#tc-future-team-size-maximal").val(_teamConfig.size_max);
			$("#tc-future-team-size-optimal").val(_teamConfig.size_opt);
			if(_teamConfig.teams_max == MAX_NB_TEAMS){
				$("#tc-future-max-team-number").val("");
			} else {
				$("#tc-future-max-team-number").val(_teamConfig.teams_max);
			}
			
			
		}

		if(_teamConfig.email_on_create){
			$("#tc-email-on-create").attr('checked', true);
		}
	}
	// Display default settings on the interface
	function displayDefaultSettings(){
		$("#tc-start-date").val("");
		$("#tc-start-time").val("00:00");
		$("#tc-end-date").val("");
		$("#tc-end-time").val("23:59");

		$("#tc-import-ld").val(LABDOC_IMPORT.NOT_ALLOWED);
		$("#tc-report-prefix").val(__("Equipe_"));

		resetMessageOptions();

		$("#tc-teaming-method").val(TEAM_CHOICE_METHOD.NO_METHOD);
		$("#tc-future-team-size-minimal").val("");
		$("#tc-future-team-size-maximal").val("");
		$("#tc-future-team-size-optimal").val("");
		$("#tc-future-max-team-number").val("");

		updateTeamingPanel(TEAM_CHOICE_METHOD.ONLY_TEACHER);

		$("#tc-email-on-create").attr('checked', true);
	}
	// Improve display of mission and class select list, mainly remove current class or mission
	function updateSelectList(){
		// Remove current mission from select list
		let missionId = $("#tc-mission").val();
		$("#tc-reuse-team-choice-mission option").show();
		$("#tc-reuse-team-choice-mission option[value='" +  missionId + "']").hide();
		$("#tc-reuse-team-choice-mission").val(0);
		$("#tc-import-ld option").show();
		$("#tc-msg-mission option").show();
		$("#tc-msg-class option").show();
	}
	// Add select option in reusable mission select
	function displayReusableMission(missions){
		$("#tc-reuse-team-choice-mission").html("<option value=\"0\">-</option>");
		for(let i = 0; i < missions.length; i++){
			$('<option/>').val(missions[i].id_mission).text(missions[i].code).appendTo('#tc-reuse-team-choice-mission');
		}
	}
	
	return {
		// Call this function when something changed (settings or students)
		change : function  () {
			_teamConfigHasChanged = true;
		},
		// Return true if at least one change has been perform
		hasChanged : function () {
			return _teamConfigHasChanged;
		},
		// Return true if tc is already saved in DB
		isSavedInDB : function () {
			return _teamConfig.id_team_config != undefined;
		},
		// Load team config from BDD
		// reuse is true when loading existing TC settings in current TC 
		load : function (selectedClass, missionId,reuse=false){
			$.ajax({
				url: '/teamconfig/getConfigAndTeamsFromClasseMission',
				method: "GET",
				data: {
					id_mission: missionId,
					id_class: _selectedClass.id_class,
					reuse: reuse?1:0
				}
			}).done(function (data){
				_teamConfigHasChanged = false;
				if(reuse){
					data.teams.forEach(function (report) {
						report.status = REPORT_STATUS.NEW;
						delete report.id_report;
					});
					if(_teamConfig){
						data.settings.id_team_config=_teamConfig.id_team_config;
					} else {
						data.settings.id_team_config=null;
					}
				}
				// TC exists in DB so load settings in the interface 
				if(data.settings){
					_teamConfig = data.settings;
					displaySettingsFromData();
				}
				// TC not exists in DB so load default settings in the interface 
				else {
					_teamConfig = {
						id_team_config: null,
					};
					displayDefaultSettings();
				}
				teamConfigReports.displayStudents(selectedClass,data.teams);
				showTeamConfigOptionsAndStudent();
				displayReusableMission(data.missionsToCopy);
				updateSelectList();
				if(data.isExternalClass){
					// Class is created from moodle
					openFutureStudentsOption();
				} else {
					if( data.settings && !reuse){
						openStudentsOption();
					}
					if(reuse){
						openTeamConfigOptionsAndStudent();
					}
				}
				
			})
				.fail(function (data){
					$("#tc-popup").dialog("close");
					alertGently(__("Vous n'avez pas les droits requis pour visualiser cette mise en équipe. Vous n'etes pas tuteur ou concepteur de la mission"));
				});
		},
		// View team config popup with classe and mission ID
		view: function (classe,missionId = false) {
			$("#tc-popup .popup-title").text(__("Mise en équipe : attribution d'une mission à la classe {{classe}}", {classe: classe.class_name}));
			$("#tc-label-external-student").text(__("Etudiants hors de la classe '{{classe}}'", {classe: classe.class_name}));
			cleanPopup();
			$("#tc-popup").dialog("open");
			_selectedClass = classe;
			_teamConfigHasChanged = false;
			if(missionId){
				this.load(_selectedClass, missionId);
				$("#tc-mission").val(missionId);
				$("#tc-delete-teaming").removeClass("disabled");
				$("#tc-popup .popup-submit").removeClass("disabled");
			}
		},
		// Use this function when opening from url
		viewFromTeamConfigId : function (id_team_config) {
			$.ajax({
				url: '/teamconfig/'+id_team_config+'/getFullTeams',
				method: "GET",
			}).done(function (data){
				if(data){
					_teamConfigHasChanged = false;
					teamConfigPopupActions.view(data.settings.classe, data.settings.id_mission);
					_teamConfig = data.settings;
					$("#tc-mission").val(_teamConfig.id_mission);
					displaySettingsFromData();
					teamConfigReports.displayStudents(data.settings,data.teams);
					showTeamConfigOptionsAndStudent();
					updateSelectList();
				}
			}).error(function() {
				alertGently(__("Erreur lors de l'accès à la mise en équipes."), "error");
			});
		},
		// Reuse previous teaming
		reuse: function () {
			if($('#tc-reuse-team-choice-mission').val() != TEAM_CHOICE_METHOD.NO_METHOD && $('#tc-reuse-team-choice-mission').val()!=$("#tc-mission").val()){
				if(_teamConfig.id_team_config === null && !_teamConfigHasChanged){
					teamConfigPopupActions.load(teamConfigPopupActions.getSelectedClass(),$('#tc-reuse-team-choice-mission').val(),true)
				} else {
					if(teamConfigReports.hasStartedReports()){
						alert(__("Il n'est pas possible de réutiliser une mise en équipe si des rapports ont déjà été débutés."));
					} else {
						if (confirm(__("Vous souhaitez remplacer la mise en équipe actuelle par celle d'une autre mission"))) {
							teamConfigPopupActions.load(teamConfigPopupActions.getSelectedClass(),$('#tc-reuse-team-choice-mission').val(),true);
						}
					}
				}
			}
		},
		// Save team config in BDD
		save : function () {
			cleanDateTime();
			var allow_import, allow_import_id_mission;
			if($('#tc-import-ld').val() > 0 ){
				allow_import_id_mission = $('#tc-import-ld').val()
				allow_import = 1;
			} else {
				allow_import = $('#tc-import-ld').val() == -1 ? 1 : 0 ,
				allow_import_id_mission = null;
			}
			var data = {
				id_team_config: _teamConfig.id_team_config,
				id_mission: parseInt($("#tc-mission").val()),
				id_class : _selectedClass.id_class,
				method : $("#tc-teaming-method").val(),
				size_opt : $("#tc-future-team-size-optimal").val(),
				size_max : $("#tc-future-team-size-maximal").val(),
				size_min : $("#tc-future-team-size-minimal").val(),
				teams_max : $("#tc-future-max-team-number").val(),
				start_datetime: _teamConfig.start_datetime,
				end_datetime: _teamConfig.end_datetime,
				allow_msg_teacher : $('input[name=tc-msg-teacher]:checked').val(),
				allow_msg_team : $('input[name=tc-msg-team]:checked').val(),
				allow_msg_id_class :  $("#tc-msg-class").val()  === "0" ? null : parseInt($('#tc-msg-class').val()) ,
				allow_msg_id_mission :  $("#tc-msg-mission").val() === "0" ? null : parseInt($('#tc-msg-mission').val()) ,
				allow_attach_ld : $('input[name=tc-msg-send-ld]:checked').val() ,
				allow_save_ld : $('input[name=tc-msg-save-ld]:checked').val() ,
				allow_import : allow_import,
				allow_import_id_mission : allow_import_id_mission ,
				name_prefix : $("#tc-report-prefix").val(),
				email_on_create : $("#tc-email-on-create").prop('checked') ? 1 : 0,
				teams : teamConfigReports.getReports(),
				retroactive : null
			}
			// All the validation method
			if(data.method==TEAM_CHOICE_METHOD.NO_METHOD){
				alert(__("Vous devez choisir une méthode de répartition des étudiants avant de valider une mise en équipe."),"warning");
				openFutureStudentsOption();
				return;
			}
			if(!validateDateTime(data)){
				return;
			}
			if(data.method==TEAM_CHOICE_METHOD.RANDOM && teamConfigReports.hasStudentsWithoutReport()){
				if (confirm(__("Les étudiants sans rapport vont maintenant être mis en équipe par un tirage aléatoire réalisé selon les contraintes que vous avez définies"))) {
					teamConfigReports.randomAssignment(data);
				}
				return;
			}
			$.ajax({
				url: '/teamconfig/saveTeamSettings',
				method: "POST",
				data: data
			}).done(function (data){
				if(data.status=="success"){
					alertGently( data.message, "info");
					teamConfigPopupActions.executePostCreateOrDeleteHook(_selectedClass.id_class);
					$("#tc-popup").dialog("close");
				} else {
					alert(data.message);
				}
			}).fail(function(jqXHR, textStatus) {
				console.log(jqXHR );
				console.log(textStatus );
			});
		},
		delete : function () {
			if( !_teamConfig.id_team_config){
				$("#tc-popup").dialog("close");
			} else {
				if(teamConfigReports.hasStartedReports()){
					alert(__("Impossible de supprimer cette mise en équipe, des rapports ont déjà été commencés."));
				} else {
					$.ajax({
						method: "POST",
						url: '/teamconfig/' + _teamConfig.id_team_config + '/delete'
					}).done(function(){
						alertGently(__("Mise en équipe supprimée."), "info");
						teamConfigPopupActions.executePostCreateOrDeleteHook(_selectedClass.id_class);
						$("#tc-popup").dialog("close");
					}).fail(function(jqXHR, textStatus) {
						console.log(jqXHR );
						console.log(textStatus );
					});
				}
			}
		},
		getSelectedClass : function (){
			return _selectedClass;
		},
		// Used to update students view after creating or deleting TC
		setPostCreateOrDeleteHook : function(hook) {
			_postHook = hook
		},
		// Used to update students view after creating or deleting TC
		executePostCreateOrDeleteHook : function (param){
			if(_postHook){
				_postHook(param);
			}
		}
	}
}();

// Reports and students management
var teamConfigReports = function() {
	// All Students without team 
	var _students ;
	// All external students added since last saving
	var _externalStudentsAddedSinceLastSave;
	// All Students of the classe
	var _initialStudentsOfTheClasse ;
	// Current student selected (for drag and drop)
	var _selectedStudent ;
	// All the reports (started + not started)
	var _reports;
	// Selected class
	var _selectedClass;

	function getNextIndexAvailable() {
		if(_reports.length>0){
			return Math.max.apply(Math, _reports.map(function(o) { return o.index; })) + 1
		}
		return 1
	}
	
	// Add HTML and data for empty report
	function addEmptyReport (){
		var reportName=  teamConfigReports.getNextReportNameAvailable ();
		var index = getNextIndexAvailable();
		
		// Display empty report
		var emptyReportHTML=
			"<div id='tc-new-report" + index +  "' data-report-index='" + index +  "' class='tc-reports'> " +
			"<input type='text' class='tc-report-name' value='"+ reportName +"' />" +
			"<div class='team tc-student-drop-zone' droppable='true' > " +
				"<div style='font-style: italic'>&nbsp;" + __("Nouvelle équipe") + "</div>"+
			"</div>" +
			"</div>";

		$("#tc-fieldset-not-started-reports").append(emptyReportHTML);
		
		_reports.push({
			id_report : null,
			name : reportName,
			index : index,
			members : [],
			status : REPORT_STATUS.NEW
		});
	}
	function removeEmptyReport(){
		_reports.filter(report => report.members.length == 0).forEach(function(report){
			$("[data-report-index='"+ report.index + "']").remove();
		})

		_reports = _reports.filter(report => report.members.length != 0);
	}
	// Add HTML and data for existing report
	function addReport(report){
		var index = getNextIndexAvailable();
		var reportHTML;
		if(report.name==""){
			report.name=teamConfigReports.getNextReportNameAvailable();
		}
		var isStarted = report.status!==REPORT_STATUS.NEW;
		if(!isStarted) {
			// Build report display
			reportHTML =
				"<div id='tc-new-report" + index + "' data-report-index='" + index + "' class='tc-reports'> " +
					"<input type='text' class='tc-report-name' value='"+ report.name +"' />";
		} else {
			reportHTML =
				"<div id='tc-on-team" + index +  "' data-report-index='" + index +  "' class='tc-reports'> "+
					"<span class='tc-report-name'>" + report.name +"</span>";
		}

		reportHTML += "<div class='tc-student-drop-zone' droppable='true'>";
		
		if(report.members.length == 0){
			reportHTML += "<div className='placeholder-drop-zone'>&nbsp;" + __("Nouvelle équipe") + "</div>";
		} else {
			for (var i = 0; i < report.members.length; i++) {
				var student = _initialStudentsOfTheClasse.filter(student => student.id_user == report.members[i].id_user);
				if(student.length>0){
					// The student is member from the current class
					reportHTML += createStudentView(report.members[i],true, !isStarted, false);
				} else {
					// The student have been added from other class
					reportHTML += createStudentView(report.members[i],true, !isStarted, true);
				}
				_students = _students.filter(student => student.id_user != report.members[i].id_user);
			}
		}
		
		reportHTML +=
				"</div>" +
			"</div>";
		
		if(!isStarted){
			$("#tc-fieldset-not-started-reports").append(reportHTML);
		} else {
			$("#tc-fieldset-started-reports").append(reportHTML);
		}
		
		report.index = index;
		_reports.push(report);
	}
	function removeStudentView(id_student){
		$("span[data-student-id=" + id_student +"]").remove();
	}

	function formatName(student) {
			let last = typeof student.lastname === 'undefined' ? student.user_name : student.lastname;
			let first = typeof student.firstname === 'undefined' ? student.first_name : student.firstname;
			return capitalize(last) + ' ' + capitalize(first);

	}
	// Add HTML for student
	function createStudentView(student, isInReport, isDraggable, isExternalToTheClass = false){
		// Various types of students
		let classes = "student-item";
		let title = "";

		isInReport ? classes += ' with-team' : classes += ' without-team';

		// TODO isExternal to the class useless ?
		if(isExternalToTheClass || student.external){
			classes += ' external-student';
			title += ", " + __("externe à la classe");
		} else {
			classes += ' class-student';
		}

		if(!student.hasOwnProperty('status')){
			isDraggable = true;
		}
		if(isDraggable){
			classes += ' draggable';
		} else {
			classes += ' not-draggable';
			title += " " + __("a débuté son rapport : toute modification doit être faite dans l'onglet 'Rapports'");
		}
		let name = formatName(student);

		let studentHTML = '<span class="' + classes + '" draggable="' + isDraggable.toString() + '" title="' + name + title + '" data-student-id="' + student.id_user + '">';
		studentHTML += "<span class='tc-user'>" + name +"</span>";
		if(isInReport && isDraggable){
			studentHTML += "<button type='button' class='tc-user-close'><i title='" + __("Retirer du rapport") + "' class='fa fa-times'></i></button>";
		}
		studentHTML += "</span>";
		
		return studentHTML;
	}
	// Move students outside team
	function addStudentInStudentsWithoutTeam(student){
		$("#tc-students-without-team").append(createStudentView(student, false, true, student.external));
		if(!_students.find(s => s.id_user === student.id_user)){
			_students.push(student);
		}
	}
	//Check if external student is not already in the teaming
	function externalStudentAlreadyInTeamConfig(externalStudent){
		// Check if student is in student without report
		if(_students.find(student => student.id_user === externalStudent.id_user)){
			alert(__("L'étudiant {{student_name}} est déjà présent dans les étudiants sans rapports", { student_name : externalStudent.first_name + " " + externalStudent.user_name }));
			return false;
		}
		//Check if student is in report
		if(_reports.map(report => report.members).flat().find(member => member.id_user === externalStudent.id_user)) {
			alert(__("L'étudiant {{student_name}} est déjà présent dans un rapport.", {student_name: formatName(externalStudent)}));
			return false;
		}
		return true;
	}
	// Add all the listeners for drag and drop students, adding and removing students
	function addStudentsListeners (){
		// A student is drop in report (started or not)
		// Cannot simply use .tc-student-drop-zone because report are not all created when we register the event
		$(".fieldset-report-drop-zone")
			.on('dragover', ".tc-student-drop-zone", false)
			.on("drop", ".tc-student-drop-zone", function (event){
				event.stopPropagation();
				if(_selectedStudent != null && $(this).parent().attr("data-report-index")!=_selectedStudent.actualReportIndex){
					removeStudentView(_selectedStudent.id_user);
					teamConfigReports.addStudentInReport( _selectedStudent, $(this).parent().attr("data-report-index"));
					updateStudentsIndicators();
				}
				_selectedStudent=null;
			});
		
		// A student is drop in students without team 
		$("#tc-students-without-team, #tc-students-without-team-label")
			.on('dragover', false)
			.on("drop", function (event){
				event.stopPropagation();
				if(_selectedStudent != null && _selectedStudent.actualReportIndex) {
					removeStudentView(_selectedStudent.id_user);
					teamConfigReports.removeStudentFromReport(_selectedStudent, _selectedStudent.actualReportIndex);
					if (_students.length == 0) {
						// Remove placeholder before adding one student
						$("#tc-students-without-team").text("");
					}
					addStudentInStudentsWithoutTeam(_selectedStudent);
					updateStudentsIndicators();
					_selectedStudent=null;
				}
			});

		// A student without team is drag 
		$("#tc-students-options")
			.on("drag",	".without-team", function (event){
				_selectedStudent=_students.find(student => student.id_user === parseInt($(this).attr('data-student-id')));
				_selectedStudent.actualReportIndex=null;
				_selectedStudent.external = $(this).hasClass("external-student");
				event.stopPropagation();
			});

		
		// A student with team is drag
		$("#tc-students-options")
			.on("drag",	".with-team", function (event){
				event.stopPropagation();
				var reportIndex = $(this).parent().parent().attr('data-report-index');
				var report = teamConfigReports.getReports().find(report => report.index==reportIndex);
				_selectedStudent=report.members.find(student => student.id_user === parseInt($(this).attr('data-student-id')));
				_selectedStudent.actualReportIndex = reportIndex;
				_selectedStudent.external = $(this).hasClass("external-student");
			});
		
		// A student with team is deleted with cross button
		$("#tc-student-with-team-panel").off('click', 'button.tc-user-close')
			.on("click", "button.tc-user-close", function (event){
				event.stopPropagation();
				var reportIndex = $(this).parents(".tc-reports").attr('data-report-index');
				var report = teamConfigReports.getReports().find(team => team.index==reportIndex);
				_selectedStudent=report.members.find(student => student.id_user === parseInt($(this).parents(".student-item").attr('data-student-id')));
				teamConfigReports.removeStudentFromReport( _selectedStudent, reportIndex);
				removeStudentView(_selectedStudent.id_user);
				if($(this).parents(".student-item").hasClass("external-student")){
					alertGently(__("L'étudiant {{student}} qui était externe à la classe a été retiré de la mise en équipe.", { student : $(this).parent().text()} ), "info");
				} else {
					if(_students.length==0){
						// Remove placeholder before adding one student
						$("#tc-students-without-team").text("");
					}
					$("#tc-students-without-team").append(createStudentView(_selectedStudent,false, true, _selectedStudent.external));
					_students.push(_selectedStudent);
					updateStudentsIndicators();
				}
			});
		
		$(".tc-report-name").on("change",function (){
			var report_index = $(this).parent().attr("data-report-index");
			var report = _reports.find(report => report.index == report_index);
			report.name=$(this).val();
		});
	}
	// Add placeholder in empty drop zone
	function addPlaceholderInDropZone(id){
		$(id).html( "<div class='placeholder-drop-zone'>&nbsp;" + __("Nouvelle équipe") + "</div>");
	}
	// Add placeholder in student without team
	function addPlaceholderInStudentWithoutReportZone(){
		$("#tc-students-without-team").html( "<div class='placeholder-drop-zone'> " + __("Tous les étudiants actuels sont affectés à un rapport.") + "</div>");
	}
	// Update indicator
	function updateStudentsIndicators(){
		var nbStudentsInReport = _reports.map(r=>r.members.length).reduce((a,b)=>a+b);
		let nb_started_reports = getReportsStarted().length;
		
		$("#tc-legend-not-started-reports").text(__("Rapports non débutés ({{nbReportsNotStarted}})", { nbReportsNotStarted : getReportsNotStarted().length-1 }));// -1 for empty team
		$("#tc-legend-started-reports").text(__("Rapports débutés ({{nbReportsStarted}})", { nbReportsStarted : nb_started_reports }));
		if (nb_started_reports > 0) {
			$("#tc-info-started-reports").text(__("Les étudiants ayant débuté leur rapport ne peuvent pas être déplacés. Ceci peut toutefois être réalisé dans l'onglet \"Rapports\"."));
		} else {
			$("#tc-info-started-reports").text("");
		}
		$("#tc-students-number").text(__("{{nbStudentsInReport}} étudiants sur {{totalStudents}} sont en équipe", { nbStudentsInReport : nbStudentsInReport , totalStudents : nbStudentsInReport + _students.length }));
		$("#tc-students-number").append(getExtPlatformLine(_selectedClass, true));

	}
			
	function getReportsNotStarted (){
		return _reports.filter(report => report.status==REPORT_STATUS.NEW)
	}

	function getReportsStarted (){
		return _reports.filter(report => report.status!=REPORT_STATUS.NEW)
	}
	
	function getTotalNumberOfStudents(){
		if(_reports.length == 0 ){
			return _students.length;
		}
		return _reports.map(r=>r.members.length).reduce((a,b)=>a+b) + _students.length;
	}
	
	function cleanInterfaceBeforeRandomizing ( ){
		$("#tc-fieldset-not-started-reports").html("");
		$("#tc-fieldset-started-reports").html("");
		/*$("#tc-fieldset-started-reports .draggable").remove()*/
	}
	function cleanReportsAndStudentStateBeforeRandomizing ( ){
		//students from not started reports
		_students = getReportsNotStarted().map(report => report.members).flat();
		_reports = getReportsStarted();
		//students not saved from started reports
		_reports.forEach(function (report) {
			_students = _students.concat(report.members.filter(member => !member.hasOwnProperty('status')));
			report.members = report.members.filter(member => member.hasOwnProperty('status'))
		});
		//_students = _students.concat(_externalStudentsAddedSinceLastSave);
	}
	
	function updateReuseAndDeleteButton() {
		if(teamConfigReports.hasStartedReports()){
			$("#tc-reuse").hide();
			$("#tc-delete-teaming").show();
			$("#tc-delete-teaming").addClass("disabled");
			$("#tc-delete-teaming").prop("title",__("La mise en équipe ne peut pas être supprimée car il y a des rapports débutés"));

		} else {
			$("#tc-reuse").show();
			$("#tc-reuse-team-choice-mission").prop("disabled",false);
			
			if(!teamConfigPopupActions.isSavedInDB()) {
				$("#tc-delete-teaming").hide();
			} else {
				$("#tc-delete-teaming").show();
				let message = '';
				if (_selectedClass.id_extplatform){
					$("#tc-delete-teaming").addClass("disabled");
					message = __("La mise en équipe ne peut pas être supprimée car elle est gérée par une activité distribuée dans Moodle. Supprimez l'activité Moodle pour supprimer la mise en équipe.");
				} else {
					$("#tc-delete-teaming").removeClass("disabled");
					message = __("Supprimer cette mise en équipe, attention cette action est irrémédiable");
				}
				$("#tc-delete-teaming").prop("title",message);
			}
		}
	}

	return {
		// Remove student from report
		removeStudentFromReport : function (student, actual_report_index){
			teamConfigPopupActions.change()
			var old_report = _reports.find(report => report.index == actual_report_index);
			_reports = _reports.filter(report => report.index != actual_report_index);
			// Warning use temp variable to avoid js error.
			old_report.members = old_report.members.filter(member => member.id_user != student.id_user);
			
			if(old_report.members.length>0){
				_reports.push(old_report);
			} else{
				if(_reports.filter(report => report.members.length == 0).length >0){
					$("[data-report-index='"+ actual_report_index + "']").remove();
				} else {
					// We have only old_report as empty team so we keep them
					_reports.push(old_report);
					addPlaceholderInDropZone("[data-report-index='"+ actual_report_index+ "'] > .tc-student-drop-zone");
				}
			}
			
			$("#tc-legend-not-started-reports").text(__("Rapports non débutés ({{nbReportsNotStarted}})", { nbReportsNotStarted : getReportsNotStarted().length - 1 })); // We remove 1, the empty team
		},
		// Add student in team config
		addStudentInTeaming: function (selectedStudent){
			if(!selectedStudent) {
				return;
			}
			let learner = {};
			learner.id_user = parseInt(selectedStudent.id);
			learner.first_name = selectedStudent.firstName;
			learner.user_name = selectedStudent.userName;
			learner.external = true;
			if (externalStudentAlreadyInTeamConfig(learner)) {
				_students.push(learner);
				_externalStudentsAddedSinceLastSave.push(learner);
				if (_students.length === 1) {
					$("#tc-students-without-team").html(createStudentView(learner, false, true, true));
				} else {
					$("#tc-students-without-team").append(createStudentView(learner, false, true, true));
				}
			}
			updateStudentsIndicators();
		},
		// Add student in report and add html view in report 
		addStudentInReport : function (student, new_report_index,){
			teamConfigPopupActions.change()
			var report = _reports.find(report => report.index == new_report_index);
			// Remove placeholder if necessary
			if(report.members.length==0){
				$("[data-report-index='"+ new_report_index+ "'] > .tc-student-drop-zone > div").remove();
			}
			if(!report.members.find(member => member.id_user === student.id_user)){
				report.members.push(student);
				$("[data-report-index='"+ new_report_index+ "'] > .tc-student-drop-zone").append(createStudentView(student,true, true, student.external));
			}
			
			if(student.actualReportIndex){
				// If student is moved from other team, we need to remove it from the old team
				this.removeStudentFromReport(student, student.actualReportIndex);
			} else {
				// Else, we need to remove student from the students without team
				_students = _students.filter(studentWithoutTeam => studentWithoutTeam.id_user != student.id_user);
				if(_students.length==0){
					addPlaceholderInStudentWithoutReportZone();	
				}
			}			
			
			// If no empty report, add one
			if(!_reports.find(report => report.members.length == 0)){
				addEmptyReport();
			}
		},
		// Display students : reports + students 
		displayStudents : function (selectedClass, reports) {
			$("#tc-fieldset-started-reports").empty();
			$("#tc-fieldset-not-started-reports").empty();
			$("#tc-students-without-team").empty();
			_selectedClass = selectedClass;
			_reports =[];
			_externalStudentsAddedSinceLastSave=[];
			$.ajax({
				url: '/classe/' + selectedClass.id_class + '/getStudents',
				method: "GET"
			}).done(function (response) {
				let i;
				_students = response.students;
				_initialStudentsOfTheClasse = response.students;
				
				if(reports){
					for(i = 0; i <reports.length; i++){
						if(reports[i].status != REPORT_STATUS.NEW){
							reports[i].members.forEach(member => member.status = SAVED);
						}
						
						addReport(reports[i]);
					}
					addEmptyReport();	
				}else{
					_reports=[];
					addEmptyReport();
				}

				// Add a warning on started reports
				let nbStarted = getReportsStarted().length;
				let msg = "";
				if (nbStarted) {
					msg = __('Aucune modification ne sera appliquée aux {{nbStarted}} rapports débutés - pour modifier ces rapports, utilisez l\'onglet "Rapports"', {nbStarted: nbStarted});
				}
				$('#tc-report-options-danger-message').text(msg);
				
				var studentsHTML ="";
				for (i = 0; i < _students.length; i++) {
					studentsHTML += createStudentView(_students[i],false,true);
				}
				// add students without team
				$("#tc-students-without-team").html(studentsHTML);
				if(_students.length==0){
					addPlaceholderInStudentWithoutReportZone();
				}
				updateReuseAndDeleteButton()
				updateStudentsIndicators();
				addStudentsListeners();
				load_add_users("tc-add-external-learner", '#tc-popup', __('Nom, prénom'), 'both', 0, selectedClass.id_inst, _initialStudentsOfTheClasse.map(e => e.id_user));

			});
		},
		// Random assignment of student without team 
		randomAssignment : function (parameters){
			const randomizer = TeamRandomization(parameters);
			if(!teamConfigReports.validateAssignmentConstraints(parameters, randomizer)){
				return;
			}

			// If no students without report, random can be perform on all students (except students in started reports)
			// if(!teamConfigReports.hasStudentsWithoutReport()){
			// 	if(confirm(__ ("Appliquer la mise en équipe aléatoire à tous les étudiants hors rapports débutés ?"))){
			// 		// TODO update message tous les étudiants non sauvés
			//	
			// 	}else {
			// 		return
			// 	}
			// }
			cleanInterfaceBeforeRandomizing();
			// If no students without report, re arrange all the students without team.
			if(_students.length == 0 ){
				cleanReportsAndStudentStateBeforeRandomizing();
			}
			removeEmptyReport();
		
			const totalUsersToAdd = _students.length;
			const numberOfTeams = randomizer.getNumberOfTeams();
			if (!numberOfTeams) {
				return;
			}
			var current_max_index = getNextIndexAvailable();
			
			// Création d'équipe
			var randomizedTeam = _reports;
			var originalNumberOfTeam = _reports.filter(report => report.members.length > 0).length;
			for(var i=1; i<=numberOfTeams-originalNumberOfTeam; i++){
				randomizedTeam.push(
					{
						id_report : null,
						name : this.getNextReportNameAvailable(),
						index : i+current_max_index,
						members : [],
						status : REPORT_STATUS.NEW
					}
				);
			}
			randomizer.allocate(_students, randomizedTeam, totalUsersToAdd);
			_reports=[]; // avoid double reports 

			randomizedTeam.sort((a, b) => a.name.localeCompare(b.name));
			
			randomizedTeam.forEach(report => addReport(report))

			if(!_reports.find(report => report.members.length == 0)){
				addEmptyReport();
			}

			$("#tc-students-without-team").empty();
			_students = [];

			openStudentsOption();
			teamConfigPopupActions.change();
			
			addPlaceholderInStudentWithoutReportZone();
			updateStudentsIndicators();
		},
		getReports : function (){
			_reports.forEach(function(report){
				// Update report with his name  
				if (report.status==REPORT_STATUS.NEW){
					report.name = $("[data-report-index=" + report.index +"] > input").val()
				}
			});

			return _reports;
		},
		hasStartedReports : function(){
			return  getReportsStarted().length > 0;
		},
		hasStudentsWithoutReport : function(){
			return _students.length>0;
		},
		getStudentsWithoutReport : function (){
			return _students;
		},
		getNextReportNameAvailable : function(){
			// This bloc is used to avoid similar name when creating/deleting class
			var nextNumberAvailable = 1;
			var prefix = ( '' + $("#tc-report-prefix").val()).replace(/'/g, '&quot;')
			
			while (_reports.find(report => report.name === prefix + formattedNumber(nextNumberAvailable))){
				nextNumberAvailable++;
			}
			return prefix +  formattedNumber(nextNumberAvailable);
		},
		updatePrefixName : function (){
			var reportsNotStarted = getReportsNotStarted();
			for(var i = 0; i < reportsNotStarted.length ; i++){
				reportsNotStarted[i].name= teamConfigReports.getNextReportNameAvailable()
				$("[data-report-index='"+ reportsNotStarted[i].index + "'] > input").val(reportsNotStarted[i].name);
			}
		},
		// Check constraints assignment of teaming 
		validateAssignmentConstraints : function(data, randomizer) {
			if (data.method != TEAM_CHOICE_METHOD.ONLY_TEACHER) {
				if (data.size_opt == "" || parseInt(data.size_opt) <= 0) {
					alert(__("La taille optimum d'une équipe doit être supérieure à 1"));
					openFutureStudentsOption();
					$("#tc-future-team-size-optimal").select();
					return false;
				}
				
				if (data.size_min == "") {
					data.size_min = 1;
				} else if (isNaN(parseInt(data.size_min)) || parseInt(data.size_min) <= 0) {
					alert(__("La taille minimale d'une équipe doit être supérieure à 0"));
					openFutureStudentsOption();
					$("#tc-future-team-size-minimal").select();
					return false;
				}
				
				if (data.size_max == "") {
					data.size_max = MAX_NB_TEAMS;
				} else if (isNaN(parseInt(data.size_max)) || parseInt(data.size_max) <= 0) {
					alert(__("La taille maximale d'une équipe doit être supérieure à 0"));
					openFutureStudentsOption();
					$("#tc-future-team-size-maximal").select();
					return false;
				}
				
				if (data.teams_max == ""){
					data.teams_max = MAX_NB_TEAMS;
				} else if (isNaN(parseInt(data.teams_max)) || parseInt(data.teams_max) <= 0) {
					alert(__("Le nombre maximal d'équipes doit être supérieur à 0"));
					openFutureStudentsOption();
					$("#tc-future-max-team-number").select();
					return false;
				}

				if (parseInt(data.size_min) > parseInt(data.size_opt) || parseInt(data.size_max) < parseInt(data.size_opt)) {
					alert(__("Problème de cohérence entre les tailles min. max. et optimum d'une équipe"));
					openFutureStudentsOption();
					return false;
				}
				
				let total_students = getTotalNumberOfStudents();
				const message = randomizer.validate(total_students);
				if (message !== '') {
					alert(message);
					openFutureStudentsOption();
					return false;
				}
			}
			return true;
		}
	}
}();

var selected_report;

$(document).ready(function() {
	defineModal("tc-popup", null, true, "cl-actions");
	defineModal("tc-popup-add-student", null, true, "tc-label-external-student");

	$("#tc-report-options, #tc-students-options").on("change", "input, select", function (){
		teamConfigPopupActions.change();
	});
	
	/*
	 * Listeners on the interface
	 */
	
	// Load team config setting from previous mission
	$('#tc-reuse-team-choice').on("click", function (){
		teamConfigPopupActions.reuse();
	});

	// Update display for messaging options
	$("input:radio[name=tc-allow-msg]").on("change",function (){
		$("#tc-messaging-options").toggle();
		if($("input:radio[name=tc-allow-msg]:checked").val()==0){
			disableMessageOptions();	
		} else {
			resetMessageOptions();
		}
	});
	
	// Change current mission and load data
	$("#tc-mission").on("change",function (){
		$("#tc-delete-teaming").hide();
		if($("#tc-mission").val()!=0){
			teamConfigPopupActions.load(teamConfigPopupActions.getSelectedClass(),parseInt($("#tc-mission").val()));
			$("#tc-delete-teaming").removeClass("disabled");
			$("#tc-popup .popup-submit").removeClass("disabled");
				
			//}
		} else{
			hideTeamConfigOptionsAndStudent();
			$("#tc-delete-teaming").addClass("disabled");
			$("#tc-popup .popup-submit").addClass("disabled");
		}
	});

	// Save team config
	$("#tc-popup-submit")
		.on("click", function (){
			if(!$(this).hasClass("disabled")){
				teamConfigPopupActions.save();
			}
	});

	// Delete teaming
	$("#tc-delete-teaming")
		.on("click", function (){
			if(!$(this).hasClass("disabled")) {
				if (confirm(__("Vous allez supprimer définitivement cette mise en équipe"))) {
					teamConfigPopupActions.delete();
				}
			} else {
				tooltipDeleteTeaming();
			}
	});

	// close team config without saving
	$("#tc-popup .popup-cancel")
		.on("click",function (){
			$("#tc-popup").dialog("close");
	});

	// Display constraints of team if necessary when method change
	$("#tc-teaming-method").on("change", function (){
		updateTeamingPanel($("#tc-teaming-method").val());
	});
	
	// Change prefix of report 
	$("#tc-report-prefix").on("input", function () {
		teamConfigReports.updatePrefixName();
	});
	
	// Do a random assignment of students without team
	$("#tc-random-assignment").on("click",function (){
		var param = {
			size_opt : $("#tc-future-team-size-optimal").val(),
			size_max : $("#tc-future-team-size-maximal").val(),
			size_min : $("#tc-future-team-size-minimal").val(),
			teams_max : $("#tc-future-max-team-number").val(),
		}
		teamConfigReports.randomAssignment(param);
	});

	// Tooltip event for mouse over
	$("#tc-reuse-tooltip").on("mouseover", function (){
		tooltipReuseTeaming();
	});
	$("#tc-report-authorizations-tooltip").on("mouseover", function (){
		tooltipReportAuthorizations();
	});
	$("#tc-work-period-tooltip").on("mouseover", function (){
		tooltipWorkPeriod();
	});
	$("#tc-import-ld-tooltip").on("mouseover", function (){
		tooltipImportLD();
	});
	$("#tc-message-tooltip").on("mouseover", function (){
		tooltipMessaging();
	});
	$("#tc-teaming-mode-tooltip").on("mouseover", function (){
		tooltipTeamingMode();
	});
	$("#tc-teaming-method-tooltip").on("mouseover", function (){
		tooltipTeamingMethod();
	});
	$("#tc-external-student-tooltip").on("mouseover", function (){
		tooltipExternalStudent();
	});
	$("#tc-report-prefix-tooltip").on("mouseover", function (){
		tooltipReportPrefix();
	});
});

/***********************
 * Interface management
 **********************/
function updateTeamingPanel(method) {
	switch (parseInt(method)) {
		case TEAM_CHOICE_METHOD.RANDOM :
			$("#tc-future-team-constraints-panel").show();
			$("#tc-random-assignment").show();
			break;
		case TEAM_CHOICE_METHOD.ONLY_TEACHER :
			openStudentsOption();
			$("#tc-future-team-constraints-panel").hide();
			break;
		case TEAM_CHOICE_METHOD.NO_METHOD :
			$("#tc-future-team-constraints-panel").hide();
			break;
		case TEAM_CHOICE_METHOD.STUDENT_CHOICE :
			$("#tc-future-team-constraints-panel").show();
			$("#tc-random-assignment").hide();
			break;
	}
}

function hideTeamConfigOptionsAndStudent(){
	$("#tc-options").hide();
	$("#tc-students").hide();
	$("#tc-future-students").hide();
	$("#tc-reuse").hide();
	$("#tc-popup .popup-submit i").addClass("disabled");
	$("#tc-students-number").hide();
}

function showTeamConfigOptionsAndStudent() {
	$("#tc-options").show();
	$("#tc-report-options").hide();
	$("#tc-toggle-reports-options").addClass("fa-caret-right").removeClass("fa-caret-down");
	
	$("#tc-students").show();
	$("#tc-students-options").hide();
	$("#tc-toggle-students-options").addClass("fa-caret-right").removeClass("fa-caret-down");
	
	$("#tc-future-students").show();
	$("#tc-future-students-options").hide();
	$("#tc-toggle-future-students-options").addClass("fa-caret-right").removeClass("fa-caret-down");
	
	$("#tc-toggle-random-options").addClass("fa-caret-right").removeClass("fa-caret-down");
	$("#tc-reuse").show();
	$("#tc-popup .popup-submit i").removeClass("disabled");
	$("#tc-students-random-panel").hide();
	$("#tc-students-random-label").show();
	
	$("#tc-students-number").show();
}

function openTeamConfigOptionsAndStudent() {
	openReportOption();
	openFutureStudentsOption();
	openStudentsOption();
}

function toggleReportOption(){
	$("#tc-toggle-reports-options").toggleClass("fa-caret-right").toggleClass("fa-caret-down");
	$("#tc-report-options").toggle();
}

function openReportOption(){
	$("#tc-toggle-reports-options").removeClass("fa-caret-right").addClass("fa-caret-down");
	$("#tc-report-options").show();
}

function toggleStudentsOption(){
	$("#tc-toggle-students-options").toggleClass("fa-caret-right").toggleClass("fa-caret-down");
	$("#tc-students-options").toggle();
}

function openStudentsOption(){
	$("#tc-toggle-students-options").removeClass("fa-caret-right").addClass("fa-caret-down");
	$("#tc-students-options").show();
}

function toggleRandomOptions(){
	$("#tc-toggle-random-options").toggleClass("fa-caret-right").toggleClass("fa-caret-down");
	$("#tc-students-random-panel").toggle();
}

function toggleFutureStudentsOption(){
	$("#tc-toggle-future-students-options").toggleClass("fa-caret-right").toggleClass("fa-caret-down");
	$("#tc-future-students-options").toggle();
}

function openFutureStudentsOption(){
	$("#tc-toggle-future-students-options").removeClass("fa-caret-right").addClass("fa-caret-down");
	$("#tc-future-students-options").show();
}

function resetMessageOptions(){
	$("#tc-messaging-options").show();
	$('input[name=tc-allow-msg]').filter("[value='1']").prop("checked", true);
	$('input[name="tc-msg-teacher"]').filter("[value='1']").prop("checked", true);
	$('input[name="tc-msg-team"]').filter("[value='1']").prop("checked", true);
	$("#tc-msg-class").val(0);
	$("#tc-msg-mission").val(0);
	$('input[name="tc-msg-save-ld"]').filter("[value='0']").prop("checked", true);
	$('input[name="tc-msg-send-ld"]').filter("[value='0']").prop("checked", true);
}

function disableMessageOptions(){
	$('input[name="tc-msg-teacher"]').filter("[value='0']").prop("checked", true);
	$('input[name="tc-msg-team"]').filter("[value='0']").prop("checked", true);
	$("#tc-msg-class").val(0);
	$("#tc-msg-mission").val(0);
	$('input[name="tc-msg-save-ld"]').filter("[value='0']").prop("checked", true);
	$('input[name="tc-msg-send-ld"]').filter("[value='0']").prop("checked", true);
}

/*
 Tooltip of the interface
 */
function tooltipDeleteTeaming(){
	let msg_title, msg_content;
	msg_title    =  __("Supprimer la mise en équipe");
	msg_content  = '<p>'+ $('#tc-delete-teaming').prop('title') + '.</p>';
	global_tooltip_popup.openTooltipPopup(msg_title, msg_content,"#tc-delete-teaming");
}
function tooltipReuseTeaming(){
	var msg_title, msg_content;
	msg_title    =  __("Réutiliser une mise en équipe");
	msg_content  = '<p>'+__("Utilise la configuration et les équipes d'une autre mission")+ '.</p>' +
		'<p>' +__("Après avoir choisi la mission-source, il est possible de modifier les équipes avant d'enregistrer.")+ '</p>' +
		'<p>'+__("La reprise d'une mise en équipe est impossible s'il y a des rapports débutés")+ '.</p>'
	global_tooltip_popup.openTooltipPopup(msg_title, msg_content,"#tc-reuse-tooltip");
}

function tooltipReportAuthorizations(){
	let msg_title    =  __("Autorisations de travail");
	let msg_content  = "<p>" + __("Paramètres utilisés lors de la création des rapports (peuvent être ultérieurement modifiés pour chaque rapport dans l'onglet 'Rapports')") + ".</p>";
	global_tooltip_popup.openTooltipPopup(msg_title, msg_content,"#tc-toggle-reports-options");
}

function tooltipWorkPeriod(){
	var msg_title, msg_content;
	msg_title    =  __("Période de travail");
	msg_content  = "<p>" + __("Période pendant laquelle les étudiants peuvent modifier leur rapport. A la fin de la période, le rapport est automatiquement 'rendu'.") + "</p>";
	global_tooltip_popup.openTooltipPopup(msg_title, msg_content,"#tc-work-period-tooltip");
}

function tooltipImportLD(){
	var msg_title, msg_content;
	msg_title    =  __("Import de labdocs");
	msg_content  = "<p>" + __("Les étudiants peuvent importer des labdocs dans leur rapport depuis un autre de leurs rapports. Ce paramètre permet d'autoriser cet import et éventuellement de limiter l'origine des labdocs à une unique mission.") + "</p>";
	global_tooltip_popup.openTooltipPopup(msg_title, msg_content,"#tc-import-ld-tooltip");
}

function tooltipMessaging(){
	var msg_title, msg_content;
	msg_title    =  __("Paramètres de messagerie");
	msg_content  = "<p>" + __("La messagerie permet aux étudiants d'envoyer des messages à des étudiants ou aux enseignants selon les autorisations accordées") + ".</p>" +
		"<p>" + __("La messagerie permet d'envoyer ou recevoir des labdocs attachés et de les enregistrer dans le rapport, selon les autorisations accordées") + ".</p>";
	global_tooltip_popup.openTooltipPopup(msg_title, msg_content,"#tc-message-tooltip");
}

function tooltipTeamingMode(){
	var msg_title, msg_content;
	msg_title    =  __("Répartition des étudiants dans les équipes");
	msg_content  = "<p>" + __("Les équipe d'étudiants attachées à un rapport peuvent être créées de trois façons différentes : au choix des étudiants, de façon aléatoire ou uniquement par les enseignants.") + "</p>" +
		"<p>" + __("Quel que soit le mode de répartition choisi, il est toujours possible de préconfigurer manuellement les équipes (cf. cadre suivant).") + "</p>" +
		"<p>" + __("Le mode de répartition s'applique aussi aux étudiants qui ne sont pas encore inscrits dans la classe.") + "</p>";
	global_tooltip_popup.openTooltipPopup(msg_title, msg_content,"#tc-future-students");
}

function tooltipTeamingMethod(){
	var msg_title, msg_content;
	msg_title    =  __("Trois modalités de création des équipes");
	msg_content  = "<p>" + __("Au choix des étudiants : les étudiants choisissent eux-mêmes leur équipe selon les contraintes définies par l'enseignant.") + "</p>" +
		"<p>" + __("Aléatoire : les étudiants sont répartis aléatoirement dans les équipes selon les contraintes définies par l'enseignant. En cas d'ajout d'un nouvel étudiant dans la classe, il est automatiquement affecté à une équipe lors de sa connexion.") + "</p>" +
		"<p>" + __("Uniquement par les enseignants : les équipes sont toutes créées par l'enseignant. Attention : en cas d'ajout d'un nouvel étudiant dans la classe, il n'a pas de rapport tant que l'enseignant ne lui en a pas affecté un.") + "</p>";
	global_tooltip_popup.openTooltipPopup(msg_title, msg_content,"#tc-teaming-method-tooltip");
}

function tooltipReportPrefix(){
	var msg_title, msg_content;
	msg_title    =  __("Préfixe des noms d'équipes");
	msg_content  = "<p>" + __("Les noms d'équipes sont formés du préfixe complété par deux chiffres") + ". " +
		__("Par exemple, 'team_' → 'team_01' etc.") + "</p>" +
		"<p>" + __("Changer le préfixe n'est pas rétroactif : le nouveau texte n'est appliqué qu'aux futures équipes") + ".</p>";
	global_tooltip_popup.openTooltipPopup(msg_title, msg_content,"#tc-report-prefix-tooltip");
}

function tooltipExternalStudent(){
	var msg_title, msg_content;
	msg_title    =  __("Ajout d'étudiants externes à la classe");
	msg_content  = "<p>" + __("Vous pouvez mettre en équipe des étudiants externes à la classe") + ".</p>" +
		"<p>" + __(" Ces étudiants ne sont pas ajoutés à la classe") + ".</p>";
	global_tooltip_popup.openTooltipPopup(msg_title, msg_content,"#tc-external-student-tooltip");
}

function validateDateTime(data){
	if( data.start_datetime > data.end_datetime) {
		alert(__("La date de fin de travail doit être supérieure à celle de début. "));
		openReportOption();
		return false;
	}
	return true;
}

/***************************************
 * Utils fonctions 
 **************************************/

// TODO moved in common.js ?
function normalizeDate(d) {
	if (!d) {
		return null;
	}
	if (d.match(/^\d{4}-\d\d-\d\d$/)) {
		// ISO
		return d;
	}
	var matches = d.match(/^(\d\d)[-\/](\d\d)[-\/](\d{4})/);

	if (matches) {
		// European
		return matches[3] + '-' + matches[2] + '-' + matches[1];
	}
	return null;
}

function formattedNumber(number){
	return number.toLocaleString('fr-FR', {
		minimumIntegerDigits: 2,
	});
}
