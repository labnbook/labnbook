var classActions = {
	/**
	 * Open a popup to add a new class.
	 *
	 * @return {undefined}
	 */
	add: function() {
		openEditClassPopup(null);
	},
	delete: function(classAttributes) {
		if (classAttributes.id_extplatform && classAttributes.teamMissions.length > 0){
			alertGently(__('Impossible de supprimer cette classe car elle est liée à des activités Moodle'), 'danger');
			return false;
		}
		if(confirm(__('ATTENTION : La suppression est irréversible'))) {
			$.ajax({
				method: "POST",
				url: "/classe/"+classAttributes.id_class+"/delete",
			})
			.done(function(content){
				$("#classes .lba-context-icons").addClass('actions-inactive');
				$("#cl-table").DataTable().ajax.reload();
				$("#students").hide();
			});
			return true;
		} else {
			return false;
		}
	},
	unsubscribe: function(classAttributes) {
		
		$.ajax({
			method: "POST",
			url: "/classe/"+classAttributes.id_class+"/unsubscribe",
		})
		.done(function(){
			$("#classes .lba-context-icons").addClass('actions-inactive');
			$("#cl-table").DataTable().ajax.reload();
			$("#students").hide();
		})
		.fail(function(responseData) {
			if(responseData.responseJSON && responseData.responseJSON.message) {
				alertGently(responseData.responseJSON.message, 'error');
			}
		});
	},
	/**
	 * Open a popup to edit this class.
	 *
	 * @param {int} idClass
	 * @return {undefined}
	 */
	edit: function(idClass) {
		openEditClassPopup(parseInt(idClass));
	},
	/**
	 * Open a popup to add students.
	 *
	 * @param {int} idClass
	 * @return {undefined}
	 */
	addStudentNumbers: function() {
		var classAttributes = getSelectedClassAttributes();
		//console.log(classAttributes);
		$("#le-add-studentnumbers-popup").find(".institution-name").text(classAttributes.inst_name);
		$("#le-add-studentnumbers-popup .alerts").html("");
		$("#le-add-studentnumbers-popup")
			.data('classAttributes', classAttributes)
			.dialog('open');
	},
	sendmail: function() {
		var dt = $('#le-table').DataTable();
		var students = dt.rows().data();
		var emails = "";
		var nb_mails = 0;
		var nb_students = dt.rows().count();
		for(var key in students) {
			if (students[key].email) {
				emails += students[key].email + ",";
				nb_mails++;
			}
		}
		if(emails) {
			if (nb_mails < dt.rows().count()) {
				alertGently(__("{{nbm}} e-mail(s) récupéré(s) pour {{nbs}} étudiant(s) dans la classe", {nbm: nb_mails, nbs: nb_students}), 'warning' );
			}
			window.location.href = "mailto:"+emails;
		} else {
			alertGently(__("Aucun e-mail récupéré pour les étudiants de cette classe"), 'warning' );
		}
	},
	openTeamConfig: function() {
		var c = getSelectedClassAttributes();
		if(c.status==="archive"){
			alertGently("Impossible de faire la mise en équipe. Si votre classe est archivée, désarchivez la.", "error")
		} else {
			$.ajax({
				method: "POST",
				url: "/classe/"+c.id_class+"/isExternal",
			}).then(function (data){
				if (data.external == false) {
					teamConfigPopupActions.view(c);
				} else {
					alertGently(__("Cette classe a été créée via Moodle vous ne pouvez pas faire de mise en équipe directement sur LabNBook."), 'error', 10000);
				}
			});
		}
	},
	archive: function(classAttributes) {
		const spreadCheckbox = document.getElementById('cl-archive-all-reports');
		const spreadArchiveToReports = spreadCheckbox && spreadCheckbox.checked; 
		if (classAttributes.status === 'normal') {
			$.ajax({
				method: "POST",
				url: "/classe/"+classAttributes.id_class+"/archive",
			})
				.done(function (content) {
					alertGently(__("La classe a été archivée."), "info");
					if ((content.toArchive.length !== 0 || content.toDelete.length !== 0) && spreadArchiveToReports) {
						if (content.toDelete.length !== 0) {
							$.ajax({
								method: "POST",
								url: "/teacher/reports/batchDelete",
								data: {
									id_reports: content.toDelete
								}
							})
							.fail(function (xhr) {
								logError("fail on delete report", "error", { "id_class": classAttributes.id_class });
							})
							.done(function (xhr) {
								alertGently(content.toDelete.length + " " + __("rapport(s) supprimé(s)"), "info")
							});
						}
						if (content.toArchive.length !== 0) {
							$.ajax({
								method: "POST",
								url: "/teacher/reports/batchArchive",
								data: {
									id_reports: content.toArchive
								}
							})
							.fail(function (xhr) {
								logError("fail on archive report", "error", { "id_class": classAttributes.id_class });
							})
							.done(function (xhr) {
								alertGently(content.toArchive.length + " " + __("rapport(s) archivé(s)"), "info")
							});
						}
					}
					$("#classes .lba-context-icons").addClass('actions-inactive');
					$("#cl-table").DataTable().ajax.reload(null,false);
					$("#students").hide();
				})
				.fail(function (xhr) {
					logError("fail on archive class", "error", { "id_class": classAttributes.id_class });
				})
		}
		else {
			$.ajax({
				method: "POST",
				url: "/classe/"+classAttributes.id_class+"/unpack",
			})
				.done(function (content) {
					$("#classes .lba-context-icons").addClass('actions-inactive');
					$("#cl-table").DataTable().ajax.reload();
					$("#students").hide();
				})
				.fail(function (xhr) {
					logError("fail on unpack class", "error", { "id_class": classAttributes.id_class });
				})
		}
	},
	getParamsArchivePopup: function (){
		const attributes = $('#cl-actions .lba-context-icons').data().params.classAttributes;
		let deleteLabel = __('Supprimer la classe "{{class}}"', {class : attributes.class_name} );
		let deleteDetailsLabel = __('ATTENTION : cette action est définitive et entrainera la suppression de tous les comptes étudiants n’étant rattachés qu’à cette classe');
		let archiveLabel = attributes.status==='archive'?
			__('Désarchiver la classe "{{class}}"', {class : attributes.class_name})
			:__('Archiver la classe "{{class}}"', {class : attributes.class_name});
		let archiveDetailsLabel = '';
		let spreadArchive = '';
		if(attributes.status!=='archive') {
			archiveDetailsLabel = __('Cette classe pourra être retrouvée en cochant la case "archives" du tableau');
			spreadArchive = `<div class="archive-popup-form-row">
							<input type="checkbox" id="cl-archive-all-reports"/>
							<label for="cl-archive-all-reports"> ${__('Archiver aussi les rapports assignés dans le cadre de cette classe')} </label>
							</div>`
		}
		let unsubscribeLabel = '';
		let unsubscribeDetailsLabel = '';
		if(attributes.teachers.length>1) {
			unsubscribeLabel = __('Se désinscrire de la class "{{class}}"', { class : attributes.class_name });
		}
		return {
			attributes: attributes,
			deleteLabel: deleteLabel,
			deleteDetailsLabel: deleteDetailsLabel,
			archiveLabel: archiveLabel,
			archiveDetailsLabel: archiveDetailsLabel,
			spreadArchive: spreadArchive,
			unsubscribeLabel: unsubscribeLabel,
			unsubscribeDetailsLabel: unsubscribeDetailsLabel
		};
	}
	
};

var global_cl_popup = function () {
	var teacherTeamsId = [];
	return {
		emptyTeacherTeam : function() {
			teacherTeamsId = [];	
		},
		addTeacherTeam : function (teacherTeamId){
			teacherTeamsId.push(teacherTeamId);
		},
		getTeacherTeams : function () {
			return teacherTeamsId;
		}
	};
}();

// Store learner data from getLearnerData
var global_learners_data_from_class;
/**
 * Return an HTML string from a user record.
 *
 * @param {object} user
 * @return {string}
 */
function escapeUserName(user) {
	return $('<span />').text(user.first_name + " " + user.user_name).html();
}

/**
 * @return {object}
 */
function getSelectedClassAttributes() {
	var htmlTable = $('#cl-table');
	var tr = $("#cl-table tr.selected").first();
	if (tr.length > 0) {
		return htmlTable.DataTable().row(tr).data();
	} else {
		return null;
	}
}

/**
 * @return {object}
 */
function getSelectedLearnerAttributes() {
	var htmlTable = $('#le-table');
	var higlightedTr = $("#le-table tr.selected");
	if (higlightedTr.length === 1) {
		return htmlTable.DataTable().row(higlightedTr).data();
	} else {
		return null;
	}
}

/**
 * selecting a table row (from any table)
 *
 * @param {Element} target DOM Element
 * @param {function} selectCallback
 * @param {function} unselectCallback
 * @return {undefined}
 */
function highlightRow(target, selectCallback, unselectCallback) {
	var tr = $(target);
	if (tr.hasClass('description-row')) {
		return;
	}
	var htmlTable = tr.closest('table.dataTable');
	var data = htmlTable.DataTable().row(tr).data();
	if (!data) {
		console.error("no data on this target, aborting: ", target);
		return;
	}
	var actionsBlock = $(htmlTable).closest('section').find(".lba-context-icons");
	$(".description-row", htmlTable).remove();
	if (tr.hasClass('selected')) {
		tr.removeClass('selected');
		actionsBlock.data('params', {});
		if (unselectCallback) {
			unselectCallback.call(this, htmlTable, tr);
		}
	} else {
		htmlTable.find("tr.selected").removeClass("selected");
		tr.addClass('selected');
		if (selectCallback) {
			selectCallback.call(this, htmlTable, tr, data);
		}
	}
	refreshStudentsIcons();
}

function showTooltipExternalStudents(location) {
	title = __("Étudiants Moodle");
	msg_content = __("Les étudiants Moodle apparaissent dans la classe LabNBook quand leur compte Moodle est lié à LabNBook. Cette liaison est réalisée au moment où l'étudiant accède à sa première activité LabNBook dans Moodle.");
	global_tooltip_popup.openTooltipPopup(title, msg_content, location);
}

function getExtPlatformLine(classAttributes, short) {
	let html="";
	if (!classAttributes.extplatform_name) {
		return html;
	}
	if (!short) {
		html += __("Cette classe est utilisée par la plateforme ") +  ' <a target="_blank" href="'+classAttributes.extplatform_url+'">'+classAttributes.extplatform_name+'</a>';
	}
	if (classAttributes.num_ext_participants) {
		if (!short) {
			html += __(
				" et compte {{nb_remote}} étudiants sur Moodle",
				{
					'nb_remote': classAttributes.num_ext_participants
				}
			);
		} else {
			html += __(
				" ({{nb_remote}} étudiants Moodle)",
				{
					'nb_remote': classAttributes.num_ext_participants
				}
			);
		}
	}
	id = "ext_students_tooltip_"+hTMLEscapeNoConv(classAttributes.id_class)+short;
	html += "<i id='"+id+"' class='fa fa-info-circle lba_icon_default' onmouseover='showTooltipExternalStudents(\"#"+id+"\")' ></i>";
	return html;
}

function highlightClassByRow(target) {
	/**
	 * Select a class row, display its description and the related learners table.
	 *
	 * @param {Element} htmlTable
	 * @param {Element} tr
	 * @param {Number} idClass
	 * @param {object} classAttributes (optional)
	 * @return {undefined}
	 */
	function highlightClass(htmlTable, tr, idClass, classAttributes) {
		if (typeof classAttributes === 'undefined' || !classAttributes) {
			classAttributes = htmlTable.DataTable().row(tr).data();
		}
		var code;
		if (classAttributes.class_code) {
			code = " <div><strong>" + __("Code d'auto-inscription") + __("&nbsp;:") + " </strong>" + classAttributes.class_code + "</div>" ;
		} else {
			code = "";
		}
		var status = "";
		if (classAttributes.status === 'archive') {
			status = "<div><strong>" + __("Statut") + "</strong> : " + __("archive") + "</div>";
		}
		var html = "<div><strong>" + __("Institution") + __("&nbsp;:") + " </strong>" + $('<span />').text(classAttributes.inst_name).html();
			html += "</div>";
			html += code +
			"<div><strong>" + __("Enseignants") + __("&nbsp;:") + " </strong>" + listUsers(classAttributes.teachers) + "</div>" +
			status;
		var descriptionCell = $('<td colspan="' + tr.children('td').length + '"></td>')
			.html(html);
		if (classAttributes.teamMissions.length) {
			descriptionCell.append(
				$("<div><strong>" + __("Mises en équipes") + __("&nbsp;:") + "</strong> </div>")
				.append (
					classAttributes.teamMissions.map(function(m) {
						let tc = $('<a href="#" class="class-team-missions"/>').attr("data-id-mission", m.id_mission);
						// TODO constant
						if(m.status == "archive"){
							tc.text(m.code + __(" (archive)")).attr('data-status-mission', m.status);
						} else {
							tc.text(m.code)
						}
						return $('<span></span>').append(
							tc
						)[0].outerHTML + " ";
					}
					)
				)
			);
		}
		descriptionCell.append($('<div>').html(getExtPlatformLine(classAttributes, false)));
		tr.after($('<tr class="description-row"></tr>').append(descriptionCell));
		var actionsBlock = $("#classes .lba-context-icons");
		actionsBlock.data('params', {idClass: classAttributes.id_class, classAttributes: classAttributes});
		actionsBlock.removeClass('actions-inactive');

		// display learners table
		var leTable = $("#le-table").data('idclass', idClass).DataTable();
		var new_url = "/classes/getLearnersData/?id_class=" + idClass;
		if(leTable.ajax.url() != new_url){
			leTable.ajax
				.url(new_url)
				.load();
		}
		$("#students .search-input").val(""); // remove the search
		leTable.search("").draw();
		$("#le-table").closest('section').show();
		refreshStudentsIcons();

		function listUsers(users) {
			return users.map(escapeUserName).join(" - ");
		}
	}

	highlightRow(
		this instanceof Element ? this : target,
		function(htmlTable, tr, data) {
			highlightClass(htmlTable, tr, data.id_class, data);
		},
		function(htmlTable, tr) {
			$("#students").hide();
			$("#classes .lba-context-icons").addClass("actions-inactive");
		}
	);
}

function highlightClassById(idClass) {
	$("#cl-table tr.selected").removeClass("selected");
	$("#students .lba-context-icons").addClass("actions-inactive");
	highlightClassByRow($('#class-' + idClass));
}

function highlightLearnerByRow(target) {
	/**
	 * Select a learner row and display its description.
	 *
	 * @param {Element} htmlTable
	 * @param {Element} tr
	 * @param {Number} idLearner
	 * @param {object} learnerAttributes (optional)
	 * @return {undefined}
	 */
	function highlightLearner(htmlTable, tr, idLearner, learnerAttributes) {
		if (typeof learnerAttributes === 'undefined' || !learnerAttributes) {
			learnerAttributes = htmlTable.DataTable().row(tr).data();
		}
		var descriptionCell = $('<td colspan="' + tr.children('td').length + '"></td>')
			.html(
				"<div><strong>" + __("Identifiant") + "</strong> : " + $('<span />').text(learnerAttributes.login).html()
				+ (learnerAttributes.inst_number ?
					"<span class='right-info'><strong>" + __("Numéro étudiant") + "</strong> : " + $('<span />').text(learnerAttributes.inst_number).html() + "</span>" :
					"")
				+ (learnerAttributes.email ?
					"<span class='right-info'><strong>" + __("E-mail") + __("&nbsp;:") + "</strong> " + '<a href="mailto:' + learnerAttributes.email + '">' + learnerAttributes.email + "</a></span>" :
					"")
				+ "</div><div><strong>" + (learnerAttributes.classes_num > 1 ? __("Inscrit(e) dans les classes") : __("Inscrit(e) dans la classe") )
				+ "</strong> : " + $('<span />').text(learnerAttributes.classes_names).html() + "</div>"
			);
		tr.after($('<tr class="description-row"></tr>').append(descriptionCell));
		var actionsBlock = $("#students .lba-context-icons");
		actionsBlock.data('params', {idClass: learnerAttributes.id_class, classAttributes: learnerAttributes});
	}

	highlightRow(
		this instanceof Element ? this : target,
		function(htmlTable, tr, data) {
			highlightLearner(htmlTable, tr, data.id_user, data);
		},
		function(htmlTable, tr, data) {
		}
	);
	refreshStudentsIcons();
}

function highlightLearnerById(idLearner) {
	$("#le-table tr.selected").removeClass("selected");
	highlightLearnerByRow($('#learner-' + idLearner));
}

function refreshStudentsIcons() {
	var hasHighlight = getSelectedLearnerAttributes() !== null;
	if (hasHighlight) {
		$("#students .lba-context-icons")
			.removeClass('actions-inactive')
			.removeClass('batch-actions').addClass('no-batch-actions');
	} else {
		$("#students .lba-context-icons")
			.addClass('actions-inactive')
			.removeClass('batch-actions').addClass('no-batch-actions');
	}
}

function searchInTable(el) {
	var dataTable = $(el).closest("section").find("table.dataTable").DataTable();
	if (dataTable) {
		dataTable.search(unaccent($(el).val())).draw();
	}
	refreshStudentsIcons();
}

function getAndDisplayLearnersTable() {
	$("#le-table").DataTable().ajax.reload();
}

$(document).ready(function() {
	"use strict";

	initDataTables();
	$('#cl-table tbody').on("click", "tr", highlightClassByRow);
	$('#le-table').on("click", "tbody > tr", highlightLearnerByRow);

	teamConfigPopupActions.setPostCreateOrDeleteHook(function(id_class) {
		$("#cl-table").DataTable().ajax.reload(function(){
			highlightClassById(id_class);
			getAndDisplayLearnersTable();
		})
	});

	// search
	$(".search-input").on('input', function() {
		searchInTable(this);
	});
	$("#classes .tb-search").on('click', 'i', function(){
		var searchPopup = $('#tb-search-class');
		if (searchPopup.css('display') === 'inline-block') {
			searchPopup.find('input').val('');
			searchInTable(this);
			searchPopup.css('display', 'none');
		} else {
			searchPopup.css('display', 'inline-block').find('input').focus();
		}
	});
	// pagination
	$(".pagination-size").on('change', function(){
		var dataTable = $(this).closest("section").find("table.dataTable").DataTable();
		if (dataTable) {
			dataTable.page.len($(this).val()).draw();
		}
	});

	// display the team config for a class+mission
	$('#classes').on('click', 'a.class-team-missions', function() {
		if($(this).attr("data-status-mission") != 'archive'){
			teamConfigPopupActions.view(getSelectedClassAttributes(),$(this).attr("data-id-mission") );
		} else {
			alertGently(__("Vous ne pouvez pas modifier une mise en équipe d'une mission archivée. "));
		}
		
	});

	// auto-open teamconfig dialog if the URL ends with #teamconfig=<ID>
	var hash = window.location && window.location.hash;
	if (hash) {
		var m = hash.match(/\bteamconfig=(\d+)\b/);
		if (m) {
			teamConfigPopupActions.viewFromTeamConfigId(parseInt(m[1]));
		}
	}

	// apply archive filter
	$("#display-archives").on('change', function() {
		$('#cl-table').DataTable().draw();
	});

	// clicking on a link with attribute "data-action" will launch this action
	$(".actions").on("click", "[data-action]", function(event) {
		event.stopPropagation();
		if ($(this).closest(".actions-inactive").length > 0) {
			return false;
		}
		var params = $(this).closest(".lba-context-icons").data('params');

		// Block action on archive item if archive items is not display
		if(!$("#display-archives").prop('checked') && params && params.classAttributes.status =="archive"){
			return false;
		}
		
		var action = $(this).attr('data-action');
		
		if(action === 'archive') {
			window.dispatchEvent(new CustomEvent('display-archive-popup'));
		} else if (classActions[action]) {
			if (params) {
				classActions[action].call(event, params.idClass, params.classAttributes);
			} else {
				classActions[action].call(event);
			}
		} else {
			console.error("unknown action: ", action, ", parameters: ", params);
		}
		return false;
	});

	$(document).keyup(function(e) {
		if (e.key === "Escape") { // escape key maps to keycode `27`
			if ($('#tb-search-class').css('display') === 'block') {
				$(".btn_close_search").click();
			}
		}
	});
	// popups
	$('.popup').each(function(){
		var popupElement = $(this);
		popupElement.dialog({
			autoOpen: false,
			modal: true
		}).draggable({
			handle: 'header',
			containment: [0,0]
		});
		var position = popupElement.attr('data-position');
		if (position) {
			popupElement.dialog(
				"option",
				"position",
				{
					my: "left top",
					at: "left+100 top+20",
					of: "#" + position
				}
			);
		}
	});
	$(".popup .popup-cancel").click(function() {
		var popupElement = $(this).closest(".popup");
		if (popupElement.attr('data-reset')) {
			popupElement.find('input, textarea').val('');
		}
		popupElement.dialog("close");
	});

	$("#le-add-studentnumbers-popup button").on('click', );

	$("#team-config-popup .popup-valid").on("click", function() {
		window.ConfigureTeams.save().then(function() {
			$("#team-config-popup").dialog("close");
			alertGently(__("Cette configuration d'équipes a été enregistrée."), 'success', 3000);
		}).catch(function(msg) {
			console.error("Saving in team_config failed: " + msg);
		});
		var attr = getSelectedClassAttributes();
		$("#cl-table").DataTable().ajax.reload(function(){
			highlightClassById(attr.id_class);
		});
	});

	function initDataTables() {
		$.fn.dataTable.ext.order.intl();
		$.fn.dataTable.ext.search.push(
			// filter out #cl-table on archive classes
			function(settings, viewData, rowIndex, rawData) {
				if (settings.sTableId !== "cl-table") {
					return true;
				}
				var includeOld = $("#display-archives").is(":checked");
				return (includeOld || rawData.status == 'normal');
			}
		);
		$.extend(true, $.fn.dataTable.defaults, {
			dom: "rtp", // https://datatables.net/reference/option/dom
			autoWidth: false,
			paging: true,
			pageLength: 20,
			lengthMenu: [20, 50, -1],
			conditionalPaging: true,
			order: [],
			language: { url: "/libraries/datatables/locale/" + window.labnbook.lang + ".json" }
		});

		var italicsArchives = function(value, cellType, row) {
			var escapedValue = $('<span>').text(value).html();
			var classAttr = (row.status === 'archive' ? ' class="status-archive"' : '');
			return '<span' + classAttr + '>' + escapedValue + '</span>';
		};

		const classesConfig = {
			ajax: "/teacher/students/getClasses",
			columns: [
				{
					data: "class_name",
					render: italicsArchives
				},
				{
					data: "nb_learners",
					orderDataType: "num"
				}
			],
			initComplete: function() {
				// Select a class on window load
				if (typeof window.global_select_class !== 'undefined') {
					$('#class-'+window.global_select_class).click();
					delete window.global_select_class
				}
			},
			rowId: function(d){ return "class-" + d.id_class; }
			/*createdRow: function( row, data, dataIndex) {
				if(data["status"] ===  "archive"){
					$('td', row).closest('tr').eq(0).addClass('status-archive');
				}
			}*/
		};
		const classes = $('#cl-table').DataTable(classesConfig);

		const learnersConfig = {
			pageLength: 20,
			ajax: {
				url: "/classes/getLearnersData"
			},
			columns: [
				{data: "user_name"},
				{data: "first_name"},
				{
					data: "last_synchro",
					render: {
						_: function(d) {
							if (d) {
								return (new Date(1000*d)).toLocaleDateString('fr-FR');
							} else {
								return '';
							}
						},
						sort: function(d) { return d; }
					}
				},
				{
					data: "missions",
					render: {
						_: function(d) {
							if (!d) {
								return "";
							}
							// display no more than 5 missions
							let missions = d.split(/ ; /);
							if (missions.length > 5) {
								return '<span title="' + escapeHTML(d) + '">' + escapeHTML(missions.slice(0, 5).join(" ; ")) +
									'…</span>';
							} else {
								return d;
							}
						},
						"filter": function(d) { return d; },
					}
				}
			],
			rowId: function(d) { return "learner-" + d.id_user; }
		};
		const learners = $('#le-table').DataTable(learnersConfig);
		learners.on('search.dt', function() {
			$('#num-visible-students').text(learners.rows({search: 'applied'}).count());
		} );
	}

});


/***********************************
 *        Initialisation     	   *
 ***********************************/

$(document).ready(function() {
	defineModal("le-popup", "le-add", true, "le-actions");

	$("#cl-popup-valid").on("click", function(){
		addOrUpdateClass(parseInt($("#cl-popup-valid").data('id_class')));
	});

	defineModal("add-students", null, false, null);
	defineModal("cl-popup", null, false, "cl-actions");
	defineModal("archive-popup", null, false, "cl-actions");
});


/**************************************************************************************************
 *                                            LES CLASSES		                                   *
 **************************************************************************************************/



/*********************************************
 *   Ajouter/modifier/supprimer une classe   *
 ********************************************/

/**
 * Affiche la popup d'ajout d'étudiants
 */
function openAddStudentDialog(id_user){
	var id_class = $("#le-table").data('idclass');
	var classRow = $("#cl-table").DataTable().row($("#class-" + id_class));
	addStudentsDialog.open(
		document.querySelector("#add-students .popup-contents"),
		classRow.data(),
		id_user
	);
}

/**
 * Récupère en ajax la liste d'étudiants d'une classe et les ajoute les étudiants
 * dans la liste d'ajout
 */
function updateLearnersFromClass(id_class){
	$.ajax({
		method: "GET",
		url: "/classes/getLearnersData",
		data: {
			id_class:id_class,
		},
		success: function(ans) {
			updateNumSelectedStudents(0);
			var div = $('#le-add-student-class-students');
			div.html('');
			for(i in ans.data){
				id=ans.data[i].id_user;
				name=escapeHTML(ans.data[i].first_name)+" "+escapeHTML(ans.data[i].user_name);
				div.append("<div><input type='checkbox' onclick='updateNumSelectedStudents(this.checked?1:-1)' value='"+id+"' id='le-add-checkbox-"+id+"'><label for='le-add-checkbox-"+id+"'>"+name+"</label></input></div>");
			}
			global_learners_data_from_class = ans.data;
		}
	});
}

/**
 * Compte le nombre d'étudiants sélectionés
 * @param int num nombre d'étudiants à ajouter ou 0 pour remettre le compteur à 0
 */
function updateNumSelectedStudents(num){
	if(typeof updateNumSelectedStudents.count == 'undefined'){
		updateNumSelectedStudents.count = 0;
	}
	if(num == 0){
		updateNumSelectedStudents.count = 0;
	}else{
		updateNumSelectedStudents.count = updateNumSelectedStudents.count + num;
	}
	$('#le-add-student-class-num-selected').html(updateNumSelectedStudents.count+"&nbsp;étudiants&nbsp;sélectionnés");
	addStudentsDialog.allowSubmit(updateNumSelectedStudents.count > 0);
}

/**
 * Affiche la popup d'édition de classe.
 * Récupère en AJAX les informations concernant cette classe et rempli les différents champs du popup.
 * Si my_id est défini : création de classe par l'enseignant d'id my_id
 */
function openEditClassPopup(id_class){
	if (typeof id_class === 'undefined' || !id_class) { // création de classe
		$("#cl-popup-title").text( __("Ajouter une classe") );
		$("#cl-popup-valid").data('id_class', null);
		$("#cl-add-inst").attr("disabled", false);
	}
	else { // modification de classe
		$("#cl-popup-title").text( __("Modifier la classe") );
		$("#cl-popup-valid").data('id_class', parseInt(id_class));
		$("#cl-add-inst").attr("disabled", true);
	}
	$.ajax({ // récupération des infos
		method: "GET",
		url: "/classe/initEdit",
		data: {
			id_class: id_class
		},
		success: function(data) {
			$("#cl-add-name").val(data.class_name) ;
			$("#cl-add-inst").html(data.institutionsHTML) ;
			$("#cl-teacher-datalist").show(0);
			$('#cl-add-teacher-or-team-list').empty();

			$('#'+getTeacherSelectId()).empty();
			$('#cl-teachers-list').empty();
			$.each(data.teachers, function(i, item) {
				addTeacherInSpan(item.name, item.id_teacher);
			});
			
			$('#cl-teacher-teams-list').empty();
			global_cl_popup.emptyTeacherTeam();
			$.each(data.teacherTeamsAssociated, function(i,item){
				global_cl_popup.addTeacherTeam(data.teacherTeamsAssociated[i].id_teacher_team);
			});
			
			loadAddTeachers();
		}
	});
	$("#cl-popup").dialog("open");
}

/**
 * remplit la data-list correspondant aux enseignants de l'institution sélectionnée
 */
function selectInstForClass(){
	// the selection is not possible anymore
	$("#cl-add-inst").prop('disabled', 'disabled');
	loadAddTeachers();
}

function checkIfTeacherIsNotAlreadyInClass(teacherId){
	return $("#cl-teachers-list").find('span[data-id-teacher='+teacherId+']').length == 0;
}

function checkIfTeacherTeamIsNotAlreadyInClass(teacherTeamId){
	return global_cl_popup.getTeacherTeams().indexOf(teacherTeamId) < 0;
}

function loadAddTeachers() {
	let placeholder = __('Nom prénom');
	if (isManager()) {
		placeholder = __('Nom prénom / équipe péda');
	} 
	let id_inst = $('#cl-add-inst').val();
	if (id_inst > 0) {
		load_add_users(getTeacherSelectId(), '#cl-popup', placeholder, 'teacher', 1, id_inst);
	}
}

function isManager() {
	return $('#cl-add-teacher-or-team').length  === 1;
}

function getTeacherSelectId() {
	return isManager() ? 'cl-add-teacher-or-team' : 'cl-add-teacher';
}

function addTeacherInSpan(teacherName, teacherId){
	add_user_in_staging_from_id_and_name(
		teacherId,
		teacherName,
		'#cl-teachers-list',
		'id-teacher',
		'deleteTeacherFromClass(' + teacherId + ')',
		__("Dissocier l'enseignant de la classe")
	);
}

function addTeacherInClass(selectedTeacher) {
	if(selectedTeacher && checkIfTeacherIsNotAlreadyInClass(selectedTeacher.id)){
		addTeacherInSpan(selectedTeacher.text, selectedTeacher.id);
	}
}

function addTeacherOrTeamInClass(selectedTeacherOrTeam){
	if(!selectedTeacherOrTeam) {
		return;
	}
	if(selectedTeacherOrTeam.type === "user"){
		if(checkIfTeacherIsNotAlreadyInClass(selectedTeacherOrTeam.id)){
			addTeacherInSpan(selectedTeacherOrTeam.text, selectedTeacherOrTeam.id);
		}
	} else {
		if( checkIfTeacherTeamIsNotAlreadyInClass(selectedTeacherOrTeam.id) ) {
			// Link the selected teacher team to the class
			global_cl_popup.addTeacherTeam(selectedTeacherOrTeam.id);
		}
		// Add the not already linked teachers from the team to the class 
		$.ajax({
			method: "POST",
			url: "/manager/getTeachersFromTeacherTeam",
			data: {
				id_teacher_team: selectedTeacherOrTeam.id
			},
		}).done(function(data){
			if(data){
				for(let id in data){
					if(checkIfTeacherIsNotAlreadyInClass(data[id].id_teacher)) {
						addTeacherInSpan( data[id].name, data[id].id_teacher);
					}
				}
			}
		});
	}
}

/**
 * supprime un enseignant de la liste des enseignant associé à une classe. Ces modifications ne seront valides qu'une fois validées
 * @param id_teacher un id d'enseignant à dé-associé de la classe selectionnée
 */
function deleteTeacherFromClass(id_teacher, allow_self_delete=false){
	if(!allow_self_delete && id_teacher == global_current_id_user){
		global_confirmation_popup.openConfirmationPopup( __("Ne plus être associé à cette classe"),  
			__("En cochant cette case vous confirmez vous désassocier de cette classe. Elle va disparaître de votre liste de classes"), function(id_user){
			deleteTeacherFromClass(id_user, true);
			});
		return ;
	}
	if ($('#cl-teachers-list').find("span").length == 1) {
		alertGently (  __("Au moins un enseignant doit être associé à cette classe") ,'error' ) ;
		return;
	} else {
		$("#cl-teachers-list").find('span[data-id-teacher="'+id_teacher+'"]').remove() ;
		window.dispatchEvent(new CustomEvent('lnb-select2-remove-id', {
			detail:{
				selector: "cl-add-teacher-or-team",
				idElement: id_teacher
			}}));
	}

	if(allow_self_delete){
		addOrUpdateClass(parseInt($("#cl-popup-valid").data('id_class')));
	}

}

/**
 * Vérifie les entrées pour le popup de création ou de modification de classe
 * Vérifie également que le nombre d'enseignant associé est non nul
 * Affiche une alert descriptive en cas d'erreur
 * @returns vrai si tout les champs d'édition/ajout de classe sont valide, faux sinon
 */
function validateAllClassPopup(){
	if (!ATControlString($('#cl-add-name'), "Nom de la classe", 1, 255, true)) {
		return false ; 
	}
	if ($("#cl-add-inst").val() == 0) {
		alertGently (  __("Veuillez sélectionner une institution") ,
			'error' ) ;
		return false ;
	}
	if ($('#cl-teachers-list').find("span").length == 0) {
		if ($("#cl-add-teacher").val() != "") {
			alertGently (  __("Veuillez valider l'enseignant à associer avant de valider la classe") ,
				'error' ) ;
		}else {
			alertGently (  __("Au moins un enseignant doit être associé à cette classe") ,
				'error' ) ;
		}
		return false ;
	}
	if ($("#cl-add-teacher").val() != undefined && $("#cl-add-teacher").val() > 0 ) {
		return confirm ( __('Attention : du texte est contenu dans le champ "Ajouter" sans avoir été validé.') + "\n" + __("Souhaitez vous confirmer les modifications effectuées ?") );
	}

	if ($("#cl-add-teacher-or-team").val() != undefined && $("#cl-add-teacher-or-team").val() > 0 ) {
		return confirm ( __('Attention : du texte est contenu dans le champ "Ajouter" sans avoir été validé.') + "\n" + __("Souhaitez vous confirmer les modifications effectuées ?") );
	}
	return true ;
}

/**
 * fonction addOrUpdateClass
 * Vérifie les entrées du popup ajout de class
 * Envoie en ajax les informations pour ajouter / mettre à jour une classe
 * En cas de succès affiche une message de succés et met à jour le tableau des classes
 * En cas d'échec affiche un alert décrivant le problème
 */
function addOrUpdateClass(id_class){
	if (validateAllClassPopup()) {
		// récupération des id des teachers
		var selected_teachers = [];
		$("#cl-teachers-list").find('span').each(function (index, element) {
			selected_teachers.push($(element).attr('data-id-teacher'));
		});
		var selected_teacher_teams = global_cl_popup.getTeacherTeams();
		$.ajax({
			method: "POST",
			url: "/classes/addOrUpdate",
			data: {
				id_class: isNaN(id_class) ? null : id_class,
				name: $("#cl-add-name").val(),
				code: '',
				inst: $("#cl-add-inst").val(),
				teachers: JSON.stringify(selected_teachers),
				teacherTeams: JSON.stringify(selected_teacher_teams)
			}
		}).done(function(id) {
			$("#cl-table").DataTable().ajax.reload(function(){
				if (id) {
					highlightClassById(id);
					if (!$('#class-'+id).length) {
						$("#students").hide();
					}
				}
			});
			$("#cl-popup").dialog("close");
		}).fail(function(xhr) {
			if (xhr.responseText) {
				alert(JSON.parse(xhr.responseText));
			} else {
				logError("AJAX addOrUpdateClass failed: " + JSON.stringify(xhr), 'error');
				alert("Erreur interne à l'enregistrement. Le problème est signalé, mais contactez les administrateurs système si le besoin est urgent.");
			}
		});
	}
}



/***************************************************************************************************
 *                                           LES ETUDIANTS		                                   *
 **************************************************************************************************/


/**
 * fonction deleteLearnerFromClass
 * Permet de supprimer un étudiant
 * Demande confirmation à l'utilisateur avant d'envoyer la requête AJAX de suppression de l'étudiant sélectionné
 * Met à jour le tableau des étudiant une fois terminé
 */
function deleteLearnerFromClass(){
	var idClass = $("#le-table").data('idclass');
	var learner = getSelectedLearnerAttributes();
	if (!learner) {
		return;
	}
	var idLearner = learner.id_user;
	var numClasses = learner.classes_num;
	var fullName = learner.first_name + " " + learner.user_name;
	if (idLearner > 0
		&& idClass > 0){
		$.ajax({
			method: "POST",
			url: "/user/"+idLearner+"/canSoftDeleteLearner",
			success: function(ans) {
				if ( (!ans && confirm("⚠ ⚠ ⚠ " + __("Suppression définitive d'un compte étudiant !\nL'étudiant '{{name}}' va être supprimé de la base LabNBook\nainsi que les rapports n'appartenant qu'à lui seul.",  {name : fullName}) + "\n\n" + __("ATTENTION : cette action est irréversible !") + "\n\n") )
					|| (ans && confirm( __("L'étudiant '{{name}}' va être supprimé de la classe.\nSon compte ne sera pas supprimé car l'étudiant appartient à au moins une autre classe.", {name : fullName}) )) ){
						$.ajax({
							method: "POST",
							url: "/user/"+idLearner+"/deleteLearnerFromClasse",
							data: {
								id_class: idClass
							},
							success: function() {
								updateNumberOfStudents(idClass, -1);
							}
						});
					}
			}
		});
	}
}

/***********************************
 *   popup ajout/edit étudiants	   *
 ***********************************/

/**
 * Initialise le popup d'ajout d'étudiant et réinitialisant la liste des classes associées
 * Ajoute également les classe selectionnées dans le tableau des classes (de gauche)
 */
function openEditLearnerPopup(){

	let lb_class = $("#le-table").data('idclass');
	let learner = getSelectedLearnerAttributes();
	if (!learner) {
		return;
	}
	let lb_learner = learner.id_user;

	// on desabonne TOUTES les actions
	$("#le-popup-valid").off("click") ;
	$("#le-add-first-name").off('input') ;
	$("#le-add-first-name").off('focusout') ;
	$("#le-add-name").off('input') ;
	$("#le-add-name").off('focusout') ;
	$("#le-add-inst-number").off('focusout') ;
	$("#le-add-email").off('input') ;
	$("#le-add-email").off('focusout') ;

	$("#le-add-password").attr('placeholder',"Non modifié") ;
	$("#le-add-send-email-div").hide() ;
	$("#le-popup-valid").on("click", function() {
		addOrUpdateUser(lb_learner, lb_class, '');
	});

	initEditLearnerData(lb_learner, lb_class, true, '');
}

function initEditLearnerData(lb_learner, lb_class, edit, prefix){
	// récupération des infos du popup
	$.ajax({
		method: "POST",
		url: "/teacher/students/initEditLearnerData",
		data: {
			id_learner: lb_learner ? lb_learner : null,
			id_class: lb_class
		},
		success: function(learner_json){
			$("#"+prefix+"le-add-inst").html(learner_json['institutionsHTML']) ; // met en cache le numéro de l'institution de la classe
			if (edit) {
				$("#"+prefix+"le-add-name").val(learner_json['user_name']);
				$("#"+prefix+"le-add-first-name").val(learner_json['first_name']);
				$("#"+prefix+"le-add-email").val(learner_json['email']);
				$("#"+prefix+"le-add-inst-number").val(learner_json['inst_number']);
				$("#"+prefix+"le-add-login").val(learner_json['login']);
				$("#"+prefix+"le-add-password").val("");
				if (learner_json['cas'] == 1) {
					$('#'+prefix+'le-add-cas').text(__(' (compte institutionnel)'));
					$('#'+prefix+'le-add-login').prop('disabled', true);
					$('#'+prefix+'le-add-password').hide();
					$('label[for='+prefix+'le-add-password]').hide();
				} else {
					$('#'+prefix+'le-add-cas').text('');
					$('#'+prefix+'le-add-login').prop('disabled', false);
					$('#'+prefix+'le-add-password').show();
					$('label[for='+prefix+'le-add-password]').show();
				}
				$("#"+prefix+"le-popup").dialog("open");
			}
		}
	});
}

/**
 * mise en place des fonctions sur les inputs
 */
function addLearnerFormFunctions(prefix){
	$("#"+prefix+"le-add-first-name").blur(function() { searchUserBy('name', prefix) ; });
	$("#"+prefix+"le-add-name").blur(function() { searchUserBy('name', prefix) ; });
	$("#"+prefix+"le-add-inst-number").blur(function() { searchUserBy('number', prefix) ; });
	$("#"+prefix+"le-add-email").blur(function() { searchUserBy('email', prefix) ; });
	$("#"+prefix+"le-add-first-name").on('input', function() { autoLogin("first_name", '#'+prefix+'le-add-first-name', '#'+prefix+'le-add-name', '#'+prefix+'le-add-login') ; });
	$("#"+prefix+"le-add-name").on('input', function() { autoLogin("user_name", '#'+prefix+'le-add-first-name', '#'+prefix+'le-add-name', '#'+prefix+'le-add-login') ; });
	$("#"+prefix+"le-add-email").on('input', function() {
		if ($("#"+prefix+"le-add-email").val()) {
			$("#"+prefix+"le-add-password").attr('placeholder',"Généré aléatoirement") ;
			$("#"+prefix+"le-add-send-email").prop('checked', true) ;
		}
		else {
			$("#"+prefix+"le-add-send-email").prop('checked', false) ;
		}
	});
}

/**
 * fonction searchUserBy
 * cherche un étudiant correspondant aux infos saisies 
 * si c'est le cas => propose de charger le profil de l'étudiant
 */
function searchUserBy(what, prefix){
	var var1 = "" ;
	var var2 = "" ;
	if (what == "name") {
		var1 = $.trim($('#'+prefix+'le-add-first-name').val()) ;
		var2 = $.trim($('#'+prefix+'le-add-name').val()) ;
	}
	else if (what == "number") {
		var1 = $.trim($('#'+prefix+'le-add-inst-number').val()) ;
	}
	else if (what == "email") {
		var1 = $.trim($('#'+prefix+'le-add-email').val()) ;
	}
	if ((what != "name" && var1 != "") || (what == "name" && var2 != "" )) {
		// ajax Search
		$.ajax({
			method: "POST",
			url: "/users/getUserWithInstBy",
			data: {
				what: what,
				id_inst: $("#"+prefix+"le-add-inst input").val(),
				var1: var1,
				var2: var2
			},
			success: function(learners){
				console.log(learners);
				if (learners.length === 0) {
					return;
				}
				// affichage d'une fenetre avec des propositions d'étudiant
				var learners_html = __("Il existe des utilisateurs avec des informations similaires dans la base LabNBook.") + "<br/>";
				learners_html += __("Validez pour ajouter l'étudiant existant à votre classe") + __("&nbsp;:") + "<ul>";
				for (var i = 0; i < learners.length; i++) {
					/**
					 * @todo Secure this HTML production
					 */
					learners_html += "<li>" + learners[i].first_name + " " + learners[i].user_name + " (" + learners[i].login + ") - " + learners[i].name;
					if (learners[i].inst_number) {
						learners_html += " n°" + learners[i].inst_number;
					}
					if (learners[i].email) {
						learners_html += " - " + learners[i].email;
					}
					learners_html += "&nbsp;&nbsp;<i class='fa fa-check' title='" + __("Ajouter cet étudiant à votre classe") + "' onclick='addLearnerToClass(" + learners[i].id_user + ")'></i></li>";
				}
				learners_html += "</ul>";
				$("#le-add-popup").find("section").html(learners_html);
				$("#le-add-popup").dialog("option", "position", { my: "left top", at: "left bottom+10", of: "#add-students-method" } );
				$("#le-add-popup").dialog("open");
			}
		});
	}
}

function updateNumberOfStudents(id_class, count){
	var classRow = $("#cl-table").DataTable().row($("#class-" + id_class));
	var value = classRow.data();
	value.nb_learners+=count;
	classRow.data(value).draw();
	highlightClassById(id_class);
	getAndDisplayLearnersTable();
}

// *
// Ajoute un étudiant existant à la classe ouverte
// *

function addLearnerToClass(id_learner) {
	var idClass =$("#le-table").data('idclass');
	$.ajax({
		method: "POST",
		url: "/classe/"+idClass+"/addLearner",
		data: {
			id_user: id_learner,
		},
		success: function(content){
			if(content == ""){
				$("#le-popup").dialog("close");
				$('#add-students').dialog("close");
				resetDialogInputs("le-popup");
				$("#le-add-popup").dialog("close");
				idLearner = id_learner ;
				updateNumberOfStudents(idClass, 1);
				$("#cl-numbst-"+idClass).html(Number($("#cl-numbst-"+idClass).text())+1) ;
			}
			else {
				alertGently(content,'error') ;
				$("#le-add-popup").dialog("close") ;
				$('#add-students').dialog("close");
			}
		}
	});
}


/**
 * Vérifie les entrées et le nombre de classe associé pour l'ajout ou la modification d'un étudiant.
 * @returns vrai si les entrées du popup d'ajout/édition d'étudiant sont valides et que le nombre de classe sélectionné est > 0, faux sinon.
 */
function validateLearnerPopup(id_learner, prefix){
	if (! ATControlString($('#'+prefix+'le-add-first-name'), "Prénom", 1, 32, true)) { return false ; }
	if (! ATControlString($('#'+prefix+'le-add-name'), "Nom", 1, 64, true)) { return false ; }
	if (! ATControlString($('#'+prefix+'le-add-login'), "Intitulé du compte", 1, 20, true)) { return false ; }
	if (!id_learner) { // on est dans le cas d'un ajout
		if (! $('#'+prefix+'le-add-email').val() && ! $('#'+prefix+'le-add-password').val()) {
			alertGently (  __("Vous devez définir un mot de passe (et le fournir à l'étudiant)\nou indiquer une adresse e-mail pour qu'il soit généré aléatoirement et envoyé par e-mail.") ,
				'error' ) ;
			return false ;
		}
		if ($('#'+prefix+'le-add-send-email').prop("checked") && ! $('#'+prefix+'le-add-email').val()) {
			alertGently (  __("Pour envoyer un e-mail récapitulatif, définissez une adresse mail") ,
				'error' ) ;
			return false ;
		}
		if (! $('#'+prefix+'le-add-password').val() && ! $('#'+prefix+'le-add-send-email').prop("checked")) {
			alertGently (  __("Si le mot de passe est généré aléatoirement, il faut envoyer un e-mail récapitulatif à l'utilisateur pour le lui faire passer") ,
				'error' ) ;
			return false ;
		}
	}
	var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/ ;
	if ($('#'+prefix+'le-add-email').val() && !regex.test($('#'+prefix+'le-add-email').val())) {
		alertGently ( __("Le format de l'adresse e-mail n'est pas reconnu"),
			'error') ;
		return false ;
	}
	if ($('#'+prefix+'le-add-password').val() && ! ATControlString($('#'+prefix+'le-add-password'), __("Mot de passe"), 8, 64, false)) {
		alertGently(__('Le mot de passe doit comporter au moins 8 caractères dont une majuscule et une minuscule'));
		return false;
	}
	return true ;
}

/**
 * fonction addOrUpdateUser
 * Récupère les informations saisies dans le popup de gestion des étudiants
 * Vérifies si les informations sont valide (informe l'utilisateur le cas échéant)
 * Si celles ci sont valide alors tentera d'ajouter ou de mettre à jour un utilisateur (décision prise coté serveur en fonction de la présence ou non de id_user)
 */
function addOrUpdateUser(id_learner, id_class, prefix){
	console.log("addOrUpdateUser");
	setTimeout(function(){
		// Wait for searchUserBy to complete before validating popup
		if (!$('#le-add-popup').is(':visible') && validateLearnerPopup(id_learner, prefix)){
			var idClass = id_class == -1 ?$("#le-table").data('idclass'): id_class;
			console.log(idClass);
			$.ajax({
				method: "POST",
				url: "/classe/"+idClass+"/addOrUpdateUser",
				data: {
					id_user: id_learner,
					user_name: $("#"+prefix+"le-add-name").val(),
					first_name: $("#"+prefix+"le-add-first-name").val(),
					id_inst: $("#"+prefix+"le-add-inst input").val(),
					inst_number: $("#"+prefix+"le-add-inst-number").val(),
					email: $("#"+prefix+"le-add-email").val(),
					login: $("#"+prefix+"le-add-login").val(),
					password: $("#"+prefix+"le-add-password").val(),
					send_mail: $('#'+prefix+'le-add-send-email').prop("checked")
				},
				success: function(content){
					if (isNaN(content)) {
						alertGently(content, 'error');
						$('#add-students .popup-submit').show();
					} else {
						$("#le-popup").dialog( "close" );
						addStudentsDialog.close()
						resetDialogInputs("le-popup");
						idLearner = content ;
						updateNumberOfStudents(idClass, 1);
					}
				}
			});
		} else {
			$('#add-students .popup-submit').show();
		}
	},
		100
	);
}



/***********************************
 *   CSV						   *
 ***********************************/

// tableau associé au tableau CSV, modifiable
var csv_global_array = [];

/**
 * fonction activateCsvContentAction
 * active le lien entre le contenu de csv-content et csv_global_array
 */
function activateCsvContentAction(){
	// Avoid duplicating controls
	$("#csv-content").unbind('change');
	$("#csv-content").unbind('click');
	// user edits a line
	$("#csv-content").on('change', 'input', function() {
		var input = $(this);
		var rowId = parseInt(input.closest('tr').data('id'));
		var num = {
			first_name: 0,
			user_name: 1,
			inst_number: 2,
			email: 3,
			login: 4,
			password: 5
		};
		var colId = num[input.attr('name')];
		csv_global_array[rowId][colId] = input.val();
	});

	// user removes a line
	$("#csv-content").on('click', '.cl-delete', function() {
		if (confirm( __("Souhaitez vous ne pas inscrire cet étudiant dans la classe ?") )) {
			var row = $(this).closest('tr');
			csv_global_array.splice(row.data('id'), 1);
			row.remove() ;
			// Re-index data field
			$('#csv-content tr').each(function(e, v) {
				if (e > 0) { // 0 is header row
					v.dataset['id'] = e -1;
				}
			});
			if (csv_global_array.length ==0) {
				var selected_method = $('input[name=student-method]:checked').val();
				addStudentsDialog.setMethod(selected_method);
				alertGently(__("Aucun étudiant n'a été ajouté à la classe"), "warning");
			}
		}
	});

	// Activate submit button
	addStudentsDialog.allowSubmit(true);
}

/**
 * fonction displayCSVPopup
 * Affiche le popup d'ajout CSV
 */
function displayCSVPopup(){
	var classAttributes = getSelectedClassAttributes();
	$("#csv-popup-title").text( __("Ajout d'étudiants dans la classe «{{class}}»", {class : classAttributes.class_name})  ) ;
	$('#csv-load').removeClass('nodisplay');
	$('#le-add-studentnumbers-form').hide();
	$('#csv-load').show() ;
	$('#csv-loading').hide();
	$('#csv-confirm').hide();
	$('#csv-content').show();
	$('#csv-content').html("");
}

/**
 * Get the CSV file and populate the csv_global_array
 */
function getCSVFile() {
	$('#csv-load').hide();
	$('#csv-loading').show();
	readFirstBlock(
		document.querySelector('#csv-input').files[0],
		function(ab) {
			var bytes = new Uint8Array(ab);
			var encoding = detectEncoding(bytes);
			console.log("CSV encoding: ", encoding);
			parseCsv(encoding);
		}
	);

	function parseCsv(encoding) {
		function filterEmptyLines(data) {
			return data.filter(sub=> !sub.every( e => (e === "" || e === null)));
		}
		$('#csv-input').parse({ // library Papa Parse
			config: {
				encoding: encoding,
				// delimiter: ";", // guess if not set
				header: false,
				skipEmptyLines: 'greedy',
				complete: function(results) {
					$('#add-students .alerts').html('');

					results.data.filter
					csv_global_array = filterEmptyLines(results.data);
					$("#add-students").data('genPasswords', true);
					if (!checkAndAddCSVLearners(false)) {
						$('#csv-load').show();
						$('#csv-loading').hide();
					}
				}
			}
		});
	}
	/**
	 * Basic detection of the 3 most frequesnt charsets.
	 *
	 * @param UintArray bytes
	 * @return {String}
	 */
	function detectEncoding(bytes) {
		if (bytes.indexOf(195) !== -1) { // xC3
			return "utf-8";
		} else if (bytes.indexOf(130) !== -1) { // x82
			return "windows-1252";
		} else if (bytes.indexOf(169) !== -1) { // xA9
			return "iso-8859-1";
		} else {
			return "iso-8859-15";
		}
	}
	/**
	 * Read the first 4096 bytes of the file.
	 *
	 * @param File file
	 * @param {function} callback
	 * @return {undefined}
	 */
	function readFirstBlock(file, callback) {
		var r = new FileReader();
		var blob = file.slice(0, 4096);
		r.onload = function(evt) {
			if (evt.target.error === null) {
				callback.call(null, evt.target.result);
			}
		};
		r.readAsArrayBuffer(blob);
	}
}

/**
 * fonction checkAndAddCSVLearners
 * Charge un fichier CSV et le parse
 * Une fois effectué effectue une requete AJAX avec le JSON représentant ce tableau CSV afin de le faire valider par le serveur
 */
function checkAndAddCSVLearners(save){
	var classAttributes = getSelectedClassAttributes();
	var idClass = classAttributes.id_class;
	var send_mail = $('#le-add-send-email').prop('checked');
    $("body").css("cursor", "progress");
	if (csv_global_array.length ==0) {
		alertGently(__("Aucun étudiant à ajouter"), "error");
		return false;
	}
	$.ajax({
		method: "POST",
		url: "/classe/"+idClass+"/checkAndAddCSVLearners",
		data: {
			json_array: csv_global_array,
			save: save,
			send_mail: send_mail,
			genPasswords: $("#add-students").data('genPasswords') || false
		},
		success: function(data){
            $("body").css("cursor", "default");
			$('#csv-loading').hide();
			$('#csv-confirm').show();
			// console.log(content) ;
			if (data.save) { // l'enregistrement des données a été fait : on affiche le tableau de la classe nouvellement remplie
				addStudentsDialog.close()
				updateNumberOfStudents(idClass, data.nb_insert);
				
				var listOfStudentsEmailAddresses;
				if(data.nb_mail_sent>8){
					listOfStudentsEmailAddresses = "( " + data.email_addresses_invited.join(', ') + "...)";
				} else {
					listOfStudentsEmailAddresses = "( " + data.email_addresses_invited.join(', ') + ")";
				}
				
				alertGently(__("{{nbe}} étudiants ajoutés à la classe et {{nbm}} mails envoyés", {nbe: data.nb_insert, nbm:data.nb_mail_sent}) + __("&nbsp;:") + listOfStudentsEmailAddresses, 'info', 10000);
			} else { // on affiche le tableau envoyé avec les modifs évaluées
				// console.log(content) ;
				document.getElementById('csv-content').innerHTML = data['html'];
				$("tr.locked input:not([name=email]), tr.already-in-db input").attr('disabled', true);
				csv_global_array = data['data'];
				activateCsvContentAction()
				if(save){
					alertGently(__("Impossible d'importer les étudiants, veuillez corriger "), 'error');
				}
			}
		},
        error: function(data){
            $("body").css("cursor", "default");
			alertGently(__("Erreur inconnue lors de l'import"), 'error');
        }
	});
	return true;
}

/**
 * Select all students available for add
 */
function addStudentsToggleSelectAll(){
	if(typeof(addStudentsToggleSelectAll.state) == "undefined"){
		addStudentsToggleSelectAll.state = 0;
	}
	selector = '#le-add-student-class-students input[type=checkbox]';
	if(addStudentsToggleSelectAll.state == 1){
		selector += ':checked';
		addStudentsToggleSelectAll.state = 0;
	}else{
		selector += ':not(:checked)';
		addStudentsToggleSelectAll.state = 1;
	}
	$(selector).each(function(){
		this.click()
	});
}

/**
 * addSelectedStudentsToClass
 * Add all students selected in the add-students popup to the class
 */
function addSelectedStudentsToClass(){
	var checked = $('#le-add-student-class-students input[type=checkbox]:checked');
	var learners = checked.map(function(){return $(this).val();}).get();
	var id_class = $("#le-table").data('idclass');
	$.ajax({
		method: "POST",
		url: "/classe/"+id_class+"/addLearners",
		data: {
			learners: learners,
		},
		success: function(data){
			if(data.failed.length > 0){
				// Alert gently errors
				html = "<p>" + data.message + "</p><ul>";
				for(var id in data.failed){
					uid = data.failed[id];
					name = $('label[for=le-add-checkbox-'+uid+']').text();
					html += "<li>" + $('<div>').text(name).html() + "</li>";
				}
				html += "</ul>";
				alertGently(html, 'warning', 5000);
			}
			addStudentsDialog.close();
			// Refresh cl datatable
			updateNumberOfStudents(id_class, data.success.length);
		}
	});
}
/**
 * Update in db the class code value of class (id_class)
 */
function updateClassCode() {
	if (!ATControlString($('#add-students-le-add-class-code'), "Code", 0, 255, true)) {
		return false ;
	}
	const class_code = document.getElementById('add-students-le-add-class-code').value;
	const id_class = $("#le-table").data('idclass');
	$.ajax({
		method: "POST",
		url: "/classe/" + id_class + "/updateClassCode",
		data: {
			class_code: class_code,
		}
	}).done(function(data){
		addStudentsDialog.close();
		$("#cl-table").DataTable().ajax.reload(function(){
			highlightClassById(id_class);
			getAndDisplayLearnersTable();
		});
	}).fail(function(jqXHR) {
		addStudentsDialog.allowSubmit(true);
		if (jqXHR.status === 403) {
			alertGently(__('Votre session a expiré. Merci de vous reconnecter.'), 'error');
		} else if(jqXHR.status === 422) {
			alertGently(__('Ce code de classe est déjà utilisé, veuillez en choisir un nouveau'), 'warning');
		}
	});
}

// Query LDAP for students numbers
function queryLdapDirectory(){
	var popup = $("#add-students");
	var numbers = popup.find('textarea').val();
	if (numbers.trim() === "") {
		alertGently(__("Veuillez saisir des numéros étudiants avant d'interoger l'annuaire"), "warning");
		return;
	}
	var classAttributes = getSelectedClassAttributes();
	popup.find('.alerts').html('');
	$.ajax({
		method: "POST",
		url: "/teacher/students/findAllByStudentNumbers",
		data: {
			institute: classAttributes.id_inst,
			studentNumbers: numbers
		}
	}).done(function(response) {
		if (response.found.length > 0) {
			popup.find('.alerts').html('');
			popup.find('textarea').val('');
			csv_global_array = response.found; // global var, erk
			displayCSVPopup();
			if (response.missing.length > 0) {
				alertGently(
					__("Les numéros suivants n'ont pas été trouvés dans l'annuaire de l'établissement") + __("&nbsp;:") + " <strong>"
					+ response.missing.join(" ")
					+ "</strong>."
					, 'warning');
			} else {
				$('#add-students .alerts').html('');
			}
			$('#csv-load').hide();
			$("#add-students").data('genPasswords', false);
			checkAndAddCSVLearners(false);
		} else {
			if (response.missing.length === 0) {
				alertGently(__("Aucun numéro valide n'a été trouvé dans votre saisie"), 'warning');
			} else {
				alertGently(__("Aucune correspondance n'a été trouvée dans l'annuaire de l'établissement à partir des données fournies"), 'warning');
			}
		}
	}).fail(function(jqXHR) {
		let message = __("Une erreur s'est produite lors de l'enregistrement");
		if (jqXHR.status == 403) {
			message = __("Votre session a expiré. Merci de vous reconnecter.");
		} else {
			try {
				message = decodeURIComponent(JSON.parse(jqXHR.responseText).error);
			} catch(e) {}
		}
		logError("findAllByStudentNumbers() " + message + jqXHR.responseText, 'danger');
		alertGently(message, 'error');
	});
}
