/**
 * Function variable.
 * @return {Function} anonymous function that draws the heatmap.
 */
var tracesHeatmap = (function(){
	// The data retrieved from DB, array of dict with the following keys: 'id_report', 'team_name', 'action_time'
	var data = [];
	// List of parameters filled in public/js/teacher/reports.js
	var params = {};
	// List of arrays computed by formatData, corresponding to the working slots / all slots: 
	// x: list of date times (list of slots).
	// y: list of teams name.
	// z: production time for a given x and a given y.
	// zTop: sum of production times for a given x.
	// zRight: sum of production times for a given y.
	// xLabels: list of date times labels.
	var workingSlotsXYZ = [];
	var allSlotsXYZ = [];
	// List of objects that defines the shapes to plot, corresponding to the working slots / all slots:
	var workingDaysShapes = [];
	var allDaysShapes = [];
	// Graph DOM element.
	var graphDiv;
	// The trace (in plotly jargon) of the heatmap, the top barchart and the left one
	var traceHeatmap;
	var traceTopMarginal;
	var traceRightMarginal;
	// List of all traces
	var graphData = [];
	// Button mechanism to hide/show empty days.
	var updateMenus;
	// Main layout
	var layout;
	// Icon menu
	var config;


	/**
	 * Main method that draws the heatmap.
	 * @param {Array} initData: data formatted as array of dict with the following keys: 'id_report', 'team_name', 'action_time'.
	 * @param {Object} initParams: list of parameters filled in public/js/teacher/reports.js
	 * Uncomment the first line to simulate data.
	 */
	function drawHeatmap(initData, initParams){
		// var initData = setMockData(teamsNumber=10, daysNumber=30, idReport=4);
		overwriteParams(initParams);
		filterGroupByAndCount(initData);
		if (data.length == 0){ return false;};
		workingSlotsXYZ = formatData(false);
		allSlotsXYZ = formatData(true);
		updateParams();
		workingDaysShapes = buildShapes(workingSlotsXYZ);
		allDaysShapes = buildShapes(allSlotsXYZ);
		setGraphDiv();
		traceHeatmap = buildHeatmap(allSlotsXYZ);
		traceTopMarginal = buildTopMarginal(allSlotsXYZ);
		traceRightMarginal = buildRightMarginal(allSlotsXYZ);
		setGraphData();
		buildUpdateMenus();
		layout = buildLayout(allDaysShapes, allSlotsXYZ);
		buildConfig();
		Plotly.newPlot(graphDiv, graphData, layout, config);
		setTitle();
		registerOrderEvent();
		return true;
	}

	
	/**
	 * Update heatmap when a user sort the reports dataTable.
	 * @param {Array} reportsId: list reports ids.
	 */
	function updateHeatmap(reportsId){
		params.reportsOrder = reportsId;
		let update = {'yaxis.categoryarray': getYOrderedList().reverse()};
		Plotly.relayout(graphDiv, update);
	}

	
	/**
	 * Overwrite default parameters with custom parameters.
	 * @param {Object} userParams: list of parameters filled in public/js/teacher/reports.js
	 */
	function overwriteParams(userParams) {
		params = {
			// Slots Labels
			slotsLabel: ['matin', 'après-midi', 'soir/nuit'],
			// Charts spaces and dimensions in px
			spaceBetweenChartsX: 35,
			spaceBetweenChartsY: 35,
			rightMarginalWidth: 100,
			topMarginalHeight: 100,
			binSizeX: 15,
			binSizeY: 20,
			// Margins in px
			lmLayout: 120,
			rmLayout: 50,
			bmLayout: 100,
			tmLayout: 50,
			// Display all x labels or just one out n
			labelTextStep: 3,
			// heatmap colorscale
			colorscale: [
				['0.000', 'rgb(209, 220, 238)'],
				['0.500', 'rgb(139, 168, 211)'],
				// LabNBook official blue color:
				['1.000', 'rgb(69, 115, 184)']
			],
		};
		Object.assign(params, userParams);
	}


	/**
	 * Perform a filter and a group by operation (over the slots starting) times and count production times.
	 * /!\ initData must be ordered by id_report asc, action_times asc.
	 * @param {Array} initData (see above).
	 * @return {Array} data: array of dict with the following keys: 'id_report', 'start_day_slot', 'start_day_slot', 'production'.
	 */
	function filterGroupByAndCount(initData){
		data = initData
			.filter(function(row){
				return params.reportsOrder.toArray().includes(parseInt(row['id_report']));
			})
			.map(function(row){
				row['start_day_slot'] = getSlotStart(row['action_time'], params.slotsEdges);
				row['production'] = parseFloat(row['production']);
				return row;
			})
			.reduce(function(acc, cur){
				if (acc.length > 0
					&& acc[acc.length - 1]['id_report'] == cur['id_report']
					&& acc[acc.length - 1]['start_day_slot'] == cur['start_day_slot']){
					acc[acc.length - 1]['production'] += cur['production'];
				} else {
					acc.push({
						'id_report': cur['id_report'],
						'team_name': cur['team_name'],
						'start_day_slot': cur['start_day_slot'],
						'production': cur['production']
					});
				}
				return acc;
			}, []);
	}


	/**
	 * Compute the starting slot time related to a dateTime.
	 * @param {String} dateTimeStr: dateTime string (from DB).
	 * @param {Array} slotsEdges: defined in params variable.
	 * @return {String} the starting slot time (a date with one of slotsEdges).
	 */
	function getSlotStart(dateTimeStr, slotsEdges){
		// Compute slotsEdges timestamps
		let slotsTimestamps = slotsEdges
			.map(x => x.split(':').map(y => parseInt(y)))
			.map(x => new Date(dateTimeStr).setHours(x[0], x[1], x[2]))
			.sort((a, b) => b - a);
		// Add previous timestamp
		let previousSlot = new Date(slotsTimestamps[0]).setDate(new Date(slotsTimestamps[0]).getDate() - 1);
		slotsTimestamps.push(previousSlot);
		// Find the closest smaller slot edge
		let dateTimestamp = new Date(dateTimeStr).getTime();
		let slot = slotsTimestamps.find(curTimestamp => dateTimestamp >= curTimestamp);
		// Convert to string (respecting the timezone offset)
		let slotDate = new Date(slot - (new Date(slot).getTimezoneOffset() * 60 * 1000));
		return slotDate.toISOString().replace('T', ' ').replace('.000Z', '');
	}

	
	/**
	 * Format data to send to plotly.
	 * @param {Boolean} fill: specifies if date time axis has to be filled with slots showing no working time.
	 * @return {Array} list of the following arrays:
	 * 	x: list of date times (list of slots).
	 * 	y: list of teams name.
	 * 	z: production time for a given x and a given y.
	 * 	zTop: sum of production times for a given x.
	 * 	zRight: sum of production times for a given y.
	 * 	xLabels: list of date times labels.
	 */
	function formatData(fill=false){
		let x = Array
			.from(new Set(data.map(x => x.start_day_slot)))
			.sort((a, b) => new Date(a) - new Date(b));
		x = fill ? fillSlots(x[0], x[x.length - 1], params.slotsEdges) : fillWorkingDaySlots(x);
		let y = Array
			.from(new Set(data.map(x => x.team_name)))
			.sort();
		let z = [...Array(y.length)].map(e => Array(x.length).fill(null));
		data.forEach(function(elt){
			let xIdx = x.indexOf(elt['start_day_slot']);
			let yIdx = y.indexOf(elt['team_name']);
			z[yIdx][xIdx] = elt['production'];
		});
		let zTop = z.reduce(function(acc, cur){
			return cur.map(function(val, idx){
				return acc[idx] + val;
			});
		}, new Array(x.length).fill(0));
		let zRight = z.map(function(cur){
			return cur.reduce((a, c) => a + c, 0);
		});
		let xLabels = formatXTicks(x)
			.map(function(tick, i){
				return i%params.labelTextStep == 0 ? tick : '';
			});
		return [x, y, z, zTop, zRight, xLabels];
	}

	/**
	 * Fill filtered working day slots based on existing edges
	 * @param datesArray
	 * @returns {*}
	 */
	function fillWorkingDaySlots(datesArray) {
		return Array.from(new Set(
			datesArray.flatMap(dateTime => {
				const date = dateTime.split(' ')[0];
				return params.slotsEdges.map(time => `${date} ${time}`);
			})
		));
	}

	/**
	 * Fill the slots series with all missing dates and slots.
	 * @param {String} firstDayAndSlot: starting date slot.
	 * @param {String} lastDayAndSlot: ending date slot.
	 * @param {Array} slotsEdges: defined in params variable.
	 * @return {Array} the label to be displayed instead.
	 */
	function fillSlots(firstDayAndSlot, lastDayAndSlot, slotsEdges){
		slotsEdges = slotsEdges.map(x => x.split(':').map(y => parseInt(y)));
		let curSlotIdx = 0;
		let curSlot = slotsEdges[curSlotIdx];
		let curDate = getSlot(firstDayAndSlot, curSlot);
		//let curDate = new Date(new Date(firstDayAndSlot).setHours(curSlot[0], curSlot[1], curSlot[2]));
		let lastSlotIdx = slotsEdges.length - 1
		let lastSlot = slotsEdges[lastSlotIdx];
		let lastDate = getSlot(lastDayAndSlot, lastSlot);
		//let lastDate = new Date(new Date(lastDayAndSlot).setHours(lastSlot[0], lastSlot[1], lastSlot[2]));
		let slotsList = [];
		while (curDate <= lastDate){
			let slotDate = new Date(curDate - (new Date(curDate).getTimezoneOffset() * 60 * 1000));
			slotsList.push(slotDate.toISOString().replace('T', ' ').replace('.000Z', ''));
			let nextCurSlotIdx = (curSlotIdx + 1) % (slotsEdges.length);
			let nextSlot = slotsEdges[nextCurSlotIdx];
			if (nextSlot == slotsEdges[0]){
				curDate.setDate(curDate.getDate() + 1);
			}
			let nextDate = getSlot(curDate, nextSlot);
			//let nextDate = new Date(new Date(curDate).setHours(nextSlot[0], nextSlot[1], nextSlot[2]));
			curDate = nextDate;
			curSlotIdx = nextCurSlotIdx;
			curSlot = nextSlot;
		}
		return slotsList;
	}


	/**
	 * Get the corresponding slot of a dateTime string.
	 * @param {String} dateTimeString: original x values (date time string).
	 * @param {Array} slot: array of integers [hour, minute, second].
	 * @return {Date} the slot date.
	 */
	function getSlot(dateTimeString, slot) {
		return new Date(new Date(dateTimeString).setHours(slot[0], slot[1], slot[2]));
	}
	/**
	 * Format a date time string to a text which will be displayed as a part of a tooltip on the heatmap.
	 * @param {String} xValue: original x values (date time string).
	 * @param {Array} slotsEdges: defined in params variable.
	 * @param {Array} slotsLabel: defined in params variable.
	 * @return {String} the label to be displayed instead.
	 */
	function formatXHover(xValue, slotsEdges, slotsLabel){
		let dateAndTime = xValue.split(' ');
		let dateTime = new Date(xValue);
		let options = { weekday: 'short', month: 'short', day: 'numeric' };
		let date = dateTime.toLocaleString(langLong().replace('_', '-'), options);
		let time = slotsLabel[slotsEdges.findIndex(u => u == dateAndTime[1])];
		return date + ' (' + time + ')';
	}
		
	
	/**
	 * Format x labels on the x-axis of the heatmap.
	 * @param {String} xValues: original x values (date time string).
	 * @return {String} the label to be displayed instead.
	 */
	function formatXTicks(xValues){
		let xLabels = xValues.map(function(x){
			let dateAndTime = x.split(' ');
			let monthDate = new Date("2022-"+ dateAndTime[0].split('-')[1] +"-15");
			let month = monthDate.toLocaleString(langLong().replace('_', '-'), {'month':'short'});
			let date = dateAndTime[0].split('-').reverse()[0]+ '<br />' + month;
			return date;
		});
		xLabels[0] = xLabels[0] + ' <br /> ' + xValues[0].split(' ')[0].split('-')[0];
		return xLabels;
	}
	
	
	/**
	 * Computes new parameters (mainly to adjust widths and heights) thanks to data formatting.
	 */
	function updateParams(){
		// When showing all days (default view)
		params['heatmapWidthAllDays'] = allSlotsXYZ[0].length * params.binSizeX;
		params['chartWidthAllDays'] = params.heatmapWidthAllDays + params.spaceBetweenChartsX + params.rightMarginalWidth
		params['chartTotalWidthAllDays'] = params.lmLayout + params.chartWidthAllDays + params.rmLayout;
		params['heatmapXEndAllDays'] = params.heatmapWidthAllDays / params.chartWidthAllDays;
		params['rightMarginalXStartAllDays'] = (params.chartWidthAllDays - params.rightMarginalWidth) / params.chartWidthAllDays;
		
		// When showing only Working days
		params['heatmapWidthWorkingDays'] = workingSlotsXYZ[0].length * params.binSizeX;
		params['chartWidthWorkingDays'] = params.heatmapWidthWorkingDays + params.spaceBetweenChartsX + params.rightMarginalWidth
		params['chartTotalWidthWorkingDays'] = params.lmLayout + params.chartWidthWorkingDays + params.rmLayout;
		params['heatmapXEndWorkingDays'] = params.heatmapWidthWorkingDays / params.chartWidthWorkingDays;
		params['rightMarginalXStartWorkingDays'] = (params.chartWidthWorkingDays - params.rightMarginalWidth) / params.chartWidthWorkingDays;

		// y axis is not affected by the switch of views: allSlotsXYZ[1] = workingSlotsXYZ[1]
		params['heatmapHeight'] = allSlotsXYZ[1].length * params.binSizeY;
		params['chartHeight'] = params.heatmapHeight + params.spaceBetweenChartsY + params.topMarginalHeight;
		params['chartTotalHeight'] = params.bmLayout + params.chartHeight + params.tmLayout;
		params['heatmapYEnd'] = params.heatmapHeight / params.chartHeight;
		params['topMarginalYStart'] = (params.chartHeight - params.topMarginalHeight) / params.chartHeight;
		params['buttonYEnd'] = (params['chartHeight'] + params.tmLayout + 15) / params['chartHeight'];
		params['colorBarHeight'] = params.topMarginalHeight;
	}


	/**
	 * Compute arrays of grey shapes for extra classes times.
	 * @param {Array} slotsXYZ: formatted data (list of arrays) computed by formatData.
	 * References: https://plotly.com/javascript/reference/layout/shapes/
	 */
	function buildShapes(slotsXYZ) {
		let [x, y, z, zTop, zRight, xLabels] = slotsXYZ;
		let xShapes = x.filter(dateString => dateString.split(' ')[1] == params.slotsEdges[params.slotsEdges.length - 1])
		let shapes = xShapes
			.map((xs) => {
				let shapeObj = [
					{
						type: 'rect',
						xref: 'x',
						yref: 'paper',
						x0: x.findIndex(elt => elt == xs) - 0.5,
						y0: 0,
						x1: x.findIndex(elt => elt == xs) + 0.5,
						y1: params.heatmapYEnd,
						fillcolor: 'rgb(226, 226, 226)',
						opacity: 0.5,
						line: {width: 0},
						layer:'below',
					},
					{
						type: 'rect',
						xref: 'x',
						yref: 'paper',
						x0: x.findIndex(elt => elt == xs) - 0.5,
						y0: params.topMarginalYStart,
						x1: x.findIndex(elt => elt == xs) + 0.5,
						y1: 1,
						fillcolor: 'rgb(226,226,226)',
						opacity: 0.5,
						line: {width: 0},
						layer:'below',
					}
				]
				return shapeObj;
			})
			.reduce((result, shapeObj) => {return result.concat(shapeObj);}, []);
		return shapes;
	}


	/**
	 * Define graph DOM element.
	 */
	function setGraphDiv(){
		graphDiv = document.getElementById(params.graphDiv.attr('id'));
	}


	/**
	 * Define the required variables to plot the plotly heatmap.
	 * @param {Array} slotsXYZ: formatted data (list of arrays) computed by formatData.
	 * @return {Object}: variables, defined here https://plotly.com/javascript/reference/heatmap/
	 */
	function buildHeatmap(slotsXYZ) {
		let [x, y, z, zTop, zRight, xLabels] = slotsXYZ;

		let traceHeatmap = {
			x: x,
			y: y,
			z: z,
			type: 'heatmap',
			colorscale: params.colorscale,
			reversescale: false,
			showscale: true,
			colorbar:{
				orientation: 'v',
				lenmode: 'pixels',
				len: params.colorBarHeight,
				ticks: 'outside',
				ticksuffix: ' min',
				x: 1,
				xanchor: 'right',
				y: 1,
				yanchor: 'top',
			},
			hovertemplate:
				" <br>" +
				"%{y}<br>" +
				"%{text}<br>" +
				"%{z:.1~f} " + __("min d'écriture") +
				"<extra></extra>",
			text: z.map(yRow => yRow.map((xElt, idx) => formatXHover(x[idx], params.slotsEdges, params.slotsLabel))),
			hoverongaps: false,
		};
		return traceHeatmap;
	}


	/**
	 * Define the required variables to plot the plotly bar chart above the heatmap.
	 * @param {Array} slotsXYZ: formatted data (list of arrays) computed by formatData.
	 * @return {Object}: variables, defined here https://plotly.com/javascript/reference/bar/
	 */
	function buildTopMarginal(slotsXYZ) {
		let [x, y, z, zTop, zRight, xLabels] = slotsXYZ;

		let traceTopMarginal = {
			x: x,
			y: zTop,
			marker: {
				color: 'rgb(186, 186, 186)',
			},		yaxis: 'y2',
			type: 'bar',
			showlegend: false,
			hovertemplate:
				" <br>" +
				"%{text}<br>" +
				"%{y:.1~f} " + __("min d'écriture") +
				"<extra></extra>",
			text: x.map(e => formatXHover(e, params.slotsEdges, params.slotsLabel)),
			textposition: 'none',
		};
		return traceTopMarginal;
	}


	/**
	 * Define the required variables to plot the plotly bar chart on the right side of the heatmap.
	 * @param {Array} slotsXYZ: formatted data (list of arrays) computed by formatData.
	 * @return {Object}: variables, defined here https://plotly.com/javascript/reference/bar/
	 */
	function buildRightMarginal(slotsXYZ) {
		let [x, y, z, zTop, zRight, xLabels] = slotsXYZ;

		let traceRightMarginal = {
			x: zRight,
			y: y,
			orientation: 'h',
			marker: {
				color: 'rgb(186, 186, 186)',
			},
			xaxis: 'x2',
			type: 'bar',
			showlegend: false,
			hovertemplate:
				" <br>" +
				"%{y}<br>" +
				"%{x:.1~f} " + __("min d'écriture") +
				"<extra></extra>"
		};
		return traceRightMarginal;
	}


	/**
	 * Concatenate the 3 graphs.
	 */
		function setGraphData(){
		graphData = [traceHeatmap, traceTopMarginal, traceRightMarginal];
	}


	/**
	 * Build the button mechanism to hide/show empty days.
	 * Reference: https://plotly.com/javascript/reference/layout/updatemenus/
	 */
	function buildUpdateMenus(){
		updateMenus = [
			{
				buttons: [
					{
						args2: [
							{
								x: [allSlotsXYZ[0], allSlotsXYZ[0], allSlotsXYZ[4]],
								y: [allSlotsXYZ[1], allSlotsXYZ[3], allSlotsXYZ[1]],
								z: [allSlotsXYZ[2], '', ''],
								text: [
									allSlotsXYZ[2].map(yRow => yRow.map(
										(xElt, idx) => formatXHover(allSlotsXYZ[0][idx], params.slotsEdges, params.slotsLabel))
									),
									allSlotsXYZ[0].map(e => formatXHover(e, params.slotsEdges, params.slotsLabel)), 
									''
								]
							},
							{
								width: params.chartTotalWidthAllDays,
								'xaxis.type': 'category',
								'xaxis.tickvals': allSlotsXYZ[0],
								'xaxis.ticktext': allSlotsXYZ[5],
								'xaxis.domain': [0, params.heatmapXEndAllDays],
								'xaxis2.domain': [params.rightMarginalXStartAllDays, 1],
								'shapes': allDaysShapes
							},
							[0, 1, 2]
						],
						label: __('Jours travaillés'),
						method: 'update',
						args: [
							{
								x: [workingSlotsXYZ[0], workingSlotsXYZ[0], workingSlotsXYZ[4]],
								y: [workingSlotsXYZ[1], workingSlotsXYZ[3], workingSlotsXYZ[1]],
								z: [workingSlotsXYZ[2], '', ''],
								text: [
									workingSlotsXYZ[2].map(yRow => yRow.map(
										(xElt, idx) => formatXHover(workingSlotsXYZ[0][idx], params.slotsEdges, params.slotsLabel))
									),
									workingSlotsXYZ[0].map(e => formatXHover(e, params.slotsEdges, params.slotsLabel)),
									''
								]
							},
							{
								width: params.chartTotalWidthWorkingDays,
								'xaxis.type': 'category',
								'xaxis.tickvals': workingSlotsXYZ[0],
								'xaxis.ticktext': workingSlotsXYZ[5],
								'xaxis.domain': [0, params.heatmapXEndWorkingDays],
								'xaxis2.domain': [params.rightMarginalXStartWorkingDays, 1],
								'shapes': workingDaysShapes
							},
							[0, 1, 2]
						],
					},
				],
				direction: 'top',
				pad: {'r': 0, 'l': 0, 't': 0, 'b': 0},
				showactive: true,
				type: 'buttons',
				x: 0,
				xanchor: 'left',
				y: params.buttonYEnd,
				yanchor: 'top',
				active: 1
			}
		];
	}


	/**
	 * Build the main layout.
	 * @param {Object} shapes: defined by buildShapes.
	 * @param {Array} slotsXYZ: formatted data (list of arrays) computed by formatData.
	 * Reference: https://plotly.com/javascript/reference/layout/
	 */
	function buildLayout(shapes, slotsXYZ) {
		let [x, y, z, zTop, zRight, xLabels] = slotsXYZ;

		let layout = {
			hoverlabel: {bgcolor: 'white'},
			xaxis: {
				domain: [0, params.heatmapXEndAllDays],
				type: 'category',
				automargin: false,
				titlefont: {size: 16},
				mirror: 'all',
				ticks: 'outside',
				showline: true,
				linecolor: 'rgb(186, 186, 186)',
				rangeselector: true,
				showspikes: true,
				spikemode: 'across+marker',
				spikecolor: 'rgb(186, 186, 186)',
				spikethickness: 1,
				tickangle: 0,
				tickvals: x,
				ticktext: xLabels,
			},
			yaxis: {
				domain: [0, params.heatmapYEnd],
				type: 'category',
				categoryorder: 'array',
				categoryarray: getYOrderedList().reverse(),
				automargin: false,
				mirror: 'all',
				ticks: 'outside',
				showline: true,
				linecolor: 'rgb(186, 186, 186)',
				showspikes: true,
				spikemode: 'across+marker',
				spikecolor: 'rgb(186, 186, 186)',
				spikethickness: 1,
			},
			xaxis2: {
				domain: [params.rightMarginalXStartAllDays, 1],
				showgrid: true,
				zeroline: true,
				zerolinecolor: 'rgb(186, 186, 186)',
				linecolor: 'rgb(186, 186, 186)',
				title: {
					text: __('Temps d\'écriture <br />par équipe (min)'),
				},
				mirror: true,
				ticks: 'outside',
				showline: true,
			},
			yaxis2: {
				domain: [params.topMarginalYStart, 1],
				showgrid: true,
				zeroline: true,
				zerolinecolor: 'rgb(186, 186, 186)',
				linecolor: 'rgb(186, 186, 186)',
				title: {
					text: __('Temps d\'écriture <br />par date (min)'),
					standoff: 20,
				},
				mirror: true,
				ticks: 'outside',
				showline: true,
			},
			autosize: false,
			paper_bgcolor: 'white',
			plot_bgcolor: 'white',
			margin: {
				l: params.lmLayout,
				r: params.rmLayout,
				b: params.bmLayout,
				t: params.tmLayout,
				pad: 0
			},
			bargap: 0.05,
			shapes: shapes,
			modebardisplay: false,
			updatemenus: updateMenus,
			width: params.chartTotalWidthAllDays,
			height: params.chartTotalHeight,
		};
		return layout;
	}


	/**
	 * Sort team names according to a given list of reports ids.
	 */
	function getYOrderedList(){
		return [...data]
			.sort(function(a, b){
				return params.reportsOrder.indexOf(parseInt(a['id_report'])) - params.reportsOrder.indexOf(parseInt(b['id_report']));
			})
			.map(x => x.team_name);
	}
	

	/**
	 * Build the icon menu.
	 * Reference: https://plotly.com/javascript/configuration-options/
	 */
	function buildConfig() {
		config = {
			scrollZoom: false,
			showLink: false,
			displaylogo: false,
			displayModeBar: true,
			locale: 'fr',
			modeBarButtonsToRemove: ['auto', 'zoomIn', 'zoomOut', 'resetScale2d', 'pan2d', 'lasso2d', 'toImage', 'select'],
		};
	}


	/**
	 * Set the title of the graph.
	 */
	function setTitle(){
		($("div#heatmap-div").html() !== "") && $("#heatmap-title").text(__("Répartition des temps d'écriture"));
	}

	
	/**
	 * Register the dataTable sorting event.
	 */
	function registerOrderEvent(){
		$('#reports-table').on( 'order.dt', function (event) {
			updateHeatmap(reportsTable.getReportsId());
		});
	}


	/**
	 * Mock data from data base.
	 * @param {Int} teamsNumber.
	 * @param {Int} daysNumber.
	 */
	function setMockData(teamsNumber=1, daysNumber=30, idReportForce=0){
		let currDateStr = '2025-02-05 08:00:00';
		let mockedData = []
		for (let teamIdx=0; teamIdx<teamsNumber; teamIdx++){
			let teamName = 'Equipe_' + teamIdx.toString();
			let idReport = idReportForce!== 0 ? idReportForce : (1000*(teamIdx+1)+teamIdx+1).toString();
			let productionValue = (Math.random() * (10 - 0.1) + 0.1).toFixed(0);
			let startDaySlot = '2025-02-05 07:30:00';
			for (let dayIdx=0; dayIdx<daysNumber; dayIdx++){
				let n = Math.floor(Math.random() * 6);
				let h = Math.floor(Math.random() * 24);
				let m = Math.floor(Math.random() * 60);
				let s = Math.floor(Math.random() * 60);
				let p = Math.floor(Math.random() * 4);
				for (let idx=0; idx<n; idx++) {
					if ((dayIdx + 1 == daysNumber) || (dayIdx == 0) || (p == 0)) {
						let actionTime = new Date(currDateStr).setDate(new Date(currDateStr).getDate() + dayIdx)
						actionTime = new Date(actionTime).setHours(Math.min(h+n, 23), m, s);
						actionTime = new Date(actionTime - (new Date(actionTime).getTimezoneOffset() * 60 * 1000));
						actionTime = actionTime.toISOString().replace('T', ' ').replace('.000Z', '');
						let elt = {"id_report": idReport, "team_name": teamName, "action_time": actionTime, "production": productionValue, "startDaySlot":startDaySlot};
						mockedData.push(elt)
					}
				}
			}
		}
		return mockedData;
	}
	
	// Main return
	return {
		draw: (initData, initParams) => drawHeatmap(initData, initParams),
		getSlotStart: (dateTimeStr, slotsEdges) => getSlotStart(dateTimeStr, slotsEdges),
		formatDateString: (dateTimeStr, slotsEdges, slotsLabel) => formatXHover(dateTimeStr, slotsEdges, slotsLabel)
	}
})();


/**
 * @return {Function} anonymous function that transforms data an draws the collaborative dashboard.
 */
var collabDashboard = (function(){
	var data = [];
	var structure = [];
	var params = {};
	var usersData = [];
	var slotsSample = [];
	var config;

	/**
	 * Main method that draws the collab dashboard.
	 * @param {Array} initstructure: report's structure (ordered list of labdocs)
	 * @param {Array} initData: data formatted as array of dict with the following keys: 
	 * 	id_labdoc, labdoc_name, labdoc_position, labdoc_type, report_part_name, report_part_position
	 * 	and	sorted by report_part_position (asc) and labdoc_position (asc)
	 * @param {Object} initParams: list of parameters filled in public/js/teacher/reports.js
	 */
	function drawCollabDashboard(initStructure, initData, initParams){
		overwriteParams(initParams);
		transformData(initData);
		formatStructure(initStructure);
		if (data.length == 0 || structure.length == 0){ return false;};
		splitDataByUser();
		computeIndicators();
		computeSlotSample();
		updateParams();
		let traces = buildTraces();
		let h_shapes = buildHorizontalShapes();
		let v_shapes = buildVerticalShapes();
		let images = buildImages();
		let annotations = buildAnnotations();
		let layout = buildLayout([...h_shapes, ...v_shapes], images, annotations);
		let graphDiv = setGraphDiv();
		buildConfig();
		Plotly.newPlot(graphDiv, traces, layout, config);
		setTitle();
		return true;
	}


	/**
	 * Overwrite default parameters with custom parameters.
	 * @param {Object} userParams: list of parameters filled in public/js/teacher/reports.js
	 */
	function overwriteParams(userParams) {
		params = {
			slotsEdges: ['07:30:00', '13:00:00', '18:30:00'],
			slotsLabel: [__('matin'), __('après-midi'), __('soir/nuit')],
			//offsetX: 0.25,
			// Height of each labdoc in px
			rowHeightY: 20,
			// Margins in px
			lmLayout: 250,
			rmLayout: 50,
			bmLayout: 80,
			tmLayout: 50,
			// Whole plotting area in px
			plotAreaWidth: 1015,
			// If a user modifies a labdoc twice and elapsed time between those modifications
			// stands below maxMergingSeconds, the corresponding traces will be merged
			maxMergingSeconds: 15,
			truncateLabels: 31,
			// colorblind friendly palette
			colorPalette: ["#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7", "#000000"],
		};
		Object.assign(params, userParams);
	}

	
	/**
	 * Transforms data.
	 * /!\ initData must be ordered by id_user (asc), id_labdoc (asc) and action_time (asc).
	 * @param {Array} initData (see above).
	 * @return {Array} data: array of dict with the following new keys: start_day_slot, production, x_start, x_end
	 */
	function transformData(initData) {
		data = initData
			// adds contextual information: start_day_slot and production time
			.map(function(row){
				row.start_day_slot = tracesHeatmap.getSlotStart(row.action_time, params.slotsEdges);
				row.production = parseFloat(row.production);
				return row;
			})
			// aggregates production time if a users modifies the same labdoc within maxMergingSeconds time
			.reduce(function(acc, cur){
				if (
					acc.length > 0
					&& acc[acc.length - 1].id_user == cur.id_user
					&& acc[acc.length - 1].id_labdoc == cur.id_labdoc
					&& acc[acc.length - 1].start_day_slot == cur.start_day_slot
					&& elapsedTimeIsShort(acc[acc.length - 1], cur)
				){
					acc[acc.length - 1].production += cur.production;
				} else {
					acc.push({...cur});
				}
				return acc;
			}, [])
			// sorts by action_time (starting time)
			.sort((a, b) => new Date(a.action_time) - new Date(b.action_time))
			// adds x_start and x_end which will be used to plot the graph
			.reduce(function(acc, cur){
				let x_start = 0;				
				if (acc.length != 0){
					// finds the trace that lasts the longest
					let longest_trace = getLongestTrace(acc);
					x_start = longest_trace.x_end - getOverlap(longest_trace, cur);
				}
				let x_end = x_start + cur.production;
				acc.push(Object.assign({}, cur, {x_start: x_start, x_end: x_end}));
				return acc;
			}, []);
	}


	/**
	 * States whether elapsed time between two traces is below maxMergingSeconds parameter
	 * @param {object} trace1 first trace which must expose action_time attribute
	 * @param {object} trace2 second trace with same constraint
	 * @return {boolean}
	 */
	function elapsedTimeIsShort(trace1, trace2){
		return new Date(Date.parse(trace1.action_time)).getTime()
			+ (60 * trace1.production + params.maxMergingSeconds) * 1000
			> new Date(Date.parse(trace2.action_time)).getTime();
	}


	/**
	 * Returns the trace which lasts the longest among a list of traces
	 * @param {Array} tracesList of traces exposing action_time and production
	 * @return {obj} trace
	 */
	function getLongestTrace(tracesList){
		return tracesList.reduce(function(acc, cur){
			let acc_end_time = new Date(acc.action_time) / (60 * 1000) + acc.production;
			let cur_end_time = new Date(cur.action_time) / (60 * 1000) + cur.production;
			return acc_end_time > cur_end_time ? acc : cur;
		});
	}


	/**
	 * Computes the overlap time between two traces (or 0 if they don't overlap)
	 * @param {object} trace1 first trace which must expose action_time and production attributes
	 * @param {object} trace2 second trace with same constraint
	 * @return {number} the overlap time
	 */
	function getOverlap(trace1, trace2){
		return Math.round(
			10 * Math.abs(
				Math.max(0, ((new Date(trace1.action_time) - new Date(trace2.action_time)) / (60 * 1000) + trace1.production)))
			) / 10;
	}


	/**
	 * Builds structure (y-axis), adding report parts
	 * /!\ some labdocs have the same report_part_position and position (id_report=34820 for example)
	 * Thus, an extra sorting criterion must be applied: the labdoc_id
	 * @param {object} initStructure list of report's labdocs
	 */
	function formatStructure(initStructure){
		structure = Object.values(initStructure)
			// sorts by report_part_position asc, labdoc_position asc and id_labdoc asc
			.sort(function(a, b){
				if (parseInt(a.report_part_position) != parseInt(b.report_part_position)){
					return parseInt(a.report_part_position) - parseInt(b.report_part_position);
				} else if (parseInt(a.labdoc_position )!= parseInt(b.labdoc_position)){
					return parseInt(a.labdoc_position) - parseInt(b.labdoc_position);
				} else {
					return parseInt(a.id_labdoc) - parseInt(b.id_labdoc);
				}
			})
			// adds report_parts to the structure
			.reduce(function(acc, cur){
				if (
					(acc.length == 0) 
					|| (acc.length > 0 && acc[acc.length - 1].report_part_position != cur.report_part_position)
				){
					let reportPart = 						{
						report_part_position: cur.report_part_position,
						labdoc_position: "0",
						report_part_name: cur.report_part_name,
					};
					acc.push({...reportPart, position_id: positionId(reportPart)});
				}
				acc.push({...cur, position_id: positionId(cur)});
				return acc;
			}, []);
	}

	
	/**
	 * Builds an array of data per user
	 */
	function splitDataByUser(){
		usersData = data
			.reduce(function(acc, cur) {
				if (!acc[cur.id_user]) {
					acc[cur.id_user] = []; 
				}
				acc[cur.id_user].push(cur);
				return acc;
			}, {});
	}


	/**
	 * Computes indicators to be displayed on bar hover
	 * Mutates usersData
	 */
	function computeIndicators(){
		data
			.sort((a, b) => new Date(a.action_time) - new Date(b.action_time))
			.map(function(elt, idx,arr){
				let last_element = arr.slice(0, idx).reverse().find(e => e.id_labdoc == elt.id_labdoc);
				if (last_element === undefined){
					elt.allPreviousUsers = [elt.id_user];
					elt.nUsers = 1;
					elt.nChanges = 0;
					elt.cumulatedTime = elt.x_end - elt.x_start;
				} else {
					if (last_element.allPreviousUsers.indexOf(elt.id_user) == -1){
						elt.allPreviousUsers = last_element.allPreviousUsers.concat(elt.id_user);
					} else {
						elt.allPreviousUsers = last_element.allPreviousUsers;
					}
					elt.nUsers = elt.allPreviousUsers.length;
					elt.nChanges = last_element.nChanges + (last_element.id_user != elt.id_user);
					elt.cumulatedTime = last_element.cumulatedTime + elt.x_end - elt.x_start
				}
			});
	}
	
	
	/**
	 * Builds an array of object with every new slot (in order to plot annotations)
	 * 
	 */
	function  computeSlotSample(){
		slotsSample = [...new Map(
				// slice is used to copy data (because reverse mutates)
				data.slice()
					.reverse()
					.map(e => [e.start_day_slot, e])
			).values()]
			.sort((a, b) => new Date(a.action_time) - new Date(b.action_time))
			.map(function(e, idx, arr){
				e.x_slot_start = parseFloat(e.x_start);
				if (idx == arr.length - 1){
					e.x_slot_end = data.reduce((acc, cur) => acc >= cur.x_end ? acc : cur.x_end, 0);
				} else {
					e.x_slot_end = parseFloat(arr[idx + 1].x_start);
				};
				e.x_slot_center = (e.x_slot_start + e.x_slot_end) / 2;
				e.x_slot_len = e.x_slot_end - e.x_slot_start;
				return e;
			});
	}
	
	
	/**
	 * Computes new parameters (mainly to adjust widths and heights)
	 */
	function updateParams(){
		params.chartHeight = Object.keys(structure).length * params.rowHeightY;
		params.nUsers = Object.keys(usersData).length;
		params.xRange = [0, data.reduce((acc, cur) => acc >= cur.x_end ? acc : cur.x_end, 0)];
		params.yRange = [-0.5, Object.keys(structure).length];
		params.onePxInUnit = 1 / (params.plotAreaWidth - params.lmLayout - params.rmLayout);
		params.yTopSpace = Math.min(...slotsSample.map(e => getPxEquiv(e.x_slot_len))) < 140 ? 200 : 70;
			/*Math.max(150, 19 * params.nUsers + 31)
			: Math.max(20, 19 * params.nUsers + 31);*/
		params.tmLayout = Math.max(params.tmLayout, params.yTopSpace);
		params.chartTotalHeight = params.bmLayout + params.chartHeight + params.tmLayout;
		params.paperCoordLeft = -1 * params.lmLayout * params.onePxInUnit;
	}
	

	/**
	 * Computes and returns an array of bar chart objects
	 * @return {array}
	 */
	function buildTraces(){
		return Object.keys(usersData).map(function(id_user, idx){
			// using anonymous function as constructor allows to access object properties inside itself
			return  new function() {
				this.type = 'bar',
				this.y = usersData[id_user].map(e => positionId(e)),
				this.base = usersData[id_user].map(e => e.x_start),
				this.x = usersData[id_user].map(e => e.x_end - e.x_start),
				this.width = 0.9,
				this.marker = {
					color: params.colorPalette[(parseInt(id_user) - 1) % params.colorPalette.length],
					line: {
						color: 'rgb(255, 255, 255)',
						width: 1
					}
				},
				this.name = id_user,
				this.orientation = 'h',
				this.hovertemplate =
					' <br>' 
					+ '<b>' + __('Titre du labdoc :') + '</b>' 
					+ ' %{customdata[0]}' + '<br>'
					+ '<b>' + __('Date :') + '</b>'
					+ ' %{customdata[1]}' + '<br>'
					+ '<b>' + __('Temps d\'écriture cumulé :') + '</b>'
					+ ' %{customdata[2]}' + '<br>'
					+ '<b>' + __('Nombre de rédacteurs différents :') + '</b>'
					+ ' %{customdata[3]}' + '<br>'
					+ '<b>' + __('Changements de rédacteur :') + '</b>'
					+ ' %{customdata[4]}' + '<br>'
					+ '<extra></extra>',
				this.customdata = getHover(
					this.y, 
					usersData[id_user].map(e => e.start_day_slot),
					usersData[id_user].map(e => e.cumulatedTime),
					usersData[id_user].map(e => e.nUsers),
					usersData[id_user].map(e => e.nChanges),
				),
				this.textposition = 'none'
			};
		});
	}

	
	/**
	 * Returns an id that identifies a labdoc position in the structure
	 * @param {object} element: the element source
	 * @return {array} list of string ids
	 */
	function positionId(element){
		return element.report_part_position.toString() 
			+ '.' + element.labdoc_position.toString()
			+ (element.id_labdoc ? '.' + element.id_labdoc.toString() : '')
	}


	/**
	 * Computes and returns the graph hover data
	 * @param {array} arrayOfPositionId
	 * @param {array} arrayOfSlots
	 * @param {array} arrayOfDurations
	 * @param {array} arrayOfUsers
	 * @param {array} arrayOfChanges
	 * @return {array} the formatted array to be used in traces
	 */
	function getHover(arrayOfPositionId, arrayOfSlots, arrayOfDurations,arrayOfUsers, arrayOfChanges) {
		ensureAllPositionsExist(arrayOfPositionId, structure);
		let hoverData = [
			arrayOfPositionId.map(e =>
				Object.values(structure).find(elt => elt.position_id == e)?.labdoc_name
			),
			arrayOfSlots.map(e => 
				tracesHeatmap.formatDateString(e, params.slotsEdges, params.slotsLabel)
			),
			arrayOfDurations.map(e =>
				parseFloat(e) > 1 ?
					(Math.round(10 * e) / 10).toString() + ' ' + __('minutes') 
					: (Math.round(10 * e) / 10).toString() + ' ' + __('minute')
			),
			arrayOfUsers,
			arrayOfChanges
		];
		// transposition
		return hoverData[0].map((_, colIndex) => hoverData.map(row => row[colIndex]));
	}

	/**
	 * Ensures that all position IDs from the given array exist in the provided structure.
	 * If a position ID is missing, it adds a new entry with a default `labdoc_name`.
	 * Goal is to avoid issue with missing position_id while trying to retrieve later for draw method
	 * @param arrayOfPositionId
	 * @param structure
	 * @returns {*}
	 */
	function ensureAllPositionsExist(arrayOfPositionId, structure) {
		const uniquePositionIds = [...new Set(arrayOfPositionId)];
		const existingPositionIds = new Set(Object.values(structure).map(item => item.position_id));
		const missingIds = uniquePositionIds.filter(id => !existingPositionIds.has(id));
		
		missingIds.map(id => {
			structure.push({
				position_id: id,
				labdoc_name: '',
			});
		});

		return structure;
	}

	/**
	 * Builds grey rectangles to identify a report part
	 * @return {array} array of shapes
	 */
	function buildHorizontalShapes() {
		return structure
			.filter(e => e.labdoc_position == "0")
			.map(e => ({
					type: 'rect',
					xref: 'paper',
					yref: 'y',
					x0: -1,
					y0: getPositionInStructure(e),
					x1: 1,
					y1: getPositionInStructure(e),
					line: {
						width: params.rowHeightY,
						color: 'rgb(225, 225, 225)'
					},
					layer:'below',
				})
			);
	}


	/**
	 * Returns the position of element e in the structure
	 * @param {object} element: the element source
	 * @return {number} position in structure
	 */
	function getPositionInStructure(element){
		return structure.length - 1 - structure.indexOf(
				structure.find(obj => obj.position_id == element.position_id)
			);
	}


	/**
	 * Builds vertical lines to identify changes in day slots
	 * @return {array} array of shapes
	 */
	function buildVerticalShapes() {
		// data is sorted by date
		return slotsSample
			.map(e => ({
					type: 'line',
					xref: 'x',
					yref: 'paper',
					x0: e.x_start,
					y0: 0,
					x1: e.x_start,
					y1: 1 + params.tmLayout / params.chartHeight,
					line: {
						color: 'rgb(192, 192, 192)',
						dash: 'dot',
						width: 1
					},
					layer:'above',
				})
			);
	}


	/**
	 * Builds images to identify a labdoc type
	 * @return {array} array of plotly images objects
	 */
	function buildImages() {
		return structure
			.filter(e => e.labdoc_position != "0")
			.map(e => ({
					layer: 'above',
					sizex: 0.1,
					sizey: 0.85,
					source: '/images/report/ld_type_' + e.labdoc_type + '.svg',
					xref: 'paper',
					yref: 'y',
					x: 0.95 * params.paperCoordLeft,
					y: getPositionInStructure(e) - 0.425,
					xanchor: 'left',
					yanchor: 'bottom',
				})
			);
	}


	/**
	 * Builds annotations to identify the day slot
	 * @return {array} array of plotly annotations objects
	 */
	function buildAnnotations() {
		return slotsSample
			.map(e => ({
				xref: 'x',
				yref: 'paper',
				xanchor: 'middle',
				yanchor: 'bottom',
				x: e.x_slot_center,
				y: 1,
				text: getPxEquiv(e.x_slot_len) < 12 ? '' : tracesHeatmap.formatDateString(e.start_day_slot, params.slotsEdges, params.slotsLabel), 
				textangle: getPxEquiv(e.x_slot_len) < 140 ? -90 : 0,
				layer:'above',
				showarrow: false,
			})
		);
	}


	/**
	 * Converts a number in x-axis unit to a number in px units
	 * @param {number} numberInXUnit
	 * @return {number} in px
	 */	
	function getPxEquiv(numberInXUnit){
		return numberInXUnit / params.xRange[1] / params.onePxInUnit;
	}

	
	/**
	 * Builds layout
	 * @param {array} shapes
	 * @param {array} images
	 * @param {array} annotations
	 * @return {object} plotly layout object
	 */
	function buildLayout(shapes, images, annotations){
		return ({
			barmode: 'stack',
			hovermode: 'closest',
			xaxis: {
				domain: [0, 1],
				range: params.xRange,
				automargin: false,
				titlefont: {
					size: 16
				},
				title: {
					text: __('Temps d\'écriture du rapport'),
				},
				ticks: 'outside',
				showline: true,
				linecolor: 'rgb(186, 186, 186)',
				rangeselector: true,
				showspikes: true,
				spikemode: 'toaxis',
				spikesnap: 'cursor',
				spikethickness: 1,
				showticksuffix: 'all',
				ticksuffix: __(' min'),
			},
			yaxis: {
				domain: [0, 1],
				autorange: false,
				range: params.yRange,
				type: 'category',
				categoryorder: 'array',
				categoryarray: Object.keys(structure).map(e => structure[e].position_id).reverse(),
				automargin: false,
				//mirror: 'all',
				ticks: '',
				//showgrid: true,
				tickson: 'labels',
				tickmode: 'array',
				tickvals: Object.keys(structure).map(e => structure[e].position_id),
				ticktext: Object.keys(structure).map(e => getLabel(structure[e])),
				showspikes: true,
				spikemode: 'toaxis',
				spikesnap: 'cursor',
				spikethickness: 1,
				fixedrange: true
			},
			autosize: false,
			paper_bgcolor: 'white',
			plot_bgcolor: 'white',
			margin: {
				l: params.lmLayout,
				r: params.rmLayout,
				b: params.bmLayout,
				t: params.tmLayout,
				pad: 0
			},
			shapes: shapes,
			images: images,
			annotations: annotations,
			modebardisplay: false,
			width: params.plotAreaWidth,
			height: params.chartTotalHeight,
			showlegend: true,
			legend: {
				x: 1,
				xanchor: 'right',
				// Max value is 3 (https://plotly.com/javascript/reference/layout/#layout-legend-y)
				y: Math.min(1 + params.tmLayout / params.chartHeight, 3),
				yanchor: 'top',
				bgcolor: 'rgb(255, 255, 255)',
				bordercolor: 'rgb(225, 225, 225)',
				borderwidth: 1,
				traceorder: 'normal',
				orientation: 'h',
				title: {
					text: __(' Comptes Etudiant :')
				}
			},
		});
	}


	/**
	 * Computes label from labdocs names
	 * @return {array} ymin and ymax
	 */
	function getLabel(arr){
		if (arr.labdoc_position != "0"){
			return truncate(arr.labdoc_name, params.truncateLabels, true);
		} else {
			return truncate(arr.report_part_name, params.truncateLabels, true);
		};
	}


	/**
	 * Returns a truncated string of a string 
	 * (from https://stackoverflow.com/questions/1199352/smart-way-to-truncate-long-strings)
	 * @param {string} str: the string to truncate
	 * @param {integer} n: number or chars
	 * @param {boolean} useWordBoundary: whether to truncate inside a word or not
	 * @return {string} the truncated string
	 */
	function truncate(str, n, useWordBoundary){
		if (str.length <= n) { 
			return str + " "; 
		}
		const subString = str.slice(0, n-1);
		return (useWordBoundary
			? subString.slice(0, subString.lastIndexOf(" "))
			: subString) + " ... ";
	}


	/**
	 * Define graph DOM element.
	 */
	function setGraphDiv(){
		return document.getElementById($(params.graphDiv.selector).attr('id'));
	}


	/**
	 * Build the icon menu.
	 * Reference: https://plotly.com/javascript/configuration-options/
	 */
	function buildConfig() {
		config = {
			scrollZoom: false,
			showLink: false,
			displaylogo: false,
			displayModeBar: true,
			locale: 'fr',
			modeBarButtons: [['toImage']], 
		};
	}


	/**
	 * Set the title of the graph.
	 */
	function setTitle(){
		let helpModalId = 'tooltip-' + $(params.graphDiv.selector).attr('id');
		($(params.graphDiv.selector).html() !== "") 
		&& $(params.graphDiv.selector).parent().children(":first").html(
			__("Chronologie de rédaction du rapport") 
			+ `<i 
				id='${helpModalId}'
				class='fa fa-info-circle lba_icon_default collaborative-dashboard-tooltip' 
				onclick='collabDashboard.tooltipCollabDashboardInfo()'
				onmouseover='collabDashboard.tooltipCollabDashboardInfo()'>
				</i>`
		);
		
	}

	/**
	 * Display tooltip information hovering an icon next to the title
	 */
	function tooltipCollabDashboardInfo(){
		let msg_title, msg_content;
		msg_title    =  __("Information");
		msg_content  = `
			<p>${__("Ce graphique est pensé comme un outil d’accompagnement (et non pas d’évaluation) des étudiants. Il illustre l’enchaînement des interventions des étudiants pour chaque labdoc du rapport. L'axe horizontal correspond au temps qu'afficherait un chronomètre qui se mettrait en route dès qu'un étudiant (au moins) écrit dans un labdoc du rapport.")}</p>
			<p>${__("Des différences peuvent exister avec les indicateurs présentés dans la fenêtre ci-dessus. Elles proviennent du fait que cette visualisation ignore le travail réalisé sur les labdocs effacés.")}</p>
			<br /><b>${__("Traces étudiantes")}</b>
			<ul>
				<li>${__("Les comptes étudiants sont volontairement anonymisés.")}</li>
				<li>${__("Un <b>double clic</b> sur un compte étudiant dans la légende permet de visualiser exclusivement les traces d'activité de cet étudiant. Un nouveau <b>double clic</b> permet de revenir à l'état antérieur.")}</li>
				<li>${__("Un <b>simple clic sur</b> le compte d'un étudiant cache les traces d'activité de cet étudiant. Un nouveau <b>simple clic</b> permet de revenir à l'état antérieur.")}</li>
			</ul>
			<br /><b>${__("Zoom")}</b>
			<ul>
				<li>${__("Il est possible de zoomer sur l'axe des temps en réalisant une sélection à l'intérieur du graphe. Un <b>double clic</b> permet de revenir à l'état initial. Il n'est pas possible de zoomer sur l'axe vertical.")}</li>
			</ul>
			<br /><b>${__("Sauvegarde au format image")}</b>
			<ul>
				<li>${__("Vous pouvez sauvegarder ce graphique au format png en <b>cliquant</b> sur le petit appareil photo situé en haut à gauche.")}</li>
			</ul>
		`;
		let helpModalId = 'tooltip-' + $(params.graphDiv.selector).attr('id');
		global_tooltip_popup.openTooltipPopup(msg_title, msg_content, '#' + helpModalId);
	}
	
	
	// Main return
	return {
		draw: (initStructure, initData, initParams) => drawCollabDashboard(initStructure, initData, initParams),
		tooltipCollabDashboardInfo: tooltipCollabDashboardInfo
	}
})();


