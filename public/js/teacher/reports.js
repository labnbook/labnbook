/**
 * @type {int} highlighted id_report
 */

var reportsTable = (function() {
	"use strict";

	var info_title = [];
	var info_colors;

	/**
	 * Dashboard config
	 */
	var lowColor = "#ff5c5c";
	var mediumColor = "#e2e2e2";
	var highColor = "#60b4ff";
	var colorLabels = [__("rouge"), __("gris"), __("bleu")];
	var annotationDomainTreshold = [0, 50, 100]; //in percent
	var indicatorDomainTreshold = [0, 100, 200]; //in percent of class median
	// Colorblind friendly palette (http://www.cookbook-r.com/Graphs/Colors_%28ggplot2%29/#a-colorblind-friendly-palette)
	// Nature publication about this palette : https://www.nature.com/articles/nmeth.1618
	var colorUsers = ["#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7", "#000000"];
	//var colorUsers 	= ["#e5d600", "#70bc32", "#fb57db", "#124910", "#bbb287", "#7b11b8"];
	
	/**
	 * @type {DataTable}
	 */
	var dataTable;
	
	/**
	 * @type {Set} The set of the integer IDs of selected reports.
	 */
	var selectedRows = new Set();

	/**
	 *  @type {Array<String>} Contains key for average
	 */

	let keyAverage = [
		'Info',
		'Cnx_time',
		'Active_time',
		'Com',
		'Work_distr'
	]

	let selectedMissions = null;
	
	/**
	 * @type {object} Contains the function that render a cell from a row of report data.
	 */
	const renderers = {
		activity: function(value, type) {
			if (type !== 'display') {
				return value;
			}
			if (!value) {
				return "";
			}
			var timeFormat = {year: "2-digit", month: "numeric", day: "numeric"};
			return (new Date(value * 1000)).toLocaleDateString("fr-FR", timeFormat); // ms in JS, s in PHP & MySQL
		},
		evaluations: function(value, type, row) {
			if(row.simulation_data && row.simulation_data.progression) {
				return `<span>${row.simulation_data.progression}%</span>`;
			}
			if (type == 'sort') {
				let outval = value === null ? -1 : (value === 'TODO' ? 0 :(value === 'DOING' ? 1 : 2));
				return outval;
			}
			if (value === null) {
				return "-";
			}
			const circle_class = value === 'TODO' ? 'stop red' : (value === 'DOING' ? 'play orange': 'check green');
			const title = value === 'TODO' ? __('Non débutée') : (value === 'DOING' ? __('En cours') : __('Rendue'));
			return '<i class="fa fa-circle-' + circle_class + '" title="' + title + '"></i>';
		},
		checkbox: function(id, type, row) {
			return '<div class="report-checkbox-div">' +
				'<input type="checkbox" class="report-checkbox" value="' + id + '" ' +
				(selectedRows.has(id) ? 'checked' : '') + ' /></div>';
		},
		date: function(d) { // ISO date to fr
			if (!d) {
				return "";
			}
			var m = d.match(/^(\d{4})-(\d\d)-(\d\d) ?(\d\d:\d\d)?/);
			if (!m || m[1] === '0000') {
				return "";
			}
			return m[3] + "/" + m[2] + "/" + m[1].substring(2) + " - " + m[4];
		},
		labdocs: function(value, type, row) {
			if (!value || !value.modified_ld) {
				return '';
			}
			if (type == 'sort'){
				return value.modified_ld;
			}
			let modified_ld = '';
			let modified_ld_title = '';
			if (value.new_mod_ld) {
				modified_ld = `<span class="new-modified-ld" style="padding: 0 4px; margin-left:5px; border-radius: 50%; background-color:#db601f; color:white;">${value.new_mod_ld}</span>`;
				modified_ld_title = __("{{nb_modified_ld}} labdocs ont été modifiés depuis votre dernière consultation.", {nb_modified_ld : value.new_mod_ld}) + '\n';
			}
			let title = __('{{nb_labdoc}} labdocs ont été travaillés par les étudiants.', {nb_labdoc : value.modified_ld}) + '\n' + modified_ld_title + __('Cliquez pour consulter le rapport');
			return `<a class="labdoc-follow" href="/report/${row.id_report}?sc=follow" target="_blank" onclick="event.stopPropagation(); $(this).find('i').remove();">
				<div class="labdoc-follow-div" title="${title}">
				${value.modified_ld + modified_ld}
				</div></a>`;
		},
		messages: function(value, type, row) {
			if (value > 0) {
				return '<i class="fa fa-star" style="color:#ffbd00;" title="' + __('Cette équipe a envoyé des messages que vous n\'avez pas encore lus.') + '" onclick="return displayReportMessages()"></i>';
			} else {
				return '';
			}
		},
		mission: function(value, type, row) {
			var classAttr = (row.status === 'arc' ? ' class="status-archive"' : '');
			return '<span' + classAttr + ' title="' + escapeHTML(row.mission_name) + '">' + escapeHTML(value) + '</span>';
		},
		status: function(value, type, row) {
			var classAttr = (row.status === 'arc' ? ' class="status-archive"' : '');
			const statuses = {
				"new": __("non-débuté"),
				"on": __("en cours"),
				"wait": __("rendu"),
				"arc": __("archivé")
			};
			var st = statuses[value];
			if (row.delete_time) {
				st += " - " +__("Supprimé le {{date}}", {date: escapeHTML(row.delete_time)});
			}
			return '<span' + classAttr + '>' + st + '</span>';
		},
		team: function(row) {
			var membersHtml = row.members.map(function(x){
				var name = document.createElement('span');
				if (x.seconds_since_synchro!==null && x.seconds_since_synchro < 2 * $("#refresh_period").val()) {
					name.classList.add("connected_user");
				}
				name.innerText = x.first_name + " " + x.user_name;
				return name.outerHTML;
			});
			var teamBlock, teamHtml;
			if (row.team_name) {
				teamBlock = document.createElement('b');
				teamBlock.innerText = row.team_name;
				teamHtml = teamBlock.outerHTML + " : ";
			} else {
				teamHtml = "";
			}
			return teamHtml + membersHtml.join(" - ");
		}
	};

	function initDataTables() {
		$.extend(true, $.fn.dataTable.defaults, {
			dom: "rtp", // https://datatables.net/reference/option/dom
			autoWidth: true,
			paging: true,
			pageLength: parseInt($("#reports .pagination-size").val()) || 20,
			lengthMenu: [20, 50, -1],
			conditionalPaging: true,
			processing: true,
			language: { 
				url: "/libraries/datatables/locale/" + window.labnbook.lang + ".json",
			}
		});

		const reportsConfig = {
			ajax: {
				url: "/teacher/reports/getReports",
				type: 'GET',
				data: function (d){
					d.filters=Alpine.store('reportFilters').getFilters();
				},
				cache: false,
			},
			scrollCollapse: true,
			scrollY: 'calc(100vh - 280px)',
			autoWidth: false,
			serverSide: true,
			searchDelay: 350,
			deferRender: true,
			orderCellsTop: true, // the first row of the header is used for sorting
			columns: [
				{
					name: "mission_code",
					data: "mission_code",
					render: {
						_: renderers.mission // the function that will render these cells
					},
					type: 'html'
				},
				{
					name: "team",
					data: renderers.team, // the function that will render these cells
					type: "html"
				},
				{
					name: "status",
					data: 'status',
					render: renderers.status,
					type: "html"
				},
				{
					name: "activity",
					data: 'labdocs.last_edition',
					render: {
						_: renderers.activity
					},
					type: "num"
				},
				{
					name: "modified_ld",
					data: "labdocs",
					render: {
						_: renderers.labdocs,
					},
					type: 'html-num'
				},
				{
					name: 'evaluations',
					data: "evaluations",
					render: renderers.evaluations,
				},
				{
					name: 'id_report',
					data: "id_report",
					orderable: false,
					render: renderers.checkbox
				}
			],
			rowId: function(d){ return "report-" + d.id_report; },
			initComplete: applyFiltersOnInit
		};
		dataTable = $('#reports-table').DataTable(reportsConfig);
	}
	
	function applyFiltersOnInit(){
		dataTable.on( 'draw.dt', function (event) {
			$('#dashboard-container').remove();
			$('#num-visible-reports').text(dataTable.page.info().recordsDisplay);
			updateSelectAll();
		} );
		reportsFilter();
	}
	
	function refreshTable() {
		if(dataTable) {
			unselectRow(null);
			dataTable.ajax.reload(function() {
				displayClassDashboard();
			});
		}
	}

	function initEvents() {
		
		dataTable.on('processing.dt', function(e, settings, processing) {
			if (processing) {
				$('#reports-table').addClass('table-loading');
			} else {
				$('#reports-table').removeClass('table-loading');
				$('.dataTable').removeClass('table-loading');
				// filter selected row => we select only the visible ones
				const visibleIds = Array.from(dataTable.data().map((e)=>e.id_report));
				[... selectedRows].forEach(idSelected => {
					if(!visibleIds.includes(idSelected)) {
						selectedRows.delete(idSelected);
					}
				});
				updateActionButtons();
			}
		});

		// pagination
		$(".pagination-size").on('change', function(){
			dataTable.page.len($(this).val()).draw();
			updateSelectAll();
		});

		// filters on rows
		$("#display-all-classes").on('change', function() {
			refreshTable();
		});
		
		// Archives
		$("#display-archives").on('change', function() {
			refreshTable();
		});
		
		// Search
		$(".tb-search input.search-input").on('input', debounce(function() {
			$('.report-checkbox').prop( "checked", false );
			Alpine.store('reportFilters').pattern = unaccent($(this).val());
			dataTable.search(Alpine.store('reportFilters').pattern).draw();
			unselectRow(null);
		}, dataTable.context[0].searchDelay));


		// Click on a row
		$("#reports-table > tbody").on('click', 'tr[role=row]', function(event) {
			let tr = $(this);
			let data = $("#reports-table").DataTable().row(tr).data();
			let id_report = data.id_report;
			if (tr.hasClass('highlighted')) {
				unselectRow(id_report);
			} else {
				unselectRow(null);
				selectRow(id_report);
				openReportDetails(id_report);
			}
		});

		// Click on a checkbox
		$("#reports-table tbody").on("click", 'tr[role=row] .report-checkbox-div', function(event) {
			event.stopPropagation();
			let cb;
			if ($(event.target).hasClass("report-checkbox")) {
				cb = $(event.target);
			} else {
				cb = $(event.target).children(".report-checkbox");
				cb.prop("checked", !cb.prop("checked")); // toggle
			}
			let id_report = parseInt(cb.val());
			if (cb.prop('checked')) {
				selectRow(id_report);
				closeReportDetails();
			} else {
				unselectRow(id_report);
			}
		});

		$('#reports-table').on( 'page.dt', function () {
			$('#reports-checkbox-all').prop( "checked", false );
		});
		
		// select all rows
		$("#reports-checkbox-all").on("change", function() {
			closeReportDetails(null);
			let check_all = $(this).is(":checked");
			let reports_id = dataTable.rows({page: 'current'}).data().toArray().map(function(item) {
				return item["id_report"];
			});
			if (check_all) {
				$('#reports-checkbox-all').prop( "checked", true );
				$('.report-checkbox').prop( "checked", true );
				reports_id.forEach(item => selectedRows.add(item))
			} else {
				$('#reports-checkbox-all').prop( "checked", false );
				$('.report-checkbox').prop( "checked", false );
				reports_id.forEach(item => selectedRows.delete(item))
			}
			updateActionButtons();
		});
		
		
		// Table buttons
		// folow reports
		$("#ra-see").click('.actions-active a', function() {
			var selectedIds = getSelectedIds();
			if (!selectedIds || selectedIds.length < 1) {
				return;
			} else if (selectedIds.length == 1) {
				followReport(selectedIds[0]);
			} else {
				$("#open-multi-reports-popup").dialog("open");
			}
			
		});
		// E-mails
		$("#ra-email").click('.actions-active a', function() {
			var selectedIds = getSelectedIds();
			if (!selectedIds || selectedIds.length < 1) {
				return;
			} else {
				createEmail(selectedIds);
			}
		});
		// Messages
		$("#ra-new-conv").click('.actions-active a', function() {
			var selectedIds = getSelectedIds();
			if (!selectedIds || selectedIds.length < 1) {
				return;
			} else {
				createConversation(selectedIds);
			}
		});
		// Show report assessments
		$("#ra-assessment").click(function() {
			var selectedIds = getSelectedIds();
			if (!selectedIds || selectedIds.length < 1){
				return;
			}
			if ( selectedMissions.size > 1) {
				alertGently(__("Imposible d'afficher les évaluations de rapports provenant de missions différentes"), "danger");
				return;
			}
			let reports_with_eval = 0;
			selectedRows.forEach(function (id_report) {
				let r = dataTable.row('#report-' + id_report).data()
				if (r.evaluations === "DONE" || r.evaluations === "DOING") {
					reports_with_eval++;
				}
			});
			if (reports_with_eval === 0) {
				alertGently(__("Pas d'évaluations pour les rapports selectionnés"));
				return;
			}
			window.location = '/scoring/assessments/getGrades?' + $.param({'productions': selectedIds});
		});
		// Update report
		$("#modify-selected-reports").on('click', function() {
			var selectedIds = getSelectedIds();
			if (!selectedIds || selectedIds.length < 1) {
				return;
			} else if (selectedIds.length == 1) {
				initEditTeamPopup();
			} else {
				$("#batch-update-popup .num-reports").text(selectedIds.length);
				fillCommonValues(selectedIds);
				$("#batch-update-popup .num-reports").text(selectedIds.length);
				$("#batch-update-popup").dialog('open');
			}
		});
		$("#batch-update-popup .popup-valid").on("click", batchUpdateReports);
		$(".popup .popup-cancel").click(function() {
			var popupElement = $(this).closest(".popup");
			popupElement.dialog("close");
		});
		// archive report
		$("#archive-report").click('.actions-active a', function() {
			const selectedIds = getSelectedIds();
			if (selectedIds && selectedIds.length > 0) {
				window.dispatchEvent(new CustomEvent('display-archive-popup'));
			}
		});

		// Restore report
		$("#ra-team-restore").click('.actions-active a', function() {
			var selectedIds = getSelectedIds();
			if (!selectedIds || selectedIds.length < 1) {
				return;
			} else {
				restoreTeams(selectedIds);
			}
		});

		/**
		 * Update the DOM of #batch-update-form to fill in fields that have the same value
		 * across all the selected rows.
		 *
		 * @param {Array} selectedIds
		 * @return {undefined}
		 */
		function fillCommonValues(selectedIds) {
			var reports = dataTable.rows().eq(0).data()
				.filter(function(row) {
					return selectedIds.includes(parseInt(row.id_report));
				});
			var popup = $('#batch-update-popup');

			var uniqueStatus = reports.pluck('status').unique().toArray();
			if (uniqueStatus.length === 1) {
				$('#batch-status').val(uniqueStatus[0]);
				if (uniqueStatus[0] === 'new') {
					$('#batch-status option[value="on"]').attr('disabled','disabled');
					$('#batch-status option[value="wait"]').attr('disabled','disabled');
				} else {
					$('#batch-status option[value="on"]').removeAttr('disabled');
					$('#batch-status option[value="wait"]').removeAttr('disabled');
				}
			} else {
				$('#batch-update-popup [name="status"]').val("");
				$('#batch-status option[value="on"]').removeAttr('disabled');
				$('#batch-status option[value="wait"]').removeAttr('disabled');
			}

			for (var fieldName of ['start', 'end']) {
				var values = reports.pluck(fieldName + '_datetime').unique().toArray();
				if (values.length === 1) {
					let date_value = "";
					let time_value = fieldName === 'start' ? '00:00' : '23:59';
					if (values[0]) {
						date_value = values[0].substring(0, 10);
						time_value  = values[0].substring(11, 16);
					}
					// if the same date times are found, display them and declare them as unified
					// (even if the dates are not specified)
					popup.find('[name="' + fieldName + '_date"]').val(date_value).show();
					popup.find('[name="' + fieldName + '_time"]').val(time_value).show();
					popup.find('[name="' + fieldName + '_datetimes_are_unified"]').val("1");
					popup.find('#modify-' + fieldName + '-datetime').parent().hide();
					popup.find('#undo-modify-' + fieldName + '-datetime').hide();				
				} else {
					// if date times are different, hide fields
					popup.find('[name="' + fieldName + '_date"]').val("").hide();
					popup.find('[name="' + fieldName + '_time"]').val(fieldName === 'start' ? '00:00' : '23:59').hide();
					popup.find('[name="' + fieldName + '_datetimes_are_unified"]').val("0");
					popup.find('#modify-' + fieldName + '-datetime').parent().show();
					popup.find('#undo-modify-' + fieldName + '-datetime').hide();
					popup.find('#undo-modify-' + fieldName + '-datetime').closest('td').height('27px');
				}
			}

			popup.find('[name="end_date"]').on('blur', function(e) {
				const end_date = new Date($(e.target).val());
				const current_date = new Date();
				// if we change the end date of a waited report we change the report status to on
				if( $('#batch-update-popup [name="status"]').val() === 'wait' && end_date > current_date) {
					$('#batch-update-popup [name="status"]').val('on');
				}
			});

			for (var fieldName of ['allow_msg_teacher', 'allow_msg_team', 'allow_attach_ld', 'allow_save_ld']) {
				var values = reports.pluck(fieldName).unique().toArray();
				if (values.length === 1 && values[0] !== null) {
					popup.find('[name="' + fieldName + '"][value="' + values[0] + '"]').prop('checked', true);
				} else {
					popup.find('[name="' + fieldName + '"][value=""]').prop('checked', true);
				}
			}

			for (var fieldName of ['allow_msg_id_class', 'allow_msg_id_mission']) {
				var values = reports.pluck(fieldName).unique().toArray();
				if (values.length === 1 && values[0] !== '') {
					popup.find('[name="' + fieldName + '"]').val(values[0]);
				} else {
					popup.find('[name="' + fieldName + '"]').val('');
				}
			}
		}
	}

	/**
	 *  Add resume of class average when filter(s) (class, mission) are selected below principal tab
	 */
	function reportsFilter(){
		const configMissionFilterSelect = {
			url: '/teacher/reports/getMissionsForFilters',
			alpineStore: [
				{
					storeIdentifier: 'reportFilters',
					storeAttributeName: 'id_class'
				}
			],
			data: {},
			groups: [
				{
					text: __('Missions'),
					childrenLabel: 'missions',
					paginated: true
				},
			],
			placeholder: __('Missions'),
			clearOnSelect: false,
			width: 'auto',
		};
		window.dispatchEvent(new CustomEvent('lnb-select2-load', {
			detail:{
				selector: 'reports-filter-mission',
				configSelect2: configMissionFilterSelect
			}}));
		const configClassFilterSelect = {
			url: '/teacher/reports/getClassesForFilters',
			alpineStore: [
				{
					storeIdentifier: 'reportFilters',
					storeAttributeName: 'id_mission'
				}
			],
			data: {},
			groups: [
				{
					text: __('Classes'),
					childrenLabel: 'classes',
					paginated: true
				},
			],
			placeholder: __('Classes'),
			clearOnSelect: false,
			width: 'auto'
		};
		window.dispatchEvent(new CustomEvent('lnb-select2-load', {
			detail:{
				selector: 'reports-filter-class',
				configSelect2: configClassFilterSelect
			}}));
	}
	
	/**
	 * Get the data for a report, which was sent by PHP getReports() and saved in DataTables
	 *
	 * @param {number} id id_report
	 * @returns {object}
	 */
	function getReport(id) {
		dataTable.on( 'draw.dt', function (event) {
			$('#dashboard-container').remove();
			updateSelectAll();
		} );
		return dataTable.row('#report-' + id).data();
	}


	function computeAllowImport(updates) {
		var allow_import=0;
		// Get and parse allowimport as an int
		var allow_import_id_mission = parseInt(updates.filter(x => {return x['name'] == 'allow_import_id_mission'})[0]['value']);
		if (allow_import_id_mission === -1 || allow_import_id_mission > 0) {
			// Import is allowd from all mission are a specific mission, set allow_import
			allow_import=1
		} else if (isNaN(allow_import_id_mission)) {
			// No change on allow_import_id_mission, do not change allow_import
			allow_import = "";
		}
		updates.push({'name': 'allow_import', 'value': allow_import});
		return updates;
	}

	function batchUpdateReports() {
		var updates = $("#batch-update-popup form").serializeArray();
		// allow_import is not in the form, so we need to compute it
		updates = computeAllowImport(updates);
		var errors = validateReportAttributes(serializedToAssoc(updates));
		if (errors.length > 0) {
			alert(errors.join("\n"));
			return false;
		}
		$.ajax({
			type: 'POST',
			url: '/teacher/reports/batchUpdate',
			data: {
				reports: getSelectedIds(),
				updates: updates
			}
		}).done(function(response) {
			dataTable.ajax.reload(null, false);
			if (response.message) {
				alert(response.message);
			} else {
				var color;
				if (response.selected == response.updated || response.selected == response.statuses) {
					color = 'success';
				} else if (response.updated == 0 && response.statuses == 0){
					color = 'danger';
				}else {
					color = 'warning';
				}
				alertGently( __("{{nb_selected}} rapports sélectionnés : {{nb_updated}} rapport(s) modifié(s) au niveau des options et {{nb_status}} avancement(s) modifié(s).", {nb_selected : response.selected, nb_updated : response.updated, nb_status : response.statuses}), color, 10000);
			}
			$("#batch-update-popup").dialog('close');
		}).fail(function(response) {
			alert( __("Erreur lors du traitement par lot") );
		});

		/**
		* Convert a serialized form into an k/v object.
		*
		* @param {Array} ser
		* @return {Object}
		*/
	   function serializedToAssoc(ser) {
		   var assoc = {};
		   for (var a of ser) {
			   assoc[a.name] = a.value;
		   }
		   return assoc;
	   }
	}

	/**
	 * @param {boolean} whether to include not initialized reports (default = true)
	 * @return {Array}
	 */
	function getSelectedIds(includesNotStarted= true) {
		let selectedArray = Array.from(selectedRows.values());
		if (includesNotStarted) {
			return selectedArray;
		} else {
			return selectedArray.filter(id => (
				dataTable.row('#report-' + id).data().initialized != 0 || dataTable.row('#report-' + id).data().status != 'new'
				)
			);
		}
	}
	
	/**
	 * @returns {int}
	 */
	function getUniqueSelectedReportId(){
		if (selectedRows.size != 1) {
			return false;
		}
		var it = selectedRows.values();
		var first = it.next();
		return first.value;
	}

	/**
	 * Build the HTML from the data of a row, as sent by getReports().
	 *
	 * @param {object} rowData
	 * @return {String}
	 */
	function getReportDetails(rowData) {
		if (typeof rowData === 'undefined') {
			return "";
		}
		
		const start_date = renderers.date(rowData.start_datetime);
		const end_date = renderers.date(rowData.end_datetime);

		let recipients_allowed = [];
		if (rowData.allow_msg_teacher) {
			recipients_allowed.push( __("enseignants") );
		}
		if (rowData.allow_msg_team) {
			recipients_allowed.push( __("étudiants de l'équipe") );
		}
		if (rowData.allow_msg_id_class && rowData.allowed_class_name) {
			recipients_allowed.push( __("étudiants de la classe <em>{{class}}</em>", { class : escapeHTML(rowData.allowed_class_name) } ) );
		}
		if (rowData.allow_msg_id_mission) {
			recipients_allowed.push( __("étudiants de la mission <em>{{task}}</em>", { task : escapeHTML(rowData.allowed_mission_code) } ) );
		}
		
		let html = `
			<div class="report-details-wrapper">
				<div class="details">
					${rowData.tc_classname?
						`<p><b>${__("Classe") + ' : '}</b>${rowData.tc_classname}</p>`:''
					}
					<p><b>${(rowData.teachers.length > 1?__("Tuteurs"):__('Tuteur'))+ ' : '}</b>${rowData.teachers.join(', ')}</p>
					${start_date?
						`<p><b>${__("Début du travail")+ ' : '}</b>${start_date}</p>`:''
					}
					${end_date?
						`<p class="lba_end_date"><b>${__("Fin du travail")+ ' : '}</b>${end_date}</p>`:''
					}
					<p><b>${__('Import de labdocs')+ ' : '}</b>${rowData.import_status}</p>
					<p>
						<b>${__('Options des messages')}</b>
						${recipients_allowed.length === 0 ?
							(' : '+__('messagerie désactivée'))
							:`
							<ul>
								<li>${__('Destinataires possibles') + " : " + recipients_allowed.join(', ')}</li>
								${rowData.allow_attach_ld?`<li>${__('Envoi de labdocs attachés')}</li>`:''}
								${rowData.allow_save_ld?`<li>${__('Enregistrement de labdocs attachés')}</li>`:''}
							</ul>
							`
						}
					</p>
					${setCollabDashboardLink(rowData)}
				</div>
				${(rowData.status !== "new" && !rowData.delete_time)?
					`<div class="dashboard-container">
						<div class="dashboard dashboard-loading">${generateLoader()}</div>
					</div>`:''
				}
			</div>
			${(rowData.status !== "new" && !rowData.delete_time) ?
				`<div class="collaborative-dashboard-container" style="display: none">
					<h2 class="collaborative-dashboard-title"></h2>
					<div class="collaborative-dashboard" id="collaborative-dashboard-div_${rowData.id_report}"></div>
				</div>`:''
			}
		`;	
		if (rowData.status !== "new" && !rowData.delete_time) {
			displayDBForOneReport(rowData);
		}
		
		if (!rowData.delete_time) {
			// Deleted repports would trigger 403
			$.ajax({
					method: 'POST',
					url: "/teacher/report/"+rowData.id_report+"/traceSelectReport",
				});
		}
		return html;		
	}

	/**
	 * Aborts all indicators related ajax calls
	 */
	function abort_indicators_ajax(ajaxVar) {
		for (let name of ["ajaxGetIndicators", "ajaxGetInfoClass", "ajaxGetInfoMission"]) {
			let ajaxVar=window[name];
			if (typeof(ajaxVar) != 'undefined') {
				ajaxVar.abort();
			}
		}
	}

	$( window ).bind('beforeunload', function(){
		abort_indicators_ajax();
	});

	function generateLoader( mini = false ){
		let miniClass = "";
		if(mini){
			miniClass = "spinner-mini";
		}
		return '<div class="container-spinner"><div class="spinner '+miniClass+'"></div></div>';
	}

	function displayDBForOneReport(rowData) {

		// On aborte les requêtes ajax afin de d'arrêter celles qui tournent avant d'en renvoyer
		abort_indicators_ajax();

		$("td.expanded .dashboard").remove();
		let colorRange = [lowColor, mediumColor, highColor, highColor];
		let indicators 		= {};

		window.ajaxGetIndicators = $.ajax({
			url: "/teacher/reports/getIndicators",
			data: {
				report: rowData.id_report
			},
			error : function (jqXhr, textStatus) {
				if (jqXhr.status === 404) {
					alertGently(__("Rapport introuvable, veuillez recharger la page avec F5"));
				}
			},
			cache: false
		}).done(function (data) {

			// Pour savoir si il y a qu'un seul étudiant, on regarde pour le premier indicateur si il y a plusieurs valeurs (correspondant au score de chaque étudiants de l'équipe)
			let singleStudentTeam = ( data.Active_time.length <= 1 );
			let $dashboard_div = $("td.expanded .dashboard");
			$dashboard_div.find(".container-spinner").remove();
			$dashboard_div.removeClass("dashboard-loading");
			$dashboard_div.append(
				`<div class="titre">
					<p></p>
					<p>${__("Score de l'équipe")}</p>
					${
						!singleStudentTeam?`
							<p title="${__("L'ordre de répartition ne correspond pas à l'ordre des noms des étudiants dans l'équipe")}" class="db-svg-bars">⚠ ${__("Répartition par compte étudiant")}</p>
						`:''
					}
					<p>${__("Médiane de classe")}</p>
					<p>${__("Médiane de mission")}</p>
				</div>`);

			for (const [indicator_name, indicator_data] of Object.entries(data)) {
				////////////////////////////////////////////
				// Construction of the indicators objects //
				////////////////////////////////////////////

				let indicator = {
					name: indicator_name, // Name of the indicator
					value: 0, // Total value
					vals: [], // Division between students
					title: "", // Name of the indicator
					display: "", // Value display
					description: "", // Descrition of the indicator display when mouse hover
					icon: "", // Add font awesome
					color: 'transparent'
				};
				
				if(!['Annotation', 'Work_distr'].includes(indicator_name)) {
					indicator.value = indicator_data.reduce((acc, curr) => parseFloat(acc) + curr, 0);
					indicator.vals = indicator_data;
				}

				// Displays for the total scores
				if (indicator_name === "Info") {
					indicator.title 	= __("Ressources & consignes");
					indicator.display 	= indicator.value + " " + __("vues");
					indicator.icon 		= '<i class="fa fa-folder-open dashboard-icon"></i>';
				} else if (indicator_name === "Com") {
					indicator.title 	= __("Messages & commentaires");
					indicator.display 	= indicator.value + " " + __("posts");
					indicator.icon 		= '<i class="fa fa-envelope dashboard-icon"></i>';
				} else if (indicator_name === "Cnx_time") {
					indicator.title 	= __("Temps de connexion");
					indicator.display 	= timeFormat(indicator.value);
					indicator.icon 		= '<i class="far fa-clock dashboard-icon"></i>';
				} else if (indicator_name === "Active_time") {
					indicator.title 	= __("Temps d'écriture");
					indicator.display 	= timeFormat(indicator.value);
					indicator.icon 		= '<i class="fas fa-pencil-alt dashboard-icon"></i>';
				} else if (indicator_name === "Annotation") {
					indicator.title 	= __("Annotations");
					indicator.display 	= indicator_data.total_annotation_read + ' ' + __('lues') + ' / ' + indicator_data.total_annotation;
					indicator.icon 		= '<i class="far fa-edit dashboard-icon"></i>';
					if(indicator_data.total_annotation > 0){
						let ratio = indicator_data.total_annotation_read / indicator_data.total_annotation;
						const colorScale = d3.scaleLinear()
							.domain([...annotationDomainTreshold, 200].map(x => x / 100))
							.range(colorRange);
						indicator.color = colorScale(ratio);
						info_title[indicator.name] += "\n" + colorLabels
							.map((x, i) => annotationDomainTreshold[i] + '% = ' + x)
							.reduce((carry, current) => carry === '' ? current : carry + " | " + current, '');
					}
				} else if (indicator_name === "Work_distr") {
					indicator.value = indicator_data;
					indicator.title = __("Changements de rédacteur");
					if(indicator.value !== "NA"){
						indicator.display = Number.parseFloat(indicator.value).toFixed(1);
					} else {
						indicator.display = "-";
					}
					indicator.icon = '<i class="fa fa-users dashboard-icon"></i>';
				}
				
				////////////////////////////
				// Display the indicators //
				////////////////////////////
				
				let row_indicator_html = `
					<div class="indicator" id="${indicator.name}">
						<div class="titre titre-left" id="${indicator.name}-title">
							<p title="${info_title[indicator.name]}">${indicator.icon + indicator.title}</p>
						</div>
						<div id="${indicator.name}-value">
							<p class="indicator-value" title="${info_title[indicator.name]}" style="background-color: ${indicator.color}">${indicator.display}</p>
						</div>
						${!singleStudentTeam?
							`<div id="${indicator.name}-bars" class="db-svg-bars"></div>`:''}
						<div class="cell cell-average-class">
							${generateLoader(true)}
							<p id="${indicator.name}-average-class" title="${info_title[indicator.name]}"></p>
						</div>
						<div class="cell cell-average-mission">
							${generateLoader(true)}
							<p id="${indicator.name}-average-mission" title="${info_title[indicator.name]}"></p>
						</div>
					</div>
				`;

				$dashboard_div.append(row_indicator_html);
				
				// Display the bars
				if(!singleStudentTeam && !['Annotation', 'Work_distr'].includes(indicator_name)) {
					let bars = d3.select('#' + indicator.name + '-bars').append("svg");
					let group = bars.append("g");
					let largeur = 0;
					let indicDivisor = Math.max.apply(null, indicator.vals);
					if (indicDivisor !== 0) {
						$(indicator.vals).each(function (index, val) {
							group.append("text")
								.attr("x", 0)
								.attr("y", 7 + 12 * index)
								.attr("font-size", "9px")
								.text(index + 1);
							group.append("rect")
								.attr("x", 10)
								.attr("y", 12 * index)
								.attr("width", (parseInt(val) * 100) / indicDivisor)
								.attr("height", 8)
								.style("stroke", "none").style("fill", colorUsers[index % (colorUsers.length)]);
							largeur += (parseInt(val) * 100) / indicDivisor;
						});
					}
					// Fix the dimensions of the svg element
					bars.attr('height', document.querySelector('#' + indicator.name + '-bars > svg').getBBox().height);
				}
				indicators[indicator.name] = indicator;
			}

			let home_time_html = `
				<div class="indicator hide-important" id="Home_time">
					<div class="titre" id="Home_time-title"><p title="Temps de travail hors présentiel">Temps de travail hors présentiel</p></div>
					<div id="Home_time-value">${generateLoader(true)}</div>
					<div id="Home_time-bars"></div>
					<div class="cell cell-average-class"> <p id="Home_time-average-class"></p></div>
					<div class="cell cell-average-mission"> <p id="Home_time-average-mission"></p></div>
				</div>
			`;
			$dashboard_div.append(home_time_html);
			
			if(rowData.simulation_code)
			{
				$.ajax({
					type: 'GET',
					url: '/simulation/getReportSimulationDashboard',
					datatype: "json",
					data: {
						idReport: rowData.id_report,
						simulationCode: rowData.simulation_code,
					},
					success: function (htmlSimulationDashboardView) {
						$dashboard_div.parent().append(htmlSimulationDashboardView);
					},
					error: function (err) {
						console.log(err);
					}
				});
			}

			//////////////////////
			// Class statistics //
			//////////////////////
			if( rowData.id_team_config !== 0 ){
				window.ajaxGetInfoClass = $.ajax({
					url: "/teacher/reports/getInfoClass",
					data: {
						id_team_config: rowData.id_team_config
					},
					cache: false
				}).done(function (data) {
					$('.cell-average-class .container-spinner').remove();
					$.each(data, function (key, val) {
						let display = "-";
						let teamScore = indicators[key].value;
						// Only use the ClassScore if at least 2 values are available for the class
						if (val["Count"] >=2 && teamScore != null && teamScore != "NA" && teamScore >= 0) {
							let classScore = val["Median"];
							let color = null; // color of the background for the value
							if (classScore != null && classScore != "NA" && classScore > 0) {
								var colorScale = d3.scaleLinear()
									.domain([...indicatorDomainTreshold, 300].map(x => x * classScore / 100))
									.range(colorRange);
								color = colorScale(teamScore);
							}
							if (color) {
								$('#' + key + '-value > p').css("background-color", color);
								let indicator_title = $('#' + key + '-value > p').prop('title');
								$('#' + key + '-value > p').prop('title', indicator_title + '\n' + info_colors);
							}
							// display the score of the class
							if (classScore != null && classScore != "NA" &&  classScore >= 0) {
								if (key == "Cnx_time" || key == "Active_time") {
									display = timeFormat(classScore);
								} else {
									display = classScore.toFixed(1);
								}
							}
						}
						$('#' + key + '-average-class').append(display);
					});
				}).fail(function(jqxhr, textStatus, errorThrown) {
					if (jqxhr.status === 403) {
						// 403 errors are normal here, ignore them silently
						$('.cell-average-class .container-spinner').remove();
						$('.cell-average-class').text('-');
					}
				});
			} else {
				$('.cell-average-class .container-spinner').remove();
				keyAverage.forEach((indicator_name) => {
					$('#' + indicator_name + '-average-class').append("-");
				});
			}

			////////////////////////
			// Mission Statistics //
			////////////////////////
			window.ajaxGetInfoMission = $.ajax({
				url: "/teacher/reports/getInfoMission",
				data: {
					mission: rowData.id_mission
				},
				cache: false,
			}).done(function (data) {
				$('.cell-average-mission .container-spinner').remove();
				$.each(data, function (key, val) {
					let display = "-";
					// Only use the missionScore if at least 2 values are available for the mission
					if (val["Count"] >=2) {
						let missionScore = val["Median"];
						// display the score of the mission
						if (missionScore != null && missionScore != "NA" && missionScore >= 0) {
							if (key == "Cnx_time" || key == "Active_time") {
								display = timeFormat(missionScore);
							} else {
								display = missionScore.toFixed(1);
							}
						}
					}
					$('#' + key + '-average-mission').append(display);
				});
			});
		});
	}

	/**
	 * @param int Timestamp;
	 * @return string Time in format 0h 0min
	 */
	function timeFormat(timestamp) {
		let hours = Math.floor(timestamp / 3600);
		let mins = Math.round((timestamp - 3600 * hours) / 60);
		return hours + 'h ' + mins + 'min';
	}


	/**
	 * @param {int} selected
	 * Show hides the assesments button depending on the parameters
	 */
	function toggleAssessments() {
		$('.lba-context-actions #ra-assessment').toggleClass('actions-inactive', selectedMissions.size!==1);
	}

	/**
	 * @param {int} deleted
	 * Show hides the restore button depending on the parameters
	 */
	function toggleRestoreButton(deleted) {
		if (deleted) {
			$('.lba-context-actions a').hide();
			$('.lba-context-actions #ra-team-restore').show();
		} else {
			$('.lba-context-actions a').show();
			$('.lba-context-actions #ra-team-restore').hide();
		}
	}

	/**
	 * Add a selected row
	 * @param {int} id_report
	 * @returns {undefined}
	 */
	function selectRow(id_report) {
		if (id_report) {
			id_report = Number(id_report);
			if ($("#report-"+id_report).length) { // do not select undisplayed reports (archives)
				let tr = $("#report-" + id_report);
				tr.find(".report-checkbox").prop("checked", true);
				if (!selectedRows.has(id_report)) {
					selectedRows.add(id_report);
				}
				updateActionButtons();
				updateSelectAll();
			}
		}
	}


	/**
	 * Remove a selected row
	 * @param {int} id_report
	 * @return {undefined}
	 */
	function unselectRow(id_report) {
		if (id_report === null) { // unselect all rows
			selectedRows = new Set();
			$('.report-checkbox').prop( "checked", false );
			closeReportDetails();
		} else {
			id_report = Number(id_report);
			selectedRows.delete(id_report);
			$("tr#report-" + id_report).find(".report-checkbox").prop("checked", false);
			closeReportDetails(id_report);
		}
		updateActionButtons();
		updateSelectAll();
	}


	/**
	 * Update the infos and the actions on the top right of the table
	 */
	function updateActionButtons() {
		$('.lba-context-actions').removeClass('actions-inactive');
		let nbSelectedReports = selectedRows.size;
		if (nbSelectedReports > 1) {
			$("#number-report-selected").text( __("{{number}} rapports selectionnés", { number : nbSelectedReports }));
		} else if (nbSelectedReports == 1){
			$("#number-report-selected").text( __("1 rapport selectionné"));
		} else {
			$("#number-report-selected").text("");
			$('.lba-context-actions').addClass('actions-inactive');
		}
		
		// Hide eye button if all selected rows correspond to not started reports
		var selectedStartedReportsIds = getSelectedIds(false);
		if (selectedStartedReportsIds.length === 0){
			$('.lba-context-actions a#ra-see').addClass('action-inactive');
		} else {
			$('.lba-context-actions a#ra-see').removeClass('action-inactive');
		}

		let deleted = 0;
		selectedMissions = new Set();
		selectedRows.forEach(function (id_report) {
			let r = dataTable.row('#report-' + id_report).data()
			if (r.delete_time) {
				deleted++;
			}
			selectedMissions.add(r.id_mission);
		});
		toggleRestoreButton (deleted);
		toggleAssessments();
	}

	function updateSelectAll(){
		if($('.report-checkbox').length > 0 && $('.report-checkbox').length==$('.report-checkbox:checked').length){
			$('#reports-checkbox-all').prop('checked',true)
		} else {
			$('#reports-checkbox-all').prop('checked',false)
		}
	}

	/**
	 * Show the row that contains the reports details
	 * @return {undefined}
	 */
	function openReportDetails(id_report){
		if (id_report) {
			let row = dataTable.row('#report-' + id_report);
			if (!row.data()) {
				console.log("Warning: select a report which has no data: ", id_report, row);
				return;
			}
			row.child(getReportDetails(row.data()), "expanded").show();
			$("#report-" + id_report).addClass('highlighted');
		}
	}
	
	/**
	 * Hide the row that contains the reports details
	 * @param id_report if null, close any open row
	 * @return {undefined}
	 */
	function closeReportDetails(id_report){
		let rows;
		if (id_report) {
			rows = $("#report-" + id_report);
		} else {
			rows = $("tr.highlighted");
		}
		rows.removeClass('highlighted');
		dataTable.row(rows).child(false);
	}
	
	/**
	 * Apply changes to the report in the table, then redraw.
	 *
	 * @param {integer} id_report
	 * @param {object} changes
	 * @return {undefined}
	 */
	function updateReportRow(id_report, changes) {
		var row = dataTable.row("#report-" + id_report);
		var rowData = row.data();
		for (var x in changes) {
			if (changes.hasOwnProperty(x)) {
				rowData[x] = changes[x];
			}
		}
		row.data(rowData);
		dataTable.draw(false);
		selectRow(id_report);
	}

	/**
	 * Update the status of a report, through an AJAX call.
	 *
	 * @param {object} report
	 */
	function updateReportStatus(report) {
		$.ajax({
			type: "POST",
			url: "/teacher/report/"+report.id_report+"/updateStatus",
			data: {
				status: report.status
			},
			success: function (content) {
				if (content !== "") {
					alertGently(content, "danger", 8000);
				} else {
					updateReportRow(report.id_report, {status: report.status});
					selectRow(report.id_report);
				}
			}
		});
	}
	/***
	 * Use this function to get new status of a report
	 * If archive, new status is arc
	 * If unarchive the status can be wait, new, on
	 * @param id_report
	 * @returns {string}
	 */
	function getNewReportStatusAfterToggleArchive ( id_report ){
		const report = getReport(id_report);
		const end_date = new Date(report.end_datetime);
		const now = new Date();
		if (report.status !== 'arc') {
			return 'arc';
		} else if (parseInt(report.initialized) === 0) {
			return 'new';
		} else if (report.end_datetime && end_date < now) {
			return 'wait';
		} else {
			return 'on';
		}
	}

	function showAssessment() {
		let id_reports = getSelectedIds();
		if (id_reports.length) {
			$.ajax({
				method: "POST",
				url: "/scoring/assessment/", // TODO
				data: {
					id_reports: selected_ids,
				}
			}).done(function (data) {
				alert("Vous allez voir les résultats de vos évaluations");
			});
		}
	}
	
	function updateReportsStatus(selected_reports) {
		let selected_ids = [];
		let new_reports_status = [];
		for (let report of selected_reports) {
			new_reports_status.push({id_report:report.id_report, new_status:report.status});
			selected_ids.push(report.id_report);
		}
		$.ajax({
			method: "POST",
			url: "/teacher/reports/batchArchive",
			data: {
				id_reports: selected_ids,
				new_status: new_reports_status
			}
		}).done(function (data) {
			dataTable.ajax.reload(null, false);
			unselectRow(null);
            alertGently(data.msg, "info");
		});
	}
	// Delete multiple reports
	function deleteReports(){
		const selectedIds = getSelectedIds();
		if(selectedIds && selectedIds.length>1) {
			$.ajax({
				type: "POST",
				url: "/teacher/reports/batchDelete",
				data: {
					id_reports: selectedIds
				}
			})
			.fail(function (xhr) {
				logError("Fail on delete multiple report", "error", { "id_reports": selectedIds });
			})
			.done(function (data) {
				alertGently(__("{{nb_report}} rapports supprimés", { nb_report : data.nb_reports_deleted} ), "info");
				dataTable.ajax.reload(null, false);
				unselectRow(null);
			});
		}
	}

	function deleteReport() {
		const id_report = getUniqueSelectedReportId();
		if (id_report) {
			$.ajax({
				type: "POST",
				url: "/teacher/report/"+id_report+"/delete"
			}).done(function () {
				dataTable.row('#report-' + id_report).remove().draw(false);
				unselectRow(null);
			});
		}
	}
	
	function restoreTeams(ids) {
		$.ajax({
			type: "POST",
			data: {
				reports: ids
			},
			url: "/teacher/reports/batchRestore",
			success: function (data) {
				if (data > 0 ) {
					alertGently(__("{{count}} rapports restaurés", {count: data}), "success");
				} else {
					alertGently(__("Aucun rapports restaurés"));
				}
				reportsTable.reload();
				toggleRestoreButton(0);
			}
		});
	}

   /**
	* permet de visualiser un rapport selectionné en scope "follow"
	*/
   function followReport(id_report, freezeLds=false, openScoring=false) {
	   $("#report-"+id_report).find('.labdoc-follow-div i').remove();
	   var url = '/report/' + id_report + '?sc=follow&freezeLds=' + freezeLds + '&openScoring='+ openScoring;
	   window.open(url, '_blank');
   }

	/**
	 * permet de visualiser plusieurs rapports selectionnés en scope "follow"
	 */
	function followReports() {
		return {
			selectedIds:[],
			freezeLds:false,
			openScoring:false,
			openReports() {
				this.selectedIds = getSelectedIds(false);
				for (let id_report of this.selectedIds) {
					followReport(id_report, this.freezeLds, this.openScoring);
				}
				$("#open-multi-reports-popup").dialog("close");
			}
		}
	}
	
	/**
	 * Get reports parameters for the Alpine component to manage the archive popup
	 * @return {object}
	 */
	function getParamsArchivePopup(){
		const selectedReports = getSelectedIds().map((id) => getReport(id));
		const deleteLabel = (selectedReports.length>1)?
			__('Supprimer {{quantity}} rapports', {quantity:selectedReports.length})
			:__('Supprimer {{quantity}} rapport', {quantity:selectedReports.length});
		const deleteDetailsLabel = (selectedReports.length>1)?
			__('ATTENTION : cette action est définitive et les étudiants ne pourront plus accéder à leurs rapports')
			:__('ATTENTION : cette action est définitive et les étudiants ne pourront plus accéder à leur rapport');
		let archiveLabel, archiveDetailsLabel = '';
		if( selectedReports.every((report) => report.status === 'arc') ) {
			archiveLabel = (selectedReports.length>1)?
				__('Désarchiver {{quantity}} rapports', {quantity:selectedReports.length})
				:__('Désarchiver {{quantity}} rapport', {quantity:selectedReports.length});
			archiveDetailsLabel = '';
		} else {
			archiveLabel = (selectedReports.length>1)?
				__('Archiver {{quantity}} rapports',{quantity:selectedReports.length})
				:__('Archiver {{quantity}} rapport',{quantity:selectedReports.length});
			archiveDetailsLabel = (selectedReports.length>1)?
				__('Les rapports peuvent être retrouvés en cochant la case "archivés" du tableau')
				:__('Le rapport peut être retrouvé en cochant la case "archivés" du tableau');
		}
		return {
			attributes: selectedReports,
			deleteLabel: deleteLabel,
			deleteDetailsLabel: deleteDetailsLabel,
			archiveLabel: archiveLabel,
			archiveDetailsLabel: archiveDetailsLabel,
			unsubscribeLabel: '',
			unsubscribeDetailsLabel: ''
		};
	}

   /**
	* 
	* @param {int array} selected_ids_r
	*/
   function createConversation(selected_ids_r) {
		var selector = selected_ids_r.map(x => "#report-" + x);
		var dt = $('#reports-table').DataTable();
		var teams = dt.rows(selector.join()).data();
		var users = [];
		var id_report_conv = null;
		var title = "";
		if(selected_ids_r.length == 1) {
			id_report_conv = selected_ids_r[0];
			title = teams[0].team_name;
		}
		// var id_missions = [];
		var members;
		for(var key in teams) {
			/*if (teams[key].id_mission) {
				id_missions.push(teams[key].id_mission);
			}*/
			members = teams[key].members;
			for(var key2 in members) {
				users.push({
					'id_user' : members[key2].id_user,
					'connected' : 0,
					'name' : members[key2].user_name+' '+members[key2].first_name,
				});
			}
		}
		if ($("#messagerie").is(":hidden")) {
			toggleWidget("messagerie");
		}
		/*if (id_missions.every( (val, i, arr) => val === arr[0] )) {
			var id_mission = id_missions[0];
		} else {
			id_mission = 0;
		}*/
		editConvParticipants(0, id_report_conv, title, users);
   }
   
   /**
	* 
	* @param {int array} selectedIds
	*/
   function createEmail(selectedIds) {
	   var selector = selectedIds.map(x => "#report-" + x);
		var dt = $('#reports-table').DataTable();
		var teams = dt.rows(selector.join()).data();
		var emails = [];
		var nb_members = 0;
		var members;
		for(var key in teams) {
			members = teams[key].members;
			for(var key2 in members) {
				nb_members++;
				if (members[key2].email) {
					emails.push(members[key2].email);
				}
			}
		}
		var nb_mails = emails.length;
		if(nb_mails) {
			if (nb_mails < nb_members) {
				alertGently(__( "{{nbm}} e-mail(s) récupéré(s) pour {{nbmem}} étudiant(s) dans les équipes sélectionnées", {nbm: nb_mails, nbmem: nb_members}), 'warning' );
			}
			window.location.href = "mailto:"+emails.join();
		} else {
			alertGently(__("Aucun e-mail récupéré pour les étudiants des équipes sélectionnées"), 'warning' );
		}
   }

   /* 
	* Modification des options d'un unique rapport
	* @returns {undefined}
	*/
   function initEditTeamPopup() { // initialisation des affichages
	   var id_report = getUniqueSelectedReportId();
	   if (id_report) {
		   $('.te-creation-only').addClass("nodisplay");
		   $('.te-updating-only').removeClass("nodisplay");
		   $("#te-popup-valid").off("click");
		   // récupération des infos du rapport
		   $.ajax({
			   type: "GET",
			   url: "/teacher/report/"+id_report+"/getReportAndLearners",
			   success: function (global_array) {
				   var report = global_array['report'];
				   $('#te-popup-title').html( __("Modifier les options du rapport {{team_name}}", { team_name : report['team_name'] } ) );
				   $("#te-mission").val(report['id_mission']);
				   if (report['status'] == "new") {
					   $("#te-mission").prop("disabled", false);
				   } else {
					   $("#te-mission").prop("disabled", true);
				   }
				   // report.status modification authorizations
				   if (report['initialized'] == 1) {
					   $('#te-team-status option[value="new"]').attr('disabled','disabled');
					   $('#te-team-status option[value="on"]').removeAttr('disabled');
					   $('#te-team-status option[value="wait"]').removeAttr('disabled');
				   } else {
					   $('#te-team-status option[value="new"]').removeAttr('disabled');
					   $('#te-team-status option[value="on"]').attr('disabled','disabled');
					   $('#te-team-status option[value="wait"]').attr('disabled','disabled');
				   }
				   $("#te-team-status").val(report.status);
				   $("#te-team-name").val(report['team_name']);
				   if (report.start_datetime) {
					   $("#te-start-date").val(report.start_datetime.substring(0, 10));
					   $("#te-start-time").val(report.start_datetime.substring(11, 16));
				   } else {
					   $("#te-start-date").val('');
					   $("#te-start-time").val('00:00');
				   }
				   $("#te-end-date").on('blur', function(e) {
					   const end_date = new Date($(e.target).val());
					   const current_date = new Date();
					   // if we change the end date of a waited report we change the report status to on
					   if( $("#te-team-status").val() === 'wait' && end_date > current_date) {
						   $("#te-team-status").val('on');
					   }
				   });
				   if (report.end_datetime) {
					   $("#te-end-date").val(report.end_datetime.substring(0, 10));
					   $("#te-end-time").val(report.end_datetime.substring(11, 16));
				   } else {
					   $("#te-end-date").val('');
					   $("#te-end-time").val('23:59');
				   }
				   if(report['allow_import'] && report['allow_import_id_mission'] == null){
					   $("#te-import-ld").val(LABDOC_IMPORT.ALL_MISSIONS_ALLOWED);
				   } else {
					   $("#te-import-ld").val(report['allow_import_id_mission']);
				   }
				  
				   if (report['allow_msg_teacher'] == 1) {
					   $('input[name=te-msg-teacher][value="1"]').prop("checked", true);
				   } else {
					   $('input[name="te-msg-teacher"][value="0"]').prop("checked", true);
				   }
				   if (report['allow_msg_team'] == 1) {
					   $('input[name=te-msg-team][value="1"]').prop("checked", true);
				   } else {
					   $('input[name="te-msg-team"][value="0"]').prop("checked", true);
				   }
				   $("#te-msg-class option").each(function() { // check if the mission still exists
					   if ($(this).val() == report['allow_msg_id_class']) {
						   $("#te-msg-class").val(report['allow_msg_id_class']);
					   }
				   });
				   $("#te-msg-mission option").each(function() { // check if the class still exists
					   if ($(this).val() == report['allow_msg_id_mission']) {
						   $("#te-msg-mission").val(report['allow_msg_id_mission']);
					   }
				   });
				   if (report['allow_attach_ld'] == 1) {
					   $('input[name=te-msg-send-ld][value="1"]').prop("checked", true);
				   } else {
					   $('input[name="te-msg-send-ld"][value="0"]').prop("checked", true);
				   }
				   if (report['allow_save_ld'] == 1) {
					   $('input[name=te-msg-save-ld][value="1"]').prop("checked", true);
				   } else {
					   $('input[name="te-msg-save-ld"][value="0"]').prop("checked", true);
				   }
				   $("#te-learners-list").html(global_array['learners']);
				   $("#te-popup-valid").on("click", function () {
					   updateReport(report.id_report);
				   });
				   let ids = [];
				   $('#te-learners-list span').each((i, e) => ids.push($(e).attr('data-id-learner')))
				   load_add_users('te-add-learner', '#te-popup', __('Nom, prénom'), 'both', 0, report.id_inst, ids);
				   $("#te-popup").dialog("open");
				   $("#te-team-name").focus(); // évite le focus sur l'ajout d'étudiants
			   }
		   });
	   }
   }
	function initInfoTitles() {
		info_title = {
			"Info": __("Nombre d'affichages de ressources et de consignes (générales, détaillées ou de parties)"),
			"Com": __("Nombre de messages ou de commentaires postés"),
			"Cnx_time": __("Temps total de connexion (ne signifie pas que les étudiants ont toujours été actifs durant ce temps)"),
			"Active_time": __("Temps passé à modifier des labdocs (les modifications peuvent être minimes ou de grands copier-coller)"),
			"Work_distr": __("Moyenne des changements de rédacteur par labdoc :\n- 0 indique que chaque labdoc n'a été écrit que par un seul étudiant\n- une valeur élevée indique que les labdocs ont été écrits par plusieurs étudiants avec de nombreux changements de rédacteur"),
			"Annotation": __("Nombre d'annotations affichées suffisamment longtemps pour être intégralement lues par au moins un étudiant / nombre total d'annotations dans le rapport")
		}
		info_colors = __("En regard de la médiane de classe : ")  + colorLabels
			.map(function(x, i){
				if (i < colorLabels.length - 1){
					return indicatorDomainTreshold[i] + '% = ' + x
				} else {
					return indicatorDomainTreshold[i] + __('% ou plus = ') + x
				}
			})
			.reduce((carry, current) => carry == '' ? current : carry + " | " + current, '');
	}
	
	function getReportsId() {
	   let reportsIds = dataTable.rows({search: 'applied'}).ids();
	   if (reportsIds.length > 0) {
		   return reportsIds.map(x => parseInt(x.replace('report-', '')));
	   } else {
		   return [];
	   }
	}
	
	function deleteLearnerFromTeam(id_user) {
		$('#te-learners-list').find('span[data-id-learner="'+id_user+'"]').remove() ;
		window.dispatchEvent(new CustomEvent('lnb-select2-remove-id', {
			detail:{
				selector: "te-add-learner",
				idElement: id_user
			}}));
	}

	function addLearnerToTheTeam(selectedLearner) {
		if(selectedLearner) {
			add_user_in_staging_from_id_and_name(selectedLearner.id,
				selectedLearner.text,
				'#te-learners-list',
				'id-learner',
				'reportsTable.deleteLearnerFromTeam($(this).closest(\'span\').attr(\'data-id-learner\'))',
				__('Supprimer l\'étudiant de l\'équipe'));
		}
	}
	
	return {
		delete: function(selectedReports) {
			if(confirm(__('ATTENTION : La suppression est irréversible'))) {
				if(selectedReports.length===1){
					deleteReport();
				}
				if(selectedReports.length>1){
					deleteReports();
				}
				return true;
			} else {
				return false;
			}
		},
		archive: function(selectedReports) {
			if(selectedReports.every((report) => report.status === 'arc')) {
				selectedReports = selectedReports.map((r) => { r.status = getNewReportStatusAfterToggleArchive(r.id_report); return r; });
			} else {
				selectedReports = selectedReports.map((r) => { r.status = 'arc'; return r; });
			}
			if (selectedReports.length === 1) {
				updateReportStatus(selectedReports[0]);
			} else if (selectedReports.length > 1) {
				updateReportsStatus(selectedReports);
			}
		},
		init: function() {
			initDataTables();
			initEvents();
			initInfoTitles();
		},
		reload: function() {
			// return dataTable.ajax.reload.apply(dataTable.ajax, arguments);
			dataTable.ajax.reload(function() {
				let id_report = getUniqueSelectedReportId();
				if (id_report) {
					selectRow(id_report);
					openReportDetails(id_report);
				}
			},false);
		},
		updateReportRow: updateReportRow,
		getReportsId: getReportsId,
		followReports: followReports,
		deleteLearnerFromTeam: deleteLearnerFromTeam,
		addLearnerToTheTeam: addLearnerToTheTeam,
		refreshTable: refreshTable,
		getParamsArchivePopup: getParamsArchivePopup
	};
})();

/**
 * Initialisations
 */
$(document).ready(function () {
	"use strict";
	var isAdmin = $('#reports-table').hasClass('always-reload');
	// auto-select class and mission if the URL ends with #teamconfig=<ID>
	var hash = window.location && window.location.hash;
	if (hash) {
		var m = hash.match(/\bteamconfig=(\d+)\b/);
		if (m) {
			var id_team_config = m[1];
			$.ajax({
				type: "GET",
				url: '/teamconfig/'+id_team_config+'/getFullTeams',
				success: function(response){
					var tc = response.settings;
					$("#reports-filter-mission").val(tc.id_mission);
					$("#reports-filter-class").val(tc.id_class);
					// toto check
				}
			});
		}
	}
	i18next_Promise.then(function() {
		reportsTable.init();
	});
	defineModal("open-multi-reports-popup", null, false, "reports-actions");
	defineModal("archive-popup", null, false, "reports-actions");
	defineModal("te-popup", null, false, "reports-actions");
	defineModal("batch-update-popup", null, false, "reports-actions");
	defineModal("ta-popup", null, false, "reports-actions");

	$("#display-archives").prop('checked', false);	
});

document.addEventListener('alpine:init', () => {
	Alpine.store('reportFilters', {
		id_class: 0,
		class_name: '',
		id_mission: 0,
		mission_code: '',
		pattern: "",
		teaming: false,
		archive: false,
		getFilters() {
			return {
				id_class: this.id_class,
				class_name: this.class_name,
				id_mission: this.id_mission,
				mission_code: this.mission_code,
				status: this.archive?['new','on','wait','arc']:['new','on','wait'],
				teaming: this.teaming?1:0,
			}
		},
		selectMission(selectedMission){
			if(selectedMission && selectedMission.id !== 0) {
				this.id_mission = selectedMission.id;
				this.mission_code = selectedMission.text;
			} else {
				this.id_mission = 0;
				this.mission_code = '';
			}
			reportsTable.refreshTable();
		},
		selectClass(selectedClass){
			if(selectedClass && selectedClass.id !== 0) {
				this.id_class = selectedClass.id;
				this.class_name = selectedClass.text;
			} else {
				this.id_class = 0;
				this.class_name = '';
			}
			reportsTable.refreshTable();
		},
		initSessionReportFilters() {
			if (window.localStorage.reportFilters) {
				const reportFilters = JSON.parse(window.localStorage.reportFilters);
				this.id_class = reportFilters.id_class ? parseInt(reportFilters.id_class) : 0;
				this.class_name = reportFilters.class_name ? reportFilters.class_name : '';
				this.id_mission = reportFilters.id_mission ? parseInt(reportFilters.id_mission) : 0;
				this.mission_code = reportFilters.mission_code ? reportFilters.mission_code : '';
				this.pattern = reportFilters.pattern ? reportFilters.pattern : '';
				this.archive = reportFilters.status ? reportFilters.status.includes('arc') : false;
				this.teaming = reportFilters.teaming ? (reportFilters.teaming === '1') : false;
				reportsTable.refreshTable();
			}
			window.addEventListener('lnb-select2-component-initialized', (event) => {
				// When the select2 are initialized we get back back the stored class and mission from the session
				if(event.detail.selector) {
					if(event.detail.selector === 'reports-filter-mission'
						&& Alpine.store('reportFilters').id_mission !== 0) {
						$('#reports-filter-mission').append(new Option(
							Alpine.store('reportFilters').mission_code,
							Alpine.store('reportFilters').id_mission, true, true))
							.trigger('change.select2');
					}
					if(event.detail.selector === 'reports-filter-class'
						&& Alpine.store('reportFilters').id_class !== 0) {
						$('#reports-filter-class').append(new Option(
							Alpine.store('reportFilters').class_name,
							Alpine.store('reportFilters').id_class, true, true))
							.trigger('change.select2');
					}
				}
			});
		}
	});
	Alpine.store('reportFilters').initSessionReportFilters();
	Alpine.data('reportFiltersComponent', () => ({
		init() {
			this.$watch('$store.reportFilters', () => {
				window.localStorage.setItem('reportFilters', JSON.stringify(Alpine.store('reportFilters').getFilters()));
			});
		}
	}));
});


function displayReportMessages(id_report) {
	toggleWidget("messagerie");
	return false;
}


/**
 * Vérifie succintement si les entrées de la popup sont valide, affiche un message à l'utilisateur dans le cas contraire.
 * @returns boolean vrai si les entrées de la popup de gestion des équipe sont valide , faux sinon.
 */
function valideTeamPopup(){
	var regex_date = /^[0-9]{4}-[0-9]{2}-[0-9]{2}$/ ;
	if ($('#te-learners-list').find("span").length == 0) {
		if ($("#te-add-learner").val() > 0) { alert ( __("Veuillez valider l'étudiant ajouté avant de valider l'équipe") ) ; }
		else { alert( __("Veuillez sélectionner des étudiants") ) ; }
		return false ;
	}
	if (document.getElementById("te-create-random").checked && ! (Number($('#te-create-random-number').val()) > 0)) {
		alert ( __("Veuillez indiquer la taille des équipes créées aléatoirement") ) ;
		return false ;
	}
	if (document.getElementById("te-create-old-mission").checked && $('#te-create-old-mission-id').val() == "0") {
		alert ( __("Veuillez indiquer la mission sur laquelle se baser pour créer les équipes") ) ;
		return false ;
	}
	if (($('#te-start-date').val() && !regex_date.test($('#te-start-date').val())) || ($('#te-end-date').val() && !regex_date.test($('#te-end-date').val()))) {
		alert( __("Les dates doivent être au format : aaaa-mm-jj") ) ;
		return false ;
	}
	if ($('#te-end-date').val()) {
		var endDate = new Date($('#te-end-date').val() + " " + $('#te-end-time').val());
		var status = $("#te-team-status").val();
		if (endDate < new Date() && (status === "new" || status === "on")) {
			alert( __("La date de fin de travail doit être dans le futur") ) ;
			return false ;
		}
		if ($('#te-start-date').val() && endDate <= new Date($('#te-start-date').val() + " " + $('#te-start-time').val())) {
			alert( __("La date de début du travail doit précéder celle de fin") ) ;
			return false ;
		}
	}
	if ($('#te-mission').val() == "0") {
		alert ( __("Veuillez indiquer la mission à attribuer aux équipe(s)") ) ;
		return false ;
	}
	if ($("#te-add-learner").val() > 0 ) {
		if (confirm ( __('Attention : du texte est contenu dans le champ "Ajouter" sans avoir été validé.') + "\n" + __("Souhaitez vous confirmer les modifications effectuées ?"))) {
			return true ;
		}
		else { return false ; }
	}
	return true ;
}

/**
 * fonction resetTeamPopup
 * restore la valeur initiale des champs qui ne doivent pas être conservé du popup de gestion des équipes
 */
function resetTeamPopup(){
	document.getElementById("te-add-learner").value = -1;
	document.getElementById("te-learners-list").value = __("En création...") ;
	//document.getElementById("te-create-classic").checked = true;
	document.getElementById("te-team-name-label").innerHTML = __("Nom de l'équipe") + __("&nbsp;:");
	$("#te-mission").prop("disabled", false) ;
}


/**
 * Vérifie la validité des informations du popup de gestion des équipes
 * récupère les informations du popup et les converti en format facilement exploitable par le serveur.
 * Ajoute ou met à jour une équipe en fonction des données de la popup de gestion des équipes
 */
function updateReport(id_report){
	if (!valideTeamPopup()){
		return $.Deferred().reject('validation failed');
	}
		
	var learners_list = [] ; // recuperation des étudiants sélectionnés
	$('#te-learners-list').find("span").each(function() { learners_list.push($(this).attr("data-id-learner")) ; });

	$('#te-popup-valid').hide();
	$('#te-popup-wait').show();

	return $.ajax({
		type: "POST",
		url: "/teacher/reports/" + id_report,
		data: {
			team_name: $("#te-team-name").val(),
			start_date: $("#te-start-date").val(),
			start_time: $("#te-start-time").val(),
			end_date: $("#te-end-date").val(),
			end_time: $("#te-end-time").val(),
			allow_import : $('#te-import-ld').val() === "0" ? 0 : 1 ,
			allow_import_id_mission:  $('#te-import-ld').val() === "-1" ? null : $("#te-import-ld").val(),
			allow_msg_teacher: $('input[name=te-msg-teacher]:checked').val(),
			allow_msg_team: $('input[name=te-msg-team]:checked').val(),
			allow_msg_id_class: $("#te-msg-class").val(),
			allow_msg_id_mission: $("#te-msg-mission").val(),
			allow_attach_ld: $('input[name=te-msg-send-ld]:checked').val(),
			allow_save_ld: $('input[name=te-msg-save-ld]:checked').val(),
			status: $('#te-popup [name=status]').val(),
			learners_list: learners_list
		},
		success: function(content){
			console.log(content);
			content.errors.forEach(function (e) {
				alertGently(e, 'danger');
			});
			if (content.errors.length === 0) {
				resetTeamPopup();
				$("#te-popup" ).dialog( "close" );
				reportsTable.reload();
			}
			$('#te-popup-valid').show();
			$('#te-popup-wait').hide();
		},
		error: function(jqXhr) {
			let message = jqXhr.responseJSON.message ? jqXhr.responseJSON.message : jqXhr.statusText;
			alertGently(message);
			$('#te-popup-valid').show();
			$('#te-popup-wait').hide();
		}
	});
}

/**
 * @param {string} date
 * @param {string} time
 * @param {boolean} end_of_day
 * @return {Date} or null
 */
function parseIsoDatetime(date, time, end_of_day) {
	if (!date) {
		return null;
	}
	else if (!date.match(/^\d{4}-\d\d-\d\d/)) {
		console.error("Invalid date, not an ISO format ", date);
		return null;
	} else if (time) {
		return new Date(date.trim() + 'T' + time + ':00');
	} else if (end_of_day) {
		return new Date(date.trim() + 'T23:59:59');
	}  else {
		return new Date(date.trim() + 'T00:00:00');
	} 
}

/**
 * @param {Object} report
 * @return {Array} List of errors, [] if the attributes are valid
 */
function validateReportAttributes(report) {
	var errors = [];
	var end_datetime = parseIsoDatetime(report.end_date, report.end_time, true);
	if (end_datetime) {
		if (report.status === 'on' && end_datetime.getTime() < (new Date().getTime())) {
			errors.push( __("La date de fin de travail doit être dans le futur") );
		}
			var start_datetime = parseIsoDatetime(report.start_date, report.start_time, false);
		if (start_datetime) {
			if (start_datetime.getTime() > end_datetime.getTime()) {
				errors.push( __("La date de début du travail doit précéder celle de fin") );
			}
		}
	}
	return errors;
}

/**
 * Display the student traces from simulation and classic report
 */
function displayClassDashboard() {
	getTracesAndPlotHeatmap();
	getClassSimulationDashboard();
}

/**
 * get and display the simulation data from the traces related to the selected class
 */
function getClassSimulationDashboard(reportFilters) {
	let id_mission = Alpine.store('reportFilters').id_mission;
	let id_class = Alpine.store('reportFilters').id_class;
	let classSimulationDashboard = document.getElementById('traces-simulation-dashboard');
	if (id_mission > 0 && id_class > 0) {
		classSimulationDashboard.innerHTML = "<i class='fa fa-spinner fa-pulse'></i> "+__("Chargement en cours");
		$.ajax({
			method: 'GET',
			url: '/simulation/getClassSimulationDashboard',
			data: {
				idMission: id_mission,
				idClass: id_class,
			}
		}).done(function (simulationDashboardView) {
			if(simulationDashboardView.message !== undefined) {
				// Not a simulation
				classSimulationDashboard.innerHTML = '';
			} else {
				classSimulationDashboard.innerHTML = simulationDashboardView;
			}
		});
	} else {
		classSimulationDashboard.innerHTML = '';
	}
}

/**
 * Retreive students traces and call the heatmap drawer
 * @return {boolean} true
 */
function getTracesAndPlotHeatmap(){
	let id_mission = Alpine.store('reportFilters').id_mission;
	let id_class = Alpine.store('reportFilters').id_class;
	let gDiv = setHeatmapParams().graphDiv;
	clearGraphDiv(gDiv);
	setSpinner(gDiv);
	if (id_mission > 0 && id_class > 0 && reportsTable.getReportsId().length > 0) {
		$.ajax({
			method: 'POST',
			url: '/teacher/traces/selectDataToPlotHeatmap',
			data: {
				id_mission: id_mission,
				id_class: id_class,
			},
			success: function(traces){
				let heatmapReturnValue = true;
				if (traces.length != 0) {
					removeSpinner(gDiv);
					heatmapReturnValue = tracesHeatmap.draw(traces, setHeatmapParams());
				} 
				if (traces.length == 0 || heatmapReturnValue == false){
					clearGraphDiv(gDiv);
				}
			}
		});
	} else {
		clearGraphDiv(gDiv);
	}
	return true;
}


/**
 * Remove the content of graphDiv
 */
function clearGraphDiv(gDiv){
	gDiv.parent().children().each(
		function(idx, val){
			$(val).empty();
		});
	removeSpinner(gDiv);
}


/**
 * Display a spinner in graphDiv
 */
function setSpinner(gDiv){
	gDiv.append('<div class="container-spinner"><div class="spinner spinner"></div></div>');
}


/**
 * Display a spinner in graphDiv
 */
function removeSpinner(gDiv){
	gDiv.children("div.container-spinner").remove();
}


/**
 * Set parameters in order to draw a heatmap
 * @return {object} params: list of parameters
 * The full list of parameters is available in public/js/teacher/plotly.js
 */
function setHeatmapParams(){
	let params = {
		reportsOrder: reportsTable.getReportsId(),
		graphDiv: $('div#heatmap-div'),
		// Fill the starting time of each slot in ascending order
		// The slot starting from the last edge and lasting with the first one is the extra-class slot
		slotsEdges: ['07:30:00', '13:00:00', '18:30:00'],
		slotsLabel: [__('matin'), __('après-midi'), __('soir/nuit')],
		// Size of graph bins
		binSizeX: 15,
		binSizeY: 20,
	};
	return params;
}


/**
 * Return a button which allows to display the collaborative dashboard
 * @param {Object} rowData: context data relative to the chosen report row
 * @return {string} html
 */
function setCollabDashboardLink(rowData){
	let id_mission = parseInt(rowData.id_mission);
	let id_report = parseInt(rowData.id_report);
	let id_team_config = parseInt(rowData.id_team_config) 
	let html = `
		<div class="collaborative-dashboard-button-container">
			<button type="button" onclick="getTracesAndPlotCollabDashboard(${id_mission},${id_report},${id_team_config})"> 
				<span>${__('Afficher la chronologie de rédaction')}</span>
				<i title="${__('Afficher la chronologie de rédaction')}" class="fa-solid fa-chart-gantt"></i>
			</button>
		</div>
	`;
	return html;
}

/**
 * Show datetime inputs when reports batch update popup is opened
 * @param element: js selector of the modify button
 */
function modifyReportsDatetime(element){
	let fieldName = $(element).attr('id').split('-')[1];
	$(element).parent().hide();
	$(element).parent().parent().find('[name="' + fieldName + '_date"]').show();
	$(element).parent().parent().find('[name="' + fieldName + '_time"]').show();
	$(element).parent().parent().find('#undo-modify-' + fieldName + '-datetime').show();
	$(element).parent().parent().find('[name="' + fieldName + '_datetimes_are_unified"]').val("1");
}


/**
 * Retreive students traces and call the collab dashboard drawer
 * @param {int} id_mission 
 * @param {int} id_report
 * @param {int} id_team_config
 * @return {bool} true
 */
function getTracesAndPlotCollabDashboard(id_mission, id_report, id_team_config){
	let params = setCollabDashboardParams(id_report);
	let gDiv = params.graphDiv;
	clearGraphDiv(gDiv);
	$('.collaborative-dashboard-container').show();
	$('.collaborative-dashboard-button-container').hide();
	setSpinner(gDiv);
	if (id_mission > 0 && id_report > 0 && id_team_config > 0) {
		$.when(
			getStructure(id_report),
			getCollabTraces(id_mission, id_report, id_team_config)
		).then(
			// Success
			function (structure, traces) {
				let collabReturnValue = true;
				if (structure[0].length != 0 && traces[0].length != 0) {
					removeSpinner(gDiv);
					collabReturnValue = collabDashboard.draw(structure[0], traces[0], params);
				} 
				if (structure[0].length == 0 || traces[0].length == 0 || collabReturnValue == false){
					alertGently(__("Les données récupérées ne permettent pas d'afficher la chronologie de rédaction du rapport."));
					clearGraphDiv(gDiv);
					$('.collaborative-dashboard-container').hide();
				}
			},
			// Failure
			function (structure, traces) {
				alertGently(__("Erreur lors de la récupération des traces de collaboration."));
				clearGraphDiv(gDiv);
				$('.collaborative-dashboard-container').hide();
			}
		);
	}
	return true;
}


/**
 * Retreive report structure: report parts, labdocs positions
 * @param {int} id_mission
 * @param {int} id_report
 * @param {int} id_team_config
 * @return {promise}
 */
function getCollabTraces(id_mission, id_report, id_team_config){
	return $.ajax({
		method: 'POST',
		url: '/teacher/traces/selectDataToPlotDashboardCollab',
		data: {
			id_mission: id_mission,
			id_report: id_report,
			id_team_config: id_team_config
		}
	});
}


/**
 * Retreive report structure: report parts, labdocs positions
 * @param {int} id_report
 * @return {promise}
 */
function getStructure(id_report){
	return $.ajax({
		method: 'GET',
		url: '/teacher/report/'+id_report+'/getStructure'
	});
}

/**
 * Set parameters in order to draw a collab dashboard
 * @param {int} id_report
 * @return {object} params: list of parameters
 * The full list of parameters is available in public/js/teacher/plotly.js
 */
function setCollabDashboardParams(id_report){
	let params = {
		graphDiv: $('div#collaborative-dashboard-div_'+id_report),
		// Fill the starting time of each slot in ascending order
		// The slot starting from the last edge and lasting with the first one is the extra-class slot
		slotsEdges: ['07:30:00', '13:00:00', '18:30:00'],
		slotsLabel: [__('matin'), __('après-midi'), __('soir/nuit')],
	};
	return params;
}

/**
 * Hide datetime inputs when reports batch update popup is opened
 * @param element: js selector of the cancel button
 */
function undoModifyReportsDatetime(element){
	let fieldName = $(element).attr('id').split('-')[2];
	$(element).parent().find('[name="' + fieldName + '_date"]').val("").hide();
	$(element).parent().find('[name="' + fieldName + '_time"]').val(fieldName === 'start' ? '00:00' : '23:59').hide();
 	$(element).parent().find('#modify-' + fieldName + '-datetime').parent().show();
	$(element).parent().parent().find('[name="' + fieldName + '_datetimes_are_unified"]').val("0");
	$(element).hide();
}
