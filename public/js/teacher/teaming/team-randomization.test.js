const randomizer = require("./team-randomization")
{
	const teamConfig = {
		size_opt: 3,
		size_max: 4,
		size_min: 1,
		teams_max: 3,
	}
	const rand = randomizer(teamConfig);
	test("randomize triplets: computeNumberOfTeams()", () => {
		expect(rand.computeNumberOfTeams(7)).toBe(2);
		expect(rand.computeNumberOfTeams(8)).toBe(3);
		expect(rand.computeNumberOfTeams(9)).toBe(3);
		expect(rand.computeNumberOfTeams(11)).toBe(3);
		expect(rand.computeNumberOfTeams(12)).toBe(3);
		expect(rand.computeNumberOfTeams(21)).toBe(3);
	})
	test("randomize triplets: allocate()", () => {
		const users = ["user1", "user2", "user3", "user4", "user5", "user6", "user7"]
		const teams = [{members:[]}, {members:[]}]
		const studentsTotal = 7

		rand.allocate(users, teams, studentsTotal)
		expect(teams[0].members.length).toBe(4);
		expect(teams[1].members.length).toBe(3);

		const members = [].concat(teams[0].members, teams[1].members)
		expect(members.sort()).toEqual(users.sort());
	})
}

{
	const teamConfig = {
		size_opt: 2,
		size_max: 4,
		size_min: 1,
		teams_max: 10,
	}
	const rand = randomizer(teamConfig);
	test("randomize duets: computeNumberOfTeams()", () => {
		expect(rand.computeNumberOfTeams(6)).toBe(3);
		expect(rand.computeNumberOfTeams(7)).toBe(3);
		expect(rand.computeNumberOfTeams(8)).toBe(4);
		expect(rand.computeNumberOfTeams(22)).toBe(10);
	})
}

