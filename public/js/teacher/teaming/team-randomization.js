'use strict';

function TeamRandomization (teamConfig) {
	let numberOfTeams = 0;
	let residualStudents = 0;
	let studentsInTeaming = 0;
	let studentsMovable = 0;
	let distributionMode = -1; // undefined
	const DISTRIB_OPT = 0;
	const DISTRIB_MAX_FIT = 1
	const DISTRIB_MIN_THEN_OPT = 2;
	/**
	 * In place shuffling.
	 *
	 * @param {array} array
	 */
	function shuffle (array) {
		const copy = Array.from(array)
		let currentIndex = copy.length;
		let randomIndex;
		let tmp;
		while (currentIndex !== 0) {
			randomIndex = Math.floor(Math.random() * currentIndex);
			currentIndex--;
			// swap
			tmp = copy[currentIndex];
			copy[currentIndex] = copy[randomIndex];
			copy[randomIndex] = tmp;
		}
		return copy;
	}
	/**
	 * Fill each team so that it contains totalPerTeam users, or the users list is empty.
	 *
	 * Modifies the array parameters.
	 *
	 * @param {array} users (modified)
	 * @param {array} teams (modified)
	 * @param {number} totalPerTeam
	 * @return {undefined}
	 */
	function allocateUsers (users, teams, totalPerTeam) {
		if (users.length === 0 || !totalPerTeam) {
			return;
		}
		for (const team of teams) {
			while (team.members.length < totalPerTeam) {
				team.members.push(users.pop());
				if (users.length === 0) {
					return;
				}
			}
		}
	}

	/**
	 * This function computes the number of teams and decide the repartition mode
	 * It will try to distribute the students in one of the following way (in this order)
	 * According to the constraint given by the teacher on the team size and number
	 * 1. All students in team of size_opt
	 * 2. All student in team of size_opt and 1 team with residualStudents
	 * 3. All students in team of size_opt and distribute residualStudents in a few teams
	 * 4. All students in team of size_opt than move a few students with residualStudents in a separate team to fit the constraints
	 */
	function computeNumberOfTeams (total_students) {
		numberOfTeams = Math.min(
			teamConfig.teams_max,
			Math.floor(total_students / teamConfig.size_opt)
		);
		residualStudents = total_students - numberOfTeams * teamConfig.size_opt;
		studentsMovable = numberOfTeams * (teamConfig.size_opt - teamConfig.size_min);
		if (residualStudents === 0) {
			distributionMode = DISTRIB_OPT;
		} else if (canPutResildualStudentsInOneTeam()) {
			distributionMode = DISTRIB_OPT;
			// We need one more team
			numberOfTeams++;
		} else if (canDistributeResidaulStudentsOverExistingTeams()) {
			distributionMode = DISTRIB_MAX_FIT;
		} else if (canMoveStudentsFromExistingTeamToCreateNewTeamForResiduals()) {
			distributionMode = DISTRIB_MIN_THEN_OPT;
			// We need one more team
			numberOfTeams++;
		}
	}

	function canPutResildualStudentsInOneTeam () {
		return residualStudents >= teamConfig.size_min && numberOfTeams + 1 <= teamConfig.teams_max;
	}

	function canDistributeResidaulStudentsOverExistingTeams () {
		return Math.ceil(residualStudents / numberOfTeams) + parseInt(teamConfig.size_opt) <= teamConfig.size_max;
	}

	function canMoveStudentsFromExistingTeamToCreateNewTeamForResiduals () {
		return studentsMovable >= teamConfig.size_min - residualStudents;
	}

	return {
		getNumberOfTeams: function () {
			return numberOfTeams;
		},
		allocate: function (usersToAdd, teams, studentsTotal) {
			if (teams.length === 0) {
				return;
			}
			const toAdd = shuffle(usersToAdd);
			console.log('Distribution ' + toAdd.length + ' students on ' + teams.length + ' teams using mode ' + distributionMode);
			switch (distributionMode) {
				case DISTRIB_OPT:
					allocateUsers(toAdd, teams, teamConfig.size_opt);
					break;
				case DISTRIB_MIN_THEN_OPT:
					// Here we must fill with less than opt during the first round, then compelete
					// the teams that we can
					allocateUsers(toAdd, teams, teamConfig.size_min);
					allocateUsers(toAdd, teams, teamConfig.size_opt);
					break
				case DISTRIB_MAX_FIT:
					// In this case we cannot add one more team, so first we fill
					// the teams with the same amount of students
					const numPerTeam = Math.floor(studentsInTeaming / numberOfTeams);
					allocateUsers(toAdd, teams, numPerTeam);
					if (toAdd.length > 0) {
						// Then we redistrubute the remaining student as scattered as possible
						const extraTeamSize = numPerTeam + toAdd.length / numberOfTeams;
						allocateUsers(toAdd, teams, extraTeamSize);
					}
					break;
				default:
					throw new Error('Undefined distributionMode');
			}
		},
		validate: function (total_students) {
			studentsInTeaming = total_students;
			const max_places = teamConfig.teams_max * parseInt(teamConfig.size_max);
			if (max_places < total_students) {
				return __("Mode de répartition mal configuré : les contraintes de répartition (taille des équipes et nombre max. d'équipes) ne permettent de mettre en équipe que {{max_places}} des {{total_students}} étudiants de la classe.", { max_places, total_students});
			}
			computeNumberOfTeams(total_students);
			if ( !canPutResildualStudentsInOneTeam()
				&& !canDistributeResidaulStudentsOverExistingTeams()
				&& !canMoveStudentsFromExistingTeamToCreateNewTeamForResiduals()) {
				return __("Mode de répartition mal configuré : impossible de répartir les étudiants en respectant toutes les contraintes");
			}
			
			return '';
		}
	};
}
// Uncomment For test
//module.exports = TeamRandomization
