// *******************************************
//            Diverse
// *******************************************

const LABDOC_IMPORT ={
	NOT_ALLOWED : 0,
	ALL_MISSIONS_ALLOWED : -1
}

$( document ).ready(function() {
	fetchMessages();
    setInterval(fetchMessages, $("#refresh_period").val() * 1000);
});

/**
 * supprime l'accentuation d'une chaine de caractère str
 * @param str : string
 */
function removeAccent(str) {
	return str.trim()
		.normalize('NFD')
		.replace(/[\u0300-\u036f]/g, "")
		.replace(/[^A-Za-z0-9]+/g, '_');
}

/**
 * Controle une chaine de caractere et envoie un message si la taille est 0 ou superieure a la valeur max donnee ou si il y a des guillements.
 * @param element {jquery} element sur lequel va porter le test
 * @param name {string} nom du champ
 * @param min_size {number} taille minimale de la chaine de caracteres
 * @param max_size {number} taille maximale de la chaine de caracteres
 * @param msg {boolean} doit on afficher un message
 */
function ATControlString(element, name, min_size, max_size, msg){
    var str_content = element.val().trim() ;
    str_content = str_content.replace(/"/g,"") ; // suppression des guillemets
    element.val(str_content) ;
    var str_size = str_content.length ;
    if (str_size < min_size) {
        if (msg && min_size==1) {
			alertGently(  __('Le champ "{{field}}" doit être renseigné', {field: name}) ,
			'error' );
		}
        else if (msg){
			alertGently(  __('Le champ "{{field}}" doit contenir au moins {{size}} caractères', {field : name, size : min_size}) ,
			'error' );
		}
        return false ;
    }
    if (str_size > max_size) {
        if (msg) {
			alertGently(  __('Le champ "{{field}}" doit contenir au plus {{size}} caractères', {field : name, size : max_size}) ,
			'error' );
		}
        return false ;
    }
    return true ;
}

/**
 * Ouvre une nouvelle fenetre avec un rapport de test (generation d'une equipe et du report)
 * suppression de l'équipe précédente si il existe déjà un report test
 * @param {int} id_mission : id de la mission sur laquelle générer un rapport
 * @returns {undefined}
 */
function openTestReport(id_mission){
	var newWindow = window.open('', '_blank');
	$.ajax({
		type: "POST",
		url: "/teacher/mission/"+id_mission+"/createTestReport",
	})
	.fail(function(msg){ alert("Erreur 777987 : "+msg); })
	.success(function(id_report){
		newWindow.location = '/report/'+id_report;
	});
}

/**
 * 
 * @returns {undefined}
 */
function fetchMessages() {
	console.log("FetchMessages");
	$.ajax({
		type: "GET",
		url: "/conversation/fetchMessages",
		cache: false,
		success: function (data) {
			// console.log(data);
			var ids_conversation = [];
			var nb_conv = 0;
			$(data).find('message conv').each(function(){
				ids_conversation["key_"+$(this).attr("id_conv")]=$(this).text();
				nb_conv++;
			});
			ids_conversation.length = nb_conv;
			refreshMessage(ids_conversation);
		}
	});
}

// *******************************************
//            Gestion des tableaux 
// *******************************************

/**
 * supprime la selection faite sur la table
 * @param tab : indique le type de table à initialiser = pm, mm, cl, le
 */
function clearSelectTable(tab){	
	$("."+tab+"-expandable").removeClass('selected') ; // on supprime l'attribut de class affiché
	$("."+tab+"-expanded").addClass('nodisplay') ; // on supprime les tr de détails (en bleu)
	$("#"+tab+"-toggle-options").addClass('nodisplay') ; // on cache les icones de modification de la sélection
	// on met à jour les variable d'état de la selection
	if (tab=="pm") { pm_selected = 0; }
	else if (tab=="mm") { mm_selected = 0; }
	else if (tab=="cl") {
		cl_selected = 0;
		clearSelectTable("le") ;
		$("#le-table").addClass("nodisplay");
	}
	else if (tab=="le") { le_selected = 0; }
}

/**
 * fonction : filterTableBody
 * @param val : valeur de recherche (saisie utilisateur)
 * @param tbody : id de la div parente contenant les lignes (tr) sur lesquelles rechercher. ces lignes doivent(ignorées sinon) contenir un champs data-keywords séparés par "§".
 * effectue un tri sur les lignes d'un tableau HTML enrichi en fonction des mot clé présent dans ces lignes
 */
function filterTableBody(val, tbody) {
	var parent = document.getElementById(tbody);
	var children = parent.getElementsByTagName('tr');// any tag could be used here..
	var keywords = "";
	var contain_keyword = false;
	val = removeAccent(val.toLowerCase());

	for (var i = 0; i < children.length; i++) {
		if (children[i].getAttribute('data-keywords') !== null) {
			keywords = children[i].getAttribute('data-keywords').split("§");
			contain_keyword = false;
			for (var j = 0; j < keywords.length; j++) {
				if (removeAccent(keywords[j].toLowerCase()).indexOf(val) > -1) {
					contain_keyword = true;
				}
			}

			if (contain_keyword) {
				children[i].classList.remove("nodisplay");
			} else {
				children[i].classList.add("nodisplay");
			}
		}
	}
	paintTable(tbody);
}

/**
 * fonction paintTable
 * affiche une ligne sur 2 en gris foncé
 * évite les incohérences de couleurs de ligne lors de la recherche (qui passe certaine lignes en nodisplay)
 */
function paintTable(tbody_id){
	var type1 = false;
	var tr_table = document.getElementById(tbody_id).getElementsByClassName("visible-line");
	for(i = 0; i < tr_table.length; i++){
		tr_table[i].classList.remove("tr-type-1");
		tr_table[i].classList.remove("tr-type-2");
		if(!tr_table[i].classList.contains("nodisplay") && !tr_table[i].classList.contains("class-outdated")){
			if(type1){
				tr_table[i].classList.add("tr-type-1");
			}
			else{
				tr_table[i].classList.add("tr-type-2");
			}
			type1 = !type1;
		}
	}
}


/***********************************
 *   Popup gestion équipes		   *
 ***********************************/

// permet de modifier certains intitulés dynamiquement
function teamPopupInit(teams) {
	if (teams == 1) {
		$("#te-team-legend").html( __("Définition de l'équipe") ) ;
		$("#te-team-name-label").html( __("Nom de l'équipe") + __("&nbsp;:") ) ;
	} else {
		$("#te-team-legend").html( __("Définition des équipes") ) ;
		$("#te-team-name-label").html( __("Nom des équipes") + __("&nbsp;:") ) ;
	}
	if (teams == 3) {
		$("#te-team-name").attr("placeholder", "Conservés");
	} else {
		$("#te-team-name").attr("placeholder", "");
	}
}




/**
 * Replace the accented characters by their ASCII equivalence.
 *
 * @param {String} str
 * @return {String}
 */
function unaccent(str) {
	if (String.prototype.normalize) {
		return str.normalize('NFD').replace(/[\u0300-\u036f]/g, "");
	} else {
		return str;
	}
};

/**********************************************************
 *        Gestion user
 **********************************************************/

/**
 * Return user search by what with var1 and var2 parameter with role 
 * role can be : all, learner, teacher
 * @param {String} what
 * @param {String} var1
 * @param {String} var2
 * @param {String} type
 * @param {String} id_inst
 * @param {function} successFunction
 * @param {function} serrorFunction
 * @return array of user
 */
function commonSearchUserBy( what, var1, var2, type, id_inst, successFunction, serrorFunction){
	$.ajax({
		method: "POST",
		url: "/users/getUserWithInstBy",
		data: {
			what: what,
			var1: var1,
			var2: var2,
			type: type,
			id_inst : id_inst
		}
	})
	.done(function (data) { successFunction(data); })
	.fail(function (jqXHR, textStatus, errorThrown) { serrorFunction(); });
}

/**
 * fonction autoLogin
 * Génère automatiquement un login en fonction du nom et du prénom saisi par l'utilisateur lors de la création d'un étudiant
 * @param {string} mod : first_name
 * @param {string} first_name_id : first_name
 * @param {string} user_name_id : user_name
 * @param {string} login_id : login_id
 * @returns {undefined}
 */
function autoLogin(mod, first_name_id, user_name_id,login_id){
	var username = getCleanVal(user_name_id)  ;
	var firstname = getCleanVal(first_name_id) ;
	var login = $(login_id).val() ;
	if (mod == "first_name" && firstname.length == 1) {
		if (login == username.substring(0,7)) { // on vient d'ajouter la première lettre du prénom
			$(login_id).val(login + firstname.charAt(0)) ;
		}
	}
	else if (mod == "first_name" && firstname.length == 0) {
		if (login.substring(0,login.length-1) == username.substring(0,8)) { // on vient d'effacer la première lettre du prénom
			$(login_id).val(username.substring(0,7)) ;
		}
	}
	else if (mod == "user_name") {
		if (login.substring(0,username.length) == username.substring(0,7)) { // on vient de supprimer une lettre au nom
			$(login_id).val(username.substring(0,7) + firstname.charAt(0)) ;
		}
		else if (login == username.substring(0,username.length-1) + firstname.charAt(0)) { // on vient d'ajouter une lettre au nom
			$(login_id).val(username.substring(0,7) + firstname.charAt(0)) ;
		}
	}
}

/**
 * Return the cleaned value of the input found with selector
 * treatments :  to lower, wihtout spaces or quotes
 * @param {string} selector: css selector, must be uniq
 * @returns {string} the cleaned value of the input
 */
function getCleanVal(selector) {
	return removeAccent($(selector).val().toLowerCase().replace(/ /g,"").replace(/'/g,""))
}

/**
 * Manage the archive / delete / unsubscribe popup
 */
function archivePopup(nameActions){
	return {
		actionType:'archive',
		deleteLabel: '',
		deleteDetailsLabel: '',
		isDeletable: true,
		archiveLabel: '',
		archiveDetailsLabel: '',
		spreadArchive: '',
		unsubscribeLabel: '',
		unsubscribeDetailsLabel: '',
		attributes: {},
		nameActions: nameActions,
		initArchivePopup() {
			$("#archive-popup").dialog("open");
			this.actionType = 'archive';
			const params = window[this.nameActions].getParamsArchivePopup();
			this.attributes = params.attributes;
			this.deleteLabel = params.deleteLabel;
			this.deleteDetailsLabel = params.deleteDetailsLabel;
			this.archiveLabel = params.archiveLabel;
			this.archiveDetailsLabel = params.archiveDetailsLabel;
			if(params.spreadArchive) {
				this.spreadArchive = params.spreadArchive;
			}
			if(params.isDeletable !== undefined) {
				this.isDeletable = params.isDeletable;
			}
			this.unsubscribeLabel = params.unsubscribeLabel;
			this.unsubscribeDetailsLabel = params.unsubscribeDetailsLabel;
		},
		submitArchivePopup(){
			const actionSuccess = window[this.nameActions][this.actionType].call(this, this.attributes);
			if(actionSuccess!==false) {
				$("#archive-popup").dialog("close");
			}
		}
	}
}

/*******************************************
 *  Document ready
 *  jQuery.fn.DataTable.ext.type.search.string is used by the following tables (string) :
 *  - Étudiants (teacher/students)
 *  - Missions personnelles (teacher/missions)
 *  - Missions publiques (teacher/missions)
 *  - Institutions (admin)
 *  - Plateformes (admin)
 *  jQuery.fn.DataTable.ext.type.search.html is used by the following tables (raw HTML) :
 *  - Rapports (/teacher/reports/)
 *  - Classes (teacher/students)
 *  - Équipes pédagogiques (manager/teachers)
 ********************************************/

$(document).ready(function() {
	// on modern browsers, ignore accents when searching
	if (String.prototype.normalize && jQuery.fn.DataTable) {
		jQuery.fn.DataTable.ext.type.search.string = function (data) {
			return (typeof data === 'string' ? unaccent(data) : data);
		};
		jQuery.fn.DataTable.ext.type.search.html = function (data) {
			return (typeof data === 'string' ? unaccent(data) : data);
		};
	} else {
		console.log("This browser is not ES6 compliant, so no collation on search.");
	}
});
