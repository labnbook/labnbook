var addStudentsDialog = function() {
	const METHOD_CLASS = 'class';
	const METHOD_CSV = 'csv';
	const METHOD_FORM = 'form';
	const METHOD_NUMBER = 'number';
	const METHOD_CODE = 'code';

	const state = {
		method: null,
		ldap: false,
		class: null,
		htmlContents: "",
		initialized: false,
	}

	const Radios = {
		METHOD_FORM: {
			value: METHOD_FORM,
			text: __("Ajouter un unique étudiant"),
			help: __("Création d'un compte étudiant et ajout dans la classe.\nSi l'étudiant est reconnu dans la base LabNBook, il est simplement ajouté à la classe."),
		},
		METHOD_NUMBER: {
			value: METHOD_NUMBER,
			text: __("Ajouter des étudiants à partir de leurs numéros d'étudiant institutionnels"),
			help: __("Création de comptes étudiants en indiquant uniquement leurs numéros d'étudiants et ajout dans la classe.\nSi des étudiants sont reconnus dans la base LabNBook, il sont simplement ajoutés à la classe."),
		},
		METHOD_CSV: {
			value: METHOD_CSV,
			text: __("Ajouter des étudiants à partir d'un fichier CSV"),
			help: __("Création de comptes étudiants à partir des informations d'un fichier CSV et ajout dans la classe.\nSi des étudiants sont reconnus dans la base LabNBook, il sont simplement ajoutés à la classe."),
		},
		METHOD_CLASS: {
			value: METHOD_CLASS,
			text: __("Ajouter des étudiants sélectionnés dans une classe"),
			help: __("Ajout d'étudiants déjà inscrits dans une autre classe LabNBook."),
		},
		METHOD_CODE: {
			value: METHOD_CODE,
			text: __('Auto-inscription des étudiants par code'),
			help: __('Le code permet aux étudiants de s’auto-inscrire à la classe'),
		}
	}

	function reset(){
		state.htmlContents = "";
		state.ldap = false;
		$("#add-students .popup-contents").html("");
	}

	function setMethod(method) {
		state.method = method
		var id_class = $("#le-table").data('idclass');
		$.ajax({
			url: '/teacher/students/getAddStudentsView',
			method: "GET",
			data: {
				method,
				id_class
			},
			extract: xhr => xhr.responseText,
		}).then(function(html) {
			state.htmlContents = html
			$("#add-students .popup-contents").html(getView());
			switch(state.method){
				case METHOD_FORM:
					addLearnerFormFunctions('add-students-');
					initEditLearnerData('', $("#le-table").data('idclass'), false, 'add-students-');
					allowSubmit(true);
					break;
				case METHOD_NUMBER:
					$("#add-students .institution-name").text(state.class.inst_name);
					// Nobreak
				case METHOD_CSV:
				case METHOD_CLASS:
					allowSubmit(false);
					break
				case METHOD_CODE:
					allowSubmit(true);
					break
			}
		});
	}

	function allowSubmit(allow){
		let btn = $('#add-students .popup-submit');
		if(allow){
			btn.show();
		}else{
			btn.hide();
		}
	}

	function radioView(help, value, text) {
		var checked = value == state.method ? "checked" : "";
		return `
		<div>
			<label title="${__(help)}">
			<input type="radio" name="student-method" value="${value}" onclick="addStudentsDialog.setMethod('${value}')" ${checked}>
			${__(text)}
			</label>
		</div>
		`
	}

	function getView() {
		// Class view
		var html = "<div>" + __("Classe") + __("&nbsp;:") + "&nbsp;<strong>" + state.class.class_name +"</strong> ";
		html += state.class.nb_learners > 0 ? "("+state.class.nb_learners + "&nbsp;" + __("étudiants")+")" : "";
		html += "</div>";
		// Add selector Radios
		html += `<fieldset id="add-students-method"><legend>`+__("Méthode")+`</legend>`;
		for (var key in Radios) {
			var radio = Radios[key];
			if (radio['value'] == METHOD_NUMBER && !state.ldap) {
				// No numbers without ldap
				continue;
			}
			html += radioView(radio['help'], radio['value'], radio['text']);
		}
		html += "</fieldset>";
		html += state.htmlContents;
		return html;
	}

	return {
		open: function(element, classAttributes, id_user) {
			let initialized = new Promise(function(resolve) {
				resolve(state.initialized);
			});
			initialized.then(function(isInitialized) {
				if (isInitialized) {
					reset();
				}
				return $.ajax({
					url: '/classe/'+classAttributes.id_class+'/hasLdap',
					method: "GET",
				});
			}).then(function(data) {
				id_class=classAttributes.id_class;
				state.ldap = data.ldap
				state.class = classAttributes;
				$(element).html(getView());
				allowSubmit(false);
				$("#add-students").dialog("open");
				$('#add-students').dialog('option','width', 'auto');
				$('#add-students').dialog('option', 'position', { my: "left top", at: "left top+40", of: $('#students') })
				$('#add-students>header>span.title').text( __("Ajout d'étudiants") );
				setMethod(state.ldap ? METHOD_NUMBER : METHOD_FORM);
				state.initialized = true;
			});
		},
		allowSubmit: allowSubmit,
		setMethod: setMethod,
		submit: function(){
			switch(state.method){
				case METHOD_CSV:
				case METHOD_NUMBER:
					checkAndAddCSVLearners(true)
					break;
				case METHOD_FORM:
					addOrUpdateUser(0,-1, 'add-students-');
					break;
				case METHOD_CLASS:
					addSelectedStudentsToClass();
					break;
				case METHOD_CODE:
					updateClassCode();
					break
			}
		},
		close: function() {
			reset();
			$("#add-students").dialog( "close" );
		}
	};
}();
