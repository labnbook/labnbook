const simulationEditionCtrl = function(simulationCode) {
	
	function init() {
		this.loading = true;
		this.loadDataJsonMigration();
	}
	function loadDataJsonMigration() {
		let self = this;
		$.ajax({
			type: "GET",
			url: "/simulation/getDataJsonMigration",
			datatype: "json",
			data: {
				simulation_code: this.simulationCode
			},
		}).done(function (responseData) {
			self.dataMigration = JSON.parse(responseData);
			if(!self.dataMigration.taskTypes) {
				self.dataMigration.taskTypes = [];
			} else {
				for (let i = 0; i < self.dataMigration.taskTypes.length; i++) {
					self.dataMigration.taskTypes[i].position = i;
				}
			}
			if(!self.dataMigration.constraints) {
				self.dataMigration.constraints = [];
			} else {
				self.dataMigration.constraints = self.dataMigration.constraints.map(
					function(c, index) {
						c.position = index;
						c.relevanceConstraintIds = c.relevanceConstraintIds ? c.relevanceConstraintIds.split(',').map(Number) : [];
						c.simulationEnabled = c.simulationEnabled === 1;
						return c;
					}
				)
			}
			if(!self.dataMigration.globalKnowledges) {
				self.dataMigration.globalKnowledges = [];
			} else {
				for (let i = 0; i < self.dataMigration.globalKnowledges.length; i++) {
					self.dataMigration.globalKnowledges[i].position = i;
				}
			}
			self.loading = false;
		}).fail(function (jqXHR, textStatus) {
			alertGently(__("Une erreur est survenue : ") + textStatus, 'danger');
		});
	}
	
	function exportJSON () {
		let cleanData = JSON.parse(JSON.stringify(this.dataMigration));
		this.linkTTAndConstraintsAndFeedback(cleanData);
		const dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(cleanData));
		const downloadAnchorNode = document.createElement('a');
		downloadAnchorNode.setAttribute("href",     dataStr);
		downloadAnchorNode.setAttribute("download", this.simulationCode+"_migration.json");
		document.body.appendChild(downloadAnchorNode); // required for firefox
		downloadAnchorNode.click();
		downloadAnchorNode.remove();
	}
	
	function exportCSV () {
		let cleanData = JSON.parse(JSON.stringify(this.dataMigration));
		this.linkTTAndConstraintsAndFeedback(cleanData);
		const columnNames = [ 'index', 'SC', 'Sim', 'RC', 'Msg Erreur', 'Cours', 'Msg Technol',
			'Msg technique', 'Msg tâche', 'index TT', 'Description TT', 'LG', 'Description K' ];
		let csvData = [];
		csvData.push('"'+columnNames.join('";"')+'";');
		cleanData.constraints.forEach((c) => {
			let row = [];
			row.push(c.position.toString());
			row.push(c.description.replace(/"/g, '""'));
			row.push(c.simulationEnabled?'1':'0');
			row.push(c.relevanceConstraintIndexes.join(','));
			row.push(c.message.replace(/"/g, '""'));
			row.push(c.theoretical.replace(/"/g, '""'));
			const taskType = cleanData.taskTypes.find((tt) => parseInt(tt.id_task_type) === parseInt(c.id_task_type));
			if(taskType) {
				row.push(taskType.technological.replace(/"/g, '""'));
				row.push(c.technical.replace(/"/g, '""'));
				row.push(c.correction.replace(/"/g, '""'));
				row.push(taskType.position.toString());
				row.push(taskType.description.replace(/"/g, '""'));
				row.push(taskType.learningGoal);
					const globalKnowledge = cleanData.globalKnowledges.find((gk) => parseInt(gk.id_global_knowledge) === parseInt(taskType.id_global_knowledge));
					if(globalKnowledge) {
						row.push(globalKnowledge.description.replace(/"/g, '""'));
					} else {
						row.push('');
					}
			} else {
				row.push('');
				row.push(c.technical.replace(/"/g, '""'));
				row.push(c.correction.replace(/"/g, '""'));
				row.push('');
				row.push('');
				row.push('');
				row.push('');
			}
			csvData.push('"'+row.join('";"')+'";');
		});
		const dataStr = "data:text/csv;charset=utf-8," + encodeURIComponent(csvData.join('\n'));
		const downloadAnchorNode = document.createElement('a');
		downloadAnchorNode.setAttribute("href",     dataStr);
		downloadAnchorNode.setAttribute("download", this.simulationCode+"_data.csv");
		document.body.appendChild(downloadAnchorNode); // required for firefox
		downloadAnchorNode.click();
		downloadAnchorNode.remove();
	}
	
	function addNewElement(listName, idName) {
		const position = this.dataMigration[listName].length;
		const ids = [...this.dataMigration[listName].map(gK => parseInt(gK[idName]))];
		let idNewElement = ids.length === 0 ? 1 : (Math.max(...ids) + 1);
		let newElement = this.getNewElement(listName, idNewElement, position);
		this.dataMigration[listName].push(newElement);
	}
	
	function getNewElement(listName, id, position) {
		switch (listName) {
			case 'constraints':
				return {
					id_constraint: id,
					position: position,
					id_task_type:null,
					description:'',
					message:'',
					key:'',
					simulationEnabled:false,
					relevanceConstraintIds:[],
					theoretical:'',
					technical:'',
					correction:''
				};
			case 'taskTypes':
				return {
					id_task_type: id,
					position: position,
					description:'',
					learningGoal:1,
					technological:'',
					id_global_knowledge: null
				};
			case 'globalKnowledges':
				return {
					id_global_knowledge: id,
					position: position,
					description:'',
				};
		}
	}
	function removeElement(listName, elementIndex, elementId = null) {
		this.dataMigration[listName].splice(elementIndex, 1);
		if(listName === 'constraints' && elementId) {
			for (let constraint of this.dataMigration.constraints) {
				for(let i=constraint.relevanceConstraintIds.length-1; i>=0; i--) {
					if (parseInt(constraint.relevanceConstraintIds[i]) === parseInt(elementId)) {
						constraint.relevanceConstraintIds.splice(i, 1);
					}
				}
			}
		}
		this.updatePositions(this.dataMigration[listName]);
	}
	function insertElementAfter(listName, idName, position) {
		const ids = [...this.dataMigration[listName].map(el => parseInt(el[idName]))];
		let idNewElement = ids.length === 0 ? 1 : (Math.max(...ids) + 1);
		let newConstraint = this.getNewElement(listName, idNewElement, position+0.5);
		this.dataMigration.constraints.splice(position+1, 0, newConstraint);
		this.updatePositions(this.dataMigration.constraints);
	}
	
	function isRelatedTTLg0(constraint) {
		const relatedTT = this.dataMigration.taskTypes.find((tt) => parseInt(tt.id_task_type) === parseInt(constraint.id_task_type));
		return relatedTT && relatedTT.learningGoal === 0; 
	}
	
	function linkTTAndConstraintsAndFeedback(dataMigration) {
		// reconstruct relation between taskTypeIndex and id_task_type and constraintIndex and constraintIds
		for(let cFb of dataMigration.constraints) {
			cFb.taskTypeIndex = dataMigration.taskTypes
				.findIndex(tt => parseInt(tt.id_task_type) === parseInt(cFb.id_task_type));
			cFb.relevanceConstraintIndexes = cFb.relevanceConstraintIds.map(id_rc =>
				dataMigration.constraints.findIndex(c => parseInt(c.id_constraint) === parseInt(id_rc)));
		}
		for(let tt of dataMigration.taskTypes) {
			tt.globalKnowledgeIndex = dataMigration.globalKnowledges
				.findIndex(gK => parseInt(gK.id_global_knowledge) === parseInt(tt.id_global_knowledge));
		}
	}
	
	function updateMigrationData() {
		let cleanData = JSON.parse(JSON.stringify(this.dataMigration));
		this.linkTTAndConstraintsAndFeedback(cleanData);
		$.ajax({ type: "POST", url: "/simulation/updateDataMigration",
			data: {
				simulation_code: this.simulationCode,
				data_migration: JSON.stringify(cleanData)
			},
		}).fail(function(jqXHR) {
			alertGently(__('La sauvegarde automatique a échouée : ') + jqXHR.responseJSON.message, 'danger');
		});
	}
	
	function initializeSorting(domElt, prefix, listName) {
		Sortable.create(domElt, {
			animation: 150,
			draggable: '.'+prefix+'-item',
			handle: '.'+prefix+'-handle',
			group: prefix,
			onUpdate: (event) => {
				// Swap item position
				this.dataMigration[listName][event.oldDraggableIndex].position = event.newDraggableIndex - 0.5 + Number(event.oldDraggableIndex < event.newDraggableIndex);
				this.updatePositions(this.dataMigration[listName]);
			}
		});
	}
	
	function getRelatedElementsById(elementTarget, idName, listName) {
		return this.dataMigration[listName].filter(el => parseInt(el[idName]) === parseInt(elementTarget[idName]));
	}

	function goToElement(element_type, id) {
		this.selected_item = element_type;
		window.dispatchEvent(new CustomEvent('simulation-scroll-to-'+element_type, {detail:{id_elem:id}}));
	}
	
	function getElementDescription(idElementTarget, idName, listName) {
		const element = this.dataMigration[listName].find(el => parseInt(el[idName]) === parseInt(idElementTarget));
		return element?element.description:'';
	}

	function sortRelevanceIds() {
		const cFbArray = this.dataMigration.constraints;
		return function(a, b) {
			const indexA = cFbArray.findIndex(cFb => parseInt(cFb.id_constraint) === parseInt(a));
			const indexB = cFbArray.findIndex(cFb => parseInt(cFb.id_constraint) === parseInt(b));
			return indexA - indexB;
		};
	}

	function filterRelevanceIds(currentCFb) {
		const currentIndex = this.dataMigration.constraints.findIndex(cFb => parseInt(cFb.id_constraint) === parseInt(currentCFb.id_constraint));
		return this.dataMigration.constraints
			.filter( (cFb, index) => index < currentIndex && !currentCFb.relevanceConstraintIds.some(rcId => parseInt(rcId)===parseInt(cFb.id_constraint)));
	}

	function updatePositions (list) {
		list.sort((a, b) => a.position - b.position);
		for (let i = 0; i < list.length; i++) {
			list[i].position = i;
		}
	}
	
	return {
		init,
		loadDataJsonMigration,
		updateMigrationData,
		exportJSON,
		exportCSV,
		linkTTAndConstraintsAndFeedback,
		initializeSorting,
		addNewElement,
		getNewElement,
		getRelatedElementsById,
		goToElement,
		getElementDescription,
		insertElementAfter,
		removeElement,
		isRelatedTTLg0,
		sortRelevanceIds,
		filterRelevanceIds,
		updatePositions,
		simulationCode: simulationCode
	}
}
