var missionActions = {
	add: function () {
		$("#modal-mission-add").dialog("open");
	},
	archive: function (mission) {
		if (!mission.id_mission) {
			return;
		}
		if (mission.teacher_type === 'designer') {
			$.ajax({
				type: "POST",
				url: "/teacher/mission/"+mission.id_mission+"/archive"
			})
				.done(function (newStatus) {
					mission.status = newStatus;
					$("#my-missions").DataTable().ajax.reload(function () {
						$("#my-missions tr#mission-" + mission.id_mission).click();
					}, false);
				})
				.fail(function () {
					alert("L'archivage de la mission a échoué.");
				});
		} else {
			// TODO display the designers names
			alert("Vous êtes tuteur sur cette mission. Veuillez contacter un concepteur pour qu'il change le statut 'archive'.");
		}
	},
	correct: function(idMission, mission) {
		let newWindow = window.open('', '_blank');
		$.ajax({
				type: "POST",
				url: "/teacher/mission/"+idMission+"/correct"
			})
			.done(function (data) {
				newWindow.location = '/report/'+data.id_report;
			})
			.fail(function () {
				
			});
	},
	delete: function (mission) {
		if (!mission.id_mission) {
			return;
		}
		if(confirm(__('ATTENTION : La suppression est irréversible'))) {
			$.ajax({
				type: "POST",
				url: "/teacher/mission/"+mission.id_mission+"/delete",
			})
			.done(function(data){
				$("#my-missions").DataTable().ajax.reload(null,false);
			});
			return true;
		} else {
			return false;
		}
	},
	unsubscribe: function(mission) {
		if (!mission.id_mission) {
			return;
		}
		$.ajax({
			type: "POST",
			url: "/teacher/mission/"+mission.id_mission+"/unsubscribe",
		})
		.done(function(data){
			$("#my-missions").DataTable().ajax.reload(null,false);
		})
		.fail(function(responseData){
			if(responseData.responseJSON && responseData.responseJSON.message) {
				alertGently(responseData.responseJSON.message, 'error');
			}
		});
	},
	/**
	 * Duplicate a mission and open the clone in a new tab, in edition mode.
	 *
	 * @param {int} idMission
	 * @return {undefined}
	 */
	duplicate: function(idMission) {
		if (!idMission) {
			return;
		}
		if (!confirm(__("Voulez-vous dupliquer cette mission ?"))) {
			return;
		}
		var actions = $('.lba-context-icons').not('.actions-inactive');
		var buttons = actions.find('.fa-files-o');
		actions.addClass('actions-inactive');
		buttons.removeClass('fa-files-o');
		buttons.addClass('fa-spinner fa-pulse');
		$.ajax({
			type: "POST",
			url: "/teacher/mission/"+idMission+"/duplicate"
		})
		.done(function(newIdMission) {
			missionActions.edit(newIdMission);
		}).error(function() {
			actions.removeClass('actions-inactive');
			buttons.addClass('fa-files-o');
			buttons.removeClass('fa-spinner fa-pulse');
			alertGently(__("Echec de la duplication"));
		});
	},
	/**
	 * Open a new tab with the edit page for this mission.
	 *
	 * @param {int} idMission
	 * @return {undefined}
	 */
	edit: function(idMission) {
		if (!idMission) {
			return;
		}
		window.location = "/teacher/mission/" + idMission;
	},
	/**
	 * Open a new tab with the test page for this mission.
	 *
	 * @param {int} idMission
	 * @return {undefined}
	 */
	test: function(idMission) {
		if (!idMission) {
			return;
		}
		openTestReport(idMission);
	},
	getParamsArchivePopup: function (){
		const attributes = $('#private-missions-actions .lba-context-icons').data().params.mission;
		let deleteLabel = '';
		let deleteDetailsLabel = '';
		let archiveLabel = '';
		let archiveDetailsLabel = '';
		if (attributes.teacher_type === 'designer') {
			if(attributes.nbreports===0) {
				deleteLabel = __('Supprimer la mission "{{mission}}"', { mission: attributes.code });
				deleteDetailsLabel =  __("ATTENTION : cette action est définitive") + " "
					+ (attributes.nbreports > 0 ? __("et entrainera la suppression de {{nb_reports}} rapport(s) d'étudiants.", { nb_reports: attributes.nbreports }) : '');
			} else {
				deleteLabel = __('La mission "{{mission}}" ne peut pas être supprimée car des rapports lui sont rattachés.', { mission: attributes.code });
				deleteDetailsLabel = __('Dans l’onglet "Rapports", supprimez tous les rapports débutés (en cours, rendus ou archivés) de la mission pour pouvoir la supprimer.');
			}
			archiveLabel = attributes.status==='archive'?
				__('Désarchiver la mission "{{mission}}"', { mission: attributes.code })
				:__('Archiver la mission "{{mission}}"', { mission: attributes.code });
			archiveDetailsLabel = attributes.status==='archive'?'':__('Cette mission pourra être retrouvée en cochant la case "afficher les missions archivées" du tableau', { mission: attributes.code });
		}
		let unsubscribeLabel = '';
		let unsubscribeDetailsLabel = '';
		if(attributes.teacher_type === 'teacher' || attributes.teachers.includes(__('concepteur')) ) {
			unsubscribeLabel = __('Se désinscrire de la mission "{{mission}}"', { mission: attributes.code });
			if(attributes.teacher_type === 'teacher') {
				unsubscribeDetailsLabel = __('Seuls les concepteurs de la mission peuvent l’archiver ou la supprimer');
			}
		}
		return {
			attributes: attributes,
			isDeletable: (attributes.nbreports===0),
			deleteLabel: deleteLabel,
			deleteDetailsLabel: deleteDetailsLabel,
			archiveLabel: archiveLabel,
			archiveDetailsLabel: archiveDetailsLabel,
			unsubscribeLabel: unsubscribeLabel,
			unsubscribeDetailsLabel: unsubscribeDetailsLabel
		};
	}
};

$(document).ready(function() {
	"use strict";

	initDataTables();

	// enable the mission-add popup
	defineModal("modal-mission-add", "", true, "private-missions-actions");
	defineModal("archive-popup", null, false, "private-missions-actions");

	$(".popup").on("click", ".popup-cancel", function(event) {
		event.stopPropagation();
		var modal = $(this).closest(".popup");
		modal.dialog("close");
		if (modal.data('reset-input')) {
			$("form", modal)[0].reset();
		}
	});
	$("#modal-mission-add").on("click", ".popup-submit", function(event) {
		event.stopPropagation();
		addMission(
			$(this).closest(".popup"),
			$("#mi-add-code").val().trim(),
			$("#mi-add-name").val().trim()
		);
	});

	$('.table-missions tbody').on("click", "tr", function() {
		var tr = $(this);
		if (tr.hasClass('description-row')) {
			return;
		}
		var htmlTable = tr.closest('table.dataTable');
		var actionsBlock = $(this).closest('section').find(".lba-context-icons");
		var data = htmlTable.DataTable().row(tr).data();
		$(".description-row", htmlTable).remove();
		if (tr.hasClass('selected')) {
			tr.removeClass('selected');
			actionsBlock.addClass('actions-inactive');
			actionsBlock.data('params', {});
		} else if (data) {
			$("tr.selected", htmlTable).removeClass("selected");
			tr.addClass('selected');

			var descriptionContent = data.description || "";
			if (data.teachers) {
				descriptionContent += '<p class="mission-list-teacher"><strong>' + __("Enseignants associés") + __("&nbsp;:") + '</strong> ' + data.teachers + '</p>';
			}
			if (data.statusHtml) {
				descriptionContent += '<p class="mission-diffusion"><strong>' + __("Visibilité") + __("&nbsp;:") + '</strong> ' + data.statusHtml + '</p>';
			}

			var descriptionCell = $('<td colspan="' + tr.children('td').length + '" class="tinyMCE-text"></td>').html(descriptionContent);
			var extraRow = tr.after($('<tr class="description-row"></tr>').append(descriptionCell)).next();
			if (data.teachertype) {
				updateActionsByRole(actionsBlock, data.teacher_type);
			}
			actionsBlock.data('params', {idMission: data.id_mission, mission: data});
			actionsBlock.removeClass('actions-inactive');
			typesetHtmlWithFormulas(".description-row");
		}
	});
	$(".actions").on("click", "a[data-action]", function(event) {
		event.stopPropagation();
		var params = $(this).closest(".lba-context-icons").data('params');

		// Block action on archived item if archive items is not display
		if(!$("#display-archives").prop('checked') && params && params.mission.status =="archive"){
			return false;
		}
		
		var action = $(this).attr('data-action');
		if(action === 'archive') {
			window.dispatchEvent(new CustomEvent('display-archive-popup'));
		}else if (missionActions[action]) {
			if (params) {
				missionActions[action].call(null, params.idMission, params.mission);
			} else {
				missionActions[action].call(null);
			}
		} else {
			console.error("unknown action: ", action, ", parameters: ", params);
		}
		return false;
	});
	$(".search-input").on('input', function(){
		var dataTable = $(this).closest("section").find("table.dataTable").DataTable();
		if (dataTable) {
			dataTable.search(unaccent($(this).val())).draw();
		}
	});
	$(".pagination-size").on('change', function(){
		var dataTable = $(this).closest("section").find("table.dataTable").DataTable();
		if (dataTable) {
			dataTable.page.len($(this).val()).draw();
		}
	});
	$("#display-archives").on('change', function () {
		var dataTable = $("#my-missions").DataTable();
		if (dataTable) {
			dataTable.draw(true);
		}
	});

	function initDataTables() {
		$.fn.dataTable.ext.order.intl();
		$.fn.dataTable.ext.search.push(
			function (settings, viewData, rowIndex, rawData) {
				if (settings.sTableId !== "my-missions") {
					return true;
				}
				var displayArchives = $("#display-archives").is(":checked");
				return (displayArchives || rawData.status !== 'archive');
			}
		);
		$.extend(true, $.fn.dataTable.defaults, {
			dom: "rtp", // https://datatables.net/reference/option/dom
			autoWidth: false,
			paging: true,
			pageLength: 15,
			lengthMenu: [15, 50, -1],
			conditionalPaging: true,
			order: [[4, 'desc']], // column 4: Modif.
			language: { url: "/libraries/datatables/locale/" + window.labnbook.lang + ".json" }
		});

		var italicsArchives = function(value, cellType, row) {
			var escapedValue = $('<span>').text(value).html();
			var classAttr = (row.status === 'archive' ? ' class="status-archive"' : '');
			return (cellType != 'display') ? escapedValue : '<span' + classAttr + '>' + escapedValue + '</span>';
		};
		let privateConfig = {
			ajax: "/teacher/missions/getMine",
			columns: [
				{
					data: "code",
					render: italicsArchives
				},
				{
					data: "name",
					render: italicsArchives
				},
				{ data: "teachertype" },
				{
					data: {
						_: "creation_date_fr",
						sort: "creation_date"
					}
				},
				{
					data: {
						_: "modif_date_fr",
						sort: "modif_date"
					}
				},
				{ data: "nbreports", filter: "description", orderDataType: "num" }
			],
			rowId: function (d) { return "mission-" + d.id_mission; }
		};
		let privateMissions = $('#my-missions').DataTable(privateConfig);
		privateMissions.on('search.dt', function() {
			$('#num-visible-missions-private').text(privateMissions.rows({search: 'applied'}).count());
		} );

		let publicConfig = {
			ajax: "/teacher/missions/getPublic",
			columns: [
				{ data: "code" },
				{ data: "name", render: $.fn.dataTable.render.text() },
				{
					data: {
						_: "creation_date_fr",
						sort: "creation_date"
					}
				},
				{
					data: {
						_: "modif_date_fr",
						sort: "modif_date"
					}
				},
				{ data: "nbreports", filter: "description", orderDataType: "num" }
			],
			rowId: function (d) { return "mission-" + d.id_mission; }
		};
		let publicMissions = $('#public-missions table').DataTable(publicConfig);
		publicMissions.on('search.dt', function() {
			$('#num-visible-missions').text(publicMissions.rows({search: 'applied'}).count());
		} );
	}

	/**
	 * fonction addMission
	 * ajoute une mission en fonction des données des champs de la popup d'ajout.
	 * ferme la popup si ajout validé
	 */
	function addMission(popup, code, name) {
		$.ajax({
			type: "POST",
			url: "/teacher/missions/create",
			data: {
				code: code,
				name: name
			}
		}).fail(function(jqXHR) {
			let msg = decodeURIComponent(JSON.parse(jqXHR.responseText));
			if (jqXHR.status === 409) { // conflict
				alertGently( __("Echec lors de la création de la mission : {{task}}", { task : msg} ) );
			} else {
				alertGently(msg);
			}
		}).done(function(data) {
			console.log("New mission ID", data);
			popup.dialog("close");
			missionActions.edit(parseInt(data));
		});
	}

	/**
	 * Update the action links.
	 *
	 * @param {Element} actionsBlock
	 * @param {String} role
	 * @return {undefined}
	 */
	function updateActionsByRole(actionsBlock, role) {
		if (role === 'teacher') {
			$("[data-action=delete]", actionsBlock).attr("title", __("Se désinscrire de la mission"));
			$("[data-action=edit]", actionsBlock).hide(0);
		} else {
			$("[data-action=delete]", actionsBlock).attr("title", __("Supprimer la mission"));
			$("[data-action=edit]", actionsBlock).show(0);
		}
	}
});
