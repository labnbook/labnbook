/* global Blob */

// Controller for evaluation
const evaluationsCtrl = function () {

	function init (grades) {
		this.grades = grades;
	}

	function tableToCSV () {
		// Inspired from https://www.geeksforgeeks.org/how-to-export-html-table-to-csv-using-javascript/

		let csv_data = [];
		const rows = document.getElementById('scoring_eval').getElementsByTagName('tr')

		for (let i = 0; i < rows.length; i++) {
			const cols_number = rows[0].querySelectorAll('td:not([style="display: none;"]),th:not([style="display: none;"])').length;
			const cols = rows[i].querySelectorAll('td:not([style="display: none;"]),th:not([style="display: none;"])');

			let csvrow = [];
			for (let j = 0; j < cols.length; j++) {
				// Two cases : either we have an input and we look if its checked
				// or we want the text
				const inputs = cols[j].getElementsByTagName('input');
				let val = cols[j].innerText.replace(' %', '').replace('+', '');
				if (cols[j].getAttribute('data-unit') !== null) {
					val += ' (' + cols[j].getAttribute('data-unit') + ')';
				}
				if (inputs.length > 0) {
					val = inputs[0].value;
				}
				csvrow.push('"' + val + '"');
			}
			let delta = cols_number - cols.length;
			for (let i = 1; i <= delta; i++) {
				csvrow = ['""', ...csvrow];
			}

			csv_data.push(csvrow.join(';'));
		}
		csv_data = csv_data.join('\n');
		return csv_data;
	}

	function downloadCSVFile (csv_data) {
		// From https://www.geeksforgeeks.org/how-to-export-html-table-to-csv-using-javascript/

		const CSVFile = new Blob([new Uint8Array([0xEF, 0xBB, 0xBF]),csv_data], { type: 'text/csv;charset=utf-8' });

		const temp_link = document.createElement('a');

		// Download csv file
		temp_link.download = 'notes.csv';
		const url = window.URL.createObjectURL(CSVFile);
		temp_link.href = url;

		// This link should not be displayed
		temp_link.style.display = 'none';
		document.body.appendChild(temp_link);

		// Automatically click the link to trigger download
		temp_link.click();
		document.body.removeChild(temp_link);
	}

	return {
		init,
		format: function (number, unit = null, round = false) {
			if (number !== null) {
				if (isNaN(parseFloat(number))) {
					return number;
				}
				if (round) {
					number = Math.round(number);
				}
				const base = number.toLocaleString(window.langLong().split('_')[0], { maximumFractionDigits: 2 });
				return unit === null ? base : base + ' ' + unit;
			}
			return '';
		},
		publishAll: async function () {
			const ids = this.grades.grades.map(e => e.id_production);
			let unpublished = 0;
			const unfinished = {};
			this.grades.grades.forEach(function (g) {
				if (!g.published) {
					unpublished ++;
					Object.keys(g.criteria).forEach(function (k) {
						if (!g.criteria[k].graded) {
							unfinished[g.id_assessment] = g.team;
						}
					});
				}
			});
			const unfinished_keys = Object.keys(unfinished)
			if (unpublished === 0 ) {
				return;
			}
			if (!confirm(__('Souhaitez vous rendre leur(s) évaluation(s) à {{count}} étudiant(s) ?', {count:unpublished}))) {
				return false;
			}
			if (unfinished_keys.length != 0) {
				let unfinished_names = [];
				unfinished_keys.forEach(function (k) {
					if (!unfinished_names.includes(unfinished[k])) {
						unfinished_names.push(unfinished[k]);
					}
				});
				unfinished_names = unfinished_names.join(', ');

				if (!confirm(__("Attention : des critères avec descripteurs ne sont pas évalués pour les équipes {{names}}. Souhaitez vous rendre les évaluations malgré tout ?", {names: unfinished_names}))) {
					return false;
				}
			}
			return await fetch('/scoring/assessments/publishBatch', {
				method: 'POST',
				headers: {
					'X-CSRF-TOKEN': document.querySelector('meta[name=csrf-token]').content,
					'Content-Type': 'application/json',
					Accept: 'application/json'
				},
				body: JSON.stringify({
					productions: ids
				})
			});
		},
		getCSV: function () {
			downloadCSVFile(tableToCSV());
		},
		valueClass: function (number) {
			const val = parseFloat(number);
			const multiple = 25;
			console.log('Value class : ' + number + ' value ' + val);
			if (isNaN(val)) {
				return '';
			}
			return 'bg_' + multiple * Math.round(val / multiple);
		}
	}
}();
