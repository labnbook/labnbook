window.rubricCtrl = function () {
    /**
	 * This function is called automatically by Alpine when the root component is initialized.
     * It retrieves the rubric associated with the mission and initializes score calculation and display.
     * @returns {Promise<void>}
     */
    async function init () {
        this.rubric = await this.getRubric(window.global_id_mission);
        this.dataLoaded = true;
        if (Object.keys(this.rubric).length !== 0) {
            this.computeAllWeightsAndMaxScores();
            this.setViewConfig();
			this.checkAll();
        }
    }

	/**
	 * Retrieves a rubric for the given mission.
	 * @param {int} id : the current mission id
	 * @return {obj} the rubric or an empty object
	 */
	async function getRubric (id) {
		const response = await fetch(
			'/scoring/rubric/' + id,
			{
				headers: {
					Accept: 'application/json'
				}
			}
		);
		if (response.status === 200) {
			const ret = await response.json();
			ret.initialized = true;
			ret.hasChanged = false;
			ret.preventSavingOnDrag = false;
			return ret;
		}
		return {};
	}

	/**
	 * Retrieves the missions related to the connected user.
	 * @return {array} a nested array (missions grouped by their status)
	 */
	async function getActivities () {
		let activities = await (await fetch('/scoring/rubric/getMyLinkedActivities')).json();
		let categories = {
			notArchived: { order: 1, title: __('Rapports'), items: [] },
			archived: { order: 2, title: __('Rapports archivés'), items: [] }
		};
		Object.values(activities).forEach(a => {
			if (['public', 'private', 'tutorial'].includes(a.status)) {
					categories.notArchived.items.push(a);
			} else {
				categories.archived.items.push(a);
			}
		});
		Object.keys(categories).forEach(k => { categories[k].items.sort((a, b) => a.code.localeCompare(b.code)); });
		categories = Object.values(categories).sort((a, b) => a.order - b.order);
		return categories.map(cat => {
			return {
				title: cat.title,
				items: cat.items.map(a => {
					return { value: a.id_mission, label: a.code }
				})
			}
		}).filter(cat => cat.items.length >0);
	}

	/**
	 * Computes the view configuration (mainly the columns number for the rubric)
	 */
	function setViewConfig() {
		if (this.rubric === null) {
			return;
		}
		const gcd = (a, b) => a ? gcd(b % a, a) : b;
		const lcm = (a, b) => a * b / gcd(a, b);
		let descrobsLCM;
		if (this.rubric.criteria.length === 0) {
			descrobsLCM = 1;
		} else {
			descrobsLCM = this.rubric.criteria.map((elt) => elt.descrobs.length || 1).reduce(lcm);
		}

		this.viewConfig.columnsNumber = descrobsLCM + 3;
		this.viewConfig.descrobColumnPercentWidth = (
			this.viewConfig.tablePercentWidth - this.viewConfig.criterionTitlePercentWidth - this.viewConfig.criterionScorePercentWidth - this.viewConfig.criterionAddDescrobWidth
		) / (this.viewConfig.columnsNumber - 3);
	}

	/**
	 * Saves the rubric.
	 */
	async function saveRubric () {
		this.checkAll();
		if (this.formInvalid) {
			alertGently(__('La grille critériée ne peut pas être sauvegardée !'), 'danger');
		} else {
			const response = await this.postRubric();
			const data = await response.json();
			if (response.ok) {
				data.rubric.initialized = true;
				data.rubric.hasChanged = false;
				data.rubric.preventSavingOnDrag = false;
				this.rubric = data.rubric;
				this.emptyDeletes();
				alertGently(__('Grille critériée sauvegardée'), 'success');
				this.setRubricEdition(false);
			} else {
				alertGently(data.message);
				this.setRubricEdition(true);
			}
		}
	}

	/**
	 * Sends a post request to save the current rubric.
	 * @return {Promise<Response>}
	 */
	async function postRubric () {
		const rubricToBeSaved = this.rubric;
		const elementsToBeDeleted = this.deletes;
		this.rubric.description = removeEquationsDisplays(this.rubric.description);
		return await fetchWitoutDuplicates('/scoring/rubric/' + window.global_id_mission, {
			method: 'POST',
			headers: {
				'X-CSRF-TOKEN': document.querySelector('meta[name=csrf-token]').content,
				'Content-Type': 'application/json',
				Accept: 'application/json'
			},
			body: JSON.stringify({
				propagate: true,
				rubric: rubricToBeSaved,
				deletes: elementsToBeDeleted
			})
		});
	}

	/**
	 * This function is triggered by an event dispatched periodically in mission_edit.js
	 * It aims to save the rubric if it is valid and edited
	 * @param event
	 * @return {Promise<void>}
	 */
	async function rubricAutoSave (event) {
		if (event.detail === 'save-rubric' && this.rubricEdition && this.rubric.hasChanged && !this.rubric.preventSavingOnDrag) {
			const rubricDomElement = document.getElementById("rubric");
			const activeElement = document.activeElement;
			// Do not save while user is editing a field
			if (['INPUT', 'TEXTAREA'].includes(activeElement.tagName) && rubricDomElement.contains(activeElement)) {
				return;
			}
			this.checkAll();
			if (!this.formInvalid) {
				const response = await this.postRubric();
				const data = await response.json();
				if (response.ok) {
					console.log('Rubric auto save');
					data.rubric.initialized = true;
					data.rubric.hasChanged = false;
					data.rubric.preventSavingOnDrag = false;
					this.rubric = data.rubric;
					this.emptyDeletes();
					this.checkAll();
					this.computeAllWeightsAndMaxScores();
					alertGently(__('Grille critériée sauvegardée'), 'success');
				} else {
					alertGently(data.message);
				}
			}
		}
	}

	/**
	 * Creates a new rubric with one default group.
	 */
	function createRubric () {
		this.clearRubric();
		this.rubric.initialized = true;
		this.editRubric();
		this.setViewConfig();
		this.checkAll();
	}

	/**
	 * Duplicates a rubric from a rubric previously fetched.
	 * @param {obj} rubricSource
	 * @return {obj} Alpine rubric object, a duplicate of rubricSource
	 */
	function duplicateRubric (rubricSource) {
        // Cache id if rubric is already existing
		let cachedIdRubric = this.rubric.id_rubric ?? null;
		this.clearRubric(cachedIdRubric != null);
		rubricSource.assessments = [];
		this.rubric = rubricSource;
		this.setRubricEdition(true);
		this.rubric.id_activity = window.global_id_mission;
		this.rubric.id_rubric = cachedIdRubric;
		// Generate fake id on all objects to force duplication
		this.rubric.criteria_group.forEach((g) => {
			const new_id = this.uuid();
			this.getCriteriaFromGroup(g).forEach((c) => {
				c.id_criteria_group = new_id;
				c.id_rubric = cachedIdRubric;
				c.id_criterion = this.uuid()
				c.descrobs.forEach((d) => {
					d.id_criterion = c.id_criterion;
					d.id_descrob = this.uuid();
				});
			});
			g.id_criteria_group = new_id;
			g.id_rubric = cachedIdRubric;
		});
		this.setViewConfig();
		this.checkAll();
		this.computeAllWeightsAndMaxScores();
	}

	/**
	 * Deletes the rubric.
	 */
	async function deleteRubric () {
		if (this.rubric.assessments.length > 0 ) {
			alertGently(__('Impossible de supprimer la grille critériée : il existe déjà des évaluations'));
			return;
		}
		const successMessage = __('Grille supprimée');
		if (confirm(__('Voulez vous supprimer la grille ?\nCette action est irréversible'))) {
			if (typeof (this.rubric.id_rubric) !== 'undefined' && !isNaN(parseInt(this.rubric.id_rubric))) {
				let response = await fetch('/scoring/rubric/' + this.rubric.id_rubric, {
					method: 'DELETE',
					headers: {
						'X-CSRF-TOKEN': document.querySelector('meta[name=csrf-token]').content,
						'Content-Type': 'application/json',
						'Accept': 'application/json'
					}
				});
				if (response.status === 200) {
					this.clearRubric();
					this.setRubricEdition(false);
					alertGently(successMessage, 'success');
				} else {
					const data = await response.json();
					alertGently(data.message);
				}
			} else {
				this.clearRubric();
				this.setRubricEdition(false);
				alertGently(successMessage, 'success');
			}
		}
	}

	/**
	 * Restores default values for all the attributes of the rubric object.
	 * @params {boolean} keepDeletes : whether to keep the deletes array or not
	 */
	function clearRubric (keepDeletes=false) {
		this.rubric.assessments = [];
		this.rubric.criteria_group = [];
		this.rubric.criteria = [];
		this.rubric.grading = null;
		this.rubric.id_activity = window.global_id_mission;
		this.rubric.id_rubric = null;
		this.rubric.initialized = false;
		this.rubric.hasChanged = false;
		this.rubric.preventSavingOnDrag = false;
		this.rubric.max_bonus = 0;
		this.rubric.max_grade = null;
		this.rubric.min_bonus = 0;
		this.rubric.min_grade = 0;
		this.rubric.name = __("Évaluation");
		this.rubric.description = null;
		this.rubric.total_weight = null;
		this.rubric.empty = null;
		this.rubric.nameInvalidText = null;
		this.rubric.minBonusInvalidValue = null;
		this.rubric.maxBonusInvalidValue = null;
		this.rubric.gradingInvalidValue = null;
		this.rubric.errors = [];
		this.rubric.errorsHtml = null;
		if (!keepDeletes) {
			this.emptyDeletes();
        }
	}

	/**
	 * Clears all arrays of items to be deleted. 
	 */
	function emptyDeletes () {
		this.deletes.criteria_group.splice(0, this.deletes.criteria_group.length);
		this.deletes.criteria.splice(0, this.deletes.criteria.length);
		this.deletes.descrobs.splice(0, this.deletes.descrobs.length);
	}

	/**
	 * Starts edition mode.
	 */
	function editRubric () {
		this.setRubricEdition(true);
		this.computeAllWeightsAndMaxScores();
	}

	/**
	 * Sets the edition mode to true or false.
	 * Copies its value to a global variable.
	 * @param {boolean} toBeSet
	 */
	function setRubricEdition (toBeSet) {
		this.rubricEdition = toBeSet;
		window.rubricEdition = toBeSet;
	}

	/**
	 * Adds a new descrob to a given criterion.
	 * @param {obj|null} descrob : the descrob preceeding the new one
	 * @param {obj} criterion : criterion in which the descrob is appened
	 * @param {string|null} description : an optional description for the descrob
	 * @param {number|string|null} value : an optional weight for the descrob
	 * @returns {string|null|*} the descrob id
	 */
	function addDescrOb (descrob, criterion, description=null, value=null) {
		const maxPosition = (criterion.descrobs.length > 0) ? Math.max(...criterion.descrobs.map(d => d.position)) : 0;
		const idDescrob = this.uuid();
		let position = descrob != null ? parseInt(descrob.position) + 0.5 : maxPosition + 0.5;
		let newDescrob = {
			id_descrob: idDescrob,
			id_criterion: criterion.id_criterion,
			position: position,
			description: description,
			value: value
		};
		criterion.descrobs.push(newDescrob);
		criterion.descrobs.sort((a, b) => a.position - b.position);
		this.updatePositions(criterion.descrobs);
		// Overwrites value if criterion is not weighted
		if (criterion.relative_weight === 0) {
			newDescrob.value = (criterion.type === 'descr') ? newDescrob.position + 1 : 1;
		}
		this.setViewConfig();
		this.check('invalidText', 'descrob', newDescrob);
		this.check('invalidValue', 'descrob', newDescrob);
		this.check('empty', 'criterion', criterion);
		this.computeAllWeightsAndMaxScores();
		return idDescrob;
	}

	/**
	 * Adds a new criterion to a given group.
	 * @param {obj} criterion : the criterion preceeding the new one
	 * @param {obj} group : group in which the descrob is appened
	 * @param {boolean} duplicate : whether to duplicate or create an empty criterion
	 * @returns {string|null|*} the criterion id
	 */
	function addCriterion (criterion, group, duplicate=false) {
		let groupCriteria = this.getCriteriaFromGroup(group);
		const maxPosition = (groupCriteria.length > 0) ? Math.max(...groupCriteria.map(c => c.position)) : 0;
		// Create a new criteria
		const idCriterion = this.uuid();
		const position = criterion != null ? parseInt(criterion.position) + 0.5 : maxPosition + 0.5;
		const newCriterion = {
			id_criterion: idCriterion,
			id_rubric: this.rubric.id_rubric,
			id_section: '',
			id_criteria_group: group.id_criteria_group,
			type: duplicate ? criterion.type : 'descr',
			position: position,
			relative_weight: duplicate ? criterion.relative_weight : null,
			description: duplicate ? criterion.description : null,
			descrobs: []
		};

		this.rubric.criteria.push(newCriterion);
		// Update criteria positions (reload groupCriteria is mandatory)
		groupCriteria = this.getCriteriaFromGroup(group);
		this.updatePositions(groupCriteria);
		// Add descriptors
		if (duplicate) {
			criterion.descrobs.forEach((d, i)=> {
				this.addDescrOb(
					newCriterion.descrobs[i - 1],
					newCriterion,
					criterion.descrobs[i].description,
					criterion.descrobs[i].value);
			})
		}
		this.updatePositions(newCriterion.descrobs);
		this.check('invalidValue', 'criterion', newCriterion);
		this.check('invalidText', 'criterion', newCriterion);
		this.check('empty', 'criterion', newCriterion);
		this.check('empty', 'criteria_group', group);
		this.computeAllWeightsAndMaxScores();
		return idCriterion;
	}

	/**
	 * Add a new group to a given rubric.
	 * @param {obj} group : the group preceeding the new one
	 * @returns {string|null|*} the group id
	 */
	function addGroup (group) {
		const maxPosition = (this.rubric.criteria_group.length > 0) ? Math.max(...this.rubric.criteria_group.map(d => d.position)) : 0;
		const idGroup = this.uuid();
		const position = group != null ? parseInt(group.position) + 0.5 : maxPosition + 0.5;
		const newGroup = {
			id_criteria_group: idGroup,
			id_rubric: this.rubric.id_rubric,
			position: position,
			description: null,
			relative_weight: null,
			criteria: []
		}
		this.rubric.criteria_group.push(newGroup);
		this.rubric.criteria_group.sort((a, b) => a.position - b.position);
		this.updatePositions(this.rubric.criteria_group);
		this.check('invalidText', 'criteria_group', newGroup);
		this.check('invalidValue', 'criteria_group', newGroup);
		this.check('empty', 'criteria_group', newGroup);
		this.check('empty', 'rubric', this.rubric);
		this.computeAllWeightsAndMaxScores();
		return idGroup;
	}

	/**
	 * Toggles the given criterion type.
	 * @param {obj} criterion
	 */
	function toggleCriterionType (criterion) {
		criterion.type = criterion.type === 'ob' ? 'descr' : 'ob';
		criterion.descrobs.forEach(d => { this.check('invalidValue', 'descrob', d);	});
	}

	/**
	 * Deletes a descrob.
	 * @param {obj} descrob
	 */
	function deleteDescrOb (descrob) {
		const criterion = this.findCriterion(descrob.id_criterion);
		this.markForDeletion(descrob.id_descrob, 'descrobs');
		criterion.descrobs = criterion.descrobs.filter(e => !this.haveTheSameId(e.id_descrob, descrob.id_descrob));
		this.updatePositions(criterion.descrobs);
		this.setViewConfig();
		this.checkAll();
		this.computeAllWeightsAndMaxScores();
	}

	/**
	 * Deletes a criterion.
	 * @param {obj} criterion
	 */
	function deleteCriterion (criterion) {
		this.markForDeletion(criterion.id_criterion, 'criteria');
		this.rubric.criteria = this.rubric.criteria.filter(e => !this.haveTheSameId(e.id_criterion, criterion.id_criterion));
		this.updatePositions(this.getCriteriaFromGroup(this.findGroup(criterion.id_criteria_group)));
		this.checkAll();
		this.computeAllWeightsAndMaxScores();
	}

	/**
	 * Deletes a criteria_group
	 * @param {obj} group
	 */
	function deleteGroup (group) {
		if (confirm(__('Voulez vous supprimer le groupe et tous ses critères ?\nCette action est irréversible'))) {
			this.markForDeletion(group.id_criteria_group, 'criteria_group');
			this.getCriteriaFromGroup(group).forEach((c) => { this.deleteCriterion(c) });
			this.rubric.criteria_group = this.rubric.criteria_group.filter(e => !this.haveTheSameId(e.id_criteria_group, group.id_criteria_group));
			this.updatePositions(this.rubric.criteria_group);
			this.checkAll();
			this.computeAllWeightsAndMaxScores();
		}
	}

	/**
	 * Marks an object as deleted in order to really erase it when saving.
	 * @param {string} id of the object
	 * @param {string} objectType : criteria|criteria_group|descrobs
	 */
	function markForDeletion (id, objectType) {
		if (!this.isFakeId(id)) {
			this.deletes[objectType].push(id);
		}
	}

	/**
	 * Displays a value
	 * @param {string|number|null|*} value to be displayed
	 * @param {boolean} forcePlus : whether to force a '+' before the value when it's positive
	 * @returns {string|null|*}
	 */
	function displayValue(value, forcePlus=false) {
		if (value != null) {
			value = parseFloat(value.toString());
			if (forcePlus && !isNaN(value) && value >= 0) {
				return '+' + this.format(value);
			} else {
				return this.format(value);
			}
		} else {
			value = '';
		}
		return value;
	}

	/**
	 * Displays a weight or a computed weight for a given object.
	 * @param {obj} object : a criterion or a group
	 * @returns {string}
	 */
	function displayValueOrComputedValue (object) {
		let auto = '';
		if (object.relative_weight == null || object.relative_weight === '') {
			auto = ' (auto)'
		}
		// If the computed relative weight overwrites the relative weight, do not overwrite
		let valueToDisplay;
		if (object.relative_weight !== 0 && object.computed_relative_weight === 0) {
			valueToDisplay = object.relative_weight;
		} else {
			valueToDisplay = object.computed_relative_weight;
		}
		return this.displayValue(valueToDisplay) + auto;
	}

	/**
	 * Formats a decimal, rounding to the indicated number of digits.
	 * @param {string|number|null|*} number : number to be formatted
	 * @param digits
	 * @returns {string}
	 */
	function format(number, digits=2) {
		if (typeof number === 'undefined' || isNaN(number) || number === null) {
			return '';
		}
		return number.toLocaleString(window.langLong().split('_')[0], { maximumFractionDigits: digits });
	}

	/**
	 * Retrieves the numerical value of a string.
	 * @param {string|number|null|*}  value
	 * @returns {number}
	 */
	function getNumericValue (value) {
		const cleanStringValue = value.toString().replace(',', '.').replace('+', '');
		if (isNaN(Number(cleanStringValue))) {
			return NaN;
		} else {
			return Math.round(100 * parseFloat(cleanStringValue)) / 100;
		}
	}

	/**
	 * Checks and overwrites a value if necessary. Run the rubric computations.
	 * @param {string} what : context
	 * @param {string} objectType : descrob, criterion, criteria_group, rubric
	 * @param {obj} object : the object whose value is to be updated
	 */
	function checkAndUpdateValue (what, objectType, object) {
		let key;
		if (this.check(what, objectType, object)) {
			return;
		}
		switch (objectType) {
			case 'descrob':
				key = 'value';
				break;
			case 'criterion':
			case 'criteria_group':
				key = 'relative_weight';
				break;
			case 'rubric':
				switch (what) {
					case 'minBonusInvalidValue':
						key = 'min_bonus';
						break;
					case 'maxBonusInvalidValue':
						key = 'max_bonus';
						break;
					case 'gradingInvalidValue':
						key = 'grading';
						break;
				}
				break;
		}
		// Overwriting
		object[key] = (object[key] == null || object[key] === '') ? null : this.getNumericValue(object[key]);
		// Special feature for criteria: a weight of zero hides inner descrobs
		if (objectType === 'criterion' && object[key] === 0) {
			object.descrobs.forEach(d => {
				d.value = (object.type === 'descr') ? d.position + 1 : 1;
				this.check('invalidValue', 'descrob', d);
			});
		}
		// Compatibility issue
		if (what === 'gradingInvalidValue') {
			this.rubric.max_grade = this.rubric.grading;
		}
		this.computeAllWeightsAndMaxScores();
	}

	/**
	 * Checks user input (dispatches to the related methods)
	 * @param {string} what : context
	 * @param {string} objectType : descrob, criterion, criteria_group, rubric
	 * @param {obj} object : the object whose value is to be updated
	 * @return {*|boolean}
	 */
	function check (what, objectType, object) {
		switch (what) {
			case 'empty':
				return this.isEmpty(what, objectType, object);
			case 'nameInvalidText':
			case 'invalidText':
				return this.isTextInvalid(what, objectType, object);
			case 'invalidValue':
			case 'minBonusInvalidValue':
			case 'maxBonusInvalidValue':
			case 'gradingInvalidValue':
				let isInvalid = this.isValueInvalid(what, objectType, object);
				if (isInvalid) {
					this.computeAllWeightsAndMaxScores();
				}
				return isInvalid;
		}
	}

	/**
	 * Checks all the descriptors value inside a criterion.
	 * @param criterion
	 */
	function checkDescriptorValue (criterion) {
		if (criterion.type === 'descr') {
			criterion.descrobs.forEach(descrob => {
				this.check('invalidValue', 'descrob', descrob);
			});
		}
	}

	/**
	 * Checks criterion and its descrobs validity.
	 * @param criterion
	 */
	function checkCriterion (criterion) {
		this.check('empty', 'criterion', criterion);
		this.check('invalidText', 'criterion', criterion);
		this.check('invalidValue', 'criterion', criterion);
		criterion.descrobs.forEach(descrob => {
			this.check('invalidText', 'descrob', descrob);
			this.check('invalidValue', 'descrob', descrob);
		});
	}

	/**
	 * Checks everything (14)
	 */
	function checkAll () {
		this.rubric.errors = [];
		this.rubric.errorsHtml = null;
		this.check('empty', 'rubric', this.rubric);
		this.check('nameInvalidText', 'rubric', this.rubric);
		this.check('gradingInvalidValue', 'rubric', this.rubric);
		this.check('minBonusInvalidValue', 'rubric', this.rubric);
		this.check('maxBonusInvalidValue', 'rubric', this.rubric);
		this.rubric.criteria_group.forEach(g => {
			this.check('empty', 'criteria_group', g);
			this.check('invalidText', 'criteria_group', g);
			this.check('invalidValue', 'criteria_group', g);
		});
		this.rubric.criteria.forEach(c => this.checkCriterion(c));
	}

	/**
	 * Checks whether a value is valid or invalid according to its type.
	 * @param {string} what : context
	 * @param {string} objectType : descrob, criterion, criteria_group, rubric
	 * @param {obj} object : the object whose value is checked
	 * @returns {*|boolean}
	 */
	function isValueInvalid (what, objectType, object) {
		if (object == null) {
			return true;
		}
		let isInvalid;
		let invalidKey;
		let value;
		let allowEmptyValue;
		let checkInvalid;
		let precedingValue;
		switch (objectType) {
			case 'descrob':
				value = object.value;
				invalidKey = 'invalidValue';
				allowEmptyValue = false;
				checkInvalid = (newVal, precedingVal) => (isNaN(newVal) || newVal < 0 || (precedingVal != null && precedingVal >= newVal));
				break;
			case 'criterion':
			case 'criteria_group':
				value = object.relative_weight;
				invalidKey = 'invalidValue';
				allowEmptyValue = true;
				checkInvalid = (newVal) => (isNaN(newVal) || newVal < 0);
				break;
			case 'rubric' :
				switch (what) {
					case 'minBonusInvalidValue':
						value = object.min_bonus;
						invalidKey = 'minBonusInvalidValue';
						allowEmptyValue = false;
						checkInvalid = (newVal) => (isNaN(newVal) || newVal > 0);
						break;
					case 'maxBonusInvalidValue':
						value = object.max_bonus;
						invalidKey = 'maxBonusInvalidValue';
						allowEmptyValue = false;
						checkInvalid = (newVal) => (isNaN(newVal) || newVal < 0);
						break;
					case 'gradingInvalidValue':
						value = object.grading;
						invalidKey = 'gradingInvalidValue';
						allowEmptyValue = false;
						checkInvalid = (newVal) => (isNaN(newVal) || newVal < 0);
						break;
				}
		}
		if (value == null || value === '') {
			isInvalid = !allowEmptyValue;
		} else {
			const newValue = this.getNumericValue(value);
			if (objectType === 'descrob') {
				precedingValue = this.getDescrobPrecedingValue(object);
				isInvalid = checkInvalid(newValue, precedingValue);
			} else {
				isInvalid = checkInvalid(newValue);
			}
		}
		return this.updateErrorList(objectType, object, isInvalid, invalidKey);
	}

	/**
	 * Checks whether a text is valid or invalid.
	 * @param {string} what : context
	 * @param {string} objectType : descrob, criterion, criteria_group, rubric
	 * @param {obj} object : the object whose value is checked
	 * @returns {boolean}
	 */
	function isTextInvalid (what, objectType, object) {
		if (object == null) {
			return true;
		}
		let isInvalid;
		let invalidKey;
		let text;
		switch (objectType) {
			case 'descrob':
			case 'criterion':
			case 'criteria_group':
				text = object.description;
				invalidKey = 'invalidText';
				break;
			case 'rubric':
				text = object.name;
				invalidKey = 'nameInvalidText';
				break;
		}
		isInvalid = text == null || text.trim() === '';
		return this.updateErrorList(objectType, object, isInvalid, invalidKey);
	}

	/**
	 * Checks whether an object is empty or not.
	 * @param {string} what : context
	 * @param {string} objectType : criterion, criteria_group, rubric
	 * @param {obj} object : the object whose value is checked
	 * @returns {boolean}
	 */
	function isEmpty (what, objectType, object) {
		if (object == null) {
			return true;
		}
		let isInvalid;
		let invalidKey = 'empty';
		switch (objectType) {
			case 'criterion':
				isInvalid = object.descrobs.length === 0;
				break;
			case 'criteria_group':
				isInvalid = this.getCriteriaFromGroup(object).length === 0;
				break;
			case 'rubric':
				isInvalid = object.criteria_group.length === 0;
				break;
		}
		return this.updateErrorList(objectType, object, isInvalid, invalidKey);
	}

	/**
	 * Stores the error status of an object and updates the global list of errors.
	 * @param {string} objectType : descro, criterion, criteria_group, rubric
	 * @param {obj} object : the object whose value is checked
	 * @param {boolean} isInvalid : object validity
	 * @param {string} invalidKey : the error status key to be updated 
	 * @return {*}
	 */
	function updateErrorList (objectType, object, isInvalid, invalidKey) {
		object[invalidKey] = isInvalid;
		let errorKey = objectType + '_' + object['id_' + objectType] + '_' + invalidKey;
		let parentObject = null;
		if (objectType === 'descrob') {
			parentObject = this.findCriterion(object.id_criterion);
		}
		let errorElement = [errorKey, this.getInformation(invalidKey, objectType, object, parentObject)];
		if (!this.rubric.errors) {
			this.rubric.errors = [];
		}
		const index = this.rubric.errors.findIndex(elt => (elt[0] === errorElement[0]));
		if (isInvalid) {
			if (index > -1) {
				this.rubric.errors.splice(index, 1);
			}
			this.rubric.errors.push(errorElement);
		} else {
			if (index > -1) {
				this.rubric.errors.splice(index, 1);
			}
		}
		this.rubric.errorsHtml = '<ul>' 
			+ this.rubric.errors
				.map(error => error[1])
				.filter((item, position, array) => array.indexOf(item) === position)
				.reduce((acc, cur) => acc + '<li>' + cur + '</li>', '')
			+ '</ul><br />';
		this.formInvalid = this.rubric.errors.length > 0;
		return isInvalid;
	}

	/**
	 * Gets the weight of the preceding descrob of a given one.
	 * Used to check if the weights ascending order is correct.
	 * @param {obj} descrob
	 * @returns {*|number}
	 */
	function getDescrobPrecedingValue(descrob) {
		const criterion = this.findCriterion(descrob.id_criterion);
		if (criterion == null || criterion.type === 'ob') {
			return -1;
		}
		const targetPosition = descrob.position - 1;
		if (targetPosition === -1) {
			return -1;
		}
		const precedingDescrob = criterion.descrobs.find(d => +d.position === targetPosition);
		if (precedingDescrob.invalidValue) {
			return -1;
		}
		return precedingDescrob.value;
	}

	/**
	 * Initializes Sortablejs object for a given object and its DOM parent selector
	 * @param {string} object
	 * @param {obj} parentDOMSelector
	 */
	function initializeSorting (object, parentDOMSelector) {
		Sortable.create(parentDOMSelector, this.setSortConfig(object, parentDOMSelector));
	}

	/**
	 * Sets the configuration for Sortablejs.
	 * @param {string} object
	 * @param {obj} parentDOMSelector
	 * @returns {obj}
	 */
	function setSortConfig (object, parentDOMSelector) {
		let config = {};
		let options = {
			revertOnSpill: false,
			animation: 200,
			handle: '.' + object + '-handle',
			draggable: '.' + object + '-item-draggable',
			onUpdate: (event) => { this.sortUpdate(event); },
			onStart: (event) => {
				this.rubric.preventSavingOnDrag = true;
				if (object === 'criterion' && Array.from(event.from.children).filter(e => e.className.indexOf('criterion-item-draggable') !== -1).length === 1) {
					this.findGroup(event.from.getAttribute('data-id')).addEmptyTrOnDrag = true;
				}
			},
			onEnd: (event) => {
				this.rubric.preventSavingOnDrag = false;
				if (object === 'criterion' && Array.from(event.from.children).filter(e => e.className.indexOf('criterion-item-draggable') !== -1).length === 1) {
					this.findGroup(event.from.getAttribute('data-id')).addEmptyTrOnDrag = false;
				}
			},
		}
		if (object === 'criterion') {
			options.group = {
				name: 'criterion-shared-group',
				pull: function (to, from) {
					return from.toArray().length > 0;
				},
			};
			options.onAdd = (event) => { this.sortAddAndRemove(event); }
		}
		Object.assign(config, options);
		return config;
	}

	/**
	 * The callback triggered when an element is moved inside the same parent with Sortablejs.
	 * @param {obj} event
	 */
	function sortUpdate(event) {
		let parentType = event.from.dataset.type;
		let parentId = event.from.dataset.id;
		let parentObject;
		let siblingList = [];
		if (!this.haveTheSameId(parentId, event.to.dataset.id)) {
			return;
		}
		switch (parentType) {
			case 'criterion':
				parentObject = this.findCriterion(parentId);
				siblingList = parentObject.descrobs;
				break;
			case 'criteria_group':
				siblingList = this.getCriteriaFromGroup(this.findGroup(parentId));
				break;
			case 'rubric':
				siblingList = this.rubric.criteria_group;
				break;
		}
		let oldWeightsOrder = this.swapPositions(event.oldDraggableIndex, event.newDraggableIndex, siblingList);
		if (parentType === 'criterion' && parentObject.type === 'descr') {
			this.swapWeights(siblingList, oldWeightsOrder);
			parentObject.descrobs.forEach(d => { this.check('invalidValue', 'descrob', d); });
		}
		this.rubric.hasChanged = true;
		this.rubric.preventSavingOnDrag = false;
	}

	/**
	 * The callback triggered when an element is moved outside the parent with Sortablejs.
	 * @param {obj} event
	 */
	function sortAddAndRemove(event) {
		let groupFromType = event.from.dataset.type;
		let groupFromId = event.from.dataset.id;
		let groupToType = event.to.dataset.type;
		let groupToId = event.to.dataset.id;
		let criterionType = event.item.dataset.type;
		let criterionId = event.item.dataset.id;
		if (groupFromId === groupToId || groupFromType !== groupToType || groupFromType !== 'criteria_group' || criterionType !== 'criterion') {
			return;
		}
		let criterion = this.findCriterion(criterionId);
		criterion.id_criteria_group = groupToId;
		criterion.position = parseInt(event.newDraggableIndex) - 0.5;

		let oldGroup = this.findGroup(groupFromId);
		let oldGroupCriteria = this.getCriteriaFromGroup(oldGroup);
		this.updatePositions(oldGroupCriteria);
		this.check('empty', 'criteria_group', oldGroup);

		let newGroup = this.findGroup(groupToId);
		let newGroupCriteria = this.getCriteriaFromGroup(newGroup);
		this.updatePositions(newGroupCriteria);
		this.computeAllWeightsAndMaxScores();
		this.check('empty', 'criteria_group', newGroup);

		this.rubric.hasChanged = true;
		this.rubric.preventSavingOnDrag = false;
	}

    /**
     * Redefines element positions according to their index (because sometimes, positions are not integers)
     * Note that index order matches position order.
     * @param {array} objectArray
     */
    function updatePositions (objectArray) {
        for (let i = 0; i < objectArray.length; i++) {
            objectArray[i].position = i;
        }
    }

    /**
     * Swaps positions after moving an element.
     * @param {number} oldIndex : previous index
     * @param {number} newIndex : new index
     * @param {array} objectArray
     * @return {array} a backup of the old weights (used to swap weights)
     */
    function swapPositions(oldIndex, newIndex, objectArray) {
        const oldWeightsOrder = objectArray
            .map(elt => elt.value === undefined ? null : elt.value)
            .filter(elt => elt !== undefined);
        if (newIndex >= objectArray.length || newIndex === oldIndex) {
            return [];
        }
        objectArray[oldIndex].position = newIndex - 0.5 + (oldIndex < newIndex);
        objectArray.sort((a, b) => a.position - b.position);
        this.updatePositions(objectArray);
        return oldWeightsOrder;
    }

    /**
     * Swaps weights after moving a descriptor.
     * @param {array} descriptorArray
     * @param {array} oldWeightsOrder
     */
    function swapWeights (descriptorArray, oldWeightsOrder) {
        if (oldWeightsOrder.length > 0) {
            for (let i = 0; i < descriptorArray.length; i++) {
                descriptorArray[i].value = oldWeightsOrder[i];
            }
        }
    }

	/**
	 * Returns the criteria of a given group.
	 * @param {obj} group
	 * @return {array} : a list of criterion
	 */
	function getCriteriaFromGroup (group) {
		if (group === undefined) {
			return [];
		}
		return this.rubric.criteria.filter(e => this.haveTheSameId(e.id_criteria_group, group.id_criteria_group))
			.sort((a, b) => a.position - b.position);
	}

	/**
	 * Gets a criterion by id.
	 * @param {string|number|*} idCriterion
	 * @return {obj} : criterion
	 */
	function findCriterion (idCriterion) {
		return this.rubric.criteria.find(e => this.haveTheSameId(e.id_criterion, idCriterion));
	}

	/**
	 * Gets a group by id.
	 * @param {string|number|*} idCriteriaGroup
	 * @return {obj} : group
	 */
	function findGroup (idCriteriaGroup) {
		return this.rubric.criteria_group.find(e => this.haveTheSameId(e.id_criteria_group, idCriteriaGroup));
	}

	/** 
	 * Gets an information given a context.
	 * @param {string} key : the key linked to the information
	 * @param {string} objectType : descrob, criterion, criteria_group, min-bonus, max-bonus, grading
	 * @param {obj} object : the object linked to the information
	 * @param {obj} parentObject : the parent object of the object
	 * @returns {string}
	 */
	function getInformation (key, objectType, object=null, parentObject=null) {
		let info = '';
		let criterionObject = null;
		let singleDescrobUpperCase = '';
		let singleDescrob = ''
		let multipleDescrobs = '';
		let multipleDescrobsList = '';
		let thisDescrob = '';
		let isThereACriterionInContext = false;
		if (objectType === 'descrob' && parentObject !== null) {
			criterionObject = parentObject;
			isThereACriterionInContext = true;
		}
		if (objectType === 'criterion' && object !== null) {
			criterionObject = object;
			isThereACriterionInContext = true;
		}
		if (isThereACriterionInContext) {
			singleDescrobUpperCase = criterionObject.type === 'descr' ? __("Descripteur") : __("Observable");
			singleDescrob = criterionObject.type === 'descr' ? __("un descripteur") : __("un observable");
			multipleDescrobs = criterionObject.type === 'descr' ? __("descripteurs") : __("observables");
			multipleDescrobsList = criterionObject.type === 'descr' ? __("checklist") : __("niveaux de maîtrise");
			thisDescrob = (criterionObject.type === 'descr' ? __("ce descripteur") : __("cet observable"));
		}
		switch (objectType) {
			case 'descrob':
				switch (key) {
					// Action buttons
					case 'addDescrobButton': info = __("Ajouter ") + singleDescrob; break;
					case 'addDescriptorButton': info = __("Ajouter un descripteur (évaluation par niveaux de maîtrise)"); break;
					case 'addObservableButton': info = __("Ajouter un observable (évaluation par checklist)"); break;
					case 'moveDescrobButton': info = __("Déplacer ") + thisDescrob; break;
					case 'deleteDescrobButton': info = __("Supprimer ") +  thisDescrob; break;
					case 'changeDescrobDescriptionButton': info = __("Modifier ") + thisDescrob; break;
					case 'changeDescrobWeightButton': info = __("Modifier le poids de ") + thisDescrob; break;
					// Hover
					case 'descrobTextareaHover': info = singleDescrobUpperCase; break;
					case 'descrobInputHover': info = __("Poids de ") + thisDescrob; break;
					// Placeholder
					case 'descrobDescriptionPlaceholder':
						info = singleDescrobUpperCase + ' #' + (object.position + 1);
						break;
					// Inline errors
					case 'descrobErrorHover': info = __("Anomalie détectée, cliquez pour en savoir davantage"); break;
					case 'descrobDescriptionErrorIcon':
						info = __("Ce champ correspond à l’intitulé d’") + singleDescrob + '.'
							+ '<br />' + __("Il doit obligatoirement être renseigné.") + '<br /><br />';
						break;
					case 'descrobWeightErrorIcon':
						info = __("Le poids relatif d’") + singleDescrob + __(" est un nombre positif ou nul qui définit son importance par rapport aux autres ") + multipleDescrobs + __(" faisant partie d’un même critère.")
							+ '<br />' + __("Il doit obligatoirement être renseigné.");
						if (criterionObject.type === 'descr') {
							info += '<br />' + __("De plus, les descripteurs d’un même critère doivent présenter des poids tous différents et rangés dans l’ordre croissant de gauche à droite.");
						}
						if (object.value === '0' && criterionObject.type === 'descr') {
							info += '<br />' + __("Si vous souhaitez qu’un critère ne soit pas pris en compte dans la notation, plutôt que d’attribuer un poids de 0 à chaque descripteur, attribuez 0 au poids du critère.");
						}
						info +=  '<br /><br />';
						break;
					// Summary errors
					case 'invalidText':
						info = __("L’intitulé d’") + singleDescrob + __(" doit être renseigné.");
						break;
					case 'invalidValue':
						info = __("Le poids relatif d’") + singleDescrob + __(" est un nombre positif ou nul. Il doit être renseigné.");
						if (criterionObject.type === 'descr') {
							info += ' ' + __("De plus, les descripteurs d’un même critère doivent présenter des poids tous différents et rangés dans l’ordre croissant de gauche à droite.");
						}
				}
				break;
			case 'criterion':
				switch (key) {
					// Action buttons
					case 'addCriterionButton': info = __("Ajouter un critère"); break;
					case 'duplicateCriterionButton': info = __("Dupliquer ce critère"); break;
					case 'moveCriterionButton': info = __("Déplacer ce critère"); break;
					case 'toggleCriterionTypeButton':
						info = __("Convertir en ") + multipleDescrobsList;
						break;
					case 'deleteCriterionButton': info = __("Supprimer ce critère"); break;
					case 'changeCriterionDescriptionButton': info = __("Modifier l’intitulé de ce critère"); break;
					case 'changeCriterionWeightButton': info = __("Modifier le poids de ce critère"); break;
					// Hover
					case 'criterionTextareaHover': info = __("Intitulé de ce critère"); break;
					case 'criterionInputHover': info = __("Poids de ce critère"); break;
					case 'criterionScoreHover': info = __("Barême de ce critère"); break;
					// Placeholder
					case 'criterionDescriptionPlaceholder': info = __("Intitulé du critère") + ' #' + (object.position + 1); break;
					// Inline errors
					case 'criterionErrorHover': info = __("Anomalie détectée, cliquez pour en savoir davantage"); break;
					case 'criterionDescriptionErrorIcon':
						info = __("Ce champ correspond à l’intitulé d’un critère.")
							+ '<br />' + __("Il doit obligatoirement être renseigné.") + '<br /><br />';
						break;
					case 'criterionWeightErrorIcon':
						info = __("Le poids relatif d’un critère est un nombre positif ou nul qui définit son importance par rapport aux autres critère du même groupe.")
							+ '<br />' + __("Il peut éventuellement ne pas être renseigné. Dans ce cas, sa valeur est calculée automatiquement en fonction des poids des ") + multipleDescrobs + __(" qui le composent.")
							+ '<br /><br />';
						break;
					// Summary errors
					case 'empty': info = __("Chaque critère doit contenir au moins un descripteur ou un observable."); break;
					case 'invalidText': info = __("L’intitulé d’un critère doit être renseigné."); break;
					case 'invalidValue':
						info = __("Le poids relatif d’un critère est un nombre positif ou nul. Il est cependant possible de ne pas le renseigner.");
						break;
				}
				break;
			case 'criteria_group':
				switch (key) {
					// Action buttons
					case 'addGroupButton': info = __("Ajouter un groupe de critères"); break;
					case 'moveGroupButton': info = __("Déplacer ce groupe de critères"); break;
					case 'deleteGroupButton': info = __("Supprimer ce groupe de critères"); break;
					case 'changeGroupDescriptionButton': info = __("Modifier l’intitulé de ce groupe de critères"); break;
					case 'changeGroupWeightButton': info = __("Modifier le poids de ce groupe de critères"); break;
					// Hover
					case 'groupTextareaHover': info = __("Intitulé de ce groupe de critères"); break;
					case 'groupInputHover': info = __("Poids de ce groupe de critères"); break;
					case 'groupScoreHover': info = __("Barême de ce groupe de critères"); break;
					// Placeholder
					case 'groupDescriptionPlaceholder': info = __("Intitulé du groupe de critères") + ' #' + (object.position + 1); break;
					// Inline errors
					case 'groupErrorHover': info = __("Anomalie détectée, cliquez pour en savoir davantage"); break;
					case 'groupDescriptionErrorIcon':
						info = __("Ce champ correspond à l’intitulé d’un groupe de critères.")
							+ '<br />' + __("Il doit obligatoirement être renseigné.") + '<br /><br />';
						break;
					case 'groupWeightErrorIcon':
						info = __("Le poids relatif d’un groupe de critères est un nombre positif ou nul qui définit son importance par rapport aux autres groupes de critères.")
							+ '<br />' + __("Il peut éventuellement ne pas être renseigné. Dans ce cas, sa valeur est calculée automatiquement en fonction des poids des critères qui le composent.") 
							+ '<br /><br />';
						break;
					// Summary errors
					case 'empty':
						info = __("Chaque groupe de critères doit contenir au moins un critère.");
						break;
					case 'invalidText':
						info = __("L’intitulé d’un groupe de critères doit être renseigné.");
						break;
					case 'invalidValue':
						info = __("Le poids relatif d’un groupe de critères est un nombre positif ou nul. Il est cependant possible de ne pas le renseigner.");
						break;
				}
				break;
			case 'rubric':
				switch (key) {
					// Action buttons
					case 'changeRubricNameButton': info = __("Modifier le nom de cette grille"); break;
					case 'changeMinBonusButton': info = __("Modifier la valeur minimale du malus"); break;
					case 'changeMaxBonusButton': info = __("Modifier la valeur maximale du bonus"); break;
					case 'changeGradingButton': info = __("Modifier le barême global"); break;
					// Hover
					case 'minBonusErrorHover': info = __("Anomalie détectée, cliquez pour en savoir davantage"); break;
					case 'maxBonusErrorHover': info = __("Anomalie détectée, cliquez pour en savoir davantage"); break;
					case 'gradingErrorHover': info = __("Anomalie détectée, cliquez pour en savoir davantage"); break;
					case 'minBonusInputHover': info = __("Valeur minimale du malus"); break;
					case 'maxBonusInputHover': info = __("Valeur maximale du bonus"); break;
					case 'gradingInputHover': info = __("Barême global"); break;
					// Inline errors
					case 'minBonusErrorIcon':
						info = __("La valeur minimale du malus est un nombre négatif ou nul.")
							+ '<br />' + __("Cette valeur doit obligatoirement être renseignée mais si vous ne voulez pas utiliser de malus, vous pouvez écrire 0 (zéro).")
							+ '<br /><br />';
						break;
					case 'maxBonusErrorIcon':
						info = __("La valeur maximale du bonus est un nombre positif ou nul.")
							+ '<br />' + __("Cette valeur doit obligatoirement être renseignée mais si vous ne voulez pas utiliser de bonus, vous pouvez écrire 0 (zéro).")
							+ '<br /><br />';
						break;
					case 'gradingErrorIcon':
						info = __("Le barême global est un nombre positif ou nul.")
							+ '<br />' + __("Il doit obligatoirement être renseigné.") + '<br /><br />';
						break;
					// Summary errors
					case 'nameInvalidText': info = __("Le nom de la grille doit être renseigné."); break;
					case 'empty': info = __("La grille doit contenir au moins un groupe de critères."); break;
					case 'minBonusInvalidValue': info = __("La valeur minimale du malus doit être un nombre négatif ou nul."); break;
					case 'maxBonusInvalidValue': info = __("La valeur maximale du bonus doit être un nombre positif ou nul."); break;
					case 'gradingInvalidValue': info = __("Le barême global doit être un nombre positif ou nul. Il est obligatoire."); break;
				}
				break;
			case 'global':
				switch (key) {
					case 'existingAssessments':
						const existingAssessmentsCount = this.rubric.assessments.filter(e => e.published === 0).length;
						if (existingAssessmentsCount === 1) {
							info = __('Il y a une évaluation rattachée à cette grille et non publiée. Toute modification de la grille sera reportée sur cette évaluation.');
						} else {
							info = __('Il y a {{existingAssessmentsCount}} évaluations rattachées à cette grille et non publiées. Toute modification de la grille sera reportée sur ces évaluations.',
								{ existingAssessmentsCount: existingAssessmentsCount })
						}
						break;
					case 'publishedAssessments':
						const publishedAssessmentsCount = this.rubric.assessments.filter(e => e.published !== 0).length;
						if (publishedAssessmentsCount === 1) {
							info = __('Il y a une évaluation rattachée à cette grille et publiée. Aucune modification de la grille ne sera reportée sur cette évaluation.');
						} else {
							info = __('Il y a {{publishedAssessmentsCount}} évaluations rattachées à cette grille et publiées. Aucune modification de la grille ne sera reportée sur ces évaluations.',
								{ publishedAssessmentsCount: publishedAssessmentsCount })
						}
						break;
				}
				break;
		}
		return info;
	}

	/**
	 * Computes the field computed_relative_weight for each criterion and criteria_group.
	 */
	function computeAllWeightsAndMaxScores () {
		// Compute criteria weight
		this.rubric.criteria.forEach((c)=> {
			if (c.relative_weight !== null) {
				// teacher defined
				c.computed_relative_weight = c.relative_weight;
			} else if (c.type === 'ob') {
				// Observable sum values
				c.computed_relative_weight = c.descrobs.reduce((acc, cur) => acc + parseFloat(cur.value), 0);
			} else {
				// descriptor max of values
				c.computed_relative_weight = c.descrobs.reduce((acc, cur) => Math.max(acc, parseFloat(cur.value)), 0);
			}
		});
		// Compute group weights
		this.rubric.criteria_group.forEach((g)=> {
			const inner_computed_weight_sum = this.getCriteriaFromGroup(g).reduce((acc, cur) => acc + parseFloat(cur.computed_relative_weight), 0);
			if (inner_computed_weight_sum === 0) {
				// All criteria with non-null weight are neutralized
				// this group weights nothing
				// This may replace the teacher choice
				g.computed_relative_weight = 0;
			} else if (g.relative_weight === null || g.relative_weight === '') {
				// Teacher has not defined the group weight, use sum of
				// computed criteria weights
				g.computed_relative_weight = inner_computed_weight_sum;
			} else {
				// Keep the teacher choice
				g.computed_relative_weight = g.relative_weight;
			}
		});
		// Compute total weight
		this.rubric.total_weight = this.rubric.criteria_group.reduce((acc, cur) => acc + parseFloat(cur.computed_relative_weight), 0);
		// Compute max scores
		this.computeMaxScores();
	}

	/**
	 * Computes the maximum reachable score of each criterion and group.
	 */
	function computeMaxScores () {
		// Criterion max scores
		this.rubric.criteria.forEach((c) => {
			const my_group_weight = this.rubric.criteria_group.find(e => this.haveTheSameId(e.id_criteria_group, c.id_criteria_group)).computed_relative_weight;
			const sum_criteria_weight_of_my_group = this.rubric.criteria
				.filter(e => this.haveTheSameId(e.id_criteria_group, c.id_criteria_group))
				.reduce((acc, cur) => acc + cur.computed_relative_weight, 0);
			if (c.computed_relative_weight === 0 || my_group_weight === 0 || sum_criteria_weight_of_my_group === 0 || this.rubric.total_weight === 0) {
				c.max_score = 0;
			} else {
				c.max_score = my_group_weight / this.rubric.total_weight * c.computed_relative_weight / sum_criteria_weight_of_my_group * this.rubric.grading;
			}
		});
		// Group max scores
		this.rubric.criteria_group.forEach((g) => {
			g.max_score = this.getCriteriaFromGroup(g).reduce((acc, cur) => acc + cur.max_score, 0);
		});
	}

	/**
	 * Checks whether two ids are the same.
	 * @param {string|number|*} firstId
	 * @param {string|number|*} secondId
	 * @returns {boolean}
	 */
	function haveTheSameId (firstId, secondId) {
		if (!isNaN(parseInt(firstId)) && !isNaN(parseInt(secondId))) {
			return parseInt(firstId) === parseInt(secondId);
		}
		return firstId === secondId;
	}

	/**
	 * Returns a unique id.
	 * Alpinejs needs us to add some ids to avoid incoherent behavior on
	 * delete, but we won't have a meaningful id until we save the rubric
	 * @return {string} - a unique id
	 */
	function uuid () {
		return 'fake_' + Math.random().toString().slice(2, 17);
	}

	/**
	 * Checks whether an id is fake or real.
	 * @param {string} id
	 */
	function isFakeId (id) {
		return String(id).indexOf('fake_') === 0;
	}

	/**
	 * Decides whether to show or hide existing assessments linked to the rubric.
	 * The parameter 'published' specifies the status of those assessments (published or not) 
	 * @param {boolean} published
	 * @returns {boolean}
	 */
	function showExistingAssessments (published) {
		return this.rubric.assessments.filter(e => { 
			if (published) {
				return e.published !== 0;
			} else {
				return e.published === 0;
			}
		}).length > 0;
	}

	return {
		init,
		getRubric, getActivities, setViewConfig,
		saveRubric, postRubric, rubricAutoSave,
		createRubric, duplicateRubric, deleteRubric,
		clearRubric, emptyDeletes, 
		editRubric, setRubricEdition,
		addDescrOb, addCriterion, addGroup, toggleCriterionType,
		deleteDescrOb, deleteCriterion, deleteGroup, markForDeletion,
		displayValue, displayValueOrComputedValue, format, getNumericValue,
		checkAndUpdateValue, check, checkDescriptorValue, checkCriterion, checkAll,
		isValueInvalid, isTextInvalid, isEmpty, updateErrorList,
		getDescrobPrecedingValue,
		initializeSorting, setSortConfig, sortUpdate, sortAddAndRemove,
        updatePositions, swapPositions, swapWeights,
		getCriteriaFromGroup, findCriterion, findGroup,
		getInformation,
		computeAllWeightsAndMaxScores, computeMaxScores,
		haveTheSameId, uuid, isFakeId,
		showExistingAssessments,
		alpineValidContent: validContent, alpineCancelContent: cancelContent, alpineEditContent: editContent,
		dataLoaded: false,
		rubricEdition: false,
		showEditor: true,
		formInvalid: false,
		rubric: { initialized: false, hasChanged: false, preventSavingOnDrag: false },
		template_rubric: {},
		viewConfig: { tablePercentWidth: 100, criterionTitlePercentWidth: 18, criterionAddDescrobWidth: 2, criterionScorePercentWidth: 8 },
		deletes: { criteria: [], criteria_group: [], descrobs: [] },
	}
};

window.onbeforeunload = function () {
	if (window.rubricEdition) {
		// The following instruction doesn't work on many browsers (2023). These are displaying their own warning.
		return __("Une grille critériée est en cours d'édition, les données non sauvées seront perdues");
	}
}
