// ****************
//     Globals
// ****************

window.global_edited_ld = 0 ; // id du LD en édition - 0 si il n'y en a pas
window.global_ld_max_local_versions = 40 ; // number of levels of undo_redo

// *************************************************
//      Functions used when displaying a labdoc
// *************************************************

/**
 * FIx the height of a LD before modifying it : avoid "jumps" in the page
 * @param {int} id_ld
 */
function blockLDHeight(id_ld) {
	let ld_height = $("#labdoc_" + id_ld).height();
	$("#labdoc_" + id_ld).css("height", ld_height);
}

/**
 * @param {int} id_ld
 * @param {int} rel_pos relative position of the LD towards the current version
 * @param {int} is_assignment are we looking for a ld history or an assignment
 * @returns {string|false}
 */
function getLDContentFromLocalHistory(id_ld, rel_pos= 0, is_assignment) {
	if (is_assignment) {
		// No history for assignments for now
		return global_tab_ld[id_ld].ld_assignment_history[0];
	}
	if (isLDVersionAvailable(rel_pos, id_ld)) {
		let i = global_tab_ld[id_ld].current_i + rel_pos;
		return global_tab_ld[id_ld].ld_history[i];
	} else {
		return false;
	}
}

/**
 * State whether a labdoc is a procedure is stored in the given format
 * 
 * @param {int} id_ld
 * @param {string} format 'xml'|'json'
 * @param {string} content
 * @param {string} type
 * @returns {Boolean}
 */
function isProcedure (id_ld, format, content = '', type = '') {
	const ld = window.global_tab_ld[id_ld];
	if (ld) {
		content = ld.ld_history[0];
		type = ld.ld_type;
	}
	if (type !== 'procedure') {
		return false;
	}
	if (content.trim().startsWith('<?xml')) {
		return format === 'xml';
	}
	return format === 'json';
}


/**
 * parse the content to create a valid XML Document that can be traversed and manipulated
 * Useful for display dataset and protocols LD
 *
 * @param {int} id_ld
 * @param {int} index position in the history stack
 * @returns {Boolean}
 */
function createXMLLD(id_ld, index) {
	let ld_content = global_tab_ld[id_ld].ld_history[index];
	try {
		global_tab_ld[id_ld].ld_content = $.parseXML(ld_content);
	}
	catch(e) {
		$("#labdoc_content_"+id_ld).html(__("Le code XML du labdoc est corrompu - affichage impossible")) ;
		console.error("The LD "+id_ld+" has a corrupted XML code: not displayed");
		return false;
	}
	return true;
}

function commonTinyMCEPostInitCb(is_assignment) {
	//Si le formulaire existe
	let id_ld = window.global_edited_ld;
	if (!id_ld) {
		// This function might have been called to late
		return;
	}
	const suffix = is_assignment ? 'assignment' : 'content';
	const suffix_txt = is_assignment ? '_assignment' : '';
	const id_ld_content = '#labdoc_' + suffix + '_' + id_ld;
	const id_ld_text = 'ld_txt_' + id_ld + suffix_txt;
	const uploadDivSelector = id_ld_content + ' .tmce_image_upload';
	if ($(uploadDivSelector).length > 0) {
		//On abonne le champ qui va permettre de choisir les photos
		$(uploadDivSelector + ' input').off();
		$(uploadDivSelector + ' input').on('change', function () {
			$(uploadDivSelector +' form').submit();
		});

		//On abonne la soumission du formulaire au traitement ajax de l'image
		$(uploadDivSelector + ' form').off();
		$(uploadDivSelector + ' form').on('submit', function (e) {
			//On empeche le navigateur de soumettre le formulaire
			e.preventDefault();

			const fileInput = $(e.target).find('input[type="file"]')[0];
			uploadImage(fileInput).resizeImageToEncoded()
				.then(function (encodedImg) {
					$('.tox-dialog__content-js .tox-textfield:first').val(encodedImg);
				}).catch(function (errorMessage) {
					if (errorMessage) {
						console.error(errorMessage);
						alert(errorMessage);
					}
				});
		});
	}

	// mandatory to update the text in global_tab_ld : TinyMCE trims some spaces in the text on init
	// may hinder the history function
	try {
		var new_ld_content = window.tinymce.get(id_ld_text).getContent(); // ERROR on first opening !
	} catch (ex) {
		let container = document.getElementById(id_ld_text);
		logError(
			"displayText(): error in tinymce.getContent()",
			'danger',
			"[HTMLid=" + id_ld_text + "] " + (container === null ? "missing. " : "exists. ") + ex.message
		);
	}
	if (typeof new_ld_content !== 'undefined' && new_ld_content !== '') {
		let i = global_tab_ld[id_ld].current_i;
		if (!is_assignment) {
			global_tab_ld[id_ld].ld_history[i] = new_ld_content;
		} else {
			global_tab_ld[id_ld].ld_assignment_history[i] = new_ld_content;
		}
	}
}

function ldTextPostInitCb() {
	commonTinyMCEPostInitCb(false);
}

function assignmentPostInitCb() {
	commonTinyMCEPostInitCb(true);
	let id_ld = window.global_edited_ld;
	$('#labdoc_assignment_' + id_ld + ' .sizing-bar').remove();
}

/**
 * Callback for file picker on LD text
 */
function ldTextFilePickerCb(callback, value, meta, is_assignment = false) {
	// Provide image and alt text for the image dialog
	const suffix = is_assignment ? 'assignment' : 'content';
	let id_ld = window.global_edited_ld;
	const id_ld_text = '#labdoc_' + suffix + '_' + id_ld;
	const uploadDivSelector = id_ld_text + ' .tmce_image_upload';
	if (meta.filetype === 'image') {
		$(uploadDivSelector + ' input').attr('accept', ".gif,.jpeg,.jpg,.png");
		$(uploadDivSelector + ' input').click();
	}
}


function ldTextAssignmentFilePickerCb(callback, value, meta) {
	ldTextFilePickerCb(callback, value, meta, true);
}

/**
 * @param string selector for the TinyMCE DIV
 * @param init_cb
 * @param file_picker_cb
 * @param array extraparams
 */
function init_tinyMCE(selector, init_cb, file_picker_cb, extraparams) {
	let params = {
		branding: false,
		//statusbar: false,
		selector: selector,
		language: langLong(),
		paste_data_images: false, // Permet de faire de drag and drop d'image en base64
		height: "100%",
		custom_undo_redo_levels: 10,
		browser_spellcheck: true,
		contextmenu: false,
		plugins: [ " "
		//+"advlist" 		// Ce plugin ajoute des options plus avancées aux boutons de liste ordonnée et non ordonnées. Il vous permet de définir les formats de nombre et les types de puces.
		//+" autolink"		// Normalise le comportement avec IE qui produit des liens lorsque vous tapez simplement des liens dans la fenêtre de contenu.
		//+" anchor" 		// Ce plugin ajoute / un bouton et le menu des favoris ancre.
		+" charmap" 		// Ce plugin ajoute un élément de menu charmap et le bouton qui vous permet d'insérer des caractères spéciaux.
		//+" code"  		// Ce plugin ajoute un élément de menu de base ou le bouton qui vous permet de modifier le code HTML.
		//+" colorpicker"  	// Ce plugin ajoute une colorpicker pleine soufflé à l'éditeur en réglant le color_picker_callback.
		//+" contextmenu" 	// Ce plugin ajoute un menu contextuel configurable.
		//+" directionality"  // Ce plugin ajoute des icônes de directivité à TinyMCE qui selon la langues de poignée qui est écrit de droite à gauche
		//+" emoticons"  	// Le émoticônes plugin est capable d'insérer des images smiley dans la zone éditable TinyMCE.
		//+" fullscreen"  	// Ce plugin ajoute le mode d'édition plein écran pour TinyMCE.
		//+" hr"  			// Ce plugin ajoute un élément et le bouton menu de commande qui vous permet d'insérer une règle horizontale à la page.
		+" image"  			// Ce plugin ajoute une image bouton d'insertion et élément de menu.
		//+" insertdatetime" // http://www.tinymce.com/wiki.php/Plugin:insertdatetime
		+" link"  			// Ce plugin ajoute un élément de menu et le bouton qui vous permet d'insérer des liens vers des textes et des images.
		+" lists"  			// Cette liste Plugin normalise le comportement de la liste entre les navigateurs, lui permettre, si vous avez des problèmes avec cohérence faire des listes.
		//+" nonbreaking"  	// Ce plugin ajoute un bouton pour insérer insécable entités & nbsp spatiales; à l'emplacement du curseur en cours.
		//+" pagebreak"  	// Ajoute le support pagebreak, certains systèmes CMS utilise un séparateur spécial pour briser le contenu dans les pages. Ce plugin vous permet d'insérer ces sauts de page dans l'éditeur.
		+" paste"  			// Ce plugin va filtrer / contenu de nettoyage collées à partir de Word.
		//+" preview"  		// Ce plugin ajoute un bouton de prévisualisation pour TinyMCE, en appuyant sur le bouton ouvre une fenêtre montrant le contenu actuel. Instructions d'installation
		//+" print"  		// Ce plugin ajoute un bouton d'impression pour TinyMCE.
		//+" save"   	   	// Ce plugin ajoute un bouton Enregistrer pour tinyMCE ce bouton Enregistrer soumettre le formulaire que l'éditeur est en dedans.
		//+" searchreplace"  	// Ce plugin ajoute rechercher / remplacer les boîtes de dialogue de TinyMCE.
		+" table"  			// Ce plugin ajoute une fonctionnalité de gestion de table de TinyMCE.
		+" textcolor"  		// Ce plugin ajoute les contrôles forecolor / bouton BackColor qui vous permettent de choisir des couleurs à partir d'un sélecteur de couleur et de les appliquer au texte.
		//+" textpattern" 	// Ce plugin correspond à motifs spéciaux dans le texte et applique des formats ou des commandes exécutées sur ces modèles de texte. Les schémas par défaut est similaire à la syntaxe Markdown vous pouvez taper "# texte" pour produire un en-tête ou "** ** texte à faire quelque chose d'audacieux.
		//+" visualblocks"  // Ce plugin ajoute un élément de bouton et le menu qui vous permet de voir les éléments de bloc dans l'éditeur.
		//+" visualchars"  	// Ce plugin ajoute la possibilité de voir des personnages invisibles comme & nbsp;. Instructions d'installation
		//+" eqneditor"  	// Plugin eqneditor codeCogs
		+" wordcount"		// Ce plugin ajoute une fonctionnalité de comptage de mot pour TinyMCE.
		+" spellchecker"
		+" mathmlelements"  // Plugin qui permet la persistance des équations mathML même si le Plugin EpsilonWriter ne fonctionne pas (éléments non éditables du coup)
		+" mathlive-equation-editor"
		],
		entity_encoding : "raw", // nécessaire pour le Plugin EpsilonWriter
		encoding: "UTF-8", // nécessaire pour le Plugin EpsilonWriter
		content_style: ".mce-content-body {font-size:9pt;}",
		fontsize_formats: "8pt 9pt 10pt 11pt 12pt 14pt 18pt 24pt 36pt",
		// menu table
		table_appearance_options: false,
		table_advtab: false,
		// menu lien
		target_list: false,
		default_link_target: "_blank",
		image_advtab: false,
		// menu file
		file_browser_callback_types: 'image',
		//On gère le procédé d'upload d'image en base 64 via tinyMCE et le plugin Image
		file_picker_callback: file_picker_cb,
		//Quand tiny MCE a fini de charger les éléments
		init_instance_callback : init_cb,
		menubar: false,
		toolbar1: "pastetext | formatselect  fontselect fontsizeselect | forecolor bold italic underline strikethrough subscript superscript",
		//toolbar2: "bullist numlist outdent indent | alignleft aligncenter alignright alignjustify | table image link | charmap epsilon",
		toolbar2: "bullist numlist outdent indent | alignleft aligncenter alignright alignjustify | table image link | charmap mathlive-equation-editor",
		content_css:"/libraries/katex/katex.min.css",
		//extended_valid_elements : 'span [ class | data-eplw | data-katex | aria-hidden | style | id ] , annotation[*]' //Necessaire epsilonwriter
		resize: false,
		extended_valid_elements : 'span [*] , annotation[*]',
		equation_editor_config: {
			lang_keys: {
				"window_title": __("Éditeur d’équations"),
				"btn_cancel_text": __("Annuler"),
				"btn_ok_text": __("Enregistrer"),
				"calculation_failed": __("Le calcul a échoué..."),
				"tinymce_btn_tooltip": __("Éditeur d’équations"),
				"center_equation_checkbox": __("Centrer l’équation"),
				"virtual_keyboard_basic_tab_tooltip": __("Accueil"),
				"virtual_keyboard_advanced_tab_tooltip": __("Avancé"),
				"virtual_keyboard_formulae_tab_tooltip": __("Formules"),
				"virtual_keyboard_greek_tab_tooltip": __("αβγ"),
				"virtual_keyboard_numeric_tab_tooltip": __("123"),
				"virtual_keyboard_alphabetic_tab_tooltip": __("abc"),
				"calculator_title": __("Effectuer le calcul"),
				"insert_result_title": __("Insérer ce résultat dans l’équation")
			},
			mathlive_config: {
				smartMode: false,
			},
		}
	};
	tinymce.init($.extend(params, extraparams));
}

async function displayNewProtocol (id_ld, edition) {
	return $.ajax({
		type: "POST",
		url: "/labdoc/"+id_ld+"/getCopexTemplate",
		datatype: "json",
		data: {edition:edition},
		error: function(){
			alertGently(__("Un labdoc protocole n'a pas pu être chargé - Vérifiez votre connexion réseau"), 'danger');
		},
		success:function(htmlData){
			$(`#labdoc_content_${id_ld}`).html(htmlData);
		}
	});
}

/**
 * display the content of a text labdoc
 *
 * @param {Number} id_ld
 * @param {Boolean} edition
 * @param {Boolean} is_assignment is the text to edit a labdoc's assignment instead of a labdoc
 * @return {Boolean}
 */
function displayText(id_ld, edition, is_assignment = false) {
	const suffix = is_assignment ? '_assignment' : '';
	const content_prefix = is_assignment ? 'assignment' : 'content';
	const id_content = '#labdoc_' + content_prefix + '_' + id_ld;
	const ld_content = getLDContentFromLocalHistory(id_ld, 0, is_assignment);
	const id_ld_text = 'ld_txt_' + id_ld + suffix;
	if (edition) {
		let ld_text_content = "<div id='" + id_ld_text + "' name='" + id_ld_text + "'>";
		if (ld_content) {
			ld_text_content += ld_content;
		}
		ld_text_content += "</div>";
		ld_text_content += "<div class='sizing-bar sizing-bar-vertical ui-resizable-s ui-resizable-n'> </div>";
		// upload of images in TinyMCE
		ld_text_content += '<div class="tmce_image_upload" style="display:none;">';
		ld_text_content += '	<form method="post" enctype="multipart/form-data" action="">';
		ld_text_content += '		<input name="image" type="file" style="width:0;height:0;overflow:hidden;">';
		ld_text_content += '		<input type="hidden" name="req" value="getCodeImage">';
		ld_text_content += '	</form>';
		ld_text_content += '</div>';
		const tinymce_cb = is_assignment ? assignmentPostInitCb : ldTextPostInitCb;
		$(id_content).html(ld_text_content);
		requestAnimationFrame(() => requestAnimationFrame(function(){ // wait for the DOM being updated
			typesetHtmlWithFormulas("#" + id_ld_text);
			var extra_params = {}
			if (is_assignment) {
				extra_params.height = '35ex'; // 10 for text, 15 for borders and menu
			}
			const file_picker_callback = is_assignment ? ldTextAssignmentFilePickerCb : ldTextFilePickerCb;
			init_tinyMCE('#' + id_ld_text, tinymce_cb, file_picker_callback, extra_params);
			// resize function
			let resizable_div = $(id_content);
			let sizing_bar = $(resizable_div).find (".sizing-bar");
			$(resizable_div).resizable({
				handles: {'s,n': $(sizing_bar)},
				minHeight: 150
			});
		}));
	} else {
		if (ld_content) {
			updateHtmlWithFormulas(id_content, ld_content);
		} else if (!is_assignment) {
			$(id_content).html("<p>"+__("Le texte est vide, cliquez sur modifier pour ajouter du texte")+"</p>");
		}
		// close TinyMCE if the LD was previously edited
		if (window.tinymce) {
			let activeTinyMCE = window.tinymce.get(id_ld_text);
			if (activeTinyMCE) {
				activeTinyMCE.remove();
			}
		}
	}
}

/**
 * Object that manage labdoc code display
 */

var ldCodeManager = function(id_ld, edition, rw_or_ro){

	/**
	 * Sets the url of the iframe that contains JupyterLite.
	 * @return {string}: the html code of the iframe.
	 */
	function prepareIframe(){
		let iframe_class = 'ld_code_iframe';
		let iframe_id = iframe_class + '_' + id_ld.toString();
		iframe_class += ' ' + rw_or_ro;
		let new_file = 'files/new.ipynb';
		// Avoid cache issue by adding a random token at the end of the url
		let alea = (Math.random() + 1).toString(36).substring(2);
		let kernel = 'null';
		if (edition) {
			kernel = 'python';
		}
		let iframe_src = window.location.origin
			+ '/jupyter/' + rw_or_ro
			+ '/lite_' + rw_or_ro
			+ '/retro/notebooks/index.html?path=' + new_file
			+ '&kernel=' + kernel
			+ '&id_ld=' + id_ld
			+ '&alea=' + alea;
		let iframe_name = 'jupyter_iframe_' + id_ld.toString();
		let iframe =  `<iframe name="${iframe_name}" src="${iframe_src}" class="${iframe_class}" id="${iframe_id}" style="border:none"></iframe>`;
		let resizable_handle = `<div class='sizing-bar sizing-bar-vertical ui-resizable-s ui-resizable-n'/>`;
		if (rw_or_ro === "ro"){
			return iframe;
		} else {
			return `<div class='ld_code_iframe_wrapper'>` + iframe + resizable_handle + `</div>`;
		}
	}

	/**
	 * Prevent iframe from stealing the title focus
	 */
	function preventIframeFromStealingTitleFocus() {
		let title_input = document.getElementById('ld_name_' + id_ld.toString());
		title_input.addEventListener('blur', (event) => {
			setTimeout(function() {
				title_input.focus();
			} , 0);
		}, {once: true});
	}

	/**
	 * Put iframe to the DOM and set it resizable (only in rw mode)
	 * @param {string} ld_code_content
	 */
	function injectToDomAndSetResizable(ld_code_content){
		let $labdoc_content = $("#labdoc_content_" + id_ld);
		$labdoc_content.html(ld_code_content);
		$(`<i class='fa fa-1x ml-1 fa-spinner fa-pulse' style='margin-left: 34px;'></i>`).insertBefore($labdoc_content);
		if ($labdoc_content.css('display') === 'none') {
			$labdoc_content.parent().find('i.fa-spinner').hide();
		}

		if (rw_or_ro === "rw") {
			let $iframe = $($labdoc_content.find(".ld_code_iframe"));
			let $iframe_wrapper = $iframe.parent();

			let $sizing_bar = $($iframe_wrapper.find(".sizing-bar"));
			$iframe_wrapper.resizable({
				handles: {'s,n': $sizing_bar},
				minHeight: 150,
				start: function (event, ui) {
					$iframe.css('pointer-events', 'none').css('height', '100%');
				},
				stop: function (event, ui) {
					$iframe.css('pointer-events', 'auto');
				}
			});
		}
	}

	/**
	 * Sends a message with a content to JupyterLite through a `postMessage` to JupyterLite iframe.
	 * @param {object} payload: the message content.
	 */
	function sendMessageToJupyterLite(payload){
		payload = {...payload, ...{id_ld: id_ld}};
		window.frames['jupyter_iframe_' + id_ld.toString()].postMessage(payload, '*');
	}

	/**
	 * Finds and returns a list of resources urls from the DOM (two cases: mission edit and report edit).
	 * @return {array}: the list of resources.
	 */
	function getResources(){
		let ld_given_docs = [];
		let ld_student_docs = [];
		let allowed_extensions = [".csv", ".txt"];
		if (typeof global_scope !== 'undefined') {
			ld_given_docs = _getDocsLinksFromDOM("#widg-res-given-docs a.resource-link", allowed_extensions);
			ld_student_docs = _getDocsLinksFromDOM("#widg-res-docs-list a.resource-link", allowed_extensions);
		} else {
			ld_given_docs = _getDocsLinksFromDOM("#bloc_rd .item_list div.rd_line", allowed_extensions);
		}
		return ld_given_docs.concat(ld_student_docs);
	}

	/**
	 * Finds and returns an arrays of urls given a DOM selector and an array of allowed extensions.
	 * @param selector: jQuery selector.
	 * @param allowed_extensions: array of allowed extensions like ".csv".
	 * @return {array}: the list of urls.
	 * @private
	 */
	function _getDocsLinksFromDOM(selector, allowed_extensions) {
		return $(selector)
			.map(function () {
				return {
					name: $(this).find("span span").text().trim(),
					url: window.location.origin + ($(this).attr('href') || $(this).attr('data-rd-href'))
				};
			}).get()
			.filter(x => allowed_extensions
				.map(ex => x.url.endsWith(ex))
				.reduce((current, carry) => current || carry, false)
			);
	}

	/**
	 * Builds two snippets of python code for importing and exporting data.
	 * @param ld_resources: an array of url to fetch.
	 * @return {(string)[]}: import and export code.
	 */
	function setImportExportCode(ld_resources){
		let ld_export_code = `data = # ${__("variable contenant vos données")}\n`;
		ld_export_code += `data_type = "pandas" # ${__("format de vos données")}: "pandas", "numpy", "list"\n`
		ld_export_code += `export_file(data=data, data_type=data_type, filename="data.csv")`;
		let ld_import_code = "";
		if (ld_resources.length > 0) {
			ld_import_code += `sep = ";" # ${__("séparateur de colonnes")}\n`
			ld_import_code += `data_format = "pandas" # ${__("format souhaité")}: "pandas", "numpy", "txt"\n`
			if (ld_resources.length == 1) {
				ld_import_code += `url = "${ld_resources[0].url}"\n`
				ld_import_code += `data = import_file(url=url, sep=sep, data_format=data_format)`;
			} else {
				ld_import_code += ld_resources.map((x, i) => `url_${(i + 1).toString()} = "${x.url}"\n`).join('');
				ld_import_code += ld_resources.map((x, i) => `data_${(i + 1).toString()} = import_file(url=url_${(i + 1).toString()}, sep=sep, data_format=data_format)`).join('\n');
			}
		} else {
			ld_import_code += '# ' + __("Aucune donnée à importer. Avant de les importer, les données doivent être déposées en tant que document dans l'outil 'Ressources'.");
		}
		return [ld_import_code, ld_export_code];
	}

	/**
	 * Builds a python snippets to be inserted in the first hidden cell of JupyterLite notebook.
	 * It exposes two functions: one for importing data and the other one for exporting data.
	 * /!\ Be careful not to break the backwards compatibility!
	 * @return {string}: the snippet code.
	 */
	function setUtilsCellCode(){
		return `import micropip
await micropip.install(["requests==2.31.0"])
import requests
import pandas as pd
import numpy as np
import base64
import pyodide_http
from IPython.display import HTML
import csv
import io

def export_file(data="", data_type="pandas", filename="data.csv", title="${__("Télécharger le fichier csv")}", **kwargs):
    try:
        if data_type == "pandas":
            return csv_export(csv_data=data.to_csv(**kwargs), title=title, filename=filename)
        elif data_type == "numpy":
            csv_data = generate_csv_string_from_data(data.tolist(**kwargs), filename, **kwargs)
            return csv_export(csv_data=csv_data, title=title, filename=filename)
        else:
            if not isinstance(data[0], list):
                data = [data]
            csv_data = generate_csv_string_from_data(data, filename, **kwargs)
            return csv_export(csv_data=csv_data, title=title, filename=filename)
    except:
        print("${__("Export impossible.")}")
        pass

def generate_csv_string_from_data(data, filename, **kwargs):
    output = io.StringIO()
    writer = csv.writer(output, **kwargs)
    writer.writerows(data)
    return output.getvalue()
        
def csv_export(csv_data="", title="", filename=""):
    b64 = base64.b64encode(csv_data.encode())
    payload = b64.decode()
    html = '<a class="bp3-button bp3-minimal"'
    html += '  style="border: 1px solid #eaeaea; color: black;"'
    html += '  download="{filename}"'
    html += '  href="data:text/csv;base64,{payload}"'
    html += '  target="_blank">'
    html += '{title}'
    html += '</a>'
    html = html.format(payload=payload, title=title, filename=filename)
    return HTML(html)

def import_file(url="", sep=";", data_format="pandas", **kwargs):
    pyodide_http.patch_all()
    try:
        if data_format == "pandas":
            return pandas_import(url=url, sep=sep, **kwargs)
        elif data_format == "numpy":
            return numpy_import(url=url, sep=sep, **kwargs)
        else:
            return txt_import(url=url, **kwargs)
    except:
        print("${__("Import impossible dans le format demandé.")}")
        return txt_import(url=url, **kwargs)
        pass

def pandas_import(url="", sep=";", **kwargs):
    return pd.read_csv(url, sep=sep, **kwargs)

def numpy_import(url="", sep=";", **kwargs):
    text = requests.get(url).text
    c = io.StringIO(text)
    return np.genfromtxt(c, delimiter=sep, **kwargs)

def txt_import(url="", **kwargs):
    text = requests.get(url).text
    return text`;
	}

	/**
	 * Set internationalized tooltips.
	 * @return {object}: a key-value list .
	 */
	function setTooltips(){
		let tooltips = {
			clearButton: __('Effacer les sorties'),
			importButton: __('Importer des données'),
			exportButton: __('Exporter des données'),
			jupyterLiteButton: __('Visiter le site de JupyterLite')
		}
		return tooltips;
	}
	/**
	 * Sets a callback that waits for a signal emitted by JupyterLite instance and initialize the notebook.
	 * @param payload: the data used to initialize the JupyterLite notebook.
	 * @return {initNotebookCallback}: a callback to be added to an eventListener.
	 */
	function setInitCallback(payload){
		const initNotebookCallback = (event) => {
			if (event.data.type === 'init_' + rw_or_ro + '_notebook' && event.data.id_ld == id_ld) {
				let $labdoc_content = $("#labdoc_content_" + id_ld);
				$labdoc_content.parent().find('i.fa-spinner').show();
				window.removeEventListener('message', initNotebookCallback);
				console.log('lnb -> lnb_' + rw_or_ro + '_init: init_' + rw_or_ro + '_notebook [id_labdoc=' + id_ld.toString() + ']');
				sendMessageToJupyterLite(payload);
				const iframe = document.getElementById('ld_code_iframe_' + id_ld.toString());
				// Display if rw mode
				if (rw_or_ro === 'rw') {
					$labdoc_content.parent().find("i.fa-spinner").hide();
					iframe.style.border = '1px solid #ccc';
				}
				// Catch iframe errors
				iframe.contentWindow.onerror = function(msg, file, line, col, error) {
					if ((msg.startsWith('Error: No kernel') || error.message.startsWith('No kernel')) && !edition){
						// Regular case: the kernel is shut down in ro mode
						return true
					} else {
						logError(
							"Labdoc code error",
							'danger',
							["id_ld = " + id_ld.toString(), msg, file, line, col, error]
						);
						console.error(msg, file, line, col, error);
					}
				};
			}
		}
		return initNotebookCallback;
	}

	/**
	 * Sets a callback that waits for a signal emitted by JupyterLite instance and display the toolbar.
	 * JupyterLite sometimes hides the menu bar. When it occurs, a message is sent to LabNBook which resizes the iframe
	 * to make it appear (it's a little hack).
	 * @return {toolbarHiddenCallback}: a callback to be added to an eventListener.
	 */
	function setHiddenToolbarCallback(){
		const toolbarHiddenCallback = (event) => {
			if (event.data.type === 'toolbar_is_hidden' && event.data.id_ld == id_ld) {
				window.removeEventListener('message', toolbarHiddenCallback);
				console.log('lnb -> lnb_rw_init: toolbar_is_hidden [id_labdoc=' + id_ld.toString() + ']');
				$('iframe#ld_code_iframe_' + id_ld.toString()).width("99.9%");
			}
		}
		return toolbarHiddenCallback;
	}

	/**
	 * Sets a callback that waits for a signal emitted by JupyterLite instance and resize the JupyterLite iframe.
	 * @return {resizeIframeCallback}: a callback to be added to an eventListener.
	 */
	function setResizeIframeCallback(){
		const resizeIframeCallback = (event) => {
			if (event.data.type === 'resize_iframe' && event.data.id_ld == id_ld) {
				window.removeEventListener('message', resizeIframeCallback);
				console.log('lnb -> lnb_ro_init: resize_iframe [id_labdoc=' + id_ld.toString() + ']');
				let $iframe = $('iframe#ld_code_iframe_' + id_ld.toString());
				let $labdoc_content = $("#labdoc_content_" + id_ld);
				$labdoc_content.parent().find("i.fa-spinner").hide();
				$iframe.css('height', event.data.height + 10).css('border', '1px solid #ccc');
				// If the notebook ships with asynchronous instructions, we need to reload the height
				// (it seems that the await methods used in lnb_ro_init doesn't handle this case) 
				setTimeout(function(){
					let height = $iframe.contents().find('.jp-NotebookPanel-notebook')[0].scrollHeight;
					if (height != event.data.height + 10){
						$iframe.css('height', height + 10);
					}
				}, 100);
			}
		}
		return resizeIframeCallback;
	}

	/**
	 * Overall description: look at the place where this method is called.
	 * Technical description: creates a promise whose resolution stands inside the callback of an eventListener.
	 * When JupyterLite instance will send a message of type `save_notebook_content`, it will trigger the following
	 * callback which would resolve the promise.
	 * @return {function(): Promise<unknown>}: the promise.
	 */
	function setSavePromise(){
		let iframeResponsePromise = () => new Promise((resolve, reject) => {
			const saveNotebookCallback = this.setSaveNotebookCallback(resolve);
			window.addEventListener('message', saveNotebookCallback);
		});
		return iframeResponsePromise;
	}

	function setSaveNotebookCallback (resolve) {
		const saveNotebookCallback = (event) => {
			if (event.data.type === 'save_notebook_content' && event.data.id_ld == id_ld) {
				window.removeEventListener('message', saveNotebookCallback);
				console.log('lnb: saving [id_labdoc=' + id_ld.toString() + ']');
				resolve(event.data.ld_content);
			}
		}
		return saveNotebookCallback;
	}

	/**
	 * Create a promise that reject after five seconds.
	 * @return {function(): Promise<unknown>}: the promise.
	 */
	function setTimeOutPromise(){
		let timeOutPromise = () => new Promise(function(resolve, reject) {
			setTimeout(function() {
				reject(new Error('Notebook code : timeout error while saving'));
			}, 5000);
		});
		return timeOutPromise;
	}

	/**
	 * Detection of changes in the resource list (mission edit + report).
	 * In this case, send a message to JupyterLite.
	 */
	function observeResourcesUpdate() {
		$('#bloc_rd .item_list, #widg-res-given-docs, #widg-res-docs-list').each(function () {
			const targetNode = this;
			const options = {childList: true};
			const mutationObserverCallback = function (mutationList, observer) {
				for (const mutation of mutationList) {
					let [ld_import_code, ld_export_code] = setImportExportCode(getResources());
					var payload = {type: 'update_rw_resources', ld_import_code: ld_import_code};
					sendMessageToJupyterLite(payload);
				}
			};
			const observer = new MutationObserver(mutationObserverCallback);
			observer.observe(targetNode, options);
		});
	}

	function setContentLoadedCallback () {
		const contentLoadedCallback = (event) => { 
			if (event.data.type === 'ld_content_loaded' && event.data.id_ld == id_ld) {
				window.removeEventListener('message', contentLoadedCallback);
				console.log('lnb: ld_content_loaded [id_labdoc=' + id_ld.toString() + ']');
				// Display the iframe with loaded content
				document.getElementById("labdoc_content_" + id_ld).style.visibility = 'visible';
			}
		}
		return contentLoadedCallback;
	}

	return {
		sendMessageToJupyterLite,
		prepareIframe,
		preventIframeFromStealingTitleFocus,
		injectToDomAndSetResizable,
		getResources,
		setImportExportCode,
		setUtilsCellCode: setUtilsCellCode,
		setTooltips: setTooltips,
		setInitCallback,
		setHiddenToolbarCallback,
		setResizeIframeCallback,
		setSavePromise,
		setTimeOutPromise,
		observeResourcesUpdate,
		setContentLoadedCallback,
		setSaveNotebookCallback
	}
};
/**
 * display the content of a code labdoc
 *
 * @param {Number} id_ld
 * @param {Boolean} edition
 * @param {Boolean} reload_iframe
 * @return {Boolean}
 */
function displayCode(id_ld, edition, reload_iframe) {
	let rw_or_ro = edition ? 'rw' : 'ro';
	let ld_content = getLDContentFromLocalHistory(id_ld);
	let ld_code_manager = ldCodeManager(id_ld, edition, rw_or_ro);

	if (!edition) {
		// Hide iframe
		document.getElementById("labdoc_content_" + id_ld).style.visibility = 'hidden';
		const contentLoadedCallback = ld_code_manager.setContentLoadedCallback();
		window.addEventListener('message', contentLoadedCallback);
	}

	if (!reload_iframe && edition) {
		// Case of undo-redo in read-and-write mode
		ld_code_manager.sendMessageToJupyterLite({ type: 'undo_redo_notebook', ld_content: ld_content });
		console.log('lnb -> lnb_rw_undoredo: undo_redo_notebook [id_labdoc=' + id_ld.toString() + ']');
	} else if (!reload_iframe && !edition) {
		// Case of synchronize in read-only mode
		// Notify JupyterLite
		ld_code_manager.sendMessageToJupyterLite({ type: 'synchronize_notebook', ld_content: ld_content });
		console.log('lnb -> lnb_ro_synchronize: synchronize_notebook [id_labdoc=' + id_ld.toString() + ']');
	} else {
		// This case covers both ro and rw mode but reload the iframe containing JupyterLite
		// Prepare iframe
		let ld_code_content = ld_code_manager.prepareIframe();
		// Put iframe to the DOM and set it resizable
		ld_code_manager.injectToDomAndSetResizable(ld_code_content);
		// Get resources from DOM
		let ld_resources = ld_code_manager.getResources();
		// Set python functions to import/export data
		let [ld_import_code, ld_export_code] = ld_code_manager.setImportExportCode(ld_resources);
		// Set utils python functions (to be inserted in the first cell)
		let ld_utils_cell = ld_code_manager.setUtilsCellCode();
		// Set tooltip translation
		let ld_tooltip = ld_code_manager.setTooltips();
		// Create an event listener that:
		//   1. Waits an init signal from JupyterLite iframe 
		//   2. Sends an init content
		const initNotebookCallback = ld_code_manager.setInitCallback(
			{ type: 'init_' + rw_or_ro + '_notebook', ld_content: ld_content, ld_utils_cell: ld_utils_cell,
			ld_import_cell: ld_import_code, ld_export_cell: ld_export_code, ld_tooltip: ld_tooltip }
		);
		window.addEventListener('message', initNotebookCallback);
		if (edition) {
			// Prevent iframe from stealing focus from the title
			ld_code_manager.preventIframeFromStealingTitleFocus();
			// Show toolbar if JupyterLite doesn't find it (case of a rw instance)
			const toolbarHiddenCallback = ld_code_manager.setHiddenToolbarCallback();
			window.addEventListener('message', toolbarHiddenCallback);
			// Observe a change in a list of resources and send a message to JupyterLite in case of update
			ld_code_manager.observeResourcesUpdate();
		} else {
			// Resize iframe if JupyterLite ordered it (case of a ro instance)
			const resizeIframeCallback = ld_code_manager.setResizeIframeCallback();
			window.addEventListener('message', resizeIframeCallback);
		}
	}
}


/* ***********************************************
           Main functions for LD display
 *************************************************/

/**
 * update the header and content of an existing LD from the DB
 * @param {int} id_ld
 * @param {boolean} edition
 * @param {string} extend : 0|1|"unchanged"
 * @param {boolean} scroll scroll the page to display the whole LD
 * @param {boolean} new_ld true if it is a LD creation
 */
function getAndDisplayLD(id_ld, edition = 0, extend = 1, scroll = 1, new_ld = 0) {
	let scope;
	if (typeof global_scope !== "undefined") {
		scope = global_scope;
	} else {
		scope = "default";
	}
	$.ajax({
		type: "POST",
		url: "/labdoc/"+id_ld+"/getLDHeader",
		data: {
			edition: edition,
			scope: scope
		},
		error: function(){
			alertGently(__("Un labdoc n'a pu être affiché. Vérifiez votre connexion réseau."), 'danger');
		},
		success:function(htmlData){
			displayLDHeader(id_ld, htmlData, edition, extend, scroll, new_ld);
		}
	});
}

/**
 * Display the LD header in DOM and then call for a refesh of the content from DB
 * @param {int} id_ld
 * @param {string} htmlHeader
 * @param {boolean} edition
 * @param {string} extend : 0|1|"unchanged"
 * @param {boolean} scroll scroll the page to display the whole LD
 * @param {boolean} new_ld true if it is a LD creation
 */
function displayLDHeader(id_ld, htmlHeader, edition = 0, extend= 1, scroll = 1, new_ld = 0) {
	if (global_tab_ld[id_ld]) {
		global_tab_ld[id_ld].loading = true;
	}
	blockLDHeight(id_ld);
	$('#labdoc_'+id_ld).html(htmlHeader);
	requestAnimationFrame(() => requestAnimationFrame(function(){ // wait for the DOM being updated
		if (edition) {
			window.global_edited_ld = id_ld;
			$('#ld_name_' + id_ld).trigger("focus");
		}
		let store_labdoc = Alpine.store('reportLabdocs').getLd(id_ld);
		if(store_labdoc) {
			store_labdoc.is_new_ld = new_ld;
		}
		getAndDisplayLDContent(id_ld, edition, extend, scroll);
	}));
}

/**
 * Update the content of a LD with a call to the DB
 * @param {int} id_ld
 * @param {boolean} edition
 * @param {boolean} extend : 0|1|"unchanged"
 * @param {boolean} scroll scroll the page to display the whole LD
 * @param {boolean} teacher_mode
 * @param {boolean} showDownloadButton
 */
function getAndDisplayLDContent(id_ld, edition= 0, extend= 1, scroll = 1, teacher_mode = 0, showDownloadButton = 0) {
	$.ajax({
		type: "GET",
		url: "/labdoc/"+id_ld+"/getData",
		datatype: "json",
		cache: false,
		error: function(){
			alertGently(__("Un labdoc n'a pu être affiché. Vérifiez votre connexion réseau."), 'danger');
		},
		success: function(ld){
			if (!global_tab_ld[id_ld] || global_tab_ld[id_ld].ld_type != ld.type_labdoc) {
				// init global_tab_ld : the main array containing the LD as objects
				global_tab_ld[id_ld] = { ld_type: ld.type_labdoc };
			}
			global_tab_ld[id_ld].ld_history = [ld.labdoc_data]; // array of ld_content for local versioning {string}
			global_tab_ld[id_ld].ld_assignment_history = [ld.assignment]; // array of assignment for local versioning {string}
			global_tab_ld[id_ld].ld_initial_name = ld.name; // initial name of the LD {string}
			global_tab_ld[id_ld].current_i = 0; // current version of the LD {int}
			global_tab_ld[id_ld].current_j = 0; // current version of the LD assignment {int}
			global_tab_ld[id_ld].send_in_db = false; // determine if the LD will be sent in DB on the next synchro
			global_tab_ld[id_ld].recovering = false;
			if (ld.type_labdoc === 'dataset'){
				global_tab_ld[id_ld].auto_fit = ld.auto_fit;
			}
			// Gestion de l'import & export du XML si procedure
			if (showDownloadButton) { showProcedureDownloadButton(id_ld); }
			// global_tab_ld[id_ld].loading is persistent
			// other attributes specific for datasets and procedure LD are persistent too
			displayLDContent(id_ld, edition, extend, 0, scroll, teacher_mode);
		}
	});
}

function showProcedureDownloadButton (id_ld) {
	if (isProcedure(id_ld, 'xml')) {
		$("#ld_edit_proc").show() ;
		$("#ld_edit_upload").off() ;
		$("#ld_edit_upload").click(function(){
			setUploadForm('xml_proc',id_ld);
		});
		$("#ld_edit_download").off() ;
		$("#ld_edit_download").click(function(){
			downloadLD(id_ld);
		});
	} else { $("#ld_edit_proc").hide() ; }
}
/**
 * Display the content of a labdoc into #labdoc_content_{id}. The content is taken from global_tab_ld
 * @param {int} id_ld
 * @param {boolean} edition
 * @param {string} extend : 0|1|"unchanged"
 * @param {int} rel_pos : -1|0|1
 * @param {boolean} scroll
 * @param {boolean} teacher_mode used for the multi-layering mode of Zwibbler
 * @param {boolean} refresh_annotations (default true) used to prevent annotator from refreshing annotations after showing diffs
 * @param {boolean} save_locally (default true) a false value would prevent the local saving (case of a teacher who wants to test in follow mode)
 * @param {boolean} reload_iframe (default true) a false value would avoid the refresh of an iframe containing the labdoc
 */
async function displayLDContent(id_ld, edition= false, extend = "unchanged", rel_pos = 0, scroll = false, teacher_mode = false, refresh_annotations= true, save_locally = true, reload_iframe = true) {
	global_tab_ld[id_ld].loading = true;

	// pre-treatment
	let ld_type = window.global_tab_ld[id_ld].ld_type;
	let current_i = global_tab_ld[id_ld].current_i;
	let index = current_i + rel_pos;
	if ((isProcedure(id_ld, 'xml') || ld_type == "dataset") && !createXMLLD(id_ld, index)) { // try to create the XML version of the LD
		return false;
	}
	if (rel_pos) {
		let ld_content = getLDContentFromLocalHistory(id_ld, rel_pos);
		if (ld_content === false) {
			return false;
		}
		if (ld_type == "text" || (ld_type == "code" && save_locally) || isProcedure(id_ld, 'json')) { // text and code LD need to be saved before navigating in history
			await saveLocalLDVersion(id_ld);
		}
		global_tab_ld[id_ld].current_i = index;
		global_tab_ld[id_ld].send_in_db = true;
	}

	// display the content
	blockLDHeight(id_ld);
	switch (ld_type) {
		case "procedure":
			if (isProcedure(id_ld, 'json')) {
				await displayNewProtocol(id_ld, edition);
			} else {
				displayProtocol(id_ld, edition);
			}
			break;
		case "dataset":
			// the LD MUST be visible for updating the graph
			let ld_hidden = $("#labdoc_content_" + id_ld).is(":hidden");
			if (ld_hidden) {
				$('#labdoc_content_' + id_ld).show();
			}
			displayDataSet(id_ld, edition);
			if (ld_hidden) {
				$('#labdoc_content_' + id_ld).hide();
			}
			break;
		case "drawing":
			displayDrawing(id_ld, edition, teacher_mode);
			break;
		case "text":
			displayText(id_ld, edition);
			break ;
		case "code":
			displayCode(id_ld, edition, reload_iframe);
			break ;
	}
	if (edition || (window.report_env != undefined && window.report_env.scope_js === 'pdf' && window.report_env.print_assignment)) {
		displayText(id_ld, false, true);
		const show_ld_assignment = ($('#labdoc_assignment_' + id_ld).text().trim() != "");
		$('#labdoc_assignment_container_' + id_ld).toggle(show_ld_assignment);
	}

	// post-treatment
	requestAnimationFrame(() => requestAnimationFrame(function(){ // wait for the DOM being updated
		global_tab_ld[id_ld].loading = false;
		$("#labdoc_content_" + id_ld).removeClass('labdoc-content-loading');
		$("#labdoc_" + id_ld).css("height",'auto'); // free the height of the LD
		if (extend != "unchanged") {
			if (edition || extend) {
				$('#labdoc_content_'+id_ld).show();
			} else {
				$('#labdoc_content_' + id_ld).hide();
			}
		}
		if (edition) {
			refreshUndoRedoButtons(id_ld);
			window.global_edited_ld = id_ld ;
		}
		else if (window.annotatorActions && refresh_annotations) {
			window.annotatorActions.refreshLDsAnnotations([id_ld]);
		}
		if (scroll) {
			let selector = "#labdoc_" + id_ld; // selector for a report
			if ($(selector).length === 0) { // ugly hack
				selector = "#ld_edit"; // selector for the page "edit mission"
			}
			scrollToDisplay(selector);
		}
	}));
	return true;
}

/**
 * Toggles all labdoc Assignments for the givent report part but the one that
 * is currently openned if any
 */
function toggleAllLdAssignment(id_report_part, show)
{
	const ids = $('#part_content_' + id_report_part +' .labdoc').map((i,e) => $(e).data('id_ld'));
	ids.each(function(k, id_ld) {
		const selector = '#labdoc_assignment_container_' + id_ld;
		if (!$(selector).is(':visible')) {
			// Force reload text before toggling if currently not visible
			// The assignment might be displayed but not visible because
			// it is empty
			displayText(id_ld, false, true);
		}
		if ($('#labdoc_assignment_' + id_ld).text().trim() == "" ) {
			// Hide empty assignements always
			$(selector).hide();
		} else {
			$(selector).toggle(show);
		}
	});
}

// *****************************************************************
//        Management of the Undo Redo with local versions
// *****************************************************************

/**
 * @param {int} rel_pos
 * @param {int} id_ld global_edited_ld par défaut
 * @return {Boolean}
 */
function isLDVersionAvailable(rel_pos, id_ld= -1) {
	rel_pos = Number(rel_pos);
	if (id_ld === -1) {
		if (global_edited_ld) {
			id_ld = global_edited_ld;
		} else {
			return false;
		}
	}
	if (!global_tab_ld[id_ld]) {
		return false;
	}
	let i = Number(global_tab_ld[id_ld].current_i) + rel_pos;
	let ld_content = global_tab_ld[id_ld].ld_history[i];
	return (i >= 0 && i < global_tab_ld[id_ld].ld_history.length && typeof ld_content !== 'undefined');

}
window.addEventListener('procedure-update-undo-redo', (event) => {
	if(event.detail && global_tab_ld[event.detail.id_ld].ld_type === "procedure") {
		let undo = document.getElementById("lnb_undo_" + event.detail.id_ld);
		let redo = document.getElementById("lnb_redo_" + event.detail.id_ld);
		if(undo && redo) {
			if(event.detail.histo_length>1 && event.detail.histo_index > 0) {
				undo.disabled = false;
				undo.classList.remove("disabled");
				undo.onclick = function (evt) {
					evt.stopPropagation();
					window.dispatchEvent(new CustomEvent('prons-undo-redo-action', { detail: { id_ld: event.detail.id_ld, undo:true} }));
				};
			} else {
				undo.disabled = true;
				undo.classList.add("disabled");
			}
			if(event.detail.histo_length>1 && event.detail.histo_index < event.detail.histo_length-1) {
				redo.disabled = false;
				redo.classList.remove("disabled");
				redo.onclick = function (evt) {
					evt.stopPropagation();
					window.dispatchEvent(new CustomEvent('prons-undo-redo-action', { detail: { id_ld: event.detail.id_ld, undo:false} }));
				};
			} else {
				redo.disabled = true;
				redo.classList.add("disabled");
			}
		}
	}
});
/**
 * @param {int} id_ld
 * @param {object} button
 * @param {int} rel_pos
 */
function refreshUndoRedoButton(id_ld, button, rel_pos) {
	if (button.length !== 0) {
		button.off();
		if (isLDVersionAvailable(rel_pos)) {
			button.removeClass("disabled");
			button.click(function () {
				if (global_tab_ld[id_ld].ld_type == "code"){
					displayLDContent(id_ld,true,1, rel_pos, 0, 0, true, true, false);
				} else{
					displayLDContent(id_ld,true,1, rel_pos);
				}
			});
		} else {
			button.addClass("disabled");
		}
	}
}

/**
 * refresh the undo & redo buttons
 * @param id_ld
 */
function refreshUndoRedoButtons(id_ld) {
	let redo = $("#lnb_redo_" + id_ld);
	refreshUndoRedoButton(id_ld, redo, 1);
	let undo = $("#lnb_undo_" + id_ld);
	refreshUndoRedoButton(id_ld, undo, -1);
}

// *****************************************************************
//        Save a local version of a LD
// *****************************************************************

/**
 * Récupération du nom du LD : peut être obtenu depuis 2 champs différents
 *    un input si le LD est en édition
 *    un span sinon
 * @param {int} id_ld
 * @return {string}
 */
function getLDNameFromDom(id_ld) {
	let name;
	if ($("#ld_name_" + id_ld).hasClass("editable_ld_name")) {
		name = $("#ld_name_" + id_ld).val();
	} else {
		name = $('#ld_name_'+id_ld).text();
	}
	if (typeof name !== 'string') {
		logError("getLDNameFromDom could not find the name of LD " + id_ld, "", "");
		return "";
	}
	if (name.length > 128) {
		name = name.slice(0, 125) + '...';
		alertGently(__("Le titre du labdoc a été coupé (128 caractères maximum)."), "warning");
	}
	return name.trim();
}

function updateContentHistory(ld_obj, current_i, history_array, new_content) {
	if (ld_content !== ld_obj.ld_history[current_i]) { // update the history stack only if the version is different
		if (current_i + 1 >= global_ld_max_local_versions) {
			ld_obj.ld_history.splice(0,1);
		} else {
			current_i++;
		}
		ld_obj.ld_history[current_i] = ld_content;
		ld_obj.current_i = current_i;
		if (ld_content || force_empty) { // update in DB only if not empty
			ld_obj.send_in_db = true ;
		}
		refreshUndoRedoButtons(id_ld);
		return true;
	} else {
		return false;
	}
}


/**
 * Save a version of the LD in global_tab_ld (but NOT in DB)
 * @param {int} id_ld
 * @param {boolean} force_empty
 * @returns {boolean} true if saved, false if not
 */
async function saveLocalLDVersion(id_ld, force_empty = false) {

	// get the content of the LD
	let ld_obj = global_tab_ld[id_ld];
	let ld_type = ld_obj.ld_type;
	let ld_content, msg;
	try {
		if (ld_type === "text") {
			if(window.tinymce.get('ld_txt_' + id_ld) == null) {
				// Editing a ld text assignment, retrieve content from history
				ld_content = ld_obj.ld_history[ld_obj.current_i];
				msg = "Error while retrieving content from history";
			} else {
				ld_content = window.tinymce.get('ld_txt_' + id_ld).getContent().trim();
				msg = "Error in tinymce.getContent()";
			}
		} else if (ld_type === "drawing") {
			ld_content = zw_contexts[id_ld].save();
			msg = "Error when saving Zwibbler";
		} else if (ld_type === "code") {
			var ld_code_manager = ldCodeManager(id_ld, 1, 'rw');
			// At the request of LabNBook, JupyterLite sends the content of the labdoc code for LabNBook to save.
			// This is done in 4 steps.
			// Step 1: Set a promise of a JupyterLite response to LabNBook request.
			//         It will be *resolved* when JupyterLite would send back a message with the labdoc content.
			let iframeResponsePromise = ld_code_manager.setSavePromise();
			// Step 2: Set a time out promise to limit the waiting time of the previous one.
			//         It will be *rejected* after 5 seconds.
			let timeOutPromise = ld_code_manager.setTimeOutPromise();
			// Step 3: Request JupyterLite for the labdoc content.
			ld_code_manager.sendMessageToJupyterLite({type: 'save_notebook_content'});
			console.log('lnb -> lnb_rw_save: save_notebook_content request [id_labdoc='+id_ld.toString()+']');
			// Step 4: Wait for the fastest promise between the save promise and the timeout promise.
			ld_content = await Promise.race([iframeResponsePromise(), timeOutPromise()]);
			// This line is only reached if JupyterLite returned a response.
			ld_content = JSON.stringify(ld_content);
			if (JSON.parse(ld_content).cells.length == 2 && JSON.parse(ld_content).cells[1].source == ""){
				ld_content = null;
				console.log('lnb -> lnb_rw_save: save_notebook_content received, notebook is empty [id_labdoc='+id_ld.toString()+']');
			} else {
				console.log('lnb -> lnb_rw_save: save_notebook_content received [id_labdoc='+id_ld.toString()+']');
			}
			msg = "Error when saving code";
		} else if (ld_type === "dataset" || isProcedure(id_ld, 'xml')) {
			ld_content = XMLToString($(global_tab_ld[id_ld].ld_content)) ;
			msg = "Error in XMLToString()";
		}  else if(isProcedure(id_ld, 'json')) {
			let copexAlpineResponsePromise = new Promise((resolve, reject) => {
				window.addEventListener('procedure-content-sent', event => {
					if (event.detail.id_ld == id_ld) {
						resolve(event.detail);
					}
				}, { once: true });
			});
			let timeOutPromise = new Promise((resolve, reject) => {
				setTimeout(function() {
					reject(new Error('Procedure : timeout error while saving'));
				}, 2000);
			});
			window.dispatchEvent(new CustomEvent('procedure-content-required', { detail: { id_ld: id_ld } }));
			let promiseResponse = await Promise.race([copexAlpineResponsePromise, timeOutPromise]);
			ld_content = promiseResponse.ld_content;
		}
	} catch (ex) {
		ld_content = null;
		logError(msg, 'danger', "[id_ld=" + id_ld + "] " + ex.message);
	}
	if (window.tool_standalone == undefined) {
		let ld_assignment = null;
		try {
			const assignment_tmce = window.tinymce.get('ld_txt_' + id_ld + '_assignment');
			if (assignment_tmce) {
				ld_assignment = assignment_tmce.getContent().trim();
			} else {
				ld_assignment = getLDContentFromLocalHistory(id_ld, 0, 1);
			}
		} catch (ex) {
			ld_assignment = null;
			logError(msg, 'danger', "[assignment for id_ld=" + id_ld + "] " + ex.message);
		}

		if (ld_assignment != getLDContentFromLocalHistory(id_ld, 0, 1)) {
			// Currently we do not handle assignment history
			ld_obj.ld_assignment_history = [ld_assignment];
		}
	}

	if (typeof ld_content === 'undefined' || ld_content === null) {
		return false;
	}

	let current_i = ld_obj.current_i;
	if (ld_content !== ld_obj.ld_history[current_i]) { // update the history stack only if the version is different
		if (current_i + 1 >= global_ld_max_local_versions) {
			ld_obj.ld_history.splice(0,1);
		} else {
			current_i++;
		}
		ld_obj.ld_history[current_i] = ld_content;
		ld_obj.current_i = current_i;
		if (ld_content || force_empty) { // update in DB only if not empty
			ld_obj.send_in_db = true ;
		}
		refreshUndoRedoButtons(id_ld);
		return true;
	} else {
		return false;
	}
}

// *****************************************************************
//          Save LD in DB
// *****************************************************************
/**
 *
 * @param {boolean} trace trace the modify_ld
 */
function autoUpdateLDinDB(trace = true) {
	if (global_edited_ld && global_tab_ld[global_edited_ld] && global_tab_ld[global_edited_ld].send_in_db) {
		updateLDinDB(global_edited_ld, trace) ;
	}
}

/**
 * Remove formulas display tags from a text LD
 * @param {string} ld_content
 * @returns {string}
 */
function removeEquationsDisplays(ld_content) {
	// console.log(ld_content);
	$("html").append('<div id="process_xml"></div>');
	let xml_div = $("#process_xml");
	$(xml_div).html(ld_content);
	$(xml_div).find("span.katex").remove();
	ld_content = $(xml_div).html();
	$(xml_div).remove();
	return ld_content;
}

/**
 * Send the LD name and content for update in DB. Used during the synchro in edition mode
 * @param {int} id_ld
 * @param {boolean} trace trace le modify_ld
 * @return {boolean}
 */
function updateLDinDB(id_ld, trace= true) {
	let ld_name = getLDNameFromDom(id_ld);
	let ld_content = getLDContentFromLocalHistory(id_ld);
	if (ld_content === false) {
		return false;
	}
	if (global_tab_ld[id_ld].ld_type === "text") {
		ld_content = removeEquationsDisplays(ld_content);
	}
	global_tab_ld[id_ld].send_in_db = false;
	if (parseInt(id_ld) === 0) {
		alertGently(__("LabNBook rencontre un problème de sauvegarde. Enregistrez tous les labdocs en cours d’édition."), "danger", 10000);
	} else {
		$.ajax({
			type: "POST",
			url: "/labdoc/" + id_ld + "/update",
			data: {
				name: ld_name,
				content: ld_content,
				trace: trace ? 1 : 0
			},
			error: function () {
				alertGently(__("Les dernières modifications apportées au labdoc n'ont pas été enregistrées - Vérifiez votre connexion réseau"), "warning");
				global_tab_ld[id_ld].send_in_db = true;
			},
			success: function (data) {
				if (data && typeof data === 'object') {
					console.log("LD saved in DB", id_ld);
					return true;
				} else {
					alertGently(__("Problème de format du labdoc ") + id_ld + " : " + data, "warning");
					global_tab_ld[id_ld].send_in_db = true;
				}
			}
		});
	}
	return false;
}

/**
 * Object that manage diffs
 */
function labdocDiff(id_labdoc, last_id_version_reviewed_on_init){
	return {
		labdocData: null,
		diffError: false,
		displayDiff:false,
		loading: false,
		id_labdoc: id_labdoc,
		id_version: last_id_version_reviewed_on_init,
		
		/**
		 * Show the diffs in each labdoc
		 */
		show(){
			let validation = this.showLdDiff();
			if (validation === false){
				alertGently(__("Une erreur est survenue lors de l'affichage des ajouts réalisés par les étudiants."));
			} else {
				// /!\ Order matters
				// Encapsulation in case of raw text, useful to draw merged border
				let toWrap = $("ins.diffins:not(:has(*))")
					.add("ins.diffmod:not(:has(*))")
					// Encapsulation in case of a link containing a span of raw text
					.add("ins.diffins:not(>a>span:has(*))")
					.add("ins.diffmod:not(>a>span:has(*))")
					.wrap("<b class='diffins'></b>");
				// If an img is directly inside del.diffhref, it means that just the href changed
				// Hide the old img and highlight the new one
				$("del.diffa.diffhref:has(>a>img)").hide();
				$("ins.diffa.diffhref:has(>a>img)").addClass("diffimg");
				// If an image is alone in a del tag, wrap the image
				$("del.diffmod img").wrap("<del class='diffmod diffimg diffsrc'></del>");
				$("del.diffdel img").wrap("<del class='diffmod diffimg diffsrc'></del>");
				// If an image is inside a ins.diffins or ins.diffmod, add diffmod.diffimg class to ins tag
				$("ins.diffins:has(>img)").addClass('diffmod diffimg');
				$("ins.diffmod:has(>img)").addClass('diffmod diffimg');
				// If an equation is displayed as a block, remove the outline
				$("b.diffins:has(>ins>span.ew_ltxformula>span.katex-display)").css('outline', 'none');
				// If a cell is empty, fill with a space
				$("table td.ins span.empty").text(" ");
				$("table td.del span.empty").parent().hide();
				// Vertical margin on inserted cells
				$("table td:has(ins.diffins)").css('height', '2em');
				$("table td:has(ins.diffmod)").css('height', '2em');
	
				showHideLD(this.id_labdoc, 'show');
			}
		},
	
		/**
		 * Log teacher_display_diff_trace
		 */
		log_trace() {
			$.ajax({
				type: "POST",
				url: "/labdoc/" + this.id_labdoc + "/logDisplayDiffsTrace"
			})
		},

		/**
		 * Show the diffs
		 * @return {boolean}
		 */
		showLdDiff() {
			if (global_tab_ld[this.id_labdoc].ld_type === 'text'){
				updateHtmlWithFormulas('#labdoc_content_' + this.id_labdoc, this.labdocData['diff_content']);
				return true;
			} else {
				return false;
			}
		},
		
		/**
		 * Hide the diffs, do not refresh annotations
		 */
		hideLdDiff() {
			/* When hiding differences, diff.hide() and window.annotatorActions.showAnnotatorAdder() are executed.
			The later command runs window.annotatorActions.refreshAllAnnotations() (mandatory since it loads annotations from the DB).
			diff.hide() runs the following commands which refresh annotations too. We created a boolean that prevents refreshing this way.
			*/
			displayLDContent(this.id_labdoc, false, "unchanged", 0, false, false, false);
			window.annotatorActions.refreshAllAnnotations();
		},
		
		/**
		 *  load then hide or show students modifications
		 */
		loadOrtoggleModifications(reload=false) {
			if (!this.displayDiff || reload){
				this.loading = true;
				const stored_ld = Alpine.store('reportLabdocs').getLd(id_labdoc);
				$.ajax({
					type: "POST",
					url: "/labdoc/" + this.id_labdoc + "/getDiff",
					data: {
						ld_to_diff: {
							last_ld_id_version_reviewed: this.id_version || null,
							current_ld_id_version: stored_ld?stored_ld.last_id_version_before_freeze:null,
						}
					},
					success: (dataDiff) => {
						this.diffError = false;
						this.labdocData = dataDiff;
						this.showModifications();
						this.loading = false;
					},
					error: (error) => {
						alertGently(__("La visualisation des modifications apportées par les étudiants n'est pas possible."), "error", 5000);
						this.diffError = true;
						this.labdocData = [];
						this.displayDiff = false;
						this.loading = false;
					}
				});
			} else {
				this.hideLdDiff();
				this.displayDiff = false;
			}
		},

		showModifications() {
			if (this.labdocData['same_version']) {
				if (this.labdocData && this.labdocData.versioning && this.labdocData.versioning.length > 0) {
					alertGently(__("La version actuelle du labdoc est identique à la version source"), "info", 3000);
				} else {
					alertGently(__("La version actuelle du labdoc est identique à la version initiale"), "info", 3000);
				}
			}
			this.show();
			this.log_trace();
			this.displayDiff = true;
		}
	}
}

/**
 * captures CTRL-Z and CTRL-Y for open labdocs
 * works only for Fitex & Copex
 * Zwibbler & TinyMCE intercept CTRL-Z and CTRL-Y before LNB => they are not synchronized with the state of the buttons => TODO
 */
$(document).keydown(function(event) {
	let id_ld = window.global_edited_ld;
	if (!(id_ld && (event.ctrlKey || event.metaKey))) {
		return;
	}
	if (event.which === KeyboardEvent.DOM_VK_Z && (isLDVersionAvailable(-1, id_ld) || isProcedure(id_ld, 'json'))) {
		event.stopPropagation();
		if(isProcedure(id_ld, 'json')){
			window.dispatchEvent(new CustomEvent('prons-undo-redo-action', { detail: { id_ld: id_ld, undo:true} }));
		}else {
			displayLDContent(id_ld,true,1,-1);
		}
	} else if (event.which === KeyboardEvent.DOM_VK_Y && (isLDVersionAvailable(1, id_ld) || isProcedure(id_ld, 'json'))) {
		event.stopPropagation();
		if(isProcedure(id_ld, 'json')){
			window.dispatchEvent(new CustomEvent('prons-undo-redo-action', { detail: { id_ld: id_ld, undo:false} }));
		}else{
			displayLDContent(id_ld, true, 1, 1);
		}
	}
});
