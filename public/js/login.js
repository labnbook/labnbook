function login_setFocus() {
    if ($('#user_login').val() != "") {
        $("#user_pwd").focus();
	}
}

function login_init_events() {
    $("#login-lnb").submit( function(event){
        if ($('#user_login').val() == "" || $('#user_pwd').val() == "") {
            event.preventDefault();
            alert((__("Veuillez indiquer vos informations de connexion")));
        }
    });

    $("#login-code").click(function() {
        var code = $("#user_code").val()
        if (code == "") {
            alert( __("Veuillez indiquer le code que vous a fourni votre enseignant"));
        }
        else {
            window.location = '/classe/joinByCode?code=' + encodeURIComponent($("#user_code").val());
        }
    });

    $("#login-mail").on('click', function(event) {
        event.preventDefault();
        if ($('#user_email').val() === "") {
            alert(__("Veuillez entrer l'e-mail correspondant à votre compte LabNBook"));
            return;
        }
        $.ajax({
            type: "POST",
            url: "/password/email",
            data: {
                email: $("#user_email").val()
            },
            success: function (response) {
                alert(response.message, response.category, 0);
            }
        });
    });
}

// init when DOM is loaded
$(function() {
    login_setFocus();
    login_init_events();

    const browser  = bowser.getParser(window.navigator.userAgent);
    let message = "";
    if (!browser.is('firefox') && !browser.is('chrome')) {
        message = __("LabNBook est développé pour Firefox, Chrome et leurs dérivés. Nous vous recommandons d'utiliser ce type de navigateur.");
    } else {
        const isValidBrowser = browser.satisfies({  // Requirements for async functions
            chrome: ">=55",
            firefox: ">=52",
        });
        if(!isValidBrowser){
            message = __("Votre navigateur est d'une ancienne génération. Nous vous recommandons de le mettre à jour.");
        }
    }
    if (message != "") {
        try { // If browser is very old might not even load common.js thus alertGently won't work
            alertGently(message, "warning", 20000);
        } catch (error) {
            alert($('<p>').html(message).text());
        }
    }
});

// Force reload page if page is loaded from cache to clean session see issue #1368
// Code from https://stackoverflow.com/q/42920668
window.addEventListener('pageshow', function (e) {
    if (e.persisted) {
        window.location.reload();
    }
}, false);
