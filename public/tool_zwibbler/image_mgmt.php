<?php

require (dirname(__DIR__).'/common/boot_laravel.php');

require("../common/default.php");

function statusToJson($code, $status) {
	if ($code !== 200) {
		error_log("zwibbler img upload: " . $status);
	}
	http_response_code($code);
    header("Content-type: application/json");
    echo json_encode(["status" => $status]);
}

function replyToExpiredSession($message = "", $code=200) {
	http_response_code($code);
	if (!request()->ajax()) {
		header("Location: /");
		addAlert("warning", $message);
	}
	die();
}

if(!auth()->check()) {
	replyToExpiredSession("Votre session a expiré. Merci de vous reconnecter.", 401);
}
if (!isset($_REQUEST['action'])) {
	replyToExpiredSession("Cette page ne peut pas être consultée.", 403);
}

if (isset($_GET['id_mission'])) { // New Zwibbler labdocs
	$id_mission = (int) $_GET['id_mission'];
} else {
	$id_mission = 0;
}
$id_u = auth()->id();


// **************************************************
// upload d'une image
// **************************************************
if ($_REQUEST['action'] == 'upload') {

	if ($_FILES["file"]["error"] == UPLOAD_ERR_INI_SIZE || $_FILES["file"]["size"] > 2097152) { // vérification de la taille en octets = 2Mo
		statusToJson(413, "La taille de l'image est trop grande (2Mo maximum).");
	}

	else if ($_FILES["file"]["error"] != UPLOAD_ERR_OK) { // vérification que l'upload s'est bien fait
		statusToJson(500, "Upload impossible - code erreur ".$_FILES["file"]["error"]);
	}

	else {
		// vérification du type. Voir aussi JS:displayDrawing() et l'attribut input[accept]
		$allowed_exts = ["gif", "jpeg", "jpg", "png"];
		$allowed_types = ["image/gif", "image/jpeg", "image/jpg", "image/pjpeg", "image/x-png", "image/png"];

		$extension = strtolower(pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION));
		$finfo = finfo_open(FILEINFO_MIME_TYPE);
		$type = strtolower(finfo_file($finfo, $_FILES['file']['tmp_name']));
		finfo_close($finfo);
		if ( !in_array($type, $allowed_types) || !in_array($extension, $allowed_exts)) {
			statusToJson(415, "Le type de l'image n'est pas reconnu.");
		}

		else { // tout va bien
			// le file id est composé du n° de user avec la date qui lui est accolée
			$now = new DateTime("now");
			$time = $now->format('Y-m-d_H-i-s');
			$fileid = $id_u.'_'.$time.'.'.$extension;

			// on checke les dossiers
			$content_dir = storage_path('app/public/missions/'.$id_mission.'/zw_img'); // dossier où sera placé le fichier
			if (!is_dir($content_dir)) {
				mkdir($content_dir, 0777, true);
			}

			// copie du fichier dans le dossier
			if (!move_uploaded_file($_FILES["file"]["tmp_name"], $content_dir.'/'.$fileid) ) {
				statusToJson(500, "Impossible de copier le fichier dans le dossier de destination.");
			}
			else {
				chmod($content_dir.'/'.$fileid, 0777);
				header("Content-type: application/json");
				echo json_encode([
					"status" => "ok",
					"fileid" => $fileid,
					"path" => "$content_dir/$fileid",
				]);
			}
		}
	}
}


// **************************************************
// récupération d'une image
// **************************************************
else if ($_REQUEST['action'] === 'get') {
	if (empty($_GET["fileid"])) {
		http_response_code(404);
		exit();
	}
	$fileid = (string) $_GET["fileid"];
	$extension = pathinfo($fileid, PATHINFO_EXTENSION);
	if ($extension === 'png') {
		header("Content-type: image/png");
	} else if ($extension === 'pdf') {
		header("Content-type: application/pdf");
	} else {
		header("Content-type: image/jpeg");
	}

	$filename = sprintf('%s/missions/%d/zw_img/%s', storage_path('app/public'), (int) $id_mission, $fileid);
	if (preg_match('/^[\w-]+\.[\w-]+$/', $fileid) && is_file($filename) && is_readable($filename)) {
		readfile($filename);
	} else {
		http_response_code(404);
	}
} else {
	http_response_code(404);
	echo "unknown action";
}
