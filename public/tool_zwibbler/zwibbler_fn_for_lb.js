/**
 * This scripts aims at integrating zwibbler in LabNBook.
 * It is based on the zwibbler-script distributed with zwibbler
 * Search [Diff] for code changed from original (the minimum !)
 */

let ARE_YOU_SURE = __("Voulez-vous vraiment supprimer cet élément?");
let ENTER_FILENAME = __("Nom de fichier:");
let LIBRARY_FILE_EXTENSION = ".zlnb";
let ERROR_OPENING_LIBRARY = __("Erreur lors de l'ouverture du fichier");
let DEFAULT_LIBRARY_FILENAME = "bibliothèque";

var zw_path = "/tool_zwibbler/icons/";
var zw_contexts = []; // Contexts for zwibbler en mode edition

var FILL_PATTERNS = [
	zw_path + "/pattern-1.png",
	zw_path + "/pattern-2.png",
	zw_path + "/pattern-3.png",
	zw_path + "/pattern-4.png",
	zw_path + "/pattern-5.png",
	zw_path + "/pattern-6.png",
	zw_path + "/pattern-7.png",
	zw_path + "/pattern-8.png",
	zw_path + "/pattern-9.png",
]

var IMAGE_FOLDERS = [ ];


$().ready(function() {
	// @ts-ignore
	Zwibbler.controller("labbook", (scope) => {
		const ctx = scope.ctx;

		/** @type {{[key:string]:ImageClipData|null}} */
		let urlMap = {};
		
		//console.log("Controller running, scope is ", scope);
	
		if (!scope.edition) {
			scope.ctx.setConfig("readOnly", true);
			scope.ctx.setConfig("background", "clear");
		} else {
			setTimeout(() => { // because it contains z-if, it won't be there for a tick
				let resizable_div = scope.ctx.getElement();
				$(resizable_div).resizable({
					handles: { 's,n' : $(resizable_div).find(".sizing-bar") },
					minHeight: 150
				});
			}, 0);
		}
	
		const zel = scope.ctx.getElement();
		const canvas = zel.querySelector("[z-canvas]");
		scope.fillPatterns = FILL_PATTERNS;
	
		// current image menu
		i18next_Promise.then(function() {
			IMAGE_FOLDERS = [
				{
					caption: __("Electricité"),
					subfolders: [{
						caption: "", // empty => one section
						images: [
							{ path: zw_path + "/phy_elec_2022/fleche-1.svg", title: __("Flèche épaisseur 1") },
							{ path: zw_path + "/phy_elec_2022/fleche-3.svg", title: __("Flèche épaisseur 3") },
							{ path: zw_path + "/phy_elec_2022/point-3.svg", title: __("Point épaisseur 3") },
							{ path: zw_path + "/phy_elec_2022/point-5.svg", title: __("Point épaisseur 5") },
			                { path: zw_path + "/phy_elec_2022/resistance.svg", title: __("résistance (conducteur ohmique)") },
			                { path: zw_path + "/phy_elec_2022/resistance-us.svg", title: __("résistance (norme US)") },
			                { path: zw_path + "/phy_elec_2022/resistance-variable.svg", title: __("résistance variable") },
			                { path: zw_path + "/phy_elec_2022/resistance-ajustable.svg", title: __("résistance ajustable") },
			                { path: zw_path + "/phy_elec_2022/thermistance.svg", title: __("thermistance") },
			                { path: zw_path + "/phy_elec_2022/potentiometre.svg", title: __("potentiomètre") },
			                { path: zw_path + "/phy_elec_2022/potentiometre-us.svg", title: __("potentiomètre (norme US)") },
			                { path: zw_path + "/phy_elec_2022/bobine.svg", title: __("bobine") },
							{ path: zw_path + "/phy_elec_2022/inductance.svg", title: __("inductance") },
			                { path: zw_path + "/phy_elec_2022/condensateur.svg", title: __("condensateur") },
			                { path: zw_path + "/phy_elec_2022/condensateur-variable.svg", title: __("condensateur variable") },
			                { path: zw_path + "/phy_elec_2022/pile.svg", title: __("pile") },
			                { path: zw_path + "/phy_elec_2022/generateur-courant.svg", title: __("générateur de courant") },
			                { path: zw_path + "/phy_elec_2022/generateur-tension.svg", title: __("générateur de tension") },
			                { path: zw_path + "/phy_elec_2022/generateur.svg", title: __("générateur") },
			                { path: zw_path + "/phy_elec_2022/lampe.svg", title: __("lampe") },
			                { path: zw_path + "/phy_elec_2022/amperemetre.svg", title: __("ampèremètre") },
			                { path: zw_path + "/phy_elec_2022/voltmetre.svg", title: __("voltmètre") },
			                { path: zw_path + "/phy_elec_2022/ohmmetre.svg", title: __("ohmmètre") },
			                { path: zw_path + "/phy_elec_2022/moteur.svg", title: __("moteur") },
			                { path: zw_path + "/phy_elec_2022/dipole.svg", title: __("dipôle à compléter") },
			                { path: zw_path + "/phy_elec_2022/terre.svg", title: __("terre") },
			                { path: zw_path + "/phy_elec_2022/transfo.svg", title: __("transformateur") },
			                { path: zw_path + "/phy_elec_2022/transfo-noyau.svg", title: __("transformateur avec noyau") },
			                { path: zw_path + "/phy_elec_2022/interrupteur-ouvert.svg", title: __("interrupteur ouvert") },
			                { path: zw_path + "/phy_elec_2022/interrupteur-ferme.svg", title: __("interrupteur fermé") },
			                { path: zw_path + "/phy_elec_2022/commutateur-ouvert.svg", title: __("commutateur ouvert") },
			                { path: zw_path + "/phy_elec_2022/commutateur-ferme-haut.svg", title: __("commutateur fermé haut") },
			                { path: zw_path + "/phy_elec_2022/commutateur-ferme-bas.svg", title: __("commutateur fermé bas") },
			                { path: zw_path + "/phy_elec_2022/diode.svg", title: __("diode") },
			                { path: zw_path + "/phy_elec_2022/del.svg", title: __("DEL") },
			                { path: zw_path + "/phy_elec_2022/photodiode.svg", title: __("photodiode") },
			                { path: zw_path + "/phy_elec_2022/ampli-op.svg", title: __("ampli op") },
			                { path: zw_path + "/phy_elec_2022/ampli-op-alimente.svg", title: __("ampli op alimenté") },
			                { path: zw_path + "/phy_elec_2022/transistor-npn.svg", title: __("transistor NPN") },
			                { path: zw_path + "/phy_elec_2022/transistor-pnp.svg", title: __("transistor PNP") },
						]
					}]
				},
				{
					caption: __("Génie des procédés"),
					// I Used this links to understand symbols 
					// https://fr.m.wikipedia.org/wiki/Sch%C3%A9ma_tuyauterie_et_instrumentation
					// https://gallery.proficad.com/symboles/autres/equipements-industriels/echangeurs-thermiques/
					subfolders: [
						{
							caption: __("Colonnes"), // useless, only one sub-folder
							images: [
								{ path: zw_path + "/process_engineering/colonnes/colonne.svg", title: __("Colonne") },
								{ path: zw_path + "/process_engineering/colonnes/colonne-2-lit-fixe-distributeur-intermediaire.svg", title: __("Colonne à lit fixe et distributeur intermédiaire") },
								{ path: zw_path + "/process_engineering/colonnes/colonne_Е_garnissage_Fluid_contacting_column.svg", title: __("Colonne à garnissage") },
								{ path: zw_path + "/process_engineering/colonnes/colonne_Е_lit_fixe.svg", title: __("Colonne à lit fixe") },
								{ path: zw_path + "/process_engineering/colonnes/colonne_Е_lit_fluidisВ.svg", title: __("Colonne à lit fluidisé") },
								{ path: zw_path + "/process_engineering/colonnes/colonne_Е_plateaux2.svg", title: __("Colonne à plateaux") },
								{ path: zw_path + "/process_engineering/colonnes/colonne_Е_plateaux_Tray_column.svg", title: __("Colonne à plateaux") },
								{ path: zw_path + "/process_engineering/colonnes/colonne_Е_plateaux_perforВs.svg", title: __("Colonne à plateaux perforés") },
								{ path: zw_path + "/process_engineering/colonnes/colonne_Е_plateaux_Е_clapets.svg", title: __("Colonne à plateaux à clapets") },
							]
						},
						{
							caption: __("Échangeurs de chaleur"),
							images: [
							
								{ path: zw_path + "/process_engineering/Echangeurs_de_chaleur/Echangeur de chaleur_3_Heat_exchanger_with_cross.svg", title: __("Échangeur de chaleur avec croisement") },
								{ path: zw_path + "/process_engineering/Echangeurs_de_chaleur/Echangeur de chaleur_4_Heat_exchanger_no_cross.svg", title: __("Échangeur de chaleur sans croisement") },
								{ path: zw_path + "/process_engineering/Echangeurs_de_chaleur/Echangeur double enveloppe_Double_pipe_heat_exchanger.svg", title: __("Échangeur de chaleur à tubes concentriques") },
								{ path: zw_path + "/process_engineering/Echangeurs_de_chaleur/Echangeur spirale 2_Spiral_heat_exchanger.svg", title: __("Échangeur de chaleur à spirale") },
								{ path: zw_path + "/process_engineering/Echangeurs_de_chaleur/Echangeur tubes ailetВs et ventilateur axial_Finned-tube_heat_exchanger_with_axial_fan.svg", title: __("Échangeur de chaleur à tubes à ailettes et ventilateur axial") },
								{ path: zw_path + "/process_engineering/Echangeurs_de_chaleur/Echangeur Tubes droits et calandre_Fixed_straight_tubes_heat_exchanger.svg", title: __("Échangeur de chaleur à faisceau tubulaire") },
								{ path: zw_path + "/process_engineering/Echangeurs_de_chaleur/Echangeur Tubes et calandre_U_shaped_tubes_heat_exchanger.svg", title: __("Échangeur de chaleur à faisceau de tubes en U") },
								{ path: zw_path + "/process_engineering/Echangeurs_de_chaleur/Echangeur Е plaques 2_Plate_heat_exchanger.svg", title: __("Échangeur thermique à plaques") },
								{ path: zw_path + "/process_engineering/Echangeurs_de_chaleur/Four_Furnace.svg", title: __("Four, incinérateur") },
								{ path: zw_path + "/process_engineering/Echangeurs_de_chaleur/Kettle bouilleur_Kettle_reboiler.svg", title: __("Rebouilleur Kettle") },
								{ path: zw_path + "/process_engineering/Echangeurs_de_chaleur/Rebouilleur_reboiler.svg", title: __("Rebouilleur") },
								{ path: zw_path + "/process_engineering/Echangeurs_de_chaleur/Refroidisseur d'air_Air_cooler.svg", title: __("Refroidisseur d'air") },
								{ path: zw_path + "/process_engineering/Echangeurs_de_chaleur/Echangeur de chaleur_1_hex_general_sim.svg", title: __("Échangeur de chaleur hex_general_sim") },
								{ path: zw_path + "/process_engineering/Echangeurs_de_chaleur/Echangeur de chaleur_2_hex_general_mform.svg", title: __("Échangeur de chaleur hex_general_mform") },
								{ path: zw_path + "/process_engineering/Echangeurs_de_chaleur/Echangeur spirale_hex_spiral.svg", title:  __("Échangeur de chaleur à spirale") },
								{ path: zw_path + "/process_engineering/Echangeurs_de_chaleur/Echangeur Е plaques_plate_heat_exchanger.svg", title: __("Échangeur thermique à plaques") }
							]
						},
						{
							caption: __("Pompes Compresseurs Ventilateurs") ,
							images: [
								{ path: zw_path + "/process_engineering/Pompes  Compresseurs Ventilateurs/Compresseur Е piston_Piston_Compressor.svg", title: __("Compresseur à piston") },
								{ path: zw_path + "/process_engineering/Pompes  Compresseurs Ventilateurs/Pompe_Pump.svg", title: __("Pompe") },
								{ path: zw_path + "/process_engineering/Pompes  Compresseurs Ventilateurs/Pompe Е vide ou compresseur_Vacuum_Pump_or_Compressor.svg", title: __("Pompe à vide ou compresseur") },
								{ path: zw_path + "/process_engineering/Pompes  Compresseurs Ventilateurs/Turbo_compresseur_Turbo_Compressor.svg", title: __("Turbo compresseur") },
								{ path: zw_path + "/process_engineering/Pompes  Compresseurs Ventilateurs/Ventilateur 2-Fan.svg", title: __("Ventilateur") },
								{ path: zw_path + "/process_engineering/Pompes  Compresseurs Ventilateurs/Ventilateur axial_Axial_fan.svg", title: __("Ventilateur axial") },
								{ path: zw_path + "/process_engineering/Pompes  Compresseurs Ventilateurs/Ventilateur radial_Radial_fan.svg", title: __("Ventilateur radial") }
							]
						},
						{
							caption: __("Réacteurs"),
							images: [
								{ path: zw_path + "/process_engineering/Réacteurs/Agitateur_Stirrer.svg", title: __("Agitateur") },
								{ path: zw_path + "/process_engineering/Réacteurs/Mélangeur_Mixer.svg", title: __("Mélangeur") },
								{ path: zw_path + "/process_engineering/Réacteurs/Réacteur avec paroi chauffante_vessel_with_heating_jacket.svg", title: __("Réacteur avec paroi chauffante") },
								{ path: zw_path + "/process_engineering/Réacteurs/Réacteur avec serpentin extВrieur_Half_pipe_reactor.svg", title: __("Réacteur avec serpentin extérieur") },
								{ path: zw_path + "/process_engineering/Réacteurs/Réacteur double_enveloppe_Autoclave.svg", title: __("Réacteur double enveloppe") }
							]
						},
						{
							caption: __("Réservoirs"),
							images: [
								{ path: zw_path + "/process_engineering/Réservoirs/bouteille de gaz_Gas_bottle.svg", title: __("Bouteille de gaz") },
								{ path: zw_path + "/process_engineering/Réservoirs/Cuve.svg", title: __("Cuve") },
								{ path: zw_path + "/process_engineering/Réservoirs/Réservoir 1.svg", title: __("Réservoir 1") },
								{ path: zw_path + "/process_engineering/Réservoirs/Réservoir 2.svg", title: __("Réservoir 2") },
								{ path: zw_path + "/process_engineering/Réservoirs/Réservoir 3.svg", title: __("Réservoir 3") },
								{ path: zw_path + "/process_engineering/Réservoirs/Réservoir fonds bombВs.svg", title: __("Réservoir fonds bombés") },
								{ path: zw_path + "/process_engineering/Réservoirs/Réservoir sous pression horizontal_Pressurized_vessel_horizontal.svg", title: __("Réservoir sous pression horizontal") },
								{ path: zw_path + "/process_engineering/Réservoirs/Réservoir sous pression vertical_Pressurized_vessel_vertical.svg", title: __("Réservoir sous pression vertical") },
								{ path: zw_path + "/process_engineering/Réservoirs/Sac_Bag.svg", title: __("Sac") }
							]
						},
						{
							caption: __("Sécurité"),
							images: [
								{ path: zw_path + "/process_engineering/Sécurité/Event couvert_Covered_gas_vent.svg", title: __("Sortie pour gaz d'échappement") },
								{ path: zw_path + "/process_engineering/Sécurité/Event_Curved_gas_vent.svg", title: __("Sortie pour gaz d'échappement")  },
								{ path: zw_path + "/process_engineering/Sécurité/PiКge Е vapeur_Steam_trap.svg", title: __("Piège à vapeur") },
								{ path: zw_path + "/process_engineering/Sécurité/Viewing_glass.svg", title: __("Regard") },
							]
						},
						{
							caption: __("Tours de refroidissement"),
							images: [
								{ path: zw_path + "/process_engineering/Tours de refroidissement/Tour de refroidissement_Cooling_tower.svg", title: __("Tour de refroidissement A") },
								{ path: zw_path + "/process_engineering/Tours de refroidissement/Tour de refroidissement 2_Wet_cooling_tower.svg", title: __("Tour de refroidissement B") }
							]
						},
						{
							caption: __("Traitement solides"),
							images: [
								{ path: zw_path + "/process_engineering/Traitement solides/Broyeur_Breaker.svg", title: __("Broyeur") },
								{ path: zw_path + "/process_engineering/Traitement solides/Broyeur 2.svg", title: __("Broyeur 2") },
								{ path: zw_path + "/process_engineering/Traitement solides/Broyeur 3.svg", title: __("Broyeur 3") },
								{ path: zw_path + "/process_engineering/Traitement solides/Broyeur 4.svg", title: __("Broyeur 4") },
								{ path: zw_path + "/process_engineering/Traitement solides/Filtre_Filter.svg", title: __("Filtre") },
								{ path: zw_path + "/process_engineering/Traitement solides/Filtre Е poussiКres_Dust_trap.svg", title: __("Filtre à poussières") },
								{ path: zw_path + "/process_engineering/Traitement solides/SВchoir_Dryer.svg", title: __("Séchoir") }
							]
						},
						{
							caption: __("Vannes et tuyauterie"),
							images: [
								{ path: zw_path + "/process_engineering/Vannes et tuyauterie/Conduite_Pipe.svg", title: __("Conduite") },
								{ path: zw_path + "/process_engineering/Vannes et tuyauterie/Conduite flexible_Flexible_pipe.svg", title: __("Conduite flexible") },
								{ path: zw_path + "/process_engineering/Vannes et tuyauterie/Conduite isolВe_Insulated_pipe.svg", title: __("Conduite isolée") },
								{ path: zw_path + "/process_engineering/Vannes et tuyauterie/Conduite refroidie ou chauffВe_Cooled_or_heated_pipe.svg", title: __("Conduite refroidie ou chauffée") },
								{ path: zw_path + "/process_engineering/Vannes et tuyauterie/Conduite Е double enveloppe_Jacketed_pipe.svg", title: __("Conduite à double enveloppe") },
								{ path: zw_path + "/process_engineering/Vannes et tuyauterie/Convergent_Concentric_piping_reducer.svg", title: __("Réducteur de tuyauterie concentrique convergent") },
								{ path: zw_path + "/process_engineering/Vannes et tuyauterie/Evacuation_Funnel.svg", title: __("Évacuation") },
								{ path: zw_path + "/process_engineering/Vannes et tuyauterie/RВducteur de Pression_Pressure_reducing_valve.svg", title: __("Réducteur de pression") },
								{ path: zw_path + "/process_engineering/Vannes et tuyauterie/Soupape de sВcuritВ_Spring_safety_valve.svg", title: __("Soupape de sécurité") },
								{ path: zw_path + "/process_engineering/Vannes et tuyauterie/Vanne d'arrИt_Gate_valve.svg", title: __("Vanne d'arrêt") },
								{ path: zw_path + "/process_engineering/Vannes et tuyauterie/Vanne de rВgulation_Control_valve.svg", title: __("Vanne de régulation") },	
								{ path: zw_path + "/process_engineering/Vannes et tuyauterie/Vanne manuelle_Manual_valve.svg", title: __("Vanne manuelle") },
								{ path: zw_path + "/process_engineering/Vannes et tuyauterie/Vanne papillon_Butterfly_valve.svg", title: __("Vanne papillon") },
								{ path: zw_path + "/process_engineering/Vannes et tuyauterie/Vanne Е aiguille_Needle_valve.svg", title: __("Vanne à aiguille") },
								{ path: zw_path + "/process_engineering/Vannes et tuyauterie/Vanne Е boisseau sphВrique_Ball_valve.svg", title: __("Vanne à boisseau sphérique") },
								{ path: zw_path + "/process_engineering/Vannes et tuyauterie/Vanne Е diaphragme_Diaphragm_valve.svg", title: __("Vanne à diaphragme") },
								{ path: zw_path + "/process_engineering/Vannes et tuyauterie/Vanne Е soupape_Globe_valve.svg", title: __("Vanne à soupape") }
							]
						}
					]
				},
			    {
					caption: __("Mécanique des fluides"),
					subfolders: [{
						caption: "", // empty => one section
						images: [
			                { path: zw_path + "/meca_flu/agitateur.svg", title: __("agitateur") },
			                { path: zw_path + "/meca_flu/agitateur_palettes.svg", title: __("agitateur à palettes") },
			                { path: zw_path + "/meca_flu/capillaire.svg", title: __("capillaire") },
			                { path: zw_path + "/meca_flu/chauffage_refroidissement_general.svg", title: __("chauffage refroidissement général") },
			                { path: zw_path + "/meca_flu/compresseur_general.svg", title: __("compresseur général") },
			                { path: zw_path + "/meca_flu/connexion_T.svg", title: __("connexion en T") },
			                { path: zw_path + "/meca_flu/debitmetre_coriolis.svg", title: __("débitmetre coriolis") },
			                { path: zw_path + "/meca_flu/debitmetre_flotteur.svg", title: __("débitmetre à flotteur") },
			                { path: zw_path + "/meca_flu/debitmetre_turbine.svg", title: __("débitmetre à turbine") },
			                { path: zw_path + "/meca_flu/debitmetre_ultrasons.svg", title: __("débitmetre à ultrasons") },
			                { path: zw_path + "/meca_flu/diaphragme.svg", title: __("diaphragme") },
			                { path: zw_path + "/meca_flu/echangeur_chaleur_plaques.svg", title: __("échangeur de chaleur à plaques") },
			                { path: zw_path + "/meca_flu/echangeur_thermique_ailettes.svg", title: __("échangeur thermique à ailettes") },
			                { path: zw_path + "/meca_flu/echangeur_thermique_serpentin.svg", title: __("échangeur thermique à serpentin") },
			                { path: zw_path + "/meca_flu/entrainement_hydraulique.svg", title: __("entrainement hydraulique") },
			                { path: zw_path + "/meca_flu/entree.svg", title: __("entrée") },
			                { path: zw_path + "/meca_flu/filtre_cartouche.svg", title: __("filtre à cartouche") },
			                { path: zw_path + "/meca_flu/fluide_air.svg", title: __("fluide : air") },
			                { path: zw_path + "/meca_flu/fluide_eau.svg", title: __("fluide : eau") },
			                { path: zw_path + "/meca_flu/fluide_huile.svg", title: __("fluide : huile") },
			                { path: zw_path + "/meca_flu/moteur_combustion.svg", title: __("moteur à combustion") },
			                { path: zw_path + "/meca_flu/moteur_electrique.svg", title: __("moteur électrique") },
			                { path: zw_path + "/meca_flu/pompe_alternative.svg", title: __("pompe alternative") },
			                { path: zw_path + "/meca_flu/pompe_centrifuge.svg", title: __("pompe centrifuge") },
			                { path: zw_path + "/meca_flu/pompe_centrifuge_moteur_electrique.svg", title: __("pompe centrifuge à moteur électrique") },
			                { path: zw_path + "/meca_flu/pompe_engrenages.svg", title: __("pompe à engrenages") },
			                { path: zw_path + "/meca_flu/pompe_general.svg", title: __("pompe générale") },
			                { path: zw_path + "/meca_flu/pompe_membrane.svg", title: __("pompe à membrane") },
			                { path: zw_path + "/meca_flu/pompe_vis.svg", title: __("pompe à vis") },
			                { path: zw_path + "/meca_flu/pulverisateur.svg", title: __("pulvérisateur") },
			                { path: zw_path + "/meca_flu/reducteur.svg", title: __("réducteur") },
			                { path: zw_path + "/meca_flu/robinet_boisseau_spherique.svg", title: __("robinet à boisseau sphérique") },
			                { path: zw_path + "/meca_flu/robinet_demontable.svg", title: __("robinet démontable") },
			                { path: zw_path + "/meca_flu/robinet_ferme.svg", title: __("robinet fermé") },
			                { path: zw_path + "/meca_flu/robinet_ouvert.svg", title: __("robinet ouvert") },
			                { path: zw_path + "/meca_flu/robinet_soupape.svg", title: __("robinet à soupape") },
			                { path: zw_path + "/meca_flu/sortie.svg", title: __("sortie") },
			                { path: zw_path + "/meca_flu/soupape_surete.svg", title: __("soupape de sureté") },
			                { path: zw_path + "/meca_flu/tableau_commande_local.svg", title: __("tableau de commande local") },
			                { path: zw_path + "/meca_flu/tuyauterie_souple.svg", title: __("tuyauterie souple") },
			                { path: zw_path + "/meca_flu/vanne_equerre.svg", title: __("vanne équerre") },
			                { path: zw_path + "/meca_flu/vanne_fermee.svg", title: __("vanne fermée") },
			                { path: zw_path + "/meca_flu/vanne_papillon.svg", title: __("vanne papillon") },
			                { path: zw_path + "/meca_flu/vanne_trois_voies.svg", title: __("vanne trois voies") },
			                { path: zw_path + "/meca_flu/ventilateur.svg", title: __("ventilateur") },
			                { path: zw_path + "/meca_flu/venturi.svg", title: __("venturi") },
			                { path: zw_path + "/meca_flu/voyant.svg", title: __("voyant") }
						]
					}]
				},
				{
					caption: __("Optique"),
					subfolders: [{
						caption: "", // empty => one section
						images: [
			                { path: zw_path + "/phy_optique/ampoule.svg", title: __("ampoule") },
			                { path: zw_path + "/phy_optique/axe_optique.svg", title: __("axe optique") },
			                { path: zw_path + "/phy_optique/demi-cylindre_plein.svg", title: __("demi-cylindre plein") },
			                { path: zw_path + "/phy_optique/demi-diaphragme.svg", title: __("demi-diaphragme") },
			                { path: zw_path + "/phy_optique/diaphragme.svg", title: __("diaphragme") },
							{ path: zw_path + "/phy_optique/dioptre.svg", title: __("dioptre") },
							{ path: zw_path + "/phy_optique/dioptre_plan.svg", title: __("dioptre plan") },
			                { path: zw_path + "/phy_optique/ecran.svg", title: __("écran") },
			                { path: zw_path + "/phy_optique/guide_section_rectangulaire.svg", title: __("guide à section rectangulaire") },
							{ path: zw_path + "/phy_optique/lame_face_parallele.svg", title: __("lame à faces parallèles") },
			                { path: zw_path + "/phy_optique/laser_rouge.svg", title: __("laser rouge") },
			                { path: zw_path + "/phy_optique/laser_vert.svg", title: __("laser vert") },
							{ path: zw_path + "/phy_optique/lentille_convergente.svg", title: __("lentille convergente") },
							{ path: zw_path + "/phy_optique/lentille_divergente.svg", title: __("lentille divergente") },
							{ path: zw_path + "/phy_optique/miroir_plan.svg", title: __("miroir plan") },
							{ path: zw_path + "/phy_optique/objet_ou_image.svg", title: __("objet ou image") },
			                { path: zw_path + "/phy_optique/oeil_droite.svg", title: __("oeil à droite") },
			                { path: zw_path + "/phy_optique/oeil_gauche.svg", title: __("oeil à gauche") },
			                { path: zw_path + "/phy_optique/prisme_equilateral.svg", title: __("Prisme équilatéral") },
			                { path: zw_path + "/phy_optique/prisme_rectangle_25_65.svg", title: __("prisme rectangle (25°,65°)") },
			                { path: zw_path + "/phy_optique/prisme_rectangle_30_60.svg", title: __("prisme rectangle (30°,60°)") },
			                { path: zw_path + "/phy_optique/rapporteur.svg", title: __("rapporteur") },
			                { path: zw_path + "/phy_optique/rayon_lumineux_blanc.svg", title: __("rayon lumineux blanc") },
			                { path: zw_path + "/phy_optique/rayon_lumineux_bleu.svg", title: __("rayon lumineux bleu") },
			                { path: zw_path + "/phy_optique/rayon_lumineux_rouge.svg", title: __("rayon lumineux rouge") },
			                { path: zw_path + "/phy_optique/rayon_lumineux_vert.svg", title: __("rayon lumineux vert") },
							{ path: zw_path + "/phy_optique/rayon_lumineux_virtuel_blanc.svg", title: __("rayon lumineux virtuel blanc") },
							{ path: zw_path + "/phy_optique/rayon_lumineux_virtuel_bleu.svg", title: __("rayon lumineux virtuel bleu") },
							{ path: zw_path + "/phy_optique/rayon_lumineux_virtuel_rouge.svg", title: __("rayon lumineux virtuel rouge") },
							{ path: zw_path + "/phy_optique/rayon_lumineux_virtuel_vert.svg", title: __("rayon lumineux virtuel vert") },
							{ path: zw_path + "/phy_optique/regle_graduee.svg", title: __("règle graduée") }
						]
					}]
				},
			    {
					caption: __("Verrerie de chimie"),
					subfolders: [{
						caption: "", // empty => one section
						images: [
							{ path: zw_path + "/chem_orga/becher.svg", title: __("bécher") },
			                { path: zw_path + "/chem_orga/erlen.svg", title: __("erlen") },
			                { path: zw_path + "/chem_orga/eprouvette.svg", title: __("éprouvette") },
			                { path: zw_path + "/chem_orga/burette.svg", title: __("burette") },
			                { path: zw_path + "/chem_orga/tube.svg", title: __("tube à essai") },
			                { path: zw_path + "/chem_orga/fiole.svg", title: __("fiole jaugée") },
			                { path: zw_path + "/chem_orga/pipette.svg", title: __("pipette") },
			                { path: zw_path + "/chem_orga/pipette-graduee.svg", title: __("pipette graduée") },
			                { path: zw_path + "/chem_orga/filtre.svg", title: __("entonnoir et filtre") },
			                { path: zw_path + "/chem_orga/microtube.svg", title: __("microtube") },
			                { path: zw_path + "/chem_orga/ampoule.svg", title: __("ampoule à décanter") },
			                { path: zw_path + "/chem_orga/ballon.svg", title: __("ballon") },
			                { path: zw_path + "/chem_orga/tricol.svg", title: __("ballon tricol") },
			                { path: zw_path + "/chem_orga/bouchon.svg", title: __("bouchon") },
			                { path: zw_path + "/chem_orga/colonne-vigreux.svg", title: __("colonne de Vigreux") },
			                { path: zw_path + "/chem_orga/refrigerant.svg", title: __("réfrigérant") },
			                { path: zw_path + "/chem_orga/thermometre.svg", title: __("thermomètre") },
			                { path: zw_path + "/chem_orga/barreau.svg", title: __("barreau aimanté") },
			                { path: zw_path + "/chem_orga/agitateur.svg", title: __("agitateur magnétique") },
			                { path: zw_path + "/chem_orga/plaque-chauffante.svg", title: __("plaque chauffante") },
			                { path: zw_path + "/chem_orga/chauffe-ballon.svg", title: __("chauffe ballon") },
			                { path: zw_path + "/chem_orga/verre-montre.svg", title: __("verre de montre") },
			                { path: zw_path + "/chem_orga/balance.svg", title: __("balance") },
			                { path: zw_path + "/chem_orga/chronometre.svg", title: __("chrononomètre") },
			                { path: zw_path + "/chem_orga/potence.svg", title: __("potence") },
			                { path: zw_path + "/chem_orga/pince.svg", title: __("pince") }
			            ]
					}]
				},
				{
					caption: __("Bibliothèque du labdoc"),
					local: true, // indicates this is the local library
					subfolders: [{
						caption: "", // empty => one section
						images: [] // not used. 
					}]
				},
				// Example of lib with mixed object, svg or Zwibbler object
				/*{
					caption: __("Bibliothèque LabNBook"),
					subfolders: [{
						caption: "", // empty => one section
						images: [
							{ path: zw_path + "/labnbook_shared/Testcarrecercle.txt",title: __("Objet zwibbler de test") },
							{ path: zw_path + "/labnbook_shared/fleche-1.svg",title: __("SVG classique") },
						] 
					}]
				}*/
			];
		
		scope.imageFolders = IMAGE_FOLDERS;
		scope.currentImageFolder = IMAGE_FOLDERS[0];
		scope.currentImageSubfolder = IMAGE_FOLDERS[0].subfolders[0];

		});

		scope.getImage = (url) => {
			// [Diff] Tricks to use element with path 
			if(url.path){
				url = url.path;
			}
			return GetImage(urlMap, ctx, url, () => {
				ctx.digest();
			}).dataURI
		}
		
		scope.snapshot = () => {
			const img = document.createElement("img");
			img.src = scope.ctx.save("png");
			document.body.appendChild(img);
		}

		// -------------------------------------------------------------------
		// When the edit text box is shown by Zwibbler, pop up the symbol panel
		scope.editElement = null;
		scope.ctx.on("edit-text-shown", function (elem) {
			scope.editElement = elem;
		});
	
		scope.ctx.on("edit-text-hidden", function () {
			scope.editElement = null;
			symbolDialog.hide();
		});

		// @ts-ignore
		const symbolDialog = Zwibbler.Dialog(zel.querySelector(".symbol-popup"));
		scope.toggleSymbolPopup = (buttonEl) => {
			if (symbolDialog.isShown()) {
				symbolDialog.hide();
			} else {
				symbolDialog.show(buttonEl, "bl tl", false, false);
			}
		}

		scope.symbolClicked = (e) => {
			if (e.target instanceof HTMLAnchorElement) {
				let text = e.target.textContent;
				var pos = GetCaret(scope.editElement);
				var newText = scope.editElement.value.substr(0, pos) + text +
					scope.editElement.value.substr(pos);
				scope.editElement.value = newText;
				SetSelectionRange(scope.editElement, pos + text.length, pos +
					text.length);

			}
		}

		function SetSelectionRange(e, start, end) {
			e.focus();
			if (e.setSelectionRange) {
				e.setSelectionRange(start, end);
			} else if (e.createTextRange) {
				e = e.createTextRange();
				e.collapse(true);
				e.moveEnd('character', end);
				e.moveStart('character', start);
				e.select();
			}
		}

		function GetCaret(el) {
			if (el.selectionStart) {
				return el.selectionStart;
			}
			return 0;
		}

		scope.hideIfPen = (name) => {
			if (scope.ctx.getCurrentTool() === "brush") {
				scope.hidePopup(name);
			}
		}

		let currentTool = "pick";
		scope.ctx.on("tool-changed", (name) => {
			currentTool = name;
		})

		scope.useTool = (name) => {
			// if already using the tool, use pick tool. Otherwise, switch to the tool.
			if (name === currentTool) {
				scope.ctx.usePickTool();
				return;
			}

			switch (name) {
				case "rectangle":
					scope.ctx.useRectangleTool();
					break;
				case "circle":
					scope.ctx.useCircleTool();
					break;
				case "line":
					scope.ctx.useLineTool();
					break;
				case "curve":
					scope.ctx.useCurveTool();
					break;
				case "arrow":
					scope.ctx.useArrowTool();
					break;
				case "brush":
					scope.ctx.useBrushTool();
					break;
				case "text":
					scope.ctx.useTextTool();
					break;
				default:
					throw new Error(`Invalid tool name: ${name}`);
			}
			currentTool = name;
		}

		ctx.on("edit-text-shown", () => {
			console.log("ctx.summary", ctx.summary);
			scope.editingText = true;
			ctx.clearSelection();
			scope.$digest();
		})

		ctx.on("edit-text-hidden", () => {
			scope.editingText = false;
			scope.$digest();
		})

		window.addEventListener('drawing-save-required', async event => {
			let id_ld = $("zwibbler").parent().data("attributes").id_labdoc;
			if (event.detail.id_ld == id_ld) {
				ctx.stopEditingText(true);
				ctx.save();
				await saveLocalLDVersion(id_ld);
				window.dispatchEvent(new CustomEvent('drawing-content-saved', { detail: { id_ld: id_ld, acknowledgement: true } }));
			}
		}, {once: true});

		scope.myStampTool = (url, folder = false) => {
			// [Diff] Little tricks to use same function local or classic image (with path or not)
			if(url.path) {
				url = url.path;
			}
			if (!IsClipboardUrl(url)) {
				ctx.useStampTool({ url: url, lockAspectRation: true }, false);
				return;
			}
			let data = GetImage(urlMap, ctx, url, () => { });
			ctx.useCustomTool(new StampClipboardTool(ctx, data.clipData, data.dataURI));			
		}

		scope.addDocumentClip = () => {
			AddDocumentClip(ctx, scope.imageFolders);
		}

		scope.removeDocumentClip = (url) => {
			// url is something like clip:
			if (confirm(ARE_YOU_SURE)) {
				RemoveDocumentClip(ctx, scope.imageFolders, url, urlMap);
			}
		}

		// When a file is opened, refresh the local saved clips
		ctx.on("document-opened", () => {
			ListDocumentClips(ctx, scope.imageFolders);
		});
		
		scope.downloadLibrary = () => {
			DownloadClips(ctx);
		}
		
		scope.uploadLibrary = () => {
			UploadClips(ctx).then(() => {
				ListDocumentClips(ctx, scope.imageFolders);
			})
		}

		scope.exportSelection = () => {
			ExportSelection(ctx);
		}

		// Initialize the hint to hold alt, which will show whenever we are dragging.
		let dragging = false;
		scope.showDragHint = false;
		ctx.setCustomMouseHandler({
			onMouseDown: function () {
				dragging = true;
			},

			onMouseUp: function () {
				dragging = false;
				scope.showDragHint = false;
			},

			onMouseMove: function () {
				if (dragging) {
					scope.showDragHint = true;
				}
			}
		})
	});
});


function MakeMoveable(element, movefn) {
	let dragging = false;
	let startX = 0, startY = 0;
	let cornerX = 0, cornerY = 0;
	function ondown(e) {
		dragging = true;
		startX = e.pageX;
		startY = e.pageY;
		cornerX = element.offsetLeft;
		cornerY = element.offsetTop;
	}

	function onmove(e) {
		if (!e.buttons) {
			dragging = false;
		}
		if (dragging) {
			let diffX = e.pageX - startX;
			let diffY = e.pageY - startY;
			movefn(cornerX + diffX, cornerY + diffY);
		}
	}

	element.addEventListener("mousedown", ondown);
	document.addEventListener("mousemove", onmove);

	return () => {
		document.removeEventListener("mousemove", onmove);
		element.removeEventListener("mousedown", ondown);
	}
}


/**
 * Handle the resizing of the canvas vertically. When the mouse is
 * pressed down, we record its position. As it moves, we increase or
 * decrease the height up to the limit. When the button is released, we
 * stop dragging.
 * @param ctx
 * @constructor
 */
function MakeSizingBar(ctx) {
	var resizing = false;
	var ry = 0, osize = 0;
	let element = ctx.getElement().querySelector(".sizing-bar")
	let zwibEl = ctx.getElement();

	if (!element) {
		return; // non-editing mode
	}

	function mousedown(e) {
		resizing = true;
		ry = e.pageY;
		osize = zwibEl.offsetHeight;
	}

	// The move and up events must be placed on the window, because when
	// the mouse moves it may briefly leave the sizing bar.
	function mousemove(e) {
		if (resizing) {
			var diff = e.pageY - ry;
			var newsize = Math.max(osize + diff, 60);

			zwibEl.style.height = newsize + "px";
			ctx.resize();
		}
	}

	function mouseup(e) {
		resizing = false;
	}

	element.addEventListener("mousedown", mousedown);
	document.addEventListener("mousemove", mousemove);
	document.addEventListener("mouseup", mouseup);

	ctx.on("destroy", () => {
		element.removeEventListener("mousedown", mousedown);
		document.removeEventListener("mousemove", mousemove);
		document.removeEventListener("mouseup", mouseup);
	})
}

/**
 * @param {string} url
 * @return {boolean}
 */
function IsClipboardUrl(url) {
	return url.substr(url.length - 4).toLowerCase() === ".txt" || url.startsWith("clip:");
}

/**
 * @typedef {Object} ImageClipData
 * @property {string} dataURI
 * @property {string} clipData
 *
 * @typedef {{[key:string]:ImageClipData|null}} UrlMap
 */

// This is a map from the .txt file to some information about
// converting it to an iamge.
// If the value is null, then we are still retrieving the image.
// If the value is a string, then that is the completed DATA-URI taht
// is the zwibbler rendering of the file.

/**
    This method takes a url and returns the ImageClipDAta with the url that should be shown in the 
    image library.
    If the url does not end in .txt, or start with clip: it returns the string.
    Otherwise, return null, retrieve the file, use Zwibbler to convert to image, and
    then call the callback function.
    @param {UrlMap} urlMap
    @param {string} url 
    @param {()=>void} callback
    @returns {ImageClipData}
 */
function GetImage(urlMap, ctx, url, callback) {
	if (!IsClipboardUrl(url)) {
		return { dataURI: url, clipData: "" };
	}
	let clipdata = "";

	if (!(url in urlMap)) {
		urlMap[url] = { dataURI: "", clipData: "" };

		let promise;
		if (url.startsWith("clip")) {
			// get clipdata from document properties.
			promise = new Promise((resolve) => {
				resolve(ctx.getDocumentProperty(url));
			})
		} else {
			// url is a .txt file with the clipdata.
			promise = fetch(url).then((response) => {
				return response.text();
			})
		}

		promise.then((text) => {
			clipdata = text;
			//@ts-ignore
			return Zwibbler.save(text, {
				format: "png",
				encoding: "data-uri",
			})
		}).then((dataURI) => {
			// if: handle case of it being deleted by user while we are retrieving it.
			if (url in urlMap) {
				urlMap[url] = {
					dataURI: dataURI,
					clipData: clipdata,
				};
				callback();
			}
		})
	}

	// we have retrieved it, or are in the process of retrieving it.
	return urlMap[url];
}
/*
 * Get the id of the mission contained in the global variable global_id_mission in the page /report/<id> or lb_mission_edit.php
 * @returns {String}
 */
function getCurrentMissionId() {
	if (typeof(global_id_mission) !== 'undefined') {
		return global_id_mission ;
	}
	else {
		return "default" ; // this should not be used
	}
}

/**
 * calls Zwibbler and display in the div $("#labdoc_content_"+id_ld)
 * a drawing based on the ld_content (plain text)
 * if !edition, display an image given by Zwibbler
 * if edition, display Zwibbler in edition mode, with all the options
 * @param id_ld
 * @param edition
 * @param teacher_mode
 * @return {boolean}
 */
function displayDrawing(id_ld, edition, teacher_mode) {
	let ld_content = getLDContentFromLocalHistory(id_ld);
	if (ld_content != "") { // check the validity of the json format
		try {
			 JSON.parse(ld_content.replace(/^zwibbler3\./,"")) ;
		} catch (e) {
			$("#labdoc_content_"+id_ld).html(__("Le code JSON du labdoc est corrompu - affichage impossible")) ;
			return false ; 
		}
	}
	let focused_element = document.activeElement;
	if (focused_element.tagName === 'IFRAME') {
		focused_element = focused_element.contentWindow.document.activeElement;
	}
	var zw_context = displayZwibbler(ld_content,"labdoc_content_"+id_ld, edition, teacher_mode);

	if (!edition) { // display a canvas with no option to edit it
		focused_element.focus();
		let canvas = Zwibbler.createPreview(zw_context.save(), {});
		$("#labdoc_content_"+id_ld).html(canvas);
	}
	else {
		zw_contexts[id_ld] = zw_context; // global var for labdoc.js
		// resizeLDWindowForZwibbler(id_ld, zw_context);

		// Handle image uploads to the server. When the upload begins, display a notification and update it.
		$("#fileinput").on("change", function() {
			console.log("upload an img");
			let form = this.parentNode;
			uploadImage(this).resizeImageToFormData().then(function(formData){
				upload(form, formData, zw_context);
				form.reset();
			});
		});

		// Display multiple upload progress notifications
		ProgressNotification.all = [];
		ProgressNotification.prototype = {
			update: function(percent) {
				this.div.text(this.name + "... " + Math.round(percent * 100) + "%");
			},

			error: function(message) {
				var self = this;
				var input = $("<input>").
					attr("type", "button").
					val("OK");

				input.click(function(e) {
					self.done();
				});

				this.div.html(this.name + "... " +  message);
				this.div.append(input);
			},

			done: function() {
				this.div.remove();
				var all = ProgressNotification.all;
				for(var i = 0; i < all.length; i++) {
					if (all[i] === this) {
						all.splice(i, 1);
						break;
					}
				}

				if (all.length === 0) {
					$("#zw_progress").hide();
				}
			}

		}

		// save on each modification
		setTimeout(function() { // to avoid a first save when editing
			zw_context.on("document-changed", function() {
				// console.log("zw save", id_ld);
				saveLocalLDVersion(id_ld) ;
			});
		}, 0);
	}
}


//@ts-ignore
Zwibbler.directive("palette", (info) => {
	info.element.style.lineHeight = "0";
	info.element.style.width = "320px";
	info.scope.ctx.generatePalette(info.element, 20, {
		onColour(clr) {
			info.scope.ctx.setProperty(info.scope.colourProperty, clr);
		},

		onOpacity(value) {
			let colour = info.scope.ctx.summary.properties[info.scope.colourProperty] || "#000"
			//@ts-ignore
			colour = Zwibbler.setColourOpacity(colour, value);
			info.scope.ctx.setProperty(info.scope.colourProperty, colour);
		}
	})
})

//@ts-ignore
Zwibbler.directive("moveable", (info) => {
	if (info.scope.toolsX) {
		info.element.style.right = "";
		info.element.style.left = info.scope.toolsX + "px";
		info.element.style.top = info.scope.toolsY + "px";
	}
	info.destructor(MakeMoveable(info.element, (x, y) => {
		info.element.style.right = "";
		info.element.style.left = x + "px";
		info.element.style.top = y + "px";
		info.scope.toolsX = x;
		info.scope.toolsY = y;
	}));
})

/** This adds a directive, "z-zoom-percentage"
When applied to an input element, it lets the
user type in the zoom percentage. It also 
synchronizes with Zwibbler's existing zoom level.
 */
//@ts-ignore
Zwibbler.directive("zoom-percentage", (info) => {
	// info is a special object that lets us
	// get information about the element and assign
	// event handlers that will be automatically removed when
	// Zwibbler is destroyed, preventing any memory leaks.
	let ctx = info.scope.ctx;

	/** @type {HTMLInputElement} */
	let element = info.element;

	// called when the zoom level changed in zwibbler.
	function onzoomchanged() {
		let level = Math.round(ctx.getCanvasScale() * 100) + "%";
		element.value = level;
	}

	// The scroll event is fired whenever the zoomlevel changes.
	// https://zwibbler.com/docs/#scroll
	// this is not automatically removed, so we also define a method
	// using the destructor operation.
	ctx.on("scroll", onzoomchanged);
	info.destructor(() => {
		ctx.removeListener("scroll", onzoomchanged);
	})

	// When the user presses "Enter", then validate what she typed and
	// set the zoom level.
	function applyZoom() {
		let percentage = parseFloat(element.value);
		if (percentage >= 5 && percentage < 1000) {
			ctx.setZoom(percentage / 100);
		} else {
			// invalid value typed; replace with what Zwibbler has.
			onzoomchanged();
		}
	}

	// When the user presses "Enter", then validate what she typed and
	// set the zoom level.
	info.listen("keypress", (e) => {
		if (e.key === "Enter") {
			applyZoom();
		}
	})

	info.listen("blur", applyZoom)
});

function initializeTemplate() {
	// find Zwibbler and take it out until needed.
	if (!template) {
		template = document.querySelector("zwibbler");
	}
}

let template;
window.addEventListener("DOMContentLoaded", () => {
	initializeTemplate();
	if (template && template.parentNode) {
		template.parentNode.removeChild(template);
	}
});

/**
 *  @param {string} content : the json that represents the drawing
 *  @param {string} selector : id of the div that contains Zwibbler
 *  @param {boolean} edition : if false, display the drawing without the toolbar and the grid
 *  @param {boolean} teacher_mode : if false, do not display the menu for changing layers
 */
function displayZwibbler(content, selector, edition, teacher_mode) {

	initializeTemplate();

	if (!(template instanceof HTMLElement)) {
		throw new Error("Cannot find Zwibbler element in HTML")
	}

	let container = document.querySelector(`#${selector}`);
	if (!(container instanceof HTMLElement)) {
		throw new Error(`Could not find container #${selector}`);
	}

	let html = template.cloneNode(true);

	// Modification for LabNBook : add form upload image in edition mode
	container.innerHTML = (edition ?
		'<form style="display:none" method=post enctype="multipart/form-data" action="/tool_zwibbler/image_mgmt.php?action=upload&id_mission='+getCurrentMissionId()+'">'+'<input type=file name=file id=fileinput accept="image/*"></form>'
		: '');
	container.append(html);

	//@ts-ignore
	let scope = Zwibbler.attach(html, {
		edition: edition,
		teacher_mode: teacher_mode,
	});

	if (content) {
		scope.ctx.load(content);
	}
	setTimeout(() => {
		scope.ctx.resize();
	}, 100);

	return scope.ctx;
}

// resize Zwibbler in edition mode
/* seems to be useless for the new version of Zwibbler
function resizeLDWindowForZwibbler(id_ld, ctx) {
	var documentRect = ctx.getDrawingRectangle();
	var dom = $("#labdoc_content_"+id_ld+" zwibbler");
	var margin = 100;
	var height = documentRect.height + documentRect.y + margin;
	var width = Math.min(documentRect.width + documentRect.x + margin, 0.9*window.innerWidth);
	if (height > dom.height()) { dom.height(height); }
	if (width > dom.width()) { dom.width(width);}
	ctx.resize();
	ctx.setConfig('background', 'grid');
}*/

class StampClipboardTool {
	constructor(zwibblerContext, clipdata, imgUrl) {
		// initialize any variables here here.
		this.ctx = zwibblerContext;
		this.clipdata = clipdata;
		this.imgUrl = imgUrl;
		this.img = document.createElement("img");
		this.img.src = imgUrl;

		// redraw when the image eventually loads
		this.img.onload = () => {
			this.ctx.redraw();
		}
	}

	enter() {
		this.ctx.setCursor("none");
	}

	leave() {
		// Called by Zwibbler when a different tool is selected
		// for any reason. Clean up here.

		// Redraw is necessary in this tool only because we have drawn the
		// "ghost text" in the onMouseMove function and we should not leave it
		// on the screen.
		this.ctx.redraw();
	}

	onMouseClick(x, y, e) {
		if (!this.img.complete) return;
		// The x, y coordinates are coordinates in the document, taking into 
		// account zooming and scrolling. "e" is the DOM event that caused the 
		// mouse click.
		this.ctx.begin();
		let nodes = this.ctx.paste(this.clipdata);
		if (nodes.length > 1) {
			this.ctx.createGroup(nodes)
		}
		this.ctx.commit();

		// wait for accurate size of pasted data
		let rect = this.ctx.getNodeRectangle(nodes);
		this.ctx.begin();
		this.ctx.translateNode(nodes, x - this.img.naturalWidth / 2 - rect.x, y - this.img.naturalHeight / 2 - rect.y);
		this.ctx.commit(true); // skip undo stack for the shifting.
		this.ctx.usePickTool();
	}

	onMouseMove(x, y, e) {
		var self = this;
		this.ctx.redraw((canvas) => {
			if (!this.img.complete) return;

			canvas.save();
			canvas.opacity = 0.5;
			canvas.drawImage(this.img, x - this.img.naturalWidth / 2, y - this.img.naturalHeight / 2);
			canvas.restore();
		});
	}
}


// When the upload is done either successfully or due to an error, this function is called.
// In the successful case, the status is set to "ok". Otherwise it is set to an error message.
// The result contains the JSON decoded response from the server. 

// This function must then insert the image into the document.
function uploadDone(status, result, ctx) {
	console.log("uploadDone: ", status, result);
	if (result.status === "ok") {
		let url = "/tool_zwibbler/image_mgmt.php?action=get&fileid="+result.fileid+"&id_mission="+getCurrentMissionId();
		ctx.beginTransaction();
		let nodeId = ctx.createNode("ImageNode", { url: url });
		ctx.translateNode(nodeId, 100, 100);
		ctx.commitTransaction();
	}
	else {
		alert(result.status);
	}
}

function upload(form, formData, ctx) {
	var progress = new ProgressNotification("Reading file");
	var xhr = new XMLHttpRequest();

	xhr.upload.addEventListener("progress", 
		function( e ) {
			progress.update( e.loaded / e.total );
		}, false
	);

	xhr.addEventListener("load", 
		function( e ) {
			progress.done();
			uploadDone( "ok", $.parseJSON( xhr.response ), ctx );
		}, false
	);

	xhr.addEventListener("error", 
		function( e ) {
			progress.error("Error");
			uploadDone( "error", null, ctx );
		}, false
	);

	xhr.addEventListener("abort", 
		function( e ) {
			progress.error("Aborted");
			uploadDone( "aborted", null, ctx );
		}, false
	);

	xhr.open(form.method, form.action);
	xhr.send(formData);
}

// Display multiple upload progress notifications
function ProgressNotification(name) {
	this.name = name;
	ProgressNotification.all.push(this);
	this.div = $("<div>");
	$("#zw_progress").append(this.div).show();
	this.update(0);
}
/**
 * @param {ZwibblerContext} ctx
 * @param {ImageFolder[]} folders
 */
function ListDocumentClips(ctx, folders) {
	/** @type {string[]} */
	let imageList = [];

	// find and clear the folder
	for (let folder of folders) {
		if (folder.local) {
			imageList = folder.subfolders[0].images = [];
		}
	}

	// find the local folder and append all the images.
	let properties = ctx.getDocumentProperties();
	for (let key in properties) {
		if (key.startsWith("clip:")) {
			imageList.push(key);
		}
	}

	imageList.sort();
}

/**
 * @param {ZwibblerContext} ctx
 * @param {ImageFolder[]} folders
 */
function AddDocumentClip(ctx, folders) {
	if (ctx.getSelectedNodes().length === 0) return;

	// get the selection as clipboard data string.
	let clipdata = ctx.copy(true);

	// find the next unused slot
	let key = `clip:${new Date().getTime().toString(36)}`;

	// store it there.
	ctx.begin();
	ctx.setDocumentProperty(key, clipdata);
	ctx.commit(true);

	// prepend the key to the local image list.
	for (let folder of folders) {
		if (folder.local) {
			folder.subfolders[0].images = folder.subfolders[0].images.concat();
			folder.subfolders[0].images.unshift(key);
		}
	}
	ctx.digest();
}

/**
 * @param {ZwibblerContext} ctx
 * @param {ImageFolder[]} folders
 * @param {string} name
 * @param {UrlMap} urlData
 */
function RemoveDocumentClip(ctx, folders, name, urlData) {
	ctx.begin();
	ctx.setDocumentProperty(name, undefined);
	ctx.commit(true);

	// remove the key from the local image list.
	for (let folder of folders) {
		if (folder.local) {
			for (let i = 0; i < folder.subfolders[0].images.length; i++) {
				if (folder.subfolders[0].images[i] === name) {
					folder.subfolders[0].images = folder.subfolders[0].images.concat();
					folder.subfolders[0].images.splice(i, 1);
					break;
				}
			}
		}
	}

	delete urlData[name];
}

/**
 * @param {ZwibblerContext} ctx
 * */
function DownloadClips(ctx) {
	let clips = {};
	let properties = ctx.getDocumentProperties();
	console.log("doc properties: ", properties);
	for (let key in properties) {
		if (key.startsWith("clip:")) {
			clips[key] = properties[key];
		}
	}

	let filename = prompt(ENTER_FILENAME, DEFAULT_LIBRARY_FILENAME);

	if (filename) {
		DEFAULT_LIBRARY_FILENAME = filename;
		if (!filename.endsWith(LIBRARY_FILE_EXTENSION)) {
			filename = filename + LIBRARY_FILE_EXTENSION;
		}

		//@ts-ignore
		Zwibbler.downloadBlob(new Blob([JSON.stringify(clips, null, 2)], { type: "text" }), filename)
	}
}


/**
 * @param {ZwibblerContext} ctx
 * */
function UploadClips(ctx) {
	return ctx.openFile({
		format: "text",
		accept: LIBRARY_FILE_EXTENSION,
	}).then((response) => {
		let properties;
		try {
			properties = JSON.parse(response.data);
		} catch (e) {
			alert(ERROR_OPENING_LIBRARY);
			return;
		}

		ctx.beginTransaction();
		// console.log("Importing", properties);
		for (let key in properties) {
			if (key.startsWith("clip:")) {
				ctx.setDocumentProperty(key, properties[key]);
			}
		}
		ctx.commit(false);
	})
}

function ExportSelection(ctx) {
	let clip = ctx.copy(true);
	let filename = prompt("Export selection as filename.txt:", "data.txt");
	if (filename) {
		if (!filename.endsWith(".txt")) {
			filename = filename + ".txt";
		}
		//@ts-ignore
		Zwibbler.downloadBlob(new Blob([clip], { type: "text" }), filename)
	}
}
