<?php

/**
 * @license http://www.gnu.org/licenses/gpl-3.0.html  GNU GPL v3
 */
namespace Legacy;

use Illuminate\Support\Facades\DB as LaravelDB;

/**
 * Class that wraps accesses to the MySQL DB.
 */
class DB
{
	/**
	 * Execute a query so that its result can be fetched.
	 *
	 * $r = Legacy\DB::query("SELECT * FROM labdoc WHERE id_report = ? AND id_ld_origin = ?", [4, null]);
	 * $rows = $r->fetchAll(); // array of assoc arrays
	 * $objects = $r->fetchAll(PDO::FETCH_OBJ); // array of objects
	 * $singleColumn = $r->fetch(PDO::FETCH_OBJ)->id_labdoc; // string|null value of this column on the first row
	 *
	 * @param string $sql
	 * @param array $args (optional)
	 * @return PDOStatement
	 */
	public static function query($sql, $args = [])
	{
		$pdo = self::getPdo();
		try {
			if ($args) {
				$sth = $pdo->prepare($sql);
				if ($sth) {
					$sth->execute($args);
				}
			} else {
				$sth = $pdo->query($sql);
			}
		} catch (\PDOException $ex) {
			self::logError($ex, $sql, $args);
			$message = config('app.debug') ?
				htmlspecialchars($ex->getMessage() . "\n$sql\nArguments : " . print_r($args, true))
				: "Une erreur s'est produite en accédant à la base de données. Elle a été signalée aux administrateurs de la plateforme.";
			throw new \Exception($message);
		}
		return $sth;
	}

	/**
	 * Return a single value (string or null) from a SQL query.
	 *
	 * $value = Legacy\DB::queryScalar("SELECT avg(allow_attach_ld) FROM report");
	 *
	 * @param string $sql
	 * @param array $args (optional)
	 * @return string|boolean False if no result, else string|null.
	 */
	public static function queryScalar($sql, $args = [])
	{
		try {
			$sth = self::query($sql, $args);
			$row = $sth->fetch(\PDO::FETCH_NUM);
		} catch (\PDOException $ex) {
			self::logError($ex, $sql, $args);
			$message = config('app.debug') ?
				htmlspecialchars($ex->getMessage() . "\n$sql\nArguments : " . print_r($args, true))
				: "Une erreur s'est produite en accédant à la base de données. Elle a été signalée aux administrateurs de la plateforme.";
			throw new \Exception($message);
		}
		if ($row) {
			return $row[0];
		} else {
			return false;
		}
	}

	/**
	 * Execute a query with no fetchable content (UPDATE, etc) and return the number of rows affected.
	 *
	 * $numChanges = Legacy\DB::exec("UPDATE labdoc SET name = 'x' WHERE id_report = :id", [':id' => 4]);
	 *
	 * @param string $sql
	 * @param array $args (optional)
	 * @return int
	 */
	public static function exec($sql, $args = [])
	{
		try {
			if ($args) {
				$sth = self::getPdo()->prepare($sql);
				$sth->execute($args);
				return $sth->rowCount();
			} else {
				return self::getPdo()->exec($sql);
			}
		} catch (\PDOException $ex) {
			self::logError($ex, $sql, $args);
			$message = config('app.debug') ?
				htmlspecialchars($ex->getMessage() . "\n$sql\nArguments : " . print_r($args, true))
				: "Une erreur s'est produite en accédant à la base de données. Elle a été signalée aux administrateurs de la plateforme.";
			throw new \Exception($message);
		}
	}

	/**
	 * Return the ID from the last INSERT.
	 *
	 * @return string
	 */
	public static function getPdo()
	{
		return LaravelDB::getPdo();
	}



	/**
	 * Return the ID from the last INSERT.
	 *
	 * @return string
	 */
	public static function getLastInsertId()
	{
		return self::getPdo()->lastInsertId();
	}

	/**
	 * @param \PDOException $ex
	 * @param string $sql
	 * @param array $params
	 */
	protected static function logError(\PDOException $ex, $sql, $params)
	{
		\App\Helper::logIntoFile(
			sprintf("\nMySQL error: [%s] %s\nSQL: %s", $ex->getCode(), $ex->getMessage(), $sql)
				. ($params ? "\nParams: " . var_export($params, true) : ""),
			'mysql_errors.log'
		);
	}
}
