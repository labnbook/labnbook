//const acornStage3 = require('acorn-stage3');
module.exports = {
  contextSeparator: '_',
  createOldCatalogs: true,
  defaultNamespace: 'translation',
  defaultValue: '',
  indentation: 2,
  keepRemoved: false,
  // Keep keys from the catalog that are no longer in code

  keySeparator: false,
  // Key separator used in your translation keys
  // If you want to use plain english keys, separators such as `.` and `:` will conflict. You might want to set `keySeparator: false` and `namespaceSeparator: false`.
  //That way, `t('Status: Loading...')` will not think that there are a namespace and three separator dots for instance.

  // see below for more details
  lexers: {
    hbs: ['HandlebarsLexer'],
    handlebars: ['HandlebarsLexer'],

    htm: ['HTMLLexer'],
    html: ['HTMLLexer'],

    js: [{
      lexer: 'JavascriptLexer',
      functions: ['__']
    }], // if you're writing jsx inside .js files, change this to JsxLexer
    jsx: ['JsxLexer'],
    mjs: ['JavascriptLexer'],

    ts: ['TypescriptLexer'],
    tsx: ['TypescriptLexer'],

    default: ['JavascriptLexer']
  },

  lineEnding: 'lf',
  locales: ['en', 'fr'],
  // An array of the locales in your applications

  namespaceSeparator: false,
  output: undefined,
  input: undefined,
  // An array of globs that describe where to look for source files
  // relative to the location of the configuration file

  reactNamespace: false,
  sort: false,  
  // Whether or not to sort the catalog

  useKeysAsDefaultValue: false,
  verbose: true
  // Display info about the parsing including some stats
}
