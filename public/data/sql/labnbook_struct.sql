-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Hôte : mariadb
-- Généré le : jeu. 03 mars 2022 à 16:40
-- Version du serveur : 10.6.5-MariaDB-1:10.6.5+maria~focal
-- Version de PHP : 8.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `labnbook`
--

-- --------------------------------------------------------

--
-- Structure de la table `annotation`
--

CREATE TABLE `annotation` (
  `id_annotation` int(10) UNSIGNED NOT NULL,
  `id_teacher` int(10) UNSIGNED DEFAULT NULL,
  `id_report` int(10) UNSIGNED NOT NULL,
  `id_labdoc` int(10) UNSIGNED DEFAULT NULL,
  `id_report_part` int(10) UNSIGNED DEFAULT NULL,
  `json` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(30) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `creation_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `popularity` smallint(6) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `class`
--

CREATE TABLE `class` (
  `id_class` int(10) UNSIGNED NOT NULL,
  `id_inst` int(11) DEFAULT NULL,
  `class_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class_code` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `creation_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` enum('normal','archive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'normal',
  `ext_code` varchar(255) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL COMMENT 'Class identifier in external institution',
  `id_extplatform` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


--
-- Déchargement des données de la table `class`
--

INSERT INTO `class` (`id_class`, `id_inst`, `class_name`) VALUES (1, 1, 'Ens. Mon institution');


-- --------------------------------------------------------

--
-- Structure de la table `comment`
--

CREATE TABLE `comment` (
  `id_comment` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `id_labdoc` int(10) UNSIGNED NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment_time` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `conversation`
--

CREATE TABLE `conversation` (
  `id_conversation` int(10) UNSIGNED NOT NULL,
  `id_report` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `extplatform`
--

CREATE TABLE `extplatform` (
  `id_extplatform` int(11) NOT NULL,
  `id_inst` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `secret` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `institution`
--

CREATE TABLE `institution` (
  `id_inst` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_class_teacher` int(10) UNSIGNED DEFAULT NULL,
  `cas` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `institution`
--

INSERT INTO `institution` (`id_inst`, `name`, `id_class_teacher`, `cas`) VALUES
(1, 'MonInstitution', 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `labdoc`
--

CREATE TABLE `labdoc` (
  `id_labdoc` int(10) UNSIGNED NOT NULL,
  `id_report_part` int(10) UNSIGNED DEFAULT NULL,
  `id_report` int(10) UNSIGNED DEFAULT NULL,
  `id_ld_origin` int(10) UNSIGNED DEFAULT NULL,
  `type_labdoc` enum('text','drawing','dataset','procedure') COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` int(10) UNSIGNED DEFAULT NULL,
  `editable` tinyint(1) NOT NULL DEFAULT 1,
  `editable_name` int(1) NOT NULL DEFAULT 1,
  `duplicatable` tinyint(1) NOT NULL DEFAULT 1,
  `deleteable` int(1) NOT NULL DEFAULT 1,
  `draggable` int(1) NOT NULL DEFAULT 1,
  `locked` int(11) DEFAULT NULL,
  `last_editor` int(10) UNSIGNED DEFAULT NULL,
  `last_edition` int(11) DEFAULT NULL,
  `labdoc_data` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `draft` tinyint(1) NOT NULL DEFAULT 0,
  `deleted` timestamp NULL DEFAULT NULL,
  `creation_time` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `labdocfile`
--

CREATE TABLE `labdocfile` (
  `filehash` binary(20) NOT NULL,
  `id_labdoc` int(10) UNSIGNED NOT NULL,
  `id_mission` int(10) UNSIGNED NOT NULL,
  `uploadtime` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `labdoc_backup`
--

CREATE TABLE `labdoc_backup` (
  `id_labdoc` int(10) NOT NULL,
  `last_edition` int(11) DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_hash` char(32) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `labdoc_status`
--

CREATE TABLE `labdoc_status` (
  `id_labdoc` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `moved` tinyint(1) NOT NULL DEFAULT 0,
  `drafted` tinyint(1) NOT NULL DEFAULT 0,
  `modified` int(1) NOT NULL DEFAULT 1,
  `mod_icon` int(1) NOT NULL DEFAULT 1,
  `extend` int(1) NOT NULL DEFAULT 1,
  `com_viewed` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `labdoc_stdalone`
--

CREATE TABLE `labdoc_stdalone` (
  `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_labdoc` enum('text','drawing','dataset','procedure') COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_modif` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `labdoc_data` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `link_annotation_learner`
--

CREATE TABLE `link_annotation_learner` (
  `id_annotation` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `link_class_learner`
--

CREATE TABLE `link_class_learner` (
  `id_class` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `link_class_learner`
--

INSERT INTO `link_class_learner` (`id_class`, `id_user`) VALUES
(1,1);


-- --------------------------------------------------------

--
-- Structure de la table `link_class_teacher`
--

CREATE TABLE `link_class_teacher` (
  `id_teacher` int(10) UNSIGNED NOT NULL,
  `id_class` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `link_class_tteam`
--

CREATE TABLE `link_class_tteam` (
  `id_class` int(10) UNSIGNED NOT NULL,
  `id_teacher_team` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `link_conversation_user`
--

CREATE TABLE `link_conversation_user` (
  `id_conversation` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `last_seen` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `link_extplatform_user`
--

CREATE TABLE `link_extplatform_user` (
  `id_extplatform` int(11) NOT NULL,
  `id_user_ext` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `link_inst_teacher`
--

CREATE TABLE `link_inst_teacher` (
  `id_teacher` int(11) UNSIGNED NOT NULL,
  `id_inst` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


--
-- Déchargement des données de la table `link_inst_teacher`
--

INSERT INTO `link_inst_teacher` (`id_teacher`, `id_inst`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `link_mission_teacher`
--

CREATE TABLE `link_mission_teacher` (
  `id_mission` int(10) UNSIGNED NOT NULL,
  `id_teacher` int(10) UNSIGNED NOT NULL,
  `teacher_type` enum('teacher','designer') COLLATE utf8mb4_unicode_ci DEFAULT 'designer'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `link_mission_tteam`
--

CREATE TABLE `link_mission_tteam` (
  `id_mission` int(10) UNSIGNED NOT NULL,
  `id_teacher_team` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `link_report_learner`
--

CREATE TABLE `link_report_learner` (
  `id_report` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `last_synchro` int(11) DEFAULT NULL,
  `reload_annotations` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `link_teacher_tteam`
--

CREATE TABLE `link_teacher_tteam` (
  `id_teacher` int(10) UNSIGNED NOT NULL,
  `id_teacher_team` int(10) UNSIGNED NOT NULL,
  `status` enum('member','manager') CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT 'member'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `link_upload_context`
--

CREATE TABLE `link_upload_context` (
  `id_upload` int(10) UNSIGNED NOT NULL,
  `id_labdoc` int(10) UNSIGNED DEFAULT NULL,
  `id_ressource` int(10) UNSIGNED DEFAULT NULL,
  `id_mission` int(10) UNSIGNED DEFAULT NULL,
  `type_mission` enum('assignment','description') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_report_part` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `maillog`
--

CREATE TABLE `maillog` (
  `to` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hash` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sent_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `message`
--

CREATE TABLE `message` (
  `id_message` int(10) UNSIGNED NOT NULL,
  `msg_content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `msg_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `id_user_sender` int(10) UNSIGNED DEFAULT 1,
  `id_labdoc` int(10) UNSIGNED DEFAULT NULL,
  `id_conversation` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `message_var`
--

CREATE TABLE `message_var` (
  `id_user` int(10) UNSIGNED NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT 1,
  `pos_x` smallint(6) UNSIGNED NOT NULL,
  `pos_y` smallint(6) UNSIGNED NOT NULL,
  `width` smallint(6) UNSIGNED NOT NULL,
  `height` smallint(6) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `migration`
--

CREATE TABLE `migration` (
  `id` binary(20) NOT NULL,
  `filename` varbinary(255) NOT NULL,
  `filehash` binary(20) NOT NULL,
  `filetime` bigint(20) NOT NULL,
  `appliedtime` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migration`
--

INSERT INTO `migration` (`id`, `filename`, `filehash`, `filetime`, `appliedtime`) VALUES
(0x050418a41fe85f9ae5dc369742edf465f14768e3, 0x323031392d30312d32355f31313734365f647261667465642e73716c, 0x5de0b200e4ab463c53252bf1b2a18ab92df82799, 1548752838, 20190205141749),
(0x07219e922ff4c585fd1b8516a8c819a0bd5fc6eb, 0x323032312d30342d30375f313630325f4164645f5475746f7269616c5f4d697373696f6e5f547970652e73716c, 0x3d4b58c84c4b5f0660e32c0237ecc473d5ec6c40, 1617895540, 20210408152556),
(0x07600b0031591aff1e911464076696210f587366, 0x323031392d30342d30325f313732355f7465616d2d636f6e6669672e73716c, 0x7c9283918816394c5320a45a7a16d1a5b53ac6c7, 1538145041, 20181102142908),
(0x0d366c6b4cdbc2fbdb0aec85cd2d8c5fa34cf7a4, 0x323031382d31302d30325f303934325f64656d6f2d726571756973697465732e73716c, 0xd15f65b5e1e10af66bcd1e79af6d5ce5d872277d, 1543307296, 20181127153131),
(0x0f7f78c05761e351aec1220e0623191cb3d84bb6, 0x323032302d30332d32395f313734385f6669785f70625f74696d657374616d702e73716c, 0xc6adba595362d73f83a0eab89e6d0e9137e83a7c, 1591283730, 20200612105827),
(0x0fc3530c8bc981368ff0a1a7e3f94af060616194, 0x323031382d30312d32325f313230305f636f6e766572736174696f6e2d6d73672e73716c, 0x5660c049e905d9551827d0b1f45f15051c6ec937, 1522418594, 20181102142908),
(0x108bb120f69f9af5242a9e4c9880957787173583, 0x323032322d30312d32365f31373033345f74726163655f656469745f7465616d696e672e73716c, 0x5c6217e4694579e5973959f10b0de62d7d28141a, 1644436948, 20220209211350),
(0x14cd969090b9e8eb8fb08b74a588f088f51ab54f, 0x323031382d30332d30315f313635365f6c6162646f6366696c652e73716c, 0xb75d8903edd6357e64a04277db045698ad01f352, 1528338405, 20181102142908),
(0x16b2495929a8a75507a18664ad241de89f2cdc84, 0x323032322d30322d30345f313732315f72656d6f76655f6475706c69636174655f666b2e73716c, 0x46e411eddee9faa71e003857317c7a2cddbaceb2, 1646325571, 20220303173935),
(0x16dbeac738eb31193d47ae3535046f940011d8e9, 0x323032302d30332d33315f313232305f72656d6f76655f726573656172636865725f7374617475732e73716c, 0x17b8b67afb5b60d225f5b374f39225e180c2fb1c, 1593014439, 20200624160042),
(0x19904c80ab438046fa49f63b57426597e90f4c8f, 0x323031392d30312d30385f313434395f68747470732e73716c, 0x52a89c6f22ed51e4521f76256660b04d6fd3cfb4, 1547713508, 20190205141749),
(0x1b66c4b371890ed2e984f54e7d122766894ee2cd, 0x323032312d31302d32305f313134325f6164645f6367755f6669656c645f6f6e5f75736572732e73716c, 0xd1e28f1903c45d775db6158a7f45c37c8e4c6e24, 1634723144, 20211020114548),
(0x1d208d91fb6cbefcddf23e4bdb269fa193d43165, 0x323031392d30322d30315f6c645f6261636b75702e73716c, 0xc46f1c2334b400d5e451cab4d9ce1a92dc65de28, 1550565598, 20190607104844),
(0x1dada0d18c37106a7c2448b81180a46e07ed4b9c, 0x323031382d30312d31315f313230305f7769646765742d6d73672e73716c, 0xa52460ab894d21ce900cf33827ecc8c33ef56d49, 1522418594, 20181102142908),
(0x241d6a3b6db4954c2f71485daa8b797207917ad1, 0x323032302d30392d31365f313334335f6d6f76655f6578745f696e73745f746f5f6578745f706c6174666f726d2e73716c, 0x8a7a16cc677e3b21ffbd898f611d503ca774b8b0, 1600270878, 20200916154249),
(0x2798370267f702988dce802d96d26ad3550bf26a, 0x323031382d30312d32335f313230305f64656275672d636f6d6d656e742e73716c, 0x52d8a202847598cc627bbd65fa097b7c1522f58a, 1522418594, 20181102142908),
(0x2d6cc144facd5e0c07443bdab733573053154c1e, 0x323031382d30392d32375f313733325f72656d6f76652d656d7074792d6772617068732e73716c, 0x9b8c6c868ef7bc51873e24458e60dd333d574dc7, 1538398118, 20181127153131),
(0x2e59e0daea69142a6eb2792370772ef4895fef16, 0x323032302d30392d31375f31333433395f6164645f657874706c6174666f726d5f75726c2e73716c, 0x368917ba77d76a452a443b0aa3fe02ca297fdb45, 1600342787, 20200917114020),
(0x2eae06a13853f20aeac7a90560207002519a59ae, 0x323031382d31312d32325f313335375f64726f705f74726163655f75736572726f6c652e73716c, 0xf99c13d02bd7fbfc63120a726c4134494c202062, 1543307296, 20181127153132),
(0x31647331d90110d48044cbe49f19ad997a1ccb39, 0x323031382d30312d32325f313330305f7265706f72742d6d73672e73716c, 0x3e41de27c5a2e80f6aa96e6e42a0aff14ed49488, 1522418594, 20181102142908),
(0x319999cf019fbb1e1a0dcbb4b2c243d78a202466, 0x323031382d30342d31395f313234305f6e756c6c2d70617373776f72642d666f722d4341532e73716c, 0x993ee703f5eea3f70a42d5fd009cb28e2b0a75a1, 1530282861, 20181102142908),
(0x341379977437ad304b17077c8607efe0a3ffc662, 0x323032312d30352d30345f313230395f61646d696e5f696e5f64622e73716c, 0x7b457860f0f62dfcef2e29cfaabaa9c9a12ca233, 1620124717, 20210504103840),
(0x3499f6fb3678a5443dcf8368fe534a4ea8931f8d, 0x323032302d31312d30345f303932375f54726163655f5245434f5645525f4c442e73716c, 0xe123868a68981e1423b093da3c77c8c0bc56db9f, 1604478501, 20201104082845),
(0x3546b8b62ebc418e85ba300089f9dd0f9e095bce, 0x323031382d31302d31365f313234325f74726163655f746561636865722d766965772d7265706f72742e73716c, 0xaaef973fc800f193328e232b050e98ba209756bd, 1543306762, 20181127153131),
(0x3560f6a5f33c0e61a97665c2ad2c7ebd710f1e48, 0x323031382d31312d32375f313534395f616c7465722d74726163652d616374696f6e732e73716c, 0xa1909542d2a1edc383a8532e279f45378f02b887, 1543330241, 20181127155358),
(0x3578cafb978dc0304da031ceac9f18c4db32b14b, 0x323031392d30352d31345f313735325f757365725f6c616e67756167652e73716c, 0x3de61e70e4edbf7a9149fd85fabe6a00f6884ba4, 1593014225, 20200624155708),
(0x39140be471333cc2dd9763d5f69638ac3d1b3026, 0x323031392d30312d31315f313035355f68747470732d777261707065722e706870, 0xda7e4a7116e972329ef2755c4feb8aae930e3c3e, 1547569428, 20190205141749),
(0x39501ecb65dfbbc528889e685e8316195eb5a300, 0x323032302d30332d32325f313131345f6861726d6f6e697a652d74696d657374616d70732e73716c, 0xa40057c84e9c7e8a1a26259be547fb71df8fc621, 1593014417, 20200624160022),
(0x3d49fa28da3c332bd4bb2152cc74ab6a6ab1f3ca, 0x323031372d31322d32385f313230305f636f6d6d656e7461697265732e73716c, 0xe97630e4ac94ccb0697bea17baab15f5612e0f66, 1522418594, 20181102142908),
(0x3f7dcc67848983754d19b3e0a42da004d369d4dd, 0x323031382d30392d32355f313733325f7570646174655f74696d652e73716c, 0x7b008c1747bcd2580844c8515960572c19a479fd, 1538398118, 20181102143654),
(0x3fdeed1b61e3f44a9324fc78caf8c66050f462c1, 0x323032302d30362d32345f313830305f6e65772e73716c, 0xda39a3ee5e6b4b0d3255bfef95601890afd80709, 1593014444, 20200624160839),
(0x400b41d30bcca2741ec40ca825f9c92bdfcde9e6, 0x323032312d30352d32365f313630315f636c617373655f696e737469747574696f6e2e73716c, 0xd126985a83a950647f819811a6bef63d75164704, 1622038430, 20210526141600),
(0x428618f27f2565d1baf009d5ab5b6783514d584c, 0x323032302d30322d31375f313834335f636c6173735f72656d6f76655f656e64646174652e73716c, 0x2c71171e44016927f9d4d9a34c0cb01828c25fb6, 1593014382, 20200624155946),
(0x43bcb34295b80fd38144040916c031bc2ff8fa09, 0x323031382d31312d32335f313233325f6669782d7472756e63617465642d616e6e6f746174696f6e732e73716c, 0xa0d5889b327d5c512dfe237ca97a03c13a6f2331, 1543307296, 20181127153132),
(0x43c9c53838c57bbe784658a4fa32f852ed6f13f8, 0x323031392d30382d32305f313035335f6d697373696f6e2d617263686976652e73716c, 0xbcc73dfbf43bd00578b08c3e52319eefea69d8a6, 1591283730, 20200612104339),
(0x43fb2c400127656a4b94d9ca7cd49f35abd76329, 0x323031382d30392d30375f313730305f7465616d5f636f6e6669672d696d706f72745f6c642e73716c, 0x8857f667e97c4ee1530b89136985141499efb553, 1538398118, 20181102143654),
(0x44d4ec65bb3b330666e659a358ce79ba195ed438, 0x323032302d31302d30325f313134385f72656d6f76655f4f4e5f5550444154455f43555252454e545f54494d455354414d502e73716c, 0x8857f667e97c4ee1530b89136985141499efb553, 1601633310, 20201002100849),
(0x4bf4502950421976bb765f23f9847e9644abba14, 0x323031382d30342d32305f313134345f72656d6f76652d7365727665722d6e616d652d66726f6d2d55524c732e73716c, 0x6f5ad1e0ab1ac82586f31d782436f2a63dfd1df7, 1528338405, 20181102142908),
(0x4cc5e36ac5a5a445b2cc17851eff73765cac2c91, 0x323031392d30382d32395f313133335f6e65772e73716c, 0xda39a3ee5e6b4b0d3255bfef95601890afd80709, 1591283730, 20200612104339),
(0x4ff5815af73b254676c3a55ed3b6db153011df84, 0x323031382d30322d30385f313031395f6d697373696e672d666b2e73716c, 0x126a88b4c558bd159e9470dfe2b9d24a3deac8df, 1522418594, 20181102142908),
(0x54c8f8c87b005e3c51f3bc7f54ad5ff79c97aec0, 0x323032312d30332d32395f303934325f526573746f72655f69645f6c645f6f726967696e2e73716c, 0xdb22115a5aa4439fac85399c8c56bc91e5c8269d, 1617090261, 20210330074513),
(0x58034cb13f426e4e0e15dfb74b10952d7868ba49, 0x323032312d31322d31355f313431335f696e6372656173655f636c6173735f6e616d655f73697a652e73716c, 0x2c22cc828588b2db83dac6c8d7ec6733750c3c94, 1639574230, 20211215141714),
(0x5c8efda3295ff5d8936a1a5bde1f2e19f81b860c, 0x323031392d30352d30335f313635365f73706c69745f74726163655f6475706c69636174652d6c642e706870, 0x21b0ebc51a9d0715f8ec0ca486e30b033bdf68c3, 1559897312, 20190607104844),
(0x6065d15341d40a3deda279ea16cf34602616d4d5, 0x323031392d30362d30365f303935335f6372656174655f696e6465785f74726163652e73716c, 0x5eacf51d4c663654f2662eb23fa71ab4a51979e0, 1591283730, 20200612104339),
(0x610adeb57874a6cb6290679dc25307a16d5f1310, 0x323031382d30382d33305f303832305f66697465782d636f6c756d6e2d636f64652e706870, 0x576eee712189c114d5701455aef95fc1f9275665, 1538145041, 20181102143625),
(0x636ffdf1e2a2ff8d1449ec6ec08e90021a81f6c6, 0x323031382d31322d30345f313532355f7472616365735f746561636865722e73716c, 0x3feeaedc1c37e609afff50d081f4c86312bde736, 1548424981, 20190205141748),
(0x637d17de7cea343d44a844542f7991b983241a46, 0x323032312d30352d30345f313230345f6361735f696e737469747574696f6e5f696e5f64622e73716c, 0xd7afbc23c839dadd9abeed564de7977549a510ab, 1620124468, 20210504103433),
(0x64d3d32ee54eacd919b03fc424fc5315a0da6696, 0x323032322d30322d31305f313031325f61766f69645f64656c6574655f636173636164655f636c6173735f696e737469747574696f6e2e73716c, 0xa6ec5b00db03ee5c2d54d2d91882fabd0369939b, 1644484478, 20220210101444),
(0x6b15f2099c463d98fde0d1452af5c6418d2b8613, 0x323031392d30352d33315f313535305f6c6162646f635f64656c657465645f747970655f7570646174652e73716c, 0xfeb9108ede141cb8d1f33ff28374697ccb47853d, 1591283730, 20200612104339),
(0x6ce82828cab7bd28244306ecc8b1b6a02847319a, 0x323031392d30392d31375f313535335f6f6c645f73747564656e745f7465616d696e672e73716c, 0x62dcc2550e1eb3e35bbe73873eea986a43f527a4, 1591283730, 20200612104339),
(0x6d725d4ffcd333f875f068ddff409590b1817b7f, 0x323031392d31322d31315f313634365f3330322d6475706c69636174652d7472616365732e73716c, 0x9e28f5f8dc716631d2ebe08668063884ff291aff, 1591283730, 20200612104339),
(0x6dd4d23230233947e3f8ce3f55e0f3a3319070cb, 0x323031392d30382d32385f303831365f6e65772e73716c, 0xda39a3ee5e6b4b0d3255bfef95601890afd80709, 1591283730, 20200612104339),
(0x7854ecabd9a02db2a07f23efb2064b4e4e61a4e4, 0x323032302d30362d31315f313635305f7461626c655f616e6e6f746174696f6e5f72656d6f76655f617474726962757465735f636f6c756d6e5f7570646174655f74696d652e73716c, 0x247ffee913bb034652dfc7e0e5be53cb1cc232fb, 1591951067, 20200612104339),
(0x79733c23f2c87bf7ae232e3b66b82b10742c0084, 0x323032302d30322d32305f303834335f6669782d7265706f72742d7374617475732e73716c, 0x302cf600e89229cde6879e3e6980d9d2105ed18e, 1591283730, 20200612104339),
(0x7ba69705bd9000a3038d296c3b650386840a9a09, 0x323031382d30332d32325f313030355f756e697175655f696e73742d6e756d6265722e73716c, 0x03182e9992a9109779ce7730f7db158cd1c9845b, 1528338405, 20181102142908),
(0x7fde4d7c976ef24d48c6a4dda0cee8fa13d51608, 0x323032302d30312d31345f313735335f7265666f726d61742d6c625f747261636b732e73716c, 0xa157d8fc3c36580061776f304b66e5f688de4aa9, 1593014357, 20200624155920),
(0x81aff569db672e0967d89750ab66d9d8efd67ea3, 0x323031382d31322d31315f303835325f7265706f72742d64617465732d74696d652e73716c, 0xe770834131608505ec45b62bafaea215956392ce, 1548424981, 20190205141749),
(0x8637b1328bfdcc6a1d2fdebcfad4761dea5a90ea, 0x323032322d30322d30335f313530385f72656e616d655f7265706f72745f6669656c64732e73716c, 0x3c18d0144856f35b96302e77251602bf9c303abb, 1645034577, 20220217093536),
(0x874d53d0123b7c32ad7063d27be88064c59d78b2, 0x323031382d30332d31395f313931305f74726163655f6164645f636f6e762e73716c, 0x0e54f2e3279b4f686af0d3c83711828e3d4a9e36, 1528338405, 20181102142908),
(0x8b1ac0c377e7a31914b674fe95bd263a38e97d18, 0x323032322d30322d30345f313731325f6c726c5f72656c6f61645f616e6e6f746174696f6e732e73716c, 0xb801a951e4a4430cf8bc73567c9d1093250e8ffc, 1644436970, 20220209211350),
(0x8b4b454536b9dd0db3f1cf4182df761150165f45, 0x323031382d31302d32335f313431325f7265706f72745f696e697469616c697a65642e73716c, 0x2f28944b39a93998b8a5ba7011f3f00f38738a23, 1543306762, 20181127153131),
(0x8f32b30b2e02a78a799e3761734bac4132cd890f, 0x323032312d30352d32355f313034395f6372656174655f73746f726167655f7461626c652e73716c, 0x9816048e29b53d475451bb080c41ffeb7189f4cb, 1623922020, 20210617111703),
(0x91305f551bc810fbb10e613f5a9cca3b352cba8f, 0x323032322d30322d30335f313832345f74626c5f746561636865725f7265706f72745f7374617475732e73716c, 0x8da2cba52dbf78dff432dc68387e80971f36c337, 1644436977, 20220209211350),
(0x9318ea146eae982e3f842bb8121a4e37d8b0b6dc, 0x323032312d30332d30342d313633355f64656c6574655f77726f6e675f6c646475706c6963617465732e73716c, 0xb1f5db5e042bbd1d68ba640c2b1587e80e3a3d18, 1614872392, 20210304162525),
(0x96bb4898e76d3e5c247611387012ae6f077a2864, 0x323032302d31322d30325f313933325f7570646174655f6d697373696f6e735f696e746572666163652e73716c, 0xea9dc5089025efd50ab20ec21807824e4385ccaf, 1606933972, 20201202183302),
(0x99704cd7c2386a03714cf9b02f4c5c5b96252660, 0x323032312d30332d30325f313132375f6164645f6c696e6b5f616e6e6f746174696f6e5f6c6561726e65722e73716c, 0x9d5a1deb2cf1b63357f82538b65d1ebff8fc8987, 1615466484, 20210311150018),
(0x9dd0ec35e76892becec7bcf2d88ccc857d400567, 0x323031392d30352d30335f313633365f74726163655f736176652d61747461636865642d6c642e73716c, 0xee876e5679629a61320f4b3d9b7efde54b5a9075, 1559897312, 20190607104844),
(0x9fa78d13e52322bf1af09d5f621fc2218c4fea95, 0x323031382d30382d31315f696d706f72742d6c642d66726f6d2d7265706f72742e73716c, 0x1f2bdb2ae5b9483e683506bfc87820215279bc97, 1538145041, 20181102143625),
(0xa5806c6419070976908baabb8b541f227dad1c6f, 0x323032322d30322d32355f313831385f6368616e67655f616374696f6e5f35305f6e616d652e73716c, 0x37e9d99081eefc8e41b434faeb519de879fb68a2, 1646324426, 20220303173935),
(0xa6d75db31a7c7effe8353bf4357060623a2e8f5a, 0x323032302d30362d31315f313635305f616e6e6f746174696f6e5f72656d6f76655f63757272656e745f74696d657374616d702e73716c, 0x1f2bdb2ae5b9483e683506bfc87820215279bc97, 1591959460, 20200612105827),
(0xa73370e4cd92a7673b1f1f0f80cc5f637ffd007f, 0x323031392d31322d31315f313235335f3330312d66697865642d6d697865642d75702d7472616365732e73716c, 0xf281acdcac379f5275b50272d7126650f7b33a18, 1591283730, 20200612104339),
(0xad0ab41b51d318ae014e1e3a923fc1835bdd42e1, 0x323031382d30332d31365f303932345f7472616365732d657874656e6465642e73716c, 0x87f6fb37838212c84385560932ead0908088b7e6, 1528338405, 20181102142908),
(0xae359b9f8f942da8f2090a75fa94ea29a61b81cb, 0x323032302d30362d32345f313735345f64726f705f6a6f62732e73716c, 0x041dc838403c91bd66cb476a41075e498f2fcb3a, 1593014086, 20200624160042),
(0xb2a1f364cdb096227bd4f11bedfbc8784515f1bd, 0x323032312d30332d33315f313830305f44726f705f7461626c655f6c6162646f635f6261636b75702e73716c, 0xb231ddc6c80796a7029d14054e451c5a0496a239, 1617206496, 20210331161456),
(0xb3ae34be1d559aa84e0b1317d946cb1eb0029b61, 0x323032312d30362d33305f313531315f746561636865725f7465616d5f74726163652e73716c, 0x95dbd8902d3a7ca046d2ff99eefe87ce3d1ab9be, 1630570203, 20210902095220),
(0xb507ac60c5d84e6c6e86377e6288f6d3204c30cf, 0x323031392d30332d30385f74726163655f616374696f6e2e73716c, 0x799045b9ebdb1900ad28cb9260984de86a0fd101, 1554382133, 20190607104844),
(0xb6e37343a633580f763fcb94a95415665a3f17a0, 0x323031382d30372d31365f696d706f72742d6c642d66726f6d2d7265706f72742e73716c, 0x7ad4b31b3d7ffea346f324d15bf17380bdcae98f, 1538145041, 20181102143625),
(0xba839de76322c4ebed1c3bda062a19e66a2fa5b3, 0x323031382d30332d30325f303834375f74726163652e73716c, 0x2d5d6eb7a1dbd646e1f16432ee9113a8ac3c5fe4, 1528338405, 20181102142908),
(0xc61f1f50c1770c636dfbde3a0aef7e7ebbed20e6, 0x323032312d30332d30332d313430365f72656e616d655f74726163655f726561645f616e6e6f746174696f6e2e73716c, 0x3ec4996134ff510e8b6beb097d12f4cdf5442267, 1614788995, 20210303163108),
(0xc6ca0584ca8b0600c8fe2e02605ecc1ac4cf4f11, 0x323032312d30342d30315f313730345f74726163655f6f70656e5f6c645f76657273696f6e696e672e73716c, 0x5073f89bf6cb7f8457db5b2146402aea7b202f03, 1617289535, 20210401150554),
(0xc9100c2bd3212e093d299a4343877bd54e46a4a0, 0x323031392d30362d31325f303935305f7570646174655f636c6173732e73716c, 0x619655125088f6ea9a194c8186f1756c1d3fa647, 1593014282, 20200624155805),
(0xca5fe11fcb255017be87c6130a90b69fd6f5641d, 0x323031382d30342d30365f313133385f7472616365732d6669782d616374696f6e732e73716c, 0xbf6c0b386ebab4cf7ceb140ebff04a8042355a55, 1528338405, 20181102142908),
(0xcb160b29d7e313cccc1f9a24f3749cc4fde13627, 0x323031382d30342d31305f313231305f6669782d616e6e6f746174696f6e2d646174612e73716c, 0xe8b1f2df8e36cc1bbea5ec0d7585c880fbd627c3, 1528338405, 20181102142908),
(0xd84c57b29ab0f6c38f7ec4f02270bb81d7c4dcfd, 0x323032302d30392d30395f313732365f6164645f616374696f6e5f544541434845525f53454c4543545f5245504f52542e73716c, 0x7fb6cbfe2906914b6259100850778e53ae6edc16, 1599665320, 20200909152914),
(0xdf76dd4928ae79625bb17ca820d9ccfef4e51ac3, 0x323031382d30392d31315f303935315f7465616d5f636f6e6669675f6e616d652d7072656669782e73716c, 0x9f55f66a695c284058992768dc965d6def617be2, 1538398118, 20181102143654),
(0xe63fe74ef079bbc12c56730124193881be407bc7, 0x323031382d31312d32335f313534355f72656e616d655f636f6e762d757365725f6c6173742d7365656e2e73716c, 0x4a8d7a0f21ab5c7a9714dc38f8d8e1c977d92839, 1544170080, 20190205141748),
(0xe6e68a91f9a4db2ec142192bce2e85b87d25386f, 0x323031382d30342d32305f313734385f7465616d732e73716c, 0x5bcfcae6647816f8a78133bdb7cec04810e35975, 1538145041, 20181102142908),
(0xe9f3197ef477da9aa08612b3b5c4937142bc85c8, 0x323032302d31322d30325f313734385f72656d6f76655f74696d657374616d705f7570646174652e73716c, 0x6d4032595b131281d67f7384128acada4bc475b2, 1606928042, 20201202165405),
(0xecbb0ca15da5dbd3d10e99acbfeab75c3f549061, 0x323032312d30332d32345f323133355f6164645f746561636865725f7465616d5f616e645f6173736f6369617465645f6c696e6b2e73716c, 0xcd8dad3203a355821fd817c1ed5fc431972c09dc, 1619625212, 20210429070729),
(0xf28c77815df65d830c278a4d073549d3dbe736f8, 0x323032312d30322d30395f313130365f6164645f6d697373696e675f64617465735f6d697373696f6e732e73716c, 0xdd6b21d46d93b43c0d10c289a0bcd7233dcfe183, 1612870250, 20210210082100),
(0xf6533ff273708e5411a2f5c03d017ea464f4e86f, 0x323031392d30362d31325f313535385f74726173685f776964672e73716c, 0xed5b731c2b93ec42cb1dae98b4baba58cd18eea2, 1591283730, 20200612104339),
(0xf9e767bfec736fa08b3169553c1edb4c7c0f1780, 0x323032312d31312d32345f313033305f686172645f64656c6574655f6c645f74726163652e73716c, 0xf4f39a7c679ab75d9569d675733306974a5fcd94, 1637843976, 20211125153648),
(0xfa9d94b3fe1f9bf400b8293bd650b0619579fc9f, 0x323031392d30382d31345f303832355f6e65772e73716c, 0xda39a3ee5e6b4b0d3255bfef95601890afd80709, 1591283730, 20200612104339),
(0xfaa57687c5ca0d5fa040343d998ed70c6cbffb9e, 0x323031392d30392d31325f313334375f7465616d636f6e6669675f756e69636974792e73716c, 0x24a0fd5d1ee20a8e814284184d9abfc49169b7de, 1593014325, 20200624155848);



-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2019_10_08_124640_add_jwt_fields', 2),
(3, '2019_11_05_141239_class_external_code', 2),
(4, '2020_03_12_150108_add_maillog', 2),
(11, '2020_05_16_153307_create_jobs_table', 3),
(12, '2020_05_18_143550_fix_zwibler_urls', 3),
(15, '2020_06_04_134521_report_soft_delete', 5),
(16, '2020_06_08_110755_add_popularity_annotation', 6),
(18, '2020_05_18_154916_fix_tinymce_img_src', 7),
(19, '2020_06_24_172217_fix_teacher_images', 8);


-- --------------------------------------------------------

--
-- Structure de la table `mission`
--

CREATE TABLE `mission` (
  `id_mission` int(10) UNSIGNED NOT NULL,
  `code` varchar(16) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `assignment` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('public','private','archive','tutorial') CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT 'private',
  `creation_date` date DEFAULT NULL,
  `modif_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `report`
--

CREATE TABLE `report` (
  `id_report` int(10) UNSIGNED NOT NULL,
  `id_mission` int(10) UNSIGNED NOT NULL,
  `id_team_config` int(10) UNSIGNED DEFAULT NULL,
  `start_datetime` datetime DEFAULT NULL,
  `end_datetime` datetime DEFAULT NULL,
  `status` enum('new','on','wait','arc','test','tuto') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'new',
  `initialized` tinyint(1) NOT NULL DEFAULT 0,
  `team_name` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `allow_msg_teacher` tinyint(1) NOT NULL DEFAULT 1,
  `allow_msg_team` tinyint(1) NOT NULL DEFAULT 1,
  `allow_msg_id_class` int(10) NOT NULL DEFAULT 0,
  `allow_msg_id_mission` int(10) NOT NULL DEFAULT 0,
  `allow_attach_ld` tinyint(1) NOT NULL DEFAULT 0,
  `allow_save_ld` tinyint(1) NOT NULL DEFAULT 0,
  `allow_import_id_mission` int(10) NOT NULL DEFAULT -1,
  `creation_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_time` timestamp NULL DEFAULT NULL,
  `delete_time` timestamp NULL DEFAULT NULL COMMENT 'deletion time'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `report_part`
--

CREATE TABLE `report_part` (
  `id_report_part` int(10) UNSIGNED NOT NULL,
  `id_mission` int(10) UNSIGNED NOT NULL,
  `position` int(10) UNSIGNED NOT NULL,
  `title` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` tinyint(3) UNSIGNED NOT NULL DEFAULT 1,
  `drawing` tinyint(3) UNSIGNED NOT NULL DEFAULT 1,
  `dataset` tinyint(3) UNSIGNED NOT NULL DEFAULT 1,
  `procedure` tinyint(3) UNSIGNED NOT NULL DEFAULT 1,
  `assignment` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `ressource`
--

CREATE TABLE `ressource` (
  `id_ressource` int(10) UNSIGNED NOT NULL,
  `id_mission` int(10) UNSIGNED DEFAULT NULL,
  `id_report` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `res_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `res_type` enum('assignment','file','url') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'file',
  `position` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `teacher`
--

CREATE TABLE `teacher` (
  `id_teacher` int(10) UNSIGNED NOT NULL,
  `teacher_domain` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `teacher_level` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `teacher_comment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_interface` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` enum('teacher','manager','admin') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'teacher'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `teacher`
--

INSERT INTO `teacher` (`id_teacher`, `teacher_domain`, `teacher_level`, `teacher_comment`, `current_interface`, `role`) VALUES
(1, 'Admin', 'univ', '', 'teacher/reports', 'admin');

-- --------------------------------------------------------

--
-- Structure de la table `teacher_report_status`
--

CREATE TABLE `teacher_report_status` (
  `id_report` int(10) UNSIGNED NOT NULL,
  `id_teacher` int(10) UNSIGNED NOT NULL,
  `last_synchro` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `teacher_team`
--

CREATE TABLE `teacher_team` (
  `id_teacher_team` int(10) UNSIGNED NOT NULL,
  `id_inst` int(11) DEFAULT NULL,
  `name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('normal','archive') CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT 'normal',
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `creation_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `team_config`
--

CREATE TABLE `team_config` (
  `id_team_config` int(10) UNSIGNED NOT NULL,
  `id_mission` int(10) UNSIGNED NOT NULL,
  `id_class` int(10) UNSIGNED NOT NULL,
  `method` tinyint(4) UNSIGNED NOT NULL DEFAULT 0 COMMENT '0=undef 1=student-choice 2=teacher-choice 3=random',
  `size_opt` smallint(5) UNSIGNED DEFAULT NULL,
  `size_max` smallint(5) UNSIGNED DEFAULT NULL,
  `size_min` smallint(5) UNSIGNED DEFAULT NULL,
  `teams_max` smallint(5) UNSIGNED DEFAULT NULL,
  `start_datetime` datetime DEFAULT NULL,
  `end_datetime` datetime DEFAULT NULL,
  `allow_msg_teacher` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'authorize messages to the class teachers (boolean)',
  `allow_msg_team` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'authorize messages to the other team members (boolean)',
  `allow_msg_id_class` int(10) UNSIGNED DEFAULT NULL COMMENT 'authorize messages to class: NULL=none other=id_class',
  `allow_msg_id_mission` int(10) UNSIGNED DEFAULT NULL COMMENT 'authorize messages to mission students: NULL=none other=id_mission',
  `allow_attach_ld` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'authorize attaching LD to outgoing messages (boolean)',
  `allow_save_ld` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'authorize saving into the report the LD received in messages (boolean)',
  `allow_import` tinyint(1) NOT NULL COMMENT 'Allow the import of LabDocs. If true, may be bound to a mission with allow_import_id_mission',
  `allow_import_id_mission` int(10) UNSIGNED DEFAULT NULL COMMENT 'Limit LD import to a single mission',
  `name_prefix` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_on_create` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'send an email to members when a team is created (boolean)',
  `creation_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `trace`
--

CREATE TABLE `trace` (
  `id_trace` bigint(20) UNSIGNED NOT NULL,
  `id_action` smallint(6) NOT NULL,
  `action_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_mission` int(10) UNSIGNED DEFAULT NULL,
  `id_report` int(10) UNSIGNED DEFAULT NULL,
  `id_labdoc` int(10) UNSIGNED DEFAULT NULL,
  `attributes` varbinary(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `trace_action`
--

CREATE TABLE `trace_action` (
  `id_action` smallint(6) NOT NULL,
  `action` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action_type` enum('administration','production','information','communication','t_mission','t_student','t_report','m_teacher') CHARACTER SET ascii COLLATE ascii_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `trace_action`
--

INSERT INTO `trace_action` (`id_action`, `action`, `action_type`) VALUES
(1, 'connect_lnb', 'administration'),
(2, 'disconnect_lnb', 'administration'),
(3, 'enter_report', 'administration'),
(4, 'leave_report', 'administration'),
(5, 'submit_report', 'administration'),
(6, 'add_ld', 'production'),
(7, 'duplicate_ld', 'production'),
(8, 'edit_ld', 'production'),
(9, 'modify_ld', 'production'),
(10, 'validate_ld', 'production'),
(11, 'toggle_ld_status', 'production'),
(12, 'update_ld_name', 'production'),
(13, 'delete_ld', 'production'),
(14, 'check annotation', 'information'),
(15, 'open_rp_assignment', 'information'),
(16, 'open_assignment', 'information'),
(17, 'open_detailed_assignment', 'information'),
(18, 'open_resource', 'information'),
(19, 'add_resource', 'communication'),
(20, 'open_comment', 'communication'),
(21, 'add_comment', 'communication'),
(22, 'open_conversation', 'communication'),
(23, 'add_message', 'communication'),
(24, 'add_conversation', 'communication'),
(25, 'import_ld', 'production'),
(26, 'save_attached_ld', 'production'),
(27, 'recover_ld', 'production'),
(28, 'open_ld_versioning', 'production'),
(29, 'hard_delete_ld', 'production'),
(31, 'follow_report', 't_report'),
(32, 'test_mission', 't_mission'),
(33, 'modify_mission', 't_mission'),
(34, 'add_mission', 't_mission'),
(35, 'duplicate_mission', 't_mission'),
(36, 'delete_mission', 't_mission'),
(37, 'add_resource', 't_mission'),
(38, 'add_ld', 't_mission'),
(39, 'add_rp', 't_mission'),
(40, 'add_class', 't_student'),
(41, 'delete_class', 't_student'),
(42, 'add_student', 't_student'),
(43, 'delete_student', 't_student'),
(44, 'add_report', 't_student'),
(45, 'edit_report', 't_report'),
(46, 'delete_report', 't_report'),
(47, 'annotate_report', 't_report'),
(48, 'add_teaming', 't_student'),
(49, 'create_user', 't_student'),
(50, 'display_report_info', 't_report'),
(51, 'add_teacher_status', 'm_teacher'),
(52, 'remove_teacher_status', 'm_teacher'),
(53, 'edit_teaming', 't_student');

-- --------------------------------------------------------

--
-- Structure de la table `upload`
--

CREATE TABLE `upload` (
  `id_upload` int(10) UNSIGNED NOT NULL,
  `hash` binary(20) NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `upload_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `id_user` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `size` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id_user` int(10) UNSIGNED NOT NULL,
  `user_name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `login` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pwd` varbinary(64) DEFAULT NULL,
  `email` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `lang` varchar(3) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `id_inst` int(11) NOT NULL,
  `inst_number` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_report` int(10) UNSIGNED DEFAULT NULL,
  `last_synchro` int(11) DEFAULT NULL,
  `reset_pwd` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `creation_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_time` timestamp NULL DEFAULT NULL,
  `cgu_accept_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id_user`, `user_name`, `first_name`, `login`, `pwd`, `email`, `lang`, `id_inst`, `inst_number`, `current_report`, `last_synchro`, `reset_pwd`, `creation_time`, `update_time`, `cgu_accept_time`) VALUES
(1, 'Admin', '', 'admin', 0x2432792431302447656e544a4c454c5457383564716e6e6f47706f6975666f4534637152536f4b5435486255675751696e30684f696c7269746f4f36, 'someone@example.org', 'fr', 1, NULL, NULL, 1646325630, 'Qtj5UyL7P04jLtfv8vewWHnP4JBSicKf', '2018-01-01 00:00:00', NULL, '2021-10-27 08:20:08');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `annotation`
--
ALTER TABLE `annotation`
  ADD PRIMARY KEY (`id_annotation`),
  ADD KEY `id_teacher` (`id_teacher`),
  ADD KEY `id_report` (`id_report`),
  ADD KEY `id_report_part` (`id_report_part`),
  ADD KEY `id_labdoc` (`id_labdoc`);

--
-- Index pour la table `class`
--
ALTER TABLE `class`
  ADD PRIMARY KEY (`id_class`),
  ADD UNIQUE KEY `class_code` (`class_code`),
  ADD KEY `id_inst` (`id_inst`),
  ADD KEY `class_ibfk_2` (`id_extplatform`);

--
-- Index pour la table `conversation`
--
ALTER TABLE `conversation`
  ADD PRIMARY KEY (`id_conversation`),
  ADD KEY `Conversation_FKIndex1` (`id_report`);

--
-- Index pour la table `extplatform`
--
ALTER TABLE `extplatform`
  ADD PRIMARY KEY (`id_extplatform`),
  ADD UNIQUE KEY `extplatformname` (`name`),
  ADD KEY `extplatform_ibfk_1` (`id_inst`);

--
-- Index pour la table `institution`
--
ALTER TABLE `institution`
  ADD PRIMARY KEY (`id_inst`),
  ADD UNIQUE KEY `institutionname` (`name`),
  ADD KEY `institution_ibfk_1` (`id_class_teacher`);

--
-- Index pour la table `labdoc`
--
ALTER TABLE `labdoc`
  ADD PRIMARY KEY (`id_labdoc`),
  ADD KEY `id_report_part` (`id_report_part`),
  ADD KEY `id_mission_conf` (`id_report`),
  ADD KEY `id_ld_origin` (`id_ld_origin`),
  ADD KEY `last_editor` (`last_editor`);

--
-- Index pour la table `labdocfile`
--
ALTER TABLE `labdocfile`
  ADD PRIMARY KEY (`filehash`,`id_labdoc`),
  ADD KEY `id_labdoc` (`id_labdoc`),
  ADD KEY `id_mission` (`id_mission`);

--
-- Index pour la table `link_class_learner`
--
ALTER TABLE `link_class_learner`
  ADD PRIMARY KEY (`id_class`,`id_user`),
  ADD KEY `lcl_id_user` (`id_user`);

--
-- Index pour la table `link_class_teacher`
--
ALTER TABLE `link_class_teacher`
  ADD PRIMARY KEY (`id_teacher`,`id_class`),
  ADD KEY `lct_id_class` (`id_class`);

--
-- Index pour la table `link_class_tteam`
--
ALTER TABLE `link_class_tteam`
  ADD PRIMARY KEY (`id_class`,`id_teacher_team`),
  ADD KEY `lctt_id_teacher_team` (`id_teacher_team`);

--
-- Index pour la table `link_conversation_user`
--
ALTER TABLE `link_conversation_user`
  ADD PRIMARY KEY (`id_conversation`,`id_user`),
  ADD KEY `lcu_id_user` (`id_user`);

--
-- Index pour la table `link_extplatform_user`
--
ALTER TABLE `link_extplatform_user`
  ADD PRIMARY KEY (`id_extplatform`,`id_user_ext`),
  ADD KEY `link_ext_user_inst_id_user_foreign` (`id_user`);

--
-- Index pour la table `link_inst_teacher`
--
ALTER TABLE `link_inst_teacher`
  ADD PRIMARY KEY (`id_teacher`,`id_inst`),
  ADD KEY `id_inst` (`id_inst`);

--
-- Index pour la table `link_mission_teacher`
--
ALTER TABLE `link_mission_teacher`
  ADD PRIMARY KEY (`id_mission`,`id_teacher`),
  ADD KEY `lmt_id_teacher` (`id_teacher`);

--
-- Index pour la table `link_mission_tteam`
--
ALTER TABLE `link_mission_tteam`
  ADD PRIMARY KEY (`id_mission`,`id_teacher_team`),
  ADD KEY `id_teacher_team` (`id_teacher_team`);

--
-- Index pour la table `link_report_learner`
--
ALTER TABLE `link_report_learner`
  ADD PRIMARY KEY (`id_report`,`id_user`),
  ADD KEY `Link_Report_Learner_FKIndex1` (`id_report`),
  ADD KEY `Link_Report_Learner_FKIndex2` (`id_user`);

--
-- Index pour la table `link_teacher_tteam`
--
ALTER TABLE `link_teacher_tteam`
  ADD PRIMARY KEY (`id_teacher`,`id_teacher_team`,`status`),
  ADD KEY `lttt_id_teacher_team` (`id_teacher_team`);

--
-- Index pour la table `link_upload_context`
--
ALTER TABLE `link_upload_context`
  ADD UNIQUE KEY `unique_mission` (`id_mission`,`type_mission`,`id_upload`),
  ADD UNIQUE KEY `unique_labdoc` (`id_labdoc`,`id_upload`),
  ADD UNIQUE KEY `unique_ressource` (`id_ressource`,`id_upload`),
  ADD UNIQUE KEY `unique_report_part` (`id_report_part`,`id_upload`),
  ADD KEY `id_upload` (`id_upload`),
  ADD KEY `id_ressource` (`id_ressource`),
  ADD KEY `id_mission` (`id_mission`),
  ADD KEY `id_report_part` (`id_report_part`);

--
-- Index pour la table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id_message`),
  ADD KEY `id_user` (`id_user_sender`),
  ADD KEY `id_conv` (`id_conversation`),
  ADD KEY `id_labdoc` (`id_labdoc`);

--
-- Index pour la table `message_var`
--
ALTER TABLE `message_var`
  ADD PRIMARY KEY (`id_user`);

--
-- Index pour la table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `mission`
--
ALTER TABLE `mission`
  ADD PRIMARY KEY (`id_mission`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Index pour la table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Index pour la table `report`
--
ALTER TABLE `report`
  ADD PRIMARY KEY (`id_report`),
  ADD KEY `Mission_Conf_FKIndex2` (`id_mission`),
  ADD KEY `id_team_config` (`id_team_config`);

--
-- Index pour la table `report_part`
--
ALTER TABLE `report_part`
  ADD PRIMARY KEY (`id_report_part`),
  ADD KEY `my_unique` (`id_mission`,`position`);

--
-- Index pour la table `ressource`
--
ALTER TABLE `ressource`
  ADD PRIMARY KEY (`id_ressource`),
  ADD KEY `id_mission` (`id_mission`),
  ADD KEY `id_mission_conf` (`id_report`);

--
-- Index pour la table `teacher`
--
ALTER TABLE `teacher`
  ADD PRIMARY KEY (`id_teacher`);

--
-- Index pour la table `teacher_report_status`
--
ALTER TABLE `teacher_report_status`
  ADD PRIMARY KEY (`id_report`,`id_teacher`),
  ADD KEY `Teacher_Report_Status_FKIndex1` (`id_report`),
  ADD KEY `Teacher_Report_Status_FKIndex2` (`id_teacher`);

--
-- Index pour la table `teacher_team`
--
ALTER TABLE `teacher_team`
  ADD PRIMARY KEY (`id_teacher_team`),
  ADD KEY `id_inst` (`id_inst`);

--
-- Index pour la table `team_config`
--
ALTER TABLE `team_config`
  ADD PRIMARY KEY (`id_team_config`),
  ADD UNIQUE KEY `unique_class_mission` (`id_class`,`id_mission`),
  ADD KEY `id_mission` (`id_mission`),
  ADD KEY `allow_import_id_mission` (`allow_import_id_mission`),
  ADD KEY `allow_msg_id_class` (`allow_msg_id_class`),
  ADD KEY `allow_msg_id_mission` (`allow_msg_id_mission`);

--
-- Index pour la table `trace`
--
ALTER TABLE `trace`
  ADD PRIMARY KEY (`id_trace`),
  ADD KEY `key_id_report` (`id_report`),
  ADD KEY `key_id_action` (`id_action`),
  ADD KEY `key_id_labdoc` (`id_labdoc`);

--
-- Index pour la table `trace_action`
--
ALTER TABLE `trace_action`
  ADD PRIMARY KEY (`id_action`);

--
-- Index pour la table `upload`
--
ALTER TABLE `upload`
  ADD PRIMARY KEY (`id_upload`),
  ADD UNIQUE KEY `path` (`path`),
  ADD KEY `upload_ibfk_1` (`id_user`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `login` (`login`,`pwd`),
  ADD UNIQUE KEY `reset_pwd` (`reset_pwd`),
  ADD UNIQUE KEY `instid_instnumber` (`id_inst`,`inst_number`),
  ADD KEY `current_report` (`current_report`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `annotation`
--
ALTER TABLE `annotation`
  MODIFY `id_annotation` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `class`
--
ALTER TABLE `class`
  MODIFY `id_class` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `conversation`
--
ALTER TABLE `conversation`
  MODIFY `id_conversation` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `extplatform`
--
ALTER TABLE `extplatform`
  MODIFY `id_extplatform` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `institution`
--
ALTER TABLE `institution`
  MODIFY `id_inst` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT pour la table `labdoc`
--
ALTER TABLE `labdoc`
  MODIFY `id_labdoc` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `message`
--
ALTER TABLE `message`
  MODIFY `id_message` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `mission`
--
ALTER TABLE `mission`
  MODIFY `id_mission` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `report`
--
ALTER TABLE `report`
  MODIFY `id_report` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `report_part`
--
ALTER TABLE `report_part`
  MODIFY `id_report_part` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `ressource`
--
ALTER TABLE `ressource`
  MODIFY `id_ressource` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `teacher_team`
--
ALTER TABLE `teacher_team`
  MODIFY `id_teacher_team` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `team_config`
--
ALTER TABLE `team_config`
  MODIFY `id_team_config` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `trace`
--
ALTER TABLE `trace`
  MODIFY `id_trace` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `upload`
--
ALTER TABLE `upload`
  MODIFY `id_upload` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1043;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `class`
--
ALTER TABLE `class`
  ADD CONSTRAINT `class_ibfk_1` FOREIGN KEY (`id_inst`) REFERENCES `institution` (`id_inst`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `class_ibfk_2` FOREIGN KEY (`id_extplatform`) REFERENCES `extplatform` (`id_extplatform`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `conversation`
--
ALTER TABLE `conversation`
  ADD CONSTRAINT `conv_id_rep` FOREIGN KEY (`id_report`) REFERENCES `report` (`id_report`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `extplatform`
--
ALTER TABLE `extplatform`
  ADD CONSTRAINT `extplatform_ibfk_1` FOREIGN KEY (`id_inst`) REFERENCES `institution` (`id_inst`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `institution`
--
ALTER TABLE `institution`
  ADD CONSTRAINT `institution_ibfk_1` FOREIGN KEY (`id_class_teacher`) REFERENCES `class` (`id_class`) ON UPDATE CASCADE;

--
-- Contraintes pour la table `labdoc`
--
ALTER TABLE `labdoc`
  ADD CONSTRAINT `labdoc_ibfk_1` FOREIGN KEY (`id_report_part`) REFERENCES `report_part` (`id_report_part`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `labdoc_ibfk_2` FOREIGN KEY (`id_ld_origin`) REFERENCES `labdoc` (`id_labdoc`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `labdoc_ibfk_3` FOREIGN KEY (`last_editor`) REFERENCES `user` (`id_user`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `ld_id_rep` FOREIGN KEY (`id_report`) REFERENCES `report` (`id_report`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `link_class_learner`
--
ALTER TABLE `link_class_learner`
  ADD CONSTRAINT `lcl_id_class` FOREIGN KEY (`id_class`) REFERENCES `class` (`id_class`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `lcl_id_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `link_class_teacher`
--
ALTER TABLE `link_class_teacher`
  ADD CONSTRAINT `lct_id_class` FOREIGN KEY (`id_class`) REFERENCES `class` (`id_class`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `lct_id_teacher` FOREIGN KEY (`id_teacher`) REFERENCES `teacher` (`id_teacher`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `link_class_tteam`
--
ALTER TABLE `link_class_tteam`
  ADD CONSTRAINT `lctt_id_class` FOREIGN KEY (`id_class`) REFERENCES `class` (`id_class`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `lctt_id_teacher_team` FOREIGN KEY (`id_teacher_team`) REFERENCES `teacher_team` (`id_teacher_team`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `link_conversation_user`
--
ALTER TABLE `link_conversation_user`
  ADD CONSTRAINT `lcu_id_conv` FOREIGN KEY (`id_conversation`) REFERENCES `conversation` (`id_conversation`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `lcu_id_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `link_extplatform_user`
--
ALTER TABLE `link_extplatform_user`
  ADD CONSTRAINT `link_extplatform_user_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE,
  ADD CONSTRAINT `link_extplatform_user_ibfk_2` FOREIGN KEY (`id_extplatform`) REFERENCES `extplatform` (`id_extplatform`) ON DELETE CASCADE;

--
-- Contraintes pour la table `link_inst_teacher`
--
ALTER TABLE `link_inst_teacher`
  ADD CONSTRAINT `link_inst_teacher_ibfk_1` FOREIGN KEY (`id_teacher`) REFERENCES `teacher` (`id_teacher`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `link_inst_teacher_ibfk_2` FOREIGN KEY (`id_inst`) REFERENCES `institution` (`id_inst`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `link_mission_teacher`
--
ALTER TABLE `link_mission_teacher`
  ADD CONSTRAINT `lmt_id_mission` FOREIGN KEY (`id_mission`) REFERENCES `mission` (`id_mission`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `lmt_id_teacher` FOREIGN KEY (`id_teacher`) REFERENCES `teacher` (`id_teacher`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `link_mission_tteam`
--
ALTER TABLE `link_mission_tteam`
  ADD CONSTRAINT `link_mission_tteam_ibfk_1` FOREIGN KEY (`id_mission`) REFERENCES `mission` (`id_mission`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `link_mission_tteam_ibfk_2` FOREIGN KEY (`id_teacher_team`) REFERENCES `teacher_team` (`id_teacher_team`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `link_teacher_tteam`
--
ALTER TABLE `link_teacher_tteam`
  ADD CONSTRAINT `lttt_id_teacher` FOREIGN KEY (`id_teacher`) REFERENCES `teacher` (`id_teacher`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `lttt_id_teacher_team` FOREIGN KEY (`id_teacher_team`) REFERENCES `teacher_team` (`id_teacher_team`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `link_upload_context`
--
ALTER TABLE `link_upload_context`
  ADD CONSTRAINT `link_upload_context_ibfk_1` FOREIGN KEY (`id_upload`) REFERENCES `upload` (`id_upload`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `link_upload_context_ibfk_2` FOREIGN KEY (`id_ressource`) REFERENCES `ressource` (`id_ressource`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `link_upload_context_ibfk_3` FOREIGN KEY (`id_mission`) REFERENCES `mission` (`id_mission`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `link_upload_context_ibfk_4` FOREIGN KEY (`id_report_part`) REFERENCES `report_part` (`id_report_part`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `message_ibfk_1` FOREIGN KEY (`id_labdoc`) REFERENCES `labdoc` (`id_labdoc`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `message_ibfk_2` FOREIGN KEY (`id_user_sender`) REFERENCES `user` (`id_user`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `msg_id_conv` FOREIGN KEY (`id_conversation`) REFERENCES `conversation` (`id_conversation`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `message_var`
--
ALTER TABLE `message_var`
  ADD CONSTRAINT `mv_id_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `report`
--
ALTER TABLE `report`
  ADD CONSTRAINT `mc_id_mission` FOREIGN KEY (`id_mission`) REFERENCES `mission` (`id_mission`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `report_ibfk_1` FOREIGN KEY (`id_team_config`) REFERENCES `team_config` (`id_team_config`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `report_part`
--
ALTER TABLE `report_part`
  ADD CONSTRAINT `rp_id_mission` FOREIGN KEY (`id_mission`) REFERENCES `mission` (`id_mission`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `ressource`
--
ALTER TABLE `ressource`
  ADD CONSTRAINT `r_id_mission` FOREIGN KEY (`id_mission`) REFERENCES `mission` (`id_mission`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `r_id_report` FOREIGN KEY (`id_report`) REFERENCES `report` (`id_report`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `teacher`
--
ALTER TABLE `teacher`
  ADD CONSTRAINT `t_id_teacher` FOREIGN KEY (`id_teacher`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `teacher_report_status`
--
ALTER TABLE `teacher_report_status`
  ADD CONSTRAINT `trs_ibfk_1` FOREIGN KEY (`id_report`) REFERENCES `report` (`id_report`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `trs_ibfk_2` FOREIGN KEY (`id_teacher`) REFERENCES `teacher` (`id_teacher`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `teacher_team`
--
ALTER TABLE `teacher_team`
  ADD CONSTRAINT `teacher_team_ibfk_1` FOREIGN KEY (`id_inst`) REFERENCES `institution` (`id_inst`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `team_config`
--
ALTER TABLE `team_config`
  ADD CONSTRAINT `team_config_ibfk_1` FOREIGN KEY (`id_mission`) REFERENCES `mission` (`id_mission`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `team_config_ibfk_2` FOREIGN KEY (`id_class`) REFERENCES `class` (`id_class`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `team_config_ibfk_3` FOREIGN KEY (`allow_import_id_mission`) REFERENCES `mission` (`id_mission`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `team_config_ibfk_4` FOREIGN KEY (`allow_msg_id_class`) REFERENCES `class` (`id_class`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `team_config_ibfk_5` FOREIGN KEY (`allow_msg_id_mission`) REFERENCES `mission` (`id_mission`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `trace`
--
ALTER TABLE `trace`
  ADD CONSTRAINT `trace_ibfk_1` FOREIGN KEY (`id_action`) REFERENCES `trace_action` (`id_action`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `upload`
--
ALTER TABLE `upload`
  ADD CONSTRAINT `upload_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);

--
-- Contraintes pour la table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`current_report`) REFERENCES `report` (`id_report`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `user_ibfk_2` FOREIGN KEY (`id_inst`) REFERENCES `institution` (`id_inst`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
