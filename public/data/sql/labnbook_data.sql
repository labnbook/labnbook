ALTER TABLE `trace_action` CHANGE `action_type` `action_type` ENUM('administration','production','information','communication','t_mission','t_student','t_report','m_teacher') CHARACTER SET ascii COLLATE ascii_bin NOT NULL;

-- Parts of the code have hardcoded behavior on "UGA"
INSERT INTO institution VALUES (2, 'UGA', NULL, True);
-- Parts of the code have hardcoded behavior on "UGA"
INSERT INTO extplatform VALUES (3, 2, 'API', 'TV8b<Nx@h3DtVf2=TvXqrPD=JBR|tzLL', 'http://labnbook-moodle.fr');

-- 8 teachers
INSERT INTO user VALUES (2,'Teacher1','Yukiko','teacher1','f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17','teacher1@example.org',NULL,2,NULL,NULL,NULL,NULL,'2018-01-01 00:00:00',NULL,'2018-01-01 00:00:00');
INSERT INTO teacher VALUES (2,'Biologie','univ','',NULL,'teacher');
INSERT INTO user VALUES (11,'Teacher2','Alain','teacher2','f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17','teacher2@example.org',NULL,2,NULL,NULL,NULL,NULL,'2018-01-01 00:00:00',NULL,'2018-01-01 00:00:00');
INSERT INTO teacher VALUES (11,'Physique','univ','',NULL,'manager');
INSERT INTO user VALUES (12,'Teacher3','Pierre','teacher3','f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17','teacher3@example.org',NULL,2,NULL,NULL,NULL,NULL,'2018-01-01 00:00:00',NULL,'2018-01-01 00:00:00');
INSERT INTO teacher VALUES (12,'Philosophie','univ','',NULL,'manager');
INSERT INTO user VALUES (13,'Teacher4','Paul','teacher4','f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17','teacher4@example.org',NULL,2,NULL,NULL,NULL,NULL,'2018-01-01 00:00:00',NULL,'2018-01-01 00:00:00');
INSERT INTO teacher VALUES (13,'Économie','univ','',NULL,'teacher');
INSERT INTO user VALUES (14,'Teacher5','Jacques','teacher5','f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17','teacher5@example.org',NULL,1,NULL,NULL,NULL,NULL,'2018-01-01 00:00:00',NULL,'2018-01-01 00:00:00');
INSERT INTO teacher VALUES (14,'Logistique','univ','',NULL,'teacher');
INSERT INTO user VALUES (15,'Teacher6','Louis','teacher6','f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17','teacher6@example.org',NULL,1,NULL,NULL,NULL,NULL,'2018-01-01 00:00:00',NULL,'2018-01-01 00:00:00');
INSERT INTO teacher VALUES (15,'Économie','univ','',NULL,'teacher');
INSERT INTO user VALUES (16,'Teacher7','Michel','teacher7','f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17','teacher7@example.org',NULL,1,NULL,NULL,NULL,NULL,'2018-01-01 00:00:00',NULL,'2018-01-01 00:00:00');
INSERT INTO teacher VALUES (16,'Logistique','univ','',NULL,'teacher');
INSERT INTO user VALUES (17,'Teacher8','Henri','teacher8','f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17','teacher8@example.org',NULL,2,NULL,NULL,NULL,NULL,'2018-01-01 00:00:00',NULL,'2018-01-01 00:00:00');
INSERT INTO teacher VALUES (17,'Économie','univ','',NULL,'teacher');

INSERT INTO link_inst_teacher VALUES
(1, 2),
(2, 2),
(11, 2),
(12, 2),
(13, 2),
(14, 1),
(15, 1),
(16, 1),
(17, 2);

-- students
INSERT INTO user VALUES (3,'CAS user','Masako','user-CAS',null,'user-cas@example.org',NULL,2,'12345679',NULL,NULL,NULL,'2018-01-01 00:00:00',NULL,'2018-01-01 00:00:00');
INSERT INTO user VALUES (4,'Student1','Kōtaro','student1','f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17','student1@example.org',NULL,2,NULL,NULL,NULL,NULL,'2019-01-01 12:00:00',NULL,'2018-01-01 00:00:00');
INSERT INTO user VALUES (5,'Student2','Shinji','student2','f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17','student2@example.org',NULL,2,NULL,NULL,NULL,NULL,'2019-01-01 12:00:00',NULL,'2018-01-01 00:00:00');
INSERT INTO user VALUES (61, 'Student3','Masato','student3','f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17','student3@example.org',NULL,2,NULL,NULL,NULL,NULL,'2019-01-01 12:00:00',NULL,'2018-01-01 00:00:00');
INSERT INTO user VALUES (7,'Student4','Évariste','student4','f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17','student4@example.org',NULL,2,NULL,NULL,NULL,NULL,'2019-01-01 12:00:00',NULL,'2018-01-01 00:00:00');
INSERT INTO user VALUES (8,'Student5','N''ji','student5','f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17','student5@example.org',NULL,2,NULL,NULL,NULL,NULL,'2019-01-01 12:00:00',NULL,'2018-01-01 00:00:00');
INSERT INTO user VALUES (9,'Student6','Sans-classe','student6','f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17','student6@example.org',NULL,2,NULL,NULL,NULL,NULL,'2019-01-01 12:00:00',NULL,'2018-01-01 00:00:00');
INSERT INTO user VALUES (10,'Uuga','Uuga','uuga','f4f263e439cf40925e6a412387a9472a6773c2580212a4fb50d224d3a817de17','student-uga@example.org',NULL,2,NULL,NULL,NULL,NULL,'2019-01-01 12:00:00',NULL,'2018-01-01 00:00:00');

-- mission with little content
INSERT INTO `mission` VALUES (1,'mission1','Mission première','<p>Ceci est la première mission</p>','<p>Réussir !</p>','private','2019-01-01','2019-01-02');
INSERT INTO `link_mission_teacher` VALUES (1,2,'designer');
INSERT INTO `report_part` VALUES (1,1,1,'Global',1,1,1,1,'<p>Tout doit s''inscrire ci-dessous.</p>');
INSERT INTO `ressource` VALUES (1,1,NULL,'consigne_longue','1-LabNbook.pdf','assignment',NULL);

INSERT INTO `mission` VALUES (3,'mission3','mission 3 vide','<p>La troisième mission est vide</p>','','private','2019-01-03','2019-01-03');
INSERT INTO `link_mission_teacher` VALUES (3,2,'designer');
INSERT INTO `mission` VALUES (4,'mission4','mission 4 vide','<p>La quatrième mission est vide</p>','','private','2019-01-03','2019-01-03');
INSERT INTO `link_mission_teacher` VALUES (4,1,'designer');
INSERT INTO `mission` VALUES (5,'mission5','mission physique','<p>La mission physique est vide</p>','','private','2019-01-03','2019-01-03');
INSERT INTO `link_mission_teacher` VALUES (5,11,'designer');
INSERT INTO `mission` VALUES (6,'mission6philo','mission philosophie','<p>La mission philosophie est vide</p>','','private','2019-01-03','2019-01-03');
INSERT INTO `link_mission_teacher` VALUES (6,11,'designer');
INSERT INTO `mission` VALUES (7,'mission7philo','mission philosophie2','<p>La mission philosophie est vide</p>','','private','2019-01-03','2019-01-03');
INSERT INTO `link_mission_teacher` VALUES (7,11,'designer');

-- class
INSERT INTO `class` VALUES (11,2,'classe 1','code_classe1','2019-01-01 12:00:00','2019-01-01 12:00:00','normal', null, null);
INSERT INTO `link_class_learner` VALUES (11,4);
INSERT INTO `link_class_learner` VALUES (11,5);
INSERT INTO `link_class_learner` VALUES (11,61);
INSERT INTO `link_class_learner` VALUES (11,7);
INSERT INTO `link_class_learner` VALUES (11,8);
INSERT INTO `link_class_teacher` VALUES (2,1);
INSERT INTO `class` VALUES (2,2,'classe 2',NULL,'2019-01-01 12:00:00','2019-01-01 12:00:00','normal', null, null);
INSERT INTO `link_class_learner` VALUES (2,4);
INSERT INTO `link_class_teacher` VALUES (2,2);
UPDATE institution SET id_class_teacher = 2 WHERE id_inst = 2;
INSERT INTO `class` VALUES (3,2,'classe 3',NULL,'2019-01-01 12:00:00','2019-01-01 12:00:00','normal', null, null);
INSERT INTO `link_class_teacher` VALUES (2,3);
INSERT INTO `class` VALUES (4,2,'classe 4',NULL,'2019-01-01 12:00:00','2019-01-01 12:00:00','normal', null, null);
INSERT INTO `link_class_teacher` VALUES (2,4);
INSERT INTO `class` VALUES (5,2,'classe LDAP',NULL,'2019-01-01 12:00:00','2019-01-01 12:00:00','normal', null, null);
INSERT INTO `link_class_teacher` VALUES (2,5);
INSERT INTO `class` VALUES (6,2,'classe archivée',NULL,'2019-01-01 12:00:00','2019-01-01 12:00:00','archive', null, null);
INSERT INTO `link_class_teacher` VALUES (2,6);
INSERT INTO `class` VALUES (7,1,'classe institution autre','code_classe7','2019-01-01 12:00:00','2019-01-01 12:00:00','normal', null, null);
INSERT INTO `link_class_teacher` VALUES (2,7);
INSERT INTO `class` VALUES (8,2,'classe philosophie UGA','code_classe8','2019-01-01 12:00:00','2019-01-01 12:00:00','normal', null, null);
INSERT INTO `link_class_teacher` VALUES (11,8);
INSERT INTO `class` VALUES (9,2,'classe maths UGA','code_classe9','2019-01-01 12:00:00','2019-01-01 12:00:00','normal', null, null);
INSERT INTO `link_class_teacher` VALUES (11,9);

-- students have reports
INSERT INTO `team_config` VALUES (1,1,1,3,1,1,1,9,NULL,NULL,1,1,1,NULL,0,1,1,3,'Equipe_',0,'2019-06-13 09:29:22',NULL);
INSERT INTO `report` VALUES (1,1,1,NULL,NULL,'new',0,'Equipe_01',1,1,0,0,0,0,-1,'2019-06-01 09:09:09',NULL, NULL);
INSERT INTO `report` VALUES (2,1,1,NULL,NULL,'on',1,'Equipe_02',1,1,0,0,0,0,-1,'2019-06-01 09:09:09','2019-06-01 09:09:09', NULL);
INSERT INTO `report` VALUES (3,1,1,NULL,NULL,'on',1,'Equipe_a_deux',1,1,0,0,0,0,-1,'2019-06-01 09:09:09','2019-06-01 09:09:09', NULL);
INSERT INTO `report` VALUES (4,1,1,NULL,NULL,'wait',1,'fini',1,1,0,0,0,0,-1,'2019-06-01 09:09:09','2019-06-01 09:09:09', NULL);
INSERT INTO `report` VALUES (5,1,1,NULL,NULL,'on',1,'soft delete',1,1,0,0,0,0,-1,'2019-06-01 09:09:09','2019-06-01 09:09:09', '2021-12-15 11:22:22');
INSERT INTO `link_report_learner` VALUES (1,5,NULL,0);
INSERT INTO `link_report_learner` VALUES (2,4,NULL,0);
INSERT INTO `link_report_learner` VALUES (4,8,NULL,0);
INSERT INTO `link_report_learner` VALUES (5,8,NULL,0);
-- student3 and student4 share a report
INSERT INTO `link_report_learner` VALUES (3,61,NULL,0);
INSERT INTO `link_report_learner` VALUES (3,7,NULL,0);

-- student1 has started working on its report
INSERT INTO `labdoc` VALUES (1,1,2,NULL,'text','labdoc texte 1',1,1,1,1,1,1,NULL,4,1560418774,'<p>Soit \\( f : \\mathbb{R}\\mapsto\\mathbb{C} \\) une fonction telle que \\[ \\forall x \\in [0,+\\infty[, f(ix)=f(x) \\]</p>',0,NULL,'2019-06-13 09:35:59');
INSERT INTO `labdoc_status` VALUES (1,4,0,0,0,0,0,1,NULL);

-- report 3 has some content
INSERT INTO `labdoc` VALUES (2,1,3,NULL,'text','labdoc texte',1,1,1,1,1,1,NULL,7,1560436174,'<p>Mon texte à moi : $$ \\sum_{0\\leq k\\leq n} \\frac{1}{k^2} $$</p>',0,NULL,'2019-06-10 10:10:10');
INSERT INTO `labdoc` VALUES (3,1,3,NULL,'drawing','labdoc dessin',2,1,1,1,1,1,NULL,7,1560436192,'zwibbler3.[{\"id\":0,\"type\":\"GroupNode\",\"fillStyle\":\"#cccccc\",\"strokeStyle\":\"#000000\",\"lineWidth\":2,\"shadow\":false,\"matrix\":[1,0,0,1,0,0],\"layer\":\"default\"},{\"id\":1,\"type\":\"PageNode\",\"parent\":0,\"fillStyle\":\"#cccccc\",\"strokeStyle\":\"#000000\",\"lineWidth\":2,\"shadow\":false,\"matrix\":[1,0,0,1,0,0],\"layer\":\"default\"},{\"id\":3,\"type\":\"PathNode\",\"parent\":1,\"fillStyle\":\"#e0e0e0\",\"strokeStyle\":\"#000000\",\"lineWidth\":2,\"shadow\":false,\"matrix\":[1,0,0,1,171,70],\"layer\":\"default\",\"textFillStyle\":\"#000000\",\"fontName\":\"Arial\",\"fontSize\":14,\"dashes\":\"\",\"shapeWidth\":0,\"smoothness\":0.3,\"sloppiness\":0,\"closed\":true,\"arrowSize\":0,\"arrowStyle\":\"simple\",\"doubleArrow\":false,\"text\":\"\",\"roundRadius\":0,\"wrap\":true,\"angleArcs\":0,\"commands\":[0,0,-50,6,50,0,50,-50,6,0,50,50,50,6,-50,0,-50,50,6,0,-50,-50,-50,7],\"seed\":48809}]',0,NULL,'2019-06-10 10:10:10');
INSERT INTO `labdoc` VALUES (4,1,3,NULL,'procedure','labdoc protocole',3,1,1,1,1,1,NULL,7,1560436205,'<?xml version=\"1.0\" encoding=\"UTF-8\"?><LD type=\"protocol\"><metadata><name>Default protocol</name><author></author><lastModification></lastModification></metadata><QHP edit=\"true\" type=\"question\"><label>Question de recherche ou objectif</label><defaultContent>Décrivez l\"objectif de votre expérimentation : la question à laquelle vous voulez répondre et/ou les objets que vous voulez produire.</defaultContent><content>Ma question</content><comment>Mon commentaire</comment></QHP><QHP edit=\"true\" type=\"hypothesis\"><label>Hypothèses ou résultats attendus</label><defaultContent>Lister les hypothèses que vous souhaitez tester au cours de votre expérimentation et/ou les résultats que vous pensez obtenir.</defaultContent><content></content><comment></comment></QHP><QHP edit=\"true\" type=\"principle\"><label>Principe de la manipulation</label><defaultContent>Décrivez rapidement la stratégie, les moyens que vous allez mettre en place. Le principe de manipulation ressemble à un mode opératoire succinct ne contenant pas les paramètres de la manipulation.</defaultContent><content></content><comment></comment></QHP><materialList select=\"true\" add=\"true\" copexGeneralMaterial=\"true\"><label>Liste du matériel</label></materialList><actionList copexGeneralActions=\"true\"><action structured=\"none\"><name>Action libre</name><content/><comment/></action></actionList><procedure><label>Mode opératoire</label><ul xmlns=\"http://www.w3.org/1999/xhtml\"><li rel=\"step\" edit=\"true\" suppr=\"true\" copy=\"true\" move=\"true\" modify=\"true\" iteration=\"1\" parameters=\"1\"><a><content>Étape \"1''</content><comment></comment><table></table></a></li></ul></procedure></LD>',0,NULL,'2019-06-01 10:10:10');
INSERT INTO `labdoc` VALUES (5,1,3,NULL,'dataset','labdoc tableur',4,1,1,1,1,1,NULL,7,1560436302,'<?xml version=\"1.0\" encoding=\"UTF-8\"?><LD type=\"dataset\"><metadata><name>default dataset</name><author>fitex</author><lastModification>2016-03-03</lastModification></metadata><constants><constant><name>pi</name><value>3.141593</value></constant></constants><dataset display_formula=\"true\"><header><column type=\"double\"><code>data1</code><name>data1</name><unit/></column><column type=\"double\"><code>data2</code><name>data2</name><unit/></column><column type=\"double\"><code>data3</code><name>data3</name><unit/></column><column type=\"double\"><code>data4</code><name>data4</name><unit/></column></header><row><value>1</value><value>0</value><value/><value/></row><row><value>2</value><value>5</value><value/><value/></row><row><value>2.5</value><value>8</value><value/><value/></row><row><value>2.8</value><value>-3</value><value/><value/></row><row><value/><value/><value/><value/></row></dataset><graph><name>mon graphe</name><xmin/><xmax/><ymin/><ymax/><plot color=\"Red\"><x>data1</x><y>data2</y><ux/><uy/></plot></graph></LD>',0,NULL,'2019-06-10 10:10:10');
INSERT INTO `labdoc_status` VALUES (2,61,0,1,0,1,1,1,NULL);
INSERT INTO `labdoc_status` VALUES (2,7,0,0,0,0,0,1,NULL);
INSERT INTO `labdoc_status` VALUES (3,61,0,1,0,1,1,1,NULL);
INSERT INTO `labdoc_status` VALUES (3,7,0,0,0,0,0,1,NULL);
INSERT INTO `labdoc_status` VALUES (4,61,0,1,0,1,1,1,NULL);
INSERT INTO `labdoc_status` VALUES (4,7,0,0,0,0,0,1,NULL);
INSERT INTO `labdoc_status` VALUES (5,61,0,1,0,1,1,1,NULL);
INSERT INTO `labdoc_status` VALUES (5,7,0,0,0,0,0,1,NULL);

-- Link user 2 to external institution
INSERT INTO `link_extplatform_user` VALUES (3, 45, 2, '2019-06-01 09:09:09','2019-06-01 09:09:09');

-- TeacherTeams 
INSERT INTO `teacher_team` (`id_teacher_team`, `id_inst`, `name`, `status`, `update_time`, `creation_time`) VALUES 
(1, 2, 'Equipe pédago chimie UGA', 'normal', '2021-04-21 10:10:48', '2021-04-21 10:10:48');
INSERT INTO `link_teacher_tteam` (`id_teacher`, `id_teacher_team`, `status`) VALUES
(11, 1, 'member'),
(11, 1, 'manager');

INSERT INTO `teacher_team` (`id_teacher_team`, `id_inst`, `name`, `status`, `update_time`, `creation_time`) VALUES
(2, 2, 'Equipe pédago physique UGA', 'normal', '2021-04-21 10:10:48', '2021-04-21 10:10:48');
INSERT INTO `link_teacher_tteam` (`id_teacher`, `id_teacher_team`, `status`) VALUES
(11, 2, 'member'),
(11, 2, 'manager'),
(2, 2, 'member');                                                                                 

INSERT INTO `teacher_team` (`id_teacher_team`, `id_inst`, `name`, `status`, `update_time`, `creation_time`) VALUES
(3, 1, 'Equipe pédago maths Todai', 'archive', '2021-04-21 10:10:48', '2021-04-21 10:10:48');
INSERT INTO `link_teacher_tteam` (`id_teacher`, `id_teacher_team`, `status`) VALUES
(16, 3, 'member'),
(16, 3, 'manager');


INSERT INTO `teacher_team` (`id_teacher_team`, `id_inst`, `name`, `status`, `update_time`, `creation_time`) VALUES
(4, 2, 'Equipe pédago philosophie UGA', 'normal', '2021-04-21 10:10:48', '2021-04-21 10:10:48');
INSERT INTO `link_teacher_tteam` (`id_teacher`, `id_teacher_team`, `status`) VALUES
(11, 4, 'member'),
(11, 4, 'manager'),
(12, 4, 'member'),
(13, 4, 'member');
INSERT INTO `link_mission_tteam` VALUES (6,4);
INSERT INTO `link_class_tteam` VALUES (8,4);

INSERT INTO `teacher_team` (`id_teacher_team`, `id_inst`, `name`, `status`, `update_time`, `creation_time`) VALUES
(5, 1, 'Equipe pédago archéologie Todai', 'normal', '2021-04-21 10:10:48', '2021-04-21 10:10:48');
INSERT INTO `link_teacher_tteam` (`id_teacher`, `id_teacher_team`, `status`) VALUES
(14, 5, 'member'),
(14, 5, 'manager'),
(15, 5, 'member');

INSERT INTO `teacher_team` (`id_teacher_team`, `id_inst`, `name`, `status`, `update_time`, `creation_time`) VALUES
(6, 1, 'Equipe pédago économie Todai', 'normal', '2021-04-21 10:10:48', '2021-04-21 10:10:48');
INSERT INTO `link_teacher_tteam` (`id_teacher`, `id_teacher_team`, `status`) VALUES
(14, 6, 'member'),
(14, 6, 'manager'),
(15, 6, 'member'),
(16, 6, 'member');

INSERT INTO `teacher_team` (`id_teacher_team`, `id_inst`, `name`, `status`, `update_time`, `creation_time`) VALUES
(7, 1, 'Equipe pédago maths UGA', 'archive', '2021-04-21 10:10:48', '2021-04-21 10:10:48');
INSERT INTO `link_teacher_tteam` (`id_teacher`, `id_teacher_team`, `status`) VALUES
(11, 7, 'member'),
(11, 7, 'manager');

-- Default lang
UPDATE `user` SET `lang` = "fr";
