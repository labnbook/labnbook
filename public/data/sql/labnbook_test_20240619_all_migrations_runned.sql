-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : labnbook-bdd
-- Généré le : mer. 19 juin 2024 à 10:36
-- Version du serveur : 10.10.2-MariaDB-1:10.10.2+maria~ubu2204
-- Version de PHP : 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `labnbook`
--

-- --------------------------------------------------------

--
-- Structure de la table `annotation`
--

DROP TABLE IF EXISTS `annotation`;
CREATE TABLE `annotation` (
  `id_annotation` int(10) UNSIGNED NOT NULL,
  `id_teacher` int(10) UNSIGNED DEFAULT NULL,
  `id_report` int(10) UNSIGNED NOT NULL,
  `id_labdoc` int(10) UNSIGNED DEFAULT NULL,
  `id_report_part` int(10) UNSIGNED DEFAULT NULL,
  `json` text DEFAULT NULL,
  `type` varchar(30) DEFAULT NULL,
  `creation_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `popularity` smallint(6) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `assessed_student`
--

DROP TABLE IF EXISTS `assessed_student`;
CREATE TABLE `assessed_student` (
  `id_assessed_student` int(10) UNSIGNED NOT NULL,
  `id_assessment` int(10) UNSIGNED NOT NULL,
  `id_student` int(10) UNSIGNED NOT NULL,
  `missing` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `assessed_student`
--

INSERT INTO `assessed_student` (`id_assessed_student`, `id_assessment`, `id_student`, `missing`, `created_at`, `updated_at`) VALUES
(148, 1, 4, 0, '2024-06-18 13:57:48', '2024-06-18 13:57:58'),
(149, 2, 61, 0, '2024-06-18 13:58:09', '2024-06-18 13:58:13'),
(150, 2, 7, 0, '2024-06-18 13:58:09', '2024-06-18 13:58:13');

-- --------------------------------------------------------

--
-- Structure de la table `assessment`
--

DROP TABLE IF EXISTS `assessment`;
CREATE TABLE `assessment` (
  `id_assessment` int(10) UNSIGNED NOT NULL,
  `id_production` int(10) UNSIGNED NOT NULL,
  `id_rubric_origin` int(10) UNSIGNED DEFAULT NULL,
  `bonus` double(8,2) DEFAULT NULL,
  `bonus_comment` longtext DEFAULT NULL,
  `score` double(8,2) DEFAULT NULL,
  `score_comment` longtext DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT 0,
  `grading` double(8,2) DEFAULT NULL,
  `min_grade` double(8,2) DEFAULT NULL,
  `max_grade` double(8,2) DEFAULT NULL,
  `min_bonus` double(8,2) DEFAULT NULL,
  `max_bonus` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `assessment`
--

INSERT INTO `assessment` (`id_assessment`, `id_production`, `id_rubric_origin`, `bonus`, `bonus_comment`, `score`, `score_comment`, `published`, `grading`, `min_grade`, `max_grade`, `min_bonus`, `max_bonus`, `created_at`, `updated_at`) VALUES
(1, 2, 1, NULL, NULL, 23.68, NULL, 1, 34.00, 0.00, 34.00, 0.00, 0.00, '2024-06-18 13:57:48', '2024-06-18 13:57:58'),
(2, 3, 1, NULL, NULL, 7.89, NULL, 0, 34.00, 0.00, 34.00, 0.00, 0.00, '2024-06-18 13:58:09', '2024-06-18 13:58:13');

-- --------------------------------------------------------

--
-- Structure de la table `class`
--

DROP TABLE IF EXISTS `class`;
CREATE TABLE `class` (
  `id_class` int(10) UNSIGNED NOT NULL,
  `id_inst` int(11) DEFAULT NULL,
  `class_name` varchar(32) DEFAULT NULL,
  `class_code` varchar(32) DEFAULT NULL,
  `creation_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` enum('normal','archive') NOT NULL DEFAULT 'normal',
  `ext_code` varchar(255) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL COMMENT 'Class identifier in external institution',
  `id_extplatform` int(11) DEFAULT NULL,
  `num_ext_participants` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `class`
--

INSERT INTO `class` (`id_class`, `id_inst`, `class_name`, `class_code`, `creation_time`, `update_time`, `status`, `ext_code`, `id_extplatform`, `num_ext_participants`) VALUES
(1, 2, 'classe 1', 'code_classe1', '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'normal', NULL, NULL, NULL),
(2, 2, 'classe 2', NULL, '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'normal', NULL, NULL, NULL),
(3, 2, 'classe 3', NULL, '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'normal', NULL, NULL, NULL),
(4, 2, 'classe 4', NULL, '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'normal', NULL, NULL, NULL),
(5, 2, 'classe LDAP', NULL, '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'normal', NULL, NULL, NULL),
(6, 2, 'classe archivée', NULL, '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'archive', NULL, NULL, NULL),
(7, 1, 'classe institution autre', 'code_classe7', '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'normal', NULL, NULL, NULL),
(8, 2, 'classe philosophie UGA', 'code_classe8', '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'normal', NULL, NULL, NULL),
(9, 2, 'classe maths UGA', 'code_classe9', '2019-01-01 11:00:00', '2019-01-01 11:00:00', 'normal', NULL, NULL, NULL),
(10, 2, 'classe-all-user', NULL, '2024-06-19 10:31:48', '2024-06-19 10:31:48', 'normal', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `comment`
--

DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `id_comment` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `id_labdoc` int(10) UNSIGNED NOT NULL,
  `content` longtext DEFAULT NULL,
  `comment_time` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `conversation`
--

DROP TABLE IF EXISTS `conversation`;
CREATE TABLE `conversation` (
  `id_conversation` int(10) UNSIGNED NOT NULL,
  `id_report` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(45) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `criteria_group`
--

DROP TABLE IF EXISTS `criteria_group`;
CREATE TABLE `criteria_group` (
  `id_criteria_group` int(10) UNSIGNED NOT NULL,
  `id_origin` int(10) UNSIGNED DEFAULT NULL,
  `position` smallint(6) NOT NULL DEFAULT 0,
  `description` longtext DEFAULT NULL,
  `relative_weight` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id_rubric` int(10) UNSIGNED DEFAULT NULL,
  `id_assessment` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `criteria_group`
--

INSERT INTO `criteria_group` (`id_criteria_group`, `id_origin`, `position`, `description`, `relative_weight`, `created_at`, `updated_at`, `id_rubric`, `id_assessment`) VALUES
(1, NULL, 0, 'Groupe 1', NULL, '2024-06-18 13:57:35', '2024-06-18 13:57:35', 1, NULL),
(2, NULL, 1, 'Groupe 2', NULL, '2024-06-18 13:57:35', '2024-06-18 13:57:35', 1, NULL),
(3, 1, 0, 'Groupe 1', NULL, '2024-06-18 13:57:48', '2024-06-18 13:57:48', NULL, 1),
(4, 2, 1, 'Groupe 2', NULL, '2024-06-18 13:57:48', '2024-06-18 13:57:48', NULL, 1),
(5, 1, 0, 'Groupe 1', NULL, '2024-06-18 13:58:09', '2024-06-18 13:58:09', NULL, 2),
(6, 2, 1, 'Groupe 2', NULL, '2024-06-18 13:58:09', '2024-06-18 13:58:09', NULL, 2);

-- --------------------------------------------------------

--
-- Structure de la table `criterion`
--

DROP TABLE IF EXISTS `criterion`;
CREATE TABLE `criterion` (
  `id_criterion` int(10) UNSIGNED NOT NULL,
  `id_origin` int(10) UNSIGNED DEFAULT NULL,
  `id_rubric` int(10) UNSIGNED DEFAULT NULL,
  `id_assessment` int(10) UNSIGNED DEFAULT NULL,
  `id_section` int(10) UNSIGNED DEFAULT NULL COMMENT 'Not used yet',
  `id_criteria_group` int(10) UNSIGNED DEFAULT NULL,
  `type` enum('descr','ob') NOT NULL DEFAULT 'descr',
  `position` smallint(6) NOT NULL DEFAULT 0,
  `description` longtext DEFAULT NULL,
  `relative_weight` double(8,2) DEFAULT NULL,
  `comment` longtext DEFAULT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `criterion`
--

INSERT INTO `criterion` (`id_criterion`, `id_origin`, `id_rubric`, `id_assessment`, `id_section`, `id_criteria_group`, `type`, `position`, `description`, `relative_weight`, `comment`, `activated`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, NULL, NULL, 1, 'descr', 0, 'Critère 1', NULL, NULL, 1, '2024-06-18 13:57:35', '2024-06-18 13:57:35'),
(2, NULL, 1, NULL, NULL, 1, 'descr', 1, 'Critère 2', NULL, NULL, 1, '2024-06-18 13:57:35', '2024-06-18 13:57:35'),
(3, NULL, 1, NULL, NULL, 2, 'descr', 0, 'Critère 2', NULL, NULL, 1, '2024-06-18 13:57:35', '2024-06-18 13:57:35'),
(4, NULL, 1, NULL, NULL, 2, 'descr', 1, 'Critère 3', NULL, NULL, 1, '2024-06-18 13:57:35', '2024-06-18 13:57:35'),
(5, 1, NULL, 1, NULL, 3, 'descr', 0, 'Critère 1', NULL, NULL, 1, '2024-06-18 13:57:48', '2024-06-18 13:57:48'),
(6, 3, NULL, 1, NULL, 4, 'descr', 0, 'Critère 2', NULL, NULL, 1, '2024-06-18 13:57:48', '2024-06-18 13:57:48'),
(7, 2, NULL, 1, NULL, 3, 'descr', 1, 'Critère 2', NULL, NULL, 1, '2024-06-18 13:57:48', '2024-06-18 13:57:48'),
(8, 4, NULL, 1, NULL, 4, 'descr', 1, 'Critère 3', NULL, NULL, 1, '2024-06-18 13:57:48', '2024-06-18 13:57:48'),
(9, 1, NULL, 2, NULL, 5, 'descr', 0, 'Critère 1', NULL, NULL, 1, '2024-06-18 13:58:09', '2024-06-18 13:58:09'),
(10, 3, NULL, 2, NULL, 6, 'descr', 0, 'Critère 2', NULL, NULL, 1, '2024-06-18 13:58:09', '2024-06-18 13:58:09'),
(11, 2, NULL, 2, NULL, 5, 'descr', 1, 'Critère 2', NULL, NULL, 1, '2024-06-18 13:58:09', '2024-06-18 13:58:09'),
(12, 4, NULL, 2, NULL, 6, 'descr', 1, 'Critère 3', NULL, NULL, 1, '2024-06-18 13:58:09', '2024-06-18 13:58:09');

-- --------------------------------------------------------

--
-- Structure de la table `descrob`
--

DROP TABLE IF EXISTS `descrob`;
CREATE TABLE `descrob` (
  `id_descrob` int(10) UNSIGNED NOT NULL,
  `id_origin` int(10) UNSIGNED DEFAULT NULL,
  `id_criterion` int(10) UNSIGNED DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `value` double(8,2) DEFAULT NULL,
  `position` smallint(6) NOT NULL DEFAULT 0,
  `selected` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `descrob`
--

INSERT INTO `descrob` (`id_descrob`, `id_origin`, `id_criterion`, `description`, `value`, `position`, `selected`, `created_at`, `updated_at`) VALUES
(1165, NULL, 1, 'aaa', 12.00, 0, 0, '2024-06-18 13:57:35', '2024-06-18 13:57:35'),
(1166, NULL, 1, 'bbb', 13.00, 1, 0, '2024-06-18 13:57:35', '2024-06-18 13:57:35'),
(1167, NULL, 1, 'ccc', 14.00, 2, 0, '2024-06-18 13:57:35', '2024-06-18 13:57:35'),
(1168, NULL, 2, 'aaa', 12.00, 0, 0, '2024-06-18 13:57:35', '2024-06-18 13:57:35'),
(1169, NULL, 2, 'bbb', 13.00, 1, 0, '2024-06-18 13:57:35', '2024-06-18 13:57:35'),
(1170, NULL, 2, 'ccc', 14.00, 2, 0, '2024-06-18 13:57:35', '2024-06-18 13:57:35'),
(1171, NULL, 3, 'aaa', 12.00, 0, 0, '2024-06-18 13:57:35', '2024-06-18 13:57:35'),
(1172, NULL, 3, 'bbb', 13.00, 1, 0, '2024-06-18 13:57:35', '2024-06-18 13:57:35'),
(1173, NULL, 3, 'ccc', 14.00, 2, 0, '2024-06-18 13:57:35', '2024-06-18 13:57:35'),
(1174, NULL, 4, 'aaa', 12.00, 0, 0, '2024-06-18 13:57:35', '2024-06-18 13:57:35'),
(1175, NULL, 4, 'bbb', 13.00, 1, 0, '2024-06-18 13:57:35', '2024-06-18 13:57:35'),
(1176, NULL, 4, 'ccc', 14.00, 2, 0, '2024-06-18 13:57:35', '2024-06-18 13:57:35'),
(1177, 1165, 5, 'aaa', 12.00, 0, 1, '2024-06-18 13:57:48', '2024-06-18 13:57:58'),
(1178, 1166, 5, 'bbb', 13.00, 1, 0, '2024-06-18 13:57:48', '2024-06-18 13:57:58'),
(1179, 1167, 5, 'ccc', 14.00, 2, 0, '2024-06-18 13:57:48', '2024-06-18 13:57:58'),
(1180, 1171, 6, 'aaa', 12.00, 0, 0, '2024-06-18 13:57:48', '2024-06-18 13:57:58'),
(1181, 1172, 6, 'bbb', 13.00, 1, 0, '2024-06-18 13:57:48', '2024-06-18 13:57:58'),
(1182, 1173, 6, 'ccc', 14.00, 2, 0, '2024-06-18 13:57:48', '2024-06-18 13:57:58'),
(1183, 1168, 7, 'aaa', 12.00, 0, 0, '2024-06-18 13:57:48', '2024-06-18 13:57:58'),
(1184, 1169, 7, 'bbb', 13.00, 1, 1, '2024-06-18 13:57:48', '2024-06-18 13:57:58'),
(1185, 1170, 7, 'ccc', 14.00, 2, 0, '2024-06-18 13:57:48', '2024-06-18 13:57:58'),
(1186, 1174, 8, 'aaa', 12.00, 0, 0, '2024-06-18 13:57:48', '2024-06-18 13:57:58'),
(1187, 1175, 8, 'bbb', 13.00, 1, 0, '2024-06-18 13:57:48', '2024-06-18 13:57:58'),
(1188, 1176, 8, 'ccc', 14.00, 2, 1, '2024-06-18 13:57:48', '2024-06-18 13:57:58'),
(1189, 1165, 9, 'aaa', 12.00, 0, 0, '2024-06-18 13:58:09', '2024-06-18 13:58:13'),
(1190, 1166, 9, 'bbb', 13.00, 1, 0, '2024-06-18 13:58:09', '2024-06-18 13:58:13'),
(1191, 1167, 9, 'ccc', 14.00, 2, 0, '2024-06-18 13:58:09', '2024-06-18 13:58:13'),
(1192, 1171, 10, 'aaa', 12.00, 0, 0, '2024-06-18 13:58:09', '2024-06-18 13:58:13'),
(1193, 1172, 10, 'bbb', 13.00, 1, 0, '2024-06-18 13:58:09', '2024-06-18 13:58:13'),
(1194, 1173, 10, 'ccc', 14.00, 2, 0, '2024-06-18 13:58:09', '2024-06-18 13:58:13'),
(1195, 1168, 11, 'aaa', 12.00, 0, 0, '2024-06-18 13:58:09', '2024-06-18 13:58:13'),
(1196, 1169, 11, 'bbb', 13.00, 1, 1, '2024-06-18 13:58:09', '2024-06-18 13:58:13'),
(1197, 1170, 11, 'ccc', 14.00, 2, 0, '2024-06-18 13:58:09', '2024-06-18 13:58:13'),
(1198, 1174, 12, 'aaa', 12.00, 0, 0, '2024-06-18 13:58:09', '2024-06-18 13:58:13'),
(1199, 1175, 12, 'bbb', 13.00, 1, 0, '2024-06-18 13:58:09', '2024-06-18 13:58:13'),
(1200, 1176, 12, 'ccc', 14.00, 2, 0, '2024-06-18 13:58:09', '2024-06-18 13:58:13');

-- --------------------------------------------------------

--
-- Structure de la table `extplatform`
--

DROP TABLE IF EXISTS `extplatform`;
CREATE TABLE `extplatform` (
  `id_extplatform` int(11) NOT NULL,
  `id_inst` int(11) NOT NULL,
  `name` varchar(128) DEFAULT NULL,
  `secret` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `contacts` text DEFAULT NULL,
  `plugin_version` varchar(255) DEFAULT NULL,
  `platform_version` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `extplatform`
--

INSERT INTO `extplatform` (`id_extplatform`, `id_inst`, `name`, `secret`, `url`, `contacts`, `plugin_version`, `platform_version`) VALUES
(3, 2, 'API', 'TV8b<Nx@h3DtVf2=TvXqrPD=JBR|tzLL', 'http://labnbook-moodle.fr', 'Alice (alice@example.org)', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `institution`
--

DROP TABLE IF EXISTS `institution`;
CREATE TABLE `institution` (
  `id_inst` int(11) NOT NULL,
  `name` varchar(128) DEFAULT NULL,
  `id_class_teacher` int(10) UNSIGNED DEFAULT NULL,
  `cas` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `institution`
--

INSERT INTO `institution` (`id_inst`, `name`, `id_class_teacher`, `cas`) VALUES
(1, 'Tōdai', 7, 0),
(2, 'UGA', 2, 0);

-- --------------------------------------------------------

--
-- Structure de la table `labdoc`
--

DROP TABLE IF EXISTS `labdoc`;
CREATE TABLE `labdoc` (
  `id_labdoc` int(10) UNSIGNED NOT NULL,
  `id_report_part` int(10) UNSIGNED DEFAULT NULL,
  `id_report` int(10) UNSIGNED DEFAULT NULL,
  `id_ld_origin` int(10) UNSIGNED DEFAULT NULL,
  `type_labdoc` enum('text','drawing','dataset','procedure','code') NOT NULL,
  `name` varchar(128) DEFAULT NULL,
  `position` int(10) UNSIGNED DEFAULT NULL,
  `editable` tinyint(1) NOT NULL DEFAULT 1,
  `editable_name` int(1) NOT NULL DEFAULT 1,
  `duplicatable` tinyint(1) NOT NULL DEFAULT 1,
  `deleteable` int(1) NOT NULL DEFAULT 1,
  `draggable` int(1) NOT NULL DEFAULT 1,
  `locked` int(11) DEFAULT NULL,
  `last_editor` int(10) UNSIGNED DEFAULT NULL,
  `last_edition` int(11) DEFAULT NULL,
  `last_id_version` bigint(20) DEFAULT NULL,
  `labdoc_data` longtext NOT NULL,
  `draft` tinyint(1) NOT NULL DEFAULT 0,
  `deleted` timestamp NULL DEFAULT NULL,
  `creation_time` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `labdoc`
--

INSERT INTO `labdoc` (`id_labdoc`, `id_report_part`, `id_report`, `id_ld_origin`, `type_labdoc`, `name`, `position`, `editable`, `editable_name`, `duplicatable`, `deleteable`, `draggable`, `locked`, `last_editor`, `last_edition`, `last_id_version`, `labdoc_data`, `draft`, `deleted`, `creation_time`) VALUES
(1, 1, 2, NULL, 'text', 'labdoc texte 1', 1, 1, 1, 1, 1, 1, NULL, 4, 1718718588, 16651933, '<p>Soit \\( f : \\mathbb{R}\\mapsto\\mathbb{C} \\) une fonction telle que \\[ \\forall x \\in [0,+\\infty[, f(ix)=f(x) \\]</p>', 0, NULL, '2019-06-13 07:35:59'),
(2, 1, 3, NULL, 'text', 'labdoc texte', 1, 1, 1, 1, 1, 1, NULL, 7, 1718719374, 48, '<p>Mon texte à moi : <span class=\"ew_ltxformula\" style=\"font-size: 24pt;\" contenteditable=\"false\" data-katex=\"\\sum_{0\\leq k\\leq n} \\frac{1}{k^2}\" data-eplw=\"undefined\"></span></p>\n<p><img src=\"/storage/missions/1/ld_img/e471838c31248e7c7434a0c1e3aadfb903fa200c.png\" alt=\"\" width=\"551\" height=\"282\"></p>\n<p>yes</p>', 0, NULL, '2019-06-10 08:10:10'),
(3, 1, 3, NULL, 'drawing', 'labdoc dessin', 2, 1, 1, 1, 1, 1, NULL, 7, 1560436192, NULL, 'zwibbler3.[{\"id\":0,\"type\":\"GroupNode\",\"fillStyle\":\"#cccccc\",\"strokeStyle\":\"#000000\",\"lineWidth\":2,\"shadow\":false,\"matrix\":[1,0,0,1,0,0],\"layer\":\"default\"},{\"id\":1,\"type\":\"PageNode\",\"parent\":0,\"fillStyle\":\"#cccccc\",\"strokeStyle\":\"#000000\",\"lineWidth\":2,\"shadow\":false,\"matrix\":[1,0,0,1,0,0],\"layer\":\"default\"},{\"id\":3,\"type\":\"PathNode\",\"parent\":1,\"fillStyle\":\"#e0e0e0\",\"strokeStyle\":\"#000000\",\"lineWidth\":2,\"shadow\":false,\"matrix\":[1,0,0,1,171,70],\"layer\":\"default\",\"textFillStyle\":\"#000000\",\"fontName\":\"Arial\",\"fontSize\":14,\"dashes\":\"\",\"shapeWidth\":0,\"smoothness\":0.3,\"sloppiness\":0,\"closed\":true,\"arrowSize\":0,\"arrowStyle\":\"simple\",\"doubleArrow\":false,\"text\":\"\",\"roundRadius\":0,\"wrap\":true,\"angleArcs\":0,\"commands\":[0,0,-50,6,50,0,50,-50,6,0,50,50,50,6,-50,0,-50,50,6,0,-50,-50,-50,7],\"seed\":48809}]', 0, NULL, '2019-06-10 08:10:10'),
(4, 1, 3, NULL, 'procedure', 'labdoc protocole', 3, 1, 1, 1, 1, 1, NULL, 7, 1560436205, NULL, '<?xml version=\"1.0\" encoding=\"UTF-8\"?><LD type=\"protocol\"><metadata><name>Default protocol</name><author></author><lastModification></lastModification></metadata><QHP edit=\"true\" type=\"question\"><label>Question de recherche ou objectif</label><defaultContent>Décrivez l\"objectif de votre expérimentation : la question à laquelle vous voulez répondre et/ou les objets que vous voulez produire.</defaultContent><content>Ma question</content><comment>Mon commentaire</comment></QHP><QHP edit=\"true\" type=\"hypothesis\"><label>Hypothèses ou résultats attendus</label><defaultContent>Lister les hypothèses que vous souhaitez tester au cours de votre expérimentation et/ou les résultats que vous pensez obtenir.</defaultContent><content></content><comment></comment></QHP><QHP edit=\"true\" type=\"principle\"><label>Principe de la manipulation</label><defaultContent>Décrivez rapidement la stratégie, les moyens que vous allez mettre en place. Le principe de manipulation ressemble à un mode opératoire succinct ne contenant pas les paramètres de la manipulation.</defaultContent><content></content><comment></comment></QHP><materialList select=\"true\" add=\"true\" copexGeneralMaterial=\"true\"><label>Liste du matériel</label></materialList><actionList copexGeneralActions=\"true\"><action structured=\"none\"><name>Action libre</name><content/><comment/></action></actionList><procedure><label>Mode opératoire</label><ul xmlns=\"http://www.w3.org/1999/xhtml\"><li rel=\"step\" edit=\"true\" suppr=\"true\" copy=\"true\" move=\"true\" modify=\"true\" iteration=\"1\" parameters=\"1\"><a><content>Étape \"1\'</content><comment></comment><table></table></a></li></ul></procedure></LD>', 0, NULL, '2019-06-01 08:10:10'),
(5, 1, 3, NULL, 'dataset', 'labdoc tableur', 4, 1, 1, 1, 1, 1, NULL, 7, 1718718588, 5, '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<LD type=\"dataset\"><metadata><name>default dataset</name><author>fitex</author><lastModification>2016-03-03</lastModification></metadata><constants><constant><name>pi</name><value>3.141593</value></constant></constants><dataset display_formula=\"true\"><header><column type=\"num\"><code>data1</code><name>data1</name><unit/></column><column type=\"num\"><code>data2</code><name>data2</name><unit/></column><column type=\"num\"><code>data3</code><name>data3</name><unit/></column><column type=\"num\"><code>data4</code><name>data4</name><unit/></column></header><row><value>1</value><value>0</value><value/><value/></row><row><value>2</value><value>5</value><value/><value/></row><row><value>2.5</value><value>8</value><value/><value/></row><row><value>2.8</value><value>-3</value><value/><value/></row><row><value/><value/><value/><value/></row></dataset><graph><name>mon graphe</name><xmin/><xmax/><ymin/><ymax/><plot color=\"Red\"><x>data1</x><y>data2</y><ux/><uy/></plot></graph></LD>\n', 0, NULL, '2019-06-10 08:10:10'),
(6, 1, NULL, NULL, 'text', 'test', 1, 1, 1, 1, 1, 1, 1718719169, 2, 1718719190, NULL, '<div>\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n</div>\n<div>\n<h2>Why do we use it?</h2>\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\n</div>', 0, NULL, '2024-06-18 13:59:29'),
(7, 1, 2, 6, 'text', 'test', 2, 1, 1, 1, 1, 1, NULL, NULL, NULL, NULL, '<div>\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n</div>\n<div>\n<h2>Why do we use it?</h2>\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\n</div>', 0, NULL, '2024-06-18 13:59:50'),
(8, 1, 3, 6, 'text', 'test', 5, 1, 1, 1, 1, 1, NULL, NULL, NULL, NULL, '<div>\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n</div>\n<div>\n<h2>Why do we use it?</h2>\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\n</div>', 0, NULL, '2024-06-18 13:59:50'),
(9, 1, 4, 6, 'text', 'test', 1, 1, 1, 1, 1, 1, NULL, NULL, NULL, NULL, '<div>\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n</div>\n<div>\n<h2>Why do we use it?</h2>\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\n</div>', 0, NULL, '2024-06-18 13:59:50'),
(10, 1, NULL, NULL, 'text', 'test (copie)', 2, 1, 1, 1, 1, 1, 1718719194, 2, 1718719197, NULL, '<div>\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n</div>\n<div>\n<h2>Why do we use it?</h2>\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\n</div>', 0, NULL, '2024-06-18 13:59:54'),
(11, 1, 2, 10, 'text', 'test (copie)', 3, 1, 1, 1, 1, 1, NULL, NULL, NULL, NULL, '<div>\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n</div>\n<div>\n<h2>Why do we use it?</h2>\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\n</div>', 0, NULL, '2024-06-18 13:59:57'),
(12, 1, 3, 10, 'text', 'test (copie)', 6, 1, 1, 1, 1, 1, NULL, NULL, NULL, NULL, '<div>\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n</div>\n<div>\n<h2>Why do we use it?</h2>\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\n</div>', 0, NULL, '2024-06-18 13:59:57'),
(13, 1, 4, 10, 'text', 'test (copie)', 2, 1, 1, 1, 1, 1, NULL, NULL, NULL, NULL, '<div>\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n</div>\n<div>\n<h2>Why do we use it?</h2>\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\n</div>', 0, NULL, '2024-06-18 13:59:57'),
(14, 1, NULL, NULL, 'drawing', 'zwibbler', 3, 1, 1, 1, 1, 1, 1718719198, 2, 1718719205, NULL, 'zwibbler3.[{\"type\":\"BaseNode\",\"id\":0},{\"type\":\"PageNode\",\"id\":1255122431,\"parent\":0,\"$type\":\"PageNode\"},{\"type\":\"PathNode\",\"id\":1918350154,\"parent\":1255122431,\"matrix\":[3.5,0,0,1.1,355,115],\"layer\":\"default\",\"fillStyle\":\"#e0e0e0\",\"strokeStyle\":\"#000000\",\"lineWidth\":1,\"shadow\":false,\"fontName\":\"Arial\",\"fontSize\":14,\"shapeWidth\":0,\"smoothness\":0.3,\"sloppiness\":0,\"closed\":true,\"arrowSize\":0,\"arrowXOffset\":null,\"arrowStyle\":\"simple\",\"doubleArrow\":false,\"text\":\"\",\"roundRadius\":0,\"wrap\":true,\"roughness\":0,\"commands\":[0,-50,-50,1,50,-50,1,50,50,1,-50,50,1,-50,-50,7],\"seed\":13102,\"textFillStyle\":\"#000000\",\"$type\":\"PathNode\"}]', 0, NULL, '2024-06-18 13:59:58'),
(15, 1, 2, 14, 'drawing', 'zwibbler', 4, 1, 1, 1, 1, 1, NULL, NULL, NULL, NULL, 'zwibbler3.[{\"type\":\"BaseNode\",\"id\":0},{\"type\":\"PageNode\",\"id\":1255122431,\"parent\":0,\"$type\":\"PageNode\"},{\"type\":\"PathNode\",\"id\":1918350154,\"parent\":1255122431,\"matrix\":[3.5,0,0,1.1,355,115],\"layer\":\"default\",\"fillStyle\":\"#e0e0e0\",\"strokeStyle\":\"#000000\",\"lineWidth\":1,\"shadow\":false,\"fontName\":\"Arial\",\"fontSize\":14,\"shapeWidth\":0,\"smoothness\":0.3,\"sloppiness\":0,\"closed\":true,\"arrowSize\":0,\"arrowXOffset\":null,\"arrowStyle\":\"simple\",\"doubleArrow\":false,\"text\":\"\",\"roundRadius\":0,\"wrap\":true,\"roughness\":0,\"commands\":[0,-50,-50,1,50,-50,1,50,50,1,-50,50,1,-50,-50,7],\"seed\":13102,\"textFillStyle\":\"#000000\",\"$type\":\"PathNode\"}]', 0, NULL, '2024-06-18 14:00:05'),
(16, 1, 3, 14, 'drawing', 'zwibbler', 7, 1, 1, 1, 1, 1, NULL, NULL, NULL, NULL, 'zwibbler3.[{\"type\":\"BaseNode\",\"id\":0},{\"type\":\"PageNode\",\"id\":1255122431,\"parent\":0,\"$type\":\"PageNode\"},{\"type\":\"PathNode\",\"id\":1918350154,\"parent\":1255122431,\"matrix\":[3.5,0,0,1.1,355,115],\"layer\":\"default\",\"fillStyle\":\"#e0e0e0\",\"strokeStyle\":\"#000000\",\"lineWidth\":1,\"shadow\":false,\"fontName\":\"Arial\",\"fontSize\":14,\"shapeWidth\":0,\"smoothness\":0.3,\"sloppiness\":0,\"closed\":true,\"arrowSize\":0,\"arrowXOffset\":null,\"arrowStyle\":\"simple\",\"doubleArrow\":false,\"text\":\"\",\"roundRadius\":0,\"wrap\":true,\"roughness\":0,\"commands\":[0,-50,-50,1,50,-50,1,50,50,1,-50,50,1,-50,-50,7],\"seed\":13102,\"textFillStyle\":\"#000000\",\"$type\":\"PathNode\"}]', 0, NULL, '2024-06-18 14:00:05'),
(17, 1, 4, 14, 'drawing', 'zwibbler', 3, 1, 1, 1, 1, 1, NULL, NULL, NULL, NULL, 'zwibbler3.[{\"type\":\"BaseNode\",\"id\":0},{\"type\":\"PageNode\",\"id\":1255122431,\"parent\":0,\"$type\":\"PageNode\"},{\"type\":\"PathNode\",\"id\":1918350154,\"parent\":1255122431,\"matrix\":[3.5,0,0,1.1,355,115],\"layer\":\"default\",\"fillStyle\":\"#e0e0e0\",\"strokeStyle\":\"#000000\",\"lineWidth\":1,\"shadow\":false,\"fontName\":\"Arial\",\"fontSize\":14,\"shapeWidth\":0,\"smoothness\":0.3,\"sloppiness\":0,\"closed\":true,\"arrowSize\":0,\"arrowXOffset\":null,\"arrowStyle\":\"simple\",\"doubleArrow\":false,\"text\":\"\",\"roundRadius\":0,\"wrap\":true,\"roughness\":0,\"commands\":[0,-50,-50,1,50,-50,1,50,50,1,-50,50,1,-50,-50,7],\"seed\":13102,\"textFillStyle\":\"#000000\",\"$type\":\"PathNode\"}]', 0, NULL, '2024-06-18 14:00:05'),
(18, 2, NULL, NULL, 'dataset', 'fitex', 1, 1, 1, 1, 1, 1, 1718719213, 2, 1718719234, NULL, '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<LD type=\"dataset\">\n	<metadata>\n		<name>default dataset</name>\n		<author>fitex</author>\n		<lastModification>2016-03-03</lastModification>\n	</metadata>\n	<constants>\n		<constant>\n			<name>pi</name>\n			<value>3.141593</value>\n		</constant>\n	</constants>\n	<dataset display_formula=\"true\">\n		<header>\n			<column type=\"num\"><code>data1</code><name>data1</name><unit/></column>\n			<column type=\"num\"><code>data2</code><name>data2</name><unit/></column>\n			<column type=\"num\"><code>data3</code><name>data3</name><unit/></column>\n			<column type=\"num\"><code>data4</code><name>data4</name><unit/></column>\n		</header>\n		<row><value>1</value><value>2</value><value/><value/></row>\n		<row><value>3</value><value>4</value><value/><value/></row>\n		<row><value/><value/><value/><value/></row>\n		<row><value/><value/><value/><value/></row>\n	</dataset>\n<graph y2=\"false\"><name>aaaa</name><xmin/><xmax/><ymin/><ymax/><xlabel/><ylabel/><plot color=\"#8400CD\" curve=\"no\" axis=\"y\"><x>data1</x><y>data2</y><ux/><uy/></plot></graph></LD>', 0, NULL, '2024-06-18 14:00:13'),
(19, 2, 2, 18, 'dataset', 'fitex', 1, 1, 1, 1, 1, 1, NULL, NULL, NULL, NULL, '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<LD type=\"dataset\">\n	<metadata>\n		<name>default dataset</name>\n		<author>fitex</author>\n		<lastModification>2016-03-03</lastModification>\n	</metadata>\n	<constants>\n		<constant>\n			<name>pi</name>\n			<value>3.141593</value>\n		</constant>\n	</constants>\n	<dataset display_formula=\"true\">\n		<header>\n			<column type=\"num\"><code>data1</code><name>data1</name><unit/></column>\n			<column type=\"num\"><code>data2</code><name>data2</name><unit/></column>\n			<column type=\"num\"><code>data3</code><name>data3</name><unit/></column>\n			<column type=\"num\"><code>data4</code><name>data4</name><unit/></column>\n		</header>\n		<row><value>1</value><value>2</value><value/><value/></row>\n		<row><value>3</value><value>4</value><value/><value/></row>\n		<row><value/><value/><value/><value/></row>\n		<row><value/><value/><value/><value/></row>\n	</dataset>\n<graph y2=\"false\"><name>aaaa</name><xmin/><xmax/><ymin/><ymax/><xlabel/><ylabel/><plot color=\"#8400CD\" curve=\"no\" axis=\"y\"><x>data1</x><y>data2</y><ux/><uy/></plot></graph></LD>', 0, NULL, '2024-06-18 14:00:34'),
(20, 2, 3, 18, 'dataset', 'fitex', 1, 1, 1, 1, 1, 1, NULL, 7, 1718719391, 51, '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<LD type=\"dataset\">\n	<metadata>\n		<name>default dataset</name>\n		<author>fitex</author>\n		<lastModification>2016-03-03</lastModification>\n	</metadata>\n	<constants>\n		<constant>\n			<name>pi</name>\n			<value>3.141593</value>\n		</constant>\n	</constants>\n	<dataset display_formula=\"true\">\n		<header>\n			<column type=\"num\"><code>data1</code><name>data1</name><unit/></column>\n			<column type=\"num\"><code>data2</code><name>data2</name><unit/></column>\n			<column type=\"num\"><code>data3</code><name>data3</name><unit/></column>\n			<column type=\"num\"><code>data4</code><name>data4</name><unit/></column>\n		</header>\n		<row><value>1</value><value>2</value><value/><value/></row>\n		<row><value>3</value><value>4</value><value/><value/></row>\n		<row><value/><value/><value/><value/></row>\n		<row><value/><value/><value/><value/></row>\n	</dataset>\n<graph y2=\"false\"><name>aaaa</name><xmin/><xmax/><ymin/><ymax/><xlabel/><ylabel/><plot color=\"#8400CD\" curve=\"segments\" axis=\"y\"><x>data1</x><y>data2</y><ux/><uy/></plot></graph></LD>', 0, NULL, '2024-06-18 14:00:34'),
(21, 2, 4, 18, 'dataset', 'fitex', 1, 1, 1, 1, 1, 1, NULL, NULL, NULL, NULL, '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<LD type=\"dataset\">\n	<metadata>\n		<name>default dataset</name>\n		<author>fitex</author>\n		<lastModification>2016-03-03</lastModification>\n	</metadata>\n	<constants>\n		<constant>\n			<name>pi</name>\n			<value>3.141593</value>\n		</constant>\n	</constants>\n	<dataset display_formula=\"true\">\n		<header>\n			<column type=\"num\"><code>data1</code><name>data1</name><unit/></column>\n			<column type=\"num\"><code>data2</code><name>data2</name><unit/></column>\n			<column type=\"num\"><code>data3</code><name>data3</name><unit/></column>\n			<column type=\"num\"><code>data4</code><name>data4</name><unit/></column>\n		</header>\n		<row><value>1</value><value>2</value><value/><value/></row>\n		<row><value>3</value><value>4</value><value/><value/></row>\n		<row><value/><value/><value/><value/></row>\n		<row><value/><value/><value/><value/></row>\n	</dataset>\n<graph y2=\"false\"><name>aaaa</name><xmin/><xmax/><ymin/><ymax/><xlabel/><ylabel/><plot color=\"#8400CD\" curve=\"no\" axis=\"y\"><x>data1</x><y>data2</y><ux/><uy/></plot></graph></LD>', 0, NULL, '2024-06-18 14:00:34'),
(22, 2, NULL, NULL, 'procedure', 'proc', 2, 1, 1, 1, 1, 1, 1718719236, 2, 1718719250, NULL, '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<LD type=\"protocol\">\n	<metadata>\n		<name>Default protocol</name>\n		<author/>\n		<lastModification/>\n	</metadata>\n	\n	<QHP edit=\"true\" type=\"question\">\n		<label>Question de recherche ou objectif</label>\n		<defaultContent>Décrivez l\"objectif de votre expérimentation : la question à laquelle vous voulez répondre et/ou les objets que vous voulez produire.</defaultContent>\n		<content/>\n		<comment/>\n	</QHP>\n	\n	<QHP edit=\"true\" type=\"hypothesis\">\n		<label>Hypothèses ou résultats attendus</label>\n		<defaultContent>Lister les hypothèses que vous souhaitez tester au cours de votre expérimentation et/ou les résultats que vous pensez obtenir.</defaultContent>\n		<content/>\n		<comment/>\n	</QHP>\n	\n	<QHP edit=\"true\" type=\"principle\">\n		<label>Principe de la manipulation</label>\n		<defaultContent>Décrivez rapidement la stratégie, les moyens que vous allez mettre en place. Le principe de manipulation ressemble à un mode opératoire succinct ne contenant pas les paramètres de la manipulation.</defaultContent>\n		<content/>\n		<comment/>\n	</QHP>\n	\n	<materialList select=\"true\" add=\"true\" copexGeneralMaterial=\"true\">\n<label>Liste du matériel</label>\n<material definedBy=\"user\" id=\"10000\" quantity=\"100000\">\n   <name>aaa</name>\n   <description/>\n   <type>none</type>\n   <selected>1</selected>\n   <used>1</used>\n</material>\n</materialList>\n	\n	<actionList copexGeneralActions=\"true\">\n	<!-- ceci est la liste des actions disponibles dans ce protocole : jamais modifié par l\"utilisateur-->\n	<!-- une action a toujours un content et un comment - si elle n\"a pas de name, c\"est l\"action libre -->\n		<action structured=\"none\"><!-- l\"action de type \"libre\" -->\n			<name>Action libre</name>\n			<content/> <!-- il n\"y a jamais de contenu par défaut -->\n			<comment/>\n		</action>\n	</actionList>\n	\n	<procedure>\n		<label>Mode opératoire</label>\n		<ul/>\n	</procedure>\n</LD>', 0, NULL, '2024-06-18 14:00:36'),
(23, 2, 2, 22, 'procedure', 'proc', 2, 1, 1, 1, 1, 1, NULL, NULL, NULL, NULL, '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<LD type=\"protocol\">\n	<metadata>\n		<name>Default protocol</name>\n		<author/>\n		<lastModification/>\n	</metadata>\n	\n	<QHP edit=\"true\" type=\"question\">\n		<label>Question de recherche ou objectif</label>\n		<defaultContent>Décrivez l\"objectif de votre expérimentation : la question à laquelle vous voulez répondre et/ou les objets que vous voulez produire.</defaultContent>\n		<content/>\n		<comment/>\n	</QHP>\n	\n	<QHP edit=\"true\" type=\"hypothesis\">\n		<label>Hypothèses ou résultats attendus</label>\n		<defaultContent>Lister les hypothèses que vous souhaitez tester au cours de votre expérimentation et/ou les résultats que vous pensez obtenir.</defaultContent>\n		<content/>\n		<comment/>\n	</QHP>\n	\n	<QHP edit=\"true\" type=\"principle\">\n		<label>Principe de la manipulation</label>\n		<defaultContent>Décrivez rapidement la stratégie, les moyens que vous allez mettre en place. Le principe de manipulation ressemble à un mode opératoire succinct ne contenant pas les paramètres de la manipulation.</defaultContent>\n		<content/>\n		<comment/>\n	</QHP>\n	\n	<materialList select=\"true\" add=\"true\" copexGeneralMaterial=\"true\">\n<label>Liste du matériel</label>\n<material definedBy=\"user\" id=\"10000\" quantity=\"100000\">\n   <name>aaa</name>\n   <description/>\n   <type>none</type>\n   <selected>1</selected>\n   <used>1</used>\n</material>\n</materialList>\n	\n	<actionList copexGeneralActions=\"true\">\n	<!-- ceci est la liste des actions disponibles dans ce protocole : jamais modifié par l\"utilisateur-->\n	<!-- une action a toujours un content et un comment - si elle n\"a pas de name, c\"est l\"action libre -->\n		<action structured=\"none\"><!-- l\"action de type \"libre\" -->\n			<name>Action libre</name>\n			<content/> <!-- il n\"y a jamais de contenu par défaut -->\n			<comment/>\n		</action>\n	</actionList>\n	\n	<procedure>\n		<label>Mode opératoire</label>\n		<ul/>\n	</procedure>\n</LD>', 0, NULL, '2024-06-18 14:00:50'),
(24, 2, 3, 22, 'procedure', 'proc', 2, 1, 1, 1, 1, 1, NULL, NULL, NULL, NULL, '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<LD type=\"protocol\">\n	<metadata>\n		<name>Default protocol</name>\n		<author/>\n		<lastModification/>\n	</metadata>\n	\n	<QHP edit=\"true\" type=\"question\">\n		<label>Question de recherche ou objectif</label>\n		<defaultContent>Décrivez l\"objectif de votre expérimentation : la question à laquelle vous voulez répondre et/ou les objets que vous voulez produire.</defaultContent>\n		<content/>\n		<comment/>\n	</QHP>\n	\n	<QHP edit=\"true\" type=\"hypothesis\">\n		<label>Hypothèses ou résultats attendus</label>\n		<defaultContent>Lister les hypothèses que vous souhaitez tester au cours de votre expérimentation et/ou les résultats que vous pensez obtenir.</defaultContent>\n		<content/>\n		<comment/>\n	</QHP>\n	\n	<QHP edit=\"true\" type=\"principle\">\n		<label>Principe de la manipulation</label>\n		<defaultContent>Décrivez rapidement la stratégie, les moyens que vous allez mettre en place. Le principe de manipulation ressemble à un mode opératoire succinct ne contenant pas les paramètres de la manipulation.</defaultContent>\n		<content/>\n		<comment/>\n	</QHP>\n	\n	<materialList select=\"true\" add=\"true\" copexGeneralMaterial=\"true\">\n<label>Liste du matériel</label>\n<material definedBy=\"user\" id=\"10000\" quantity=\"100000\">\n   <name>aaa</name>\n   <description/>\n   <type>none</type>\n   <selected>1</selected>\n   <used>1</used>\n</material>\n</materialList>\n	\n	<actionList copexGeneralActions=\"true\">\n	<!-- ceci est la liste des actions disponibles dans ce protocole : jamais modifié par l\"utilisateur-->\n	<!-- une action a toujours un content et un comment - si elle n\"a pas de name, c\"est l\"action libre -->\n		<action structured=\"none\"><!-- l\"action de type \"libre\" -->\n			<name>Action libre</name>\n			<content/> <!-- il n\"y a jamais de contenu par défaut -->\n			<comment/>\n		</action>\n	</actionList>\n	\n	<procedure>\n		<label>Mode opératoire</label>\n		<ul/>\n	</procedure>\n</LD>', 0, NULL, '2024-06-18 14:00:50'),
(25, 2, 4, 22, 'procedure', 'proc', 2, 1, 1, 1, 1, 1, NULL, NULL, NULL, NULL, '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<LD type=\"protocol\">\n	<metadata>\n		<name>Default protocol</name>\n		<author/>\n		<lastModification/>\n	</metadata>\n	\n	<QHP edit=\"true\" type=\"question\">\n		<label>Question de recherche ou objectif</label>\n		<defaultContent>Décrivez l\"objectif de votre expérimentation : la question à laquelle vous voulez répondre et/ou les objets que vous voulez produire.</defaultContent>\n		<content/>\n		<comment/>\n	</QHP>\n	\n	<QHP edit=\"true\" type=\"hypothesis\">\n		<label>Hypothèses ou résultats attendus</label>\n		<defaultContent>Lister les hypothèses que vous souhaitez tester au cours de votre expérimentation et/ou les résultats que vous pensez obtenir.</defaultContent>\n		<content/>\n		<comment/>\n	</QHP>\n	\n	<QHP edit=\"true\" type=\"principle\">\n		<label>Principe de la manipulation</label>\n		<defaultContent>Décrivez rapidement la stratégie, les moyens que vous allez mettre en place. Le principe de manipulation ressemble à un mode opératoire succinct ne contenant pas les paramètres de la manipulation.</defaultContent>\n		<content/>\n		<comment/>\n	</QHP>\n	\n	<materialList select=\"true\" add=\"true\" copexGeneralMaterial=\"true\">\n<label>Liste du matériel</label>\n<material definedBy=\"user\" id=\"10000\" quantity=\"100000\">\n   <name>aaa</name>\n   <description/>\n   <type>none</type>\n   <selected>1</selected>\n   <used>1</used>\n</material>\n</materialList>\n	\n	<actionList copexGeneralActions=\"true\">\n	<!-- ceci est la liste des actions disponibles dans ce protocole : jamais modifié par l\"utilisateur-->\n	<!-- une action a toujours un content et un comment - si elle n\"a pas de name, c\"est l\"action libre -->\n		<action structured=\"none\"><!-- l\"action de type \"libre\" -->\n			<name>Action libre</name>\n			<content/> <!-- il n\"y a jamais de contenu par défaut -->\n			<comment/>\n		</action>\n	</actionList>\n	\n	<procedure>\n		<label>Mode opératoire</label>\n		<ul/>\n	</procedure>\n</LD>', 0, NULL, '2024-06-18 14:00:50'),
(26, 2, NULL, NULL, 'code', 'code', 3, 1, 1, 1, 1, 1, 1718719252, 2, 1718719292, NULL, '{\"metadata\":{\"language_info\":{\"codemirror_mode\":{\"name\":\"python\",\"version\":3},\"file_extension\":\".py\",\"mimetype\":\"text/x-python\",\"name\":\"python\",\"nbconvert_exporter\":\"python\",\"pygments_lexer\":\"ipython3\",\"version\":\"3.8\"},\"kernelspec\":{\"name\":\"python\",\"display_name\":\"Python (Pyodide)\",\"language\":\"python\"}},\"nbformat_minor\":4,\"nbformat\":4,\"cells\":[{\"cell_type\":\"code\",\"source\":\"import micropip\\nawait micropip.install([\\\"requests==2.31.0\\\"])\\nimport requests\\nimport pandas as pd\\nimport numpy as np\\nimport base64\\nimport pyodide_http\\nfrom IPython.display import HTML\\nimport csv\\nimport io\\n\\ndef export_file(data=\\\"\\\", data_type=\\\"pandas\\\", filename=\\\"data.csv\\\", title=\\\"Télécharger le fichier csv\\\", **kwargs):\\n    try:\\n        if data_type == \\\"pandas\\\":\\n            return csv_export(csv_data=data.to_csv(**kwargs), title=title, filename=filename)\\n        elif data_type == \\\"numpy\\\":\\n            csv_data = generate_csv_string_from_data(data.tolist(**kwargs), filename, **kwargs)\\n            return csv_export(csv_data=csv_data, title=title, filename=filename)\\n        else:\\n            if not isinstance(data[0], list):\\n                data = [data]\\n            csv_data = generate_csv_string_from_data(data, filename, **kwargs)\\n            return csv_export(csv_data=csv_data, title=title, filename=filename)\\n    except:\\n        print(\\\"Export impossible.\\\")\\n        pass\\n\\ndef generate_csv_string_from_data(data, filename, **kwargs):\\n    output = io.StringIO()\\n    writer = csv.writer(output, **kwargs)\\n    writer.writerows(data)\\n    return output.getvalue()\\n        \\ndef csv_export(csv_data=\\\"\\\", title=\\\"\\\", filename=\\\"\\\"):\\n    b64 = base64.b64encode(csv_data.encode())\\n    payload = b64.decode()\\n    html = \'<a class=\\\"bp3-button bp3-minimal\\\"\'\\n    html += \'  style=\\\"border: 1px solid #eaeaea; color: black;\\\"\'\\n    html += \'  download=\\\"{filename}\\\"\'\\n    html += \'  href=\\\"data:text/csv;base64,{payload}\\\"\'\\n    html += \'  target=\\\"_blank\\\">\'\\n    html += \'{title}\'\\n    html += \'</a>\'\\n    html = html.format(payload=payload, title=title, filename=filename)\\n    return HTML(html)\\n\\ndef import_file(url=\\\"\\\", sep=\\\";\\\", data_format=\\\"pandas\\\", **kwargs):\\n    pyodide_http.patch_all()\\n    try:\\n        if data_format == \\\"pandas\\\":\\n            return pandas_import(url=url, sep=sep, **kwargs)\\n        elif data_format == \\\"numpy\\\":\\n            return numpy_import(url=url, sep=sep, **kwargs)\\n        else:\\n            return txt_import(url=url, **kwargs)\\n    except:\\n        print(\\\"Import impossible dans le format demandé.\\\")\\n        return txt_import(url=url, **kwargs)\\n        pass\\n\\ndef pandas_import(url=\\\"\\\", sep=\\\";\\\", **kwargs):\\n    return pd.read_csv(url, sep=sep, **kwargs)\\n\\ndef numpy_import(url=\\\"\\\", sep=\\\";\\\", **kwargs):\\n    text = requests.get(url).text\\n    c = io.StringIO(text)\\n    return np.genfromtxt(c, delimiter=sep, **kwargs)\\n\\ndef txt_import(url=\\\"\\\", **kwargs):\\n    text = requests.get(url).text\\n    return text\",\"metadata\":{\"tags\":[\"utils_functions\"],\"trusted\":true},\"execution_count\":1,\"outputs\":[]},{\"cell_type\":\"code\",\"source\":\"for i in range(100):\\n    print(\\\"itération numéro \\\" + str(i))\",\"metadata\":{\"trusted\":true},\"execution_count\":3,\"outputs\":[{\"name\":\"stdout\",\"text\":\"itération numéro 0\\nitération numéro 1\\nitération numéro 2\\nitération numéro 3\\nitération numéro 4\\nitération numéro 5\\nitération numéro 6\\nitération numéro 7\\nitération numéro 8\\nitération numéro 9\\nitération numéro 10\\nitération numéro 11\\nitération numéro 12\\nitération numéro 13\\nitération numéro 14\\nitération numéro 15\\nitération numéro 16\\nitération numéro 17\\nitération numéro 18\\nitération numéro 19\\nitération numéro 20\\nitération numéro 21\\nitération numéro 22\\nitération numéro 23\\nitération numéro 24\\nitération numéro 25\\nitération numéro 26\\nitération numéro 27\\nitération numéro 28\\nitération numéro 29\\nitération numéro 30\\nitération numéro 31\\nitération numéro 32\\nitération numéro 33\\nitération numéro 34\\nitération numéro 35\\nitération numéro 36\\nitération numéro 37\\nitération numéro 38\\nitération numéro 39\\nitération numéro 40\\nitération numéro 41\\nitération numéro 42\\nitération numéro 43\\nitération numéro 44\\nitération numéro 45\\nitération numéro 46\\nitération numéro 47\\nitération numéro 48\\nitération numéro 49\\nitération numéro 50\\nitération numéro 51\\nitération numéro 52\\nitération numéro 53\\nitération numéro 54\\nitération numéro 55\\nitération numéro 56\\nitération numéro 57\\nitération numéro 58\\nitération numéro 59\\nitération numéro 60\\nitération numéro 61\\nitération numéro 62\\nitération numéro 63\\nitération numéro 64\\nitération numéro 65\\nitération numéro 66\\nitération numéro 67\\nitération numéro 68\\nitération numéro 69\\nitération numéro 70\\nitération numéro 71\\nitération numéro 72\\nitération numéro 73\\nitération numéro 74\\nitération numéro 75\\nitération numéro 76\\nitération numéro 77\\nitération numéro 78\\nitération numéro 79\\nitération numéro 80\\nitération numéro 81\\nitération numéro 82\\nitération numéro 83\\nitération numéro 84\\nitération numéro 85\\nitération numéro 86\\nitération numéro 87\\nitération numéro 88\\nitération numéro 89\\nitération numéro 90\\nitération numéro 91\\nitération numéro 92\\nitération numéro 93\\nitération numéro 94\\nitération numéro 95\\nitération numéro 96\\nitération numéro 97\\nitération numéro 98\\nitération numéro 99\\n\",\"output_type\":\"stream\"}]},{\"cell_type\":\"code\",\"source\":\"\",\"metadata\":{},\"execution_count\":null,\"outputs\":[]}]}', 0, NULL, '2024-06-18 14:00:52'),
(27, 2, 2, 26, 'code', 'code', 3, 1, 1, 1, 1, 1, NULL, NULL, NULL, NULL, '{\"metadata\":{\"language_info\":{\"codemirror_mode\":{\"name\":\"python\",\"version\":3},\"file_extension\":\".py\",\"mimetype\":\"text/x-python\",\"name\":\"python\",\"nbconvert_exporter\":\"python\",\"pygments_lexer\":\"ipython3\",\"version\":\"3.8\"},\"kernelspec\":{\"name\":\"python\",\"display_name\":\"Python (Pyodide)\",\"language\":\"python\"}},\"nbformat_minor\":4,\"nbformat\":4,\"cells\":[{\"cell_type\":\"code\",\"source\":\"import micropip\\nawait micropip.install([\\\"requests==2.31.0\\\"])\\nimport requests\\nimport pandas as pd\\nimport numpy as np\\nimport base64\\nimport pyodide_http\\nfrom IPython.display import HTML\\nimport csv\\nimport io\\n\\ndef export_file(data=\\\"\\\", data_type=\\\"pandas\\\", filename=\\\"data.csv\\\", title=\\\"Télécharger le fichier csv\\\", **kwargs):\\n    try:\\n        if data_type == \\\"pandas\\\":\\n            return csv_export(csv_data=data.to_csv(**kwargs), title=title, filename=filename)\\n        elif data_type == \\\"numpy\\\":\\n            csv_data = generate_csv_string_from_data(data.tolist(**kwargs), filename, **kwargs)\\n            return csv_export(csv_data=csv_data, title=title, filename=filename)\\n        else:\\n            if not isinstance(data[0], list):\\n                data = [data]\\n            csv_data = generate_csv_string_from_data(data, filename, **kwargs)\\n            return csv_export(csv_data=csv_data, title=title, filename=filename)\\n    except:\\n        print(\\\"Export impossible.\\\")\\n        pass\\n\\ndef generate_csv_string_from_data(data, filename, **kwargs):\\n    output = io.StringIO()\\n    writer = csv.writer(output, **kwargs)\\n    writer.writerows(data)\\n    return output.getvalue()\\n        \\ndef csv_export(csv_data=\\\"\\\", title=\\\"\\\", filename=\\\"\\\"):\\n    b64 = base64.b64encode(csv_data.encode())\\n    payload = b64.decode()\\n    html = \'<a class=\\\"bp3-button bp3-minimal\\\"\'\\n    html += \'  style=\\\"border: 1px solid #eaeaea; color: black;\\\"\'\\n    html += \'  download=\\\"{filename}\\\"\'\\n    html += \'  href=\\\"data:text/csv;base64,{payload}\\\"\'\\n    html += \'  target=\\\"_blank\\\">\'\\n    html += \'{title}\'\\n    html += \'</a>\'\\n    html = html.format(payload=payload, title=title, filename=filename)\\n    return HTML(html)\\n\\ndef import_file(url=\\\"\\\", sep=\\\";\\\", data_format=\\\"pandas\\\", **kwargs):\\n    pyodide_http.patch_all()\\n    try:\\n        if data_format == \\\"pandas\\\":\\n            return pandas_import(url=url, sep=sep, **kwargs)\\n        elif data_format == \\\"numpy\\\":\\n            return numpy_import(url=url, sep=sep, **kwargs)\\n        else:\\n            return txt_import(url=url, **kwargs)\\n    except:\\n        print(\\\"Import impossible dans le format demandé.\\\")\\n        return txt_import(url=url, **kwargs)\\n        pass\\n\\ndef pandas_import(url=\\\"\\\", sep=\\\";\\\", **kwargs):\\n    return pd.read_csv(url, sep=sep, **kwargs)\\n\\ndef numpy_import(url=\\\"\\\", sep=\\\";\\\", **kwargs):\\n    text = requests.get(url).text\\n    c = io.StringIO(text)\\n    return np.genfromtxt(c, delimiter=sep, **kwargs)\\n\\ndef txt_import(url=\\\"\\\", **kwargs):\\n    text = requests.get(url).text\\n    return text\",\"metadata\":{\"tags\":[\"utils_functions\"],\"trusted\":true},\"execution_count\":1,\"outputs\":[]},{\"cell_type\":\"code\",\"source\":\"for i in range(100):\\n    print(\\\"itération numéro \\\" + str(i))\",\"metadata\":{\"trusted\":true},\"execution_count\":3,\"outputs\":[{\"name\":\"stdout\",\"text\":\"itération numéro 0\\nitération numéro 1\\nitération numéro 2\\nitération numéro 3\\nitération numéro 4\\nitération numéro 5\\nitération numéro 6\\nitération numéro 7\\nitération numéro 8\\nitération numéro 9\\nitération numéro 10\\nitération numéro 11\\nitération numéro 12\\nitération numéro 13\\nitération numéro 14\\nitération numéro 15\\nitération numéro 16\\nitération numéro 17\\nitération numéro 18\\nitération numéro 19\\nitération numéro 20\\nitération numéro 21\\nitération numéro 22\\nitération numéro 23\\nitération numéro 24\\nitération numéro 25\\nitération numéro 26\\nitération numéro 27\\nitération numéro 28\\nitération numéro 29\\nitération numéro 30\\nitération numéro 31\\nitération numéro 32\\nitération numéro 33\\nitération numéro 34\\nitération numéro 35\\nitération numéro 36\\nitération numéro 37\\nitération numéro 38\\nitération numéro 39\\nitération numéro 40\\nitération numéro 41\\nitération numéro 42\\nitération numéro 43\\nitération numéro 44\\nitération numéro 45\\nitération numéro 46\\nitération numéro 47\\nitération numéro 48\\nitération numéro 49\\nitération numéro 50\\nitération numéro 51\\nitération numéro 52\\nitération numéro 53\\nitération numéro 54\\nitération numéro 55\\nitération numéro 56\\nitération numéro 57\\nitération numéro 58\\nitération numéro 59\\nitération numéro 60\\nitération numéro 61\\nitération numéro 62\\nitération numéro 63\\nitération numéro 64\\nitération numéro 65\\nitération numéro 66\\nitération numéro 67\\nitération numéro 68\\nitération numéro 69\\nitération numéro 70\\nitération numéro 71\\nitération numéro 72\\nitération numéro 73\\nitération numéro 74\\nitération numéro 75\\nitération numéro 76\\nitération numéro 77\\nitération numéro 78\\nitération numéro 79\\nitération numéro 80\\nitération numéro 81\\nitération numéro 82\\nitération numéro 83\\nitération numéro 84\\nitération numéro 85\\nitération numéro 86\\nitération numéro 87\\nitération numéro 88\\nitération numéro 89\\nitération numéro 90\\nitération numéro 91\\nitération numéro 92\\nitération numéro 93\\nitération numéro 94\\nitération numéro 95\\nitération numéro 96\\nitération numéro 97\\nitération numéro 98\\nitération numéro 99\\n\",\"output_type\":\"stream\"}]},{\"cell_type\":\"code\",\"source\":\"\",\"metadata\":{},\"execution_count\":null,\"outputs\":[]}]}', 0, NULL, '2024-06-18 14:01:32');
INSERT INTO `labdoc` (`id_labdoc`, `id_report_part`, `id_report`, `id_ld_origin`, `type_labdoc`, `name`, `position`, `editable`, `editable_name`, `duplicatable`, `deleteable`, `draggable`, `locked`, `last_editor`, `last_edition`, `last_id_version`, `labdoc_data`, `draft`, `deleted`, `creation_time`) VALUES
(28, 2, 3, 26, 'code', 'code', 3, 1, 1, 1, 1, 1, NULL, NULL, NULL, NULL, '{\"metadata\":{\"language_info\":{\"codemirror_mode\":{\"name\":\"python\",\"version\":3},\"file_extension\":\".py\",\"mimetype\":\"text/x-python\",\"name\":\"python\",\"nbconvert_exporter\":\"python\",\"pygments_lexer\":\"ipython3\",\"version\":\"3.8\"},\"kernelspec\":{\"name\":\"python\",\"display_name\":\"Python (Pyodide)\",\"language\":\"python\"}},\"nbformat_minor\":4,\"nbformat\":4,\"cells\":[{\"cell_type\":\"code\",\"source\":\"import micropip\\nawait micropip.install([\\\"requests==2.31.0\\\"])\\nimport requests\\nimport pandas as pd\\nimport numpy as np\\nimport base64\\nimport pyodide_http\\nfrom IPython.display import HTML\\nimport csv\\nimport io\\n\\ndef export_file(data=\\\"\\\", data_type=\\\"pandas\\\", filename=\\\"data.csv\\\", title=\\\"Télécharger le fichier csv\\\", **kwargs):\\n    try:\\n        if data_type == \\\"pandas\\\":\\n            return csv_export(csv_data=data.to_csv(**kwargs), title=title, filename=filename)\\n        elif data_type == \\\"numpy\\\":\\n            csv_data = generate_csv_string_from_data(data.tolist(**kwargs), filename, **kwargs)\\n            return csv_export(csv_data=csv_data, title=title, filename=filename)\\n        else:\\n            if not isinstance(data[0], list):\\n                data = [data]\\n            csv_data = generate_csv_string_from_data(data, filename, **kwargs)\\n            return csv_export(csv_data=csv_data, title=title, filename=filename)\\n    except:\\n        print(\\\"Export impossible.\\\")\\n        pass\\n\\ndef generate_csv_string_from_data(data, filename, **kwargs):\\n    output = io.StringIO()\\n    writer = csv.writer(output, **kwargs)\\n    writer.writerows(data)\\n    return output.getvalue()\\n        \\ndef csv_export(csv_data=\\\"\\\", title=\\\"\\\", filename=\\\"\\\"):\\n    b64 = base64.b64encode(csv_data.encode())\\n    payload = b64.decode()\\n    html = \'<a class=\\\"bp3-button bp3-minimal\\\"\'\\n    html += \'  style=\\\"border: 1px solid #eaeaea; color: black;\\\"\'\\n    html += \'  download=\\\"{filename}\\\"\'\\n    html += \'  href=\\\"data:text/csv;base64,{payload}\\\"\'\\n    html += \'  target=\\\"_blank\\\">\'\\n    html += \'{title}\'\\n    html += \'</a>\'\\n    html = html.format(payload=payload, title=title, filename=filename)\\n    return HTML(html)\\n\\ndef import_file(url=\\\"\\\", sep=\\\";\\\", data_format=\\\"pandas\\\", **kwargs):\\n    pyodide_http.patch_all()\\n    try:\\n        if data_format == \\\"pandas\\\":\\n            return pandas_import(url=url, sep=sep, **kwargs)\\n        elif data_format == \\\"numpy\\\":\\n            return numpy_import(url=url, sep=sep, **kwargs)\\n        else:\\n            return txt_import(url=url, **kwargs)\\n    except:\\n        print(\\\"Import impossible dans le format demandé.\\\")\\n        return txt_import(url=url, **kwargs)\\n        pass\\n\\ndef pandas_import(url=\\\"\\\", sep=\\\";\\\", **kwargs):\\n    return pd.read_csv(url, sep=sep, **kwargs)\\n\\ndef numpy_import(url=\\\"\\\", sep=\\\";\\\", **kwargs):\\n    text = requests.get(url).text\\n    c = io.StringIO(text)\\n    return np.genfromtxt(c, delimiter=sep, **kwargs)\\n\\ndef txt_import(url=\\\"\\\", **kwargs):\\n    text = requests.get(url).text\\n    return text\",\"metadata\":{\"tags\":[\"utils_functions\"],\"trusted\":true},\"execution_count\":1,\"outputs\":[]},{\"cell_type\":\"code\",\"source\":\"for i in range(100):\\n    print(\\\"itération numéro \\\" + str(i))\",\"metadata\":{\"trusted\":true},\"execution_count\":3,\"outputs\":[{\"name\":\"stdout\",\"text\":\"itération numéro 0\\nitération numéro 1\\nitération numéro 2\\nitération numéro 3\\nitération numéro 4\\nitération numéro 5\\nitération numéro 6\\nitération numéro 7\\nitération numéro 8\\nitération numéro 9\\nitération numéro 10\\nitération numéro 11\\nitération numéro 12\\nitération numéro 13\\nitération numéro 14\\nitération numéro 15\\nitération numéro 16\\nitération numéro 17\\nitération numéro 18\\nitération numéro 19\\nitération numéro 20\\nitération numéro 21\\nitération numéro 22\\nitération numéro 23\\nitération numéro 24\\nitération numéro 25\\nitération numéro 26\\nitération numéro 27\\nitération numéro 28\\nitération numéro 29\\nitération numéro 30\\nitération numéro 31\\nitération numéro 32\\nitération numéro 33\\nitération numéro 34\\nitération numéro 35\\nitération numéro 36\\nitération numéro 37\\nitération numéro 38\\nitération numéro 39\\nitération numéro 40\\nitération numéro 41\\nitération numéro 42\\nitération numéro 43\\nitération numéro 44\\nitération numéro 45\\nitération numéro 46\\nitération numéro 47\\nitération numéro 48\\nitération numéro 49\\nitération numéro 50\\nitération numéro 51\\nitération numéro 52\\nitération numéro 53\\nitération numéro 54\\nitération numéro 55\\nitération numéro 56\\nitération numéro 57\\nitération numéro 58\\nitération numéro 59\\nitération numéro 60\\nitération numéro 61\\nitération numéro 62\\nitération numéro 63\\nitération numéro 64\\nitération numéro 65\\nitération numéro 66\\nitération numéro 67\\nitération numéro 68\\nitération numéro 69\\nitération numéro 70\\nitération numéro 71\\nitération numéro 72\\nitération numéro 73\\nitération numéro 74\\nitération numéro 75\\nitération numéro 76\\nitération numéro 77\\nitération numéro 78\\nitération numéro 79\\nitération numéro 80\\nitération numéro 81\\nitération numéro 82\\nitération numéro 83\\nitération numéro 84\\nitération numéro 85\\nitération numéro 86\\nitération numéro 87\\nitération numéro 88\\nitération numéro 89\\nitération numéro 90\\nitération numéro 91\\nitération numéro 92\\nitération numéro 93\\nitération numéro 94\\nitération numéro 95\\nitération numéro 96\\nitération numéro 97\\nitération numéro 98\\nitération numéro 99\\n\",\"output_type\":\"stream\"}]},{\"cell_type\":\"code\",\"source\":\"\",\"metadata\":{},\"execution_count\":null,\"outputs\":[]}]}', 0, NULL, '2024-06-18 14:01:32'),
(29, 2, 4, 26, 'code', 'code', 3, 1, 1, 1, 1, 1, NULL, NULL, NULL, NULL, '{\"metadata\":{\"language_info\":{\"codemirror_mode\":{\"name\":\"python\",\"version\":3},\"file_extension\":\".py\",\"mimetype\":\"text/x-python\",\"name\":\"python\",\"nbconvert_exporter\":\"python\",\"pygments_lexer\":\"ipython3\",\"version\":\"3.8\"},\"kernelspec\":{\"name\":\"python\",\"display_name\":\"Python (Pyodide)\",\"language\":\"python\"}},\"nbformat_minor\":4,\"nbformat\":4,\"cells\":[{\"cell_type\":\"code\",\"source\":\"import micropip\\nawait micropip.install([\\\"requests==2.31.0\\\"])\\nimport requests\\nimport pandas as pd\\nimport numpy as np\\nimport base64\\nimport pyodide_http\\nfrom IPython.display import HTML\\nimport csv\\nimport io\\n\\ndef export_file(data=\\\"\\\", data_type=\\\"pandas\\\", filename=\\\"data.csv\\\", title=\\\"Télécharger le fichier csv\\\", **kwargs):\\n    try:\\n        if data_type == \\\"pandas\\\":\\n            return csv_export(csv_data=data.to_csv(**kwargs), title=title, filename=filename)\\n        elif data_type == \\\"numpy\\\":\\n            csv_data = generate_csv_string_from_data(data.tolist(**kwargs), filename, **kwargs)\\n            return csv_export(csv_data=csv_data, title=title, filename=filename)\\n        else:\\n            if not isinstance(data[0], list):\\n                data = [data]\\n            csv_data = generate_csv_string_from_data(data, filename, **kwargs)\\n            return csv_export(csv_data=csv_data, title=title, filename=filename)\\n    except:\\n        print(\\\"Export impossible.\\\")\\n        pass\\n\\ndef generate_csv_string_from_data(data, filename, **kwargs):\\n    output = io.StringIO()\\n    writer = csv.writer(output, **kwargs)\\n    writer.writerows(data)\\n    return output.getvalue()\\n        \\ndef csv_export(csv_data=\\\"\\\", title=\\\"\\\", filename=\\\"\\\"):\\n    b64 = base64.b64encode(csv_data.encode())\\n    payload = b64.decode()\\n    html = \'<a class=\\\"bp3-button bp3-minimal\\\"\'\\n    html += \'  style=\\\"border: 1px solid #eaeaea; color: black;\\\"\'\\n    html += \'  download=\\\"{filename}\\\"\'\\n    html += \'  href=\\\"data:text/csv;base64,{payload}\\\"\'\\n    html += \'  target=\\\"_blank\\\">\'\\n    html += \'{title}\'\\n    html += \'</a>\'\\n    html = html.format(payload=payload, title=title, filename=filename)\\n    return HTML(html)\\n\\ndef import_file(url=\\\"\\\", sep=\\\";\\\", data_format=\\\"pandas\\\", **kwargs):\\n    pyodide_http.patch_all()\\n    try:\\n        if data_format == \\\"pandas\\\":\\n            return pandas_import(url=url, sep=sep, **kwargs)\\n        elif data_format == \\\"numpy\\\":\\n            return numpy_import(url=url, sep=sep, **kwargs)\\n        else:\\n            return txt_import(url=url, **kwargs)\\n    except:\\n        print(\\\"Import impossible dans le format demandé.\\\")\\n        return txt_import(url=url, **kwargs)\\n        pass\\n\\ndef pandas_import(url=\\\"\\\", sep=\\\";\\\", **kwargs):\\n    return pd.read_csv(url, sep=sep, **kwargs)\\n\\ndef numpy_import(url=\\\"\\\", sep=\\\";\\\", **kwargs):\\n    text = requests.get(url).text\\n    c = io.StringIO(text)\\n    return np.genfromtxt(c, delimiter=sep, **kwargs)\\n\\ndef txt_import(url=\\\"\\\", **kwargs):\\n    text = requests.get(url).text\\n    return text\",\"metadata\":{\"tags\":[\"utils_functions\"],\"trusted\":true},\"execution_count\":1,\"outputs\":[]},{\"cell_type\":\"code\",\"source\":\"for i in range(100):\\n    print(\\\"itération numéro \\\" + str(i))\",\"metadata\":{\"trusted\":true},\"execution_count\":3,\"outputs\":[{\"name\":\"stdout\",\"text\":\"itération numéro 0\\nitération numéro 1\\nitération numéro 2\\nitération numéro 3\\nitération numéro 4\\nitération numéro 5\\nitération numéro 6\\nitération numéro 7\\nitération numéro 8\\nitération numéro 9\\nitération numéro 10\\nitération numéro 11\\nitération numéro 12\\nitération numéro 13\\nitération numéro 14\\nitération numéro 15\\nitération numéro 16\\nitération numéro 17\\nitération numéro 18\\nitération numéro 19\\nitération numéro 20\\nitération numéro 21\\nitération numéro 22\\nitération numéro 23\\nitération numéro 24\\nitération numéro 25\\nitération numéro 26\\nitération numéro 27\\nitération numéro 28\\nitération numéro 29\\nitération numéro 30\\nitération numéro 31\\nitération numéro 32\\nitération numéro 33\\nitération numéro 34\\nitération numéro 35\\nitération numéro 36\\nitération numéro 37\\nitération numéro 38\\nitération numéro 39\\nitération numéro 40\\nitération numéro 41\\nitération numéro 42\\nitération numéro 43\\nitération numéro 44\\nitération numéro 45\\nitération numéro 46\\nitération numéro 47\\nitération numéro 48\\nitération numéro 49\\nitération numéro 50\\nitération numéro 51\\nitération numéro 52\\nitération numéro 53\\nitération numéro 54\\nitération numéro 55\\nitération numéro 56\\nitération numéro 57\\nitération numéro 58\\nitération numéro 59\\nitération numéro 60\\nitération numéro 61\\nitération numéro 62\\nitération numéro 63\\nitération numéro 64\\nitération numéro 65\\nitération numéro 66\\nitération numéro 67\\nitération numéro 68\\nitération numéro 69\\nitération numéro 70\\nitération numéro 71\\nitération numéro 72\\nitération numéro 73\\nitération numéro 74\\nitération numéro 75\\nitération numéro 76\\nitération numéro 77\\nitération numéro 78\\nitération numéro 79\\nitération numéro 80\\nitération numéro 81\\nitération numéro 82\\nitération numéro 83\\nitération numéro 84\\nitération numéro 85\\nitération numéro 86\\nitération numéro 87\\nitération numéro 88\\nitération numéro 89\\nitération numéro 90\\nitération numéro 91\\nitération numéro 92\\nitération numéro 93\\nitération numéro 94\\nitération numéro 95\\nitération numéro 96\\nitération numéro 97\\nitération numéro 98\\nitération numéro 99\\n\",\"output_type\":\"stream\"}]},{\"cell_type\":\"code\",\"source\":\"\",\"metadata\":{},\"execution_count\":null,\"outputs\":[]}]}', 0, NULL, '2024-06-18 14:01:32');

-- --------------------------------------------------------

--
-- Structure de la table `labdocfile`
--

DROP TABLE IF EXISTS `labdocfile`;
CREATE TABLE `labdocfile` (
  `filehash` binary(20) NOT NULL,
  `id_labdoc` int(10) UNSIGNED NOT NULL,
  `id_mission` int(10) UNSIGNED NOT NULL,
  `uploadtime` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `labdocfile`
--

INSERT INTO `labdocfile` (`filehash`, `id_labdoc`, `id_mission`, `uploadtime`) VALUES
(0x0000000000000000000000000000000000000000, 2, 1, 1718719369);

-- --------------------------------------------------------

--
-- Structure de la table `labdoc_status`
--

DROP TABLE IF EXISTS `labdoc_status`;
CREATE TABLE `labdoc_status` (
  `id_labdoc` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `moved` tinyint(1) NOT NULL DEFAULT 0,
  `drafted` tinyint(1) NOT NULL DEFAULT 0,
  `modified` int(1) NOT NULL DEFAULT 1,
  `mod_icon` int(1) NOT NULL DEFAULT 1,
  `extend` int(1) NOT NULL DEFAULT 1,
  `com_viewed` timestamp NULL DEFAULT NULL,
  `last_id_version_reviewed` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'Last id_trace, corresponding to a labdoc version, that matches last user review'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `labdoc_status`
--

INSERT INTO `labdoc_status` (`id_labdoc`, `id_user`, `deleted`, `moved`, `drafted`, `modified`, `mod_icon`, `extend`, `com_viewed`, `last_id_version_reviewed`) VALUES
(1, 2, 0, 0, 0, 0, 0, 1, NULL, 16651933),
(1, 4, 0, 0, 0, 0, 0, 1, NULL, NULL),
(2, 2, 0, 0, 0, 1, 1, 0, NULL, NULL),
(2, 7, 0, 0, 0, 0, 0, 1, NULL, 48),
(2, 61, 0, 1, 0, 1, 1, 1, NULL, NULL),
(3, 2, 0, 0, 0, 0, 1, 0, NULL, NULL),
(3, 7, 0, 0, 0, 0, 0, 1, NULL, NULL),
(3, 61, 0, 1, 0, 1, 1, 1, NULL, NULL),
(4, 2, 0, 0, 0, 0, 1, 0, NULL, NULL),
(4, 7, 0, 0, 0, 0, 0, 1, NULL, NULL),
(4, 61, 0, 1, 0, 1, 1, 1, NULL, NULL),
(5, 2, 0, 0, 0, 0, 1, 0, NULL, NULL),
(5, 7, 0, 0, 0, 0, 0, 1, NULL, NULL),
(5, 61, 0, 1, 0, 1, 1, 1, NULL, NULL),
(7, 2, 0, 0, 0, 0, 0, 0, NULL, NULL),
(7, 4, 0, 1, 0, 1, 1, 1, NULL, NULL),
(8, 2, 0, 0, 0, 1, 1, 0, NULL, NULL),
(8, 7, 0, 0, 0, 0, 1, 1, NULL, NULL),
(8, 61, 0, 1, 0, 1, 1, 1, NULL, NULL),
(9, 2, 0, 0, 0, 1, 1, 0, NULL, NULL),
(9, 8, 0, 1, 0, 1, 1, 1, NULL, NULL),
(11, 2, 0, 0, 0, 0, 0, 0, NULL, NULL),
(11, 4, 0, 1, 0, 1, 1, 1, NULL, NULL),
(12, 2, 0, 0, 0, 0, 0, 0, NULL, NULL),
(12, 7, 0, 0, 0, 0, 1, 1, NULL, NULL),
(12, 61, 0, 1, 0, 1, 1, 1, NULL, NULL),
(13, 2, 0, 0, 0, 0, 0, 0, NULL, NULL),
(13, 8, 0, 1, 0, 1, 1, 1, NULL, NULL),
(15, 2, 0, 0, 0, 0, 0, 0, NULL, NULL),
(15, 4, 0, 1, 0, 1, 1, 1, NULL, NULL),
(16, 2, 0, 0, 0, 1, 1, 0, NULL, NULL),
(16, 7, 0, 0, 0, 0, 1, 1, NULL, NULL),
(16, 61, 0, 1, 0, 1, 1, 1, NULL, NULL),
(17, 2, 0, 0, 0, 1, 1, 0, NULL, NULL),
(17, 8, 0, 1, 0, 1, 1, 1, NULL, NULL),
(19, 2, 0, 0, 0, 0, 1, 0, NULL, NULL),
(19, 4, 0, 1, 0, 1, 1, 1, NULL, NULL),
(20, 2, 0, 0, 0, 1, 1, 0, NULL, NULL),
(20, 7, 0, 0, 0, 0, 0, 1, NULL, 51),
(20, 61, 0, 1, 0, 1, 1, 1, NULL, NULL),
(21, 2, 0, 0, 0, 1, 1, 0, NULL, NULL),
(21, 8, 0, 1, 0, 1, 1, 1, NULL, NULL),
(23, 2, 0, 0, 0, 0, 1, 0, NULL, NULL),
(23, 4, 0, 1, 0, 1, 1, 1, NULL, NULL),
(24, 2, 0, 0, 0, 1, 1, 0, NULL, NULL),
(24, 7, 0, 0, 0, 0, 1, 1, NULL, NULL),
(24, 61, 0, 1, 0, 1, 1, 1, NULL, NULL),
(25, 2, 0, 0, 0, 1, 1, 0, NULL, NULL),
(25, 8, 0, 1, 0, 1, 1, 1, NULL, NULL),
(27, 2, 0, 0, 0, 0, 1, 0, NULL, NULL),
(27, 4, 0, 1, 0, 1, 1, 1, NULL, NULL),
(28, 2, 0, 0, 0, 1, 1, 0, NULL, NULL),
(28, 7, 0, 0, 0, 0, 1, 1, NULL, NULL),
(28, 61, 0, 1, 0, 1, 1, 1, NULL, NULL),
(29, 2, 0, 0, 0, 1, 1, 0, NULL, NULL),
(29, 8, 0, 1, 0, 1, 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `labdoc_stdalone`
--

DROP TABLE IF EXISTS `labdoc_stdalone`;
CREATE TABLE `labdoc_stdalone` (
  `code` varchar(10) NOT NULL,
  `type_labdoc` enum('text','drawing','dataset','procedure','code') NOT NULL,
  `last_modif` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `labdoc_data` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `link_annotation_learner`
--

DROP TABLE IF EXISTS `link_annotation_learner`;
CREATE TABLE `link_annotation_learner` (
  `id_annotation` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `link_class_learner`
--

DROP TABLE IF EXISTS `link_class_learner`;
CREATE TABLE `link_class_learner` (
  `id_class` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `link_class_learner`
--

INSERT INTO `link_class_learner` (`id_class`, `id_user`) VALUES
(1, 4),
(1, 5),
(1, 7),
(1, 8),
(1, 61),
(2, 4),
(2, 8);

-- --------------------------------------------------------

--
-- Structure de la table `link_class_teacher`
--

DROP TABLE IF EXISTS `link_class_teacher`;
CREATE TABLE `link_class_teacher` (
  `id_teacher` int(10) UNSIGNED NOT NULL,
  `id_class` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `link_class_teacher`
--

INSERT INTO `link_class_teacher` (`id_teacher`, `id_class`) VALUES
(1, 10),
(2, 1),
(2, 2),
(2, 3),
(2, 4),
(2, 5),
(2, 6),
(2, 7),
(11, 8),
(11, 9);

-- --------------------------------------------------------

--
-- Structure de la table `link_class_tteam`
--

DROP TABLE IF EXISTS `link_class_tteam`;
CREATE TABLE `link_class_tteam` (
  `id_class` int(10) UNSIGNED NOT NULL,
  `id_teacher_team` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `link_class_tteam`
--

INSERT INTO `link_class_tteam` (`id_class`, `id_teacher_team`) VALUES
(8, 4);

-- --------------------------------------------------------

--
-- Structure de la table `link_conversation_user`
--

DROP TABLE IF EXISTS `link_conversation_user`;
CREATE TABLE `link_conversation_user` (
  `id_conversation` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `last_seen` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `link_extplatform_user`
--

DROP TABLE IF EXISTS `link_extplatform_user`;
CREATE TABLE `link_extplatform_user` (
  `id_extplatform` int(11) NOT NULL,
  `id_user_ext` varchar(255) NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `link_extplatform_user`
--

INSERT INTO `link_extplatform_user` (`id_extplatform`, `id_user_ext`, `id_user`, `created_at`, `updated_at`) VALUES
(3, '45', 2, '2019-06-01 07:09:09', '2019-06-01 07:09:09');

-- --------------------------------------------------------

--
-- Structure de la table `link_inst_teacher`
--

DROP TABLE IF EXISTS `link_inst_teacher`;
CREATE TABLE `link_inst_teacher` (
  `id_teacher` int(11) UNSIGNED NOT NULL,
  `id_inst` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `link_inst_teacher`
--

INSERT INTO `link_inst_teacher` (`id_teacher`, `id_inst`) VALUES
(1, 2),
(2, 2),
(11, 2),
(12, 2),
(13, 2),
(14, 1),
(15, 1),
(16, 1),
(17, 2);

-- --------------------------------------------------------

--
-- Structure de la table `link_mission_teacher`
--

DROP TABLE IF EXISTS `link_mission_teacher`;
CREATE TABLE `link_mission_teacher` (
  `id_mission` int(10) UNSIGNED NOT NULL,
  `id_teacher` int(10) UNSIGNED NOT NULL,
  `teacher_type` enum('teacher','designer') DEFAULT 'designer'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `link_mission_teacher`
--

INSERT INTO `link_mission_teacher` (`id_mission`, `id_teacher`, `teacher_type`) VALUES
(1, 2, 'designer'),
(3, 2, 'designer'),
(4, 1, 'designer'),
(5, 11, 'designer'),
(6, 11, 'designer'),
(7, 11, 'designer'),
(8, 2, 'designer');

-- --------------------------------------------------------

--
-- Structure de la table `link_mission_tteam`
--

DROP TABLE IF EXISTS `link_mission_tteam`;
CREATE TABLE `link_mission_tteam` (
  `id_mission` int(10) UNSIGNED NOT NULL,
  `id_teacher_team` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `link_mission_tteam`
--

INSERT INTO `link_mission_tteam` (`id_mission`, `id_teacher_team`) VALUES
(6, 4);

-- --------------------------------------------------------

--
-- Structure de la table `link_report_learner`
--

DROP TABLE IF EXISTS `link_report_learner`;
CREATE TABLE `link_report_learner` (
  `id_report` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `last_synchro` int(11) DEFAULT NULL,
  `reload_annotations` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `link_report_learner`
--

INSERT INTO `link_report_learner` (`id_report`, `id_user`, `last_synchro`, `reload_annotations`) VALUES
(1, 5, NULL, 0),
(2, 4, 1718719146, 0),
(3, 7, 1718719736, 0),
(3, 61, NULL, 0),
(4, 8, NULL, 0),
(5, 8, NULL, 0),
(6, 4, NULL, 0),
(6, 5, NULL, 0),
(7, 8, NULL, 0);

-- --------------------------------------------------------

--
-- Structure de la table `link_ressource_user`
--

DROP TABLE IF EXISTS `link_ressource_user`;
CREATE TABLE `link_ressource_user` (
  `id_ressource` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `already_seen` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `link_teacher_tteam`
--

DROP TABLE IF EXISTS `link_teacher_tteam`;
CREATE TABLE `link_teacher_tteam` (
  `id_teacher` int(10) UNSIGNED NOT NULL,
  `id_teacher_team` int(10) UNSIGNED NOT NULL,
  `status` enum('member','manager') CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT 'member'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `link_teacher_tteam`
--

INSERT INTO `link_teacher_tteam` (`id_teacher`, `id_teacher_team`, `status`) VALUES
(2, 2, 'member'),
(11, 1, 'member'),
(11, 1, 'manager'),
(11, 2, 'member'),
(11, 2, 'manager'),
(11, 4, 'member'),
(11, 4, 'manager'),
(11, 7, 'member'),
(11, 7, 'manager'),
(12, 4, 'member'),
(13, 4, 'member'),
(14, 5, 'member'),
(14, 5, 'manager'),
(14, 6, 'member'),
(14, 6, 'manager'),
(15, 5, 'member'),
(15, 6, 'member'),
(16, 3, 'member'),
(16, 3, 'manager'),
(16, 6, 'member');

-- --------------------------------------------------------

--
-- Structure de la table `maillog`
--

DROP TABLE IF EXISTS `maillog`;
CREATE TABLE `maillog` (
  `to` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `sent_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `message`
--

DROP TABLE IF EXISTS `message`;
CREATE TABLE `message` (
  `id_message` int(10) UNSIGNED NOT NULL,
  `msg_content` longtext DEFAULT NULL,
  `msg_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `id_user_sender` int(10) UNSIGNED DEFAULT 1,
  `id_labdoc` int(10) UNSIGNED DEFAULT NULL,
  `id_conversation` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `message_var`
--

DROP TABLE IF EXISTS `message_var`;
CREATE TABLE `message_var` (
  `id_user` int(10) UNSIGNED NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT 1,
  `pos_x` smallint(6) UNSIGNED NOT NULL,
  `pos_y` smallint(6) UNSIGNED NOT NULL,
  `width` smallint(6) UNSIGNED NOT NULL,
  `height` smallint(6) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `message_var`
--

INSERT INTO `message_var` (`id_user`, `visible`, `pos_x`, `pos_y`, `width`, `height`) VALUES
(1, 0, 50, 10, 300, 150),
(2, 0, 50, 10, 300, 150),
(4, 0, 50, 10, 300, 150),
(7, 0, 50, 10, 300, 150);

-- --------------------------------------------------------

--
-- Structure de la table `migration`
--

DROP TABLE IF EXISTS `migration`;
CREATE TABLE `migration` (
  `id` binary(20) NOT NULL,
  `filename` varbinary(255) NOT NULL,
  `filehash` binary(20) NOT NULL,
  `filetime` bigint(20) NOT NULL,
  `appliedtime` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migration`
--

INSERT INTO `migration` (`id`, `filename`, `filehash`, `filetime`, `appliedtime`) VALUES
(0x050418a41fe85f9ae5dc369742edf465f14768e3, 0x323031392d30312d32355f31313734365f647261667465642e73716c, 0x2e26b4bfccb9114f4a158dd8651e4a09b7247044, 1548752847, 20190129100729),
(0x07219e922ff4c585fd1b8516a8c819a0bd5fc6eb, 0x323032312d30342d30375f313630325f4164645f5475746f7269616c5f4d697373696f6e5f547970652e73716c, 0x3d4b58c84c4b5f0660e32c0237ecc473d5ec6c40, 1622727828, 20210603154358),
(0x07600b0031591aff1e911464076696210f587366, 0x323031392d30342d30325f313732355f7465616d2d636f6e6669672e73716c, 0x7c9283918816394c5320a45a7a16d1a5b53ac6c7, 1569327343, 20190924141732),
(0x0d366c6b4cdbc2fbdb0aec85cd2d8c5fa34cf7a4, 0x323031382d31302d30325f303934325f64656d6f2d726571756973697465732e73716c, 0xd15f65b5e1e10af66bcd1e79af6d5ce5d872277d, 1543334648, 20181127170408),
(0x0f7f78c05761e351aec1220e0623191cb3d84bb6, 0x323032302d30332d32395f313734385f6669785f70625f74696d657374616d702e73716c, 0xc6adba595362d73f83a0eab89e6d0e9137e83a7c, 1585552663, 20200330091750),
(0x0fc3530c8bc981368ff0a1a7e3f94af060616194, 0x323031382d30312d32325f313230305f636f6e766572736174696f6e2d6d73672e73716c, 0x5660c049e905d9551827d0b1f45f15051c6ec937, 1517796410, 2018),
(0x108bb120f69f9af5242a9e4c9880957787173583, 0x323032322d30312d32365f31373033345f74726163655f656469745f7465616d696e672e73716c, 0x5c6217e4694579e5973959f10b0de62d7d28141a, 1651759043, 20220505155732),
(0x145bb0b341906f1caf8b0f7d24a8805c0b7054d0, 0x323032322d30362d31365f313435355f6372656174655f646966665f6669656c64732e73716c, 0x1ae3d74287c989f8a9e617dcf109317256ba5fa8, 1664532321, 20220930120542),
(0x14cd969090b9e8eb8fb08b74a588f088f51ab54f, 0x323031382d30332d30315f313635365f6c6162646f6366696c652e73716c, 0xb75d8903edd6357e64a04277db045698ad01f352, 1522956990, 20180406180548),
(0x16b2495929a8a75507a18664ad241de89f2cdc84, 0x323032322d30322d30345f313732315f72656d6f76655f6475706c69636174655f666b2e73716c, 0x46e411eddee9faa71e003857317c7a2cddbaceb2, 1644526732, 20220210215900),
(0x16dbeac738eb31193d47ae3535046f940011d8e9, 0x323032302d30332d33315f313232305f72656d6f76655f726573656172636865725f7374617475732e73716c, 0x17b8b67afb5b60d225f5b374f39225e180c2fb1c, 1585669763, 20200331174930),
(0x19904c80ab438046fa49f63b57426597e90f4c8f, 0x323031392d30312d30385f313434395f68747470732e73716c, 0x929b3c99d89203f4b1868d470bc7c36a2ffa794b, 1547215865, 20190111151110),
(0x1b66c4b371890ed2e984f54e7d122766894ee2cd, 0x323032312d31302d32305f313134325f6164645f6367755f6669656c645f6f6e5f75736572732e73716c, 0xd1e28f1903c45d775db6158a7f45c37c8e4c6e24, 1635338123, 20211027143531),
(0x1d208d91fb6cbefcddf23e4bdb269fa193d43165, 0x323031392d30322d30315f6c645f6261636b75702e73716c, 0x503e4d2c3285503e2e6b2762ac1c6f4fd04de01b, 1549549080, 20190207151801),
(0x1dada0d18c37106a7c2448b81180a46e07ed4b9c, 0x323031382d30312d31315f313230305f7769646765742d6d73672e73716c, 0xa52460ab894d21ce900cf33827ecc8c33ef56d49, 1517796410, 2018),
(0x241d6a3b6db4954c2f71485daa8b797207917ad1, 0x323032302d30392d31365f313334335f6d6f76655f6578745f696e73745f746f5f6578745f706c6174666f726d2e73716c, 0x8a7a16cc677e3b21ffbd898f611d503ca774b8b0, 1600354487, 20200917165459),
(0x2798370267f702988dce802d96d26ad3550bf26a, 0x323031382d30312d32335f313230305f64656275672d636f6d6d656e742e73716c, 0x52d8a202847598cc627bbd65fa097b7c1522f58a, 1517796410, 2018),
(0x2d6cc144facd5e0c07443bdab733573053154c1e, 0x323031382d30392d32375f313733325f72656d6f76652d656d7074792d6772617068732e73716c, 0x9b8c6c868ef7bc51873e24458e60dd333d574dc7, 1538117215, 20180928084658),
(0x2e59e0daea69142a6eb2792370772ef4895fef16, 0x323032302d30392d31375f31333433395f6164645f657874706c6174666f726d5f75726c2e73716c, 0x368917ba77d76a452a443b0aa3fe02ca297fdb45, 1600354487, 20200917165459),
(0x2eae06a13853f20aeac7a90560207002519a59ae, 0x323031382d31312d32325f313335375f64726f705f74726163655f75736572726f6c652e73716c, 0xf99c13d02bd7fbfc63120a726c4134494c202062, 1542964559, 20181123101610),
(0x31647331d90110d48044cbe49f19ad997a1ccb39, 0x323031382d30312d32325f313330305f7265706f72742d6d73672e73716c, 0x3e41de27c5a2e80f6aa96e6e42a0aff14ed49488, 1517796410, 2018),
(0x319999cf019fbb1e1a0dcbb4b2c243d78a202466, 0x323031382d30342d31395f313234305f6e756c6c2d70617373776f72642d666f722d4341532e73716c, 0x993ee703f5eea3f70a42d5fd009cb28e2b0a75a1, 1528264534, 20180606075537),
(0x341379977437ad304b17077c8607efe0a3ffc662, 0x323032312d30352d30345f313230395f61646d696e5f696e5f64622e73716c, 0x7b457860f0f62dfcef2e29cfaabaa9c9a12ca233, 1622122626, 20210527153714),
(0x3499f6fb3678a5443dcf8368fe534a4ea8931f8d, 0x323032302d31312d30345f303932375f54726163655f5245434f5645525f4c442e73716c, 0xe123868a68981e1423b093da3c77c8c0bc56db9f, 1604574794, 20201105121322),
(0x3546b8b62ebc418e85ba300089f9dd0f9e095bce, 0x323031382d31302d31365f313234325f74726163655f746561636865722d766965772d7265706f72742e73716c, 0xaaef973fc800f193328e232b050e98ba209756bd, 1539955314, 20181019152155),
(0x3560f6a5f33c0e61a97665c2ad2c7ebd710f1e48, 0x323031382d31312d32375f313534395f616c7465722d74726163652d616374696f6e732e73716c, 0xa1909542d2a1edc383a8532e279f45378f02b887, 1543581398, 20181130133639),
(0x3578cafb978dc0304da031ceac9f18c4db32b14b, 0x323031392d30352d31345f313735325f757365725f6c616e67756167652e73716c, 0x3de61e70e4edbf7a9149fd85fabe6a00f6884ba4, 1569327343, 20190924141732),
(0x39140be471333cc2dd9763d5f69638ac3d1b3026, 0x323031392d30312d31315f313035355f68747470732d777261707065722e706870, 0x7f57038b81178deb8e7b748e56caf88f327e2536, 1547215865, 20190111151114),
(0x3933d9f852dff996184288f322777ac7a165ced5, 0x323032302d30352d31365f313432385f6e65772e73716c, 0xda39a3ee5e6b4b0d3255bfef95601890afd80709, 1589639327, 20200614155353),
(0x39501ecb65dfbbc528889e685e8316195eb5a300, 0x323032302d30332d32325f313131345f6861726d6f6e697a652d74696d657374616d70732e73716c, 0xa40057c84e9c7e8a1a26259be547fb71df8fc621, 1585220102, 20200326115512),
(0x3b439dd0733c6cb6b862a9081d31cdd580a66b85, 0x323032322d30332d30395f313030325f656469745f7465616d5f636f6e6669672e73716c, 0x62e2e842dce99e3c42ee66759d281a7e345c5518, 1651759043, 20220505155734),
(0x3d49fa28da3c332bd4bb2152cc74ab6a6ab1f3ca, 0x323031372d31322d32385f313230305f636f6d6d656e7461697265732e73716c, 0xe97630e4ac94ccb0697bea17baab15f5612e0f66, 1517796410, 2018),
(0x3f7dcc67848983754d19b3e0a42da004d369d4dd, 0x323031382d30392d32355f313733325f7570646174655f74696d652e73716c, 0x7b008c1747bcd2580844c8515960572c19a479fd, 1538117153, 20180928084617),
(0x3fdeed1b61e3f44a9324fc78caf8c66050f462c1, 0x323032302d30362d32345f313830305f6e65772e73716c, 0xda39a3ee5e6b4b0d3255bfef95601890afd80709, 1593018747, 20200624191237),
(0x400b41d30bcca2741ec40ca825f9c92bdfcde9e6, 0x323032312d30352d32365f313630315f636c617373655f696e737469747574696f6e2e73716c, 0xd126985a83a950647f819811a6bef63d75164704, 1622122626, 20210527153714),
(0x428618f27f2565d1baf009d5ab5b6783514d584c, 0x323032302d30322d31375f313834335f636c6173735f72656d6f76655f656e64646174652e73716c, 0x2c71171e44016927f9d4d9a34c0cb01828c25fb6, 1582207621, 20200220150710),
(0x43bcb34295b80fd38144040916c031bc2ff8fa09, 0x323031382d31312d32335f313233325f6669782d7472756e63617465642d616e6e6f746174696f6e732e73716c, 0xd9c34992cf9eb6cfa575f414085a7dc440cc10b0, 1542992441, 20181123180052),
(0x43c9c53838c57bbe784658a4fa32f852ed6f13f8, 0x323031392d30382d32305f313035335f6d697373696f6e2d617263686976652e73716c, 0xbcc73dfbf43bd00578b08c3e52319eefea69d8a6, 1569327343, 20190924141853),
(0x43fb2c400127656a4b94d9ca7cd49f35abd76329, 0x323031382d30392d30375f313730305f7465616d5f636f6e6669672d696d706f72745f6c642e73716c, 0x8857f667e97c4ee1530b89136985141499efb553, 1536679667, 20180911173002),
(0x463b6112771e99caabf5165a600bfaa9f46f7029, 0x323032322d30362d31355f313132365f6164645f73636f72696e675f7661722e73716c, 0x4db9b9cd9ea508c1ba5597c450d5b2efb19b0dd2, 1695909259, 20230928155430),
(0x4bf4502950421976bb765f23f9847e9644abba14, 0x323031382d30342d32305f313134345f72656d6f76652d7365727665722d6e616d652d66726f6d2d55524c732e73716c, 0x6f5ad1e0ab1ac82586f31d782436f2a63dfd1df7, 1525334710, 20180503100644),
(0x4cc5e36ac5a5a445b2cc17851eff73765cac2c91, 0x323031392d30382d32395f313133335f6e65772e73716c, 0xda39a3ee5e6b4b0d3255bfef95601890afd80709, 1589632685, 20200516160334),
(0x4ff5815af73b254676c3a55ed3b6db153011df84, 0x323031382d30322d30385f313031395f6d697373696e672d666b2e73716c, 0x126a88b4c558bd159e9470dfe2b9d24a3deac8df, 1518700467, 2018),
(0x54c8f8c87b005e3c51f3bc7f54ad5ff79c97aec0, 0x323032312d30332d32395f303934325f526573746f72655f69645f6c645f6f726967696e2e73716c, 0xdb22115a5aa4439fac85399c8c56bc91e5c8269d, 1617091370, 20210330100315),
(0x58034cb13f426e4e0e15dfb74b10952d7868ba49, 0x323032312d31322d31355f313431335f696e6372656173655f636c6173735f6e616d655f73697a652e73716c, 0x2c22cc828588b2db83dac6c8d7ec6733750c3c94, 1639664017, 20211216151351),
(0x58e636d903d3e3bc37e99a73da823668a6ff589c, 0x323032322d30342d30385f313230305f6164645f6e756d5f6578745f7061727469636970616e74735f746f5f636c6173732e73716c, 0x92cb6a5ca8ee1a3be9c58d71b1913c70b75ee558, 1655995866, 20220623165113),
(0x5c8efda3295ff5d8936a1a5bde1f2e19f81b860c, 0x323031392d30352d30335f313635365f73706c69745f74726163655f6475706c69636174652d6c642e706870, 0x3b748e4ef9533aed5376b9e365e23bc8c07d6a8f, 1557204559, 20190507065042),
(0x6065d15341d40a3deda279ea16cf34602616d4d5, 0x323031392d30362d30365f303935335f6372656174655f696e6465785f74726163652e73716c, 0x5eacf51d4c663654f2662eb23fa71ab4a51979e0, 1589978926, 20200614155353),
(0x610adeb57874a6cb6290679dc25307a16d5f1310, 0x323031382d30382d33305f303832305f66697465782d636f6c756d6e2d636f64652e706870, 0x82706417f5930cfa04775e4aad87fed4c1b53ccf, 1536679667, 20180911173002),
(0x636ffdf1e2a2ff8d1449ec6ec08e90021a81f6c6, 0x323031382d31322d30345f313532355f7472616365735f746561636865722e73716c, 0x3feeaedc1c37e609afff50d081f4c86312bde736, 1548336726, 20190124143206),
(0x637d17de7cea343d44a844542f7991b983241a46, 0x323032312d30352d30345f313230345f6361735f696e737469747574696f6e5f696e5f64622e73716c, 0xd7afbc23c839dadd9abeed564de7977549a510ab, 1622122626, 20210527153714),
(0x64d3d32ee54eacd919b03fc424fc5315a0da6696, 0x323032322d30322d31305f313031325f61766f69645f64656c6574655f636173636164655f636c6173735f696e737469747574696f6e2e73716c, 0xa6ec5b00db03ee5c2d54d2d91882fabd0369939b, 1644526594, 20220210215736),
(0x6b15f2099c463d98fde0d1452af5c6418d2b8613, 0x323031392d30352d33315f313535305f6c6162646f635f64656c657465645f747970655f7570646174652e73716c, 0xfeb9108ede141cb8d1f33ff28374697ccb47853d, 1569327343, 20190924141853),
(0x6ce82828cab7bd28244306ecc8b1b6a02847319a, 0x323031392d30392d31375f313535335f6f6c645f73747564656e745f7465616d696e672e73716c, 0x62dcc2550e1eb3e35bbe73873eea986a43f527a4, 1569327343, 20190924141854),
(0x6d725d4ffcd333f875f068ddff409590b1817b7f, 0x323031392d31322d31315f313634365f3330322d6475706c69636174652d7472616365732e73716c, 0x9e28f5f8dc716631d2ebe08668063884ff291aff, 1576080470, 20191211170804),
(0x6dd4d23230233947e3f8ce3f55e0f3a3319070cb, 0x323031392d30382d32385f303831365f6e65772e73716c, 0xda39a3ee5e6b4b0d3255bfef95601890afd80709, 1589632685, 20200516160334),
(0x7854ecabd9a02db2a07f23efb2064b4e4e61a4e4, 0x323032302d30362d31315f313635305f7461626c655f616e6e6f746174696f6e5f72656d6f76655f617474726962757465735f636f6c756d6e5f7570646174655f74696d652e73716c, 0x247ffee913bb034652dfc7e0e5be53cb1cc232fb, 1591954455, 20200614155353),
(0x79733c23f2c87bf7ae232e3b66b82b10742c0084, 0x323032302d30322d32305f303834335f6669782d7265706f72742d7374617475732e73716c, 0x302cf600e89229cde6879e3e6980d9d2105ed18e, 1582818642, 20200227165048),
(0x7ba69705bd9000a3038d296c3b650386840a9a09, 0x323031382d30332d32325f313030355f756e697175655f696e73742d6e756d6265722e73716c, 0x59c1ce5c6d2ba382c792ef32319d8b94a06efb0c, 1521819129, 2018),
(0x7fde4d7c976ef24d48c6a4dda0cee8fa13d51608, 0x323032302d30312d31345f313735335f7265666f726d61742d6c625f747261636b732e73716c, 0xb9068ca9b4a2b9746e1a51e2a7797813b6b226c7, 1579077732, 20200115094227),
(0x81aff569db672e0967d89750ab66d9d8efd67ea3, 0x323031382d31322d31315f303835325f7265706f72742d64617465732d74696d652e73716c, 0xe770834131608505ec45b62bafaea215956392ce, 1548336726, 20190124143207),
(0x8637b1328bfdcc6a1d2fdebcfad4761dea5a90ea, 0x323032322d30322d30335f313530385f72656e616d655f7265706f72745f6669656c64732e73716c, 0x3c18d0144856f35b96302e77251602bf9c303abb, 1645102397, 20220217135327),
(0x874d53d0123b7c32ad7063d27be88064c59d78b2, 0x323031382d30332d31395f313931305f74726163655f6164645f636f6e762e73716c, 0x0e54f2e3279b4f686af0d3c83711828e3d4a9e36, 1521818978, 2018),
(0x8b1ac0c377e7a31914b674fe95bd263a38e97d18, 0x323032322d30322d30345f313731325f6c726c5f72656c6f61645f616e6e6f746174696f6e732e73716c, 0xb801a951e4a4430cf8bc73567c9d1093250e8ffc, 1644526594, 20220210215645),
(0x8b4b454536b9dd0db3f1cf4182df761150165f45, 0x323031382d31302d32335f313431325f7265706f72745f696e697469616c697a65642e73716c, 0xad77fa56d642475801a14f7b1bde5bded15ae206, 1540469144, 20181025140546),
(0x91305f551bc810fbb10e613f5a9cca3b352cba8f, 0x323032322d30322d30335f313832345f74626c5f746561636865725f7265706f72745f7374617475732e73716c, 0x8da2cba52dbf78dff432dc68387e80971f36c337, 1644526594, 20220210215645),
(0x9281b2f3721fc735b1c63e8e7a86514ae6bb68f7, 0x323032322d30342d30385f313031305f6e65772e73716c, 0xda39a3ee5e6b4b0d3255bfef95601890afd80709, 1649405434, 20220428232132),
(0x9318ea146eae982e3f842bb8121a4e37d8b0b6dc, 0x323032312d30332d30342d313633355f64656c6574655f77726f6e675f6c646475706c6963617465732e73716c, 0xb1f5db5e042bbd1d68ba640c2b1587e80e3a3d18, 1614872397, 20210304164021),
(0x96bb4898e76d3e5c247611387012ae6f077a2864, 0x323032302d31322d30325f313933325f7570646174655f6d697373696f6e735f696e746572666163652e73716c, 0xea9dc5089025efd50ab20ec21807824e4385ccaf, 1607637525, 20201210225859),
(0x99704cd7c2386a03714cf9b02f4c5c5b96252660, 0x323032312d30332d30325f313132375f6164645f6c696e6b5f616e6e6f746174696f6e5f6c6561726e65722e73716c, 0x2bdb4c4fa96b61d341147ca056523824ce065594, 1615465053, 20210311131752),
(0x9dd0ec35e76892becec7bcf2d88ccc857d400567, 0x323031392d30352d30335f313633365f74726163655f736176652d61747461636865642d6c642e73716c, 0xee876e5679629a61320f4b3d9b7efde54b5a9075, 1557204559, 20190507065041),
(0x9fa78d13e52322bf1af09d5f621fc2218c4fea95, 0x323031382d30382d31315f696d706f72742d6c642d66726f6d2d7265706f72742e73716c, 0x1f2bdb2ae5b9483e683506bfc87820215279bc97, 1536679667, 20180911172755),
(0xa5806c6419070976908baabb8b541f227dad1c6f, 0x323032322d30322d32355f313831385f6368616e67655f616374696f6e5f35305f6e616d652e73716c, 0x37e9d99081eefc8e41b434faeb519de879fb68a2, 1646327137, 20220303180545),
(0xa73370e4cd92a7673b1f1f0f80cc5f637ffd007f, 0x323031392d31322d31315f313235335f3330312d66697865642d6d697865642d75702d7472616365732e73716c, 0xf281acdcac379f5275b50272d7126650f7b33a18, 1576080470, 20191211170756),
(0xad0ab41b51d318ae014e1e3a923fc1835bdd42e1, 0x323031382d30332d31365f303932345f7472616365732d657874656e6465642e73716c, 0x87f6fb37838212c84385560932ead0908088b7e6, 1521818978, 2018),
(0xae359b9f8f942da8f2090a75fa94ea29a61b81cb, 0x323032302d30362d32345f313735345f64726f705f6a6f62732e73716c, 0x041dc838403c91bd66cb476a41075e498f2fcb3a, 1593018747, 20200624191237),
(0xb2a1f364cdb096227bd4f11bedfbc8784515f1bd, 0x323032312d30332d33315f313830305f44726f705f7461626c655f6c6162646f635f6261636b75702e73716c, 0xb231ddc6c80796a7029d14054e451c5a0496a239, 1621518332, 20210520154540),
(0xb3ae34be1d559aa84e0b1317d946cb1eb0029b61, 0x323032312d30362d33305f313531315f746561636865725f7465616d5f74726163652e73716c, 0x95dbd8902d3a7ca046d2ff99eefe87ce3d1ab9be, 1631198194, 20210909163645),
(0xb507ac60c5d84e6c6e86377e6288f6d3204c30cf, 0x323031392d30332d30385f74726163655f616374696f6e2e73716c, 0x799045b9ebdb1900ad28cb9260984de86a0fd101, 1552387438, 20190312114358),
(0xb6e37343a633580f763fcb94a95415665a3f17a0, 0x323031382d30372d31365f696d706f72742d6c642d66726f6d2d7265706f72742e73716c, 0x7ad4b31b3d7ffea346f324d15bf17380bdcae98f, 1536679667, 20180911172755),
(0xba839de76322c4ebed1c3bda062a19e66a2fa5b3, 0x323031382d30332d30325f303834375f74726163652e73716c, 0x2d5d6eb7a1dbd646e1f16432ee9113a8ac3c5fe4, 1520013554, 2018),
(0xc61f1f50c1770c636dfbde3a0aef7e7ebbed20e6, 0x323032312d30332d30332d313430365f72656e616d655f74726163655f726561645f616e6e6f746174696f6e2e73716c, 0x3ec4996134ff510e8b6beb097d12f4cdf5442267, 1614790074, 20210303174802),
(0xc6ca0584ca8b0600c8fe2e02605ecc1ac4cf4f11, 0x323032312d30342d30315f313730345f74726163655f6f70656e5f6c645f76657273696f6e696e672e73716c, 0x5073f89bf6cb7f8457db5b2146402aea7b202f03, 1621518332, 20210520154540),
(0xc745867577bafc623e48990cd5ddd695ff62db1f, 0x323032322d30352d3031335f313531355f6164645f74726163655f617263686976655f7265706f72742e73716c, 0x115a73cebba57877bbea9ef5bd38dd765692620e, 1654254704, 20220603131152),
(0xc9100c2bd3212e093d299a4343877bd54e46a4a0, 0x323031392d30362d31325f303935305f7570646174655f636c6173732e73716c, 0x619655125088f6ea9a194c8186f1756c1d3fa647, 1569327343, 20190924141853),
(0xca5fe11fcb255017be87c6130a90b69fd6f5641d, 0x323031382d30342d30365f313133385f7472616365732d6669782d616374696f6e732e73716c, 0xe65241c1095a77afc02c4055f5addaf1b4aca54e, 1523030664, 20180406180548),
(0xcb160b29d7e313cccc1f9a24f3749cc4fde13627, 0x323031382d30342d31305f313231305f6669782d616e6e6f746174696f6e2d646174612e73716c, 0xe8b1f2df8e36cc1bbea5ec0d7585c880fbd627c3, 1523357017, 20180410124344),
(0xd84c57b29ab0f6c38f7ec4f02270bb81d7c4dcfd, 0x323032302d30392d30395f313732365f6164645f616374696f6e5f544541434845525f53454c4543545f5245504f52542e73716c, 0x7fb6cbfe2906914b6259100850778e53ae6edc16, 1599666643, 20200909175054),
(0xdf76dd4928ae79625bb17ca820d9ccfef4e51ac3, 0x323031382d30392d31315f303935315f7465616d5f636f6e6669675f6e616d652d7072656669782e73716c, 0x9f55f66a695c284058992768dc965d6def617be2, 1536679667, 20180911173002),
(0xe63fe74ef079bbc12c56730124193881be407bc7, 0x323031382d31312d32335f313534355f72656e616d655f636f6e762d757365725f6c6173742d7365656e2e73716c, 0x4a8d7a0f21ab5c7a9714dc38f8d8e1c977d92839, 1544089727, 20181206104847),
(0xe6e68a91f9a4db2ec142192bce2e85b87d25386f, 0x323031382d30342d32305f313734385f7465616d732e73716c, 0x5bcfcae6647816f8a78133bdb7cec04810e35975, 1536679667, 20180911172755),
(0xe9f3197ef477da9aa08612b3b5c4937142bc85c8, 0x323032302d31322d30325f313734385f72656d6f76655f74696d657374616d705f7570646174652e73716c, 0x6d4032595b131281d67f7384128acada4bc475b2, 1607637525, 20201210225859),
(0xecbb0ca15da5dbd3d10e99acbfeab75c3f549061, 0x323032312d30332d32345f323133355f6164645f746561636865725f7465616d5f616e645f6173736f6369617465645f6c696e6b2e73716c, 0x65b9f8973befc6862137f5ca3ecf97e7674ecaea, 1623139274, 20210608100124),
(0xf28c77815df65d830c278a4d073549d3dbe736f8, 0x323032312d30322d30395f313130365f6164645f6d697373696e675f64617465735f6d697373696f6e732e73716c, 0xdd6b21d46d93b43c0d10c289a0bcd7233dcfe183, 1613145342, 20210212165550),
(0xf6533ff273708e5411a2f5c03d017ea464f4e86f, 0x323031392d30362d31325f313535385f74726173685f776964672e73716c, 0xed5b731c2b93ec42cb1dae98b4baba58cd18eea2, 1569327343, 20190924141853),
(0xf9e767bfec736fa08b3169553c1edb4c7c0f1780, 0x323032312d31312d32345f313033305f686172645f64656c6574655f6c645f74726163652e73716c, 0xf4f39a7c679ab75d9569d675733306974a5fcd94, 1637841654, 20211125130102),
(0xfa9d94b3fe1f9bf400b8293bd650b0619579fc9f, 0x323031392d30382d31345f303832355f6e65772e73716c, 0xda39a3ee5e6b4b0d3255bfef95601890afd80709, 1589632685, 20200516160334),
(0xfaa57687c5ca0d5fa040343d998ed70c6cbffb9e, 0x323031392d30392d31325f313334375f7465616d636f6e6669675f756e69636974792e73716c, 0x24a0fd5d1ee20a8e814284184d9abfc49169b7de, 1569327343, 20190924141853);

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2019_10_08_124640_add_jwt_fields', 2),
(3, '2019_11_05_141239_class_external_code', 2),
(4, '2020_03_12_150108_add_maillog', 2),
(11, '2020_05_16_153307_create_jobs_table', 3),
(12, '2020_05_18_143550_fix_zwibler_urls', 3),
(15, '2020_06_04_134521_report_soft_delete', 5),
(16, '2020_06_08_110755_add_popularity_annotation', 6),
(18, '2020_05_18_154916_fix_tinymce_img_src', 7),
(19, '2020_06_24_172217_fix_teacher_images', 8),
(20, '2022_03_23_154823_fix_trace_recover_ld_instead_of_validate_ld_issue_738', 9),
(21, '2022_04_21_161916_fix_trace_modify_LD_enter_report_issue_743', 9),
(22, '2022_05_10_221406_extract_zwibbler_images', 9),
(23, '2022_09_15_153052_last_version_id_in_labdoc', 10),
(24, '2022_11_18_093949_add_diff_and_collab_tdb_traces', 11),
(25, '2022_12_08_153752_add_labdoc_type_code', 12),
(26, '2022_12_14_111515_datasets_formula_comma_to_semicolon', 12),
(27, '2023_02_23_160509_trace_user_merge', 13),
(28, '2023_03_03_172249_last_version_id_in_labdoc', 13),
(29, '2023_03_17_161749_fix_inconsistency_mission_ld_new_initialized', 13),
(30, '2023_04_20_111515_datasets_change_columns_type', 13),
(31, '2023_05_13_161749_auto_fit_option', 14),
(32, '2023_05_13_171749_add_fitex_param_attribute_auto_fit', 14),
(33, '2023_06_09_160809_create_link-ressource-user', 15),
(34, '2023_07_06_121531_set_allow_import_id_mission_0_to_null', 15),
(35, '2023_08_11_101114_add_extplatform_contact_plugin_version_platform_version', 16),
(36, '2023_08_31_141254_do_not_delete_class_on_extplatform_delete', 16),
(37, '2023_10_20_143115_rubric_broadcast_option', 16),
(38, '2023_11_30_162557_retrieve_scoring_backend', 17),
(39, '2023_12_14_093347_add_scoring_traces', 18),
(40, '2023_12_23_145635_add_rubric_description', 19),
(41, '2024_01_16_093347_add_modif_report_trace', 20),
(42, '2024_02_05_142034_modify_institution_constraint', 20),
(43, '2024_02_23_085834_add_freeze_report_trace', 21),
(44, '2024_04_04_151805_scoring_retrieve_missing_id_criteria_group', 21);

-- --------------------------------------------------------

--
-- Structure de la table `mission`
--

DROP TABLE IF EXISTS `mission`;
CREATE TABLE `mission` (
  `id_mission` int(10) UNSIGNED NOT NULL,
  `code` varchar(16) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `assignment` longtext DEFAULT NULL,
  `auto_fit` tinyint(1) NOT NULL DEFAULT 0,
  `rubric_broadcast` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Allows/disallows students to view an unpublished rubric',
  `status` enum('public','private','archive') CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT 'private',
  `creation_date` date DEFAULT NULL,
  `modif_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `mission`
--

INSERT INTO `mission` (`id_mission`, `code`, `name`, `description`, `assignment`, `auto_fit`, `rubric_broadcast`, `status`, `creation_date`, `modif_date`) VALUES
(1, 'mission1', 'Mission première', '<p>Ceci est la première mission</p>', '<p>Réussir !</p>', 0, 0, 'private', '2019-01-01', '2024-06-18'),
(3, 'mission3', 'mission 3 vide', '<p>La troisième mission est vide</p>', '', 1, 0, 'private', '2019-01-03', '2019-01-03'),
(4, 'mission4', 'mission 4 vide', '<p>La quatrième mission est vide</p>', '', 0, 0, 'private', '2019-01-03', '2019-01-03'),
(5, 'mission5', 'mission physique', '<p>La mission physique est vide</p>', '', 1, 0, 'private', '2019-01-03', '2019-01-03'),
(6, 'mission6philo', 'mission philosophie', '<p>La mission philosophie est vide</p>', '', 0, 0, 'private', '2019-01-03', '2019-01-03'),
(7, 'mission7philo', 'mission philosophie2', '<p>La mission philosophie est vide</p>', '', 1, 0, 'private', '2019-01-03', '2019-01-03'),
(8, 'mission8', 'mission 8 vide', '<p>La huitième mission est vide</p>', '', 0, 0, 'private', '2019-01-03', '2019-01-03');

-- --------------------------------------------------------

--
-- Structure de la table `report`
--

DROP TABLE IF EXISTS `report`;
CREATE TABLE `report` (
  `id_report` int(10) UNSIGNED NOT NULL,
  `id_mission` int(10) UNSIGNED NOT NULL,
  `id_team_config` int(10) UNSIGNED DEFAULT NULL,
  `start_datetime` datetime DEFAULT NULL,
  `end_datetime` datetime DEFAULT NULL,
  `status` enum('new','on','wait','arc','test') NOT NULL DEFAULT 'new',
  `initialized` tinyint(1) NOT NULL DEFAULT 0,
  `team_name` varchar(32) NOT NULL DEFAULT '',
  `allow_msg_teacher` tinyint(1) NOT NULL DEFAULT 1,
  `allow_msg_team` tinyint(1) NOT NULL DEFAULT 1,
  `allow_msg_id_class` int(10) NOT NULL DEFAULT 0,
  `allow_msg_id_mission` int(10) NOT NULL DEFAULT 0,
  `allow_attach_ld` tinyint(1) NOT NULL DEFAULT 0,
  `allow_save_ld` tinyint(1) NOT NULL DEFAULT 0,
  `allow_import` tinyint(4) NOT NULL DEFAULT 0,
  `allow_import_id_mission` int(10) UNSIGNED DEFAULT 0,
  `creation_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `delete_time` timestamp NULL DEFAULT NULL COMMENT 'deletion time'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `report`
--

INSERT INTO `report` (`id_report`, `id_mission`, `id_team_config`, `start_datetime`, `end_datetime`, `status`, `initialized`, `team_name`, `allow_msg_teacher`, `allow_msg_team`, `allow_msg_id_class`, `allow_msg_id_mission`, `allow_attach_ld`, `allow_save_ld`, `allow_import`, `allow_import_id_mission`, `creation_time`, `update_time`, `delete_time`) VALUES
(1, 1, 1, NULL, NULL, 'new', 0, 'Equipe_01', 1, 1, 0, 0, 0, 0, 1, NULL, '2019-06-01 07:09:09', '2024-06-18 13:44:33', NULL),
(2, 1, 1, NULL, NULL, 'on', 1, 'Equipe_02', 1, 1, 0, 0, 0, 0, 1, NULL, '2019-06-01 07:09:09', '2024-06-18 13:44:33', NULL),
(3, 1, 1, NULL, NULL, 'on', 1, 'Equipe_a_deux', 1, 1, 0, 0, 0, 0, 1, NULL, '2019-06-01 07:09:09', '2024-06-18 13:44:33', NULL),
(4, 1, 1, NULL, NULL, 'wait', 1, 'fini', 1, 1, 0, 0, 0, 0, 1, NULL, '2019-06-01 07:09:09', '2024-06-18 13:44:33', NULL),
(5, 1, 1, NULL, NULL, 'on', 1, 'soft delete', 1, 1, 0, 0, 0, 0, 1, NULL, '2019-06-01 07:09:09', '2024-06-18 13:44:33', '2021-12-15 10:22:22'),
(6, 8, 2, NULL, NULL, 'new', 0, 'Equipe_01', 1, 1, 0, 0, 0, 0, 1, NULL, '2019-06-01 07:09:09', NULL, NULL),
(7, 3, 3, NULL, NULL, 'new', 0, 'Equipe_01', 1, 1, 0, 0, 0, 0, 0, NULL, '2022-05-11 11:26:43', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `report_part`
--

DROP TABLE IF EXISTS `report_part`;
CREATE TABLE `report_part` (
  `id_report_part` int(10) UNSIGNED NOT NULL,
  `id_mission` int(10) UNSIGNED NOT NULL,
  `position` int(10) UNSIGNED NOT NULL,
  `title` varchar(512) DEFAULT NULL,
  `text` tinyint(3) UNSIGNED NOT NULL DEFAULT 1,
  `drawing` tinyint(3) UNSIGNED NOT NULL DEFAULT 1,
  `dataset` tinyint(3) UNSIGNED NOT NULL DEFAULT 1,
  `procedure` tinyint(3) UNSIGNED NOT NULL DEFAULT 1,
  `code` tinyint(3) UNSIGNED NOT NULL DEFAULT 1,
  `assignment` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `report_part`
--

INSERT INTO `report_part` (`id_report_part`, `id_mission`, `position`, `title`, `text`, `drawing`, `dataset`, `procedure`, `code`, `assignment`) VALUES
(1, 1, 1, 'Global', 1, 1, 1, 1, 0, '<p>Tout doit s\'inscrire ci-dessous.</p>'),
(2, 1, 2, 'Partie 2', 1, 1, 1, 1, 1, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `ressource`
--

DROP TABLE IF EXISTS `ressource`;
CREATE TABLE `ressource` (
  `id_ressource` int(10) UNSIGNED NOT NULL,
  `id_mission` int(10) UNSIGNED DEFAULT NULL,
  `id_report` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(64) NOT NULL,
  `res_path` varchar(255) DEFAULT NULL,
  `res_type` enum('assignment','file','url') NOT NULL DEFAULT 'file',
  `position` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `ressource`
--

INSERT INTO `ressource` (`id_ressource`, `id_mission`, `id_report`, `name`, `res_path`, `res_type`, `position`) VALUES
(1, 1, NULL, 'consigne_longue', '1-LabNbook.pdf', 'assignment', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `rubric`
--

DROP TABLE IF EXISTS `rubric`;
CREATE TABLE `rubric` (
  `id_rubric` int(10) UNSIGNED NOT NULL,
  `id_activity` int(10) UNSIGNED DEFAULT NULL,
  `name` char(255) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `grading` double(8,2) DEFAULT NULL,
  `min_grade` double(8,2) DEFAULT NULL,
  `max_grade` double(8,2) DEFAULT NULL,
  `min_bonus` double(8,2) DEFAULT NULL,
  `max_bonus` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `rubric`
--

INSERT INTO `rubric` (`id_rubric`, `id_activity`, `name`, `description`, `grading`, `min_grade`, `max_grade`, `min_bonus`, `max_bonus`, `created_at`, `updated_at`) VALUES
(1, 1, 'Évaluation', '<p>Description de l\'évaluation</p>\n<p><span style=\"color: #e03e2d;\"><strong>Très important</strong></span></p>', 34.00, 0.00, 34.00, 0.00, 0.00, '2024-06-18 13:57:35', '2024-06-18 13:57:35');

-- --------------------------------------------------------

--
-- Structure de la table `teacher`
--

DROP TABLE IF EXISTS `teacher`;
CREATE TABLE `teacher` (
  `id_teacher` int(10) UNSIGNED NOT NULL,
  `teacher_domain` varchar(64) DEFAULT NULL,
  `teacher_level` varchar(64) DEFAULT NULL,
  `teacher_comment` text DEFAULT NULL,
  `current_interface` varchar(30) DEFAULT NULL,
  `role` enum('teacher','manager','admin') NOT NULL DEFAULT 'teacher'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `teacher`
--

INSERT INTO `teacher` (`id_teacher`, `teacher_domain`, `teacher_level`, `teacher_comment`, `current_interface`, `role`) VALUES
(1, 'Admin', 'univ', '', 'teacher/students', 'admin'),
(2, 'Biologie', 'univ', '', 'teacher/missions', 'teacher'),
(11, 'Physique', 'univ', '', NULL, 'manager'),
(12, 'Philosophie', 'univ', '', NULL, 'manager'),
(13, 'Économie', 'univ', '', NULL, 'teacher'),
(14, 'Logistique', 'univ', '', NULL, 'teacher'),
(15, 'Économie', 'univ', '', NULL, 'teacher'),
(16, 'Logistique', 'univ', '', NULL, 'teacher'),
(17, 'Économie', 'univ', '', NULL, 'teacher');

-- --------------------------------------------------------

--
-- Structure de la table `teacher_report_status`
--

DROP TABLE IF EXISTS `teacher_report_status`;
CREATE TABLE `teacher_report_status` (
  `id_report` int(10) UNSIGNED NOT NULL,
  `id_teacher` int(10) UNSIGNED NOT NULL,
  `last_synchro` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `teacher_report_status`
--

INSERT INTO `teacher_report_status` (`id_report`, `id_teacher`, `last_synchro`) VALUES
(2, 2, 1718792686),
(3, 2, 1718719109);

-- --------------------------------------------------------

--
-- Structure de la table `teacher_team`
--

DROP TABLE IF EXISTS `teacher_team`;
CREATE TABLE `teacher_team` (
  `id_teacher_team` int(10) UNSIGNED NOT NULL,
  `id_inst` int(11) DEFAULT NULL,
  `name` varchar(64) NOT NULL,
  `status` enum('normal','archive') CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT 'normal',
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `creation_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `teacher_team`
--

INSERT INTO `teacher_team` (`id_teacher_team`, `id_inst`, `name`, `status`, `update_time`, `creation_time`) VALUES
(1, 2, 'Equipe pédago chimie UGA', 'normal', '2021-04-21 08:10:48', '2021-04-21 08:10:48'),
(2, 2, 'Equipe pédago physique UGA', 'normal', '2021-04-21 08:10:48', '2021-04-21 08:10:48'),
(3, 1, 'Equipe pédago maths Todai', 'archive', '2021-04-21 08:10:48', '2021-04-21 08:10:48'),
(4, 2, 'Equipe pédago philosophie UGA', 'normal', '2021-04-21 08:10:48', '2021-04-21 08:10:48'),
(5, 1, 'Equipe pédago archéologie Todai', 'normal', '2021-04-21 08:10:48', '2021-04-21 08:10:48'),
(6, 1, 'Equipe pédago économie Todai', 'normal', '2021-04-21 08:10:48', '2021-04-21 08:10:48'),
(7, 1, 'Equipe pédago maths UGA', 'archive', '2021-04-21 08:10:48', '2021-04-21 08:10:48');

-- --------------------------------------------------------

--
-- Structure de la table `team_config`
--

DROP TABLE IF EXISTS `team_config`;
CREATE TABLE `team_config` (
  `id_team_config` int(10) UNSIGNED NOT NULL,
  `id_mission` int(10) UNSIGNED NOT NULL,
  `id_class` int(10) UNSIGNED NOT NULL,
  `method` tinyint(4) UNSIGNED NOT NULL DEFAULT 0 COMMENT '0=undef 1=student-choice 2=teacher-choice 3=random',
  `size_opt` smallint(5) UNSIGNED DEFAULT NULL,
  `size_max` smallint(5) UNSIGNED DEFAULT NULL,
  `size_min` smallint(5) UNSIGNED DEFAULT NULL,
  `teams_max` smallint(5) UNSIGNED DEFAULT NULL,
  `start_datetime` datetime DEFAULT NULL,
  `end_datetime` datetime DEFAULT NULL,
  `allow_msg_teacher` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'authorize messages to the class teachers (boolean)',
  `allow_msg_team` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'authorize messages to the other team members (boolean)',
  `allow_msg_id_class` int(10) UNSIGNED DEFAULT NULL COMMENT 'authorize messages to class: NULL=none other=id_class',
  `allow_msg_id_mission` int(10) UNSIGNED DEFAULT NULL COMMENT 'authorize messages to mission students: NULL=none other=id_mission',
  `allow_attach_ld` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'authorize attaching LD to outgoing messages (boolean)',
  `allow_save_ld` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'authorize saving into the report the LD received in messages (boolean)',
  `allow_import` tinyint(1) NOT NULL COMMENT 'Allow the import of Labdocs. If true, may be bound to a mission with allow_import_id_mission',
  `allow_import_id_mission` int(10) UNSIGNED DEFAULT NULL COMMENT 'Limit LD import to a single mission',
  `name_prefix` varchar(30) NOT NULL,
  `email_on_create` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'send an email to members when a team is created (boolean)',
  `creation_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `team_config`
--

INSERT INTO `team_config` (`id_team_config`, `id_mission`, `id_class`, `method`, `size_opt`, `size_max`, `size_min`, `teams_max`, `start_datetime`, `end_datetime`, `allow_msg_teacher`, `allow_msg_team`, `allow_msg_id_class`, `allow_msg_id_mission`, `allow_attach_ld`, `allow_save_ld`, `allow_import`, `allow_import_id_mission`, `name_prefix`, `email_on_create`, `creation_time`, `update_time`) VALUES
(1, 1, 1, 3, 1, 1, 1, 9, NULL, NULL, 1, 1, 1, NULL, 0, 1, 1, 3, 'Equipe_', 0, '2019-06-13 07:29:22', NULL),
(2, 8, 1, 3, 1, 1, 1, 9, NULL, NULL, 1, 1, 1, NULL, 0, 1, 1, 3, 'Equipe_', 0, '2019-06-13 07:29:22', NULL),
(3, 3, 4, 2, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, 0, 0, 0, NULL, 'Equipe_', 0, '2022-05-11 11:26:43', '2022-05-11 11:26:43');

-- --------------------------------------------------------

--
-- Structure de la table `trace`
--

DROP TABLE IF EXISTS `trace`;
CREATE TABLE `trace` (
  `id_trace` bigint(20) UNSIGNED NOT NULL,
  `id_action` smallint(6) NOT NULL,
  `action_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_mission` int(10) UNSIGNED DEFAULT NULL,
  `id_report` int(10) UNSIGNED DEFAULT NULL,
  `id_labdoc` int(10) UNSIGNED DEFAULT NULL,
  `attributes` varbinary(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `trace`
--

INSERT INTO `trace` (`id_trace`, `id_action`, `action_time`, `id_user`, `id_mission`, `id_report`, `id_labdoc`, `attributes`) VALUES
(1, 1, '2024-06-18 13:44:35', 2, NULL, NULL, NULL, NULL),
(2, 2, '2024-06-18 13:55:24', 2, NULL, NULL, NULL, NULL),
(3, 1, '2024-06-18 13:55:31', 4, NULL, NULL, NULL, NULL),
(4, 3, '2024-06-18 13:55:35', 4, 1, 2, NULL, 0x7b22636f6465223a226d697373696f6e31227d),
(5, 4, '2024-06-18 13:55:54', 4, 1, 2, NULL, NULL),
(6, 2, '2024-06-18 13:55:56', 4, NULL, NULL, NULL, NULL),
(7, 1, '2024-06-18 13:56:03', 2, NULL, NULL, NULL, NULL),
(8, 58, '2024-06-18 13:57:35', 2, 1, NULL, NULL, NULL),
(9, 50, '2024-06-18 13:57:45', 2, 1, 2, NULL, NULL),
(10, 31, '2024-06-18 13:57:47', 2, 1, 2, NULL, 0x7b22636f6465223a226d697373696f6e31227d),
(11, 59, '2024-06-18 13:57:58', 2, 1, 2, NULL, 0x7b2273636f7265223a32332e36387d),
(12, 60, '2024-06-18 13:57:58', 2, 1, 2, NULL, NULL),
(13, 50, '2024-06-18 13:58:07', 2, 1, 3, NULL, NULL),
(14, 31, '2024-06-18 13:58:08', 2, 1, 3, NULL, 0x7b22636f6465223a226d697373696f6e31227d),
(15, 59, '2024-06-18 13:58:13', 2, 1, 3, NULL, 0x7b2273636f7265223a372e38397d),
(16, 2, '2024-06-18 13:58:34', 2, NULL, NULL, NULL, NULL),
(17, 1, '2024-06-18 13:58:51', 4, NULL, NULL, NULL, NULL),
(18, 3, '2024-06-18 13:58:55', 4, 1, 2, NULL, 0x7b22636f6465223a226d697373696f6e31227d),
(19, 62, '2024-06-18 13:58:57', 4, 1, 2, NULL, NULL),
(20, 4, '2024-06-18 13:59:10', 4, 1, 2, NULL, NULL),
(21, 2, '2024-06-18 13:59:10', 4, NULL, NULL, NULL, NULL),
(22, 1, '2024-06-18 13:59:15', 2, NULL, NULL, NULL, NULL),
(23, 38, '2024-06-18 13:59:29', 2, 1, NULL, 6, 0x7b2274797065223a2274657874227d),
(24, 33, '2024-06-18 13:59:29', 2, 1, NULL, NULL, NULL),
(25, 33, '2024-06-18 13:59:50', 2, 1, NULL, NULL, NULL),
(26, 33, '2024-06-18 13:59:54', 2, 1, NULL, NULL, NULL),
(27, 33, '2024-06-18 13:59:57', 2, 1, NULL, NULL, NULL),
(28, 38, '2024-06-18 13:59:58', 2, 1, NULL, 14, 0x7b2274797065223a2264726177696e67227d),
(29, 33, '2024-06-18 13:59:58', 2, 1, NULL, NULL, NULL),
(30, 33, '2024-06-18 14:00:05', 2, 1, NULL, NULL, NULL),
(31, 39, '2024-06-18 14:00:11', 2, 1, NULL, NULL, NULL),
(32, 33, '2024-06-18 14:00:11', 2, 1, NULL, NULL, NULL),
(33, 38, '2024-06-18 14:00:13', 2, 1, NULL, 18, 0x7b2274797065223a2264617461736574227d),
(34, 33, '2024-06-18 14:00:13', 2, 1, NULL, NULL, NULL),
(35, 33, '2024-06-18 14:00:34', 2, 1, NULL, NULL, NULL),
(36, 38, '2024-06-18 14:00:36', 2, 1, NULL, 22, 0x7b2274797065223a2270726f636564757265227d),
(37, 33, '2024-06-18 14:00:36', 2, 1, NULL, NULL, NULL),
(38, 33, '2024-06-18 14:00:50', 2, 1, NULL, NULL, NULL),
(39, 38, '2024-06-18 14:00:52', 2, 1, NULL, 26, 0x7b2274797065223a22636f6465227d),
(40, 33, '2024-06-18 14:00:52', 2, 1, NULL, NULL, NULL),
(41, 33, '2024-06-18 14:01:32', 2, 1, NULL, NULL, NULL),
(42, 3, '2024-06-18 14:01:48', 7, 1, 3, NULL, 0x7b22636f6465223a226d697373696f6e31227d),
(43, 8, '2024-06-18 14:02:14', 7, 1, 3, 2, 0x7b2274797065223a2274657874227d),
(44, 9, '2024-06-18 14:02:19', 7, 1, 3, 2, NULL),
(45, 9, '2024-06-18 14:02:29', 7, 1, 3, 2, NULL),
(46, 9, '2024-06-18 14:02:49', 7, 1, 3, 2, NULL),
(47, 9, '2024-06-18 14:02:54', 7, 1, 3, 2, NULL),
(48, 10, '2024-06-18 14:02:54', 7, 1, 3, 2, NULL),
(49, 8, '2024-06-18 14:03:05', 7, 1, 3, 20, 0x7b2274797065223a2264617461736574227d),
(50, 9, '2024-06-18 14:03:11', 7, 1, 3, 20, NULL),
(51, 10, '2024-06-18 14:03:11', 7, 1, 3, 20, NULL),
(52, 2, '2024-06-19 10:23:39', 1, NULL, NULL, NULL, NULL),
(53, 1, '2024-06-19 10:23:46', 2, NULL, NULL, NULL, NULL),
(54, 50, '2024-06-19 10:23:51', 2, 1, 1, NULL, NULL),
(55, 55, '2024-06-19 10:23:53', 2, 1, 1, NULL, NULL),
(56, 50, '2024-06-19 10:24:12', 2, 1, 2, NULL, NULL),
(57, 31, '2024-06-19 10:24:14', 2, 1, 2, NULL, 0x7b22636f6465223a226d697373696f6e31227d),
(58, 50, '2024-06-19 10:24:17', 2, 1, 1, NULL, NULL),
(59, 2, '2024-06-19 10:24:50', 2, NULL, NULL, NULL, NULL),
(60, 1, '2024-06-19 10:24:57', 1, NULL, NULL, NULL, NULL),
(61, 40, '2024-06-19 10:31:48', 1, NULL, NULL, NULL, 0x7b2269645f636c617373223a31307d);

-- --------------------------------------------------------

--
-- Structure de la table `trace_action`
--

DROP TABLE IF EXISTS `trace_action`;
CREATE TABLE `trace_action` (
  `id_action` smallint(6) NOT NULL,
  `action` varchar(255) DEFAULT NULL,
  `action_type` enum('administration','production','information','communication','t_mission','t_student','t_report','m_teacher') CHARACTER SET ascii COLLATE ascii_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `trace_action`
--

INSERT INTO `trace_action` (`id_action`, `action`, `action_type`) VALUES
(1, 'connect_lnb', 'administration'),
(2, 'disconnect_lnb', 'administration'),
(3, 'enter_report', 'administration'),
(4, 'leave_report', 'administration'),
(5, 'submit_report', 'administration'),
(6, 'add_ld', 'production'),
(7, 'duplicate_ld', 'production'),
(8, 'edit_ld', 'production'),
(9, 'modify_ld', 'production'),
(10, 'validate_ld', 'production'),
(11, 'toggle_ld_status', 'production'),
(12, 'update_ld_name', 'production'),
(13, 'delete_ld', 'production'),
(14, 'open_annotation', 'information'),
(15, 'open_rp_assignment', 'information'),
(16, 'open_assignment', 'information'),
(17, 'open_detailed_assignment', 'information'),
(18, 'open_resource', 'information'),
(19, 'add_resource', 'communication'),
(20, 'open_comment', 'communication'),
(21, 'add_comment', 'communication'),
(22, 'open_conversation', 'communication'),
(23, 'add_message', 'communication'),
(24, 'add_conversation', 'communication'),
(25, 'import_ld', 'production'),
(26, 'save_attached_ld', 'production'),
(27, 'recover_ld', 'production'),
(28, 'open_ld_versioning', 'production'),
(29, 'hard_delete_ld', 'production'),
(31, 'follow_report', 't_report'),
(32, 'test_mission', 't_mission'),
(33, 'modify_mission', 't_mission'),
(34, 'add_mission', 't_mission'),
(35, 'duplicate_mission', 't_mission'),
(36, 'delete_mission', 't_mission'),
(37, 'add_resource', 't_mission'),
(38, 'add_ld', 't_mission'),
(39, 'add_rp', 't_mission'),
(40, 'add_class', 't_student'),
(41, 'delete_class', 't_student'),
(42, 'add_student', 't_student'),
(43, 'delete_student', 't_student'),
(44, 'add_report', 't_student'),
(45, 'edit_report', 't_report'),
(46, 'delete_report', 't_report'),
(47, 'annotate_report', 't_report'),
(48, 'add_teaming', 't_student'),
(49, 'create_user', 't_student'),
(50, 'select_report', 't_report'),
(51, 'add_teacher_status', 'm_teacher'),
(52, 'remove_teacher_status', 'm_teacher'),
(53, 'edit_teaming', 't_student'),
(54, 'archive_report', 't_report'),
(55, 'open_collaborative_dashboard', 't_report'),
(56, 'display_diffs', 't_report'),
(57, 'user_merged', 'administration'),
(58, 'scoring_save_grid', 't_mission'),
(59, 'scoring_save_eval', 't_report'),
(60, 'scoring_publish_eval', 't_report'),
(61, 'scoring_edit_eval', 't_report'),
(62, 'scoring_view_eval', 'information'),
(63, 'modify_rp', 't_mission'),
(64, 'freeze_report', 't_report');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id_user` int(10) UNSIGNED NOT NULL,
  `user_name` varchar(64) NOT NULL,
  `first_name` varchar(32) NOT NULL,
  `login` varchar(20) NOT NULL,
  `pwd` varbinary(64) DEFAULT NULL,
  `email` varchar(64) NOT NULL DEFAULT '',
  `lang` varchar(3) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `id_inst` int(11) NOT NULL,
  `inst_number` varchar(64) DEFAULT NULL,
  `current_report` int(10) UNSIGNED DEFAULT NULL,
  `last_synchro` int(11) DEFAULT NULL,
  `reset_pwd` varchar(32) DEFAULT NULL,
  `creation_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_time` timestamp NULL DEFAULT NULL,
  `cgu_accept_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id_user`, `user_name`, `first_name`, `login`, `pwd`, `email`, `lang`, `id_inst`, `inst_number`, `current_report`, `last_synchro`, `reset_pwd`, `creation_time`, `update_time`, `cgu_accept_time`) VALUES
(1, 'Admin', '', 'admin', 0x243279243130244a337779493862505564675a773553354f344343677570674e54654244304f6f74525237396178583646624148717471692f6c612e, 'someone@example.org', 'fr', 1, NULL, NULL, 1718793394, 'ShqCuAWqm3h5c84YkBIv0zddHDqXcFo5', '2017-12-31 23:00:00', NULL, '2017-12-31 23:00:00'),
(2, 'Teacher1', 'Yukiko', 'teacher1', 0x243279243130244d6962474273505a4d35423468427644695a32796d2e69696b506f584d6279574b4e37412e6b6d6e475079485445416b6b54386947, 'teacher1@example.org', 'fr', 2, NULL, NULL, 1718792686, 'YWT4tA4ehn2pd05goEUJY6NeOG59wv8D', '2017-12-31 23:00:00', NULL, '2017-12-31 23:00:00'),
(3, 'CAS user', 'Masako', 'user-CAS', NULL, 'user-cas@example.org', 'fr', 2, '12345679', NULL, NULL, NULL, '2017-12-31 23:00:00', NULL, '2017-12-31 23:00:00'),
(4, 'Student1', 'Kōtaro', 'student1', 0x243279243130246f37374e473134346569445247504b4766795a744675474b5a4d684168716b52363935774e584f417967584d59656c675630634b6d, 'student1@example.org', 'fr', 2, NULL, 2, 1718719146, 'vkGAmTEJrBn1fdbM4aaAO5HraGTEQ8lG', '2019-01-01 11:00:00', NULL, '2017-12-31 23:00:00'),
(5, 'Student2', 'Shinji', 'student2', 0x66346632363365343339636634303932356536613431323338376139343732613637373363323538303231326134666235306432323464336138313764653137, 'student2@example.org', 'fr', 2, NULL, NULL, NULL, NULL, '2019-01-01 11:00:00', NULL, '2017-12-31 23:00:00'),
(7, 'Student4', 'Évariste', 'student4', 0x66346632363365343339636634303932356536613431323338376139343732613637373363323538303231326134666235306432323464336138313764653137, 'student4@example.org', 'fr', 2, NULL, 3, 1718719736, NULL, '2019-01-01 11:00:00', NULL, '2017-12-31 23:00:00'),
(8, 'Student5', 'N\'ji', 'student5', 0x66346632363365343339636634303932356536613431323338376139343732613637373363323538303231326134666235306432323464336138313764653137, 'student5@example.org', 'fr', 2, NULL, NULL, NULL, NULL, '2019-01-01 11:00:00', NULL, '2017-12-31 23:00:00'),
(9, 'Student6', 'Sans-classe', 'student6', 0x66346632363365343339636634303932356536613431323338376139343732613637373363323538303231326134666235306432323464336138313764653137, 'student6@example.org', 'fr', 2, NULL, NULL, NULL, NULL, '2019-01-01 11:00:00', NULL, '2017-12-31 23:00:00'),
(10, 'Uuga', 'Uuga', 'uuga', 0x66346632363365343339636634303932356536613431323338376139343732613637373363323538303231326134666235306432323464336138313764653137, 'student-uga@example.org', 'fr', 2, NULL, NULL, NULL, NULL, '2019-01-01 11:00:00', NULL, '2017-12-31 23:00:00'),
(11, 'Teacher2', 'Alain', 'teacher2', 0x66346632363365343339636634303932356536613431323338376139343732613637373363323538303231326134666235306432323464336138313764653137, 'teacher2@example.org', 'fr', 2, NULL, NULL, NULL, NULL, '2017-12-31 23:00:00', NULL, '2017-12-31 23:00:00'),
(12, 'Teacher3', 'Pierre', 'teacher3', 0x66346632363365343339636634303932356536613431323338376139343732613637373363323538303231326134666235306432323464336138313764653137, 'teacher3@example.org', 'fr', 2, NULL, NULL, NULL, NULL, '2017-12-31 23:00:00', NULL, '2017-12-31 23:00:00'),
(13, 'Teacher4', 'Paul', 'teacher4', 0x66346632363365343339636634303932356536613431323338376139343732613637373363323538303231326134666235306432323464336138313764653137, 'teacher4@example.org', 'fr', 2, NULL, NULL, NULL, NULL, '2017-12-31 23:00:00', NULL, '2017-12-31 23:00:00'),
(14, 'Teacher5', 'Jacques', 'teacher5', 0x66346632363365343339636634303932356536613431323338376139343732613637373363323538303231326134666235306432323464336138313764653137, 'teacher5@example.org', 'fr', 1, NULL, NULL, NULL, NULL, '2017-12-31 23:00:00', NULL, '2017-12-31 23:00:00'),
(15, 'Teacher6', 'Louis', 'teacher6', 0x66346632363365343339636634303932356536613431323338376139343732613637373363323538303231326134666235306432323464336138313764653137, 'teacher6@example.org', 'fr', 1, NULL, NULL, NULL, NULL, '2017-12-31 23:00:00', NULL, '2017-12-31 23:00:00'),
(16, 'Teacher7', 'Michel', 'teacher7', 0x66346632363365343339636634303932356536613431323338376139343732613637373363323538303231326134666235306432323464336138313764653137, 'teacher7@example.org', 'fr', 1, NULL, NULL, NULL, NULL, '2017-12-31 23:00:00', NULL, '2017-12-31 23:00:00'),
(17, 'Teacher8', 'Henri', 'teacher8', 0x66346632363365343339636634303932356536613431323338376139343732613637373363323538303231326134666235306432323464336138313764653137, 'teacher8@example.org', 'fr', 2, NULL, NULL, NULL, NULL, '2017-12-31 23:00:00', NULL, '2017-12-31 23:00:00'),
(61, 'Student3', 'Masato', 'student3', 0x66346632363365343339636634303932356536613431323338376139343732613637373363323538303231326134666235306432323464336138313764653137, 'student3@example.org', 'fr', 2, NULL, NULL, NULL, NULL, '2019-01-01 11:00:00', NULL, '2017-12-31 23:00:00');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `annotation`
--
ALTER TABLE `annotation`
  ADD PRIMARY KEY (`id_annotation`),
  ADD KEY `id_teacher` (`id_teacher`),
  ADD KEY `id_report` (`id_report`),
  ADD KEY `id_report_part` (`id_report_part`),
  ADD KEY `id_labdoc` (`id_labdoc`);

--
-- Index pour la table `assessed_student`
--
ALTER TABLE `assessed_student`
  ADD PRIMARY KEY (`id_assessed_student`),
  ADD UNIQUE KEY `assessed_student_id_student_id_assessment_unique` (`id_student`,`id_assessment`),
  ADD KEY `assessed_student_id_assessment_foreign` (`id_assessment`);

--
-- Index pour la table `assessment`
--
ALTER TABLE `assessment`
  ADD PRIMARY KEY (`id_assessment`),
  ADD KEY `assessment_ibfk1` (`id_production`);

--
-- Index pour la table `class`
--
ALTER TABLE `class`
  ADD PRIMARY KEY (`id_class`),
  ADD UNIQUE KEY `class_code` (`class_code`),
  ADD KEY `id_inst` (`id_inst`),
  ADD KEY `class_id_extplatform_foreign` (`id_extplatform`);

--
-- Index pour la table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id_comment`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_labdoc` (`id_labdoc`);
  
--
-- Index pour la table `conversation`
--
ALTER TABLE `conversation`
  ADD PRIMARY KEY (`id_conversation`),
  ADD KEY `Conversation_FKIndex1` (`id_report`);

--
-- Index pour la table `criteria_group`
--
ALTER TABLE `criteria_group`
  ADD PRIMARY KEY (`id_criteria_group`),
  ADD KEY `criteria_group_id_origin_foreign` (`id_origin`),
  ADD KEY `criteria_group_ibfk2` (`id_rubric`),
  ADD KEY `criteria_group_ibfk3` (`id_assessment`);

--
-- Index pour la table `criterion`
--
ALTER TABLE `criterion`
  ADD PRIMARY KEY (`id_criterion`),
  ADD KEY `criterion_id_rubric_foreign` (`id_rubric`),
  ADD KEY `criterion_id_assessment_foreign` (`id_assessment`),
  ADD KEY `criterion_id_criteria_group_foreign` (`id_criteria_group`),
  ADD KEY `criterion_id_origin_foreign` (`id_origin`);

--
-- Index pour la table `descrob`
--
ALTER TABLE `descrob`
  ADD PRIMARY KEY (`id_descrob`),
  ADD KEY `descrob_id_criterion_foreign` (`id_criterion`),
  ADD KEY `descrob_id_origin_foreign` (`id_origin`);

--
-- Index pour la table `extplatform`
--
ALTER TABLE `extplatform`
  ADD PRIMARY KEY (`id_extplatform`),
  ADD UNIQUE KEY `extplatformname` (`name`),
  ADD KEY `extplatform_ibfk_1` (`id_inst`);

--
-- Index pour la table `institution`
--
ALTER TABLE `institution`
  ADD PRIMARY KEY (`id_inst`),
  ADD UNIQUE KEY `institutionname` (`name`),
  ADD KEY `institution_ibfk_1` (`id_class_teacher`);

--
-- Index pour la table `labdoc`
--
ALTER TABLE `labdoc`
  ADD PRIMARY KEY (`id_labdoc`),
  ADD KEY `id_report_part` (`id_report_part`),
  ADD KEY `id_mission_conf` (`id_report`),
  ADD KEY `id_ld_origin` (`id_ld_origin`),
  ADD KEY `last_editor` (`last_editor`);

--
-- Index pour la table `labdocfile`
--
ALTER TABLE `labdocfile`
  ADD PRIMARY KEY (`filehash`,`id_labdoc`),
  ADD KEY `id_labdoc` (`id_labdoc`),
  ADD KEY `id_mission` (`id_mission`);

--
-- Index pour la table `labdoc_status`
--
ALTER TABLE `labdoc_status`
  ADD PRIMARY KEY (`id_labdoc`,`id_user`),
  ADD KEY `lds_id_user` (`id_user`);

--
-- Index pour la table `labdoc_stdalone`
--
ALTER TABLE `labdoc_stdalone`
  ADD PRIMARY KEY (`code`);

--
-- Index pour la table `link_annotation_learner`
--
ALTER TABLE `link_annotation_learner`
  ADD PRIMARY KEY (`id_annotation`,`id_user`),
  ADD KEY `id_user` (`id_user`);

--
-- Index pour la table `link_class_learner`
--
ALTER TABLE `link_class_learner`
  ADD PRIMARY KEY (`id_class`,`id_user`),
  ADD KEY `lcl_id_user` (`id_user`);

--
-- Index pour la table `link_class_teacher`
--
ALTER TABLE `link_class_teacher`
  ADD PRIMARY KEY (`id_teacher`,`id_class`),
  ADD KEY `lct_id_class` (`id_class`);

--
-- Index pour la table `link_class_tteam`
--
ALTER TABLE `link_class_tteam`
  ADD PRIMARY KEY (`id_class`,`id_teacher_team`),
  ADD KEY `lctt_id_teacher_team` (`id_teacher_team`);

--
-- Index pour la table `link_conversation_user`
--
ALTER TABLE `link_conversation_user`
  ADD PRIMARY KEY (`id_conversation`,`id_user`),
  ADD KEY `lcu_id_user` (`id_user`);

--
-- Index pour la table `link_extplatform_user`
--
ALTER TABLE `link_extplatform_user`
  ADD PRIMARY KEY (`id_extplatform`,`id_user_ext`),
  ADD KEY `link_ext_user_inst_id_user_foreign` (`id_user`);

--
-- Index pour la table `link_inst_teacher`
--
ALTER TABLE `link_inst_teacher`
  ADD PRIMARY KEY (`id_teacher`,`id_inst`),
  ADD KEY `id_inst` (`id_inst`);

--
-- Index pour la table `link_mission_teacher`
--
ALTER TABLE `link_mission_teacher`
  ADD PRIMARY KEY (`id_mission`,`id_teacher`),
  ADD KEY `lmt_id_teacher` (`id_teacher`);

--
-- Index pour la table `link_mission_tteam`
--
ALTER TABLE `link_mission_tteam`
  ADD PRIMARY KEY (`id_mission`,`id_teacher_team`),
  ADD KEY `id_teacher_team` (`id_teacher_team`);

--
-- Index pour la table `link_report_learner`
--
ALTER TABLE `link_report_learner`
  ADD PRIMARY KEY (`id_report`,`id_user`),
  ADD KEY `Link_Report_Learner_FKIndex1` (`id_report`),
  ADD KEY `Link_Report_Learner_FKIndex2` (`id_user`);

--
-- Index pour la table `link_ressource_user`
--
ALTER TABLE `link_ressource_user`
  ADD KEY `id_ressource` (`id_ressource`),
  ADD KEY `id_user` (`id_user`);

--
-- Index pour la table `link_teacher_tteam`
--
ALTER TABLE `link_teacher_tteam`
  ADD PRIMARY KEY (`id_teacher`,`id_teacher_team`,`status`),
  ADD KEY `lttt_id_teacher_team` (`id_teacher_team`);

--
-- Index pour la table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id_message`),
  ADD KEY `id_user` (`id_user_sender`),
  ADD KEY `id_conv` (`id_conversation`),
  ADD KEY `id_labdoc` (`id_labdoc`);

--
-- Index pour la table `message_var`
--
ALTER TABLE `message_var`
  ADD PRIMARY KEY (`id_user`);

--
-- Index pour la table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `mission`
--
ALTER TABLE `mission`
  ADD PRIMARY KEY (`id_mission`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Index pour la table `report`
--
ALTER TABLE `report`
  ADD PRIMARY KEY (`id_report`),
  ADD KEY `Mission_Conf_FKIndex2` (`id_mission`),
  ADD KEY `id_team_config` (`id_team_config`);

--
-- Index pour la table `report_part`
--
ALTER TABLE `report_part`
  ADD PRIMARY KEY (`id_report_part`),
  ADD KEY `my_unique` (`id_mission`,`position`);

--
-- Index pour la table `ressource`
--
ALTER TABLE `ressource`
  ADD PRIMARY KEY (`id_ressource`),
  ADD KEY `id_mission` (`id_mission`),
  ADD KEY `id_mission_conf` (`id_report`);

--
-- Index pour la table `rubric`
--
ALTER TABLE `rubric`
  ADD PRIMARY KEY (`id_rubric`),
  ADD KEY `rubric_ibfk1` (`id_activity`);

--
-- Index pour la table `teacher`
--
ALTER TABLE `teacher`
  ADD PRIMARY KEY (`id_teacher`);

--
-- Index pour la table `teacher_report_status`
--
ALTER TABLE `teacher_report_status`
  ADD PRIMARY KEY (`id_report`,`id_teacher`),
  ADD KEY `Teacher_Report_Status_FKIndex1` (`id_report`),
  ADD KEY `Teacher_Report_Status_FKIndex2` (`id_teacher`);

--
-- Index pour la table `teacher_team`
--
ALTER TABLE `teacher_team`
  ADD PRIMARY KEY (`id_teacher_team`),
  ADD KEY `id_inst` (`id_inst`);

--
-- Index pour la table `team_config`
--
ALTER TABLE `team_config`
  ADD PRIMARY KEY (`id_team_config`),
  ADD UNIQUE KEY `unique_class_mission` (`id_class`,`id_mission`),
  ADD KEY `id_mission` (`id_mission`),
  ADD KEY `allow_import_id_mission` (`allow_import_id_mission`),
  ADD KEY `allow_msg_id_class` (`allow_msg_id_class`),
  ADD KEY `allow_msg_id_mission` (`allow_msg_id_mission`);

--
-- Index pour la table `trace`
--
ALTER TABLE `trace`
  ADD PRIMARY KEY (`id_trace`),
  ADD KEY `key_id_report` (`id_report`),
  ADD KEY `key_id_action` (`id_action`);

--
-- Index pour la table `trace_action`
--
ALTER TABLE `trace_action`
  ADD PRIMARY KEY (`id_action`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `login` (`login`,`pwd`),
  ADD UNIQUE KEY `reset_pwd` (`reset_pwd`),
  ADD UNIQUE KEY `instid_instnumber` (`id_inst`,`inst_number`),
  ADD KEY `current_report` (`current_report`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `annotation`
--
ALTER TABLE `annotation`
  MODIFY `id_annotation` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1000002;

--
-- AUTO_INCREMENT pour la table `assessed_student`
--
ALTER TABLE `assessed_student`
  MODIFY `id_assessed_student` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=151;

--
-- AUTO_INCREMENT pour la table `assessment`
--
ALTER TABLE `assessment`
  MODIFY `id_assessment` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `class`
--
ALTER TABLE `class`
  MODIFY `id_class` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `comment`
--
ALTER TABLE `comment`
  MODIFY `id_comment` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `conversation`
--
ALTER TABLE `conversation`
  MODIFY `id_conversation` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `criteria_group`
--
ALTER TABLE `criteria_group`
  MODIFY `id_criteria_group` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `criterion`
--
ALTER TABLE `criterion`
  MODIFY `id_criterion` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT pour la table `descrob`
--
ALTER TABLE `descrob`
  MODIFY `id_descrob` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1201;

--
-- AUTO_INCREMENT pour la table `extplatform`
--
ALTER TABLE `extplatform`
  MODIFY `id_extplatform` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `institution`
--
ALTER TABLE `institution`
  MODIFY `id_inst` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `labdoc`
--
ALTER TABLE `labdoc`
  MODIFY `id_labdoc` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT pour la table `message`
--
ALTER TABLE `message`
  MODIFY `id_message` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT pour la table `mission`
--
ALTER TABLE `mission`
  MODIFY `id_mission` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `report`
--
ALTER TABLE `report`
  MODIFY `id_report` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `report_part`
--
ALTER TABLE `report_part`
  MODIFY `id_report_part` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `ressource`
--
ALTER TABLE `ressource`
  MODIFY `id_ressource` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `rubric`
--
ALTER TABLE `rubric`
  MODIFY `id_rubric` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `teacher_team`
--
ALTER TABLE `teacher_team`
  MODIFY `id_teacher_team` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `team_config`
--
ALTER TABLE `team_config`
  MODIFY `id_team_config` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `trace`
--
ALTER TABLE `trace`
  MODIFY `id_trace` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `annotation`
--
ALTER TABLE `annotation`
  ADD CONSTRAINT `annotation_ibfk_1` FOREIGN KEY (`id_teacher`) REFERENCES `teacher` (`id_teacher`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `annotation_ibfk_2` FOREIGN KEY (`id_report`) REFERENCES `report` (`id_report`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `annotation_ibfk_3` FOREIGN KEY (`id_report_part`) REFERENCES `report_part` (`id_report_part`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `annotation_ibfk_4` FOREIGN KEY (`id_labdoc`) REFERENCES `labdoc` (`id_labdoc`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `assessed_student`
--
ALTER TABLE `assessed_student`
  ADD CONSTRAINT `assessed_ibfk1` FOREIGN KEY (`id_assessment`) REFERENCES `assessment` (`id_assessment`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `assessed_ibfk2` FOREIGN KEY (`id_student`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `assessment`
--
ALTER TABLE `assessment`
  ADD CONSTRAINT `assessment_ibfk1` FOREIGN KEY (`id_production`) REFERENCES `report` (`id_report`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `class`
--
ALTER TABLE `class`
  ADD CONSTRAINT `class_ibfk_1` FOREIGN KEY (`id_inst`) REFERENCES `institution` (`id_inst`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `class_id_extplatform_foreign` FOREIGN KEY (`id_extplatform`) REFERENCES `extplatform` (`id_extplatform`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `com_id_ld` FOREIGN KEY (`id_labdoc`) REFERENCES `labdoc` (`id_labdoc`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `com_id_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `conversation`
--
ALTER TABLE `conversation`
  ADD CONSTRAINT `conv_id_rep` FOREIGN KEY (`id_report`) REFERENCES `report` (`id_report`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `criteria_group`
--
ALTER TABLE `criteria_group`
  ADD CONSTRAINT `criteria_group_ibfk1` FOREIGN KEY (`id_origin`) REFERENCES `criteria_group` (`id_criteria_group`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `criteria_group_ibfk2` FOREIGN KEY (`id_rubric`) REFERENCES `rubric` (`id_rubric`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `criteria_group_ibfk3` FOREIGN KEY (`id_assessment`) REFERENCES `assessment` (`id_assessment`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `criterion`
--
ALTER TABLE `criterion`
  ADD CONSTRAINT `criterion_ibfk1` FOREIGN KEY (`id_origin`) REFERENCES `criterion` (`id_criterion`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `criterion_ibfk2` FOREIGN KEY (`id_criteria_group`) REFERENCES `criteria_group` (`id_criteria_group`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `criterion_ibfk3` FOREIGN KEY (`id_rubric`) REFERENCES `rubric` (`id_rubric`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `criterion_ibfk4` FOREIGN KEY (`id_assessment`) REFERENCES `assessment` (`id_assessment`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `descrob`
--
ALTER TABLE `descrob`
  ADD CONSTRAINT `descrob_ibfk1` FOREIGN KEY (`id_criterion`) REFERENCES `criterion` (`id_criterion`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `descrob_ibfk2` FOREIGN KEY (`id_origin`) REFERENCES `descrob` (`id_descrob`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `extplatform`
--
ALTER TABLE `extplatform`
  ADD CONSTRAINT `extplatform_ibfk_1` FOREIGN KEY (`id_inst`) REFERENCES `institution` (`id_inst`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `institution`
--
ALTER TABLE `institution`
  ADD CONSTRAINT `institution_ibfk_1` FOREIGN KEY (`id_class_teacher`) REFERENCES `class` (`id_class`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `labdoc`
--
ALTER TABLE `labdoc`
  ADD CONSTRAINT `labdoc_ibfk_1` FOREIGN KEY (`id_report_part`) REFERENCES `report_part` (`id_report_part`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `labdoc_ibfk_2` FOREIGN KEY (`id_ld_origin`) REFERENCES `labdoc` (`id_labdoc`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `labdoc_ibfk_3` FOREIGN KEY (`last_editor`) REFERENCES `user` (`id_user`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `ld_id_rep` FOREIGN KEY (`id_report`) REFERENCES `report` (`id_report`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `labdoc_status`
--
ALTER TABLE `labdoc_status`
  ADD CONSTRAINT `lds_id_labdoc` FOREIGN KEY (`id_labdoc`) REFERENCES `labdoc` (`id_labdoc`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `lds_id_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `link_annotation_learner`
--
ALTER TABLE `link_annotation_learner`
  ADD CONSTRAINT `link_annotation_learner_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `link_annotation_learner_ibfk_2` FOREIGN KEY (`id_annotation`) REFERENCES `annotation` (`id_annotation`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `link_class_learner`
--
ALTER TABLE `link_class_learner`
  ADD CONSTRAINT `lcl_id_class` FOREIGN KEY (`id_class`) REFERENCES `class` (`id_class`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `lcl_id_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `link_class_teacher`
--
ALTER TABLE `link_class_teacher`
  ADD CONSTRAINT `lct_id_class` FOREIGN KEY (`id_class`) REFERENCES `class` (`id_class`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `lct_id_teacher` FOREIGN KEY (`id_teacher`) REFERENCES `teacher` (`id_teacher`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `link_class_tteam`
--
ALTER TABLE `link_class_tteam`
  ADD CONSTRAINT `lctt_id_class` FOREIGN KEY (`id_class`) REFERENCES `class` (`id_class`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `lctt_id_teacher_team` FOREIGN KEY (`id_teacher_team`) REFERENCES `teacher_team` (`id_teacher_team`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `link_conversation_user`
--
ALTER TABLE `link_conversation_user`
  ADD CONSTRAINT `lcu_id_conv` FOREIGN KEY (`id_conversation`) REFERENCES `conversation` (`id_conversation`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `lcu_id_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `link_extplatform_user`
--
ALTER TABLE `link_extplatform_user`
  ADD CONSTRAINT `link_extplatform_user_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE,
  ADD CONSTRAINT `link_extplatform_user_ibfk_2` FOREIGN KEY (`id_extplatform`) REFERENCES `extplatform` (`id_extplatform`) ON DELETE CASCADE;

--
-- Contraintes pour la table `link_inst_teacher`
--
ALTER TABLE `link_inst_teacher`
  ADD CONSTRAINT `link_inst_teacher_ibfk_1` FOREIGN KEY (`id_teacher`) REFERENCES `teacher` (`id_teacher`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `link_inst_teacher_ibfk_2` FOREIGN KEY (`id_inst`) REFERENCES `institution` (`id_inst`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `link_mission_teacher`
--
ALTER TABLE `link_mission_teacher`
  ADD CONSTRAINT `lmt_id_mission` FOREIGN KEY (`id_mission`) REFERENCES `mission` (`id_mission`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `lmt_id_teacher` FOREIGN KEY (`id_teacher`) REFERENCES `teacher` (`id_teacher`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `link_mission_tteam`
--
ALTER TABLE `link_mission_tteam`
  ADD CONSTRAINT `link_mission_tteam_ibfk_1` FOREIGN KEY (`id_mission`) REFERENCES `mission` (`id_mission`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `link_mission_tteam_ibfk_2` FOREIGN KEY (`id_teacher_team`) REFERENCES `teacher_team` (`id_teacher_team`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `link_report_learner`
--
ALTER TABLE `link_report_learner`
  ADD CONSTRAINT `link_report_learner_ibfk_1` FOREIGN KEY (`id_report`) REFERENCES `report` (`id_report`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `link_report_learner_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `link_ressource_user`
--
ALTER TABLE `link_ressource_user`
  ADD CONSTRAINT `lru_id_resource` FOREIGN KEY (`id_ressource`) REFERENCES `ressource` (`id_ressource`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `lru_id_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `link_teacher_tteam`
--
ALTER TABLE `link_teacher_tteam`
  ADD CONSTRAINT `lttt_id_teacher` FOREIGN KEY (`id_teacher`) REFERENCES `teacher` (`id_teacher`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `lttt_id_teacher_team` FOREIGN KEY (`id_teacher_team`) REFERENCES `teacher_team` (`id_teacher_team`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `message_ibfk_1` FOREIGN KEY (`id_labdoc`) REFERENCES `labdoc` (`id_labdoc`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `message_ibfk_2` FOREIGN KEY (`id_user_sender`) REFERENCES `user` (`id_user`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `msg_id_conv` FOREIGN KEY (`id_conversation`) REFERENCES `conversation` (`id_conversation`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `message_var`
--
ALTER TABLE `message_var`
  ADD CONSTRAINT `mv_id_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `report`
--
ALTER TABLE `report`
  ADD CONSTRAINT `mc_id_mission` FOREIGN KEY (`id_mission`) REFERENCES `mission` (`id_mission`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `report_ibfk_1` FOREIGN KEY (`id_team_config`) REFERENCES `team_config` (`id_team_config`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `report_part`
--
ALTER TABLE `report_part`
  ADD CONSTRAINT `rp_id_mission` FOREIGN KEY (`id_mission`) REFERENCES `mission` (`id_mission`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `ressource`
--
ALTER TABLE `ressource`
  ADD CONSTRAINT `r_id_mission` FOREIGN KEY (`id_mission`) REFERENCES `mission` (`id_mission`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `r_id_report` FOREIGN KEY (`id_report`) REFERENCES `report` (`id_report`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `rubric`
--
ALTER TABLE `rubric`
  ADD CONSTRAINT `rubric_ibfk1` FOREIGN KEY (`id_activity`) REFERENCES `mission` (`id_mission`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `teacher`
--
ALTER TABLE `teacher`
  ADD CONSTRAINT `t_id_teacher` FOREIGN KEY (`id_teacher`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `teacher_report_status`
--
ALTER TABLE `teacher_report_status`
  ADD CONSTRAINT `trs_ibfk_1` FOREIGN KEY (`id_report`) REFERENCES `report` (`id_report`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `trs_ibfk_2` FOREIGN KEY (`id_teacher`) REFERENCES `teacher` (`id_teacher`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `teacher_team`
--
ALTER TABLE `teacher_team`
  ADD CONSTRAINT `teacher_team_ibfk_1` FOREIGN KEY (`id_inst`) REFERENCES `institution` (`id_inst`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `team_config`
--
ALTER TABLE `team_config`
  ADD CONSTRAINT `team_config_ibfk_1` FOREIGN KEY (`id_mission`) REFERENCES `mission` (`id_mission`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `team_config_ibfk_2` FOREIGN KEY (`id_class`) REFERENCES `class` (`id_class`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `team_config_ibfk_3` FOREIGN KEY (`allow_import_id_mission`) REFERENCES `mission` (`id_mission`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `team_config_ibfk_4` FOREIGN KEY (`allow_msg_id_class`) REFERENCES `class` (`id_class`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `team_config_ibfk_5` FOREIGN KEY (`allow_msg_id_mission`) REFERENCES `mission` (`id_mission`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `trace`
--
ALTER TABLE `trace`
  ADD CONSTRAINT `trace_ibfk_1` FOREIGN KEY (`id_action`) REFERENCES `trace_action` (`id_action`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`current_report`) REFERENCES `report` (`id_report`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `user_ibfk_2` FOREIGN KEY (`id_inst`) REFERENCES `institution` (`id_inst`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
