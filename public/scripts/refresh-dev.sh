#!/bin/sh
DIR=$(dirname $0)
BASEDIR="$DIR/../.."
cd $BASEDIR
cronJob(){
    while true
    do
        ./lnb artisan schedule:run >> storage/logs/cron.log 2>&1
        sleep 60
    done
}
./lnb update
echo "# LabNBook updates (SQL structure, etc)"
cronJob &
yarn watch
