<?php
require_once dirname(__DIR__ ) . '/common/default.php';

if (PHP_SAPI !== 'cli') {
	echo "CLI only";
	exit();
}

if (empty(config('ldap.host'))) {
		echo <<<EOPHP
Please set the following variables in your .env file :

LDAP_HOST=
LDAP_PORT=
LDAP_ENCRYPTION=
LDAP_CREDENTIALS_BIND=
LDAP_CREDENTIALS_PASSWORD=

EOPHP;
	exit();
}

if (count($argv) < 2) {
	echo <<<EOL
Usage:
    scripts/ldap.php search <filter>
    scripts/ldap.php student <UGA|G-INP|USMB> <num-etu>
    scripts/ldap.php students <UGA|G-INP|USMB> <num-etu1> ...

Examples
    php5.6 scripts/ldap.php search uid=gannazf
	php5.6 scripts/ldap.php student UGA 12345678
	php5.6 scripts/ldap.php students UGA 12345678 87654321

EOL;
	exit();
}

switch ($argv[1]) {
	case 'search':
		if (empty($argv[2]) || strpos($argv[2], '=') === false) {
			die("Missing parameter, e.g. 'cn=François Gannaz'\n");
		}
		ldapTest($argv[2]);
		break;
	case 'student':
		if (!isset($argv[3])) {
			die("Missing parameters: [UGA|G-INP|USMB] [numero AGALAN]\n");
		}
		ldapStudent($argv[2], $argv[3]);
		break;
	case 'students':
		ldapStudents($argv[2], array_slice($argv, 3));
		break;
	default:
		echo "Unknown action.\n";
		break;
}

function ldapTest($filter)
{
	$ldap = new \App\Processes\Ldap();
	$users = $ldap->getUsers($filter);
	if ($users !== null) {
		echo "Nombre de résultats : " . count($users) . "\n";
		print_r($users);
	} else if ($ldap->hasError()) {
		echo "Erreur LDAP : " . $ldap->getError();
	} else {
		echo "Pas de correspondance.\n";
	}
}

function ldapStudent($institutionName, $studentNumber) {
	$ldap = new \App\Processes\Ldap();
    if (!$ldap || $ldap->hasError() || !$ldap->hasInstitute($institutionName)) {
		echo "Erreur ldap ou institution non connectée au ldap";
		return;
	}
	$student = $ldap->getStudentByNumber($studentNumber);
	if ($student) {
		echo "Étudiant : ";
		print_r($student);
	} else if ($ldap->hasError()) {
		echo "Erreur LDAP : " . $ldap->getError();
	} else {
		echo "Pas de correspondance.\n";
	}
}

function ldapStudents($institutionName, $studentNumbers) {
	$ldap = new \App\Processes\Ldap();
	$students = $ldap->getStudentsByNumbers($institutionName, $studentNumbers);
	if ($students) {
		echo "Étudiants : ";
		print_r($students);
	} else if ($ldap->hasError()) {
		echo "Erreur LDAP : " . $ldap->getError();
	} else {
		echo "Pas de correspondance.\n";
	}
}
