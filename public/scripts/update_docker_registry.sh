#!/bin/bash
DIR=$(realpath $(dirname $0)/../..)
echo $DIR
if [ ! -z "$1" ];
then
	tag=$1
else
	tag="latest"
fi
docker login gricad-registry.univ-grenoble-alpes.fr
docker build -t gricad-registry.univ-grenoble-alpes.fr/labnbook/labnbook:$tag $DIR/docker/labnbook/
docker push gricad-registry.univ-grenoble-alpes.fr/labnbook/labnbook:$tag

