#!/bin/bash
set -e
set -x
LNB_DIR=$(realpath $(dirname $(realpath $0))/../..)
gotolabnbook() {
	cd $LNB_DIR
}

init_or_update_ext() {
	if [ ! -d "$1" ]; then
		git clone https://gricad-gitlab.univ-grenoble-alpes.fr/labnbook/jupyter/$1.git
	fi
}

check_deps() {
	set +e
	. ~/.bashrc
	conda > /dev/null 2>&1
	ret=$?
	if [ $ret -ne 0 ]; then
		echo "Impossible de continuer, vous devez installer conda"
		exit 1
	fi
	set -e

}

check_deps

echo "--------------------------"
echo "Téléchargement des sources"
echo "--------------------------"
gotolabnbook
mkdir -p ../jupyterLab-extensions/
cd ../jupyterLab-extensions/
for ext in lnb_ro_init lnb_ro_synchronize lnb_ro_theme lnb_rw_init lnb_rw_save lnb_rw_theme lnb_rw_undoredo; do
	init_or_update_ext $ext
done

echo "------------------------------------------------------"
echo "Effacement de l'environnement et des fichiers compilés"
echo "------------------------------------------------------"
echo $CONDA_PREFIX
[ ! -z "$CONDA_PREFIX" ] && echo "Deactivating" conda deactivate
source $(conda info --base)/etc/profile.d/conda.sh
conda env remove --name jl-lnb-rw-env
gotolabnbook
cd public/jupyter/rw/
rm -rf .cache .jupyterlite.doit.db lite_rw
cd ../ro/
rm -rf .cache .jupyterlite.doit.db lite_ro
cd ../../../../jupyterLab-extensions/

echo ""
echo "-----------------------------------------"
echo "Création de l'environnement jl-lnb-rw-env"
echo "-----------------------------------------"
conda create -n jl-lnb-rw-env --override-channels --strict-channel-priority -c conda-forge -c nodefaults jupyterlab=3.4.8 retrolab==0.3.21 cookiecutter==2.2.3 nodejs==20.5.1 pkginfo==1.9.6 jupyterlab_widgets==3.0.8 plotly==5.15.0 --yes
conda activate jl-lnb-rw-env

echo ""
echo "---------------------------"
echo "Compilation de lnb_rw_theme"
echo "---------------------------"
cd lnb_rw_theme
pip install -ve .
jupyter labextension develop . --overwrite
jlpm run build

echo ""
echo "--------------------------"
echo "Compilation de lnb_rw_init"
echo "--------------------------"
cd ../lnb_rw_init
pip install -ve .
jupyter labextension develop . --overwrite
jlpm run build

echo ""
echo "--------------------------"
echo "Compilation de lnb_rw_save"
echo "--------------------------"
cd ../lnb_rw_save
pip install -ve .
jupyter labextension develop . --overwrite
jlpm run build

echo ""
echo "------------------------------"
echo "Compilation de lnb_rw_undoredo"
echo "------------------------------"
cd ../lnb_rw_undoredo
pip install -ve .
jupyter labextension develop . --overwrite
jlpm run build

echo ""
echo "----------------------------"
echo "Compilation pour jupyterLite"
echo "----------------------------"
pip install jupyterlite==0.1.0b18
gotolabnbook
cd public/jupyter/rw/
jupyter lite build --output-dir lite_rw --pyodide https://github.com/pyodide/pyodide/releases/download/0.22.0/pyodide-0.22.0.tar.bz2

echo ""
echo "-----------------------------------------"
echo "Création de l'environnement jl-lnb-ro-env"
echo "-----------------------------------------"
conda deactivate
conda create -n jl-lnb-ro-env --override-channels --strict-channel-priority -c conda-forge -c nodefaults jupyterlab=3.4.8 retrolab==0.3.21 cookiecutter==2.2.3 nodejs==20.5.1 pkginfo==1.9.6 jupyterlab_widgets==3.0.8 plotly==5.15.0 --yes
conda activate jl-lnb-ro-env

echo ""
echo "---------------------------"
echo "Compilation de lnb_ro_theme"
echo "---------------------------"
cd ../../../../jupyterLab-extensions/lnb_ro_theme
pip install -ve .
jupyter labextension develop . --overwrite
jlpm run build

echo ""
echo "--------------------------"
echo "Compilation de lnb_ro_init"
echo "--------------------------"
cd ../lnb_ro_init
pip install -ve .
jupyter labextension develop . --overwrite
jlpm run build

echo ""
echo "---------------------------------"
echo "Compilation de lnb_ro_synchronize"
echo "---------------------------------"
cd ../lnb_ro_synchronize
pip install -ve .
jupyter labextension develop . --overwrite
jlpm run build

echo ""
echo "----------------------------"
echo "Compilation pour jupyterLite"
echo "----------------------------"
pip install jupyterlite==0.1.0b18
gotolabnbook
cd public/jupyter/ro/
jupyter lite build --output-dir lite_ro --pyodide https://github.com/pyodide/pyodide/releases/download/0.22.0/pyodide-0.22.0.tar.bz2

echo ""
echo "-------------------------"
echo "Update jupyter-lite.json"
echo "-------------------------"
pip install jsonmerge
gotolabnbook
cd public/jupyter/rw/
python3 ../../scripts/updateJupyterLiteConfig.py "rw"
gotolabnbook
cd public/jupyter/ro/
python3 ../../scripts/updateJupyterLiteConfig.py "ro"
