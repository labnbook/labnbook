<?php

require_once dirname(__DIR__) . '/common/default.php';

if (PHP_SAPI !== 'cli') {
	die("CLI only!");
}

extractImages();

/**
 * Extract the images into files from all the labdocs.
 */
function extractImages() {
	$maxId = (int) Legacy\DB::queryScalar("SELECT MAX(id_labdoc) FROM labdoc");
	$batchSize = 500;
	for ($min = 0; $min < $maxId; $min += $batchSize) {
		$max = $min + $batchSize - 1;
		$labdocs = Legacy\DB::query(
			"SELECT l.id_labdoc, l.id_report, r.id_mission, l.labdoc_data"
			. " FROM labdoc l JOIN report r USING (id_report)"
			. " WHERE l.id_labdoc BETWEEN $min AND $max AND type_labdoc IN ('procedure', 'text')"
		);
		while ($labdoc = $labdocs->fetch(PDO::FETCH_ASSOC)) {
			$changed = extractImagesFromLabdoc((int) $labdoc['id_mission'], (int) $labdoc['id_labdoc'], $labdoc['labdoc_data']);
			if ($changed) {
				printf(
					"- %d images extraites du labdoc %d, rapport %d\n",
					$changed->getCount(),
					$labdoc['id_labdoc'],
					$labdoc['id_report']
				);
			}
		}
	}
}

/**
 * Extract the images to files, and update the labdoc in the DB.
 *
 * @param int $idMission
 * @param int $idLabdoc
 * @param string $content
 * @return \App\ImagesExtractor null if labdoc was not changed.
 */
function extractImagesFromLabdoc($idMission, $idLabdoc, $content) {
	try {
		$images = new \App\ImagesExtractor($idMission, $idLabdoc, $content);
		if ($images->getCount() > 0) {
			Legacy\DB::query(
				"UPDATE labdoc SET labdoc_data = ? WHERE id_labdoc = ?",
				[$images->getContent(), $idLabdoc]
			);
			return $images;
		}
	} catch (\Exception $ex) {
		printf("- labdoc %d, mission %d : ERREUR de données ? %s\n", $idLabdoc, $idMission, $ex->getMessage());
	}
}
