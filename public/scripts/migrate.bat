@echo off

rem The PHP directory must be in Windows' runtime path.
rem Configure with: Control Panel / System / Advanced system settings / Advanced / Environment variables... / Path
rem                 and paste your path there.

@setlocal

set SCRIPTDIR=%~dp0
if "%PHP_COMMAND%" == "" set PHP_COMMAND=php.exe
"%PHP_COMMAND%" "%SCRIPTDIR%migrate.php" %*
pause

@endlocal
