<?php

require_once dirname(__DIR__) . '/common/default.php';

if (PHP_SAPI !== 'cli') {
	die("CLI only!");
}

cleanupTexts();

/**
 * Cleanup labdocs texts, see https://gricad-gitlab.univ-grenoble-alpes.fr/labnbook/labnbook/issues/46
 */
function cleanupTexts() {
	$maxId = (int) Legacy\DB::queryScalar("SELECT MAX(id_labdoc) FROM labdoc");
	$batchSize = 500;
	$count = 0;
	for ($min = 0; $min < $maxId; $min += $batchSize) {
		$max = $min + $batchSize - 1;
		$labdocs = Legacy\DB::query(
			"SELECT l.id_labdoc, l.labdoc_data"
			. " FROM labdoc l"
			. " WHERE l.id_labdoc BETWEEN $min AND $max AND type_labdoc IN ('text')"
			. " AND labdoc_data <> ''"
		);
		while ($labdoc = $labdocs->fetch(PDO::FETCH_ASSOC)) {
			$count += (int) filterLabdocHtml((int) $labdoc['id_labdoc'], $labdoc['labdoc_data']);
		}
	}
	echo "$count labdocs changed\n";
}

/**
 * @param int $idLabdoc
 * @param string $content
 * @return boolean Changed?
 */
function filterLabdocHtml($idLabdoc, $content) {
	$newContent = HtmlFilter::clean($content);
	echo "------------------------------------------------
BEFORE:
$content

AFTER:
$newContent


";
	Legacy\DB::exec(
		"UPDATE labdoc SET labdoc_data = ? WHERE id_labdoc = ?",
		[$newContent, $idLabdoc]
	);
	return true;
}
