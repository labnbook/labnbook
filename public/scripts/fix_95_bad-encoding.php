<?php

require_once dirname(__DIR__) . '/common/default.php';

if (PHP_SAPI !== 'cli') {
	die("CLI only!");
}

$fixer = new BadlyEncoded();
$fixer->fixLabdocs();

class BadlyEncoded
{
	private $characters = [];

	public function __construct()
	{
		$this->characters = self::listCharacters();
	}

	public function fixLabdocs() {
		$maxId = (int) Legacy\DB::queryScalar("SELECT MAX(id_labdoc) FROM labdoc");
		$batchSize = 1000;
		$totalDetected = 0;
		$totalUpdated = 0;
		for ($min = 0; $min < $maxId; $min += $batchSize) {
			$max = $min + $batchSize - 1;
			$labdocs = Legacy\DB::query(
				"SELECT l.id_labdoc, l.labdoc_data, l.id_report"
				. " FROM labdoc l"
				. " WHERE type_labdoc = 'text' AND id_labdoc BETWEEN $min AND $max AND labdoc_data RLIKE ?",
				[$this->buildBadCharRegexp()]
			);
			$updated = 0;
			$count = 0;
			while($labdoc = $labdocs->fetch(PDO::FETCH_ASSOC)) {
				$count++;
				$fixedText = $this->fixText($labdoc['labdoc_data']);
				if ($fixedText !== $labdoc['labdoc_data']) {
					$updated += Legacy\DB::exec(
						"UPDATE labdoc SET labdoc_data = ? WHERE id_labdoc = ?",
						[$fixedText, $labdoc['id_labdoc']]
					);
                    printf("    RP %5ld / LD %d\n", $labdoc['id_report'], $labdoc['id_labdoc']);
				}
			}
            if ($count) {
                $totalDetected += $count;
                $totalUpdated += $updated;
                printf("[%6d-%6d] %d labdocs detected, %d fixed.\n", $min, $max, $count, $updated);
            }
		}
		printf("TOTAL: %d labdocs detected, %d fixed%s.\n", $totalDetected, $totalUpdated, $totalDetected > $totalUpdated ? " WARNING" : "");
	}

	public function fixText($text) {
		$new = str_replace(array_keys($this->characters), array_values($this->characters), $text);
		if (!mb_check_encoding($new, 'UTF-8')) {
			fprintf(STDERR, "Not valid UTF-8\n");
		}
		return $new;
	}

	private static function listCharacters() {
		$characters = preg_split('//u', " ´’çàâéèêëîïôùü", null, PREG_SPLIT_NO_EMPTY);
		$map = [];
		foreach ($characters as $c) {
			$map[mb_convert_encoding($c, 'UTF-8', 'ISO-8859-1')] = $c;
		}
		$map["\xC3\xA3\xC2\x80\xC2\x80"] = " ";
		return $map;
	}

	private function buildBadCharRegexp() {
		return join('|', array_keys($this->characters));
	}
}
