#!/bin/sh
DIR=$(dirname $0)
BASEDIR="$DIR/../.."
lnb="$BASEDIR/lnb"

echo "# PHP dependencies"
[ -e composer.json ] && composer install --optimize-autoloader --prefer-dist --no-interaction
echo "# JS dependencies"
[ -e "webpack.mix.js" ] && yarn install --prefer-offline && yarn dev
echo "# LabNBook updates (SQL structure, etc)"
php $DIR/migrate.php -y
$lnb artisan migrate
$lnb artisan clear-compiled
$lnb artisan ide-helper:generate
$lnb cache
