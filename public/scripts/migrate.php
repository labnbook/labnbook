<?php

require_once dirname(__DIR__) . '/common/default.php';

if (PHP_SAPI !== 'cli') {
	die("CLI only!");
}
$automatic = isset($argv[1]) && $argv[1] === '-y';
$force = isset($argv[2]) && $argv[2] === '-f';


migrationInit();

$migrationFiles = glob(__DIR__ . '/migrations/*');
$migrationsToApply = [];
if ($migrationFiles) {
	foreach ($migrationFiles as $migrationFile) {
		$migrationName = basename($migrationFile);
		if (!migrationIsApplied($migrationName)) {
			$migrationsToApply[$migrationFile] = $migrationName;
		}
	}
	if ($migrationsToApply) {
		echo count($migrationsToApply) . " migrations to apply:\n";
		if (!$automatic) {
			echo "    " . join("\n    ", $migrationsToApply) . "\n";
			echo "Press Ctrl-C to abort, Enter to continue.\n";
			fgets(STDIN);
		}
		foreach ($migrationsToApply as $migrationFile => $migrationName) {
			try {
				if (!migrationApply($migrationFile, $migrationName)) {
					echo "Migration failed. Aborting migrations.\n";
					if(!$force){
						break;
					}
				}
			} catch (Exception $ex) {
				echo "\nFatal error: " . $ex->getMessage();
				echo "\nAborting migrations.\n";
				if(!$force){
					break;
				}
			}
		}
	}
}
if (!$migrationsToApply && !$automatic) {
	echo "No new migration.\n";
	echo "Press Ctrl-C to abort, Enter to create a migration template.\n";
	fgets(STDIN);
	if (!is_dir(__DIR__ . '/migrations')) {
		mkdir(__DIR__ . '/migrations');
	}
	$filename = '/migrations/' . date('Y-m-d_Hi') . '_new.sql';
	touch(__DIR__ . $filename);
	echo "Please rename 'scripts/$filename'\n";
}

/**
 * Create the required environment if necessary.
 */
function migrationInit() {
	$exists = (boolean) Legacy\DB::query("SHOW TABLES LIKE 'migration'")->fetchAll();
	if (!$exists) {
		echo "### Creating migration table...\n";
		Legacy\DB::exec(<<<EOSQL
CREATE TABLE migration (
	id BINARY(20) NOT NULL PRIMARY KEY,
	filename VARBINARY(255) NOT NULL,
	filehash BINARY(20) NOT NULL,
	filetime BIGINT NOT NULL,
	appliedtime BIGINT NOT NULL
)
EOSQL
		);
		echo "### Done.\n\n";
		$startingPoint = Legacy\DB::query("SHOW COLUMNS FROM `report` LIKE 'com_restr_to_mission'")->fetchAll();
		if (empty($startingPoint)) {
			migrationSkip("2017-12-28_1200_commentaires.sql");
			migrationSkip("2018-01-11_1200_widget-msg.sql");
			migrationSkip("2018-01-22_1200_conversation-msg.sql");
			migrationSkip("2018-01-22_1300_report-msg.sql");
			migrationSkip("2018-01-23_1200_debug-comment.sql");
		}
	}
}

/**
 * @param string $name File name (no path)
 * @return boolean
 */
function migrationIsApplied($name) {
	return (boolean) Legacy\DB::queryScalar(
		"SELECT 1 FROM migration WHERE id = UNHEX(?)",
		[sha1($name)]
	);
}

/**
 * Apply a migration, display messages, and store the migration's status.
 *
 * @param string $filename
 * @param string $name
 * @return boolean success?
 */
function migrationApply($filename, $name) {
	echo "### Applying $name...\n";
	if (!migration_execute($filename)) {
		return false;
	}
	migrationSkip($name);
	echo "\n### Done.\n\n";
	return true;
}

/**
 * Mark a migration as done.
 *
 * @param string $name
 */
function migrationSkip($name) {
	$filename = __DIR__ . "/migrations/$name";
	Legacy\DB::exec(
		"INSERT INTO migration VALUES (UNHEX(?), ?, UNHEX(?), ?, NOW())",
		[sha1($name), $name, sha1_file($filename), filemtime($filename)]
	);
}
/**
 * Execute the SQL or PHP content of a migration file.
 *
 * @param string $filename
 * @return boolean success?
 */
function migration_execute($filename) {
	if (preg_match('/\.sql$/', $filename)) {
		$wholeSql = file_get_contents($filename);
		$sqls = preg_split('/\s*;\s*\n/', $wholeSql, -1, PREG_SPLIT_NO_EMPTY);
		foreach ($sqls as $sql) {
			echo "## SQL:\n$sql\n";
			$changes = Legacy\DB::exec($sql);
			printf("%d rows changed.\n", $changes);
		}
		return true;
	} else if (preg_match('/\.php$/', $filename)) {
		require($filename);
		return true;
	} else {
		echo "Migration de type inconnu.";
		return false;
	}
}
