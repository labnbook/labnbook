<?php
$FILENAME="out.csv";
$CHUNK_SIZE=200;

require_once dirname(__DIR__) . '/common/default.php';
$f = fopen($FILENAME, "w");

// Generate the header line
$r = \App\Indicators::getIndicators(\App\Report::first(), false);
$r['id_report'] =0;
$r['id_mission'] =0;
fputcsv($f, array_keys($r));

// Process all reports by batch
\App\Report::whereIn('status', ['wait','arc'])->chunk($CHUNK_SIZE, function ($reports) use ($f) {
    foreach ($reports as $r) {
        $tmp = \App\Indicators::getIndicators($r, false);
        $tmp['id_report'] = $r->id_report;
        $tmp['id_mission'] = $r->id_mission;
        fputcsv($f, $tmp);
    }
});
fclose($f);
