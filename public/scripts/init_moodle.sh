#!/bin/bash

DIR="$(dirname $0)/../.."
cd $DIR
if [ ! -d "moodle" ]; then
	git clone https://github.com/moodle/moodle.git
fi
cd moodle
git checkout MOODLE_402_STABLE
cd mod
if [ ! -d "labnbook" ]; then
	git clone git@gricad-gitlab.univ-grenoble-alpes.fr:labnbook/moodle-labnbook.git labnbook
fi
cd ../..
pwd
if [ -z "$(grep moodle.yml .env)" ]; then
	echo "Adding Moodle yml"
	val=$(awk -F '=' '/COMPOSE_FILE/{print $2}' .env )
	if [ -z "$val" ]; then
		echo "COMPOSE_FILE=docker-compose.yml:docker/moodle.yml" >> .env
	else
		sed -i -e "s/\(COMPOSE_FILE=.*\)/\1:docker\/moodle.yml/" .env
	fi
	echo "bla"
fi
echo "blabla"
if [ -z "$(grep MOODLE_PORT .env)" ]; then
	echo MOODLE_PORT=8002 >> .env
fi
sudo chown -R :www-data moodle 
sudo chmod g+w -R moodle
docker-compose down 
docker-compose up
