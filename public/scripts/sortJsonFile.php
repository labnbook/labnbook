<?php
if($argc != 2) {
	echo "Usage $argv[0] <jsonfile>\n";
	echo "Sorts the json file inplace using array keys\n";
	return;
}
$data = (array)json_decode(file_get_contents($argv[1]));
ksort($data);
file_put_contents($argv[1], json_encode($data, JSON_PRETTY_PRINT| JSON_UNESCAPED_UNICODE));
