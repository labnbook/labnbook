-- Shuffle function  from https://stackoverflow.com/questions/11860023/shuffle-a-string-with-mysql-sql?noredirect=1

DELIMITER //

DROP FUNCTION IF EXISTS shuffle;

CREATE FUNCTION shuffle(
    v_chars TEXT
)
RETURNS TEXT
NOT DETERMINISTIC -- multiple RAND()'s
NO SQL
SQL SECURITY INVOKER
COMMENT ''
BEGIN
    DECLARE v_retval TEXT DEFAULT '';
    DECLARE u_pos    INT UNSIGNED;
    DECLARE u        INT UNSIGNED;

    SET u = LENGTH(v_chars);
    WHILE u > 0
    DO
      SET u_pos = 1 + FLOOR(RAND() * u);
      SET v_retval = CONCAT(v_retval, MID(v_chars, u_pos, 1));
      SET v_chars = CONCAT(LEFT(v_chars, u_pos - 1), MID(v_chars, u_pos + 1, u));
      SET u = u - 1;
    END WHILE;

    RETURN v_retval;
END;
//

DELIMITER ;

-- From https://dba.stackexchange.com/a/211664

DROP TABLE IF EXISTS random_first_names;
DROP TABLE IF EXISTS random_last_names;

CREATE TABLE random_first_names (
    id_user INT(10) UNSIGNED AUTO_INCREMENT,
    name VARCHAR(32),
    PRIMARY KEY(id_user) )
SELECT shuffle(SUBSTRING(first_name, 1, 8)) as name FROM user WHERE first_name IS NOT NULL ORDER BY RAND();

CREATE TABLE random_last_names (
    id_user INT(10) UNSIGNED AUTO_INCREMENT,
    name VARCHAR(64),
    PRIMARY KEY(id_user) )
SELECT shuffle(SUBSTRING(user_name, 1, 8)) as name FROM user WHERE user_name IS NOT NULL ORDER BY RAND();

UPDATE user
       SET first_name = (SELECT name
                                FROM random_first_names
                                ORDER BY rand()
                                LIMIT 1),
           user_name = (SELECT name
                               FROM random_last_names
                               ORDER BY rand()
                               LIMIT 1);

DROP TABLE IF EXISTS random_first_names;
DROP TABLE IF EXISTS random_last_names;

-- ADDED by dbeniamine
UPDATE IGNORE user SET login=CONCAT(SUBSTRING(first_name, 1, 2), SUBSTRING(user_name, 1, 8));
UPDATE IGNORE user SET email=CONCAT(login, "@example.com");
UPDATE user SET INST_NUMBER=NULL;
UPDATE user SET pwd=NULL;

-- Anonymize teams
UPDATE report SET team_name = CONCAT(id_report, '_', id_mission);

-- Suppression des messages .. a quitté / a été invité à la conversation
DELETE FROM message WHERE msg_content LIKE "%la conversation%";
-- Suppression des messages Conversation crée par ...
DELETE FROM message WHERE msg_content LIKE "Conversation cr%";
-- Suppression des données de password-reset
TRUNCATE password_resets;

