<?php

$do_maj = false;
if (isset($_GET['do']) && $_GET['do'] == 1) {
	$do_maj = true;
}

require("../common/default.php");

echo "Mise à jour des institutions des classes et des étudiants à partir de celles des enseignants<br/>";

function result($nb_modif, $do_maj) {
	if ($do_maj) {
		echo $nb_modif . " mises à jour effectuées<br/>";
	} else {
		echo $nb_modif . " mises à jour à effectuer - Ajouter ?do=1 pour effectuer la mise à jour<br/>";
	}
}

$nb_modif = 0;
echo "<br/>Mise à jour des institutions des classes avec l'institution du premier enseignant rattaché (id_class/class_name/id_inst)<br/>";
$res = Legacy\DB::query("SELECT * FROM class");
while ($obj = $res->fetchObject()) {
	$res2 = Legacy\DB::query("SELECT lct.id_teacher, id_inst FROM link_class_teacher as lct, link_inst_teacher as lit WHERE id_class=" . $obj->id_class . " AND lct.id_teacher = lit.id_teacher");
	$obj2 = $res2->fetchObject();
	if ($obj->id_inst != $obj2->id_inst && $obj2->id_teacher > 5) {
		$nb_modif++;
		echo $obj->id_class . " ; " . $obj->class_name . " ; " . $obj->id_inst . " -> " . $obj2->id_inst . " (" . $obj2->id_teacher . ")<br/>";
		if ($do_maj) {
			Legacy\DB::exec("UPDATE class SET id_inst = " . $obj2->id_inst . " WHERE id_class = " . $obj->id_class);
		}
	}
	//else { echo $obj->id_class." ; ".$obj->class_name." ; ".$obj->id_inst." OK<br/>" ; }
}
result($nb_modif, $do_maj);

$nb_modif = 0;
echo "<br/>Mise à jour des institutions des étudiants avec l'institution de la première classe à laquel ils sont rattachés (id_user/first_name/user_name/id_inst)<br/>";
/* $res = Legacy\DB::query("SELECT * FROM user WHERE id_user NOT IN (SELECT id_teacher FROM teacher)");
  while($obj = $res->fetchObject()) {
  $res2 = Legacy\DB::query("SELECT id_inst FROM link_class_learner, class WHERE id_user=".$obj->id_user." AND link_class_learner.id_class = class.id_class") ;
  $obj2 = $res2->fetchObject() ;
  if ($obj->id_inst != $obj2->id_inst) {
  $nb_modif++ ;
  echo $obj->id_user." ; ".$obj->first_name." ; ".$obj->user_name." ; ".$obj->id_inst." -> ".$obj2->id_inst."<br/>" ;
  if ($do_maj) { Legacy\DB::exec("UPDATE user SET id_inst = ".$obj2->id_inst." WHERE id_user = ".$obj->id_user) ; }
  }
  //else { echo $obj->id_user." ; ".$obj->first_name." ; ".$obj->user_name." ; ".$obj->id_inst." OK<br/>" ; }
  } */


$res = Legacy\DB::query("SELECT user.id_user, first_name, user_name, user.id_inst as u_id_inst, class.id_inst as c_id_inst, class.id_class, class_name FROM class, link_class_learner, user WHERE class.id_class = link_class_learner.id_class AND link_class_learner.id_user = user.id_user AND user.id_inst <> class.id_inst AND user.id_user NOT IN (SELECT id_teacher FROM teacher) order by id_user, user.id_inst");
while ($obj = $res->fetchObject()) {
	$nb_modif++;
	echo $obj->id_user . " ; " . $obj->first_name . " ; " . $obj->user_name . " ; " . $obj->u_id_inst . " -> " . $obj->c_id_inst . "<br/>";
	if ($do_maj) {
		Legacy\DB::exec("UPDATE user SET id_inst = " . $obj->c_id_inst . " WHERE id_user = " . $obj->id_user);
	}
}
result($nb_modif, $do_maj);
