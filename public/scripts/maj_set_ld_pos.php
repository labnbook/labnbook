<?php

$do_maj = false;
if (isset($_GET['do']) && $_GET['do'] == 1) {
	$do_maj = true;
}

require("../common/default.php");

echo "<b>Mise en cohérence des numéros de position des LD dans une RP</b><br/><br/>";

echo "<b>Pour les LD des missions</b><br/>"; // les LD des RP créés par les enseignants
echo "id_rp ; id_ld ; position<br/>";

$nb_modif = 0;
$nb_maj = 0;
$id_rp = 0;
$query = "SELECT id_labdoc, id_report_part, position FROM labdoc"
		. " WHERE id_report IS NULL AND id_report_part IS NOT NULL AND position IS NOT NULL"
		. " ORDER BY id_report_part, position, id_labdoc";
$result = Legacy\DB::query($query);
while ($ld = $result->fetchObject()) {
	if ($ld->id_report_part != $id_rp) {
		$pos = 1;
		$id_rp = $ld->id_report_part;
	} else {
		$pos++;
	}
	if ($pos != $ld->position) {
		echo $ld->id_report_part . " ; " . $ld->id_labdoc . " ; " . $ld->position . " --> " . $pos . "<br/>";
		$nb_modif++;
		if ($do_maj && $pos) {
			DB::exec("UPDATE labdoc SET position=" . $pos . " WHERE id_labdoc=" . $ld->id_labdoc);
			$nb_maj++;
		}
	}
}
echo "<br/><b>" . $nb_modif . " mises à jour à effectuer - " . $nb_maj . " mises à jour effectuées</b><br/>";
if (!$do_maj) {
	echo "Ajouter ?do=1 pour effectuer la mise à jour<br/>";
}

echo "<br/><br/><b>Pour les LD des rapports</b><br/>"; // Les LD des rapports étudiants dans les RP
echo "id_r ; id_rp ; id_ld ; position<br/>";

$nb_modif = 0;
$nb_maj = 0;
$id_r = 0;
$id_rp = 0;
$query = "SELECT id_labdoc, id_report, id_report_part, position FROM labdoc"
		. " WHERE id_report IS NOT NULL AND id_report_part IS NOT NULL AND id_report_part <> 0 AND position IS NOT NULL"
		. " ORDER BY id_report, id_report_part, position, id_labdoc";
$result = Legacy\DB::query($query);
while ($ld = $result->fetchObject()) {
	if ($ld->id_report != $id_r || $ld->id_report_part != $id_rp) {
		$pos = 1;
		$id_r = $ld->id_report;
		$id_rp = $ld->id_report_part;
	} else {
		$pos++;
	}
	if ($pos != $ld->position) {
		echo $ld->id_report . " ; " . $ld->id_report_part . " ; " . $ld->id_labdoc . " ; " . $ld->position . " --> " . $pos . "<br/>";
		$nb_modif++;
		if ($do_maj  && $pos) {
			Legacy\DB::exec("UPDATE labdoc SET position=" . $pos . " WHERE id_labdoc=" . $ld->id_labdoc);
			$nb_maj++;
		}
	}
}
echo "<br/><b>" . $nb_modif . " mises à jour à effectuer - " . $nb_maj . " mises à jour effectuées</b><br/>";
if (!$do_maj) {
	echo "Ajouter ?do=1 pour effectuer la mise à jour<br/>";
}
