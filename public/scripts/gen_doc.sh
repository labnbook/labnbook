#!/bin/bash
if [ ! -f "artisan" ]; then
	echo "This script should be run at the project's root"
	exit 1;
fi


if [ -z "$(which phpdoc)" ]; then
	dir='/data'
	cmd="docker run --rm -v $(pwd):/data phpdoc/phpdoc:3.2 run"
else
	dir='./'
	cmd="phpdoc"
fi
$cmd --directory $dir --target $dir/doc --ignore vendor/ --ignore node_modules/
