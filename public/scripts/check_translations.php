<?php
$js_path=dirname(__FILE__)."/../data/languages";
$php_path=dirname(__FILE__)."/../../resources/lang/";
$baselang = 'fr';
// String with more than this number of words identical to the translation key will be reported as errors
$MAX_IDENTICAL_TRANS_WORDS=2;

/**
 * Checks that a translation file is complete
 * Side effect : echoes errors
 * @param SplFileInfo file
 * @param string $type
 * @return int number of errors
 */
function check_json($file, $type) {
	global $MAX_IDENTICAL_TRANS_WORDS;
	$json = json_decode(file_get_contents($file->getRealPath()));
	$errors = 0;
	foreach($json as $k => $v) {
		// Remove i18next markers from key
		$trans_key = preg_replace('/{{-|}}|{{/', '',$k);
		if (str_word_count($trans_key) > $MAX_IDENTICAL_TRANS_WORDS && (empty($v) || $k == $v)) {
			echo "Traduction $type manquante : '$k' = '$v'\n";
			$errors++;
		}
	}
	return $errors;
}

// JS translation files
$dir = new DirectoryIterator($js_path);
foreach ($dir as $fileinfo) {
	if ($fileinfo->isDir() && !in_array($fileinfo->getFilename(), [$baselang, '.', '..'])) {
		$errors = check_json(new SplFileInfo($fileinfo->getRealPath()."/translation.json"), 'javascript');
	}
}

// PHP translation files
$dir = new DirectoryIterator($php_path);
foreach ($dir as $fileinfo) {
	if ($fileinfo->isFile() && $fileinfo->getFilename() != "$baselang.json") {
		$errors += check_json($fileinfo, 'PHP');
	}
}
exit($errors);
