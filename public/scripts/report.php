<?php
require_once dirname(__DIR__ ) . '/common/default.php';

if (PHP_SAPI !== 'cli') {
	echo "CLI only";
	exit();
}

$helpMessage = <<<EOL
Usage:
    scripts/report.php backup <ID>
    scripts/report.php restore <ID>

EOL;

$scriptName = array_shift($argv);
$action = array_shift($argv);
switch ($action) {
	case 'backup':
		$id = array_shift($argv);
		if (!$id) {
			die($help);
		}
		$backup = new \App\Processes\BackupReport($id);
		if ($backup->backup($id)) {
			printf("Rapport sauvegardé avec succès dans '%s'.\n", $backup->getBackupdir($id));
		} else {
			echo "Sauvegarde annulée. Vérifiez l'ID et les permissions de fichiers.\n";
			exit(1);
		}
		break;
	case 'restore':
		$id = array_shift($argv);
		if (!$id) {
			die($help);
		}
		$backup = new \App\Processes\BackupReport($id);
		if ($backup->restore()) {
			echo "Rapport restauré avec succès.\n";
		} else {
			echo "Restauration annulée. Vérifiez qu'elle n'existe pas déjà et la sauvegarde existe.\n";
			exit(1);
		}
		break;
	default:
		if ($action) {
			echo "Unknown action.\n";
		}
		echo $helpMessage;
		break;
}
