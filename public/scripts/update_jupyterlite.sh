#!/bin/bash
set -e
LNB_DIR=$(realpath $(dirname $(realpath $0))/../..)
gotolabnbook() {
	cd $LNB_DIR
}


init_or_update_ext() {
	if [ ! -d "$1" ]; then
		git clone https://gricad-gitlab.univ-grenoble-alpes.fr/labnbook/jupyter/$1.git
	else
		cd $1
		git stash
		git switch main
		git pull origin main
		cd ..
	fi
}

echo "--------------------------"
echo "Téléchargement des sources"
echo "--------------------------"
gotolabnbook
mkdir -p ../jupyterLab-extensions/
cd ../jupyterLab-extensions/
for ext in lnb_ro_init lnb_ro_synchronize lnb_ro_theme lnb_rw_init lnb_rw_save lnb_rw_theme lnb_rw_undoredo; do
	init_or_update_ext $ext
done
