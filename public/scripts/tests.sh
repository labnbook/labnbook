#!/bin/bash

DIR=$(dirname $0)
BASEDIR="$DIR/../.."
lnb="$BASEDIR/lnb"

cleanup() {
    echo "## configure LabNBook back"
    if [ ! -z "$PID" ]; then
        kill $PID
    fi
    cp -f $BASEDIR/.env.bak $BASEDIR/.env
	$lnb cache

    echo "## enhance report"
    perl -i -pe 's/carousel slide/carousel/; s/wrap: true/wrap: false/; s/carousel-control\.right/carousel-control-next/; s/carousel-control\.left/carousel-control-prev/;' $BASEDIR/tests/_output/*/index.html
}

if [ ! -f "artisan" ]; then
	echo "This script should be run at the project's root"
	exit 1;
fi

trap cleanup INT

echo "## Configuring labnbook test server"
cp -f $BASEDIR/.env $BASEDIR/.env.bak
cp -f $BASEDIR/.env.test $BASEDIR/.env
$lnb cache

$lnb artisan serve --quiet --port 8003 --env .env.test > /dev/null 2>&1 &
PID=$!

echo "## clean leftovers from the previous run"
rm -fr $BASEDIR/tests/_output/*

echo "## run tests"
export LANGUAGE="fr_FR-utf8"
php $BASEDIR/vendor/bin/codecept run "$@"
ret=$?

cleanup
exit $ret
