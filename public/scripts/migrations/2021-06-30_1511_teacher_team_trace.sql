ALTER TABLE `trace_action` CHANGE `action_type` `action_type` ENUM('administration','production','information','communication','t_mission','t_student','t_report','m_teacher') CHARACTER SET ascii COLLATE ascii_bin NOT NULL;

INSERT INTO trace_action (id_action,action,action_type) VALUES (51,'add_teacher_status','m_teacher');
INSERT INTO trace_action (id_action,action,action_type) VALUES (52,'remove_teacher_status','m_teacher');
