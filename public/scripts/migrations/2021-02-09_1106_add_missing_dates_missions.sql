UPDATE mission AS m
SET creation_date=(
    SELECT MIN(creation_time)
    FROM labdoc
    WHERE id_report_part
    IN (
        SELECT id_report_part
        FROM report_part as rp
        WHERE rp.id_mission=m.id_mission
        )
    )
WHERE creation_date IS NULL;

UPDATE mission SET creation_date=modif_date WHERE creation_date IS NULL AND modif_date IS NOT NULL;
UPDATE mission SET creation_date=NOW() WHERE creation_date IS NULL;
UPDATE mission SET modif_date=creation_date WHERE modif_date IS NULL;