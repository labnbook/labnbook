ALTER TABLE mission
CHANGE COLUMN status status ENUM('public', 'private', 'archive') COLLATE ascii_bin NOT NULL DEFAULT 'private'
;
