-- user role
ALTER TABLE trace
  ADD COLUMN id_mission INT unsigned NULL AFTER id_user
, ADD COLUMN user_role ENUM('learner','teacher','researcher','admin') COLLATE ascii_bin NULL AFTER id_user
;

-- guess mission from report
UPDATE trace JOIN report ON trace.id_report = report.id_report
  SET trace.id_mission = report.id_mission
;

-- user role: admin is hard-coded
UPDATE trace SET trace.user_role = 'admin' WHERE id_user = 1;

-- user role: teacher/researcher in mission
UPDATE trace
  JOIN report r ON trace.id_report = r.id_report
  JOIN link_mission_teacher lmt ON r.id_mission = lmt.id_mission
  JOIN teacher t ON trace.id_user = t.id_teacher AND lmt.id_teacher = t.id_teacher
  SET trace.user_role = IF(t.researcher, 'researcher', 'teacher')
  WHERE trace.user_role IS NULL
;

-- user role: remaining users are learners
UPDATE trace SET trace.user_role = 'learner' WHERE trace.user_role IS NULL;

-- user role: now NOT NULL is possible
ALTER TABLE trace
  CHANGE COLUMN user_role user_role ENUM('learner','teacher','researcher','admin') COLLATE ascii_bin NOT NULL
;



ALTER TABLE trace_action
  ADD COLUMN action_type ENUM('administration','production','information','collaboration') COLLATE ascii_bin NULL AFTER `action`
;

UPDATE trace_action
  SET action_type = CASE id_action
    WHEN 1 THEN 'administration'
    WHEN 2 THEN 'administration'
    WHEN 3 THEN 'administration'
    WHEN 4 THEN 'administration'
    WHEN 5 THEN 'production'
    WHEN 6 THEN 'production'
    WHEN 7 THEN 'production'
    WHEN 8 THEN 'production'
    WHEN 9 THEN 'production'
    WHEN 10 THEN 'production'
    WHEN 11 THEN 'production'
    WHEN 12 THEN 'production'
    WHEN 13 THEN 'production'
    WHEN 14 THEN 'information'
    WHEN 15 THEN 'information'
    WHEN 16 THEN 'information'
    WHEN 17 THEN 'information'
    WHEN 18 THEN 'information'
    WHEN 19 THEN 'information'
    WHEN 20 THEN 'information'
    WHEN 21 THEN 'information'
    WHEN 22 THEN 'collaboration'
    WHEN 23 THEN 'collaboration'
  END
;

ALTER TABLE trace_action
  CHANGE COLUMN action_type action_type ENUM('administration','production','information','collaboration') COLLATE ascii_bin NOT NULL
;
