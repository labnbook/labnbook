-- 28/12/2017 : reprise de l'outil commentaires
ALTER TABLE `labdoc_status` ADD COLUMN `com_viewed` TIMESTAMP NULL DEFAULT NULL AFTER `extend`;
ALTER TABLE `comment` DROP FOREIGN KEY `com_isanswer`;
ALTER TABLE `comment` DROP COLUMN `status`, DROP COLUMN `is_answer_to`, DROP INDEX `is_answer_to` ;
DROP TABLE `link_comment_user`;
