UPDATE user SET inst_number = NULL WHERE inst_number = '';

CREATE UNIQUE INDEX instid_instnumber ON user (id_inst, inst_number);

DROP INDEX id_inst ON user;
