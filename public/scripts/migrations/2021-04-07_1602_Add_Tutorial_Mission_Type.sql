ALTER TABLE mission MODIFY COLUMN `status` enum('public','private','archive', 'tutorial') CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT 'private';

ALTER TABLE report MODIFY COLUMN `status` enum('new','on','wait','arc','test', 'tuto') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'new';
