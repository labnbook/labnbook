CREATE TABLE `labdoc_backup` (
  `id_labdoc` INT(10) NOT NULL,
  `last_edition` INT(11) NULL,
  `content` LONGTEXT NULL,
  `content_hash` CHAR(32) COLLATE ascii_bin NOT NULL,
  PRIMARY KEY (`id_labdoc`, `content_hash`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;
