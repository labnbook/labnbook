CREATE TABLE IF NOT EXISTS team_config (
  id_team_config INT UNSIGNED AUTO_INCREMENT
, id_mission INT unsigned NOT NULL
, id_class INT unsigned NOT NULL
, size_opt SMALLINT unsigned NULL
, size_max SMALLINT unsigned NULL
, size_min SMALLINT unsigned NULL
, teams_max SMALLINT unsigned NULL
, `start_date` date DEFAULT NULL
, `end_date` date DEFAULT NULL
, `send_teacher` tinyint(1) NOT NULL DEFAULT 1
, `send_team` tinyint(1) NOT NULL DEFAULT 1
, `send_class` int(10) NOT NULL DEFAULT 0
, `send_mission` int(10) NOT NULL DEFAULT 0
, `send_ld` tinyint(1) NOT NULL DEFAULT 0
, `save_ld` tinyint(1) NOT NULL DEFAULT 0
, PRIMARY KEY(id_team_config)
, FOREIGN KEY (id_mission) REFERENCES mission (id_mission) ON DELETE CASCADE ON UPDATE CASCADE
, FOREIGN KEY (id_class) REFERENCES class (id_class) ON DELETE CASCADE ON UPDATE CASCADE
);
