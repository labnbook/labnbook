ALTER TABLE `message` 
CHANGE COLUMN `msg_time` `msg_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ;

ALTER TABLE `link_conversation_user` 
CHANGE COLUMN `last_seen` `last_seen` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ;

ALTER TABLE `annotation` 
ADD COLUMN `creation_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `type`,
CHANGE COLUMN `modif_date` `update_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ;

UPDATE annotation SET creation_time = update_time;

ALTER TABLE `annotation` 
CHANGE COLUMN `update_time` `update_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ;

ALTER TABLE `user` 
CHANGE COLUMN `last_visit` `last_synchro` INT(11) NULL DEFAULT NULL ;

ALTER TABLE `link_report_learner` 
ADD COLUMN `last_synchro` INT(11) NULL DEFAULT NULL AFTER `last_visit`;

UPDATE link_report_learner SET last_synchro = UNIX_TIMESTAMP(last_visit);

ALTER TABLE `link_report_learner` 
DROP COLUMN `last_visit`;
