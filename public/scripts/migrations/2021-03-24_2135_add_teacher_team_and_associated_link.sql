ALTER TABLE `teacher` CHANGE `current_interface` `current_interface` VARCHAR(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;

-- If #543 was not merged, the column `role` does not exists
ALTER TABLE `teacher` ADD COLUMN IF NOT EXISTS `role` enum('teacher', 'manager', 'admin') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'teacher';
-- Else it exists but might only containsmight only contains  and `manager`
ALTER TABLE `teacher` CHANGE COLUMN `role` `role` enum('teacher', 'manager', 'admin') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'teacher';


CREATE TABLE teacher_team (
      `id_teacher_team` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
      `id_inst` int(11),
      `name` varchar(64) NOT NULL,
      `status` enum('normal','archive') CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT 'normal',
      `update_time` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
      `creation_time` timestamp NULL DEFAULT NULL,
      PRIMARY KEY (`id_teacher_team`),
      FOREIGN KEY (`id_inst`) REFERENCES `institution` (`id_inst`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE link_mission_tteam (
    `id_mission` int (10) UNSIGNED NOT NULL,
    `id_teacher_team` int (10) UNSIGNED NOT NULL,
    PRIMARY KEY ( `id_mission`,`id_teacher_team` ),
    FOREIGN KEY (`id_mission`) REFERENCES `mission` (`id_mission`) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (`id_teacher_team`) REFERENCES `teacher_team` (`id_teacher_team`) ON DELETE CASCADE ON UPDATE CASCADE
)  ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE link_class_tteam (
      `id_class` int (10) UNSIGNED NOT NULL,
      `id_teacher_team` int (10) UNSIGNED NOT NULL,
      PRIMARY KEY ( `id_class`,`id_teacher_team` ),
      CONSTRAINT `lctt_id_class` FOREIGN KEY (`id_class`) REFERENCES `class` (`id_class`) ON DELETE CASCADE ON UPDATE CASCADE,
      CONSTRAINT `lctt_id_teacher_team` FOREIGN KEY (`id_teacher_team`) REFERENCES `teacher_team` (`id_teacher_team`) ON DELETE CASCADE ON UPDATE CASCADE
)  ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE link_teacher_tteam (
    `id_teacher` int (10) UNSIGNED NOT NULL,
    `id_teacher_team` int (10) UNSIGNED NOT NULL,
    `status` enum('member','manager') CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT 'member',
    PRIMARY KEY ( `id_teacher`,`id_teacher_team`, `status` ),
    CONSTRAINT `lttt_id_teacher` FOREIGN KEY (`id_teacher`) REFERENCES `teacher` (`id_teacher`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `lttt_id_teacher_team` FOREIGN KEY (`id_teacher_team`) REFERENCES `teacher_team` (`id_teacher_team`) ON DELETE CASCADE ON UPDATE CASCADE
)  ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

