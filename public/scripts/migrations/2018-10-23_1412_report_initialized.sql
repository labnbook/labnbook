ALTER TABLE report
  ADD COLUMN initialized BOOLEAN NOT NULL DEFAULT 0 AFTER `status`
;

UPDATE report SET initialized = 1 WHERE `status` NOT IN ('new', 'test');
