START TRANSACTION;
ALTER TABLE `labdoc` ADD COLUMN `tmp_deleted` TIMESTAMP NULL DEFAULT NULL AFTER `deleted`;
UPDATE `labdoc` SET `tmp_deleted` = FROM_UNIXTIME(`last_edition`) WHERE `deleted` = 1;
ALTER TABLE `labdoc` DROP `deleted`;
ALTER TABLE `labdoc` CHANGE `tmp_deleted` `deleted` TIMESTAMP NULL DEFAULT NULL AFTER `draft`; 
COMMIT;