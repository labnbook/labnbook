CREATE TABLE `scoring_var` (
	 `id_user` int(10) UNSIGNED NOT NULL,
	 `visible` tinyint(1) NOT NULL DEFAULT 1,
	 `pos_x` smallint(6) UNSIGNED NOT NULL,
	 `pos_y` smallint(6) UNSIGNED NOT NULL,
	 `width` smallint(6) UNSIGNED NOT NULL,
	 `height` smallint(6) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
