CREATE TABLE labdocfile (
  filehash BINARY(20) NOT NULL,
  id_labdoc INT unsigned NOT NULL,
  id_mission INT unsigned NOT NULL,
  uploadtime BIGINT NOT NULL,
  PRIMARY KEY(filehash, id_labdoc),
  KEY id_labdoc (id_labdoc),
  KEY id_mission (id_mission)
)
