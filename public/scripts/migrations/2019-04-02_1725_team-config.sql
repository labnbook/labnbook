-- Split import_ld into allow_import (boolean) and allow_import_id_mission (FK on mission.id_mission)
ALTER TABLE team_config
  ADD COLUMN allow_import BOOLEAN NOT NULL COMMENT 'Allow the import of labdocs. If true, may be bound to a mission with allow_import_id_mission' AFTER save_ld
;
UPDATE team_config SET allow_import = (import_ld >= 0);
ALTER TABLE team_config
  CHANGE COLUMN import_ld allow_import_id_mission INT(10) NULL
;
UPDATE team_config SET allow_import_id_mission = NULL WHERE allow_import_id_mission <= 0;
ALTER TABLE team_config
  CHANGE COLUMN allow_import_id_mission allow_import_id_mission INT(10) UNSIGNED NULL DEFAULT NULL COMMENT 'Limit LD import to a single mission'
, ADD INDEX allow_import_id_mission (allow_import_id_mission)
, ADD FOREIGN KEY (allow_import_id_mission) REFERENCES mission (id_mission) ON DELETE SET NULL ON UPDATE CASCADE
;

-- Convert send_class into the FK allow_msg_id_class
ALTER TABLE team_config
  CHANGE COLUMN send_class allow_msg_id_class INT(10) UNSIGNED NULL DEFAULT NULL COMMENT 'authorize messages to class: NULL=none other=id_class'
;
UPDATE team_config SET allow_msg_id_class = NULL WHERE allow_msg_id_class = 0;
ALTER TABLE team_config
  ADD INDEX allow_msg_id_class (allow_msg_id_class)
, ADD FOREIGN KEY (allow_msg_id_class) REFERENCES class (id_class) ON DELETE SET NULL ON UPDATE CASCADE
;

-- Convert send_mission into the FK allow_msg_id_mission
ALTER TABLE team_config
  CHANGE COLUMN send_mission allow_msg_id_mission  INT(10) UNSIGNED NULL DEFAULT NULL COMMENT 'authorize messages to mission students: NULL=none other=id_mission'
;
UPDATE team_config SET allow_msg_id_mission = NULL WHERE allow_msg_id_mission = 0;
ALTER TABLE team_config
  ADD INDEX allow_msg_id_mission (allow_msg_id_mission)
, ADD FOREIGN KEY (allow_msg_id_mission) REFERENCES mission (id_mission) ON DELETE SET NULL ON UPDATE CASCADE
;


ALTER TABLE team_config
  ADD COLUMN `method` TINYINT(4) UNSIGNED NOT NULL DEFAULT 0 COMMENT '0=undef 1=student-choice 2=teacher-choice 3=random' AFTER `id_class`
, ADD COLUMN `email_on_create` BOOLEAN NOT NULL DEFAULT 0 COMMENT 'send an email to members when a team is created (boolean)' AFTER `name_prefix`
, ADD COLUMN update_time TIMESTAMP NULL DEFAULT NULL on update current_timestamp()
, CHANGE COLUMN send_teacher allow_msg_teacher BOOLEAN NOT NULL DEFAULT 1 COMMENT 'authorize messages to the class teachers (boolean)'
, CHANGE COLUMN send_team allow_msg_team BOOLEAN NOT NULL DEFAULT 1 COMMENT 'authorize messages to the other team members (boolean)'
, CHANGE COLUMN send_ld allow_attach_ld BOOLEAN NOT NULL DEFAULT 0 COMMENT 'authorize attaching LD to outgoing messages (boolean)'
, CHANGE COLUMN save_ld allow_save_ld BOOLEAN NOT NULL DEFAULT 0 COMMENT 'authorize saving into the report the LD received in messages (boolean)'
;

UPDATE team_config SET method = 1;


ALTER TABLE report
  ADD COLUMN id_team_config INT UNSIGNED NULL DEFAULT NULL AFTER id_mission
, ADD INDEX id_team_config (id_team_config)
, ADD FOREIGN KEY (id_team_config) REFERENCES team_config (id_team_config) ON DELETE SET NULL ON UPDATE CASCADE
;
