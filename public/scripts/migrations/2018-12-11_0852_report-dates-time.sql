-- table report

ALTER TABLE report
  ADD COLUMN start_datetime DATETIME NULL DEFAULT NULL AFTER start_date
, ADD COLUMN end_datetime DATETIME NULL DEFAULT NULL AFTER end_date
;

UPDATE report
  SET start_datetime = CONCAT(start_date, ' 00:00:00')
  WHERE start_date IS NOT NULL
;

UPDATE report
  SET end_datetime = CONCAT(end_date, ' 23:59:59')
  WHERE end_date IS NOT NULL
;

ALTER TABLE report
  DROP COLUMN start_date
, DROP COLUMN end_date
;

-- table team_config

ALTER TABLE team_config
  ADD COLUMN start_datetime DATETIME NULL DEFAULT NULL AFTER start_date
, ADD COLUMN end_datetime DATETIME NULL DEFAULT NULL AFTER end_date
;

UPDATE team_config
  SET start_datetime = CONCAT(start_date, ' 00:00:00')
  WHERE start_date IS NOT NULL
;

UPDATE team_config
  SET end_datetime = CONCAT(end_date, ' 23:59:59')
  WHERE end_date IS NOT NULL
;

ALTER TABLE team_config
  DROP COLUMN start_date
, DROP COLUMN end_date
;
