-- 11/01/2018 : modifs de la table report pour évolution du widget msg
ALTER TABLE `report` CHANGE COLUMN `group_name` `team_name` VARCHAR(32) NOT NULL DEFAULT '' AFTER `status`;
