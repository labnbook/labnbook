ALTER TABLE `report` ADD `allow_import` TINYINT(1) NOT NULL DEFAULT '1'  COMMENT 'Allows the import of labdocs. If true, may be bound to a mission with allow_import_id_mission' AFTER `allow_save_ld`;
ALTER TABLE `report` CHANGE `allow_import_id_mission` `allow_import_id_mission` INT(10) NULL DEFAULT '-1';

UPDATE `report`
SET `allow_import`= 1
WHERE `allow_import_id_mission` > 0;

UPDATE `report`
SET `allow_import`= 1, `allow_import_id_mission` = NULL
WHERE `allow_import_id_mission` = -1;

ALTER TABLE `report` CHANGE `allow_import_id_mission` `allow_import_id_mission` INT(10) UNSIGNED DEFAULT NULL;
# Not working
#ALTER TABLE `report` ADD CONSTRAINT `report_ibfk_2` FOREIGN KEY (`allow_import_id_mission`) REFERENCES `mission` (`id_mission`) ON DELETE SET NULL ON UPDATE CASCADE;
