CREATE TEMPORARY TABLE teamingreport (
    SELECT r.id_report, r.id_mission, MAX(lcl.id_class) AS id_class, r.creation_time
   	FROM report r
   	    JOIN link_report_learner lrl USING(id_report)
        JOIN link_class_learner lcl USING(id_user)
    WHERE r.id_team_config IS NULL
   	GROUP BY r.id_report
   	HAVING MAX(lcl.id_class) = MIN(lcl.id_class)
);

UPDATE report
    JOIN teamingreport r USING(id_report)
    JOIN team_config tc ON tc.id_mission = r.id_mission AND tc.id_class = r.id_class
SET report.id_team_config = tc.id_team_config
WHERE
    tc.method = 1
    AND tc.creation_time <= r.creation_time;
