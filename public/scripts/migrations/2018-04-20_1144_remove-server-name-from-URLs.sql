UPDATE ressource SET res_path = replace(res_path, 'http://labbook.imag.fr/', '/') WHERE res_path LIKE 'http://labbook.imag.fr/%';

UPDATE ressource SET res_path = replace(res_path, 'http://labbookprod.liglab.fr/', '/') WHERE res_path LIKE 'http://labbookprod.liglab.fr/%';

UPDATE labdoc
  SET labdoc_data = replace(replace(labdoc_data, 'http://labbook.imag.fr/', '/'), 'http://labbookprod.liglab.fr/', '/')
  WHERE labdoc_data LIKE '%http://labbook.imag.fr%'
;

UPDATE labdoc
  SET labdoc_data = replace(labdoc_data, 'http://labnbook.fr/', '/')
  WHERE labdoc_data LIKE '%http://labbook.fr%'
;
