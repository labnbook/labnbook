<?php

$r = Legacy\DB::query("SELECT id_labdoc, labdoc_data FROM labdoc WHERE labdoc_data LIKE '%src=_http://%'");
foreach ($r->fetchAll(PDO::FETCH_OBJ) as $row) {
	updateLabdoc($row);
}


function updateLabdoc($record) {
	$replacing = [];
	foreach (extractUrls($record->labdoc_data) as $url) {
		$replacing[" src=\"http://$url\""] = sprintf(' src="https://images.weserv.nl/?url=%s"', rawurlencode($url));
	}
	$record->labdoc_data = str_replace(array_keys($replacing), array_values($replacing), $record->labdoc_data);
	saveLabdoc($record);
}

function extractUrls($html) {
	$m = [];
	preg_match_all('#src="http://([^"]+)"#', $html, $m, PREG_PATTERN_ORDER);
	return array_unique($m[1]);
}

function saveLabdoc($record) {
	$changes = Legacy\DB::exec(
		"UPDATE labdoc SET labdoc_data = ? WHERE id_labdoc = ?",
		[$record->labdoc_data, $record->id_labdoc]
	);
	if ($changes !== 1) {
		echo "ERROR, {$record->id_labdoc} was not fixed.\n";
	}
}
