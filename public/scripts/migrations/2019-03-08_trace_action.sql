ALTER TABLE `trace_action` CHANGE COLUMN `action_type` `action_type` ENUM('administration', 'production', 'information', 'collaboration', 'communication', 'teacher', 't_mission', 't_student', 't_report') CHARACTER SET 'ascii' COLLATE 'ascii_bin' NOT NULL ;
UPDATE trace_action SET action_type = "communication" WHERE action_type = "collaboration";
UPDATE trace_action SET action_type = "t_mission" WHERE id_action >= 32 AND id_action <= 39;
UPDATE trace_action SET action_type = "t_student" WHERE id_action = 48 OR id_action = 49 OR (id_action >= 40 AND id_action <= 44);
UPDATE trace_action SET action_type = "t_report" WHERE id_action = 31 OR (id_action >= 45 AND id_action <= 47);
ALTER TABLE `trace_action` CHANGE COLUMN `action_type` `action_type` ENUM('administration', 'production', 'information', 'communication', 't_mission', 't_student', 't_report') CHARACTER SET 'ascii' COLLATE 'ascii_bin' NOT NULL ;
