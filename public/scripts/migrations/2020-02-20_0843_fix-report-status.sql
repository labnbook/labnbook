UPDATE report SET initialized = 1 WHERE status = "test";
UPDATE report SET status = 'on' WHERE status = "new" AND initialized = 1;
UPDATE report JOIN labdoc USING(id_report) SET initialized = 1 WHERE status = 'on' AND initialized = 0;
UPDATE report SET status = 'new' WHERE status = "on" AND initialized = 0;
UPDATE report JOIN labdoc USING(id_report) SET initialized = 1 WHERE status = 'wait' AND initialized = 0;
UPDATE report SET status = 'new' WHERE status = 'wait' AND initialized = 0;