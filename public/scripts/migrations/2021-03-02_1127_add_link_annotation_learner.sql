ALTER TABLE annotation ENGINE = InnoDB;

DROP TABLE IF EXISTS link_annotation_learner;
CREATE TABLE link_annotation_learner (
    `id_annotation` int(10) UNSIGNED NOT NULL,
    `id_user` int(10) UNSIGNED NOT NULL,
    PRIMARY KEY (`id_annotation`,`id_user`),
    FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (`id_annotation`) REFERENCES annotation (`id_annotation`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


INSERT IGNORE INTO link_annotation_learner ( id_annotation, id_user )
SELECT DISTINCT TRIM(BOTH '"' FROM JSON_EXTRACT(CAST(attributes AS CHAR(255) CHARSET utf8), '$.id_annotation')), id_user
FROM trace
WHERE id_action = 14;
