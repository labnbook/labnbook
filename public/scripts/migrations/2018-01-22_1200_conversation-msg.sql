-- 22/01/18 : modifs de la table conversation pour évolution du widget msg
ALTER TABLE `conversation` ADD COLUMN `title` VARCHAR(45) NOT NULL DEFAULT '' AFTER `id_report`;
ALTER TABLE `conversation` DROP COLUMN `id_owner`, DROP INDEX `id_owner` ;
