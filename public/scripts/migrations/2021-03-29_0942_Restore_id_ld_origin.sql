UPDATE labdoc ld
JOIN labdoc ld_origin
	ON ld.id_report_part = ld_origin.id_report_part AND ld_origin.id_report IS NULL
	AND ld.type_labdoc = ld_origin.type_labdoc AND ld.name = ld_origin.name AND ld.editable_name = ld_origin.editable_name 
	AND ld.duplicatable = ld_origin.duplicatable AND ld.deleteable = ld_origin.deleteable AND ld.draggable = ld_origin.draggable
SET ld.id_ld_origin = ld_origin.id_labdoc
WHERE ld.id_report IS NOT NULL AND ld.id_ld_origin IS NULL AND ld.creation_time > "2020-03-11 00:00:00"
AND ld_origin.id_labdoc NOT IN ( -- do not update LD when multiple origin-LD have the same name in the same RP
	SELECT ld2.id_labdoc FROM labdoc ld2
	JOIN (
		SELECT id_report_part, name, COUNT(*)
		FROM labdoc
		WHERE id_report IS NULL
		GROUP BY id_report_part, name
		HAVING COUNT(*) > 1
	) ld3
	ON ld2.id_report_part = ld3.id_report_part AND ld2.name = ld3.name
	WHERE ld2.id_report IS NULL);
