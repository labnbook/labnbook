CREATE TABLE trace_action (
	id_action SMALLINT NOT NULL,
	action VARCHAR(255),
	PRIMARY KEY (id_action)
);

CREATE TABLE trace (
	id_trace BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
	id_action SMALLINT NOT NULL,
	action_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	id_user INT unsigned NOT NULL,
	id_report INT unsigned NULL,
	id_labdoc INT unsigned NULL,
	attributes VARBINARY(255),
	PRIMARY KEY (id_trace),
	KEY key_id_report (id_report),
	KEY key_id_action (id_action),
	FOREIGN KEY (id_action) REFERENCES `trace_action` (id_action) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO trace_action VALUES
  (1,  'connect_lnb')
, (2,  'disconnect_lnb')
, (3,  'enter_report')
, (4,  'leave_report')
, (5,  'submit_report')
, (6,  'add_ld')
, (7,  'duplicate_ld')
, (8,  'edit_ld')
, (9,  'modify_ld')
, (10, 'validate_ld')
, (11, 'toggle_ld_status')
, (12, 'update_ld_name')
, (13, 'delete_ld')
, (14, 'open_annotation')
, (15, 'open_rp_assignment')
, (16, 'open_assignment')
, (17, 'open_detailed_assignment')
, (18, 'open_resource')
, (19, 'add_resource')
, (20, 'open_comment')
, (21, 'add_comment')
, (22, 'open_conversation')
, (23, 'add_message')
;
