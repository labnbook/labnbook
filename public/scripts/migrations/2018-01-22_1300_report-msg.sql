-- 22/01/18 : modifs de la table report pour évolution du widget msg
ALTER TABLE `report`
   ADD COLUMN `send_teacher` TINYINT(1) NOT NULL DEFAULT '1' AFTER `com_restr_to_mission`,
   ADD COLUMN `send_team` TINYINT(1) NOT NULL DEFAULT '1' AFTER `send_teacher`,
   ADD COLUMN `send_class` INT(10) NOT NULL DEFAULT '0' AFTER `send_team`,
   ADD COLUMN `send_mission` INT(10) NOT NULL DEFAULT '0' AFTER `send_class`;
UPDATE report SET send_mission = id_mission
   WHERE (com_restr_to_mission = 1 AND ( communication_rights = 'all' OR communication_rights = 'class')) ;
UPDATE report SET send_team = 0, send_teacher = 0
   WHERE (communication_rights = 'none') ;
ALTER TABLE `report` CHANGE COLUMN `send_ld` `send_ld` TINYINT(1) NOT NULL DEFAULT '0' ;
ALTER TABLE `report` DROP COLUMN `communication_rights`;
ALTER TABLE `report` DROP COLUMN `com_restr_to_mission`;
