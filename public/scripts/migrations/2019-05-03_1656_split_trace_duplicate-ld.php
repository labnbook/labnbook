<?php

$traces = Legacy\DB::query("SELECT id_trace, attributes FROM trace WHERE id_action = ?", [Trace::DUPLICATE_LD]);
foreach ($traces as $t) {
	$id_trace = (int) $t['id_trace'];
	$attributes = json_decode($t['attributes']);
	if (empty($attributes->ld_dup)) {
		continue;
	}
	$isAttached = Legacy\DB::queryScalar(
		"SELECT 1 FROM labdoc WHERE id_report_part IS NULL AND position IS NULL AND id_labdoc = ?",
		[$attributes->ld_dup]
	);
	if ($isAttached) {
		$changes = Legacy\DB::exec(
			"UPDATE trace SET id_action = ? WHERE id_trace = ?",
			[Trace::SAVE_ATTACHED_LD, $id_trace]
		);
		if ($changes) {
			printf("Converted trace %d from DUPLICATE_LD TO SAVE_ATTACHED_LD\n", $id_trace);
		} else {
			printf("FAILED to convert trace %d from DUPLICATE_LD TO SAVE_ATTACHED_LD\n", $id_trace);
		}
	}
}