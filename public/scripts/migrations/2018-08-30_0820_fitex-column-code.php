<?php

libxml_use_internal_errors(true);
$r = Legacy\DB::query("SELECT id_labdoc, labdoc_data FROM labdoc WHERE type_labdoc = 'dataset'");
foreach ($r->fetchAll() as $row) {
	updateLabdoc($row);
}


function updateLabdoc($record) {
	//echo "ID " . $record['id_labdoc'] . "\n";
	$doc = new DOMDocument('1.0', 'utf-8');
	$doc->preserveWhiteSpace = false;
	if (empty($record['labdoc_data'])) {
		echo "No XML content in labdoc {$record['id_labdoc']}.\n";
		return;
	}
	try {
		$doc->loadXML($record['labdoc_data']);
	} catch (Exception $ex) {
		echo "Labdoc XML error, ID {$record['id_labdoc']}, " . $ex->getMessage() . "\n";
		return;
	}
	$renamed = updateXmlDocument($doc);
	$record['labdoc_data'] = stringifyXmlDocument($doc);
	if ($renamed) {
		$old = listGraphTags(array_keys($renamed));
		$new = listGraphTags(array_values($renamed));
		$record['labdoc_data'] = str_replace($old, $new, $record['labdoc_data']);
	}
	saveLabdoc($record);
}

function listGraphTags($names) {
	return array_merge(
		array_map(function($x) { return "<x>" . htmlspecialchars($x) . "</x>"; }, $names),
		array_map(function($x) { return "<y>" . htmlspecialchars($x) . "</y>"; }, $names)
	);
}


function updateXmlDocument($doc) {
	$renamed = [];
	$columns = $doc->getElementsByTagName('column');
	foreach ($columns as $col) {
		$descriptionNode = $col->getElementsByTagName('description')->item(0);
		$descriptionText = "";
		if ($descriptionNode) {
			$descriptionText = $descriptionNode->textContent;
			$col->removeChild($descriptionNode);
		}
		$nameNode = $col->getElementsByTagName('name')->item(0);
		if (!$nameNode) {
			continue;
		}
		$nameText = $nameNode->textContent;
		if ($descriptionText && strpos($descriptionText, "\n") === false) {
			$nameNode->textContent = $descriptionText;
		}
		$codeNode = $doc->createElement("code");
		$codeText = cleanupCode($nameText);
		$codeNode->textContent = $codeText;
		$col->insertBefore($codeNode, $nameNode);
		if ($codeText !== $nameText) {
			$renamed[$nameText] = $codeText;
		}
	}
	return $renamed;
}

function stringifyXmlDocument($doc) {
	$doc->formatOutput = false;
	return $doc->saveXML();
}

function saveLabdoc($record) {
	$changes = Legacy\DB::exec(
		"UPDATE labdoc SET labdoc_data = ? WHERE id_labdoc = ?",
		[$record['labdoc_data'], $record['id_labdoc']]
	);
	if ($changes !== 1) {
		echo "ERROR, {$record['id_labdoc']} was not fixed.\n";
	}
}

function cleanupCode($name) {
	return preg_replace(
		['/[^A-Za-z0-9]+/', '/^\d+/'],
		['_', ''],
		iconv('UTF-8', 'ASCII//TRANSLIT', $name)
	);
}
