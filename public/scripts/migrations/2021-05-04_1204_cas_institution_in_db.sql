ALTER TABLE institution ADD COLUMN id_class_cas INT(10) unsigned DEFAULT NULL;
ALTER TABLE institution ADD CONSTRAINT `institution_ibfk_1` FOREIGN KEY (`id_class_cas`) REFERENCES `class` (`id_class`) ON DELETE CASCADE ON UPDATE CASCADE;
-- Link to CAS institutions that where linked to cas from CODE in DB
UPDATE institution SET id_class_cas=2 WHERE id_inst IN (2,3,6);
