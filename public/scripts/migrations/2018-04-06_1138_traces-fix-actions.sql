UPDATE trace_action
  SET action_type = CASE id_action
    WHEN 1 THEN 'administration'
    WHEN 2 THEN 'administration'
    WHEN 3 THEN 'administration'
    WHEN 4 THEN 'administration'
    WHEN 5 THEN 'administration'
    WHEN 6 THEN 'production'
    WHEN 7 THEN 'production'
    WHEN 8 THEN 'production'
    WHEN 9 THEN 'production'
    WHEN 10 THEN 'production'
    WHEN 11 THEN 'production'
    WHEN 12 THEN 'production'
    WHEN 13 THEN 'production'
    WHEN 14 THEN 'information'
    WHEN 15 THEN 'information'
    WHEN 16 THEN 'information'
    WHEN 17 THEN 'information'
    WHEN 18 THEN 'information'
    WHEN 19 THEN 'collaboration'
    WHEN 20 THEN 'collaboration'
    WHEN 21 THEN 'collaboration'
    WHEN 22 THEN 'collaboration'
    WHEN 23 THEN 'collaboration'
	WHEN 24 THEN 'collaboration'
  END
;

