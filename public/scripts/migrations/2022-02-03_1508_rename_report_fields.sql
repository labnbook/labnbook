ALTER TABLE report
	CHANGE `send_teacher` `allow_msg_teacher` TINYINT(1) NOT NULL DEFAULT '1',
	CHANGE `send_team` `allow_msg_team` TINYINT(1) NOT NULL DEFAULT '1',
	CHANGE `send_class` `allow_msg_id_class` INT(10) NOT NULL DEFAULT '0',
	CHANGE `send_mission` `allow_msg_id_mission` INT(10) NOT NULL DEFAULT '0',
	CHANGE `send_ld` `allow_attach_ld` TINYINT(1) NOT NULL DEFAULT '0',
	CHANGE `save_ld` `allow_save_ld` TINYINT(1) NOT NULL DEFAULT '0',
	CHANGE `import_ld` `allow_import_id_mission` INT(10) NOT NULL DEFAULT '-1';


