ALTER TABLE `class` ADD COLUMN `update_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;
  
ALTER TABLE `class` ADD COLUMN `status` ENUM('normal', 'archive') DEFAULT 'normal' NOT NULL ;

UPDATE `class`
  SET status = 'archive'
  WHERE end_date < NOW()
;