UPDATE labdoc
  SET labdoc_data = REPLACE(labdoc_data, 'http://labbookprod.liglab.fr', 'https://uga.labnbook.fr')
  WHERE labdoc_data LIKE '%http://labbookprod.liglab.fr%'
;

UPDATE labdoc
  SET labdoc_data = REGEXP_REPLACE(labdoc_data, 'http://(latex\\.codecogs\\.com|upload\\.wikimedia\\.org|\\w+\\.imgur\\.com|blogs\\.futura-sciences\\.com|ahmedbouazzi\\.org)', 'https://\\1')
  WHERE labdoc_data LIKE '%src=_http://%'
;


UPDATE report_part
  SET assignment = REPLACE(assignment, 'http://latex.codecogs.com', 'https://latex.codecogs.com')
  WHERE assignment LIKE '%src=_http://latex.codecogs.com%'
;
