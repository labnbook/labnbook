ALTER TABLE `labdoc_status` 
	ADD `last_id_version_reviewed` BIGINT(20) UNSIGNED DEFAULT NULL
		COMMENT 'Last id_trace, corresponding to a labdoc version, that matches last user review'
		AFTER `com_viewed`;
