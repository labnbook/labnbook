UPDATE labdoc
SET
    labdoc_data = REPLACE(labdoc_data, '<graph><name/><xmin/><xmax/><ymin/><ymax/><plot><x/><y/><ux/><uy/></plot><plot><x/><y/><ux/><uy/></plot><plot><x/><y/><ux/><uy/></plot></graph></LD>\n', '</LD>\n')
WHERE
    type_labdoc = 'dataset'
    AND deleted = 0
    AND labdoc_data LIKE '%<graph><name/><xmin/><xmax/><ymin/><ymax/><plot><x/><y/><ux/><uy/></plot><plot><x/><y/><ux/><uy/></plot><plot><x/><y/><ux/><uy/></plot></graph></LD>\n'
;