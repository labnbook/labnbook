-- labdoc_stdalone was a MYISAM table while other tables where InnoDB
ALTER TABLE `labdoc_stdalone` ENGINE='InnoDB' COLLATE 'utf8_unicode_ci';

-- annotation: remove references to deleted records
UPDATE
  annotation a LEFT JOIN teacher t ON a.id_teacher = t.id_teacher
  SET a.id_teacher = NULL
  WHERE a.id_teacher IS NOT NULL AND t.id_teacher IS NULL
;
DELETE a
  FROM annotation a LEFT JOIN report r ON a.id_report = r.id_report
  WHERE a.id_report IS NOT NULL AND r.id_report IS NULL
;
DELETE a
  FROM annotation a LEFT JOIN report_part rp ON a.id_report_part = rp.id_report_part
  WHERE a.id_report_part IS NOT NULL AND rp.id_report_part IS NULL
;
DELETE a
  FROM annotation a LEFT JOIN labdoc l ON a.id_labdoc = l.id_labdoc
  WHERE a.id_labdoc IS NOT NULL AND l.id_labdoc IS NULL
;

-- FK : annotation
ALTER TABLE `annotation`
  ADD INDEX `id_teacher` (`id_teacher`)
, ADD INDEX `id_report` (`id_report`)
, ADD INDEX `id_report_part` (`id_report_part`)
, ADD INDEX `id_labdoc` (`id_labdoc`)
;
ALTER TABLE `annotation`
  ADD FOREIGN KEY (`id_teacher`) REFERENCES `teacher` (`id_teacher`) ON DELETE SET NULL ON UPDATE SET NULL
, ADD FOREIGN KEY (`id_report`) REFERENCES `report` (`id_report`) ON DELETE CASCADE ON UPDATE CASCADE
, ADD FOREIGN KEY (`id_report_part`) REFERENCES `report_part` (`id_report_part`) ON DELETE CASCADE ON UPDATE CASCADE
, ADD FOREIGN KEY (`id_labdoc`) REFERENCES `labdoc` (`id_labdoc`) ON DELETE CASCADE ON UPDATE CASCADE
;

-- handle the case where labdoc.id_report_part = 0
UPDATE labdoc SET position = 1 WHERE id_report_part = 0 AND position IS NULL ;
UPDATE labdoc SET id_report_part = NULL WHERE id_report_part = 0 ;

-- remove labdocs that should have been deleted with their report_part
DELETE l
  FROM labdoc l LEFT JOIN report_part rp ON l.id_report_part = rp.id_report_part
  WHERE rp.id_report_part IS NULL
;

-- labdoc: remove references to deleted parents
UPDATE
  labdoc l1 LEFT JOIN labdoc l2 ON l1.id_ld_origin = l2.id_labdoc
  SET  l1.id_ld_origin = NULL
  WHERE l1.id_ld_origin IS NOT NULL AND l2.id_labdoc IS NULL
;

-- labdoc: remove references to deleted users
UPDATE
  labdoc l LEFT JOIN user u ON l.last_editor = u.id_user
  SET l.last_editor = NULL
  WHERE l.last_editor IS NOT NULL AND u.id_user IS NULL
;

-- FK : labdoc
ALTER TABLE `labdoc`
  ADD INDEX `id_ld_origin` (`id_ld_origin`)
, ADD INDEX `last_editor` (`last_editor`)
;
ALTER TABLE `labdoc`
  CHANGE `last_editor` `last_editor` int(10) unsigned NULL AFTER `locked`
;
ALTER TABLE `labdoc`
  ADD FOREIGN KEY (`id_report_part`) REFERENCES `report_part` (`id_report_part`) ON DELETE CASCADE ON UPDATE CASCADE
, ADD FOREIGN KEY (`id_ld_origin`) REFERENCES `labdoc` (`id_labdoc`) ON DELETE SET NULL ON UPDATE SET NULL
, ADD FOREIGN KEY (`last_editor`) REFERENCES `user` (`id_user`) ON DELETE SET NULL ON UPDATE SET NULL
;

-- message: remove references to deleted users
ALTER TABLE `message`
  CHANGE `id_user_sender` `id_user_sender` int(10) unsigned NULL DEFAULT 1 AFTER `msg_time`
;
UPDATE
  message m LEFT JOIN user u ON m.id_user_sender = u.id_user
  SET  m.id_user_sender = NULL
  WHERE u.id_user IS NULL
;

-- message: remove references to deleted labdocs
DELETE m
  FROM message m LEFT JOIN labdoc l ON m.id_labdoc = l.id_labdoc
  WHERE m.id_labdoc IS NOT NULL AND l.id_labdoc IS NULL
;

-- FK : message
ALTER TABLE `message`
  ADD INDEX `id_labdoc` (`id_labdoc`)
;
ALTER TABLE `message`
  ADD FOREIGN KEY (`id_labdoc`) REFERENCES `labdoc` (`id_labdoc`) ON DELETE CASCADE ON UPDATE CASCADE
, ADD FOREIGN KEY (`id_user_sender`) REFERENCES `user` (`id_user`) ON DELETE SET NULL ON UPDATE SET NULL
;

-- FK : user
ALTER TABLE `user`
  ADD FOREIGN KEY (`id_inst`) REFERENCES `institution` (`id_inst`) ON DELETE CASCADE ON UPDATE CASCADE
;
