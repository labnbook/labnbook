SET NAMES utf8mb4;
CREATE TABLE `teacher_report_status` (
                                       `id_report` int(10) unsigned NOT NULL,
                                       `id_teacher` int(10) unsigned NOT NULL,
                                       `last_synchro` int(11) DEFAULT NULL,
                                       PRIMARY KEY (`id_report`,`id_teacher`),
                                       KEY `Teacher_Report_Status_FKIndex1` (`id_report`),
                                       KEY `Teacher_Report_Status_FKIndex2` (`id_teacher`),
                                       CONSTRAINT `trs_ibfk_1` FOREIGN KEY (`id_report`) REFERENCES `report` (`id_report`) ON DELETE CASCADE ON UPDATE CASCADE,
                                       CONSTRAINT `trs_ibfk_2` FOREIGN KEY (`id_teacher`) REFERENCES `teacher` (`id_teacher`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;