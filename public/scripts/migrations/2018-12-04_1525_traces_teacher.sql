INSERT IGNORE INTO trace_action
(id_action, action, action_type)
VALUES
  (31, 'follow_report', 'teacher')
, (32, 'test_mission', 'teacher')
, (33, 'modify_mission', 'teacher')
, (34, 'add_mission', 'teacher')
, (35, 'duplicate_mission', 'teacher')
, (36, 'delete_mission', 'teacher')
, (37, 'add_resource', 'teacher')
, (38, 'add_ld', 'teacher')
, (39, 'add_rp', 'teacher')
, (40, 'add_class', 'teacher')
, (41, 'delete_class', 'teacher')
, (42, 'add_student', 'teacher')
, (43, 'delete_student', 'teacher')
, (44, 'add_report', 'teacher')
, (45, 'edit_report', 'teacher')
, (46, 'delete_report', 'teacher')
, (47, 'annotate_report', 'teacher')
, (48, 'add_teaming', 'teacher')
, (49, 'create_user', 'teacher')
;

