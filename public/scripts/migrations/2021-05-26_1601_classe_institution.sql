ALTER TABLE `institution` DROP CONSTRAINT `institution_ibfk_1`;
ALTER TABLE `institution` CHANGE COLUMN `id_class_cas` `id_class_teacher` int(10) unsigned DEFAULT NULL;
ALTER TABLE institution ADD CONSTRAINT `institution_ibfk_1` FOREIGN KEY (`id_class_teacher`) REFERENCES `class` (`id_class`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `institution` ADD COLUMN `cas` BOOLEAN DEFAULT FALSE;
UPDATE institution SET cas=TRUE WHERE id_class_teacher IS NOT NULL;
