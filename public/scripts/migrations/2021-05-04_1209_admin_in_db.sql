-- If #543 was not merged, the column `role` does not exists
ALTER TABLE `teacher` ADD COLUMN IF NOT EXISTS `role` enum('teacher', 'manager', 'admin') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'teacher';
-- Else it exists but might only containsmight only contains  and `manager`
ALTER TABLE `teacher` CHANGE COLUMN `role` `role` enum('teacher', 'manager', 'admin') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'teacher';
-- Make user 1 admin
UPDATE teacher SET role='admin' WHERE id_teacher = 1;
