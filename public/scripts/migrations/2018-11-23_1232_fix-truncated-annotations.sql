UPDATE annotation SET `json` = REGEXP_REPLACE(`json`, ',"highlights":\\[.+\\],', ',');

ALTER TABLE annotation CHANGE COLUMN `json` `json` TEXT DEFAULT NULL;
