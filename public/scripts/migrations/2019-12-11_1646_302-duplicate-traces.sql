create table dups  select * from trace where id_action=49 AND attributes like '{"count"%';

CREATE TABLE id_to_remove SELECT DISTINCT  min(t1.id_trace) as min_id FROM dups t1 JOIN dups t2 ON t1.id_user = t2.id_user AND t2.id_trace = t1.id_trace +1 GROUP BY t1.id_trace, t2.id_trace;
ALTER TABLE id_to_remove ADD INDEX id (min_id);
DELETE FROM trace WHERE id_trace IN (SELECT min_id FROM id_to_remove);

-- Cleaning
DROP TABLE dups;
DROP TABLE id_to_remove;

-- ADD missing counts
UPDATE trace SET attributes = '{"count":1}' WHERE id_action = 49 AND attributes IS NULL;
