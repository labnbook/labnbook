ALTER TABLE `trace_action`
CHANGE `action_type` `action_type` enum('administration','production','information','collaboration','teacher') COLLATE 'ascii_bin' NOT NULL AFTER `action`
;

UPDATE trace_action SET action_type = 'teacher' WHERE `action` = 'follow_report';
