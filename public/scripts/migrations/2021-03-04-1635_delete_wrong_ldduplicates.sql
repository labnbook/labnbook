UPDATE labdoc
    JOIN report USING(id_report)
    JOIN report_part USING(id_report_part)
    SET labdoc.deleted = NOW()
    WHERE report.id_mission <> report_part.id_mission
