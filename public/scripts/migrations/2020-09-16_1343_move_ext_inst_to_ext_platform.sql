-- CREATE extplatform
CREATE TABLE `extplatform` (
	`id_extplatform` int(11) NOT NULL AUTO_INCREMENT,
	`id_inst` int(11) NOT NULL,
	`name` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
	`secret` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
	PRIMARY KEY (`id_extplatform`),
	UNIQUE KEY `extplatformname` (`name`),
	CONSTRAINT `extplatform_ibfk_1` FOREIGN KEY (`id_inst`) REFERENCES `institution` (`id_inst`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ;
ALTER TABLE `class` ADD COLUMN `id_extplatform` INT(11), ADD CONSTRAINT `class_ibfk_2` FOREIGN KEY (`id_extplatform`) REFERENCES `extplatform` (`id_extplatform`) ON DELETE CASCADE ON UPDATE CASCADE;
-- Populate extplatform from institution
INSERT INTO extplatform (id_inst, id_extplatform, name, secret)
	SELECT (SELECT id_inst FROM institution WHERE name = 'UGA'), id_inst as id_extplatform, name, secret
	FROM institution WHERE secret is NOT NULL;
-- UPDATE users
UPDATE user u SET u.id_inst = (SELECT id_inst FROM extplatform WHERE id_extplatform = u.id_inst) WHERE u.id_inst IN (SELECT id_extplatform FROM extplatform);
-- UPDATE classes
UPDATE class SET id_extplatform = id_inst WHERE id_inst IN (SELECT id_extplatform FROM extplatform);
UPDATE class c SET c.id_inst = (SELECT id_inst FROM extplatform WHERE id_extplatform = c.id_inst) WHERE c.id_inst IN (SELECT id_extplatform FROM extplatform);
RENAME TABLE link_ext_user_inst TO link_extplatform_user;
ALTER TABLE link_extplatform_user DROP CONSTRAINT link_ext_user_inst_id_inst_foreign;
ALTER TABLE link_extplatform_user DROP CONSTRAINT link_ext_user_inst_id_user_foreign;
ALTER TABLE link_extplatform_user CHANGE COLUMN `id_inst` id_extplatform INT(11) NOT NULL;
ALTER TABLE link_extplatform_user ADD CONSTRAINT `link_extplatform_user_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE;
ALTER TABLE link_extplatform_user ADD CONSTRAINT `link_extplatform_user_ibfk_2` FOREIGN KEY (`id_extplatform`) REFERENCES `extplatform` (`id_extplatform`) ON DELETE CASCADE;
-- DELETE institutions
DELETE FROM institution WHERE secret IS NOT NULL;
-- DELETE secret
ALTER TABLE institution DROP COLUMN secret;
