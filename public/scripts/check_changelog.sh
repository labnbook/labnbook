#!/bin/bash
main="origin/master"
lastMergeTimestamp=$(git log  --pretty=format:"%ct %s" --merges --first-parent $main --  | grep -v 'Merge remote-tracking' | head -n 1 | awk '{print $1}')
changelogTimestamp=$(stat -c %Y CHANGELOG)
if [ "$lastMergeTimestamp" -gt "$changelogTimestamp" ]; then
    echo "There is a merge more recent than the CHANGELOG, please update it"
    exit 1
fi
