#!/bin/bash

if [ ! -f "artisan" ]; then
	echo "This script should be run at the project's root"
	exit 1;
fi

DIR=$(dirname $0)
$DIR/../../lnb update
$DIR/../../lnb syntax
$DIR/../../lnb tests -f
$DIR/check_translations.sh
$DIR/check_changelog.sh
