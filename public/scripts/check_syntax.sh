#!/bin/bash

echo "## Running syntax check (php)"
for file in $(find app/ resources/ tests/ public/ -iname '*.php'); do
    out=$(php -l $file)
    ret=$?
    if [ $ret -ne 0 ]; then
        echo "$out"
        echo "Syntax error on $file"
        exit $ret
    fi
done

echo "## Running syntax check (javascript)"
for file in $(find public/js public/tool* -iname '*.js'); do
    out=$(./node_modules/acorn/bin/acorn  --ecma2021 --silent $file)
    ret=$?
    if [ $ret -ne 0 ]; then
        echo "$out"
        echo "Syntax error on $file"
        exit $ret
    fi
done
