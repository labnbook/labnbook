<?php

require_once dirname(__DIR__) . '/common/default.php';

if (PHP_SAPI !== 'cli') {
	die("CLI only!");
}

fixDrawings();

/**
 * Fix drawings, see https://gricad-gitlab.univ-grenoble-alpes.fr/labnbook/labnbook/issues/45
 */
function fixDrawings() {
	$maxId = (int) Legacy\DB::queryScalar("SELECT MAX(id_labdoc) FROM labdoc");
	$batchSize = 500;
	$count = 0;
	for ($min = 0; $min < $maxId; $min += $batchSize) {
		$max = $min + $batchSize - 1;
		$labdocs = Legacy\DB::query(
			"SELECT rp.id_mission, l.id_labdoc, l.labdoc_data"
			. " FROM labdoc l JOIN report_part rp USING (id_report_part)"
			. " WHERE l.id_labdoc BETWEEN $min AND $max AND type_labdoc IN ('drawing')"
			. " AND labdoc_data LIKE '%tool_zwibbler/image_mgmt.php%' AND labdoc_data NOT LIKE '%&id_mission=%'"
		);
		while ($labdoc = $labdocs->fetch(PDO::FETCH_ASSOC)) {
			printf(
				"- correction du dessin dans le labdoc %d, mission %d\n",
				$labdoc['id_labdoc'],
				$labdoc['id_mission']
			);
			$count += (int) fixImgUrl((int) $labdoc['id_mission'], (int) $labdoc['id_labdoc'], $labdoc['labdoc_data']);
		}
	}
	echo "$count labdocs changed";
}

/**
 * @param int $idMission
 * @param int $idLabdoc
 * @param string $content
 * @return boolean Changed?
 */
function fixImgUrl($idMission, $idLabdoc, $content) {
	// "url":"../tool_zwibbler/image_mgmt.php?action=get&fileid=3791_2018-03-05_18-41-45.png&id_mission=502"
	if (preg_match('#tool_zwibbler/image_mgmt.php\?action=get&fileid=([\w.-]+)"#', $content, $m)) {
		printf("    replacing %s with %s\n", $m[0], "tool_zwibbler/image_mgmt.php?action=get&fileid={$m[1]}&id_mission=$idMission\"");
	}
	$newContent = preg_replace(
		'#tool_zwibbler/image_mgmt.php\?action=get&fileid=([\w.-]+)"#',
		"tool_zwibbler/image_mgmt.php?action=get&fileid=\$1&id_mission=$idMission\"",
		$content
	);
	if (strlen($content) === strlen($newContent)) {
		echo "    Erreur, aucun remplacement.\n";
		return false;
	}
	Legacy\DB::exec(
		"UPDATE labdoc SET labdoc_data = ? WHERE id_labdoc = ?",
		[$newContent, $idLabdoc]
	);
	return true;
}
