#!/bin/bash
if [ -z "$1" ]; then
	echo "usage $0 fromDate [toDate]"
	echo -e "\t ex $0 20230609"
	echo -e "\t toDate defaults to now"
	exit 1
fi

from=$(date -d "$1" +%s)
if [ ! -z "$2" ]; then
	to=$(date -d "$2" +%s)
else
	to=$(date +%s)
fi
basedir="$(dirname $0)/../.."
curdate=""
fix=0
add=0
chg=0
del=0
sec=0
while read line; do
	if [[ $line =~ ^##.([^ ]*).*$ ]]; then
		match=${BASH_REMATCH[1]}
		# echo $match
		curdate=$(date -d $match +%s 2> /dev/null)
	elif [ ! -z "$curdate" ]; then
		if [ $curdate -ge $from ] && [ $curdate -le $to ];
		then
			if [[ $line =~ ^\+( |	)+(NAN|[0-9]+)( |	)+(FIX|ADD|CHG|DEL|SEC).*$ ]]; then
				# echo "${BASH_REMATCH[2]} is as ${BASH_REMATCH[4]}"
				case ${BASH_REMATCH[4]} in
					"FIX")
						fix=$(( $fix + 1 ))
						;;
					"ADD")
						add=$(( $add + 1 ))
						;;
					"CHG")
						chg=$(( $chg + 1 ))
						;;
					"DEL")
						del=$(( $del + 1 ))
						;;
					"SEC")
						sec=$(( $sec + 1 ))
						;;
				esac
			elif [ ! -z "$line" ]; then
				echo "Bad line format '$line'"
			fi
		elif [ $curdate -lt $from ]; then
			break;
		fi
	fi
done < $basedir/CHANGELOG

total=$(echo $fix+$add+$chg+$del+$sec | bc )

fromFormat=$(date -d @$from)
toFormat=$(date -d @$to)
echo "$total tickets dans le changelog entre le $fromFormat et le $toFormat"
echo -e "\t$add Ajouts"
echo -e "\t$chg Modifications"
echo -e "\t$fix Correctifs"
echo -e "\t$sec Correctifs de sécurité"
echo -e "\t$del Suppression de fonctionnalités"
