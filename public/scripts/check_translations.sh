#!/bin/bash

public/scripts/genTranslationJson > /dev/null  2>&1
ret=$?
if [ $ret -ne 0 ]; then
	echo "Erreur lors de la génération des traductions"
	exit $ret
fi
php public/scripts/check_translations.php
ret=$?
if [ $ret -ne 0 ]; then
	echo "$ret erreurs de traductions"
	exit 1
fi
echo "Les traductions sont à jour"
