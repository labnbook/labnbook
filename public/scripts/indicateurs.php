<?php

require_once dirname(__DIR__ ) . '/common/default.php';

if (PHP_SAPI !== 'cli') {
	echo "CLI only";
	exit();
}

/**
 * array [filename => SQL]
 */
$queries = [
	// les missions IDEX et leurs enseignants IDEX + l'activité des enseignants
	"missions_enseignants" => <<<END_OF_SQL
SELECT
  m.code AS code_mission,
  m.id_mission,
  m.name AS mission,
  u.first_name AS prenom,
  u.user_name AS nom,
  FROM_UNIXTIME(u.last_synchro, '%Y-%m-%d') AS der_visite,
  COUNT(t.id_trace) AS num_connexions
FROM mission m
  JOIN link_mission_teacher lmt USING(id_mission)
  JOIN link_class_learner lcl ON lcl.id_user = lmt.id_teacher AND lcl.id_class IN (46,162)  -- les enseignants des classes IDEX
  JOIN user u USING(id_user)
  LEFT JOIN trace t ON t.id_user = u.id_user AND t.id_action = 1  -- trace connect_lnb
GROUP BY m.id_mission, u.id_user
ORDER BY m.code ASC, num_connexions DESC, u.user_name ASC
END_OF_SQL
	,
	// les missions IDEX avec leurs caractéristiques déterminées par le concepteur
	"missions_conception" => <<<END_OF_SQL
SELECT
  -- mission properties
  m.code, m.id_mission, m.name, m.creation_date, m.modif_date,
  -- teacher content in each mission
  def.num_report_parts,
  COUNT(ressource.id_ressource) AS num_ressources,
  def.allows_procedure, def.allows_text, def.allows_dataset, def.allows_drawing, def.allows_code,
  def_num_procedure, def_num_text, def_num_dataset, def_num_drawing, def_num_code
FROM mission m
  JOIN link_mission_teacher lmt USING(id_mission)
  JOIN link_class_learner lcl ON lcl.id_user = lmt.id_teacher AND lcl.id_class IN (46,162)  -- les enseignants des classes IDEX
  LEFT JOIN ressource ON ressource.id_report IS NULL AND ressource.id_mission = m.id_mission
  JOIN (
    SELECT
      rp.id_mission,
      COUNT(*) AS num_report_parts,
      MAX(rp.procedure) AS allows_procedure,
      MAX(rp.`text`) AS allows_text,
      MAX(rp.dataset) AS allows_dataset,
      MAX(rp.drawing) AS allows_drawing,
      MAX(rp.code) AS allows_code,
      SUM(l.type_labdoc = 'procedure') AS def_num_procedure,
      SUM(l.type_labdoc = 'text') AS def_num_text,
      SUM(l.type_labdoc = 'dataset') AS def_num_dataset,
      SUM(l.type_labdoc = 'drawing') AS def_num_drawing,
      SUM(l.type_labdoc = 'code') AS def_num_code
    FROM report_part rp
      JOIN labdoc l ON l.id_report IS NULL AND l.deleted IS NULL AND l.id_report_part = rp.id_report_part
    GROUP BY rp.id_mission
    ) def ON def.id_mission = m.id_mission
GROUP BY m.id_mission
ORDER BY m.code
END_OF_SQL
	,
	// les missions IDEX avec leurs usages (du côté étudiant et enseignant)
	"missions_usages" => <<<END_OF_SQL
SELECT
  -- mission properties
  m.code, m.id_mission, m.name,
  -- teacher content in each mission
  count(DISTINCT a.id_annotation) AS teacher_annotations,
  IFNULL(msg.from_teacher, 0) AS teacher_msg,
  -- student count
  COUNT(DISTINCT r.id_report) AS num_reports,
  COUNT(DISTINCT lrl.id_user) AS num_students,
  AVG(session.num) AS st_avg_sessions,
  ROUND(AVG(session.duration)) AS st_avg_days,
  STDDEV(session.duration) AS st_dev_days,
  -- student content in each mission
  COUNT(ressource.id_ressource) AS st_ressources,
  IFNULL(msg.from_student, 0) AS st_msg,
  IFNULL(activity.num_procedure, 0) AS st_procedure,
  IFNULL(activity.num_text, 0) AS st_text,
  IFNULL(activity.num_dataset, 0) AS st_dataset,
  IFNULL(activity.num_drawing, 0) AS st_drawing,
  IFNULL(activity.num_code, 0) AS st_code
FROM mission m
  JOIN link_mission_teacher lmt USING (id_mission)
  JOIN link_class_learner lcl ON lcl.id_user = lmt.id_teacher AND (lcl.id_class IN (46,162))    -- les enseignants des classes IDEX
  LEFT JOIN ressource ON ressource.id_report IS NOT NULL AND ressource.id_mission = m.id_mission
  LEFT JOIN report r ON m.id_mission = r.id_mission AND r.status <> 'test'
  LEFT JOIN (
    SELECT
      r.id_mission,
      SUM(l.type_labdoc = 'procedure') AS num_procedure,
      SUM(l.type_labdoc = 'text') AS num_text,
      SUM(l.type_labdoc = 'dataset') AS num_dataset,
      SUM(l.type_labdoc = 'drawing') AS num_drawing,
      SUM(l.type_labdoc = 'code') AS num_code
    FROM report r
    JOIN labdoc l ON l.id_report = r.id_report AND l.deleted IS NULL AND l.id_report_part IS NOT NULL -- only reports with user input
    GROUP BY r.id_mission
    ) activity ON activity.id_mission = r.id_mission
  LEFT JOIN link_report_learner lrl ON r.id_report = lrl.id_report
  LEFT JOIN annotation a ON a.id_report = r.id_report
  LEFT JOIN (
    SELECT
      report.id_mission,
      COUNT(id_teacher) AS from_teacher,
      (count(*) - COUNT(id_teacher)) AS from_student
    FROM trace
    JOIN trace_action USING(id_action)
    JOIN report USING(id_report)
    LEFT JOIN teacher ON trace.id_user = teacher.id_teacher
    WHERE action = 'add_message'
    GROUP BY report.id_mission
    ) msg ON msg.id_mission = m.id_mission
  LEFT JOIN (
    SELECT
      report.id_mission,
      trace.id_user,
      COUNT(*) AS num,
      (MAX(action_time) - MIN(action_time))/86400 AS duration
    FROM trace
    JOIN trace_action USING(id_action)
    JOIN report USING(id_report)
    WHERE action = 'enter_report'
    GROUP BY report.id_mission, trace.id_user
    ) session ON session.id_mission = m.id_mission
GROUP BY id_mission
ORDER BY code
END_OF_SQL
	,
];

$date = date('Y-m-d_h-i');
foreach ($queries as $name => $sql) {
	writeCsvFile("{$name}_{$date}.csv", $sql);
}

function writeCsvFile($fileName, $sql) {
	$file = fopen($fileName, "w");
	echo "CSV $fileName... ";
	writeCsv($file, $sql);
	fclose($file);
}
function writeCsv($fileHandle, $sql) {
	$q = Legacy\DB::query($sql);
	$headerInserted = false;
	echo $q->rowCount() . " lignes\n";
	while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
		if (!$headerInserted) {
			fputcsv($fileHandle, array_keys($row), ";");
			$headerInserted = true;
		}
		fputcsv($fileHandle, array_values($row), ";");
	}
}
