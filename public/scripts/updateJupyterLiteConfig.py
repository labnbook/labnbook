#!/usr/bin/python3

import sys
import json
from jsonmerge import Merger

filename = "jupyter-lite.json"
target_folder = "./lite_" + sys.argv[1] + "/"

with open(filename) as f:
    updated_json = json.load(f)

with open(target_folder + filename, "r+") as f:
    old_json = json.load(f)
    schema = {
		"properties": {
			"jupyter-config-data": {
				"mergeStrategy": "objectMerge"
			},
			"disabledExtensions": {
				"mergeStrategy": "objectMerge"
			}
		}
    }
    merger = Merger(schema)
    result = merger.merge(old_json, updated_json)
    f.seek(0)
    json.dump(result, f, indent=4)
    f.truncate()
