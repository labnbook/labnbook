<?php
// Ce script DOIT rester or de Laravel : si la requete puppeteer est emise dans
// le context laravel, l'execution bloque.

// Prepare args
$DIR = dirname(__DIR__);

$data = $_REQUEST;

$base_url = implode('', preg_filter('/APP_URL=(.*)\n/', '$1', file("{$DIR}/.env")));
$query = http_build_query($data['query']);
$idr = (int)$data['report'];
$url = $base_url.'/report/'.$idr.'/printPDF?' . $query;

$tmp = sys_get_temp_dir();
$filepath = $data['filepath'];
if (substr($filepath, 0, strlen($tmp)) != $tmp) {
	// Check that filepath has not been tempered
	logIntoFile("PDF Puppeteer bad filepath {$filepath} {$tmp}, aborting");
	leave();
}

date_default_timezone_set('Europe/Brussels');
$params = escapeshellarg(json_encode([
	'url' => $url,
	'filepath' => $filepath,
	'date' => date('d/m/Y - H:i:s'),
	'nom' => htmlspecialchars($data['nom']),
]));

$cmd = escapeshellcmd('node "' . __DIR__ . '/js/puppeteer/NodeJS_cmd_pup.js" ') . $params . " 2>&1";

if (file_exists($filepath)) {
	unlink($filepath);
}

$output = "";
$returnCode = 0;

if ($data['debug']) {
	logIntoFile("PDF Puppeteer [INFO]: shell command: " . $cmd);
}
exec($cmd, $output, $returnCode);
if ($returnCode !== 0) {
	logIntoFile("PDF Puppeteer [ERROR]: shell command failed with code $returnCode and output:\n" . join("\n", $output));
}

leave();

function leave()
{
	global $filepath;
	global $data;
	header("Content-Type: application/json");
	if (file_exists($filepath)) {
		echo json_encode([
			"success" => true,
			"filepath" => $filepath,
			"filename" => $data['nom'],
		]);
	} else {
		echo json_encode([
			"success" => false,
			"message" => "Erreur lors de l'export PDF",
		]);
	}
	die();
}

function logIntoFile($log)
{
	global $DIR;
	error_log($log, 3, $DIR.'/storage/logs/js_errors.log');
}
