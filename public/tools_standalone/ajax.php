<?php
require (dirname(__DIR__).'/common/boot_laravel.php');

require_once("../common/default.php");

// *****************    Fonctions   ****************************************************************************

function generateCode() {
	$caracteres = "ABCDEFGHiJKLMNPQRSTUVWXYZ123456789";
	$nb_car = strlen($caracteres);
	$code = "";
	//8 caractères aléatoires sur 34 pour faire un code (si 100 000 codes sont utilisés, on a une chance sur 18 millions de retrouver aléatoirement un code)
	for ($i = 0; $i < 4; $i++) {
		$al = mt_rand(0, ($nb_car - 1));
		$code .= $caracteres[$al];
	}
	$code .= "-";
	for ($i = 0; $i < 4; $i++) {
		$al = mt_rand(0, ($nb_car - 1));
		$code .= $caracteres[$al];
	}
	return $code;
}

// *****************    Gestion des requêtes AJAX     **********************************************************

if (isset($_GET['func'])) {
	$func = $_GET['func'];
	if ($func === "alerts") {
		echo commonHtmlAlerts();
	}
    if ($func === "check_code") {
        $code = $_POST["code"];
        echo (int)Legacy\DB::queryScalar("SELECT count(*) FROM labdoc_stdalone WHERE code = ?", [$code]);
    }
    if ($func === "save_LD") {
        $code = $_POST["code"];
        $content = $_POST["content"];
        if ($code) { // mise à jour d'un LD
            Legacy\DB::exec("UPDATE labdoc_stdalone SET labdoc_data = ? WHERE code = ?", [$content, $code]);
        } else { // création d'un nouveau LD
            // création du code aléatoire
            do {
                $code = generateCode();
                $exists = Legacy\DB::queryScalar("SELECT 1 FROM labdoc_stdalone WHERE code = ?", [$code]);
            } while ($exists);
            // création de l'entrée pour le labdoc
            Legacy\DB::exec("INSERT INTO labdoc_stdalone SET code = ?, type_labdoc = 'dataset', labdoc_data = ?", [$code, $content]);
        }
        echo $code;
    }
}
