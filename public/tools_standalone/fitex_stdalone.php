<?php
require_once("../common/default.php");

$fxstd_code = 0;
$ld_data = 0;
$warning = false;
$auto_fit = isset($_GET['auto']) ? $_GET['auto'] : 0;

if (isset($_POST["fxstd_code"])) { // recuperation du contenu du LD depuis la base
    $ld_data = Legacy\DB::queryScalar("SELECT labdoc_data FROM labdoc_stdalone WHERE code = ?", [$_POST["fxstd_code"]]);
    if (!$ld_data) {
        $warning = true;
    } else {
        $fxstd_code = $_POST["fxstd_code"];
    }
}
if (!$ld_data) { // récupération du ld par défaut
    $ld_data = file_get_contents('../tool_fitex/default-dataset.xml');
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <link rel="shortcut icon" type="image/png" href="favicon_fx.png"/>
    <link rel="icon" type="image/png" href="favicon_fx.png"/>
    <link rel="apple-touch-icon" href="favicon_fx.png"/>
    <title>Fitex - standalone version</title>

    <script>
        window.labnbook = { lang: <?= json_encode(\App\Helper::selectLanguage()); ?> };
        window.global_code = <?= $fxstd_code ? json_encode($fxstd_code) : 0 ?>;
        window.tool_standalone = 'fitex';
        window.ld_data = <?= json_encode($ld_data) ?>;
		window.auto_fit = <?= $auto_fit ?>;
        <?php if ($warning) {
            echo "alert('Code non trouvé dans la base');";
        }
        ?>
    </script>

    <!-- css -->
    <?= loadAsset("/css/common.css") ?>
    <?= loadAsset("/css/report.css") ?>
    <?= loadAsset("/tool_fitex/fitex.css") ?>
    <?= loadAsset("/tools_standalone/fitex_stdalone.css") ?>

    <!-- Libraries -->
    <?= loadAsset("/libraries/font_awesome/fontawesome-free-6.1.1-web/css/all.css") ?>
    <?= loadAsset("/libraries/jquery/jquery-2.2.4.min.js") ?>
    <?= loadAsset("/libraries/jquery/jquery-ui.min.js") ?>
    <?= loadAsset("/libraries/jquery/jquery-ui.min.css") ?>
    <?= loadAsset("/libraries/formJQuery/jquery.form.js") ?>
    <?= loadAsset("/libraries/ml-levenberg-marquardt/ml-levenberg-marquardt.js") ?>
	
    <!-- funcs -->
    <?= loadAsset("/libraries/i18next/i18next.js"); ?>
    <?= loadAsset("/libraries/i18next-xhr-backend/i18nextXHRBackend.js"); ?>
    <?= loadAsset("/js/common.js") ?>
    <?= loadAsset("/js/labdoc.js") ?>

    <!-- Fitex -->
    <?= loadAsset("/libraries/mathjs/math.min.js") ?>
    <?= loadAsset("/tool_fitex/libraries/jquery.flot.all.min.js") ?>
    <?= loadAsset("/tool_fitex/libraries/jquery.contextMenu.min.js") ?>
    <?= loadAsset("/tool_fitex/libraries/jquery.contextMenu.css") ?>
    <?= loadAsset("/tool_fitex/fitex_view.js") ?>
    <?= loadAsset("/tool_fitex/fitex_control.js") ?>
    <?= loadAsset("/tool_fitex/fitex_model.js") ?>
    <?= loadAsset("/tools_standalone/fitex_stdalone.js") ?>
    <?= loadAsset("/libraries/js-xlsx/xlsx.full.min.js") ?>
</head>

<body>
<div id="fxstd_header">

    <h1>
        <b>Fitex</b> : traitement des données expérimentales (un outil de la plateforme <a href="https://labnbook.fr" target="_blank">LabNBook</a>)
    </h1>

    <div id="page-alerts"></div>

    <div id="fxstd_info">Fitex a été développé<br/>pour Mozilla Firefox<br/>ou Google Chrome<br/></div>

    <table id="fxstd_main_menu">

        <tr title="Efface la feuille de données actuelle et ouvre une feuille de données">
            <td class="fxstd_menu_item fxstd_btn" onclick="toggleOpenMenu()">
                Ouvrir une feuille de données
            </td>
            <td></td>
        </tr>

        <tr class="fxstd_open_menu" title="Indiquez le code d'une de vos feuilles de calcul puis validez">
            <td class="fxstd_menu_item">
                <form id="form1" name="form1" method="post" action="fitex_stdalone.php">
                    - feuille de données existante - code :
                    <input name="fxstd_code" type="text" id="fxstd_code" size="10" maxlength="9"/>
                    <i class="fa fa-check fxstd_btn"  onclick="openLDWithCode()"></i>
                </form>
            </td>
            <td></td>
        </tr>

        <tr class="fxstd_open_menu">
            <td class="fxstd_menu_item fxstd_btn" onclick="openNewLD()">- feuille de données vierge</td>
        </tr>

        <tr>
            <td id="fxstd_saved_status" class="fxstd_menu_item fxstd_btn" onclick="toggleSave()">
                Sauvegarder la feuille de données actuelle
                <i class="fas fa-cloud-upload-alt fxstd-update"></i>
            </td>
            <td></td>
        </tr>

    <!-- <div class="fxstd_menu_item" onclick="uploadFile()">Ouvrir un fichier Fitex stocké sur votre ordinateur</div>
    <div class="fxstd_menu_item" onclick="saveFile('<?php // echo $fxstd_code ?>')">Enregistrer le fichier Fitex sur votre ordinateur</div>  -->

        <tr>
            <td class="fxstd_current_code">
                <span id="fxstd_current_code" onmouseover="$('#fxstd_help').show()" onmouseout="$('#fxstd_help').hide()">
                    Code de la feuille de données affichée : <b id="fxstd_code_code"></b>
                </span>
            </td>
            <td class="fxstd_current_code lnb_undoredo">
                <i class="fa fa-undo disabled" id="lnb_undo_1" title="Annuler la dernière modification"></i>&nbsp
                <i class="fa fa-redo disabled" id="lnb_redo_1" title="Rétablir la modification"></i>
            </td>
        </tr>
    </table>

    <div id="fxstd_help">
        <p>Le code permet de récupérer votre travail ultérieurement.</p><p> A conserver précieusement !</p>
    </div>
</div>

<div class='edited_labdoc_content edited_ld_dataset' id='labdoc_content_1' style='display: block'></div>

<!-- Add blade tooltip => need to be update when we modify the blade _tooltip-popup -->
<!-- tooltip d'information -->
<div id="tooltip_popup" class="read">
	<h1 id="tooltip_popup_title"></h1>
	<div style="text-align: justify" id="tooltip_popup_comment"></div>
</div>


</body>
</html>
