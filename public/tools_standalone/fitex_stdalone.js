window.global_save = false;
window.global_tab_ld = [];
window.global_edited_ld = 1;

$(document).ready(function() {

	$("#fxstd_code").val("");

	// initialize the main objet global_tab_ld[1]
	window.global_tab_ld[1] = {
		ld_type: "dataset",
		ld_history: [window.ld_data],
		auto_fit: window.auto_fit,
		ld_assignment_history: [],
		current_i: 0,
		fxstd_send_in_db : false,

		// add a listener on the value of global_tab_ld[1].send_in_db
		send_in_db_listener: function(val) {},
		set send_in_db(val) {
			this.fxstd_send_in_db = val;
			this.send_in_db_listener(val);
		},
		get send_in_db() {
			return this.fxstd_send_in_db;
		},
		registerListener: function(listener) {
			this.send_in_db_listener = listener;
		}
	};
	delete window.ld_data;
	window.global_tab_ld[1].registerListener(function() {
		refreshSaveIcon();
	});

	displayLDContent(1,1) ;

	// initialize the save mode
	if (window.global_code) {
		window.global_save = true;
		showCode();
	}
	refreshSaveIcon();
	setInterval(function(){
		if (window.global_save) {
			stdaloneUpdateLDinDB();
		}
	}, 20000) ;

	// prevents to submit the form on enter
	$(window).keydown(function(event){
		if(event.keyCode === 13) {
			event.preventDefault();
			return false;
		}
	});
});

/* ********************************************************
                    Displays
 **********************************************************/

/**
 * refresh the color and the title of the save line in the menu
 */
function refreshSaveIcon() {
	if (!window.global_save) {
		$("#fxstd_saved_status").attr("title",__("Sauvegarde désactivée - cliquer pour activer"));
		$("#fxstd_saved_status .fxstd-update").css("color","red") ;
	} else {
		if (window.global_tab_ld[1].send_in_db) {
			$("#fxstd_saved_status").attr("title",__("En attente - cliquer pour sauvegarder immédiatement"));
			$("#fxstd_saved_status .fxstd-update").css("color","yellow") ;
		} else {
			$("#fxstd_saved_status").attr("title",__("Sauvegarde activée - cliquer pour désactiver"));
			$("#fxstd_saved_status .fxstd-update").css("color","LimeGreen") ;
		}
	}
}

function showCode() {
	$("#fxstd_code_code").text(window.global_code);
	$("#fxstd_current_code").show();
}

/**
 * check if the page can be left and produce a message if not
 * @return {string}
 */
function getConfirmationMsg() {
	let msg = "" ;
	if (window.global_tab_ld[1].send_in_db) {
		if (window.global_save) {
			stdaloneUpdateLDinDB();
		}
		else {
			msg = __("Des modifications n'ont pas été sauvegardées et seront perdues.")+"\n";
		}
	}
	if (window.global_code) {
		msg += __("N'oubliez pas de noter le code de la feuille ({{page_code}}) pour la retrouver.",{page_code: window.global_code}) ;
	}
	return msg ;
}


/* ********************************************************
                    User actions
 **********************************************************/

function toggleOpenMenu() {
	$(".fxstd_open_menu").toggle();
}

function openNewLD() {
	let msg = getConfirmationMsg() ;
	if (msg === "" || confirm(msg)) {
		window.open('../tool_fitex/index.php','_self');
	}
}

/**
 * get a LD from labdoc_standalone and display it
 */
function openLDWithCode() {
	let msg = getConfirmationMsg() ;
	if (msg === "" || confirm(msg)) {
		$.ajax({ // check if the code exists
			type: 'POST',
			url: 'ajax.php?func=check_code',
			data: {
				code: $("#fxstd_code").val()
			},
			error: function(){
				alertGently(__('Problème de requête serveur'));
			},
			success: function(resp){
				if (!resp) {
					alertGently(__("Ce code n'existe pas."));
				}
				else {
					$("#form1").submit();
				}
			}
		});
	}
}

/**
 * change the save status of the LD
 */
function toggleSave() {
	if (!window.global_save) {
		window.global_save = true;
		window.global_tab_ld[1].send_in_db = true;
		refreshSaveIcon();
		stdaloneUpdateLDinDB() ;
		if (!window.global_code) {
			$('#fxstd_help').show();
			setTimeout(function(){
				$('#fxstd_help').hide();
				}, 5000);
		}
	} else {
		if (window.global_tab_ld[1].send_in_db) {
			// sauvegarde immédiate
			stdaloneUpdateLDinDB() ;
		} else {
			window.global_save = false;
			refreshSaveIcon();
		}
	}
}

/**
 * Send the LD in DB if necessary
 */
function stdaloneUpdateLDinDB() {
	if (window.global_tab_ld[1].send_in_db) {
		let ld_content = getLDContentFromLocalHistory(1) ;
		if (ld_content) {
			$.ajax({
				type: "POST",
				url: "ajax.php?func=save_LD",
				data: {
					code: window.global_code,
					content: ld_content
				},
				error: function() {
					alertGently(__("Le dernier enregistrement n'a pas fonctionné - Vérifiez votre connexion réseau"));
				},
				success: function(resp) {
					console.log("LD saved : " + resp);
					window.global_tab_ld[1].send_in_db = 0;
					if (!window.global_code) {
						window.global_code = resp;
						showCode();
					}
				}
			});
		}
	}
}
