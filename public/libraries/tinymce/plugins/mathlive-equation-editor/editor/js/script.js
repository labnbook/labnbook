let equationEditorCtrl = function() {
	// Html <math-field> element
	let mathField = '';
	// Html latex textarea
	let latexElement = '';
	// Html result field
	let resultElement = '';
	// Html insert result button
	let insertResultElement = '';
	// Latex formula
	let latex = '';
	// Contains 'empty' or '|% display|' for backward compatibility
	let display = '';
	// Config sent by LabNBook
	let mathLiveConfig = {};
	// Translations sent by LabNBook
	let langKeys = {};
	// Used to adjust the iframe when the virtual keyboard is displayed
	let iframeHeight = 0;

	/**
	 * Initialize MathLive editor with a possible formula
	 * @returns {Promise<void>}
	 */
	async function initMathLiveEditor() {
		// Wait for the <math-field> element to be ready
		await window.customElements.whenDefined('math-field');
		// Request params to TinyMCE
		window.parent.postMessage({	mceAction: 'mathlive-editor-request-params', status: true }, '*');
		// Wait for the response with a timeout
		let [initResponsePromise, timeOutPromise] = _setPromisesForParamsRequest();
		await Promise.race([initResponsePromise(), timeOutPromise()]);
		// Display translated texts
		_displayTexts();
		// Set additional options to mathField
		_setMathFieldOptions();
		// Initialize the virtual keyboard
		_initializeVirtualKeyboard();
		// Attach events to resize the Iframe when the virtual keyboard is displayed
		_setResizeIframeEventListeners();
		// Attach events to the mathField
		_setMathFieldEventListeners();
		// Attach events to bind both editors and display result if any
		_setBindingLatexMathField();
		// Initialize latexElement
		latexElement.value = mathField.getValue();
		// Render maths
		setTimeout(function() {
			MathLive.renderMathInDocument();
			mathVirtualKeyboard.show();
		}, 10);
	}

	/**
	 * Display the labels and title in Html document
	 * @private
	 */
	function _displayTexts() {
		const centeringCheckbox = document.getElementById('mathlive-equation-center-label');
		const calculator = document.getElementById('mathlive-calculator');
		const insertResult = document.getElementById('mathlive-insert-calculator-result');
		centeringCheckbox.setAttribute('title', langKeys['center_equation_checkbox']);
		centeringCheckbox.innerText = langKeys['center_equation_checkbox'];
		calculator.setAttribute('title', langKeys['calculator_title']);
		insertResult.setAttribute('title', langKeys['insert_result_title']);
	}

	/**
	 * Set two promises used to retrieve the parameters sent by TinyMCE
	 * @returns {((function(): Promise<unknown>)|(function(): Promise<unknown>))[]}
	 * @private
	 */
	function _setPromisesForParamsRequest() {
		return [
			() => new Promise((resolve, _) => {
				// initResponsePromise
				window.addEventListener('message', function initialize(event) {
					if (event.data.hasOwnProperty('type') && event.data.type === 'init-mathlive-editor') {
						window.removeEventListener('message', initialize);
						_setRetrievedParams(event);
						resolve(event.data);
					}
				});
			}), () => new Promise(function(resolve, reject) {
				// timeOutPromise
				setTimeout(function() {
					reject(new Error('Equation editor : timeout error while loading'));
				}, 10000);
			})
		]
	}

	/**
	 * Additional MathField options
	 * @private
	 */
	function _setMathFieldOptions() {
		mathField.setOptions({
			virtualKeyboardContainer: document.getElementById('mathlive-keyboard-container'),
			mathVirtualKeyboardPolicy: 'sandboxed'
		});
		// Emptying the context menu
		mathField.menuItems = mathField.menuItems.filter(item => false);
		// Disabling macros
        mathField.macros = [];
        // Enabling only some shortcuts
		mathField.inlineShortcuts = {
			"tan(": "\\tan(",
			"sin(": "\\sin(",
			"cos(": "\\cos(",
			"ln(": "\\ln(",
			"log(": "\\log(",
			"arcsin(": "\\arcsin(",
			"sinh(": "\\sinh(",
			"arccos(": "\\arccos(",
			"cosh(": "\\cosh(",
			"arctan(": "\\arctan(",
			"tanh(": "\\tanh(",
			" ": "\\space"
		};
        mathField.keybindings = [
            { key: "left", command: "moveToPreviousChar" },
            { key: "right", command: "moveToNextChar" },
            { key: "up", command: "moveUp" },
            { key: "down", command: "moveDown" },
            { key: "shift+[ArrowLeft]", command: "extendSelectionBackward" },
            { key: "shift+[ArrowRight]", command: "extendSelectionForward" },
            { key: "shift+[ArrowUp]", command: "extendSelectionUpward" },
            { key: "shift+[ArrowDown]", command: "extendSelectionDownward" },
            { key: "[Backspace]", command: "deleteBackward" },
            { key: "[Delete]", command: "deleteForward" },
            { key: "[Cut]", command: "cutToClipboard" },
            { key: "[Copy]", command: "copyToClipboard" },
            { key: "[Paste]", command: "pasteFromClipboard" },
            { key: "[Clear]", command: "deleteBackward" },
            { key: "[Undo]", command: "undo" },
            { key: "[Redo]", command: "redo" },
            { key: "ctrl+z", ifPlatform: "!macos", command: "undo" },
            { key: "shift+ctrl+z", ifPlatform: "!macos", command: "redo" }
		];
	}

	/**
	 * Events triggered when the virtual keyboard is displayed. Used to resize the iframe
	 * @private
	 */
	function _setResizeIframeEventListeners() {
		window.mathVirtualKeyboard.addEventListener('geometrychange', () => {
			const windowHeight = parent.document.getElementsByClassName("tox-dialog")[0].clientHeight;
			iframeHeight = iframeHeight === 0 ? windowHeight : iframeHeight;
			const height = iframeHeight + window.mathVirtualKeyboard.boundingRect.height;
			const width = parent.document.getElementsByClassName("tox-dialog")[0].clientWidth;
			parent.document.getElementsByClassName("tox-dialog")[0].setAttribute(
				"style", `height: ${height}px; max-height: ${height}px; width:${width}px; max-width:${width}px;`
			);
			if(mathVirtualKeyboard.previousHeight) {
				parent.document.getElementsByClassName("tox-dialog")[0].style.top = -(mathVirtualKeyboard.previousHeight/2)+'px';
				mathVirtualKeyboard.previousHeight = null;
			}
		});
		window.mathVirtualKeyboard.addEventListener('before-virtual-keyboard-toggle', () => {
			if (window.mathVirtualKeyboard.visible) {
				const height = parent.document.getElementsByClassName("tox-dialog")[0].clientHeight - window.mathVirtualKeyboard.boundingRect.height;
				const width = parent.document.getElementsByClassName("tox-dialog")[0].clientWidth;
				parent.document.getElementsByClassName("tox-dialog")[0].setAttribute(
					"style", `height: ${height}px; max-height: ${height}px; width:${width}px; max-width:${width}px;`
				);
				mathVirtualKeyboard.previousHeight = mathVirtualKeyboard.boundingRect.height;
			}
		});
	}
	/**
	 * Events triggered on the mathField
	 * @private
	 */
	function _setMathFieldEventListeners() {
		// Hack to enable the physical keyboard while the virtual keyboard is displayed
		mathField.addEventListener('keydown', (event) => {
			mathField.blur();
			mathField.focus();
		});

		mathField.addEventListener('focusin', () =>  mathVirtualKeyboard.show());
	}
	/**
	 * Bind latex formula and mathfield formula
	 * @private
	 */
	function _setBindingLatexMathField() {
		latexElement = document.getElementById('mathlive-latex-formula');
		resultElement = document.getElementById('mathlive-result');
		insertResultElement = document.getElementById('mathlive-insert-calculator-result');
		mathField.addEventListener('input', function() {
			resultElement.classList.remove('is-visible');
			insertResultElement.classList.remove('is-visible');
			latexElement.value = mathField.getValue();
			// This is a hack due to a Mathlive bug related to mhchem
			if (event.data.includes('\\tmpce')) {
				latexElement.value = latexElement.value.replaceAll('\\tmpce', '\\ce');
				mathField.setValue(latexElement.value);
			}
			if (event.data.includes('\\tmppu')) {
				latexElement.value = latexElement.value.replaceAll('\\tmppu', '\\pu');
				mathField.setValue(latexElement.value);
			}
			latex = latexElement.value;
			_sendDataToTinyMCE();
		});
		latexElement.addEventListener('input', function(event) {
			resultElement.classList.remove('is-visible');
			insertResultElement.classList.remove('is-visible');
			mathField.setValue(latexElement.value);
			latex = latexElement.value;
			_sendDataToTinyMCE();
		});
		latexElement.addEventListener('focusin', function(event) {
			mathVirtualKeyboard.hide();
		});
	}

	/**
	 * Change equation display mode (inline or centered) and update TinyMCE data
	 * @param element
	 */
	function changeDisplayMode(element) {
		if (element.checked) {
			display = '|% display|';
		} else {
			display = '';
		}
		_sendDataToTinyMCE();
	}

	/**
	 * After retrieving parameters from TinyMCE, set local variables accordingly
	 * @param event
	 * @private
	 */
	function _setRetrievedParams(event) {
		mathField = document.getElementById("mathlive-formula");
		const centeringCheckboxInput = document.getElementById('mathlive-equation-center-input');
		let data = event.data;
		latex = data.latex;
		display = data.display;
		centeringCheckboxInput.checked = (display !== '' && display !== undefined);
		mathLiveConfig = data.settings.mathlive_config;
		langKeys = data.settings.lang_keys;
		if (latex) {
			mathField.setValue(latex);
		}
		mathField.setOptions(mathLiveConfig);
		mathField.focus();
	}

	/**
	 * Send data to TinyMCE each time a change is made (inputs and checkbox) 
	 * @private
	 */
	function _sendDataToTinyMCE() {
		const content = {
			mceAction: 'update-equation-in-tinymce',
			html: MathLive.convertLatexToMarkup(latex || ''),
			latex: latex,
			display: display
		}
		window.parent.postMessage(content, '*');
	}

	/**
	 * Try to set a result in div#mathlive-result
	 * @private
	 */
	function setResult() {
		const expr = mathField.expression;
		let result = '';
		window.MathfieldElement.computeEngine.precision = 7;

		// Robust method to display expr (borrowed from @arnog)
		const exprN = expr.N();
		if (!exprN.isSame(expr)) {
			result = exprN.latex;
		}
		if (!result) {
			const exprSimplify = expr.simplify();
			if (!exprSimplify.isSame(expr)) {
				result = exprSimplify.latex;
			}
		}
		if (!result) {
			const exprEval = expr.evaluate();
			if (!exprEval.isSame(expr)) {
				result = exprEval.latex;
			}
		}
		if (!result) {
			result = expr.numericValue;
		}
		const resultElement = document.getElementById('mathlive-result');
		if (result) {
			this.latexResult = result;
			resultElement.innerHTML = MathLive.convertLatexToMarkup('= ' + result);
			insertResultElement.classList.add('is-visible');
		} else {
			resultElement.innerHTML = langKeys['calculation_failed'];
		}
		resultElement.classList.add('is-visible');
	}
	
	function insertResult() {
		mathField.setValue(mathField.getValue() + '\\space = \\space ' + this.latexResult);
		latexElement.value = mathField.getValue();
		latex = latexElement.value;
		_sendDataToTinyMCE();
	}

	/**
	 * Set options and load layouts of the virtual keyboard
	 * @private
	 */
	function _initializeVirtualKeyboard() {
		const basic_layout = {
			label: langKeys['virtual_keyboard_basic_tab_tooltip'],
			labelClass: 'MLK__tex-math',
			tooltip:  langKeys['virtual_keyboard_basic_tab_tooltip'],
			displayEditToolbar: false,
			layers: [
				{
					style: `.w100 { 
								width: calc(1 * var(--_keycap-width) - var(--_keycap-gap)) !important; 
							}
							.w110 { 
								width: calc(1.1 * var(--_keycap-width) - var(--_keycap-gap)) !important; 
							}
							.w120 { 
								width: calc(1.2 * var(--_keycap-width) - var(--_keycap-gap)) !important; 
							}
							.w130 { 
								width: calc(1.3 * var(--_keycap-width) - var(--_keycap-gap)) !important; 
							}
							.w140 {
								width: calc(1.4 * var(--_keycap-width) - var(--_keycap-gap)) !important;
							}
							.shift8 {
								 --_keycap-shift-font-size: 8px !important;
							 }
							.shift10 {
								 --_keycap-shift-font-size: 10px !important;
							 }
							.shift12 {
								 --_keycap-shift-font-size: 12px !important;
							 }
							 .shift20 {
								 --_keycap-shift-font-size: 20px !important;
							 }
							 .font12 {
								 --_keycap-font-size: 12px !important;
							 }
							.font18 {
								 --_keycap-font-size: 18px !important;
							 }`,
					rows: [
						[
							{ class: 'shift20', latex: '\\times', 
								shift:     { latex: '\\cdot' }, 
								variants: [{ latex: '\\cdot' }] },
							{ class: 'left small shift12', latex: '\\frac{#0}{#?}',
								shift:     { class: 'w100', latex: '{#0}/{#?}' },
								variants: [{ latex: '{#0}/{#?}' }] },
							{ class: 'left w130', latex: '\\sqrt{#0}',
								shift:     { class: 'w130', latex: '\\sqrt[#?]{#0}' },
								variants: [{ latex: '\\sqrt[#?]{#0}' }] },
							{ class: 'left', latex: '{#0}_{#?}',
								shift:     { latex: '{{#0}_{#?}}^{#?}' }, 
								variants: [{ latex: '{{#0}_{#?}}^{#?}' }] },
							{ class: 'left w110', latex: '{#0}^{#?}', 
								shift:     { class: 'w110', latex: '\\tan' },
								variants: [{ latex: '\\tan' }] },
							{ latex: '\\sin',
								shift:     { latex: '\\cos' }, 
								variants: [{ latex: '\\cos' }] },
							{ latex: '\\ln',
								shift:     { latex: '\\log' }, 
								variants: [{ latex: '\\log' }] },
							{ class: 'left w130', latex: 'e^{#0}',
								shift:     { class: 'w130', latex: '\\log_{#?}{#0}' }, 
								variants: [{ class: 'w130', latex: '\\log_{#?}{#0}' }] },
							{ class: 'left w120', latex: '\\cdot 10^{#0}',
								shift:     { class: 'w120', latex: 'E#0' },
								variants: [{ latex: 'E#0' }] },
						],
						[
							{ class: 'left', latex: '\\overrightarrow{#0}', 
								shift:     { class: '', latex: '\\overline{#0}' }, 
								variants: [{ class: '', latex: '\\overline{#0}' }] },
							{ class: 'left w120', latex: '\\lVert #0 \\rVert', 
								shift:     { class: 'w120', latex: '\\lvert #0 \\rvert' }, 
								variants: [{ latex: '\\lvert #0 \\rvert' }] },
							{ latex: '\\overset{\\Large{\\cdot}}{#0}',
								shift:     { latex: '\\overset{\\Large{\\cdot\\cdot}}{#0}' },
								variants: [{ latex: '\\overset{\\Large{\\cdot\\cdot}}{#0}' }] },
							{ class: 'left small shift10 w110', latex: '\\int{#0}', 
								shift:     { class: 'small w110', latex: '\\int_{#?}^{#?}{#0}' }, 
								variants: [{ class: 'small', latex: '\\int_{#?}^{#?}{#0}' }] },
							{ class: 'left small shift12 w130', latex: '\\iint{#0}', 
								shift:     { class: 'small w130', latex: '\\iiint{#0}' }, 
								variants: [{ class: 'small', latex: '\\iiint{#0}' }] },
							{ class: 'left font12 shift8', latex: '\\sum{#0}', 
								shift:     { class: 'font12', latex: '\\sum_{#?}^{#?}{#0}' }, 
								variants: [{ class: 'small', latex: '\\sum_{#?}^{#?}{#0}' }] },
							{ latex: '\\pm',
								shift:     { latex: '\\infty' }, 
								variants: [{ latex: '\\infty' }] },
							{ latex: '\\geq',
								shift:     { latex: '\\leq' }, 
								variants: [{ latex: '\\leq' }] },
							{ latex: '\\neq',
								shift:     { latex: '\\approx' }, 
								variants: [{ latex: '\\approx' }] },
						],
						[
							{ latex: '\\Rightarrow',
								shift:     { latex: '\\Leftrightarrow' }, 
								variants: [{ latex: '\\Leftrightarrow' }] },
							{ latex: '\\rightarrow',
								shift:     { latex: '\\underrightarrow{#0}' }, 
								variants: [{ latex: '\\underrightarrow{#0}' }] },
							{ class: 'left', latex: '\\rightleftharpoons', 
								shift:     { class: 'font18', latex: '\\xtofrom[#?]{#?}' }, 
								variants: [{ class: 'font18', latex: '\\xtofrom[#?]{#?}'}] },
							{ latex: '\\ell', 
								shift:     { label: 'ℏ', class: 'MLK__tex', insert: 'ℏ' }, 
								variants: [{ label: 'ℏ', class: 'MLK__tex', insert: 'ℏ' }] },
							{ latex: '\\partial',
								shift:     { latex: '\\nabla' }, 
								variants: [{ latex: '\\nabla' }] },
							{ class: 'MLK__tex', label: '<i>&alpha;</i>', insert: '\\alpha', 
								shift:     { class: 'MLK__tex', label: '<i>&beta;</i>', insert: '\\beta' }, 
								variants: [{ class: 'MLK__tex', label: '<i>&beta;</i>', insert: '\\beta' }] },
							{ class: 'MLK__tex', label: '<i>&gamma;</i>', insert: '\\gamma', 
								shift:     { class: 'MLK__tex', label: '<i>&Gamma;</i>', insert: '\\Gamma' }, 
								variants: [{ class: 'MLK__tex', label: '<i>&Gamma;</i>', insert: '\\Gamma' }] },
							{ class: 'MLK__tex', label: '<i>&delta;</i>', insert: '\\delta', 
								shift:     { class: 'MLK__tex', label: '<i>&Delta;</i>', insert: '\\Delta' }, 
								variants: [{ class: 'MLK__tex', label: '<i>&Delta;</i>', insert: '\\Delta' }] },
							{ class: 'MLK__tex', label: '<i>&#x03b5;</i>', insert: '\\varepsilon', 
								shift:     { class: 'MLK__tex', label: '<i>&xi;</i>', insert: '\\xi' }, 
								variants: [{ class: 'MLK__tex', label: '<i>&xi;</i>', insert: '\\xi' }] },
						],
						[
							'[shift]',
							{ class: 'MLK__tex', label: '<i>&theta;</i>', insert: '\\theta', 
								shift:     { class: 'MLK__tex', label: '<i>&eta;</i>', insert: '\\eta' }, 
								variants: [{ class: 'MLK__tex', label: '<i>&eta;</i>', insert: '\\eta' }] },
							{ class: 'MLK__tex', label: '<i>&lambda;</i>', insert: '\\lambda', 
								shift:     { class: 'MLK__tex', label: '<i>&Lambda;</i>', insert: '\\Lambda' }, 
								variants: [{ class: 'MLK__tex', label: '<i>&Lambda;</i>', insert: '\\Lambda' }] },
							{ class: 'MLK__tex', label: '<i>&mu;</i>', insert: '\\mu', 
								shift:     { class: 'MLK__tex', label: '<i>&nu;</i>', insert: '\\nu' }, 
								variants: [{ class: 'MLK__tex', label: '<i>&nu;</i>', insert: '\\nu' }] },
							{ class: 'MLK__tex', label: '<i>&pi;</i>', insert: '\\pi', 
								shift:     { class: 'MLK__tex', label: '<i>&rho;</i>', insert: '\\rho' }, 
								variants: [{ class: 'MLK__tex', label: '<i>&rho;</i>', insert: '\\rho' }] },
							{ class: 'MLK__tex', label: '<i>&sigma;</i>', insert: '\\sigma', 
								shift:     { class: 'MLK__tex', label: '<i>&tau;</i>', insert: '\\tau' }, 
								variants: [{ class: 'MLK__tex', label: '<i>&tau;</i>', insert: '\\tau' }] },
							{ class: 'MLK__tex', label: '<i>&phi;</i>', insert: '\\varphi', 
								shift:     { class: 'MLK__tex', label: '<i>&Phi;</i>', insert: '\\Phi' }, 
								variants: [{ class: 'MLK__tex', label: '<i>&Phi;</i>', insert: '\\Phi' }] },
							{ class: 'MLK__tex', label: '<i>&chi;</i>', insert: '\\chi', 
								shift:     { class: 'MLK__tex', label: '<i>&Psi;</i>', insert: '\\Psi' }, 
								variants: [{ class: 'MLK__tex', label: '<i>&Psi;</i>', insert: '\\Psi' }] },
							{ class: 'MLK__tex', label: '<i>&omega;</i>', insert: '\\omega', 
								shift:     { class: 'MLK__tex', label: '<i>&Omega;</i>', insert: '\\Omega' }, 
								variants: [{ class: 'MLK__tex', label: '<i>&Omega;</i>', insert: '\\Omega' }] },
						],
					],
				}
			]
		};
		const advanced_layout = {
			label: langKeys['virtual_keyboard_advanced_tab_tooltip'],
			labelClass: 'MLK__tex-math',
			tooltip:  langKeys['virtual_keyboard_advanced_tab_tooltip'],
			displayEditToolbar: false,
			layers: [
				{
					style: `.w110 { 
								width: calc(1.1 * var(--_keycap-width) - var(--_keycap-gap)) !important; 
							}
							.w120 { 
								width: calc(1.2 * var(--_keycap-width) - var(--_keycap-gap)) !important; 
							}
							.w130 { 
								width: calc(1.3 * var(--_keycap-width) - var(--_keycap-gap)) !important; 
							}
							.w140 { 
								width: calc(1.4 * var(--_keycap-width) - var(--_keycap-gap)) !important; 
							}
							.w150 { 
								width: calc(1.5 * var(--_keycap-width) - var(--_keycap-gap)) !important; 
							}
							.shift8 {
								 --_keycap-shift-font-size: 8px !important;
							 }
							.shift12 {
								 --_keycap-shift-font-size: 12px !important;
							 }
							 .font12 {
								 --_keycap-font-size: 12px !important;
							 }
							 .font14 {
								 --_keycap-font-size: 14px !important;
							 }
							 .font16 {
								 --_keycap-font-size: 16px !important;
							 }
							 .font18 {
								 --_keycap-font-size: 18px !important;
							 }`,
					rows: [
						[
							{ latex: '\\wedge',
								shift:     { latex: '\\circ' }, 
								variants: [{ latex: '\\circ' }] },
							{ class: 'left w110', latex: '_{#?}^{#?}{#0}',
								shift:     { class: 'w110', latex: '{#0}_{#?}^{#?}' },
								variants: [{ latex: '{#0}_{#?}^{#?}' }] },
							{ latex: '\\overset{\\Large{\\cdot\\cdot\\cdot}}{#0}',
								shift:     { latex: '\\overset{\\sim}{#0}' }, 
								variants: [{ latex: '\\overset{\\sim}{#0}' }] },
							{ latex: '\\overset{\\circ}{#0}',
								shift:     { latex: '\\widehat{#0}', insert: '\\widehat{#@}' }, 
								variants: [{ latex: '\\widehat{#0}', insert: '\\widehat{#@}' }] },
							{ latex: '{#0}^{\\prime}',
								shift:     { latex: '{#0}^{\\prime\\prime}' }, 
								variants: [{ latex: '{#0}^{\\prime\\prime}' }] },
							{ class: 'font18', latex: '\\arcsin',
								shift:     { class: '', latex: '\\sinh' }, 
								variants: [{ class: '', latex: '\\sinh' }] },
							{ class: 'font18', latex: '\\arccos',
								shift:     { class: '', latex: '\\cosh' }, 
								variants: [{ class: '', latex: '\\cosh' }] },
							{ class: 'font18', latex: '\\arctan',
								shift:     { class: '', latex: '\\tanh' }, 
								variants: [{ class: '', latex: '\\tanh' }] },
						],
						[
							{ class: 'left w110', latex: '\\left( #0\\right)',
								shift:     { class: 'w110', latex: '\\left[ #0\\right]' }, 
								variants: [{ latex: '\\left[ #0\\right]' }] },
							{ class: 'left w120', latex: '\\lbrace #0\\rbrace',
								shift:     { class: 'w120', latex: '\\langle #0\\rangle' }, 
								variants: [{ latex: '\\langle #0\\rangle' }] },
							{ class: 'left w110', latex: '\\langle #0\\vert',
								shift:     { class: 'w110', latex: '\\vert #0\\rangle' }, 
								variants: [{ latex: '\\vert #0\\rangle' }] },
							{ class: 'w120', latex: '\\langle #0 \\vert #?\\rangle' },
							{ class: 'left font14 shift8', latex: '\\prod{#0}',
								shift:     { class: 'font12', latex: '\\prod_{#?}^{#?}{#0}' }, 
								variants: [{ class: 'small', latex: '\\prod_{#?}^{#?}{#0}' }] },
							{ latex: '\\mp',
								shift:     { latex: '\\varpropto' }, 
								variants: [{ latex: '\\varpropto' }] },
							{ latex: '\\ngtr',
								shift:     { latex: '\\nless' }, 
								variants: [{ latex: '\\nless' }] },
							{ latex: '\\gg',
								shift:     { latex: '\\ll' }, 
								variants: [{ latex: '\\ll' }] },
							{ latex: '\\sim',
								shift:     { latex: '\\equiv' }, 
								variants: [{ latex: '\\equiv' }] },
						],
						[
							{ latex: '\\leftrightarrow',
								shift:     { latex: '\\leftarrow' }, 
								variants: [{ latex: '\\leftarrow' }] },
							{ latex: '\\searrow',
								shift:     { latex: '\\nearrow' }, 
								variants: [{ latex: '\\nearrow' }] },
							{ class: 'left w110 font18', latex: '\\lim_{#?\\to{#?}}{#0}' },
							{ latex: '\\%',
								shift:     { latex: '‰' },
								variants: [{ latex: '‰' }] },
							{ latex: '\\emptyset',
								shift:     { latex: 'Å' },
								variants: [{ latex: 'Å' }] },
							{ latex: '\\degree',
								shift:     { latex: '\\ast' }, 
								variants: [{ latex: '\\ast' }] },
							{ latex: '\\dagger',
								shift:     { latex: '\\ddag' }, 
								variants: [{ latex: '\\ddag' }] },
							{ latex: '\\bot',
								shift:     { latex: '\\parallel' }, 
								variants: [{ latex: '\\parallel' }] },
							{ class: 'font18', latex: '\\backslash' },
						],
						[
							'[shift]',
							{ latex: '\\varepsilon_0',
								shift:     { latex: '\\mu_0' }, 
								variants: [{ latex: '\\mu_0' }] },
							{ latex: '\\exists',
								shift:     { latex: '\\not\\exists' }, 
								variants: [{ latex: '\\not\\exists' }] },
							{ latex: '\\forall',
								shift:     { latex: '\\subseteq' }, 
								variants: [{ latex: '\\subseteq' }] },
							{ latex: '\\subset',
								shift:     { latex: '\\not\\subset' }, 
								variants: [{ latex: '\\not\\subset' }] },
							{ latex: '\\in',
								shift:     { latex: '\\not\\in' }, 
								variants: [{ latex: '\\not\\in' }] },
							{ latex: '\\cup',
								shift:     { latex: '\\cap' }, 
								variants: [{ latex: '\\cap' }] },
							{ latex: '\\mathbb{N}',
								shift:     { latex: '\\mathbb{Z}' }, 
								variants: [{ latex: '\\mathbb{Z}' }] },
							{ latex: '\\mathbb{Q}',
								shift:     { latex: '\\mathbb{R}' }, 
								variants: [{ latex: '\\mathbb{R}' }] },
						],
					],
				}
			]
		};

		const formulae_layout = {
			label: langKeys['virtual_keyboard_formulae_tab_tooltip'],
			labelClass: 'MLK__tex-math',
			tooltip:  langKeys['virtual_keyboard_formulae_tab_tooltip'],
			displayEditToolbar: false,
			layers: [
				{
					style: `.w110 { 
								width: calc(1.1 * var(--_keycap-width) - var(--_keycap-gap)) !important; 
							}
							.w120 { 
								width: calc(1.2 * var(--_keycap-width) - var(--_keycap-gap)) !important; 
							}
							.w130 { 
								width: calc(1.3 * var(--_keycap-width) - var(--_keycap-gap)) !important; 
							}
							.w140 { 
								width: calc(1.4 * var(--_keycap-width) - var(--_keycap-gap)) !important; 
							}
							.w160 { 
								width: calc(1.6 * var(--_keycap-width) - var(--_keycap-gap)) !important; 
							}
							.w260 { 
								width: calc(2.6 * var(--_keycap-width) - var(--_keycap-gap)) !important; 
							}
							.shift8 {
								 --_keycap-shift-font-size: 8px !important;
							 }
							.shift12 {
								 --_keycap-shift-font-size: 12px !important;
							 }
							 .shift20 {
								 --_keycap-shift-font-size: 20px !important;
							 }
							 .font10 {
								 --_keycap-font-size: 10px !important;
							 }
							 .font12 {
								 --_keycap-font-size: 12px !important;
							 }
							 .font14 {
								 --_keycap-font-size: 14px !important;
							 }
							 .font16 {
								 --_keycap-font-size: 16px !important;
							 }
							 .font18 {
								 --_keycap-font-size: 18px !important;
							 }
							 .font20 {
								 --_keycap-font-size: 20px !important;
							 }`,
					rows: [
						[
							{ class: 'font18', latex: '\\frac{1}{\\overline{#0}}' },
                            { class: 'font16 hide-shift', latex: '\\frac{U(#?)}{#?}',
								shift:     { latex: '\\frac{u(#?)}{#?}' },
								variants: [{ latex: '\\frac{u(#?)}{#?}' }] },
                            { class: 'font14 hide-shift', latex: '\\left(\\frac{U(#?)}{#?}\\right)^2',
								shift:     { latex: '\\left(\\frac{u(#?)}{#?}\\right)^2' },
								variants: [{ class: 'small', latex: '\\left(\\frac{u(#?)}{#?}\\right)^2' }] },
                            { class: 'font12 hide-shift', width: 2, latex: '\\sqrt{\\left(\\frac{U(#?)}{#?}\\right)^2+\\left(\\frac{U(#?)}{#?}\\right)^2}',
								shift:     { latex: '\\sqrt{\\left(\\frac{u(#?)}{#?}\\right)^2+\\left(\\frac{u(#?)}{#?}\\right)^2}' },
								variants: [{ class: 'small w260', latex: '\\sqrt{\\left(\\frac{u(#?)}{#?}\\right)^2+\\left(\\frac{u(#?)}{#?}\\right)^2}' }] },
							{ class: 'w110 font18', latex: 'e^{j(\\omega t+\\varphi)}' },
							{ class: 'font18', latex: 'e^{j\\omega}' },
							{ class: 'w140 font18', latex: '\\cos(\\omega t+\\varphi)' },
							{ class: 'w140 font18', latex: '\\sin(\\omega t+\\varphi)' },
						],
						[
							{ class: 'font16', latex: '\\frac{d}{d #?}' },
							{ class: 'font16', latex: '\\frac{d^2}{d {#?}^2}' },
							{ class: 'font16', latex: '\\frac{\\partial}{\\partial #?}' },
							{ class: 'font16', latex: '\\frac{\\partial^2}{\\partial {#?}^2}' },
							{ class: 'w140 font18', latex: '\\frac{e^x+e^{-x}}{2}' },
							{ class: 'w140 font18', latex: '\\frac{e^x-e^{-x}}{2}' },
							{ class: 'w140 font18', latex: '\\frac{e^x-e^{-x}}{e^x+e^{-x}}' },
							{ class: '', latex: 'e^{-t/\\tau}' },
						],
						[
							{ class: 'w120 font18', latex: 'gr\\vec{a}d\\left(#0\\right)' },
							{ class: 'w120 font18', latex: 'r\\vec{o}t\\left(#0\\right)' },
							{ latex: '\\vec{\\Delta}',
								shift:     { latex: '\\vec{\\nabla}' }, 
								variants: [{ latex: '\\vec{\\nabla}' }] },
							{ latex: '\\vec{u}_N',
								shift:     { latex: '\\vec{u}_T' }, 
								variants: [{ latex: '\\vec{u}_T' }] },
							{ latex: '\\vec{u}_\\varphi',
								shift:     { latex: '\\vec{u}_\\theta' }, 
								variants: [{ latex: '\\vec{u}_\\theta' }] },
							{ latex: '\\vec{u}_\\rho',
								shift:     { latex: '\\vec{u}_r' }, 
								variants: [{ latex: '\\vec{u}_r' }] },
							{ latex: '\\vec{i}',
								shift:     { latex: '\\vec{k}' }, 
								variants: [{ latex: '\\vec{k}' }] },
							{ latex: '\\vec{j}',
								shift:     { latex: 'd\\vec{\\ell}' }, 
								variants: [{ latex: 'd\\vec{\\ell}' }] },
						],
						[
							'[shift]',
							{ class: 'font12', latex: '\\begin{cases} #? \\\\ #? \\end{cases}' },
							{ class: 'small', latex: '\\begin{pmatrix} #? \\\\ #? \\end{pmatrix}' },
							{ class: 'small', latex: '\\begin{pmatrix} #? & #? \\end{pmatrix}' },
							{ class: 'small', latex: '\\begin{bmatrix} #? & #? \\\\ #? & #? \\end{bmatrix}' },
							{ class: 'small', latex: '\\begin{vmatrix} #? & #? \\\\ #? & #? \\end{vmatrix}' },
							{ class: 'small', latex: '\\begin{matrix} #? & + \\\\ #? & + \\end{matrix}', command: ['addColumnAfter'] },
							{ class: 'small', latex: '\\begin{matrix} #? & #? \\\\ + & + \\end{matrix}', command: ['addRowAfter'] },
						],
					],
				}
			]
		};

		const greek_layout = {
			label: langKeys['virtual_keyboard_greek_tab_tooltip'],
			labelClass: 'MLK__tex-math',
			tooltip: langKeys['virtual_keyboard_greek_tab_tooltip'],
			displayEditToolbar: false,
			layers: [
				{
					style: `.font18 {
								 --_keycap-font-size: 18px !important;
							 }`,
					rows: [
						[
							{ label: '<i>&alpha;</i>', class: 'MLK__tex hide-shift', insert: '\\alpha', aside: 'alpha', shift: '\\char"391' },
							{ label: '<i>&beta;</i>', class: 'MLK__tex hide-shift', insert: '\\beta', aside: 'beta', shift: '\\char"392' },
							{ label: '<i>&gamma;</i>', class: 'MLK__tex hide-shift', insert: '\\gamma', aside: 'gamma', shift: '\\Gamma' },
							{ label: '<i>&delta;</i>', class: 'MLK__tex hide-shift', insert: '\\delta', aside: 'delta', shift: '\\Delta' },
							{ label: '<i>&#x03f5;</i>', class: 'MLK__tex hide-shift', insert: '\\epsilon', aside: 'espilon', shift: '\\char"0190' },
							{ label: '<i>&zeta;</i>', class: 'MLK__tex hide-shift', insert: '\\zeta', aside: 'zeta', shift: '\\char"396' },
							{ label: '<i>&eta;</i>', class: 'MLK__tex hide-shift', insert: '\\eta', aside: 'eta', shift: '\\char"397' },
							{ label: '<i>&theta;</i>', class: 'MLK__tex hide-shift', insert: '\\theta', aside: 'theta', shift: '\\Theta' },
							{ label: '<i>&iota;</i>', class: 'MLK__tex hide-shift', insert: '\\iota', aside: 'iota', shift: '\\char"399' },
							{ label: '<i>&kappa;</i>', class: 'MLK__tex hide-shift', insert: '\\kappa', aside: 'kappa', shift: '\\Kappa' },
						],
						[
							'[separator-5]',
							{ label: '<i>&lambda;</i>', class: 'MLK__tex hide-shift', insert: '\\lambda', aside: 'lambda', shift: '\\Lambda' },
							{ label: '<i>&mu;</i>', class: 'MLK__tex hide-shift', insert: '\\mu', aside: 'mu', shift: '\\char"39C' },
							{ label: '<i>&nu;</i>', class: 'MLK__tex hide-shift', insert: '\\nu', aside: 'nu', shift: '\\char"39D' },
							{ label: '<i>&xi;</i>', class: 'MLK__tex hide-shift', insert: '\\xi', aside: 'xi', shift: '\\Xi' },
							{ label: '<i>&omicron;</i>', class: 'MLK__tex hide-shift', insert: '\\omicron', aside: 'omicron', shift: '\\char"39F' },
							{ label: '<i>&pi;</i>', class: 'MLK__tex hide-shift', insert: '\\pi', aside: 'pi', shift: '\\Pi' },
							{ label: '<i>&rho;</i>', class: 'MLK__tex hide-shift', insert: '\\rho', aside: 'rho', shift: '\\char"3A1' },
							{ label: '<i>&sigma;</i>', class: 'MLK__tex hide-shift', insert: '\\sigma', aside: 'sigma', shift: '\\Sigma' },
							{ label: '<i>&tau;</i>', class: 'MLK__tex hide-shift', insert: '\\tau', aside: 'tau', shift: '\\char"3A4' },
							'[separator-5]',
						],
						[
							'[shift]',
							{ label: '<i>&upsilon;</i>', class: 'MLK__tex hide-shift', insert: '\\upsilon', aside: 'upsilon', shift: '\\Upsilon' },
							{ label: '<i>&phi;</i>', class: 'MLK__tex hide-shift', insert: '\\phi', aside: 'phi', shift: '\\Phi' },
							{ label: '<i>&chi;</i>', class: 'MLK__tex hide-shift', insert: '\\chi', aside: 'chi', shift: '\\char"3A7' },
							{ label: '<i>&psi;</i>', class: 'MLK__tex hide-shift',  insert: '\\psi', aside: 'zeta', shift: '\\Psi' },
							{ label: '<i>&omega;</i>', class: 'MLK__tex hide-shift', insert: '\\omega', aside: 'omega', shift: '\\Omega' },
							{ label: '<i>&#x03c6;</i>',	class: 'MLK__tex hide-shift', insert: '\\varphi', aside: 'phi var.', shift: '\\Phi' },
							{ label: '<i>&#x03c2;</i>', class: 'MLK__tex hide-shift', insert: '\\varsigma', aside: 'sigma var.', shift: '\\Sigma' },
							'[backspace]',
						],
						[
							{ label: '<i>&#x03b5;</i>', class: 'MLK__tex', insert: '\\varepsilon', aside: 'espilon var.' },
							{ label: '<i>&#x03d1;</i>', class: 'MLK__tex', insert: '\\vartheta', aside: 'theta var.' },
							{ label: '<i>&#x3f0;</i>', class: 'MLK__tex', insert: '\\varkappa', aside: 'kappa var.' },
							{ label: '<i>&#x03d6;</i>', class: 'MLK__tex', insert: '\\varpi', aside: 'pi var.' },
							{ label: '<i>&#x03f1;</i>', class: 'MLK__tex', insert: '\\varrho', aside: 'rho var.' },
							{ label: '<i>Å</i>', class: 'MLK__tex font18', insert: 'Å', aside: 'ångström' },
							{ label: '<i>ℓ</i>', class: 'MLK__tex', insert: 'ℓ' },
							{ label: '<i>ℏ</i>', class: 'MLK__tex', insert: 'ℏ' },
							{ label: '<i>&#8706;</i>', class: 'MLK__tex', insert: '\\partial' },
						],
					]
				}
			]
		};

		const numeric_layout = {
			label: langKeys['virtual_keyboard_numeric_tab_tooltip'],
			labelClass: 'MLK__tex-math',
			tooltip: langKeys['virtual_keyboard_numeric_tab_tooltip'],
			displayEditToolbar: false,
			layers: [
				{
					rows: [
						[
							{
								latex: 'x',
								shift: 'y',
								variants: [
									'y',
									'z',
									't',
									'r',
									'x^2',
									'x^n',
									'x^{#?}',
									'x_n',
									'x_i',
									'x_{#?}',
									{ latex: 'f(#?)', class: 'small' },
									{ latex: 'g(#?)', class: 'small' },
								],
							},
							{ latex: 'n', shift: 'a', variants: ['i', 'j', 'p', 'k', 'a', 'u'] },
							'[separator-5]',
							'[7]',
							'[8]',
							'[9]',
							'[/]',
							'[separator-5]',
							{
								latex: '\\exponentialE',
								shift: '\\ln',
								variants: ['\\exp', '\\times 10^{#?}', '\\ln', '\\log_{10}', '\\log'],
							},
							{
								latex: '\\imaginaryI',
								variants: ['\\Re', '\\Im', '\\imaginaryJ', '\\Vert #0 \\Vert'],
							},
							{
								latex: '\\pi',
								shift: '\\sin',
								variants: [
									'\\prod',
									{ latex: '\\theta', aside: 'theta' },
									{ latex: '\\rho', aside: 'rho' },
									{ latex: '\\tau', aside: 'tau' },
									'\\sin',
									'\\cos',
									'\\tan',
								],
							},
						],
						[
							{
								label: '<',
								latex: '<',
								class: 'hide-shift',
								shift: { latex: '\\le', label: '≤' },
							},
							{
								label: '>',
								latex: '>',
								class: 'hide-shift',
								shift: { latex: '\\ge', label: '≥' },
							},
							'[separator-5]',
							'[4]',
							'[5]',
							'[6]',
							'[*]',
							'[separator-5]',
							{
								class: 'hide-shift',
								latex: '#0^2}',
								shift: '#0^{\\prime}}',
							},
							{
								latex: '#0^{#0}}',
								class: 'hide-shift',
								shift: '#0_{#?}',
							},
							{
								class: 'hide-shift',
								latex: '\\sqrt{#0}',
								shift: { latex: '\\sqrt[#0]{#?}}' },
							},
						],
						[
							'[(]',
							'[)]',
							'[separator-5]',
							'[1]',
							'[2]',
							'[3]',
							'[-]',
							'[separator-5]',
							{
								latex: '\\int^{\\infty}_{0}\\!#?\\,\\mathrm{d}x',
								class: 'small hide-shift',
								shift: '\\int',
								variants: [
									{ latex: '\\int_{#?}^{#?}', class: 'small' },
									{ latex: '\\int', class: 'small' },
									{ latex: '\\iint', class: 'small' },
									{ latex: '\\iiint', class: 'small' },
									{ latex: '\\oint', class: 'small' },
									'\\mathrm{d}x',
									{ latex: '\\dfrac{\\mathrm{d}}{\\mathrm{d} x}', class: 'small' },
									{ latex: '\\frac{\\partial}{\\partial x}', class: 'small' },

									'\\partial',
								],
							},
							{
								class: 'hide-shift',
								latex: '\\forall',
								shift: '\\exists',
							},
							{ label: '[backspace]', width: 1.0 },
						],
						[
							{ label: '[shift]', width: 2.0 },
							'[separator-5]',
							'[0]',
							'[.]',
							'[=]',
							'[+]',
							'[separator-5]',
							'[left]',
							'[right]',
							{ label: '[action]', width: 1.0 },
						],
					]
				}
			]
		};
		window.mathVirtualKeyboard.layouts = [basic_layout, advanced_layout, formulae_layout, greek_layout, numeric_layout, 'alphabetic'];
		// Remove first row of the alphabetic layout (numeric row) and rename the alphabetic layout
		window.mathVirtualKeyboard.normalizedLayouts[5].layers[0].rows.shift();
		window.mathVirtualKeyboard.normalizedLayouts[5].label = langKeys['virtual_keyboard_alphabetic_tab_tooltip'];
		window.mathVirtualKeyboard.normalizedLayouts[5].tooltip = langKeys['virtual_keyboard_alphabetic_tab_tooltip'];
		window.mathVirtualKeyboard.rebuild();
	}

	return {
		initMathLiveEditor,
		changeDisplayMode,
		setResult,
		insertResult,
		latexResult: ''
	}
}();
