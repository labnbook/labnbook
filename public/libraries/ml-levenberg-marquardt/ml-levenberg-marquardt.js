var CurveFitting = (() => {
	var __defProp = Object.defineProperty;
	var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
	var __getOwnPropNames = Object.getOwnPropertyNames;
	var __hasOwnProp = Object.prototype.hasOwnProperty;
	var __export = (target, all) => {
		for (var name in all)
			__defProp(target, name, { get: all[name], enumerable: true });
	};
	var __copyProps = (to, from, except, desc) => {
		if (from && typeof from === "object" || typeof from === "function") {
			for (let key of __getOwnPropNames(from))
				if (!__hasOwnProp.call(to, key) && key !== except)
					__defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
		}
		return to;
	};
	var __toCommonJS = (mod) => __copyProps(__defProp({}, "__esModule", { value: true }), mod);

	// src/main.ts
	var main_exports = {};
	__export(main_exports, {
		levenbergMarquardt: () => levenberg_marquardt_default
	});

	// src/LinearAlgebra.ts
	var Matrix = class {
		rows;
		columns;
		values;
		constructor(rows, columns, values) {
			this.rows = rows;
			this.columns = columns;
			if (values) {
				if (values instanceof Float64Array) {
					this.values = values;
				} else {
					this.values = new Float64Array(values);
				}
			} else {
				this.values = new Float64Array(rows * columns);
				this.values.fill(0);
			}
		}
		toString() {
			let output = "";
			let stringValues = [];
			const maxlength = this.values.reduce((carry, current) => Math.max(current.toExponential(2).length, carry), 0);
			this.values.forEach(function(n) {
				stringValues.push(n.toExponential(2).replace("0.00e+0", "0").padEnd(maxlength, " "));
			});
			for (let rowIdx = 0; rowIdx < this.rows; rowIdx++) {
				output += stringValues.slice(rowIdx * this.columns, (rowIdx + 1) * this.columns).join("  ") + "\n";
			}
			return output;
		}
	};
	function normInf(m) {
		let max = 0;
		for (const x of m.values) {
			max = Math.max(max, Math.abs(x));
		}
		return max;
	}
	var almostZero = 1e-20;
	function sum(A, B) {
		if (A.columns !== B.columns || A.rows !== B.rows) {
			throw new Error("Cannot sum because of wrong dimensions.");
		}
		let C = new Matrix(A.rows, A.columns);
		for (let l = 0; l < A.rows * A.columns; l++) {
			C.values[l] = A.values[l] + B.values[l];
		}
		return C;
	}
	function product(A, B) {
		if (A.columns !== B.rows) {
			throw new Error("Cannot multiply because of wrong dimensions.");
		}
		let C = new Matrix(A.rows, B.columns);
		for (let l = 0; l < A.rows * B.columns; l++) {
			C.values[l] = 0;
			let div = Math.floor(l / C.columns);
			let rem = l % C.columns;
			for (let k = 0; k < B.rows; k++) {
				C.values[l] += A.values[div * A.columns + k] * B.values[k * B.columns + rem];
			}
		}
		return C;
	}
	function transpose(A) {
		let At = new Matrix(A.columns, A.rows);
		for (let l = 0; l < A.rows * A.columns; l++) {
			At.values[l] = A.values[l % A.rows * A.columns + Math.floor(l / A.rows)];
		}
		return At;
	}
	function diag(A) {
		let dLength = Math.min(A.rows, A.columns);
		let d = new Float64Array(dLength).fill(0);
		for (let l = 0; l < dLength; l++) {
			d[l] = A.values[l * A.columns + l];
		}
		return d;
	}
	function matrixFromDiag(V) {
		let dimension = V.length;
		if (dimension === 0) {
			throw new Error("Cannot build matrix because of 0 dimension.");
		}
		let A = new Matrix(dimension, dimension);
		for (let l = 0; l < dimension; l++) {
			A.values[l * dimension + l] = V[l];
		}
		return A;
	}
	function solve(m, y) {
		if (m.rows !== y.rows) {
			throw new Error("Cannot solve because of wrong dimensions.");
		}
		const a = new Float64Array(m.rows * (y.columns + m.columns));
		let p = 0;
		let py = 0;
		for (let im = 0; im < m.rows * m.columns; im++) {
			a[p] = m.values[im];
			p++;
			if (im % m.columns === m.columns - 1) {
				for (let j = 0; j < y.columns; j++) {
					a[p] = y.values[py];
					p++;
					py++;
				}
			}
		}
		const numRows = m.rows;
		const numColumns = m.columns + y.columns;
		const size = m.rows * m.columns;
		for (let i = 0; i < numRows; i++) {
			const pivot = i * (numColumns + 1);
			if (Math.abs(a[pivot]) < almostZero) {
				let swapped = false;
				for (let j = pivot + numColumns; j < size; j += numColumns) {
					if (Math.abs(a[j]) > almostZero) {
						const row = a.slice(j, j + numColumns - i);
						for (let k = 0; k < numColumns - i; k++) {
							a[j + k] = a[pivot + k];
							a[pivot + k] = row[k];
						}
						swapped = true;
						break;
					}
				}
				if (!swapped) {
					console.error("m matrix");
					console.error(m.toString());
					console.error("a matrix");
					console.error(new Matrix(m.rows, y.columns + m.columns, a).toString());
					throw new Error("No solution.");
				}
			}
			const c = a[pivot];
			a[pivot] = 1;
			for (let k = 1; k < numColumns - i; k++) {
				a[pivot + k] /= c;
			}
			for (let r = 0; r < numRows; r++) {
				if (r === i || Math.abs(a[r * numColumns + i]) < almostZero) {
					continue;
				}
				const sub = a[r * numColumns + i];
				a[r * numColumns + i] = 0;
				for (let k = 1; k < numColumns - i; k++) {
					a[r * numColumns + i + k] -= sub * a[pivot + k];
				}
			}
		}
		const x = new Matrix(m.columns, y.columns);
		for (let i = 0; i < x.rows; i++) {
			for (let j = 0; j < x.columns; j++) {
				x.values[i * x.columns + j] = a[i * numColumns + m.columns + j];
			}
		}
		return x;
	}

	// src/lm/compute-next.ts
	var epsilonDifferences = 1e-4;
	var ComputeNext = class {
		#centralDifferences;
		#input;
		#modelFunction;
		#options;
		/**
		 * A vector that scales the parameter (ellipsoidal trust region)
		 */
		#dtd;
		constructor(input, modelFunction, o) {
			this.#input = input;
			this.#modelFunction = modelFunction;
			this.#options = o;
			this.#centralDifferences = new Array(o.parameters.initial.length);
			this.#centralDifferences.fill(false);
			this.#dtd = new Float64Array(o.parameters.initial.length);
			this.#dtd.fill(1);
		}
		/**
		 * Return the vector H that solves : (J^T W J + λ I) H = J^T W (Y - Ŷ).
		 */
		computeParametersDelta(lambda, JtWJ, JtWdy) {
			for (let i = 0; i < JtWJ.rows; i++) {
				JtWJ.values[i * JtWJ.columns + i] += lambda * this.#dtd[i];
			}
			const Y = new Matrix(JtWdy.length, 1, JtWdy);
			const h = solve(JtWJ, Y).values;
			for (let i = 0; i < h.length; i++) {
				this.#centralDifferences[i] = h[i] < epsilonDifferences;
			}
			return h;
		}
		computeChi2(y) {
			let sum2 = 0;
			for (let i = 0; i < this.#input.y.length; i++) {
				let sq = Math.pow(this.#input.y[i] - y[i], 2) * this.#options.weights[i];
				sum2 += sq;
			}
			return sum2;
		}
		computeRhoDenominator(h, lambda, JtWdy) {
			let sum2 = 0;
			for (let i = 0; i < h.length; i++) {
				sum2 += h[i] * (lambda * h[i] + JtWdy[i]);
			}
			return Math.abs(sum2);
		}
		updateJacobian(J, params, y) {
			const deltas = this.#updateDifferenceScales(y);
			const x = Array.from(this.#input.x);
			const pRight = Array.from(params);
			const pLeft = Array.from(params);
			for (let j = 0; j < J.columns; j++) {
				for (let i = 0; i < J.rows; i++) {
					const delta = deltas[i];
					pRight[j] += delta;
					pLeft[j] -= delta;
					const yRight = this.#modelFunction(pRight).call(null, x[i]);
					if (this.#centralDifferences) {
						const yLeft = this.#modelFunction(pLeft).call(null, x[i]);
						J.values[i * J.columns + j] = (yRight - yLeft) / (2 * delta);
					} else {
						J.values[i * J.columns + j] = (yRight - y[i]) / delta;
					}
					pRight[j] = params[j];
					pLeft[j] = params[j];
				}
			}
		}
		updateJtWJ(JtWJ, jacobian) {
			for (let i = 0; i < JtWJ.rows; i++) {
				for (let j = 0; j < JtWJ.columns; j++) {
					let sum2 = 0;
					for (let k = 0; k < jacobian.rows; k++) {
						sum2 += jacobian.values[k * jacobian.columns + i] * this.#options.weights[k] * jacobian.values[k * jacobian.columns + j];
					}
					JtWJ.values[i * JtWJ.columns + j] = sum2;
				}
			}
		}
		updateJtWdy(JtWdy, jacobian, y) {
			for (let j = 0; j < JtWdy.length; j++) {
				let sum2 = 0;
				for (let k = 0; k < jacobian.rows; k++) {
					sum2 += jacobian.values[k * jacobian.columns + j] * this.#options.weights[k] * (this.#input.y[k] - y[k]);
				}
				JtWdy[j] = sum2;
			}
		}
		/**
		 * Find the optimal precision for finite differences.
		 *
		 * For a double float, the optimal h in the central difference
		 * (f(x+h) - f(x-h)) / (2*h) is cubicRoot(M * precision / 2),
		 * where precision = 2^-52 ≈ 10^-16 and M = f(h) / f''(x).
		 */
		#updateDifferenceScales(y) {
			const deltas = new Float64Array(y.length);
			const centralScale = 5e-6;
			const forwardScale = 1e-9;
			for (let i = 0; i < y.length; i++) {
				deltas[i] = this.#centralDifferences[i] ? centralScale : forwardScale;
			}
			return deltas;
		}
	};

	// src/lm/compute-uncertainties.ts
	var ComputeUncertainties = class {
		#JtWJm1;
		// AKA [Jt * W * J]^-1
		#JtWJm1Jt;
		// AKA [Jt * W * J]^-1 * Jt
		#JJtWJm1Jt;
		// AKA J * [Jt * W * J]^-1 * Jt
		#Vy;
		#error;
		constructor(JtWJ, J, Vy) {
			this.#Vy = Vy;
			const diag2 = new Float64Array(J.columns).fill(1);
			const I = matrixFromDiag(diag2);
			try {
				this.#JtWJm1 = solve(JtWJ, I);
				this.#JtWJm1Jt = product(this.#JtWJm1, transpose(J));
				this.#JJtWJm1Jt = product(J, this.#JtWJm1Jt);
				this.#error = false;
			} catch (error) {
				console.log(error);
				this.#error = true;
			}
		}
		/**
		 * (from https://people.duke.edu/~hpgavin/ExperimentalSystems/lm.pdf)
		 * Gives a measure of how unexplained variability in the data propagates to variability in the
		 * parameters, and is essentially an error measure for the parameters.
		 */
		computeAsymptoticStandardParameterUncertainties() {
			if (!this.#error) {
				return diag(this.#JtWJm1).map((elt) => Math.sqrt(elt));
			} else {
				return new Float64Array();
			}
		}
		/**
		 * (from https://people.duke.edu/~hpgavin/ExperimentalSystems/lm.pdf)
		 * Indicates how variability in the parameters affects the variability in the curve-fit.
		 */
		computeAsymptoticStandardFitUncertainties() {
			if (!this.#error) {
				return diag(this.#JJtWJm1Jt).map((elt) => Math.sqrt(elt));
			} else {
				return new Float64Array();
			}
		}
		/**
		 * (from https://people.duke.edu/~hpgavin/ExperimentalSystems/lm.pdf)
		 * Reflects the standard error of the fit as well as the measurement error.
		 */
		computeAsymptoticStandardPredictionUncertainties() {
			if (!this.#error) {
				return diag(sum(this.#Vy, this.#JJtWJm1Jt)).map((elt) => Math.sqrt(elt));
			} else {
				return new Float64Array();
			}
		}
	};

	// src/lm/options.ts
	function completeOptions(o, input) {
		if (!Array.isArray(o?.parameters?.initial) || o?.parameters?.initial.length === 0) {
			throw new Error("You must pass an array of numbers into the option 'parameters.initial'.");
		}
		const numParameters = o.parameters.initial.length;
		let weights;
		let errorScaling;
		if (o.weights) {
			errorScaling = o?.errorScaling || false;
			weights = new Float64Array(o.weights);
		} else {
			errorScaling = true;
			weights = new Float64Array(input.x.length).fill(1);
		}
		let maxIterations = 500;
		if (typeof o?.maxIterations === "number" && o.maxIterations > 0) {
			maxIterations = o.maxIterations;
		} else if (numParameters > 1) {
			maxIterations = 400 * numParameters;
		}
		return {
			damping: typeof o?.damping === "number" && o.damping > 0 ? o.damping : 2,
			maxIterations,
			parameters: {
				initial: new Float64Array(o.parameters.initial),
				max: o.parameters?.max ? new Float64Array(o.parameters.max) : null,
				min: o.parameters?.min ? new Float64Array(o.parameters.min) : null
			},
			weights,
			forceParameterConvergence: o?.forceParameterConvergence || false,
			errorScaling
		};
	}

	// src/lm/levenberg-marquardt.ts
	var epsilonStep = 0.01;
	var epsilonGradient = 1e-8;
	var epsilonParameters = 1e-6;
	var epsilonChi2red = 1e-12;
	var dampingStepUp = 8;
	var dampingStepDown = 9;
	var dampingMin = 1e-20;
	var dampingMax = 1e25;
	function levenbergMarquardt(input, fn, o) {
		const options = completeOptions(o, input);
		const numParameters = options.parameters.initial.length;
		const numPoints = input.x.length;
		if (numPoints !== input.y.length) {
			throw new Error("The arrays input.x and input.y have different sizes.");
		}
		if (numPoints <= numParameters) {
			throw new Error("Fitting requires more input points than parameters in the model function.");
		}
		let parameters = options.parameters.initial.slice(0);
		let parametersOld = parameters.slice(0);
		let yOld = new Float64Array(numPoints);
		let y = new Float64Array(input.x.map(fn(Array.from(parameters))));
		const computeNext = new ComputeNext(input, fn, options);
		const jacobian = new Matrix(numPoints, numParameters);
		let JtWJ = new Matrix(numParameters, numParameters);
		const JtWdy = new Float64Array(numParameters);
		computeNext.updateJacobian(jacobian, parameters, y);
		computeNext.updateJtWJ(JtWJ, jacobian);
		computeNext.updateJtWdy(JtWdy, jacobian, y);
		let chi2 = computeNext.computeChi2(y);
		let chi2red = chi2 / (numPoints - numParameters);
		let chi2Old = 0;
		let lambda = options.damping;
		let convergence = "";
		let iteration = 0;
		while (convergence === "") {
			iteration++;
			if (iteration > options.maxIterations) {
				convergence = "iterations";
				break;
			}
			const h = computeNext.computeParametersDelta(lambda, JtWJ, JtWdy);
			parametersOld = parameters.slice(0);
			for (let i = 0; i < numParameters; i++) {
				parameters[i] += h[i];
				if (options.parameters.min && parameters[i] < options.parameters.min[i]) {
					parameters[i] = options.parameters.min[i];
				}
				if (options.parameters.max && parameters[i] > options.parameters.max[i]) {
					parameters[i] = options.parameters.max[i];
				}
			}
			yOld = y.slice(0);
			y = new Float64Array(input.x.map(fn(Array.from(parameters))));
			chi2Old = chi2;
			chi2 = computeNext.computeChi2(y);
			chi2red = chi2 / (numPoints - numParameters);
			let hrMax = 0;
			for (let i = 0; i < numParameters; i++) {
				hrMax = Math.max(hrMax, Math.abs(h[i] / parameters[i]));
			}
			const rho = (chi2Old - chi2) / computeNext.computeRhoDenominator(h, lambda, JtWdy);
			if (rho > epsilonStep) {
				computeNext.updateJacobian(jacobian, parameters, y);
				computeNext.updateJtWJ(JtWJ, jacobian);
				computeNext.updateJtWdy(JtWdy, jacobian, y);
				lambda = Math.max(lambda / dampingStepDown, dampingMin);
			} else {
				parameters = parametersOld.slice(0);
				y = yOld.slice(0);
				chi2 = chi2Old;
				chi2red = chi2 / (numPoints - numParameters);
				if (lambda >= dampingMax) {
					convergence = "failed";
					break;
				} else {
					lambda = lambda * dampingStepUp;
				}
			}
			if (iteration <= 2) {
				continue;
			}
			if (normInf(JtWJ) < epsilonGradient && !options.forceParameterConvergence) {
				convergence = "gradient";
			} else if (chi2red < epsilonChi2red) {
				convergence = "residual";
			} else if (hrMax < epsilonParameters) {
				convergence = "parameter";
			}
		}
		let computeUncertainties;
		let sigmaP = new Float64Array([]);
		let sigmaYFit = new Float64Array([]);
		let sigmaYPred = new Float64Array([]);
		if (convergence && convergence !== "failed") {
			let Vy;
			if (options.errorScaling) {
				let W = matrixFromDiag(new Float64Array(options.weights.map((w) => w / chi2red)));
				JtWJ = product(product(transpose(jacobian), W), jacobian);
			}
			Vy = matrixFromDiag(new Float64Array(options.weights.map((w) => 1 / w)));
			computeUncertainties = new ComputeUncertainties(JtWJ, jacobian, Vy);
			sigmaP = computeUncertainties.computeAsymptoticStandardParameterUncertainties();
		}
		return {
			convergence,
			error: chi2,
			reducedChi2: chi2red,
			iterations: iteration,
			parameters: Array.from(parameters),
			parameterUncertainties: sigmaP
		};
	}
	var levenberg_marquardt_default = levenbergMarquardt;
	return __toCommonJS(main_exports);
})();
