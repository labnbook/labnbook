Annotator.Plugin.Header = (function(superClass) {
    extend(Header, superClass);
  
    Header.prototype.options = {
        position                : '',
        contentButtonValidate   : '<i class="fa fa-check-circle"></i>',
        classButtonValidate     : '',
        contentButtonCancel     : '<i class="fa fa-times-circle"></i>',
        classButtonCancel       : ''
    };
  
    
    Header.prototype.events = {
      'annotationEditorShown': "createHeader"
    };
  
    Header.prototype.pluginInit = function() {
        if (!Annotator.supported()) {
            return;
        }
    };
  
    function Header(element, options) {
      Header.__super__.constructor.call(this, element, options);
      this.element = element;
    }

    Header.prototype.createHeader = function(event, annotation) {
      $('.annotator-save').remove();
      $('.annotator-cancel').remove();
      $('.annotator-category').parent().addClass('annotator-header');

      $('.annotator-category').parent().append('<a href="#save" class="annotator-submit annotator-save '+this.options.classButtonValidate+'">'+this.options.contentButtonValidate+'</a>');
      $('.annotator-category').parent().append('<a href="#cancel" class="annotator-submit annotator-cancel '+this.options.classButtonCancel+'">'+this.options.contentButtonCancel+'</a>');

      if( this.options.position == "" ){
        if( !$('.annotator-invert-y').length > 0 ){
          $('.annotator-widget').addClass('annotator-widget-reverse');
          $('.annotator-listing').addClass('annotator-listing-reverse');
        }else{
          $('.annotator-widget').removeClass('annotator-widget-reverse');
          $('.annotator-listing').removeClass('annotator-listing-reverse');
        }
      }else if( this.options.position != "bottom" ){
        $('.annotator-widget').addClass('annotator-widget-reverse');
          $('.annotator-listing').addClass('annotator-listing-reverse');
      }

    }    
  
    return Header;
  
  })(Annotator.Plugin);