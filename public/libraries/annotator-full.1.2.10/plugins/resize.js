Annotator.Plugin.Resize = (function(superClass) {
    extend(Resize, superClass);

    let formId;
    let heightTextarea;
    let heightPreviousAnnotations;
    let minHeightForm;
  
    Resize.prototype.options = {
        formId : 'form.annotator-widget',
        minHeightTextarea: 90,
        minHeightPreviousAnnotations:110,
        minWidthForm: 300,
    };
    
    Resize.prototype.events = {
      'annotationEditorShown'                   : 'createResize',
      'annotationEditorHidden'                  : 'closeResize',
      '.annotator-category click'               : "changeSelectedCategory",
      'resizeAfterDisplayPreviousAnnotations'   : "afterDisplayPreviousAnnotations"
    };
  
    Resize.prototype.pluginInit = function() {
        if (!Annotator.supported()) {
            return;
        }
    };
  
    function Resize(element, options) {
        Resize.__super__.constructor.call(this, element, options);

        formId                          = this.options.formId
        heightTextarea                  = this.options.minHeightTextarea;
        heightPreviousAnnotations       = this.options.minHeightPreviousAnnotations;
        minHeightForm                   = 200;

        this.element    = element;
    }

    Resize.prototype.createResize = function(event, annotation) {
        this.createResizable();
    }

    // Utilisé lorsqu'on change de catégorie
    Resize.prototype.changeSelectedCategory = function(event) {
        this.resize($(formId).height());
    }

    // utilisé lorsqu'on a effectué l'appel ajax pour récupérer l'historique
    Resize.prototype.afterDisplayPreviousAnnotations = function(event) {
        this.resize($(formId).height());
    }

    Resize.prototype.closeResize = function(event, annotation) {
        /*
        $('#annotator-field-0').outerHeight( this.options.minHeightTextarea, true );
        $(formId + ' .annotator-controls .previous-annotations').outerHeight( this.options.minHeightPreviousAnnotations );

        $(formId).outerWidth( this.options.minWidthForm, true );
        $(formId).outerHeight( 'auto', true );
        $(formId).css('top','auto');
        */
    }

    Resize.prototype.createResizable = function(){
        let handles;

        // Positionnement du cursor pour agrandir la fenêtre
        if( $('.annotator-invert-y').length > 0 ){
            handles = "se";
        }else{
            handles = "ne";
        }

        resizePrototype = this;

        $(formId).css('top','auto');

        // On set la taille minimun du formulaire
        if($(formId).height() < minHeightForm){
            $(formId).css('height',minHeightForm);
        }else{
            $(formId).css('height','auto');
        }
        
        $(formId).resizable({
            handles: handles,
            resize: function( event, ui ) {
                resizePrototype.resize(ui.size.height);
            }
        });

        $(formId).resizable("option","minHeight",minHeightForm);
		
		/* On cache la div .previous-annotations lors cette création d'une annotation afin que la fonction resize 
		appelée juste après ne considère pas à tort qu'il existe un historique d'annotations */
		$(formId + ' .annotator-controls .previous-annotations').hide();

        resizePrototype.resize($(formId).height());

        if(handles == "ne"){
            $(formId + ' .ui-resizable-ne').addClass('ui-icon ui-icon-gripsmall-diagonal-ne')
        }
    }

    Resize.prototype.resize = function(height){

        "use strict";

        // On récupère les éléments à soustraire du Height
        let heightHeader                            = $('form.annotator-widget li.annotator-header').outerHeight(true);
        let TextareaParentPaddingTop                = parseInt($('#annotator-field-0').parent().css('padding-top').replace('px', '')) ? parseInt($('#annotator-field-0').parent().css('padding-top').replace('px', '')) : 0;
        let TextareaParentPaddingBottom             = parseInt($('#annotator-field-0').parent().css('padding-bottom').replace('px', '')) ? parseInt($('#annotator-field-0').parent().css('padding-bottom').replace('px', '')) : 0;
        let PreviousAnnotationsParentPaddingTop     = parseInt($(formId + ' .annotator-controls .previous-annotations').parent().css('padding-top').replace('px', '')) ? parseInt($(formId + ' .annotator-controls .previous-annotations').parent().css('padding-top').replace('px', '')) : 0;
        let PreviousAnnotationsParentPaddingBottom  = parseInt($(formId + ' .annotator-controls .previous-annotations').parent().css('padding-bottom').replace('px', '')) ? parseInt($(formId + ' .annotator-controls .previous-annotations').parent().css('padding-bottom').replace('px', '')) : 0;

        let subtract = heightHeader + TextareaParentPaddingTop + TextareaParentPaddingBottom + PreviousAnnotationsParentPaddingTop + PreviousAnnotationsParentPaddingBottom;

        let allPreviousAnnotations          = $(formId + ' .annotator-controls .previous-annotations');
        let allPreviousAnnotationsHidden    = true;

        // On regarde si il a aucun historique affiché
        for( let i = 0; i < allPreviousAnnotations.length && allPreviousAnnotationsHidden; i++ ){
            if( $(allPreviousAnnotations[i]).css('display') != 'none' ){
                allPreviousAnnotationsHidden = false;
            }
        }

        // On mets la hauteur minimum
        if(height < minHeightForm || height == "min"){
            height = minHeightForm;
        }

        if( allPreviousAnnotationsHidden ){

            // Pas d'historique alors le textarea prend toute la place
            heightTextarea            = (height - subtract)*1;
            heightPreviousAnnotations = 0;

            $('#annotator-field-0').outerHeight( heightTextarea, true );
            $(formId + ' .annotator-controls .previous-annotations').outerHeight( heightPreviousAnnotations, true );

        }else{

            heightTextarea            = (height - subtract)*0.4;
            heightPreviousAnnotations = (height - subtract)*0.6;

            $('#annotator-field-0').outerHeight( heightTextarea, true );
            $(formId + ' .annotator-controls .previous-annotations').outerHeight( heightPreviousAnnotations, true );

        }
    }
  
    return Resize;
  
  })(Annotator.Plugin);
