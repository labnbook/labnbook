/*
The model of a dataset is in global_tab_ld[id_ld] (referenced by globalLD)
	.ld_type {string}: "dataset"
	.ld_history {array of XML strings}: local versions of LD contents
	... other usual attributes of LD (ld_initial_name, current_i, etc. - not used by the Fitex functions)
	.ld_content {XML object}: LD content
	.max_display_rows {int}: number of rows displayed on screen
	.columns[] {asociative array of objects}: [col_code => object] only the columns that have a formula
		.cfunc {compiled Mathjs-function}
		.dependencies[] {array of strings} symbols of constants or column-codes (numline and numrow are excluded)
	.graphs[] {array of objects}:
		.xmin, xmax, ymin, ymax {float}
		.plots[] {array of arrays}: [x,y,ux,uy] (used by flot to draw the plots, sorted on x)
		.outliers_plots[] {array of arrays}: [x,y,ux,uy] (used by flot to draw the plots)
		.pfuncs[] {array of objects}:
			.cfunc {compiled Mathjs-function}
			.is_fy {boolean}
			.dependencies[] {array of strings} symbols of constants or parameters, x or y
			.data[] {array of arrays}: [x,y] (used by flot to draw the curves, sorted on x)
	.view: references the js object that encapsulates the display functions
	.model: references the js object that encapsulates the model functions

The fitex_model object contains the functions of the model:
	- calculate the model data
	- check the coherence of the model
	- update the XML model
	- give access to data of the model
 */

/**
 * init the global_tab_ld[id_ld] object with missing values when displaying the dataset after its code import
 * @param {int} id_ld
 * @param {boolean} edition
 */
let fitex_model = (function(id_ld, edition) {
	"use strict";

	const default_max_display_rows = 60;
	const globalLD = global_tab_ld[id_ld];
	const $XMLcodeLD = addXMLDefaultSettings($(globalLD.ld_content));
	const forbidden_codes = [ "abs", "acos", "acosh", "asin", "asinh", "atan", "atanh", "atan2", "cbrt", "ceil", "clz32", "cos", "cosh", "e", "E", "exp", "expm1", "floor", "fround", "hypot", "imul", "ln", "log", "log1p", "log10", "log2", "max", "min",  "numline", "numrow", "pow", "rac", "random", "round", "sign", "sin", "sinh", "sqrt", "tan", "tanh", "trunc"];

	initModelJSObject();

	function addXMLDefaultSettings($xml) {
		let $settings = $xml.find('settings');
		if ($settings.length === 0) {
			$xml.find('metadata').after(`<settings><reducedKhi2>1</reducedKhi2><uxShiftedToy>1</uxShiftedToy><upNormalized>0</upNormalized></settings>`);
		}
		return $xml;
	}

	function initModelJSObject() {
		if (edition || !globalLD.max_display_rows) {
			globalLD.max_display_rows = default_max_display_rows;
		}
		try {
			updateJSObjectColFormulas(); // JS array of columns : formulas and dependencies
		} catch { } // do nothing on init
		try {
			updateJSObjectPFuncs(); // JS arrays of pfuncs : formulas, dependencies and data points (for Flot)
		} catch { } // do nothing on init
		updateJSObjectPlotsAndBounds(); // JS array of graphs : bounds and plots (for Flot)
	}

	// ***********************************************************************************************************
	//                                           JS Object
	// ***********************************************************************************************************

	/**
	 * Updates the JS object that contains the columns functions .columns[] with: .cfunc, .dependencies
	 * @param {string} col_code if not defined, update all columns
	 */
	function updateJSObjectColFormulas(col_code = "") {
		let $cols, sel_col;
		if (col_code === "") {
			$cols = $XMLcodeLD.find('column');
			globalLD.columns = [];
		} else {
			sel_col = getIcolFromCode(col_code);
			$cols = $XMLcodeLD.find('column').eq(sel_col);
		}
		$cols.each(function(){
			let col = {};
			col_code = $(this).find('code').text().trim();
			let $formula = $(this).find('formula', col_code);
			if ($formula.length && $formula.text().trim() !== "") {
				let pf = {};
				try {
					pf = parseAndCompileFormula(this);
				} catch (e) {
					pf.dependencies = [];
					if (col_code !== "") {
						throw e;  // the error is sent to alert the user when he defines a bad formula
					}
				} finally {
					col.cfunc = pf.compiled;
					col.dependencies = pf.dependencies;
					globalLD.columns[col_code] = col;
				}
			} else {
				delete globalLD.columns[col_code];
			}
		});
	}

	/**
	 * Updates the JS object that contains the parametered functions .pfuncs[] with: .cfunc, .dependencies, .is_fy
	 * Inits .data[]
	 * @param {int} sel_graph si non défini, mettre à jour toutes les fonctions de tous les graphiques
	 * @param {int} sel_func si non défini, mettre à jour toutes les fonctions du graphique
	 */
	function updateJSObjectPFuncs(sel_graph = -1, sel_func = -1) {
		let $graphs, $funcs;
		if (sel_graph === -1) {
			$graphs = $XMLcodeLD.find('graph');
			globalLD.graphs = [];
		} else {
			$graphs = $XMLcodeLD.find('graph').eq(sel_graph);
		}
		$graphs.each(function(igraph) {
			if (sel_graph !== -1) {
				igraph = sel_graph;
			}
			if (sel_func === -1) {
				$funcs = $(this).find('function');
				globalLD.graphs[igraph] = {};
				globalLD.graphs[igraph].pfuncs = [];
			} else {
				$funcs = $(this).find('function').eq(sel_func);
			}

			$funcs.each(function (ifunc) {
				if (sel_func !== -1) {
					ifunc = sel_func;
				}
				let func = {};
				let pf = {};
				try {
					pf = parseAndCompileFormula(this);
				} catch (e) {
					pf.dependencies = [];
					pf.compiled = null;
					if (sel_func !== -1) { // the error is sent to alert the user when he defines a bad formula
						throw e;
					}
				} finally {
					func.cfunc = pf.compiled;
					func.dependencies = pf.dependencies;
					func.is_fy = !pf.dependencies.includes("x") && pf.dependencies.includes("y"); // includes the case no x and no y
					func.data = [];
					globalLD.graphs[igraph].pfuncs[ifunc] = func;
				}
			});
		});
	}

	/**
	 * Parses a formula from a xml tag and indicates as an attribute of the xml tag if an error occured : "parse_error"
	 * @param {object} xml <column> or <function> - contains a <formula>
	 * @returns {{compiled: *, dependencies: *[]}}
	 */
	function parseAndCompileFormula(xml) {
		let $formula = $(xml).find('formula');
		let formula = $formula.text();
		formula = unEscapeXML(formula);
		formula = formula
			.replace(/;/g, ",")
			.replace(/ /g, "");

		if ($formula.attr('error') === 'parse_error') {
			$formula.removeAttr('error');
		}
		let dependencies = [];
		let compiled = null;
		let reserved_in_col_formulas = ['numline', 'numrow'];
		let formula_type = xml.tagName;
		let col_codes = [];
		$XMLcodeLD.find('column').each(function () {
			col_codes.push($(this).find('code').text());
		});
		try {
			if (formula_type === "column") {
				// Hack the formula of a column for relative indexes:
				// each column's code is a discrete function, applied on 0 by default.
				// e.g. data1 --> data1(0)
				col_codes.forEach(function(code) {
					let re1 = new RegExp('\\b' + code + '\\b', 'g');
					let re2 = new RegExp('\\b' + code + '\\(0\\)\\(', 'g');
					let re3 = new RegExp('\\b(' + code + ')\\(([+-]?[0-9]*):\\)', 'g');
					let re4 = new RegExp('\\b' + code + '\\(:', 'g');
					let re5 = new RegExp('\\b' + code + '\\(,', 'g');
					formula = formula
						.replace(re1, code + '(0)')
						.replace(re2, code + '(')
						.replace(re3, "$1($2,'fitex_array_end')")
						.replace(re4, code + "('fitex_array_start',")
						.replace(re5, code + "('fitex_array_start',");
				});
			}
			let parsed = math.parse(formula);
			parsed.traverse(function (node) {
				if (node.type === 'SymbolNode' && !dependencies.includes(node.name)
					&& (formula_type !== "column" || !reserved_in_col_formulas.includes(node.name))) {
					dependencies.push(node.name);
				} else if (node.type === 'FunctionNode'  && !dependencies.includes(node.name)
					&& formula_type === "column" && col_codes.includes(node.name)) {
					dependencies.push(node.name);
				}
			});
			compiled = parsed.compile();
		} catch(e) {
			console.log("error in parseAndCompileFormula for", formula);
			$formula.attr('error', "parse_error");
			throw "parse_error";
		}
		return {
			compiled: compiled,
			dependencies: dependencies
		}
	}

	/**
	 * Updates the Javascript object that contains the ploted data
	 * Updates xmin & xmax: it is the reason why all plotted data of each graph a reprocessed
	 * @param {int} sel_graph - if not defined, reprocess all graphs
	 * @param {boolean} force_update_pfuncs_data recalculate data if y-axis changed in term of log transformation
	 */
	function updateJSObjectPlotsAndBounds(sel_graph = -1, force_update_pfuncs_data = false) {

		function parseBound(bound) {
			bound = parseFloat(bound);
			return isNaN(bound) ? null : bound;
		}
		function NaNToZero(num) {
			return isNaN(num) ? 0 : num;
		}

		let $graphs;
		if (sel_graph === -1) {
			$graphs = $XMLcodeLD.find('graph');
		} else {
			$graphs = $XMLcodeLD.find('graph').eq(sel_graph);
		}
		$graphs.each(function(igraph) {
			if (sel_graph !== -1) {
				igraph = sel_graph;
			}
			let graph = globalLD.graphs[igraph];
			let graphType = getGraphType(igraph);
			let is_x_log = $(this).find('xscale').text() === 'log';
			let xmin = parseBound($(this).find('xmin').text()),
				xmax = parseBound($(this).find('xmax').text());
			let update_xmin = xmin === null;
			let update_xmax = xmax === null;

			// init plots & update xmin and xmax if needed
			graph.plots = [];
			graph.outliers_plots = [];
			$(this).find('plot').each(function(iplot) {
				// récupération des colonnes
				let xname = $(this).find('x').text() ;
				let yname = $(this).find('y').text() ;
				let uxname = $(this).find('ux').text();
				let uyname = $(this).find('uy').text();
				if (xname && (graphType === 'histogram' || yname)) {
					graph.plots[iplot]=[];
					graph.outliers_plots[iplot]=[];
					let icol = 0;
					let idx, idy, idux, iduy;

					// check if ux or uy are defined by constants
					let ux = null, uy = null;
					$XMLcodeLD.find("constant").each(function () {
						if ($(this).find('name').text() === uxname) {
							ux = NaNToZero(parseFloat($(this).find('value').text()));
						}
						if ($(this).find('name').text() === uyname) {
							uy = NaNToZero(parseFloat($(this).find('value').text()));
						}
					});

					$XMLcodeLD.find("column").find('code').each(function () { // recherche des numéros de colonne
						if ($(this).text() === xname) { idx = icol; }
						if ($(this).text() === yname) { idy = icol; }
						if (ux === null && $(this).text() === uxname) { idux = icol; }
						if (uy === null && $(this).text() === uyname) { iduy = icol; }
						icol++ ;
					});
					// updates the arrays of datapoints (outliers or not) with data (x,y,ux,uy)
					$XMLcodeLD.find("row").each(function () {
						let x = $(this).find('value').eq(idx);
						let valx = parseFloat(x.text());
						let y = $(this).find('value').eq(idy);
						let valy = parseFloat(y.text());
						if (!isNaN(valx) && (graphType === 'histogram' || !isNaN(valy))) { // a datapoint is added only if the 2 values are defined and are Numbers
							let valux = ux !== null ? ux : NaNToZero(parseFloat($(this).find('value').eq(idux).text()));
							let valuy = uy !== null ? uy : NaNToZero(parseFloat($(this).find('value').eq(iduy).text()));
							if ((x.attr('ignored') === 'true') || (y.attr('ignored') === 'true')) {
								graph.outliers_plots[iplot].push([valx, valy, valux, valuy]);
							}
							else {
								graph.plots[iplot].push([valx, valy, valux, valuy]);
							}
							// update xmin & xmax if needed
							if (update_xmin
								&& (xmin === null || valx < xmin) // find the lower
								&& (update_xmax || valx <= xmax) // avoid xmin > xmax
								&& (!is_x_log || valx > 0)) // avoid xmin <= 0 in case of log scale
							{
								xmin = valx;
							}
							if (update_xmax
								&& (xmax === null || valx > xmax) // find the higher
								&& (update_xmin || valx >= xmin)  // avoid xmin > xmax
								&& (!is_x_log || valx > 0)) // avoid xmax <= 0 in case of log scale
							{
								xmax = valx;
							}
						}
					});
					// sort the plot on x
					graph.plots[iplot].sort((a,b) => a[0] - b[0]);
				}
			});

			let min_max = manageFXAxisBounds(xmin, xmax, update_xmin, update_xmax, is_x_log);
			xmin = min_max.min;
			xmax = min_max.max;

			if (typeof graph.xmin === 'undefined' || graph.xmin !== xmin || typeof graph.xmax === 'undefined' || graph.xmax !== xmax) {
				graph.xmin = xmin;
				graph.xmax = xmax;
				try {
					updateJSObjectPFuncData(igraph);
				} catch(e) {
					console.log("Error : ", e);
				}
			} else if (force_update_pfuncs_data) {
				try {
					updateJSObjectPFuncData(igraph);
				} catch(e) {
					console.log("Error : ", e);
				}
			}
		});
	}

	/**
	 * Update the data of one or all Pfuncs of a graph
	 * @param {int} igraph
	 * @param {int} sel_func all functions if not defined
	 */
	function updateJSObjectPFuncData(igraph, sel_func = -1) {
		globalLD.graphs[igraph].pfuncs.forEach(function (pfunc, ifunc) {
			if (sel_func === -1 || sel_func === ifunc) { // update only the chosen Pfuncs
				let pfunc = globalLD.graphs[igraph].pfuncs[ifunc];
				pfunc.data = [];
				let $XMLGraph =  $XMLcodeLD.find('graph').eq(igraph);
				let $XMLFunc = $XMLGraph.find('function').eq(ifunc);
				// errors management
				if ($XMLFunc.find('formula').attr('error') === 'parse_error') {
					console.log("Data can't be calculated because of an error - Pfunction", igraph, ifunc);
					return;
				}

				const nb_points = 1000;
				// xmin and xmax
				let xmin = globalLD.graphs[igraph].xmin;
				let xmax = globalLD.graphs[igraph].xmax;
				if (xmin >= xmax) {
					return;
				}
				let is_x_log = $XMLGraph.find('xscale').text() === 'log'; // manage the x-axis increment
				let increment;
				if (is_x_log && xmin > 0) {
					increment = (math.log10(xmax) - math.log10(xmin)) / nb_points;
				} else {
					increment = (xmax - xmin) / nb_points;
				}
				let interval = calculatePFuncInterval(igraph, ifunc, xmin, xmax);
				if (interval === null) {
					return;
				}
				xmin = interval[0];
				xmax = interval[1];

				let data = [];
				let cfunc = pfunc.cfunc;
				let scope = {};
				try {
					scope = buildPFuncScope(igraph, ifunc);
				} catch (e) {
					throw (e);
				}
				// data points
				if (!pfunc.is_fy) {
					let x = xmin, y;
					let i = 0;
					try {
						while (x <= xmax) {
							if (is_x_log && xmin > 0) {
								x = xmin * math.pow(10, i * increment);
							} else {
								x = xmin + i * increment;
							}
							scope["x"] = x;
							y = cfunc.eval(scope);
							if (!isNaN(y)) {
								data.push([x,y]);
							}
							i++;
						}
						// data is sorted on x by construction
					} catch(e) {
						console.log("Error while processing the function");
					}
				}
				else if (pfunc.is_fy) { //case x = f(y) (pH functions between y = 0 and y = 14)
					const ymin = 0;
					const ymax = 14;
					increment = (ymax-ymin)/nb_points;
					// keep the the function only in the definition interval
					let xstart = parseFloat($XMLFunc.find('xstart').text());
					let xend = parseFloat($XMLFunc.find('xend').text());
					if (isNaN(xstart)) {
						xstart = -Infinity;
					}
					if (isNaN(xend)) {
						xend = +Infinity;
					}
					let x;
					try {
						for (let y = ymin ; y <= ymax ; y += increment) {
							scope["y"] = y;
							x = cfunc.eval(scope);
							if (!isNaN(x) && x >= 0 && x >= xstart && x <= xend) {
								data.push([x,y]);
							}
						}
						data.sort( (a, b) => { // sort the pfunc data on x once
							return a[0] - b[0]
						});
					} catch(e) {
						console.log("Error while processing the function");
					}
				}
				pfunc.data = data;
			}
		});
	}

	// ***********************************************************************************************************
	//                                           Constants
	// ***********************************************************************************************************

	/**
	 * add or delete constants (there is no update of constant values)
	 * possible errors : undef_symbol (in case of used constant in a col_formula or a pfunc)
	 * @param {array} constants associative array: symbol => value
	 * @returns { {updated_cols: string[], cols_in_circ_ref: string[], updated_graphs: int[], errors: string[]} }
	 */
	function updateConstants(constants) {
		let old_consts = {}; // associative array: symbol => value
		let added_deleted_consts = [], updated_consts = [], modified_consts = []; // arrays of symbols
		$XMLcodeLD.find('constant').each(function() {
			old_consts[$(this).find('name').text()] = $(this).find('value').text();
		});

		// update the XML
		for (let key in old_consts) {
			if (!(key in constants)) {
				added_deleted_consts.push(key);
				modified_consts.push(key);
			}
		}
		let xml_content = '<constants>' ;
		for (let key in constants) {
			xml_content += `<constant><name>${key}</name><value>${constants[key]}</value></constant>`;
			if (!(key in old_consts)) {
				added_deleted_consts.push(key);
				modified_consts.push(key);
			} else if (old_consts[key] !== constants[key]) {
				updated_consts.push(key);
				modified_consts.push(key);
			}
		}
		xml_content += '</constants>' ;
		$XMLcodeLD.find('constants').replaceWith(xml_content);

		// update the data (through column formulas)
		let modified = cascadeUpdateDataOnInput("constants", modified_consts);
		let modified_graphs = modified.updated_graphs;

		// update the Pfuncs - this happens AFTER the updateJSObjectPlotsAndBounds that may redefine the BOUNDS
		globalLD.graphs.forEach(function (graph, igraph) {
			graph.pfuncs.forEach(function (pfunc, ifunc) {
				for (const symbol of added_deleted_consts) { // added or deleted const => the pfunc is re-processed
					if (pfunc.dependencies.includes(symbol)) {
						try {
							updateJSObjectPFuncs(igraph, ifunc);
							updateJSObjectPFuncData(igraph, ifunc);
						} catch (e) { } // do nothing : the parse_error is known and the undef_symbol error should never happen
						if (!modified_graphs.includes(igraph)) {
							modified_graphs.push(igraph);
						}
						break;
					}
				}
				for (const symbol of updated_consts) { // the constant's value has been updated : calculate the data of the pfunc
					if (pfunc.dependencies.includes(symbol)) {
						try {
							updateJSObjectPFuncData(igraph, ifunc);
						} catch (e) { } // do nothing : the undef_symbol error should never happen
						if (!modified_graphs.includes(igraph)) {
							modified_graphs.push(igraph);
						}
						break;
					}
				}
			});
		});

		return {
			updated_cols: modified.updated_cols,
			cols_in_circ_ref: modified.cols_in_circ_ref,
			updated_graphs: modified_graphs,
			errors: modified.errors
		};
	}

	// ***********************************************************************************************************
	//                                           Cell Data
	// ***********************************************************************************************************

	/**
	 * Update a cell value and cascade
	 * @param {object} position
	 * @param {boolean} checked
	 * @param {Number} value
	 * @returns { {updated_cols: string[], cols_in_circ_ref: string[], updated_graphs: int[], errors: string[]} }
	 */
	function updateCellValue(position, checked, value = null) {
		let icol = position.col;
		let irow = position.row;
		let $xml_col = $XMLcodeLD.find('column').eq(icol);
		let $xml_val = $XMLcodeLD.find('row').eq(irow).find('value').eq(icol);
		let type = $xml_col.attr('type');

		// update in XML
		if (value === null) {
			value = $xml_val.text();
		}
		let xml = `<value ${(type === 'num' && !checked) ? ' ignored="true"' : ""}>${value}</value>` ;
		$xml_val.replaceWith(xml);

		// Update the dependant columns
		if (type === "num") {
			let col_code = $xml_col.find("code").text();
			return cascadeUpdateDataOnInput("data", [col_code]);
		} else {
			return {updated_cols: [], cols_in_circ_ref: [], updated_graphs: [], errors: []};
		}
	}

	/**
	 * From a 2D array of strings, update the values of the model from the selected position and cascade
	 * @param { string[][] } matrix
	 * @param { {col: number, row: number} | null} insert_pos
	 * @param {boolean} has_header
	 * @param {string} import_mode  "overwrite" | "appendColumns" | "appendRows"
	 * @return { {updated_cols: string[], cols_in_circ_ref: string[], updated_graphs: int[], errors: string[]} }
	 */
	function updateCellValuesFromMatrix(matrix, insert_pos, has_header = false, import_mode= "") {
		// console.log(matrix, "try add data on pos", insert_pos, "method", import_mode);
		let nb_cols_i, nb_rows_i;
		if (import_mode === "overwrite") {
			$XMLcodeLD.find('column').remove();
			$XMLcodeLD.find('row').remove();
			$XMLcodeLD.find('stat').find('value').remove();
			$XMLcodeLD.find('graph').remove();
			globalLD.graphs = [];
			nb_cols_i = 0;
			nb_rows_i = 0;
		} else {
			nb_cols_i = $XMLcodeLD.find('column').length;
			nb_rows_i = $XMLcodeLD.find('row').length;
		}

		// calculate the position of the insertion when the data is imported from a file
		if (insert_pos === null) {
			if (import_mode === "overwrite") { insert_pos = {col: 0, row: 0}; }
			else if (import_mode === "appendColumns") { insert_pos = {col: nb_cols_i, row: 0}; }
			else if (import_mode === "appendRows") { insert_pos = {col: 0, row: nb_rows_i}; }
			else { return {updated_cols: [], cols_in_circ_ref: [], updated_graphs: [], errors: []}; }
		}

		// pre-treatment of the inserted data
		matrix = normalizeRowLength(matrix);
		let nb_added_cols = matrix[0].length;
		let headers = has_header ? matrix.shift() : []; // remove the first row of matrix

		// add the missing columns - could be refactored with insertColumn but it is quite different
		let nb_missing_cols = insert_pos.col + nb_added_cols - nb_cols_i;
		if (nb_missing_cols > 0) {
			let added_col_codes = [];
			let col_types = detectColumnTypes(matrix, nb_added_cols);
			for (let i = nb_added_cols - nb_missing_cols; i < nb_added_cols; i++) {
				let name = headers[i] ? headers[i].substring(0, 59) : "";
				let code;
				if (name || name === "0") {
					code = buildColumnCodeFromName(name);
				} else {
					code = createFXCode();
					name = code;
				}
				let XMLName = $("<name>", { text: name });
				let XMLCode = $("<code>", { text: code });
				let XMLColumn = $("<column>", { type: `${col_types[i]}` });
				XMLColumn.append(XMLName, XMLCode, "<unit/>");
				$XMLcodeLD.find('header').append(XMLColumn);
				added_col_codes.push(code);
			}
			let add_vals = "<value/>".repeat(nb_missing_cols);
			let $rows_stats = $XMLcodeLD.find('row, stat');
			$rows_stats.each(function () {
				$(this).append(add_vals);
			});
			// recompile the formulas that have the column code in their dependencies
			// in order to transform the dependency as a function in the compiled (a rare bug could happen)
			for (let code in globalLD.columns) {
				for (const added_code of added_col_codes){
					if (globalLD.columns[code].dependencies.includes(added_code)) {
						updateJSObjectColFormulas(code);
					}
				}
			}
		}

		// add the missing rows
		let nb_added_rows = matrix.length;
		let nb_missing_rows = insert_pos.row + nb_added_rows - nb_rows_i;
		if (nb_missing_rows > 0) {
			insertRows(null, nb_missing_rows, false);
		}

		// update the data
		let value, modified_cols = [], errors = [];
		let columns_formats = getColumnsFormats();
		$XMLcodeLD.find('column').each(function(icol) {
			if (icol >= insert_pos.col && icol < insert_pos.col + nb_added_cols) {
				if (columns_formats[icol].formula !== "") { // the column can't be updated because of a formula that defines the value
					if (!errors.includes("insert_in_col_formula")) {
						errors.push("insert_in_col_formula");
					}
					return true;
				}
				modified_cols.push(columns_formats[icol].code);
				let col_type = columns_formats[icol].type;
				let update = true;
				let $row = $XMLcodeLD.find('row');
				for (let i = 0; i < nb_added_rows; i++) {
					value = matrix[i][icol - insert_pos.col];
					if (col_type === "num") {
						value = formatFXInputNumValue(value); // value is a string
						if (isNaN(value)) {
							if (!errors.includes("insert_text_in_num")) {
								errors.push("insert_text_in_num");
							}
							update = false;
						}
					}
					if (update) {
						$row.eq(insert_pos.row + i).find('value').eq(icol).text(value);
					}
					update = true;
				}
			}
		});

		let updated;
		if (nb_missing_rows > 0) {
			updated = cascadeUpdateDataOnInput("added_rows", modified_cols);
		} else {
			updated = cascadeUpdateDataOnInput("data", modified_cols);
		}
		updated.errors = errors.concat(updated.errors);
		return updated;

		/**
		 * Sets the same length for all the rows of the matrix by adding "" if needed
		 * @param { [][] } data matrix
		 * @returns { [][] } normalized matrix
		 */
		function normalizeRowLength(data) {
			let rowsLength = data.map(function(x){ return x.length; });
			let rowMaxSize = rowsLength.reduce(function(a, b){ return Math.max(a, b); });
			let rowMinSize = rowsLength.reduce(function(a, b){ return Math.min(a, b); });
			if (rowMinSize < rowMaxSize) {
				for (let i = 0; i < data.length; i++) {
					while (data[i].length < rowMaxSize) {
						data[i].push("");
					}
				}
			}
			return data;
		}

		/**
		 * detect the types of the data in each column of rows
		 * @param { [][] } data matrix of string values
		 * @param {int|null} nb_cols nb of columns
		 * @returns { ["num|txt"] }
		 */
		function detectColumnTypes(data, nb_cols = null) {
			if (nb_cols  === null) {
				nb_cols = data[0].length;
			}
			let types = new Array(nb_cols).fill("num");
			for (let row of data) {
				for (let i = 0; i < nb_cols; i++) {
					if (types[i] !== 'txt' && typeof row[i] !== 'undefined') {
						let v = row[i].trim();
						if (v !== "" && v !== null && typeof v !== 'number') {
							if (!(v.match(/^[+-]?\d*[,.]?\d+([eE][+-]?\d+)?$/) || v.match(/^[+-]?\d([  ]\d{3})+$/))) {
								//        e.g. +34,4e-17                                  e.g. +6 487
								types[i] = 'txt';
							}
						}
					}
				}
			}
			return types;
		}

		/**
		 * builds a valid column code from its name
		 * @param name
		 * @returns {string}
		 */
		function buildColumnCodeFromName(name) {
			let code;
			if (name) {
				code = formatStringToAlphanum(name, false);
				if (code.match(/^[^A-Za-z]/)) {
					code = "c_" + code;
				}
				code = code.substring(0, 11);
				if (forbidden_codes.includes(code) || code === 'x' || code === 'y' || existsFXCode(code)) {
					let prefix = code.substring(0, 8); // 9 chars + "_" + digits
					code = createFXCode(prefix);
				}
			} else if (import_mode === 'appendColumns') {
				code = createFXCode("import");
			} else {
				code = createFXCode();
			}
			return code;
		}
	}

	/**
	 * fill the paperboard with the values of the copied cells
	 * @param {{col_min: int, col_max: int, row_min: int, row_max: int}} matrix_corners
	 * @param {string} selected_type col | row | cell
	 * @returns {string} values separated by tabs and carrier returns
	 */
	function getCellValues(matrix_corners, selected_type) {
		if (selected_type === 'col') {
			matrix_corners.row_min = 0;
			matrix_corners.row_max = $XMLcodeLD.find('row').length - 1;
		} else if (selected_type === 'row') {
			matrix_corners.col_min = 0;
			matrix_corners.col_max = $XMLcodeLD.find('column').length - 1;
		}
		let cells_values = '';
		for (let r = matrix_corners.row_min; r <= matrix_corners.row_max; r++) {
			for (let c = matrix_corners.col_min; c <= matrix_corners.col_max; c++) {
				cells_values += $XMLcodeLD.find('row').eq(r).find('value').eq(c).text();
				cells_values += '\t';
			}
			cells_values = cells_values.slice(0, -1);
			cells_values += '\n';
		}
		cells_values = cells_values.slice(0, -1);
		return cells_values;
	}

	/**
	 * Delete cell values | complete columns | complete rows
	 * @param {{col_min: number, col_max: number, row_max: number, row_min: number}} matrix_corners
	 * @param {string} selected_type 'col' | 'row' | 'cell'
	 */
	function deleteCellValues(matrix_corners, selected_type) {
		if (selected_type === 'cell') {
			let modified_cols = [];
			let $cols = $XMLcodeLD.find('column');
			let $rows = $XMLcodeLD.find('row');
			let col_code, formula;
			for (let icol = matrix_corners.col_min; icol <= matrix_corners.col_max; icol++) {
				formula = $cols.eq(icol).find('formula').text();
				if (formula === "") {
					col_code = $cols.eq(icol).find('code').text();
					modified_cols.push(col_code);
					for (let irow = matrix_corners.row_min; irow <= matrix_corners.row_max; irow++) {
						$rows.eq(irow).find('value').eq(icol).text('');
					}
				}
			}
			return cascadeUpdateDataOnInput('data', modified_cols);
		} else if (selected_type === 'row') {
			for (let irow = matrix_corners.row_max; irow >= matrix_corners.row_min; irow--) {
				$XMLcodeLD.find('row').eq(irow).remove();
			}
			return cascadeUpdateDataOnInput("deleted_rows");
		} else if (selected_type === 'col') {
			// delete in XML-model
			let $cols = $XMLcodeLD.find('column');
			let deleted_codes = [];
			for (let icol = matrix_corners.col_max; icol >= matrix_corners.col_min; icol--) {
				let $col = $cols.eq(icol);
				let deleted_code = $col.find('code').text();
				deleted_codes.push(deleted_code);
				$col.remove();
				$XMLcodeLD.find('row, stat').each(function () {
					$(this).find('value').eq(icol).remove();
				});
				// delete in JSObject-model
				if (globalLD.columns.hasOwnProperty(deleted_code)) {
					delete globalLD.columns[deleted_code];
				}
				// recompile the formulas that have the column code in their dependencies
				// in order to transform the dependency as a constant in the compiled (a rare bug could happen)
				for (let code in globalLD.columns) {
					if (globalLD.columns[code].dependencies.includes(deleted_code)) {
						updateJSObjectColFormulas(code);
					}
				}
			}
			// update dependant columns, graphs and functions
			return cascadeUpdateDataOnInput("deleted_cols", deleted_codes);
		}
	}

	/**
	 * delete a column that was just added (it can be made straightfully: there will be no consequence)
	 * @param {int} icol
	 */
	function deleteEmptyColumn(icol) {
		// delete in XML-model
		let $col = $XMLcodeLD.find('column').eq(icol);
		$col.remove();
		$XMLcodeLD.find('row, stat').each(function () {
			$(this).find('value').eq(icol).remove();
		});
	}

	/**
	 * Create CSV data from the dataset
	 * @param column_separator
	 * @param decimal_sep
	 * @returns {*}
	 */
	function exportDataToCSV(column_separator, decimal_sep) {
		let csvContent = "data:text/csv;charset=utf-8,";
		let $xmlDataset = $XMLcodeLD.find('dataset');
		let columnTypes = [];
		let columnCodes = [];
		$xmlDataset.find('header').children('column').each(function () {
			columnTypes.push(this.getAttribute("type") === 'num' ? "n" : "s");
			columnCodes.push($(this).children("code").text());
		});
		csvContent += columnCodes.join(column_separator);
		let $xmlRows = $xmlDataset.children('row');
		$xmlRows.each(function () {
			let values = $(this).children("value").map(function() {
				return $(this).text();
			});
			let cleanedValues = [];
			csvContent += "\n";
			for (let i = 0; i < columnTypes.length; i++) {
				let value = values[i];
				if (columnTypes[i] === "n") {
					if ((typeof value === 'undefined') || value === null || value.trim() === "") {
						value = null;
					} else {
						value = String(parseFloat(value)).replace('.', decimal_sep);
					}
				} else {
					value = escapeCSVString(value, column_separator);
				}
				cleanedValues.push(value);
			}
			csvContent += cleanedValues.join(column_separator);
		});
		return csvContent;
	}
	
	function escapeCSVString (stringValue, stringToEscape) {
		if (stringValue.indexOf(stringToEscape) > -1 || stringValue.indexOf('"') > -1) {
			stringValue = '"' + stringValue.replaceAll('"', '""') + '"';
		}
		return stringValue;
	}
	
	/**
	 * Create XLSX data from the dataset
	 * @returns {*}
	 */
	function exportDataToXLSX() {
		let sheetName = __("Feuille 1");
		let $xmlds = $XMLcodeLD.find('dataset');
		// read the column types
		let columnTypes = [];
		let columnCodes = [];
		$xmlds.find('header').children('column').each(function () {
			columnTypes.push(this.getAttribute("type") === 'num' ? "n" : "s");
			columnCodes.push($(this).children("code").text());
		});
		// spreadsheet with headers
		let ws = XLSX.utils.aoa_to_sheet([columnCodes]);
		// add cells to the sheet
		let $xmlRows = $xmlds.children('row');
		$xmlRows.each(function (irow) {
			let values = $(this).children("value").map(function() {
				return $(this).text();
			})
			for (let i = 0; i < columnTypes.length; i++) {
				let cell = {
					v: values[i],
					t: "s"
				};
				let cLetter = String.fromCodePoint(65 + i);
				if (columnTypes[i] === "n") {
					if ((typeof cell.v === 'undefined') || cell.v === null || cell.v.trim() === "") {
						cell.v = null;
						cell.t = "z";
					} else {
						cell.v = parseFloat(cell.v);
						cell.t = "n";
					}
				} else if (cell.v === "###") {
					cell.t = "e"; // error
				} else {
					cell.t = columnTypes[i];
				}
				ws[cLetter + (irow + 2).toString()] = cell; // 2 <= 1-based + header-row
			}
		});
		ws['!ref'] = XLSX.utils.encode_range({
			s: {
				c: 0,
				r: 0
			},
			e: {
				c: columnCodes.length,
				r: $xmlRows.length + 1
			}
		});
		// Create a WorkBook (Common Spreadsheet Format) with this single spreadsheet
		let wb = XLSX.utils.book_new();
		wb.Props = { Title: "LabNBook" };
		wb.SheetNames.push(sheetName);
		wb.Sheets[sheetName] = ws;
		return wb;
	}


	// ***********************************************************************************************************
	//                                  Statistical indicators
	// ***********************************************************************************************************

	/** calcul de l'indicateur statistique "si" et mise à jour dans le XML
	 * tous les indicateurs stats déjà présents si "si" est null
	 * @param {string} si code of the stat indicator
	 * @param {null | int} icol position of the column to update (can be defined only if si is not defined)
	 */
	function updateStatIndicators(si = "", icol = null) {
		let $col = $XMLcodeLD.find('column');
		let nb_col = $col.length;

		// selector of the indicators to update
		let $stat_inds;
		if (si) { // ajout d'un seul indicateur
			$stat_inds = $XMLcodeLD.find('stat[property="'+si+'"]');
			if (!$stat_inds.length) { // la ligne n'existe pas pour cet indicateur, on le crée
				$XMLcodeLD.find('dataset').append(`<stat property="${si}">` + '<value/>'.repeat(nb_col) + '</stat>');
				$stat_inds = $XMLcodeLD.find('stat[property="'+si+'"]');
			}
		}
		else {
			$stat_inds = $XMLcodeLD.find('stat') ;
		}

		// calculus are made column by column
		let val, si_val = "";
		let icolmin = 0, icolmax = nb_col;
		if (icol !== null) {
			icolmin = icol;
			icolmax = icol + 1;
		}
		for (let i = icolmin ; i < icolmax ; i++) {
			let is_num = ($col.eq(i).attr('type') === "num");
			// get the values of the column in an array
			let data_val = [] ;
			let nb_val = 0;
			$XMLcodeLD.find('row').each(function() {
				val = $(this).children('value').eq(i).text() ;
				if (val && $(this).children('value').eq(i).attr('ignored') !== "true") {
					if (is_num) {
						data_val.push(parseFloat(val));
					} else {
						nb_val++;
					}
				}
			});
			if (is_num) {
				nb_val = data_val.length;
			}
			// calcul de chacun des indicateurs pour la colonne
			$stat_inds.each(function() {
				si = $(this).attr('property') ;
				if (si !== 'n' && (nb_val === 0 || !is_num)) { si_val = ""; }
				else if (si === 'n')       { si_val = nb_val; }
				else if (si === 'sum')     { si_val = calculateStatIndSum(data_val) ; }
				else if (si === 'min')     { si_val = Math.min.apply(null, data_val) ; }
				else if (si === 'max')     { si_val = Math.max.apply(null, data_val) ; }
				else if (si === 'mean')    { si_val = calculateStatIndMean(data_val) ; }
				else if (si === 'med')     { si_val = calculateStatIndMedian(data_val) ; }
				else if (si === 'std_dev') { si_val = calculateStatIndStdDev(data_val) ; }
				else if (si === 'c_var')   { si_val = calculateStatIndCoefVar(data_val) ; }
				if (si_val !== "" && (isNaN(si_val) /*|| si_val === Infinity || si_val === -Infinity*/ )) {
					si_val = "###";
				}
				let xml_val = `<value>${si_val}</value>`;
				let $xmlSIVals = $(this).find('value');
				if ($xmlSIVals.length > i) {
					$xmlSIVals.eq(i).replaceWith(xml_val) ;
				} else {
					$(this).append(xml_val);
				}
			});
		}

		function calculateStatIndSum(values) {
			let sum = 0;
			for (let i=0, n=values.length; i<n; i++) {
				sum += values[i];
			}
			return sum ;
		}
		function calculateStatIndMean(values) {
			return calculateStatIndSum(values)/values.length;
		}
		function calculateStatIndMedian(values) {
			values.sort( function(a,b) {return a - b;} );
			let half = Math.floor(values.length/2);
			if(values.length % 2) {
				return values[half];
			} else {
				return (values[half-1] + values[half]) / 2.0;
			}
		}
		function calculateStatIndStdDev(values) {
			let n = values.length ;
			if (n>1) {
				let mean = calculateStatIndMean(values);
				let qvar = 0;
				for (let i=0; i<n; i++) {
					qvar += Math.pow((mean-values[i]),2);
				}
				return Math.sqrt(qvar/(n-1));
			} else {
				return 0;
			}
		}
		function calculateStatIndCoefVar(values) {
			return calculateStatIndStdDev(values)/calculateStatIndMean(values);
		}
	}

	// ***********************************************************************************************************
	//                                           Columns
	// ***********************************************************************************************************

	/**
	 * Insert a column to the left of the column icol
	 * @param {int|null} icol null if the column goes in last position
	 * @returns { {updated_cols: string[], cols_in_circ_ref: string[], updated_graphs: int[], errors: string[]} }
	 */
	function insertColumn(icol = null) {
		let $header = $XMLcodeLD.find('header');
		let $cols = $header.find('column');
		let $rows_stats = $XMLcodeLD.find('row, stat');
		let nb_col = $cols.length;
		let new_code = createFXCode();
		let xml_col = `<column type="num"><code>${new_code}</code><name>${new_code}</name></column>`;

		if (icol !== null && icol < nb_col) { // insert a column before another one
			$cols.eq(icol).before(xml_col);
			$rows_stats.each(function () {
				$(this).find('value').eq(icol).before('<value></value>');
			});
		} else { // insert the column at the end
			$header.append(xml_col);
			$rows_stats.each(function () {
				$(this).append('<value/>');
			});
		}
		// recompile the formulas that have the column code in their dependencies
		// in order to transform the dependency as a function in the compiled (a rare bug could happen)
		for (let code in globalLD.columns) {
			if (globalLD.columns[code].dependencies.includes(new_code)) {
				updateJSObjectColFormulas(code);
			}
		}
		// the new column has no values, treat it as a constant to update dependant columns
		return cascadeUpdateDataOnInput("constants", [new_code]);
	}

	/**
	 * Shift a column
	 * @param {int|string} icol
	 * @param {1|-1} direction to the left (direction -1) or to the right (direction +1)
	 */
	function moveColumn(icol, direction) {
		icol = parseInt(icol);
		// header
		if (direction === 1) {
			$XMLcodeLD.find("column").eq(icol+direction).after($XMLcodeLD.find("column").eq(icol)); // header
			$XMLcodeLD.find("row").each(function() {
				$(this).find("value").eq(icol+direction).after($(this).find("value").eq(icol)); // cells
			});
			$XMLcodeLD.find("stat").each(function() {
				$(this).find("value").eq(icol+direction).after($(this).find("value").eq(icol)); // stst indicators
			});
		}
		else if (icol !== 0) { // ne pas déplacer à G si déjà au taquet
			$XMLcodeLD.find("column").eq(icol+direction).before($XMLcodeLD.find("column").eq(icol));
			$XMLcodeLD.find("row").each(function() {
				$(this).find("value").eq(icol+direction).before($(this).find("value").eq(icol));
			});
			$XMLcodeLD.find("stat").each(function() {
				$(this).find("value").eq(icol+direction).before($(this).find("value").eq(icol));
			});
		}
	}

	/**
	 * A code of constant or columns must be unique
	 * @param {string} code
	 * @returns {string} "col","const"
	 */
	function existsFXCode(code) {
		let exists = "";
		$XMLcodeLD.find('column').children('code').each(function () {
			if (code === $(this).text()) {
				exists = "col";
				return false;
			}
		});
		$XMLcodeLD.find('constant').children('name').each(function () {
			if (code === $(this).text()) {
				exists = "const";
				return false;
			}
		});
		return exists;
	}

	/**
	 * @param {string} prefix e.g. "data"
	 * @return {string}
	 */
	function createFXCode(prefix = "data") {
		let i = 1;
		let code = prefix + "1";
		while (existsFXCode(code)) {
			i++;
			code = prefix + i.toString();
		}
		return code;
	}

	/**
	 * @param {string} col_code
	 * @return {null | int}
	 */
	function getIcolFromCode(col_code) {
		let icol = null;
		$XMLcodeLD.find('column').each(function(i){
			if ($(this).find('code').text() === col_code) {
				icol = i;
				return false;
			}
		});
		return icol;
	}

	/**
	 * @param {int} icol
	 * @return {number}
	 */
	function getNbValuesInCol(icol) {
		let $rows = $XMLcodeLD.find("dataset").find("row");
		let nb_vals = 0;
		$rows.each(function() {
			if ($(this).find("value").eq(icol).text() !== "") {
				nb_vals++;
			}
		});
		return nb_vals;
	}


	/**
	 * @returns {[{code : string, scientific: boolean, digits: number, type: string, error: boolean, formula: string}]}
	 */
	function getColumnsFormats() {
		let columns_formats = [];
		$XMLcodeLD.find('column').each(function() {
			let sc_format = $(this).attr('scientific_notation');
			let digits = $(this).attr('digits');
			if (digits === "" || digits === undefined || isNaN(parseInt(digits))) {
				digits = null;
			} else {
				digits = parseInt(digits);
			}
			if (sc_format === "true") {
				sc_format = true;
			} else if (digits === null) {
				sc_format = null;
			} else {
				sc_format = false;
			}

			columns_formats.push({
				code: $(this).find("code").text(),
				scientific: sc_format,
				digits: digits,
				type: $(this).attr('type'),
				error: $(this).attr('error'),
				formula: $(this).find('formula').text().trim()
			});
		});
		return columns_formats;
	}

	/**
	 * @param {int} icol
	 * @param {string: "num"|"txt"} type
	 * @param {string} name
	 * @param {string} code
	 * @param {string} unite
	 * @param {string} formula
	 * @param {boolean} scientific_display
	 * @param {int|""} nb_digits
	 * @param {boolean} remove_txt_values
	 * @return {{
	 * 		updated_cols: string[],
	 * 		cols_in_circ_ref: string[],
	 * 		updated_graphs : int[],
	 * 		errors: string[]}} ["undef_symbol"|"circ_ref"]
	 */
	function updateColumnProperties(icol, type, name, code, unite ="", formula="", scientific_display=false, nb_digits="", remove_txt_values=false) {
		let $column = $XMLcodeLD.find('column').eq(icol);
		let old_code = $column.find('code').text();
		let old_type = $column.attr('type');
		if (remove_txt_values) {
			$XMLcodeLD.find("row").each(function() {
				let val = $(this).find("value").eq(icol).text();
				if (val !== "" && isNaN(parseFloat(val))) {
					$(this).find("value").eq(icol).text("");
				}
			});
		}
		let col_attributes = "", xml_unite = "", xml_formula = "";
		if (type === "num")  {
			col_attributes = `scientific_notation="${scientific_display}" digits="${nb_digits}"`;
			xml_unite = `<unit>${escapeXML(unite)}</unit>`;
			xml_formula = `<formula>${escapeXML(formula)}</formula>`;
		}
		let xml_col = `<column type="${type}" ${col_attributes}><code>${code}</code><name>${escapeXML(name)}</name>${xml_unite}${xml_formula}</column>`;
		$column.replaceWith(xml_col);

		// propagate the modifications
		if (code !== old_code) {
			// update globalLD.columns if necessary
			if (globalLD.columns.hasOwnProperty(old_code)) {
				globalLD.columns[code] = globalLD.columns[old_code];
				delete globalLD.columns[old_code];
			}
			cascadeColumnRecoding(old_code, code);
		}
		if (type !== old_type) {
			if (type === "txt") {
				deletePlots(code);
				// delete in JSObject-model
				if (globalLD.columns.hasOwnProperty(code)) {
					delete globalLD.columns[code];
				}
			}
			// recompile the formulas that have the column code in their dependencies
			// in order to transform the dependency as a constant/column in the compiled
			for (let code in globalLD.columns) {
				if (globalLD.columns[code].dependencies.includes(code)) {
					updateJSObjectColFormulas(code);
				}
			}
			return cascadeUpdateDataOnInput("data", [code]);
		} else {
			let graphs_to_update = getModifiedGraphsFromCodes([code]);
			return {
				updated_cols: [code],
				cols_in_circ_ref: [],
				updated_graphs: graphs_to_update,
				errors: []
			}
		}
	}

	/**
	 * When the user renames a column, propagate the renaming in the plots & the columns formulas
	 * i.e. replace the col_codes in all col_formulas => no new error of circ_ref or undef_symbol to handle
	 * @param old_code
	 * @param new_code
	 */
	function cascadeColumnRecoding(old_code, new_code) {
		// propagate in column formulas
		for (let code in globalLD.columns) {
			if (globalLD.columns[code].dependencies.includes(old_code)) {
				// Search and replace in the formula string
				let re = new RegExp('\\b' + old_code + '\\b', 'g');
				let icol = getIcolFromCode(code);
				let $formula = $XMLcodeLD.find('header').find("column").eq(icol).find("formula");
				let new_formula = $formula.text().replaceAll(re, new_code);
				$formula.text(new_formula);
				// Update the dependencies and the compiled function
				updateJSObjectColFormulas(code);
			}
		}
		// propagate in plots
		$XMLcodeLD.find('plot').children('x, y, ux, uy').each(function () {
			if ($(this).text() === old_code) {
				$(this).text(new_code);
			}
		});
	}

	/**
	 * change, add or delete the formula of a column
	 * possible errors : parse_error, circ_ref, undef_symbol
	 * @param icol
	 * @param formula
	 * @param update_values
	 * @returns { {updated_cols: string[], cols_in_circ_ref: string[], updated_graphs: int[], errors: string[]} }
	 */
	function updateColFormula(icol, formula, update_values = true) {
		// XML
		let $col = $XMLcodeLD.find('header').find('column').eq(icol);
		let col_code = $col.find('code').text();
		$col.find('formula').remove();
		$col.append(`<formula>${escapeXML(formula)}</formula>`);
		// JSObject
		let errors = [];
		try {
			updateJSObjectColFormulas(col_code);
		} catch (e) {
			errors.push(e);
		}
		if (formula === "" && !update_values) {
			return {
				updated_cols: [col_code],
				cols_in_circ_ref: [],
				updated_graphs: [],
				errors: []
			};
		}
		let modified = cascadeUpdateDataOnInput("col_formula", [col_code]);
		errors = errors.concat(modified.errors);
		return {
			updated_cols: modified.updated_cols,
			cols_in_circ_ref : modified.cols_in_circ_ref,
			updated_graphs: modified.updated_graphs,
			errors: errors
		};
	}

	// ***********************************************************************************************************
	//                                           Rows
	// ***********************************************************************************************************

	/**
	 * @param {int|null} irow - if null, insert at last position
	 * @param {int} nb_added_rows
	 * @param {boolean} cascade
	 * @returns { {updated_cols: string[], cols_in_circ_ref: string[], updated_graphs: int[], errors: string[]} }
	 */
	function insertRows(irow, nb_added_rows, cascade = true) {
		let $rows = $XMLcodeLD.find('row');
		let nb_cols = $XMLcodeLD.find('column').length;
		let nb_rows = $rows.length;
		// build the XML
		let row = "<row>" + '<value></value>'.repeat(nb_cols) + '</row>';
		let new_rows = row.repeat(nb_added_rows);
		// modify the XML code
		if (irow !== null && irow < nb_rows) { // insert a row before another one
			$XMLcodeLD.find('row').eq(irow).before(new_rows) ;
		} else if (nb_rows > 0) { // insert at the end
			$rows.eq(nb_rows-1).after(new_rows) ;
		} else {
			$XMLcodeLD.find('header').after(new_rows) ;
		}
		// displayed rows
		if (globalLD.max_display_rows && nb_rows <= globalLD.max_display_rows) {
			globalLD.max_display_rows = Math.max(globalLD.max_display_rows, nb_rows+Math.min(nb_added_rows, default_max_display_rows));
		} else { // all rows are displayed
			if (nb_added_rows > default_max_display_rows) { // big increment
				globalLD.max_display_rows = nb_rows + default_max_display_rows;
			}
		}
		if (cascade) {
			return cascadeUpdateDataOnInput("added_rows");
		}
	}

	/**
	 * Sort the rows of the table by a column data
	 * @param {int} icol
	 * @param {int} direction
	 */
	function sortRowsByColumn(icol, direction) {
		let type = $XMLcodeLD.find('column').eq(icol).attr('type');
		let val, data = [];
		$XMLcodeLD.find('row').each(function() {
			val = $(this).find("value").eq(icol).text() ;
			if (type === 'num') {
				if (val === "" || isNaN(val)) {
					val = "" ;
				}
				else {
					val = parseFloat(val);
				}
			} else {
				val = val.toLowerCase();
			}
			if (val === undefined) {
				val = "";
			}
			data.push([val, $(this)]) ;
		});
		// tri du tableau
		data.sort(function(a,b) {
			let nameA = a[0];
			let nameB = b[0];
			// empty values are at the end of the column
			if (nameA === "" && nameB !== "") { return 1; }
			if (nameB === "" && nameA !== "") { return -1; }
			if (nameA === "" && nameB === "") { return 0; }
			if (nameA < nameB) { return -1*direction ; }
			if (nameA > nameB) { return 1*direction ; }
			return 0; //default return value (no sorting)
		});
		// Update rows of datapoints in XML
		$XMLcodeLD.find('row').remove();
		for (let i=0 ; i<data.length ; i++) {
			$XMLcodeLD.find('dataset').append(data[i][1]) ;
		}
		// There is no use to update datapoints of the plots in the JSon model updateJSObjectPlotsAndBounds()
	}

	// ***********************************************************************************************************
	//                                           Calculus of data
	// ***********************************************************************************************************

	/**
	 * On a changed value of { some constants | a column formula | some data in the table | a row | a col } update all the dependents objects
	 * @param {string} inputs_type constants, col_formula, data, deleted_rows, added_rows
	 * @param {[string]} inputs constants 	 => [symbols of constants]
	 * 							col_formula  => [single col code]
	 * 							data 		 => [col codes]
	 * 							deleted_cols => [col codes]
	 * 							added_rows 	 => [col codes]
	 * 							deleted_rows => []
	 * @return {{
	 * 		updated_cols: string[],
	 * 		cols_in_circ_ref: string[],
	 * 		updated_graphs : int[],
	 * 		errors: string[]}} ["undef_symbol"|"circ_ref"]
	 */
	function cascadeUpdateDataOnInput(inputs_type, inputs= []) {
		// 1- build the subgraph of the input-dependent columns that must be updated
		// this subgraph is an object { col_code: [dependencies] } built from globalLD.columns[symbol].dependencies
		let col_subgraph = {};
		if (inputs_type === "deleted_rows" || inputs_type === "added_rows") {
			// here, the subgraph must include all the columns with a formula because there may be references between rows (relative indexes)
			// the performance could be optimized by treating only the rows that change, but the algorithm could be quite complicated
			for (const [code, col] of Object.entries(globalLD.columns)) {
				col_subgraph[code] = [];
				col.dependencies.forEach(dependency => { // add only dependencies that are formula columns
					if (dependency in globalLD.columns) {
						col_subgraph[code].push(dependency);
					}
				});
			}
		} else {
			if (inputs_type === "col_formula") { // the input column goes into the graph
				col_subgraph[inputs[0]] = [];
			}
			let parent_nodes = [...inputs]; // initially = the modified inputs
			let child_nodes; // the n+1 nodes
			do {
				child_nodes = [];
				parent_nodes.forEach(function(node) {
					for (const [code, col] of Object.entries(globalLD.columns)) { // only formula-columns
						if (col.dependencies.includes(node)) {      // {code => col} is a child of input
							if (col_subgraph[code] === undefined) { // the child-node is not in the subgraph
								col_subgraph[code] = [];            // add the child-node in the subgraph
								child_nodes.push(code);             // go through this child-node in the next iteration
							}
							if (!col_subgraph[code].includes(node)) { // add the dependency in the child-node of the graph
								col_subgraph[code].push(node);
							}
						}
					}
				});
				parent_nodes = [...child_nodes];
			} while (child_nodes.length > 0);
			// remove the circ_ref errors for each column of the subgraph
			for (const [code] of Object.entries(col_subgraph)) {
				let icol = getIcolFromCode(code);
				if (icol) {
					let $formula = $XMLcodeLD.find("column").eq(icol).find('formula');
					if ($formula.length && $formula.attr('error') === 'circ_ref') {
						$formula.removeAttr('error');
					}
				}
			}
			// remove the inputs from dependencies for constants, deleted_cols or non-formula-columns
			if (inputs_type !== "col_formula") {
				inputs.forEach(function(input) {
					col_subgraph = removeDependencyFromGraph(col_subgraph, input);
				});
			}
		}

		// 2- topological sort: in the subgraph, determine an optimized sequence for updating the columns that have a formula
		// Sources: https://www.youtube.com/watch?v=cIBFEhD77b4&t=431s
		let cols_to_update = [];
		let added_node;
		do {
			added_node = false;
			for (const [code, dependencies] of Object.entries(col_subgraph)) {
				if (dependencies.length === 0) {
					cols_to_update.push(code);
					delete col_subgraph[code];
					col_subgraph = removeDependencyFromGraph(col_subgraph, code);
					added_node = true;
					break;
				}
			}
		} while (added_node);

		// 3- separate the columns in the subgraph that are part of cycles and those that are not
		/* easy idea: make a second topological sort on the reverse graph => extract the dependant nods that are not included in cycles
		   this method is not optimal : it can't detect the nodes between 2 cycles and that are not included in cycles
		   but it will be sufficient for most cases - better could be done using Tarjan or Kosaraju algorithms */
		let reverse_col_subgraph = {};
		for (const [code] of Object.entries(col_subgraph)) {
			reverse_col_subgraph[code] = [];
		}
		for (const [code, dependencies] of Object.entries(col_subgraph)) {
			dependencies.forEach(col => {
				reverse_col_subgraph[col].push(code);
			});
		}
		// console.log("reverse subgraph", JSON.stringify(reverse_col_subgraph));

		let circ_ref_dependant_cols = [];
		do {
			added_node = false;
			for (const [code, dependencies] of Object.entries(reverse_col_subgraph)) {
				if (dependencies.length === 0) {
					circ_ref_dependant_cols.push(code);
					delete reverse_col_subgraph[code];
					reverse_col_subgraph = removeDependencyFromGraph(reverse_col_subgraph, code);
					added_node = true;
					break;
				}
			}
		} while (added_node);

		let cols_in_circ_ref = []; // the remaining columns
		for (const [code] of Object.entries(reverse_col_subgraph)) {
			cols_in_circ_ref.push(code);
		}
		cols_to_update = cols_to_update.concat(circ_ref_dependant_cols);
		// console.log("cols_to_update", cols_to_update, "cols_in_circ_ref", cols_in_circ_ref, "circ_ref_dependant_cols", circ_ref_dependant_cols);

		// 4- update the values for the cols in cols_to_update, cols_in_circ_ref and circ_ref_dependant_cols
		let errors = [];
		cols_in_circ_ref.forEach(function(code) {
			let icol = getIcolFromCode(code);
			$XMLcodeLD.find("column").eq(icol).find('formula').attr('error', 'circ_ref');
			updateColumnValuesWithFormula(icol);
			if (code === inputs[0]) {
				errors.push("circ_ref");
			}
		});
		cols_to_update.forEach(function(code) {
			let icol = getIcolFromCode(code);
			try {
				updateColumnValuesWithFormula(icol);
			} catch (e) {
				console.log("error", e, "column", code);
				if (e === "undef_symbol" &&
					(inputs_type === "constants" || inputs_type === "deleted_cols" ||
						(inputs_type === "col_formula" && code === inputs[0]))) {
					errors.push(e);
				}
			}
		});

		// 5- manage the other columns that may have changed
		let updated_cols = [...cols_to_update].concat(cols_in_circ_ref);
		if (inputs_type === "data" || inputs_type === "added_rows") {
			inputs.forEach(function(input) {
				let icol = getIcolFromCode(input);
				updated_cols.push(input);
				updateStatIndicators("", icol);
			});
		} else if (inputs_type === "deleted_rows") {
			// a column may have changed when deleting a row if a value was deleted
			$XMLcodeLD.find('column').each(function(icol) {
				let col_code = $(this).find("code").text();
				if (!updated_cols.includes(col_code)) {
					updated_cols.push(col_code);
					updateStatIndicators("", icol);
				}
			});
		}

		// 6- update the graphs depending on the columns or constants
		let graphs_to_update = [];
		if (inputs_type === "constants" || inputs_type === "deleted_cols") {
			graphs_to_update = getModifiedGraphsFromCodes(inputs);
		}
		if (graphs_to_update.length === 0) {
			graphs_to_update = getModifiedGraphsFromCodes(updated_cols);
		} else { // concatenate efficiently two arrays without duplicates
			graphs_to_update = [...new Set([...graphs_to_update, ...getModifiedGraphsFromCodes(updated_cols)])];
		}
		if (inputs_type === "deleted_cols") {
			inputs.forEach(col_code => deletePlots(col_code));
			if (updated_cols.length === 0) {
				updated_cols = inputs;
			} else { // concatenate efficiently two arrays without duplicates
				updated_cols = [...new Set([...updated_cols, ...inputs])];
			}
		}
		graphs_to_update.forEach(function(igraph) {
			updateJSObjectPlotsAndBounds(igraph);
		});

		return {
			updated_cols: updated_cols,
			cols_in_circ_ref: cols_in_circ_ref,
			updated_graphs: graphs_to_update,
			errors: errors
		};

		/**
		 * remove the symbol from the dependencies of the nodes of the graph
		 * @param {array} graph col_code => [dependencies]
		 * @param {string} symbol a column code or a constant name
		 * @returns {array} col_code => [dependencies]
		 */
		function removeDependencyFromGraph(graph, symbol) {
			for (const [code, dependencies] of Object.entries(graph)) {
				graph[code] = dependencies.filter(function(val) {
					return val !== symbol;
				});
			}
			return graph;
		}
	}

	/**
	 * Given an array of col_codes, return the graphs that are dependant of these col_codes
	 * @param {array} modified_codes array of column codes or constant names {string}
	 * @returns {array} array of graphs {int}
	 */
	function getModifiedGraphsFromCodes(modified_codes) {
		let modified_graphs = [];
		let $graphs = $XMLcodeLD.find("graph");
		$graphs.each(function(igraph) {
			let uses_col = false;
			let $plots = $(this).find("plot");
			$plots.each(function() {
				let x = $(this).find("x").text();
				let y = $(this).find("y").text();
				let ux = $(this).find("ux").text();
				let uy = $(this).find("uy").text();
				if (   (x && modified_codes.includes(x))
					|| (y && modified_codes.includes(y))
					|| (ux && modified_codes.includes(ux))
					|| (uy && modified_codes.includes(uy))
				) {
					uses_col = true;
					return false;
				}
			})
			if (uses_col) {
				if (!modified_graphs.includes(igraph)) {
					modified_graphs.push(igraph);
				}
			}
		});
		return modified_graphs;
	}

	/**
	 * Calculate the values of a column
	 * @param {int} icol
	 */
	function updateColumnValuesWithFormula(icol) {

		/** @type {Boolean} Can be updated by any function representing the values of a column. */
		let hasUsedIgnoredValue;
		let hasUsedMissingValue;

		const $col = $XMLcodeLD.find("column").eq(icol);
		const $rows = $XMLcodeLD.find('row');
		let $formula = $col.find('formula');
		
		// errors management
		let error = $formula.attr('error');
		if (error === 'undef_symbol') {
			$formula.removeAttr('error');
		} else if (error) {
			console.log(`1- Data of column ${icol} can't be calculated because of: ` + error);
			$rows.each(function() {
				$(this).find('value').eq(icol).text("###");
			});
			updateStatIndicators("", icol);
			return;
		}

		const formula = $formula.text();
		if (formula === "") { // empty the column
			$rows.each(function() {
				$(this).find('value').eq(icol).text('');
			});
			updateStatIndicators("", icol);
			return;
		}

		const col_code = $col.find('code').text();
		const compiled = globalLD.columns[col_code].cfunc;

		// build the default scope for the formula
		const dependencies = globalLD.columns[col_code].dependencies;
		const columns = parseColumnsFromLabdoc();
		const columnsByCode = columns.byCode;
		let constants = new Map(); // name: value
		$XMLcodeLD.find('constant').each(function () {
			constants.set($(this).find('name').text(), parseFloat($(this).find('value').text()));
		});
		let defaultScope = {
			rac: Math.sqrt,
			ln: Math.log,
			log: Math.log10,
			count: math.setSize
		};
		dependencies.forEach(function(symbol) {
			if (constants.has(symbol)) {
				defaultScope[symbol] = constants.get(symbol);
			} else if (!columnsByCode.has(symbol)) {
				console.log(`2- Data of column ${icol} can't be calculated because of an undef_symbol`);
				$rows.each(function() {
					$(this).find('value').eq(icol).text("###");
				});
				$formula.attr('error', 'undef_symbol');
				updateStatIndicators("", icol);
				throw "undef_symbol";
			}
		});

		// get the values of columns to be used in the formula
		const columnsByRank = columns.byRank;
		const valuesPerColumn = readUsedValues(columnsByRank, dependencies);

		// compute and update the values of the column
		$rows.each(function(irow) {
			hasUsedIgnoredValue = false; // may be changed by buildRowScope
			hasUsedMissingValue = false;
			// complete the scope of the function for a specific cell
			let scope = buildRowScope(defaultScope, irow, columnsByCode.keys(), valuesPerColumn);
			let result;
			if (scope === null) {
				result = "";
			} else {
				try {
					result = compiled.eval(scope);
					if (isNaN(result)) {
						result = "###";
					}
				} catch(e) {
					console.log(irow, "error", e);
					result = "###";
				}
			}
			let $val = $(this).find('value').eq(icol);
			if (hasUsedMissingValue) {
				result = "";
			}
			$val.text(result);
			if (hasUsedIgnoredValue) {
				$val.attr('ignored', 'true');
			} else {
				$val.removeAttr('ignored');
			}
		});
		updateStatIndicators("", icol);

		/**
		 * send an object to easily transform col-code <=> icol
		 * @return {Object}
		 */
		function parseColumnsFromLabdoc() {
			let columnsByCode = new Map(); // code => rank
			let columnsByRank = new Map(); // rank => code
			$XMLcodeLD.find('column').each(function (index) {
				if ($(this).attr('type') === "num") {
					let code = $(this).find('code').text().trim();
					columnsByCode.set(code, index);
					columnsByRank.set(index, code);
				}
			});
			return {
				byCode: columnsByCode,
				byRank: columnsByRank
			};
		}

		/**
		 * Returns a nested Map with all the values from the columns that are used in the formula.
		 * @param {Map} columnsByRank
		 * @param {Array} dependencies
		 * @return {Map} result.get(columnCode).get(rowNumber) is a { value: String, ignored: Boolean }
		 */
		function readUsedValues(columnsByRank, dependencies) {
			let values = new Map();
			if (typeof dependencies === 'undefined' || !dependencies || dependencies.length === 0) {
				return values;
			}
			$XMLcodeLD.find('row').each(function(irow) {
				$(this).children('value').each(function(icol) {
					const code = columnsByRank.get(icol);
					if (columnsByRank.has(icol) && dependencies.includes(code)) {
						const value = $(this).text().trim();
						if (!values.has(code)) {
							values.set(code, new Map());
						}
						let codeValues = values.get(code);
						codeValues.set(
							irow,
							{
								value: value,
								ignored: Boolean($(this).attr('ignored'))
							}
						);
					}
				});
			});
			return values;
		}

		/**
		 * Add the local context to the default scope, by adding properties for each column.
		 * Side effect: changes the hasUsedIgnoredValue and hasUsedMissingValue variable.
		 * @param {Object} defaultScope
		 * @param {int} irow
		 * @param {Map} columnNames
		 * @param {Map} valuesPerColumn
		 * @return {null | Object}
		 */
		function buildRowScope(defaultScope, irow, columnNames, valuesPerColumn) {
			/**
			 * Slice last empty rows
			 * @param {Map<unknown, unknown>} mapValues: values to be sliced
			 * @returns {Map<unknown, unknown>} values sliced
			 */
			function slicedValues(mapValues) {
				let arrayValues = [...mapValues].sort((a, b) => a[0] - b[0]);
				while (arrayValues.slice(-1)[0][1]['value'] === "") {
					arrayValues.pop();
					if (arrayValues.length === 0) {
						break;
					}
				}
				return new Map(arrayValues);
			}
			let scope = Object.assign({numline: 1+irow}, {numrow: 1+irow}, defaultScope);
			for (let code of columnNames) {
				if (valuesPerColumn.has(code)) {
					let values = slicedValues(valuesPerColumn.get(code));
					/**
					 * Main function that defines what col_code(x,y) should return
					 * col_code(x) -> the value of col_code shifted by x
					 * col_code(x:y) -> an array of col_code values
					 * @param {int|string} x
					 * @param {int|string|null} y
					 * @returns {number|*}
					 */
					scope[code] = function (x, y=null) {
						/**
						 * Returns the value of a column with a given index in cas of a single input (like data(1))
						 * @param {int} irow: target row index
						 * @param {int} relativeIndex: the relative index
						 * @returns {object}: the corresponding value
						 */
						function returnValueForUniqueInput(irow, relativeIndex) {
							const shiftedIndex = irow + relativeIndex;
							if (values.has(shiftedIndex) && values.get(shiftedIndex).value !== "") {
								let v = values.get(shiftedIndex);
								if (v.ignored) {
									hasUsedIgnoredValue = true;
								}
								return Number(v.value);
							} else {
								hasUsedMissingValue = true;
								return NaN;
							}
						}

						/**
						 * Returns the values of a column with a given index in case of multiple inputs (like data(1:))
						 * @param {int} irow: target row index
						 * @param {array} arrayOfRelativeIndexes
						 * @returns {number[]|*[]}: the corresponding values
						 */
						function returnValuesForMultipleInputs(irow, arrayOfRelativeIndexes) {
							// no input given
							if (arrayOfRelativeIndexes.length === 0) {
								hasUsedMissingValue = true;
								return [NaN];
							}
							// each value takes exactly one of the following attributes: empty, ignored, missing or clean
							// array of non-empty, non-ignored and non-missing values
							let arrayOfCleanValues = [];
							// array of non-empty, non-missing but ignored values
							let arrayOfIgnoredValues = []
							for (const relativeIndex of arrayOfRelativeIndexes) {
								const shiftedIndex = irow + relativeIndex;
								if (!values.has(shiftedIndex)) {
									hasUsedMissingValue = true;
									return [NaN];
								}
								if (values.get(shiftedIndex).value !== "") {
									let v = values.get(shiftedIndex);
									if (v.ignored) {
										arrayOfIgnoredValues.push(Number(v.value));
									} else {
										arrayOfCleanValues.push(Number(v.value));
									}
								}
							}
							if (arrayOfCleanValues.length > 0) {
								return arrayOfCleanValues;
							}
							// at this line, there are no longer clean or missing values
							if (arrayOfIgnoredValues.length > 0) {
								hasUsedIgnoredValue = true;
								return arrayOfIgnoredValues;
							}
							// at this line, there are only empty values
							hasUsedMissingValue = true;
							return [NaN];
						}
						
						if (x === parseInt(x) && y === null) {
							return returnValueForUniqueInput(irow, x);
						} else if (x.hasOwnProperty('_data') && y === null) {
							return returnValuesForMultipleInputs(irow, x.toArray());
						} else if (x === 'fitex_array_start' && y === 'fitex_array_end') {
							return returnValuesForMultipleInputs(
								irow, 
								[...values.keys()].map(i => i - irow)
							);
						} else if (x === 'fitex_array_start' && y === parseInt(y)) {
							return returnValuesForMultipleInputs(
								irow, 
								[...Array(math.max(0, 1 + irow + y)).keys()].map(i => i - irow)
							);
						} else if (x === parseInt(x) && y === 'fitex_array_end') {
							return returnValuesForMultipleInputs(
								irow, 
								[...Array(math.max(0, values.size - irow - x)).keys()].map(i => i + x)
							);
						}
					};
				}
			}
			return scope;
		}
	}

	// ***********************************************************************************************************
	//                                           Graphs
	// ***********************************************************************************************************

	/**
	 * @param {int} igraph
	 * @param {string} graphType 'X-Y' or 'histogram'
	 */
	function addGraph(igraph, graphType='X-Y') {
		let graphXML;
		if (graphType === 'histogram') {
			graphXML = `<graph type="${graphType}"><name/><xmin/><xmax/><ymin/><ymax/><bins_number/></graph>`;
		} else {
			graphXML = '<graph><name/><xmin/><xmax/><ymin/><ymax/></graph>';
		}
		$XMLcodeLD.find('LD').append(graphXML);
		globalLD.graphs[igraph] = { pfuncs: [] };
	}

	/**
	 * @param {int} igraph
	 * @param {string} name
	 * @param {string} xmin
	 * @param {string} xmax
	 * @param {boolean} xlog
	 * @param {string} xlabel
	 * @param {string} ymin
	 * @param {string} ymax
	 * @param {boolean} ylog
	 * @param {string} ylabel
	 * @param {boolean} y2
	 * @param {string} y2min
	 * @param {string} y2max
	 * @param {boolean} y2log
	 * @param {string} y2label
	 * @param {array} plots
	 * @param {string} bins_number
	 * @param {boolean} frequency
	 */
	function updateGraph(igraph, name, xmin, xmax, xlog, xlabel, ymin, ymax, ylog, ylabel, y2, y2min, y2max, y2log, y2label,plots, bins_number, frequency){
		let $graph = $XMLcodeLD.find('graph').eq(igraph);
		let graphType = getGraphType(igraph);
		$graph.attr("y2", y2 ? "true" : "false");
		let old_yscale = $graph.find('yscale').text();
		// remove all non mandatory tags
		$graph.find('xscale, xlabel, yscale, ylabel, y2min, y2max, y2scale, y2label, bins_number').remove();
		$graph.removeAttr('frequency');
		// update
		$graph.children('name').text(escapeXML(name));
		$graph.children('xmin').text(xmin);
		$graph.children('xmax').text(xmax);
		$graph.children('ymin').text(ymin);
		$graph.children('ymax').text(ymax);
		
		// histogram specific
		if (graphType === 'histogram') {
			if (frequency) {
				$graph.attr('frequency', 'true');
			}
			bins_number = parseInt(bins_number);
			if (bins_number) {
				$graph.append(`<bins_number>${bins_number}</bins_number>`);
			}
		}

		if (xlog) {
			$graph.append('<xscale>log</xscale>');
		}
		let force_update_pfuncs_data = false; // if the yscale changes, the pfuncs must be redrawn to apply the pow(10, pfunc)
		if (ylog) {
			$graph.append('<yscale>log</yscale>');
			if (old_yscale !== 'log') {
				force_update_pfuncs_data = true;
			}
		} else if (old_yscale === 'log') {
			force_update_pfuncs_data = true;
		}

		$graph.append(`<xlabel>${escapeXML(xlabel)}</xlabel>`);
		$graph.append(`<ylabel>${escapeXML(ylabel)}</ylabel>`);

		if (y2) {
			$graph.append(`<y2min>${escapeXML(y2min)}</y2min>`);
			$graph.append(`<y2max>${escapeXML(y2max)}</y2max>`);
			$graph.append(`<y2label>${escapeXML(y2label)}</y2label>`);
			if (y2log) {
				$graph.append('<y2scale>log</y2scale>');
			}
		}

		// plots
		$graph.find('plot').remove()
		let xml_plots = "";
		let existing_iplots = [];
		plots.forEach(function(plot){
			xml_plots += `<plot color="${plot.color}" curve="${plot.curve}" axis="${plot.axis}"><x>${plot.x}</x><y>${plot.y}</y><ux>${plot.ux}</ux><uy>${plot.uy}</uy></plot>`;
			existing_iplots.push(plot.iplot);
		})
		$graph.append(xml_plots);

		// update the fit_iplot of the functions
		let $funcs = $XMLcodeLD.find('graph').eq(igraph).find('function');
		// reinitialize the fit_iplot attributes of the deleted plots
		$funcs.each(function () {
			if (!existing_iplots.includes($(this).attr("fit_iplot"))) {
				$(this).attr("fit_iplot", 0);
			}
		});
		// update the other fit_iplot attributes
		$funcs.each(function () {
			$(this).attr("fit_iplot",
				existing_iplots.filter(iplot => iplot < Number($(this).attr("fit_iplot"))).length);
		});

		updateJSObjectPlotsAndBounds(igraph, force_update_pfuncs_data);
	}

	/**
	 * @param {int} igraph
	 */
	function moveUpGraph(igraph) {
		if (igraph > 0) {
			let $graph = $XMLcodeLD.find('graph').eq(igraph);
			$XMLcodeLD.find('graph').eq(igraph-1).before($graph);
			let graph = globalLD.graphs[igraph];
			globalLD.graphs[igraph] = globalLD.graphs[igraph-1];
			globalLD.graphs[igraph-1] = graph;
		}
	}

	/**
	 * @param {int} igraph
	 */
	function getGraphType(igraph) {
		let $graph = $XMLcodeLD.find('graph').eq(igraph);
		return $graph.attr("type") || 'X-Y'
	}

	/**
	 * @param {int} igraph
	 */
	function deleteGraph(igraph) {
		$XMLcodeLD.find('graph').eq(igraph).remove();
		globalLD.graphs.splice(igraph, 1);
	}

	/**
	 * @param {string} code column code
	 */
	function deletePlots(code) {
		$XMLcodeLD.find('graph').each(function() {
			let deleted_plots = [];
			$(this).find('plot').each(function (iplot) {
				if ($(this).find('x').text() === code || $(this).find('y').text() === code) { // delete plot
					$(this).remove();
					deleted_plots.push(iplot);
				} else {
					if ($(this).find('ux').text() === code) { // delete uncertainty
						$(this).find('ux').text('');
					}
					if ($(this).find('uy').text() === code) {
						$(this).find('uy').text('');
					}
				}
			});
			$(this).find('function').each(function () {
				let fit_iplot = Number($(this).attr("fit_iplot"));
				if (deleted_plots.includes(fit_iplot)) {
					$(this).attr("fit_iplot", 0);
				} else {
					$(this).attr("fit_iplot",
						fit_iplot - deleted_plots.filter(iplot => iplot < fit_iplot).length);
				}
			});
		});
	}

	/**
	 * @param {int} igraph
	 * @param {int} ifunc
	 */
	function deletePFunc(igraph, ifunc) {
		// JS object
		if (typeof globalLD.graphs[igraph] !== 'undefined') {
			globalLD.graphs[igraph].pfuncs.splice(ifunc, 1);
		}
		// XML model
		$XMLcodeLD.find('graph').eq(igraph).find('function').eq(ifunc).remove();
	}

	/**
	 * @param {string} formula
	 * @param {int} igraph
	 * @param {int} ifunc
	 * @param {string} description
	 */
	function updatePFunc(formula, igraph, ifunc, description = "") {
		// update globalLD: JSObject & XML (formula & description)
		let $pfuncs = $XMLcodeLD.find('graph').eq(igraph).find('function')
		let $pfunc = $pfuncs.eq(ifunc);
		if (!$pfunc.length && formula !== "") { // pfunc creation
			let color, used_colors = new Set();
			let fx_colors = globalLD.view.fx_colors;
			$pfuncs.each(function() {
				used_colors.add($(this).attr("color"));
			});
			let available_colors = fx_colors.filter(col => !used_colors.has(col));
			if (available_colors.length) {
				color = available_colors[0];
			} else {
				color = fx_colors[0];
			}

			$XMLcodeLD.find('graph').eq(igraph).append(`<function color="${color}" fit_iplot="0"><formula>${formula}</formula></function>`);
			$pfunc = $XMLcodeLD.find('graph').eq(igraph).find('function').eq(ifunc);
		} else if ($pfunc.length && formula === "") { // pfunc deletion
			$pfunc.remove();
			globalLD.graphs[igraph].pfuncs.splice(ifunc, 1);
			return;
		} else { // pfunc update
			$pfunc.find('formula').html(escapeXML(formula));
			$pfunc.find('description').remove();
		}
		if (description) {
			$pfunc.append(`<description>${description}</description>`);
		}
		updateJSObjectPFuncs(igraph, ifunc); // update the JS object with .cfunc, .dependencies, .is_fy

		// update the parameters in XML
		let set_parameters = []; // keep the values of the set parameters: name => value
		$pfunc.find('parameter').each(function () {
			set_parameters[$(this).children('name').text()] = {
				value: $(this).find('value').text(),
				uncertainty: $(this).find('uncertainty') ? $(this).find('uncertainty').text() : ''
			};
			$(this).remove();
		});
		let constants = []; // name => value
		$XMLcodeLD.find('constant').each(function () {
			constants[$(this).find('name').text()] = $(this).find('value').text();
		});
		let xml_params = "";
		let dependencies = globalLD.graphs[igraph].pfuncs[ifunc].dependencies;
		dependencies.sort(function (a, b) { // case insensitive sort
			return a.toLowerCase().localeCompare(b.toLowerCase());
		});
		let value;
		let uncertainty = '';
		let func_parameter = "x";
		if (globalLD.graphs[igraph].pfuncs[ifunc].is_fy) {
			func_parameter = "y";
		}
		dependencies.forEach(function(symbol) {
			if (constants[symbol] === undefined && symbol !== func_parameter) {
				if (set_parameters[symbol] !== undefined) {
					value = set_parameters[symbol].value;
					uncertainty = set_parameters[symbol].uncertainty;
				} else {
					value = "0";
				}
				xml_params += `<parameter ${symbol !== func_parameter ? 'adjustable="true"' : ""}><name>${symbol}</name><value>${value}</value><uncertainty>${uncertainty}</uncertainty></parameter>`;
			}
		});
		$pfunc.append(xml_params);

		// update the data
		updateJSObjectPFuncData(igraph, ifunc);
	}

	/**
	 * @param {int} igraph
	 * @param {int} ifunc
	 * @param {string} param
	 * @param {string} val
	 */
	function updatePFuncParam(igraph, ifunc, param, val) {
		$XMLcodeLD.find('graph').eq(igraph).find('function').eq(ifunc).find('parameter').each(function () {
			if ($(this).children('name').text() === param) {
				$(this).find('value').text(val);
				return false;
			}
		});
		updateJSObjectPFuncData(igraph, ifunc);
	}
	
	function updateUncertaintyModel ($parameter, uncertaintyValue) {
		let $uncertainty = $parameter.find('uncertainty');
		if ($uncertainty.length === 0) {
			$parameter.append($("<uncertainty>", {}));
		}
		$parameter.find('uncertainty').text(uncertaintyValue);
	}

	/**
	 * @param {int} igraph
	 * @param {int} ifunc
	 * @param {string} param
	 * @returns {string} adjustable
	 */
	function getPFuncParamAdjustable(igraph, ifunc, param) {
		return $XMLcodeLD.find('graph').eq(igraph).find('function').eq(ifunc).find('parameter')
			.children('name').filter((i, e) => $(e).text() === param).parent().attr("adjustable");
	}

	/**
	 * @param {int} igraph
	 * @param {int} ifunc
	 * @param {string} param
	 * @param {string} adjustable
	 */
	function updatePFuncParamAdjustable(igraph, ifunc, param, adjustable) {
		$XMLcodeLD.find('graph').eq(igraph).find('function').eq(ifunc).find('parameter')
			.children('name').filter((i, e) => $(e).text() === param).parent().attr("adjustable", adjustable);
	}

	/**
	 * @param {int} igraph
	 * @param {int} ifunc
	 * @param {string} bound
	 * @param {string} val
	 */
	function updatePFuncInterval(igraph, ifunc, bound, val) {
		let $xmlFunc = $XMLcodeLD.find('graph').eq(igraph).find('function').eq(ifunc);
		$xmlFunc.find(bound).remove();
		$xmlFunc.append(`<${bound}>${val}</${bound}>`);
		updateJSObjectPFuncData(igraph, ifunc);
	}

	/**
	 * Determine if a function must be limited to an interval of the x-axis
	 * @param {int} igraph
	 * @param {int} ifunc
	 * @param {Number} xmin lower bound of the graph x axis
	 * @param {Number} xmax higher bound of the graph x axis
	 * @returns {number[] | null} null means that there is no valid interval
	 */
	function calculatePFuncInterval(igraph, ifunc, xmin, xmax) {
		let xmlFunc = $XMLcodeLD.find('graph').eq(igraph).find('function').eq(ifunc);
		let xstart = parseFloat(xmlFunc.find('xstart').text());
		let xend = parseFloat(xmlFunc.find('xend').text());
		if (!isNaN(xstart) && !isNaN(xend) && xstart >= xend) { // si les deux sont définis, ils doivent être des nombres ordonnées
			return [xmin, xmax];
		}
		if (!isNaN(xstart) && xstart > xmin) {
			xmin = xstart;
		}
		if (!isNaN(xend) && xend < xmax) {
			xmax = xend;
		}
		if (xmin >= xmax) { // Warning: it is possible to have xmin > xmax
			return null;
		}
		return [xmin, xmax];
	}

	/**
	 * Build the code of a parametered functions with values of constants and parameters
	 * @param {int} igraph
	 * @param {int} ifunc
	 * @returns {object} {string} => {number | string}
	 */
	function buildPFuncScope(igraph, ifunc) {
		// build the scope of the function with constants & parameters
		let $func = $XMLcodeLD.find('graph').eq(igraph).find('function').eq(ifunc);
		let $formula = $func.find('formula');
		let error = $formula.attr('error');
		if (error === 'undef_symbol') {
			$formula.removeAttr('error');
		} else if (error) {
			return {};
		}
		let constants = new Map(); // name: value
		$XMLcodeLD.find('constant').each(function () {
			constants.set($(this).find('name').text(), parseFloat($(this).find('value').text()));
		});
		let params = new Map(); // name: value
		$func.find('parameter').each(function () {
			params.set($(this).find('name').text(), parseFloat($(this).find('value').text()));
		});
		let scope = {
			x: 0,
			rac: Math.sqrt,
			ln: Math.log,
			log: Math.log10,
			count: math.setSize
		};
		let dependencies = globalLD.graphs[igraph].pfuncs[ifunc].dependencies;
		let func_parameter = "x";
		if (globalLD.graphs[igraph].pfuncs[ifunc].is_fy) {
			func_parameter = "y";
		}
		dependencies.forEach(function(symbol) {
			if (constants.has(symbol))  {
				scope[symbol] = constants.get(symbol);
			} else if (params.has(symbol)) {
				scope[symbol] = params.get(symbol);
			} else if (symbol !== func_parameter) { // should not happen
				$formula.attr('error', 'undef_symbol');
				throw "undef_symbol";
			}
		});
		return scope;
	}

	/**
	 * compute the distance (sigma or Khi2 if there are Uy uncertainties) between data points and a function
	 * @param {number} igraph
	 * @param {number} iplot
	 * @param {number} ifunc
	 * @returns {{dist: number | null, khi2: boolean, u_dist: number, nb_pts: number}}
	 */
	function computeDistance(igraph, iplot, ifunc) {
		let distance = {dist: null, khi2: false, u_dist: 0, nb_pts: 0};
		let pfunc = globalLD.graphs[igraph].pfuncs[ifunc];
		let exp_data = globalLD.graphs[igraph].plots[iplot]; // note: deep clone is useless when using filter or map
		// histogram specific
		let graphType = getGraphType(igraph);
		if (graphType === 'histogram') {
			exp_data = [];
			let xyBins;
			let minClassesValue;
			let maxClassesValue;
			let jsGraph = globalLD.graphs[igraph];
			let bins_number = parseFloat($XMLcodeLD.find("graph").eq(igraph).find('bins_number').text());
			bins_number = isNaN(bins_number) ? null : bins_number;
			let frequency = $XMLcodeLD.find("graph").eq(igraph).attr('frequency') === 'true';
			({ xyBins, bins_number, minClassesValue, maxClassesValue } = globalLD.model.initializeBinsData(jsGraph.plots, bins_number));
			let series = jsGraph.plots[iplot].map(quadruplet => quadruplet[0]).sort((a, b) => a > b);
			exp_data = globalLD.model.fillBinsValues(xyBins, series, bins_number, minClassesValue, maxClassesValue, frequency);
		}
		
		let n = exp_data.length; // total number of data points (N)
		if (n === 0) {
			return distance;
		}
		let $XMLGraph =  $XMLcodeLD.find('graph').eq(igraph);
		let is_x_log = $XMLGraph.find('xscale').text() === 'log';
		let is_y_log = $XMLGraph.find('yscale').text() === 'log';
		// calculate the xmin & xmax for the plot
		let xmin, xmax;
		exp_data.forEach((pt) => {
			if (typeof xmin === 'undefined' || pt[0] < xmin) {
				xmin = pt[0];
			}
			if (typeof xmax === 'undefined' || pt[0] > xmax) {
				xmax = pt[0];
			}
		});
		// filter the data to keep only the points whose abscisses are in the interval defined for the function
		// in case of log axes, the coordinates must be positive
		let interval = calculatePFuncInterval(igraph, ifunc, xmin, xmax);
		if (interval === null) {
			return distance;
		}
		if (interval[0] > xmin || interval[1] < xmax || is_x_log || is_y_log) {
			exp_data = exp_data.filter(function (pt) {
				return (
					pt[0] >= interval[0]
					&& pt[0] <= interval[1]
					&& (!is_x_log || pt[0] > 0)
					&& (!is_y_log || pt[1] > 0)
				);
			});
			n = exp_data.length;
		}
		distance.nb_pts = n;
		// distance can be computed only if N > p
		let parametersWithInitialValues = getFunctionParams(igraph, ifunc);
		let p = Array.from(parametersWithInitialValues.keys()).length;
		if (n <= p) {
			return distance;
		}

		// compute distance
		let sum = 0;
		let khi2 = parseInt($XMLcodeLD.find('settings').find('reducedKhi2').text()) === 1;
		let uxToy = parseInt($XMLcodeLD.find('settings').find('uxShiftedToy').text()) === 1;
		// general case y=f(x)
		if (!pfunc.is_fy) {
			let cfunc = pfunc.cfunc; // compiled function
			if (!cfunc) {
				return distance;
			}
			try {
				let scope = buildPFuncScope(igraph, ifunc);
				let x, y, ux, uy, ei, ui, delta_x, delta_y, y0, y1, y2, ux_y;
				// try to compute khi2 : sum{(ei/Ui)²}/(N-p) ± sqrt(2/(N-p))
				// each point must have a global uncertainty value (combined Uy & Ux) > 0
				if (khi2) {
					for (let i = 0; i < n; i++) {
						x = exp_data[i][0];
						y = exp_data[i][1];
						ux = exp_data[i][2];
						uy = exp_data[i][3];
						scope["x"] = x;
						y1 = cfunc.eval(scope);
						ei = y - y1;
						// compute ui
						ui = uy;
						if (ux !== 0 && uxToy) { // combine ux with uy by adding to uy the ux vertical componant following the tangent
							delta_x = 0.675 * ux / 2; // interval corresponding to 50% chance of error, assuming that ux = 2 * std dev
							scope["x"] = x - delta_x;
							y0 = cfunc.eval(scope);
							scope["x"] = x + delta_x;
							y2 = cfunc.eval(scope);
							delta_y = (y2 - y0) / 2;
							ux_y = ux * delta_y / delta_x;
							ui = Math.sqrt(uy * uy + ux_y * ux_y);
						}
						if (!isNaN(ui) && ui !== 0) {
							ei = ei / ui;
						} else {
							khi2 = false;
							sum = 0;
							break;
						}
						sum += ei * ei;
					}
				}
				if (!khi2) { // compute sigma: sqrt(sum{ei²}/(N-p))
					for (let i = 0 ; i < n ; i++) {
						x = exp_data[i][0];
						y = exp_data[i][1];
						scope["x"] = x;
						y1 = cfunc.eval(scope);
						ei = y - y1;
						sum += ei*ei;
					}
				}
			} catch(e) {
				return distance;
			}
		}
		// case x=f(y) : pH functions => compute an approximation of sigma or khi2
		else {
			let pfunc_data = globalLD.graphs[igraph].pfuncs[ifunc].data;
			let n_pfunc = pfunc_data.length;
			if (n_pfunc <= 1) { // needs at least 2 points in the function
				return distance;
			}
			sum = computeSumXfY(pfunc_data, n_pfunc, exp_data, khi2); // khi2: sum{(ei/Ui)²}
			if (sum === false) {
				khi2 = false;
				sum = computeSumXfY(pfunc_data, n_pfunc, exp_data, khi2); // sigma: sum{ei²}
			}
		}

		let dist, u_dist = 0;
		if (khi2) {
			dist = sum/(n-p);
			u_dist = Math.sqrt(2/(n-p));
		} else {
			dist = Math.sqrt(sum/(n-p));
		}
		return {
			dist: dist,
			khi2: khi2,
			u_dist: u_dist,
			nb_pts: n
		};
	}

	/**
	 * Compute sum{(ei/Ui)²} or sum{ei²} in order to evaluate khi2 or sigma for a x=f(y) function
	 * @param {float[][]} pfunc data ordered on the x
	 * @param {int} n_pfunc length of the previous array
	 * @param {float[][]} exp_data ordered on the x
	 * @param {boolean} khi2
	 * @returns {float | boolean}
	 */
	function computeSumXfY(pfunc, n_pfunc, exp_data, khi2) {
		let x, y, yf, a, b, ei, Ui, Ux, Ux_y;
		let sum = 0;
		let i_pf = 0;
		let xf1 = pfunc[0][0],
			xf2 = pfunc[1][0],
			yf1 = pfunc[0][1],
			yf2 = pfunc[1][1];
		let nokhi2 = false;

		exp_data.forEach(point => {
			x = point[0];
			y = point[1];
			while (x > xf2 && i_pf < n_pfunc - 2) {
				i_pf++;
				xf1 = pfunc[i_pf][0];
				xf2 = pfunc[i_pf + 1][0];
				yf1 = pfunc[i_pf][1];
				yf2 = pfunc[i_pf + 1][1];
			}
			// a & b are the parameters of the tangent
			a = (yf2 - yf1) / (xf2 - xf1);
			b = yf1 - a * xf1;
			yf = a * x + b;
			ei = y - yf;
			// compute Ui
			if (khi2) {
				Ui = point[3]; // Uy
				Ux = point[2];
				if (Ux !== 0) { // combine Ux with Uy by adding to Uy the Ux vertical componant following the tangent
					Ux_y = Ux * a;
					Ui = Math.sqrt(Ui*Ui + Ux_y*Ux_y);
				}
				if (Ui !== 0) {
					ei = ei / Ui;
				} else { // for computing khi2, each point must have a global uncertainty value (combined Uy & Ux) > 0
					nokhi2 = true;
					return;
				}
			}
			sum += ei*ei;
		});
		if (nokhi2) {
			return false;
		}
		return sum;
	}

	/**
	 * Computes the parameters of a model that are as close as possible to the experimental data
	 * @param igraph
	 * @param ifunc
	 * @param iplot
	 * @returns {array} the list of free parameters or error messages
	 */
	function autoFit(igraph, ifunc, iplot=0){
		let graph = globalLD.graphs[igraph];
		let pfunc = graph.pfuncs[ifunc];
		let $XMLFunc = $XMLcodeLD.find('graph').eq(igraph).find('function').eq(ifunc);
		// Slice data according to [xstart, xend]
		let xmin = graph.plots[iplot][0][0];
		let xmax = graph.plots[iplot].slice(-1)[0][0];
		let xstart = parseFloat($XMLFunc.find('xstart').text());
		let xend = parseFloat($XMLFunc.find('xend').text());
		if (xstart > xmax || xend < xmin){
			return ["error", __("L'intervalle [xmin, xmax] n'est pas cohérent avec les données sélectionnées")];
		}
		xstart = !isNaN(xstart) ? Math.max(xstart, xmin) : xmin;
		xend = !isNaN(xend) ? Math.min(xend, xmax) : xmax;
		let plot = graph.plots[iplot].map((e, i) => e.concat(i));
		let first_data_index = plot.find(e => e[0] >= xstart)[4];
		let last_data = plot.find(e => e[0] > xend);
		let last_data_index = last_data === undefined ? plot.slice(-1)[0][4] : last_data[4] - 1;
		let data = {
			x: new Float64Array(plot.map(x => x[0]).slice(first_data_index, last_data_index + 1)),
			y: new Float64Array(plot.map(x => x[1]).slice(first_data_index, last_data_index + 1)),
		};
		if (data.x.length === 0){
			return ["error", __("Aucune valeur de 'x' n'est disponible")];
		}
		// Get parameters
		let parametersWithInitialValues = getFunctionParams(igraph, ifunc);
		let parameters = Array.from(parametersWithInitialValues.keys());
		let initialValues = Array.from(parametersWithInitialValues.values());
		let normalizedInitialValues = initialValues.map(initialValue => initialValue ? 1 : initialValue);
		let normalizedFuncOfParameters = parameters => (
			x => funcOfParameters(
				parameters.map(
					(p, idx) => initialValues[idx] ? p * initialValues[idx] : p
				)
			)(x)
		);
		// Test if at least one parameter is free
		if (parameters.length === 0){
			return ["error", __("Aucun paramètre n'est libre")];
		}
		// Test if there are enough data points
		if (data.x.length <= parameters.length) {
			return ["error", __("Trop de paramètres libres ({{nb_p}}) par rapport au nombre de points expérimentaux ({{nb_pt}})", {nb_p:parameters.length, nb_pt:data.x.length}) ];
		}
		// Function variable should be x (for the moment)
		if (pfunc.is_fy){
			return ["error", __("La variable de la fonction doit être 'x'")];
		}
		let scope = buildPFuncScope(igraph, ifunc);
		// Dynamic function of parameters
		let funcOfParameters = eval(`
			(function ([${parameters.join(', ')}]) {
				var scope_update = { ${parameters.map(p => p + ":" + p).join(', ')} };
				return (x) => pfunc.cfunc.eval({...scope, ...scope_update, x: x});
			})
		`);
		// Weights
		let ws;
		let are_y_weights = plot.map(x => x[3])
			.slice(first_data_index, last_data_index + 1)
			.reduce((acc, cur) => acc && !isNaN(cur) && cur !== 0, true);
		if (are_y_weights) {
			ws = Array.from(new Float64Array(plot.map(x => math.inv(x[3]*x[3])).slice(first_data_index, last_data_index + 1)));
		}
		// Levenberg-Marquardt configuration
		const options = {
			damping: undefined,
			parametersDelta: undefined,
			maxIterations: undefined,
			parameters: {
				initial: normalizedInitialValues,
				max: undefined,
				min: undefined
			},
			weights: ws,
			forceParameterConvergence: true,
			errorScaling: parseInt($XMLcodeLD.find('settings').find('upNormalized').text()) === 1,
		};
		let fittingOutputs = null;
		try {
			fittingOutputs = CurveFitting.levenbergMarquardt(data, normalizedFuncOfParameters, options);
			fittingOutputs.parameters = fittingOutputs.parameters.map(
				(p, idx) => initialValues[idx] ? p * initialValues[idx] : p
			);
			fittingOutputs.parameterUncertainties = fittingOutputs.parameterUncertainties.map(
				(pu, idx) => initialValues[idx] ? pu * Math.abs(initialValues[idx]) : pu
			);
		} catch {
			return ["error", __("Erreur lors de l'exécution de l'algorithme d'ajustement automatique")];
		}
		// TODO Remove alertGently in model
		if ((fittingOutputs.convergence && fittingOutputs.convergence === 'failed') || fittingOutputs.parameterUncertainties.length === 0) {
			alertGently(__("L'algorithme d'ajustement automatique n'a pas convergé, essayez de modifier les valeurs initiales ou bien de bloquer certains paramètres."));
		} else if (fittingOutputs.parameterUncertainties.some(v => isNaN(v))) {
			alertGently(__("L'algorithme d'ajustement automatique a convergé mais les incertitudes n'ont pas pu être calculées."));
		}
		return parameters.map((e, i) => ({
			name: e, 
			value: fittingOutputs.parameters[i],
			uncertainty: fittingOutputs.parameterUncertainties.length > 0 ? fittingOutputs.parameterUncertainties[i] : '',
			iterations: fittingOutputs.iterations
		}));
	}

	/**
	 * Get free params from a model and returns their initial values
	 * @param igraph
	 * @param ifunc
	 * @return {Map<any, any>} name and initial value
	 */
	function getFunctionParams(igraph, ifunc){
		const result = new Map;
		$XMLcodeLD.find('graph').eq(igraph).find('function').eq(ifunc).find('parameter')
			.filter((i, e) => $(e).attr('adjustable') === 'true')
			.each(function () {
			result.set(
				$(this).children('name').text(),
				parseFloat($(this).find('value').text())
			);
		});
		return result;
	}

	/**
	 * Histogram specific case: computes number of bins and extreme values
	 * @param plots
	 * @param bins_number
	 * @returns {{maxClassesValue: number, minClassesValue: number, bins_number: number}}
	 */
	function computeBinsNumberAndMinMaxClasses(plots, bins_number) {
		let allData = plots.filter(plt => plt.length > 0).flatMap(plt => plt.map(coord => coord[0]));
		let minClassesValue = Math.min(...allData);
		let maxClassesValue = Math.max(...allData);
		if (!bins_number) {
			bins_number = Math.ceil(Math.sqrt(allData.length));
		}
		return {
			bins_number: bins_number, 
			minClassesValue: minClassesValue, 
			maxClassesValue: maxClassesValue
		}
	}

	/**
	 * Histogram specific case: computes histogram parameters and initializes bins
	 * @param plots
	 * @param bins_number
	 * @returns {{maxClassesValue: number, minClassesValue: number, xyBins: *[], binsWidth: number}}
	 */
	function initializeBinsData(plots, bins_number) {
		let xyBins = [];
		let minClassesValue;
		let maxClassesValue;
		({ bins_number, minClassesValue, maxClassesValue } = computeBinsNumberAndMinMaxClasses(plots, bins_number));
		let binsWidth = (maxClassesValue - minClassesValue) / bins_number;
		for (let iBins = 0; iBins < bins_number; iBins ++) {
			xyBins.push([minClassesValue + iBins * binsWidth + binsWidth / 2, 0]);
		}
		return {
			xyBins: xyBins,
			bins_number: bins_number,
			minClassesValue: minClassesValue, 
			maxClassesValue: maxClassesValue, 
			binsWidth: binsWidth
		}
	}

	/**
	 * Histogram specific case: fills bins with number or frequency
	 * @param xyBins
	 * @param series
	 * @param bins_number
	 * @param minClassesValue
	 * @param maxClassesValue
	 * @param frequency
	 * @returns {*}
	 */
	function fillBinsValues(xyBins, series, bins_number, minClassesValue, maxClassesValue, frequency) {
		for (let x of series) {
			let iBins = 0;
			while (bins_number * (parseFloat(x) - minClassesValue) >= iBins * (maxClassesValue - minClassesValue)) {
				iBins += 1;
			}
			iBins = Math.min(Math.max(0, iBins - 1), xyBins.length - 1);
			xyBins[iBins][1] += 1;
		}
		if (frequency) {
			xyBins = xyBins.map(coordinates => [coordinates[0], coordinates[1] / series.length]);
		}
		return xyBins;
	}

	return { // API
		forbidden_codes: forbidden_codes,
		updateConstants: updateConstants,
		updateCellValue: updateCellValue,
		updateCellValuesFromMatrix: updateCellValuesFromMatrix,
		getCellValues: getCellValues,
		deleteCellValues: deleteCellValues,
		exportDataToCSV: exportDataToCSV,
		exportDataToXLSX: exportDataToXLSX,
		updateStatIndicators: updateStatIndicators,
		insertColumn: insertColumn,
		deleteEmptyColumn: deleteEmptyColumn,
		moveColumn: moveColumn,
		existsFXCode: existsFXCode,
		getNbValuesInCol: getNbValuesInCol,
		getColumnsFormats: getColumnsFormats,
		updateColumnProperties: updateColumnProperties,
		updateColFormula: updateColFormula,
		insertRows: insertRows,
		sortRowsByColumn: sortRowsByColumn,
		addGraph: addGraph,
		updateGraph: updateGraph,
		moveUpGraph: moveUpGraph,
		getGraphType: getGraphType,
		deleteGraph: deleteGraph,
		deletePFunc: deletePFunc,
		updatePFunc: updatePFunc,
		updatePFuncParam: updatePFuncParam,
		updateUncertaintyModel: updateUncertaintyModel,
		getPFuncParamAdjustable: getPFuncParamAdjustable,
		updatePFuncParamAdjustable: updatePFuncParamAdjustable,
		updatePFuncInterval: updatePFuncInterval,
		autoFit: autoFit,
		computeDistance: computeDistance,
		initializeBinsData: initializeBinsData,
		fillBinsValues: fillBinsValues
	};
});
