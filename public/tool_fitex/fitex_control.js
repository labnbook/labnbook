/*
This file contains the control functions that are initialized at the creation of the LD through displayDataSet()
All controls are attached to $("#labdoc_content_"+id_ld) = $HTMLcontentLD or $table and then filtered by a class
  Ex: $HTMLcontentLD.on("click", ".class", function () {  });
A usual control function does the following actions:
  - checks the input values
  - updates the model with the input values by calling model functions: [ let updated = ] globalLD.model.updateXXX()
  - saves a local version of the LD: saveLocalLDVersion(id_ld);
  - updates the view: refreshDataSet(updated) or globalLD.view.xxx
 */

/**
 * update the display of a whole dataset in $('#labdoc_content_'+id_ld)
 * @param {int} id_ld
 * @param  {boolean} edition
 */
function displayDataSet(id_ld, edition) {
	let globalLD = global_tab_ld[id_ld];
	globalLD.model = fitex_model(id_ld, edition); // Init the model
	globalLD.view = fitex_view(id_ld, edition); // Init the display
	initFXControls(id_ld, edition);
}

function initFXControls(id_ld, edition) {
	"use strict";

	const warning_for_large_nb_of_rows = 4000;
	const globalLD = global_tab_ld[id_ld];
	const $XMLcodeLD = $(globalLD.ld_content);
	const $HTMLcontentLD = $("#labdoc_content_"+id_ld);
	const $table = $HTMLcontentLD.find(".fx_table");

	// clean previous controls
	$HTMLcontentLD.off(); // useful ??
	$HTMLcontentLD.find(".context-menu-root").remove(); // the context menu doesn't kill itself - useful ??

	// Init all necessary controls
	initDefaultControls();
	if (!edition) {
		initControlsInViewMode();
	} else {
		setTableMainMenuControls();
		setImportDataControls();
		setExportPanelControls();
		setSettingsPanelControls();
		setConstantsPanelControls();
		setColMenus();
		setColPropPanelControls();
		setFormulasRowControls();
		setRowsControls();
		setEditableCellControls();
		setSpecialKeysControls();
		setGraphControls();
		setGraphPropertiesControls();
		setPFuncsControls();
		setColorPickerControls();
		setFormulasControls();
	}

	function validateOnEnter(selector) {
		$HTMLcontentLD.on("keydown", selector, function (event) {
			if (event.which === 13) { // enter
				event.preventDefault();
				$(this).find(".fx_validate_btn").click();
				return false;
			}
		});
	}

	function initDefaultControls() {

		setHighlightCellsBehavior();

		// grey-highlight columns on hover (for the rows it is handled by CSS)
		$HTMLcontentLD.on("mouseenter", ".fx_col_header", function () {
			let icol = $(this).data('col');
			$table.find(".fx_cell[data-col='" + icol + "']").css("background-color", "#eee");
		}).on('mouseleave', ".fx_col_header", function () {
			let icol = $(this).data('col');
			$table.find(".fx_cell[data-col='" + icol + "']").css("background-color", "");
		});

		// show all rows
		$HTMLcontentLD.on("click", ".fx_show_rows", function () {
			let nb_rows = $XMLcodeLD.find('row').length;
			if (nb_rows > warning_for_large_nb_of_rows) {
				if (!confirm(__("Attention, votre tableau contient {{nb_rows}} lignes. Leur affichage peut causer des ralentissements du navigateur.",{nb_rows:nb_rows}))) {
					return;
				}
			}
			globalLD.max_display_rows = null;
			globalLD.view.displayTableRows();
		});

		// init Sizing Bar
		let $resizable_div = $HTMLcontentLD.find(".fx_table_wrapper");
		let $sizing_bar = $resizable_div.parent().find(".sizing-bar");
		const min_height = 80;
		let previous_height;
		let slider_height;

		function updateSlideHeight() { // updates slider_height
			if ($table.outerWidth() >= $resizable_div.width()) {
				slider_height = 20; // the horizontal slider
			} else {
				slider_height = 2;
			}
		}

		$resizable_div.resizable({
			handles: {'s,n': $sizing_bar},
			minHeight: min_height,
			start: function() { // set the max_height as a function of the table height
				if (!edition) {
					updateSlideHeight();
					let height = $resizable_div.height();
					$resizable_div.css('height', height + 'px');
					let max_height = $table.outerHeight() + slider_height;
					$resizable_div.css('max-height', max_height + 'px');
				}
			},
			stop: function() { // need to resize the datasets LD containing graphs
				if (edition) {
					let height = $resizable_div.height();
					$XMLcodeLD.find('dataset').attr('display_height', height);
					saveLocalLDVersion(id_ld);
				}
			}
		});
		// sizing bar auto-height
		$sizing_bar.on("dblclick", function (e) { // auto height
			e.stopPropagation();
			let height = $resizable_div.height();
			updateSlideHeight();
			let max_height = $table.outerHeight() + slider_height;
			if (height !== max_height) {
				previous_height = height;
				$resizable_div.css('height', max_height + 'px');
				$resizable_div.css('max-height', 'none');
			} else {
				$resizable_div.css('height', previous_height + 'px');
			}
			if (edition) {
				let height = $resizable_div.height();
				$XMLcodeLD.find('dataset').attr('display_height', height);
				saveLocalLDVersion(id_ld);
			}
		});
	}

	function initControlsInViewMode() {
		// display the constants when hover the main menu of the table
		$HTMLcontentLD.on('mouseenter', ".fx_main_menu", function () {
			globalLD.view.displayConstantPanel();
		}).on('mouseleave', ".fx_main_menu", function () {
			$HTMLcontentLD.find(".fx_constant_panel").hide();
		});
	}

	function setTableMainMenuControls() { // !! this function is a mix of views & controls
		// object corresponding to all stat indicators
		let stat_ind = globalLD.view.stat_ind;
		let si_menu_items = {};
		stat_ind.forEach((val, key)=>{
			si_menu_items[key] = {
				name: `${val.name}  (${val.code}) <span class="fx_menu_mark fx_menu_mark_${key}"
					${$XMLcodeLD.find('stat[property="'+key+'"]').length ? 'style="display: inline"' : ''}>
					&nbsp;&nbsp;&#10003;</span>`,
				isHtmlName: true,
				callback: function () {
					toggleStatIndRow(key);
				}
			}
		});

		$HTMLcontentLD.contextMenu({
			selector: ".fx_main_menu",
			appendTo: $HTMLcontentLD,
			trigger: 'left',
			hideOnSecondTrigger: true,
			autoHide: false,
			position: function (opt) {
				opt.$menu.position({
					my: 'left top',
					at: 'left bottom',
					of: opt.$trigger,
					offset: "-1 -4"
				});
			},
			events: {
				show: function() { blurFormulaInput() }
			},
			items: {
				copy: {
					name: __("Copier des données"),
					callback: function() { copyCells(); }
				},
				paste: {
					name: __("Coller des données"),
					callback: function() { pasteDataInTable(); }
					// disabled: fitex_copy_paste.isClipboardEmpty
				},
				delete: {
					name: __("Supprimer des données / colonnes / lignes"),
					callback: function() { deleteItems(); }
				},
				sep0: "---------",
				import_data: {
					name: __("Importer des données..."),
					callback: function() { globalLD.view.displayImportPanel(); }
				},
				export_data: {
					name: __("Exporter toutes les données"),
					callback: function() { globalLD.view.displayExportPanel(); }
				},
				sep1: "---------",
				show_hide_formula: {
					name: `${__('Afficher la ligne des formules')}
						<span class="fx_menu_mark fx_menu_mark_formula" 
						${ $XMLcodeLD.find('dataset').attr('display_formula') === "true" ? 'style="display: inline"' : ''}
						>&nbsp;&nbsp;&#10003;</span>`,
					isHtmlName: true,
					callback: function() { toggleFormulaRow(); }
				},
				fold: {
					name: __('Afficher des indicateurs statistiques') + " ",
					items: si_menu_items
				},
				sep2: "---------",
				define_constants: {
					name: __("Définir des constantes..."),
					callback: function() { globalLD.view.displayConstantPanel(); }
				},
				sep3: "---------",
				cartesian: {
					name: __('Ajouter un graphique cartésien X-Y') + " ",
					callback: function () {
						insertGraph('X-Y');
					}
				},
				histogram: {
					name: __('Ajouter un histogramme') + " ",
					callback: function () {
						insertGraph('histogram');
					}
				},
				sep4: "---------",
				advanced_options: {
					name: __("Options d'ajustement..."),
					callback: function() { globalLD.view.displayFitSettings(); }
				},
			}
		});
	}

	/**
	 * Import a data file: csv, xlsx, ods
	 */
	function setImportDataControls() {

		const separators = ['comma', 'semicolon', 'tabulation'];

		$HTMLcontentLD.find("select[name='fx_import_panel_column_separator_select']").on("change", function(event) {
			if (event.target.value === 'auto') {
				$HTMLcontentLD.find("select[name='fx_import_panel_inner_column_separator_select']").val("none").change();
				$HTMLcontentLD.find("select[name='fx_import_panel_inner_column_separator_select']").attr('disabled', '');
			} else {
				$HTMLcontentLD.find("select[name='fx_import_panel_inner_column_separator_select']").removeAttr('disabled');
				$HTMLcontentLD.find("select[name='fx_import_panel_inner_column_separator_select'] option[value='" + event.target.value + "']").attr('disabled', '');
				for (let separator of separators) {
					if (separator !== event.target.value) {
						$HTMLcontentLD.find("select[name='fx_import_panel_inner_column_separator_select'] option[value='"+ separator +"']").removeAttr('disabled');
					}
				}
			}
		});

		$HTMLcontentLD.find("select[name='fx_import_panel_inner_column_separator_select']").on("change", function(event) {
			$HTMLcontentLD.find("select[name='fx_import_panel_column_separator_select'] option[value='" + event.target.value + "']").attr('disabled', '');
			for (let separator of separators) {
				if (separator !== event.target.value) {
					$HTMLcontentLD.find("select[name='fx_import_panel_column_separator_select'] option[value='"+ separator +"']").removeAttr('disabled');
				}
			}
		});

		$HTMLcontentLD.on("change", ".fx_file_import", function () {
			let $form = $HTMLcontentLD.find('.fx_import_panel form');
			let mode = $form.find('[name=mode] option:selected').val();
			mode = mode ? mode : 'overwrite';
			let has_header = $form.find('[name=has_header]').is(':checked');
			if (mode === 'overwrite' && !confirm(__("Attention, toutes les données pré-existantes du tableau seront écrasées."))) {
				return false;
			}
			$form.find('.fx_import_alerts').html('<i class="fa fa-spinner fa-pulse" aria-hidden="true"></i>');

			// pre-process the file
			let is_csv = this.files.length && this.files[0].name.match(/\.csv$/i);

			// csv import
			let import_column_separator;
			let import_inner_column_separator;
			if (is_csv) {
				let $import_panel = $HTMLcontentLD.find(".fx_import_panel");
				import_column_separator = $import_panel.find("select[name='fx_import_panel_column_separator_select'] :selected").val();
				import_inner_column_separator = $import_panel.find("select[name='fx_import_panel_inner_column_separator_select'] :selected").val();
				if (import_column_separator === 'comma') {
					import_column_separator = ',';
				} else if (import_column_separator === 'semicolon') {
					import_column_separator = ';';
				} else if (import_column_separator === 'tabulation') {
					import_column_separator = '\t';
				}
				if (import_inner_column_separator === 'comma') {
					import_inner_column_separator = ',';
				} else if (import_inner_column_separator === 'semicolon') {
					import_inner_column_separator = ';';
				} else if (import_inner_column_separator === 'tabulation') {
					import_inner_column_separator = '\t';
				} else if (import_inner_column_separator === 'none') {
					import_inner_column_separator = '';
				}
			}

			let reader = new FileReader();
			reader.onerror = function() {
				$form.find('.fx_import_alerts').html(__("Fichier inadapté") + __("&nbsp;: ") + reader.error);
			};
			reader.onload = function (e) {
				let data = e.target.result;
				if (is_csv && import_column_separator === 'auto' && data.match(/^[^\n"]+;/)) {
					data = "sep=;\n" + data; // force ";" as separator
				} else if (is_csv && import_column_separator !== 'auto') {
					data = "sep=" + import_column_separator + "\n" + data;
				}
				let values = [];
				try {
					let workbook;
					if (is_csv && import_column_separator !== 'auto') {
						workbook = XLSX.read(data, {type: 'binary', raw: true});
					} else {
						workbook = XLSX.read(data, {type: 'binary'});
					}
					let firstSheetName = workbook.SheetNames[0];
					let ws = workbook.Sheets[firstSheetName];
					values =  window.XLSX.utils.sheet_to_json(ws, {header: 1, raw: false, type: "string"}); // .filter(function(r) { return r.length > 0; });
				} catch {
					$form.find('.fx_import_alerts').html(__("Le fichier n'a pas pu être décodé"));
					return false;
				}
				if (values.length === 0) {
					$form.find('.fx_import_alerts').html(__("Aucune donnée trouvée"));
					return false;
				}

				if (is_csv && import_column_separator !== 'auto') {
					let startDataIdx = (has_header && values.length > 1) ? 1 : 0;
					if (import_inner_column_separator) {
						// fill empty slots
						for (let rowIdx = startDataIdx; rowIdx < values.length; rowIdx++) {
							for (let columnIdx = 0; columnIdx < values[rowIdx].length; columnIdx++) {
								if (!(columnIdx in values[rowIdx])) {
									values[rowIdx][columnIdx] = '';
								}
							}
						}
						// we assume that all rows are formatted in the same way, with at most, one header row
						// and that there are no columns in text format
						for (let columnIdx = values[startDataIdx].length - 1; columnIdx >= 0; columnIdx--) {
							let re = new RegExp(`${import_inner_column_separator}`, "g");
							let matches = values[startDataIdx][columnIdx].match(re);
							if (matches) {
								if (has_header) {
									let column_header = values[0][columnIdx];
									for (let matchesIdx = 0; matchesIdx < matches.length; matchesIdx++) {
										values[0].splice(columnIdx, 0, column_header);
									}
								}
								for (let rowIdx = startDataIdx; rowIdx < values.length; rowIdx++) {
									values[rowIdx][columnIdx] = values[rowIdx][columnIdx].split(import_inner_column_separator);
									values[rowIdx] = values[rowIdx].flat();
								}
							}
						}
					}
				}
				let updated = globalLD.model.updateCellValuesFromMatrix(values, null, has_header, mode);
				saveLocalLDVersion(id_ld);
				$(".fx_import_panel").hide();
				globalLD.view.toggleFXModal(false);
				refreshDataset(updated);
				if (mode === 'overwrite') {
					$HTMLcontentLD.find(".fx_graph").remove();
				}
			};
			if (is_csv && import_column_separator !== 'auto') {
				reader.readAsText(this.files[0]);
			} else {
				reader.readAsBinaryString(this.files[0]);
			}
		});
	}

	function exportData(format='csv', column_separator=',', decimal_sep='.') {
		let now = new Date();
		let filename = 'data_LNB_' + now.toISOString().substring(0, 9) + "_" + ("0" + now.getHours()).slice(-2) + 'h' + ("0" + now.getMinutes()).slice(-2);
		if (format === 'csv') {
			let encodedUri = encodeURI(globalLD.model.exportDataToCSV(column_separator, decimal_sep));
			let link = document.createElement("a");
			link.setAttribute("href", encodedUri);
			link.setAttribute("download", filename + ".csv");
			$HTMLcontentLD.append(link);
			link.click();
		} else if (format === 'xlsx') {
			XLSX.writeFile(globalLD.model.exportDataToXLSX(), filename + ".xlsx");
		}
	}

	function toggleFormulaRow() {
		let $formulaRow = $HTMLcontentLD.find(".fx_formula_row");
		// model
		($XMLcodeLD.find('dataset').attr('display_formula') === 'false') ?
			$XMLcodeLD.find('dataset').attr('display_formula', 'true') :
			$XMLcodeLD.find('dataset').attr('display_formula', 'false');
		saveLocalLDVersion(id_ld);
		// view
		$HTMLcontentLD.find(".context-menu-item .fx_menu_mark_formula").toggle();
		$formulaRow.toggle();
	}

	// statistical indicators rows toggle
	function toggleStatIndRow(si) {
		// model
		let si_row = $XMLcodeLD.find('stat[property="' + si + '"]');
		if (si_row.length === 0) {
			globalLD.model.updateStatIndicators(si); // calcul des valeurs et enregistrement dans le XML
		} else {
			$(si_row).remove(); // delete stat indicator (less to compute on update)
		}
		saveLocalLDVersion(id_ld);
		// view
		$HTMLcontentLD.find(".context-menu-item .fx_menu_mark_" + si).toggle(); // the menu is in the document and not in the LD
		globalLD.view.displayTableStatIndicators(); // redraw all stat indicators
	}

	function setSettingsPanelControls() {

		validateOnEnter(".fx_settings_panel");

		$HTMLcontentLD.on("change", ".fx_setting_khi2_reduced", function () {
			if (this.checked) {
				$HTMLcontentLD.find(".fx_setting_khi2_ux_y").removeAttr("disabled");
			} else {
				$HTMLcontentLD.find(".fx_setting_khi2_ux_y").attr("disabled", true);
			}
		});

		// cancel
		$HTMLcontentLD.on("click", ".fx_settings_cancel", function () {
			$HTMLcontentLD.find(".fx_settings_panel").hide();
			globalLD.view.toggleFXModal(false);
		});

		// validate the constants panel
		$HTMLcontentLD.on("click", ".fx_settings_panel .fx_validate_btn", function () {
			let $settings_panel = $HTMLcontentLD.find(".fx_settings_panel");
			let $XMLSettings = $XMLcodeLD.find('settings');
			$XMLSettings.find('reducedKhi2').text($settings_panel.find('.fx_setting_khi2_reduced')[0].checked ? 1 : 0);
			$XMLSettings.find('uxShiftedToy').text($settings_panel.find('.fx_setting_khi2_ux_y')[0].checked ? 1 : 0);
			if (globalLD.auto_fit) {
				$XMLSettings.find('upNormalized').text($settings_panel.find('.fx_setting_normalize')[0].checked ? 1 : 0);
			}
			globalLD.view.toggleFXModal(false);
			$settings_panel.hide();
			saveLocalLDVersion(id_ld);
			globalLD.view.displayGraphs(-1);
		})
	}
	function setExportPanelControls() {

		validateOnEnter(".fx_export_panel");

		// Select comma for the column separator disables comma for the decimal separator
		$HTMLcontentLD.find("select[name='fx_export_panel_column_separator_select']").on("change", function(event) {
			if (event.target.value === 'comma') {
				$HTMLcontentLD.find("select[name='fx_export_panel_decimal_separator_select'] option[value='comma']").attr('disabled', '');
			} else {
				$HTMLcontentLD.find("select[name='fx_export_panel_decimal_separator_select'] option[value='comma']").removeAttr('disabled');
			}
		});

		// Select comma for the decimal separator disables comma for the column separator
		$HTMLcontentLD.find("select[name='fx_export_panel_decimal_separator_select']").on("change", function(event) {
			if (event.target.value === 'comma') {
				$HTMLcontentLD.find("select[name='fx_export_panel_column_separator_select'] option[value='comma']").attr('disabled', '');
			} else {
				$HTMLcontentLD.find("select[name='fx_export_panel_column_separator_select'] option[value='comma']").removeAttr('disabled');
			}
		});
		
		// Select xlsx does everything auto
		$HTMLcontentLD.find("select[name='fx_export_panel_format_select']").on("change", function(event) {
			if (event.target.value === 'xlsx') {
				$HTMLcontentLD.find(".fx_export_panel_options_title").hide();
			} else {
				$HTMLcontentLD.find(".fx_export_panel_options_title").show();
			}
		});

		// cancel
		$HTMLcontentLD.on("click", ".fx_export_cancel", function () {
			$HTMLcontentLD.find(".fx_export_panel").hide();
			globalLD.view.toggleFXModal(false);
		});

		// validate the constants panel
		$HTMLcontentLD.on("click", ".fx_export_panel .fx_validate_btn", function () {
			let $export_panel = $HTMLcontentLD.find(".fx_export_panel");
			let export_format = $export_panel.find("select[name='fx_export_panel_format_select'] :selected").val();
			let export_column_separator= $export_panel.find("select[name='fx_export_panel_column_separator_select'] :selected").val();
			let export_decimal_separator = $export_panel.find("select[name='fx_export_panel_decimal_separator_select'] :selected").val();
			if (export_column_separator === 'comma') {
				export_column_separator = ',';
			} else if (export_column_separator === 'semicolon') {
				export_column_separator = ';';
			} else if (export_column_separator === 'tabulation') {
				export_column_separator = '\t';
			}
			if (export_decimal_separator === 'comma') {
				export_decimal_separator = ',';
			} else if (export_decimal_separator === 'dot') {
				export_decimal_separator = '.';
			}
			if (export_format === 'xlsx') {
				exportData('xlsx');
			} else if (export_format === 'csv') {
				exportData('csv', export_column_separator, export_decimal_separator);
			}
			globalLD.view.toggleFXModal(false);
			$(".fx_export_panel").hide();
		})
	}
	
	// add graph sub menu
	function insertGraph (graphType) {
		let igraph = $XMLcodeLD.find('graph').length;
		globalLD.model.addGraph(igraph, graphType);
		saveLocalLDVersion(id_ld);
		globalLD.view.displayGraphs(igraph);
		globalLD.view.displayGraphPropPanel(igraph, true, graphType);
	}
	
	function setConstantsPanelControls() {

		validateOnEnter(".fx_constant_panel");

		// Show the new constant row
		$HTMLcontentLD.on("click", ".fx_constant_panel_add", function() {
			globalLD.view.addConstantRowInPanel();
		});

		// suppress a constant
		$HTMLcontentLD.on("click", ".fx_constant_panel_del", function() {
			if (confirm(__('Supprimer la constante ?'))) {
				$(this).closest('tr').remove();
			}
		});

		// format a constant code on input
		$HTMLcontentLD.on("input", ".fx_constant_code", function() {
			let code = formatStringToAlphanum($(this).text());
			if ($(this).text() !== code) {
				$(this).text(code);
				// Place caret at the end of the contenteditable
				let range, selection;
				range = document.createRange(); //Create a range (a range is a like the selection but invisible)
				range.selectNodeContents(this); //Select the entire contents of the element with the range
				range.collapse(false); //collapse the range to the end point. false means collapse to end rather than the start
				selection = window.getSelection(); //get the selection object (allows you to change selection)
				selection.removeAllRanges(); //remove any selections already made
				selection.addRange(range); //make the range you have just created the visible selection
			}
		});

		// cancel
		$HTMLcontentLD.on("click", ".fx_constants_cancel", function () {
			$HTMLcontentLD.find(".fx_constant_panel").hide();
			globalLD.view.toggleFXModal(false);
		});

		// validate the constants panel
		$HTMLcontentLD.on("click", ".fx_constant_panel .fx_validate_btn", function () {
			let $c_panel = $HTMLcontentLD.find(".fx_constant_panel");
			let consts = [], code, val;
			let checked = true;
			// build an associative array with the new constants while checking the inputs
			$c_panel.find(".fx_constant").each(function () {
				code = $(this).find('td').eq(0).find('div').text();
				val = $(this).find('td').eq(1).find('div').text();
				if (code !== "" || val !== "") { // ne prend en compte que les lignes qui comportent des données
					code = formatStringToAlphanum(code);
					$(this).find('td').eq(0).find('div').text(code);
					val = formatFXInputNumValue(val);
					$(this).find('td').eq(1).find('div').text(val)
					if (!checkFXCode(code, "const")) {
						checked = false;
						return false;
					}
					// a constant code must be unique
					if (code in consts) {
						alertGently(__("Vous avez défini deux fois la même constante"));
						checked = false;
						return false;
					}
					if (val === "") {
						alertGently(__("Une valeur de constante est manquante"));
						checked = false;
						return false;
					}
					if (isNaN(Number(val))) {
						alertGently(__("La valeur {{val}} n'est pas reconnue", {val: val}));
						checked = false;
						return false;
					}
					consts[code] = val;
				}
			});
			if (checked) {
				let updated = globalLD.model.updateConstants(consts);
				saveLocalLDVersion(id_ld);
				globalLD.view.toggleFXModal(false);
				$c_panel.hide();
				refreshDataset(updated); // do not alert errors => special handling
				if (updated.errors.length) {
					alertGently(updated.errors.length + " " + __("colonne(s) non calculée(s) : symbole non reconnu."));
				}
			}
		});
	}

	function setColMenus() {
		// menu
		$HTMLcontentLD.contextMenu({
			selector: '.fx_col_menu',
			appendTo: $HTMLcontentLD,
			trigger: 'none',
			hideOnSecondTrigger: true,
			autoHide: false,
			position: function (opt) {
				opt.$menu.position({
					my: 'left top',
					at: 'left bottom',
					of: opt.$trigger
				});
			},
			events: {
				show: function() {
					blurFormulaInput();
					if (event && event.shiftKey) {
						return false;
					}
				}
			},
			items: {
				props: {
					name: __("Propriétés de la colonne") + __(" (DOUBLE-CLICK)"),
					callback: function(){ globalLD.view.displayColPropPanel($(this).data('col')); }
				},
				sep1: "---------",
				insertleft: {
					name: __("Insérer une colonne a gauche"),
					callback: function(){ insertColumn(parseInt($(this).data('col'))) }
				},
				insertright: {
					name: __("Insérer une colonne a droite"),
					callback: function(){ insertColumn(parseInt($(this).data('col')), 1) }
				},
				sep2: "---------",
				moveleft: {
					name: __("Déplacer à gauche"),
					callback: function(){
						globalLD.model.moveColumn($(this).data('col'), -1);
						saveLocalLDVersion(id_ld);
						globalLD.view.displayTable();
					}
				},
				moveright: {
					name: __("Déplacer à droite"),
					callback: function(){
						globalLD.model.moveColumn($(this).data('col'), 1);
						saveLocalLDVersion(id_ld);
						globalLD.view.displayTable();
					}
				},
				select: {
					name: __("Sélectionner la colonne (SHIFT-CLICK)"),
					callback: function() {
						$(this).addClass('fx_highlighted_col');
						let icol = parseInt($(this).data('col'));
						$table.find('.fx_default_row').each(function() {
							$(this).find('.fx_cell_val').eq(icol).addClass('fx_highlighted_cell');
						});
						alertGently(__("SHIFT-CLICK sur une autre colonne pour étendre la sélection"), "info", 2500);
					}
				},
				delete: {
					name: __("Supprimer la colonne"),
					callback: function(){
						deleteColumn(parseInt($(this).data('col')));
					}
				},
				sep3: "---------",
				sort_incr: {
					name: __("Trier (croissant)"),
					callback: function(){
						globalLD.model.sortRowsByColumn(parseInt($(this).data('col')), 1);
						saveLocalLDVersion(id_ld);
						globalLD.view.displayTableRows();
					}
				},
				sort_decr: {
					name: __("Trier (décroissant)"),
					callback: function(){
						globalLD.model.sortRowsByColumn(parseInt($(this).data('col')), -1);
						saveLocalLDVersion(id_ld);
						globalLD.view.displayTableRows();
					}
				}
			}
		});

		// Custom Activated Context Menu + column properties shortcut on double click
		$HTMLcontentLD.on("click", ".fx_col_menu", function (e) {
			if (event.shiftKey) {
				return false;
			}
			if (e.detail === 2) {
				blurFormulaInput();
				globalLD.view.displayColPropPanel($(this).data('col'));
				setTimeout(() => { $('.context-menu-list').trigger('contextmenu:hide'); }, 300);
			} else {
				setTimeout(() => { $(this).contextMenu(); }, 300);
			}
		});

		// add a column at the end of the table
		$HTMLcontentLD.on("click", ".fx_add_col", function () {
			insertColumn();
		});


		/**
		 * @param {int|null} icol the new column is inserted to the left of column icol
		 * @param {int} offset the offset to the right in number of columns
		 */
		function insertColumn(icol= null, offset= 0) {
			if (icol !== null) {
				icol += offset;
			}
			let updated = globalLD.model.insertColumn(icol);
			// don't saveLocalLDVersion: this will be made only if the user validate the proerties of the column
			refreshDataset(updated, false, true);
			if (icol === null) {
				icol = $XMLcodeLD.find('column').length - 1;
			}
			globalLD.view.displayColPropPanel(icol, true);
		}

		function deleteColumn(icol) {
			let mc = {col_min: icol, col_max: icol, row_max: 0, row_min: 0};
			let updated = globalLD.model.deleteCellValues(mc, 'col');
			saveLocalLDVersion(id_ld) ;
			refreshDataset(updated, false, true);
		}
	}

	function setColPropPanelControls() {
		validateOnEnter(".fx_col_prop_panel");
		let linked;
		$HTMLcontentLD.on("click", ".fx_col_type_num", function () {
			$HTMLcontentLD.find(".fx_col_prop_num_type").show();
		});
		$HTMLcontentLD.on("click", ".fx_col_type_txt", function () {
			$HTMLcontentLD.find(".fx_col_prop_num_type").hide();
		});
		$HTMLcontentLD.on("focus", ".fx_col_name", function () {
			linked = formatStringToAlphanum($(this).val()).substring(0,11) === $HTMLcontentLD.find(".fx_col_code").val();
		});
		$HTMLcontentLD.on("input", ".fx_col_name", function () {
			if (linked) {
				$HTMLcontentLD.find(".fx_col_code").val(formatStringToAlphanum($(this).val()).substring(0,11));
			}
		});
		$HTMLcontentLD.on("input", ".fx_col_code", function () {
			let code = formatStringToAlphanum($(this).val());
			$(this).val(code);
		});
		$HTMLcontentLD.on("focus", ".fx_col_formula", function() {
			globalLD.view.displayMathFormulaHelp(this, ".fx_col_prop_div_formula");
			$(this).addClass("fx-active-formula");
			$HTMLcontentLD.find(".fx_modal_cover").off("click"); // the modal cover doesn't close the ColPropPanel
		});
		$HTMLcontentLD.on("mouseup", ".fx_col_formula", function() {
			// stick the help panel when the textarea is resized
			if ($(this).hasClass("fx-active-formula")) {
				globalLD.view.displayMathFormulaHelp(this, ".fx_col_prop_div_formula");
			}
		});
		$HTMLcontentLD.on("keydown", ".fx_col_formula", function (event) {
			let keycode = event.which;
			if (keycode === 13 || keycode === 9) { // Enter or Tab
				blurFormulaInput();
				if (keycode === 13) {
					$HTMLcontentLD.find(".fx_col_prop_panel ").focus();
				}
				return false;
			}
		});

		$HTMLcontentLD.on("click", ".fx_sc_notation", function () {
			$HTMLcontentLD.find(".fx_nb_digits span").toggle();
		});

		// cancel
		$HTMLcontentLD.on("click", ".fx_col_cancel", function () {
			let $panel = $HTMLcontentLD.find(".fx_col_prop_panel");
			if ($panel.data("cancel_add_col")) {
				globalLD.model.deleteEmptyColumn($panel.data("icol"));
				let updated = {updated_cols: [], cols_in_circ_ref: [], updated_graphs: [], errors: []};
				refreshDataset(updated, false, true);
			}
			$HTMLcontentLD.find(".fx_col_prop_panel").hide();
			globalLD.view.toggleFXModal(false);
		});

		// validate
		$HTMLcontentLD.on("click", ".fx_col_prop_validate", function () {
			let $panel = $HTMLcontentLD.find(".fx_col_prop_panel");
			let icol = $panel.data("icol");
			// old values
			let $column = $XMLcodeLD.find('column').eq(icol);
			// get & check the common data of the panel
			let type = $panel.find("input[name='fx_col_type']:checked").val();
			let code = formatStringToAlphanum($panel.find(".fx_col_code").val());
			$panel.find(".fx_col_code").val(code);
			let name = $panel.find(".fx_col_name").val().trim();
			if (name === '') {
				name = code;
			}
			$panel.find(".fx_col_name").val(name);
			// checks
			if (code === '') {
				alertGently(__('Vous devez indiquer un code pour la colonne'));
				return;
			}
			if (code !== $column.find('code').text() && !checkFXCode(code, "col")) {
				return;
			}

			let formula_is_updated = false;
			let updated;
			if (type === "txt") { // update in model
				updated = globalLD.model.updateColumnProperties(icol, type, name, code);
			} else {
				// get and check data of numerical columns
				let unite = escapeXML($panel.find(".fx_col_unit").val().trim());
				let scientific_display = $panel.find(".fx_sc_notation").is(":checked");
				let nb_digits = $panel.find(".fx_nb_digits").find("input").val().trim();
				let formula = formatFXInputFormula($panel.find(".fx_col_formula").val());
				$panel.find(".fx_col_formula").val(formula);
				// check nb significant digits or decimals
				if (nb_digits !== "") {
					nb_digits = parseInt(nb_digits);
					if (isNaN(nb_digits)) {
						if (scientific_display) {
							alertGently(__("Le nombre de chiffres significatifs doit être un nombre"));
						} else {
							alertGently(__("Le nombre de décimales doit être un nombre"));
						}
						return;
					} else if (scientific_display && nb_digits > 16) {
						alertGently(__("Le nombre de chiffres significatifs ne doit pas être supérieur à 16"));
						return;
					}
				}
				// if switching to numeric, check that all values are numeric
				let remove_txt_values = false;
				let old_type = $column.attr('type');
				if (old_type !== 'num' && !formula) {
					let values = [];
					$XMLcodeLD.find("row").each(function() {
						values.push($(this).find("value").eq(icol).text());
					});
					let valuesToRemove = values.filter(function(x) {
						return x !== "" && isNaN(parseFloat(x));
					}).length;
					if (valuesToRemove && !confirm(__("Attention, {{nb_cell}} cellules contiennnent des valeurs non-numériques et seront affichées vides si vous confirmez.",{nb_cell:valuesToRemove}))) {
						type = "txt";
					} else {
						remove_txt_values = true;
					}
				}
				// update model with all values but formula
				updated = globalLD.model.updateColumnProperties(icol, type, name, code, unite, formula, scientific_display, nb_digits, remove_txt_values);
				// formula management
				let old_formula = $column.find('formula').text();
				if (formula !== old_formula) {
					formula_is_updated = updateColFormula(icol, old_formula, formula);
				}
			}
			if (!formula_is_updated) {
				saveLocalLDVersion(id_ld);
				refreshDataset(updated, true); // show_errors: false or true ??
			}

			$HTMLcontentLD.find(".fx_col_prop_panel").hide();
			globalLD.view.toggleFXModal(false);
		});
	}

	/**
	 * check that a column or constant code is OK
	 * @param {string} code
	 * @param {"const", "col"} type type of the code to check
	 * @returns {boolean}
	 */
	function checkFXCode(code, type) {
		let type_name;
		if (type === "const") {
			type_name = __("constante");
		} else {
			type_name = __("colonne");
		}
		if (code === "") {
			alertGently (__("Vous devez renseigner le nom de ") + type_name);
			return false;
		}
		if (code.match(/^\d/)) {
			alertGently (__("Un code de {{type_name}} ne peut pas commencer par un chiffre", {type_name:type_name}));
			return false;
		}
		// ne peut contenir que des lettres, chiffres et _
		if (!code.match(/^\w+$/)) {
			alertGently (__("Un code de {{type_name}} ne peut comporter que des lettres sans accent, des chiffres, et '_'.", {type_name:type_name}));
			return false;
		}
		// can't be a math function
		let forbidden_codes = globalLD.model.forbidden_codes;
		if (forbidden_codes.includes(code)) {
			alertGently (__("Un code de {{type_name}} ne peut pas être le nom d'une fonction mathématique",{type_name:type_name}) + __("&nbsp;:") + "\n" + forbidden_codes.join(", "));
			return false;
		}
		// ne doit pas être les variables x et y
		if (code === 'x' || code === 'y') {
			alertGently (__("Un code de {{type_name}} ne peut pas être le nom d'une variable d'axe : x ou y",{type_name:type_name}));
			return false;
		}
		// 12 chars max for a column code
		if (code.length > 12) {
			alertGently (__("Un code de {{type_name}} est limité à 12 caractères"));
			return false;
		}
		if (type !== "const") { // do not check existence for constants, it is checked on validation
			let exists = globalLD.model.existsFXCode(code);
			if (exists === "col") {
				alertGently (__('Le code {{code}} est déjà utilisé pour une colonne',{code:code}));
				return false;
			} else if (exists === "const") {
				alertGently (__('Le code {{code}} est déjà utilisé pour une constante',{code:code}));
				return false;
			}
		}
		return true;
	}

	function setFormulasRowControls() {
		$HTMLcontentLD.on("click", ".fx_editable_formula_cell", function() {
			event.stopPropagation();
			let icol = $(this).parent("td").data('col');
			globalLD.view.displayColPropPanel(icol, false, ".fx_col_formula");
			return false;
		});
	}

	/**
	 * Refresh the table and the graphs following an update that called cascadeUpdateDataOnInput
	 * @param { {updated_cols: string[], cols_in_circ_ref: string[], updated_graphs: int[], errors: string[]} } updated
	 * @param {boolean} show_errors
	 * @param {boolean} force_table_refresh
	 */
	function refreshDataset(updated, show_errors = false, force_table_refresh = false) {
		if (force_table_refresh || updated.updated_cols.length > 0) {
			globalLD.view.displayTable();
		}
		updated.updated_graphs.forEach(function(igraph) {
			globalLD.view.displayGraphs(igraph);
		});
		let errors = updated.errors;
		if (errors.length && show_errors) {
			if (errors.includes("insert_in_col_formula")) {
				alertGently(globalLD.view.fx_errors["insert_in_col_formula"]);
			}
			if (errors.includes("insert_text_in_num")) {
				alertGently(globalLD.view.fx_errors["insert_text_in_num"]);
			}
			if (errors.includes("parse_error")) {
				alertGently(globalLD.view.fx_errors["parse_error"]);
			} else if (errors.includes("undef_symbol")) {
				alertGently(globalLD.view.fx_errors["undef_symbol"]);
			} else if (errors.includes("circ_ref")) {
				alertGently(__("Les colonnes {{col_codes}} sont incluses dans une référence circulaire", {col_codes: updated.cols_in_circ_ref.join(", ")}));
			}
		}
	}

	function setRowsControls() {

		// add some rows
		$HTMLcontentLD.on("click", ".fx_add_row", function () {
			let nb_added_rows = parseInt($(this).find(".fx_num_rows_added").val());
			if (nb_added_rows && !isNaN(nb_added_rows)) {
				insertRowsBefore(null, nb_added_rows);
			}
		});
		$HTMLcontentLD.on("click", ".fx_num_rows_added", function (event) {
			event.stopPropagation();
		});
		$HTMLcontentLD.on("keypress", ".fx_num_rows_added", function (event) {
			if(event.which === 13){ // enter key
				$HTMLcontentLD.find('.fx_add_row').click();
			}
		});

		$HTMLcontentLD.contextMenu({
			selector: '.fx_row_menu',
			appendTo: $HTMLcontentLD,
			trigger: 'left',
			hideOnSecondTrigger: true,
			autoHide: false,
			position: function (opt) {
				opt.$menu.position({
					my: 'left top',
					at: 'left bottom',
					of: opt.$trigger
				});
			},
			events: {
				show: function() {
					blurFormulaInput();
					if (event.shiftKey) {
						return false;
					}
				}
			},
			items: {
				insertBefore: {
					name: __("Insérer une ligne au dessus"),
					callback: function(){
						let irow = parseInt($(this).closest('.fx_default_row').data('row'));
						insertRowsBefore(irow, 1);
					}
				},
				insertAfter: {
					name: __("Insérer une ligne en dessous"),
					callback: function(){
						let irow = 1 + parseInt($(this).closest('.fx_default_row').data('row'));
						insertRowsBefore(irow, 1);
					}
				},
				sep1: "---------",
				select: {
					name: __("Sélectionner la ligne (SHIFT-CLICK)"),
					callback: function() {
						let $row = $(this).closest('.fx_default_row');
						$row.addClass('fx_highlighted_row');
						$row.find('.fx_cell_val').each(function (){
							$(this).addClass('fx_highlighted_cell');
						});
						alertGently(__("SHIFT-CLICK sur une autre ligne pour étendre la sélection"), "info", 2500);

					}
				},
				delete: {
					name: __("Supprimer la ligne"),
					callback: function() {
						let irow = parseInt($(this).closest('.fx_default_row').data('row'));
						deleteRow(irow);
					}
				}
			}
		});

		function deleteRow(irow) {
			let mc = {col_min: 0, col_max: 0, row_max: irow, row_min: irow};
			let updated = globalLD.model.deleteCellValues(mc, 'row');
			saveLocalLDVersion(id_ld);
			refreshDataset(updated);
		}
	}

	/**
	 * @param {int|null} irow
	 * @param {int} nb_added_rows
	 */
	function insertRowsBefore(irow, nb_added_rows) {
		let updated = globalLD.model.insertRows(irow, nb_added_rows, true);
		saveLocalLDVersion(id_ld);
		refreshDataset(updated, false, true);
	}

	/**
	 * @param {object} $cell
	 * @returns {{col: number, row: number}}
	 */
	function getCellPosition($cell) {
		let row = parseInt($cell.closest("tr").data('row'));
		let col = parseInt($cell.closest("td").data('col'));
		return {row: row, col: col};
	}

	/**
	 * @param {{col: number, row: number}} pos
	 * @returns {object}
	 */
	function getCellFromPosition(pos) {
		return $table.find("tr[data-row='" + pos.row + "']").find("td[data-col='" + pos.col + "']").find(".fx_cell_val");
	}

	function unselectCell() {
		$table.find(".fx_editable_cell").removeClass("fx_selected_cell").removeClass("fx_edited_cell").removeAttr('contenteditable');
		$table.find(".fx_ignored_cb").css("display", "none");
	}

	function setEditableCellControls() {

		/**
		 * get the next editable cell
		 * @param {object} $cell
		 * @param {int} keycode
		 * @returns {boolean} true if an editable cell has been found
		 */
		function getNextCell($cell, keycode) {
			let direction;
			if (keycode === 39 || keycode === 9) {
				direction = "right";
			} else if (keycode === 37) {
				direction = "left";
			} else if (keycode === 38) {
				direction = "up";
			} else {
				direction = "down";
			}
			let $newcell = false;
			do {
				$cell = getClosestCell($cell, direction);
				if ($cell.length && $cell.hasClass("fx_editable_cell")) {
					$newcell = $cell;
				}
			} while (!$newcell && $cell.length);
			return $newcell;
		}

		/**
		 * get the closest cell following a direction
		 * @param $cell
		 * @param direction
		 * @returns {*}
		 */
		function getClosestCell($cell, direction) {
			let pos = getCellPosition($cell);
			if (direction === "right") {
				pos = {row: pos.row, col: pos.col+1}
			} else if (direction === "left") {
				pos = {row: pos.row, col: pos.col-1}
			} else if (direction === "up") {
				pos = {row: pos.row-1, col: pos.col}
			} else if (direction === "down") {
				pos = {row: pos.row+1, col: pos.col}
			}
			return getCellFromPosition(pos);
		}

		/**
		 *
		 * @param {object} $cell
		 */
		function selectCell($cell) {
			if ($cell.length === 0) {
				console.log("cells are unselectable");
				return;
			}
			if ($cell.hasClass("fx_selected_cell")) {               // edit
				let pos = getCellPosition($cell);
				let val = $cell.html();
				let xml_val = $XMLcodeLD.find('row').eq(pos.row).find('value').eq(pos.col).text();
				if (val !== xml_val) {
					$cell.html(xml_val);
				}
				$cell.addClass("fx_edited_cell");
				$cell.parent().find(".fx_ignored_cb").css("display", "none");
				$cell.focus();  // useless ?
				$cell.select(); // useless ?
			} else if ($cell.hasClass("fx_editable_cell")){         // select
				unselectCell();
				$cell.attr('contenteditable', 'true');
				$cell.addClass("fx_selected_cell");
				$cell.parent().find(".fx_ignored_cb").css("display", "block");
				$cell.blur(); // usefull to fix a bug in Firefox that doesn't focus otherwise
				$cell.focus();
			}
		}

		/**
		 * check and update the value of the cell
		 * @param $cell
		 * @returns {boolean}
		 */
		function checkAndUpdateCell($cell) {
			let value = $cell.text();
			let $td = $cell.parent("td");
			let is_text = $td.hasClass("fx_text");
			let pos = getCellPosition($cell);
			// validate Numeric value
			if (!is_text) {
				$cell.css("color", "");
				value = formatFXInputNumValue(value);
				if (isNaN(value)) {
					alertGently(__('Vous ne pouvez pas ajouter de texte dans une colonne au format numérique.')+'\n\
'+__('Utilisez uniquement les caractères -0123456789.E\n\
Ex : -7.5E-6'), "warning");
					$cell.focus();
					$cell.css("color","red");
					return false;
				}
			}
			let checked = $td.find(".fx_ignored_cb").is(":checked");
			let updated = globalLD.model.updateCellValue(pos, checked, value);
			saveLocalLDVersion(id_ld);
			refreshDataset(updated);
			if (is_text && value === '') {
				$cell.html("&nbsp;"); // to avoid an empty cell that would'nt feel the TD
			}
			return true;
		}

		// click somewhere on the labdoc and update a data value if necessary
		$HTMLcontentLD.on("click", function(event) {
			let $target = $(event.target);
			if ($target.hasClass("fx_edited_cell")) {
				return;
			}
			let $cell_to_select = null;
			let position;
			let formula_col = null;
			// record the position in case of table refresh
			if ($target.hasClass("fx_editable_cell")) {
				$cell_to_select = $target;
				position = getCellPosition($target);
			} else if ($target.hasClass("fx_formula_cell")) {
				formula_col = parseInt($target.closest("td").data('col'));
			}

			// is there an edited cell to validate ?
			let edited_cell = $(this).find(".fx_edited_cell").first();
			if (edited_cell.length) { // update a cell
				if (!checkAndUpdateCell(edited_cell)) {
					return;
				}
				// table refresh done
				if (position) {
					$cell_to_select = getCellFromPosition(position);
				} else if (formula_col) {
					$cell_to_select = $(this).find("td[data-col='" + formula_col + "']").find(".fx_formula_cell");
				}
			}

			// the effect of the click on the target
			if ($cell_to_select) {
				selectCell($cell_to_select);
			} else if (!$target.hasClass("fx_ignored_cb")) {
				unselectCell();
			}
		});

		// Change a checkbox state
		$table.on("change", ".fx_ignored_cb", function(){
			let checked = $(this).is(":checked");
			let $cell = $(this).parent().find(".fx_cell_val");
			let pos = getCellPosition($cell);
			let updated = globalLD.model.updateCellValue(pos, checked);
			saveLocalLDVersion(id_ld);
			refreshDataset(updated, false, true);
			// table refresh done => now select
			$cell = getCellFromPosition(pos);
			selectCell($cell);
		});

		// press a key in a selected cell
		$table.on("keydown", ".fx_selected_cell", function(event) {
			if (event.ctrlKey || event.metaKey) {
				return;
			}
			let keycode = event.which;
			let is_edited_cell = $(this).hasClass("fx_edited_cell");
			let pos_keycodes = [9, 37, 38, 39, 40];

			if (is_edited_cell) { // validation et déplacement
				if (keycode === 9 || keycode === 13 || keycode === 38 || keycode === 40) {
					let add_a_row = false;
					event.preventDefault();
					let $newcell = getNextCell($(this), keycode);
					let pos;
					if (!$newcell && keycode === 13) { // ajout d'une ligne en fin de table
						let cell_pos = getCellPosition($(this));
						pos = {row: cell_pos.row+1, col: cell_pos.col};
						if ($($table).find("tr[data-row='" + pos.row + "']").length === 0) {
							add_a_row = true;
						}
					} else {
						pos = getCellPosition($newcell);
					}
					if (!checkAndUpdateCell($(this))) {
						return;
					}
					// table refresh done
					if (add_a_row) {
						insertRowsBefore(null, 1); // asks for another refresh (possibly different) - could be optimized
					}
					if (pos) {
						let cell_to_select = getCellFromPosition(pos);
						selectCell(cell_to_select);
					}
				}
			} else { // not edited cell
				if (pos_keycodes.includes(keycode)) {  // déplacement
					event.preventDefault();
					let $newcell = getNextCell($(this), keycode);
					if ($newcell) {
						selectCell($newcell);
					}
				} else { // all other keycodes, including 13 => enter the cell
					let special_codes = [16,17,18,19,20,27,33,34,35,36,45,91,92,93,112,113,114,115,116,117,118,119,12,121,122,123,144,145];
					if (special_codes.includes((keycode))) { // excludes special chars
						return;
					}
					selectCell($(this));
					if (keycode === 13) {
						event.preventDefault();
					} else {
						$(this).html("");
					}
					$(this).focus();
				}
			}
		});
	}


	function setHighlightCellsBehavior() {

		/**
		 * @param {jQuery} $cell1
		 * @param {jQuery} $cell2
		 * @return {{col_min: int, col_max: int, row_max: int, row_min: int}}
		 */
		function getCellMatrixCorners($cell1, $cell2) {
			let pos1 = getCellPosition($cell1);
			let pos2 = getCellPosition($cell2);
			let col_max = Math.max(pos1.col, pos2.col);
			let col_min = Math.min(pos1.col, pos2.col);
			let row_max = Math.max(pos1.row, pos2.row);
			let row_min = Math.min(pos1.row, pos2.row);
			return {
				col_min: col_min,
				col_max: col_max,
				row_min: row_min,
				row_max: row_max
			}
		}

		/**
		 * attributes the class fx_highlighted_cell to the cells in the matrix matrix_corners
		 * @param {{col_min: int, col_max: int, row_min: int, row_max: int}} matrix_corners
		 */
		function highlightCells(matrix_corners) {
			for (let i = matrix_corners.col_min; i <= matrix_corners.col_max; i++) {
				for (let j = matrix_corners.row_min; j <= matrix_corners.row_max; j++) {
					getCellFromPosition({col: i, row: j}).addClass("fx_highlighted_cell");
				}
			}
			$table.focus(); // used to intercept CTRL-C and CTRL-X
		}

		function unhighlightCells() {
			$table.find(".fx_highlighted_cell").removeClass("fx_highlighted_cell");
			$table.find(".fx_highlighted_col").removeClass("fx_highlighted_col");
			$table.find(".fx_highlighted_row").removeClass("fx_highlighted_row");
		}

		// select cells by SHIFT click on columns headers
		$HTMLcontentLD.on('click', '.fx_col_header', function(event) {
			highlightRowsOrCols(event, "col");
		});
		// select cells by SHIFT click on row titles
		$HTMLcontentLD.on('click', '.fx_row_title', function(event) {
			highlightRowsOrCols(event, "row");
		});

		/**
		 * copy cells by SHIFT click on columns headers or row titles
		 * I'm not sure that it was bette to factorize the func for rows and cols
		 * @param event
		 * @param {string} type "col" | "row"
		 */
		function highlightRowsOrCols(event, type) {
			unselectCell();
			if (event.shiftKey) {
				let nb_max, class_for_highlighted, class_for_highlight, msg;
				switch(type) {
					case 'col':
						class_for_highlight = 'fx_col_header';
						class_for_highlighted = 'fx_highlighted_col';
						nb_max = $table.find('.fx_default_row').length;
						msg = __("SHIFT-CLICK sur une autre colonne pour étendre la sélection");
						break;
					case 'row':
						class_for_highlight = 'fx_default_row';
						class_for_highlighted = 'fx_highlighted_row';
						nb_max = $table.find('.fx_col_header').length;
						msg = __("SHIFT-CLICK sur une autre ligne pour étendre la sélection");
				}
				let $elt = $(event.target).closest('.' + class_for_highlight);
				let i = parseInt($elt.data(type));
				let matrix_corners = {
					row_min: 0,
					row_max: type === "col" ? nb_max - 1 : 0,
					col_min: 0,
					col_max: type === "row" ? nb_max - 1 : 0,
				};
				let $highlighted_elts = $table.find('.' + class_for_highlighted);
				if ($highlighted_elts.length === 1) {     // extend selection
					let i2 = parseInt($highlighted_elts.data(type));
					let min = Math.min(i,i2);
					let max = Math.max(i,i2);
					for (let j=min; j<=max; j++) {
						$table.find('.' + class_for_highlight).eq(j).addClass(class_for_highlighted);
					}
					if (type === 'col') {
						matrix_corners.col_min = min;
						matrix_corners.col_max = max;
					} else {
						matrix_corners.row_min = min;
						matrix_corners.row_max = max;
					}
				} else {                              // new selection
					unhighlightCells();
					$elt.addClass(class_for_highlighted);
					if (type === 'col') {
						matrix_corners.col_min = i;
						matrix_corners.col_max = i;
					} else {
						matrix_corners.row_min = i;
						matrix_corners.row_max = i;
					}
					alertGently(msg, "info", 2500);
				}
				highlightCells(matrix_corners);
				if (!edition) {
					copyCellsInClipBoard(matrix_corners, type);
				}
			}
		}

		// select cells by SHIFT select of two opposite corner cells
		$table.on("click", ".fx_cell", function(event) {
			if (event.shiftKey) {
				event.stopPropagation();
				unselectCell();
				let $target = $(event.target);
				if ($target.hasClass("fx_cell_val")) {
					let $selected_cells = $table.find(".fx_highlighted_cell");
					if ($selected_cells.length !== 1) { // new selection
						unhighlightCells();
						$selected_cells = $target;
						alertGently(__("SHIFT-CLICK sur une autre cellule pour étendre la sélection"), "info", 2500);
					}
					let matrix_corners = getCellMatrixCorners($target, $selected_cells);
					highlightCells(matrix_corners);
					if (!edition) {
						copyCellsInClipBoard(matrix_corners, 'cell');
					}
				}
			}
		});

		// unhighlight cells when clicking elsewhere. Cannot be done in mousedown because mousedown includes the scrollbar
		$HTMLcontentLD.on("click", ".fx_col_menu, .fx_row_menu, .fx_editable_formula_cell", function(event) {
			if (!event.shiftKey) {
				unhighlightCells();
			}
		});
		$HTMLcontentLD.on("click", ".fx_table_wrapper", function(event) {
			if(event.target === this) {
				unhighlightCells();
			}
		});


		// select cells by dragging the mouse
		let $fx_selected_cell_origin = null;
		let $fx_cell_over = null;

		$HTMLcontentLD.on("mousedown", function(event) {
			if ($table.find(".fx_edited_cell").length) { // impossible to select by gliding if there is an edited cell
				return;
			}
			let $target = $(event.target);
			if ($target.hasClass("fx_cell_val") && !event.shiftKey) {
				unhighlightCells();
				// init the copy
				$fx_selected_cell_origin = $target;
				$table.on("mousemove", listenMouseMove);
			}
		});

		function listenMouseMove(event) {
			let $target = $(event.target);
			if ($target.hasClass("fx_cell_val") && !$target.is($fx_cell_over) && !$target.is($fx_selected_cell_origin)) {
				$fx_cell_over = $target;
				$table.find(".fx_highlighted_cell").removeClass("fx_highlighted_cell");
				let matrix_corners = getCellMatrixCorners($fx_selected_cell_origin, $fx_cell_over);
				highlightCells(matrix_corners);
			}
		}

		$HTMLcontentLD.on("mouseup", function(event) {
			if (event.shiftKey) {
				return;
			}
			$table.off("mousemove");
			if (!edition && $fx_selected_cell_origin !== null && $fx_cell_over !== null) {
				let matrix_corners = getCellMatrixCorners($fx_selected_cell_origin, $fx_cell_over);
				copyCellsInClipBoard(matrix_corners, 'cell');
			}
			$fx_selected_cell_origin = null;
			$fx_cell_over = null;
		});
	}

	/**
	 * detect the matrix corners of the highlighted cells and the type of selection: 'col' | 'row' | 'cell' | ''
	 * @return {{matrix_corners: {col_min: number, col_max: number, row_max: number, row_min: number}, type: string}}
	 */
	function getHighlightedItems() {
		let type = '';
		let matrix_corners = {
			row_min: 0,
			row_max: 0,
			col_min: 0,
			col_max: 0
		};
		let $cols = $table.find(".fx_highlighted_col");
		let $rows = $table.find(".fx_highlighted_row");
		let $cells = $table.find(".fx_highlighted_cell");
		if ($cols.length) {
			type = 'col';
			matrix_corners.col_min = $cols.first().data('col');
			matrix_corners.col_max = $cols.last().data('col');
		} else if ($rows.length) {
			type = 'row';
			matrix_corners.row_min = $rows.first().data('row');
			matrix_corners.row_max = $rows.last().data('row');
		} else if ($cells.length) {
			type = 'cell';
			matrix_corners.col_min = $cells.first().closest('td').data('col');
			matrix_corners.row_min = $cells.first().closest('tr').data('row');
			matrix_corners.col_max = $cells.last().closest('td').data('col');
			matrix_corners.row_max = $cells.last().closest('tr').data('row');
		}
		if (!type) {
			alertGently(__("Pour utiliser cette fonction, sélectionnez d'abord des cellules&nbsp;:<br> - par glissement de la souris sur les cellules,<br> - en utilisant SHIFT-CLICK sur les coins opposés d'une sélection de cellules,<br> - en utilisant SHIFT-CLICK sur l'entête des colonnes ou des lignes."), 'warning', 15000);
		}
		return { matrix_corners: matrix_corners, type: type }
	}

	function copyCells() {
		if ($table.find(".fx_edited_cell").length) { // edited cell => default behaviour
			return;
		}
		let $sel_cell = $table.find(".fx_selected_cell");
		if ($sel_cell.length) { // selected cell => highlight it
			unselectCell();
			$sel_cell.addClass("fx_highlighted_cell");
		}
		let highlighted = getHighlightedItems();
		if (highlighted.type) {
			copyCellsInClipBoard(highlighted.matrix_corners, highlighted.type);
		}
	}

	function deleteItems() {
		if ($table.find(".fx_edited_cell").length) { // edited cell => default behaviour
			return;
		}
		let $sel_cell = $table.find(".fx_selected_cell");
		if ($sel_cell.length) { // selected cell => highlight it
			$sel_cell.addClass("fx_highlighted_cell");
		}
		let highlighted = getHighlightedItems();
		if (highlighted.type) {
			let updated = globalLD.model.deleteCellValues(highlighted.matrix_corners, highlighted.type);
			saveLocalLDVersion(id_ld);
			refreshDataset(updated, true, true);
		}
	}

	/**
	 * fill the paperboard with the values of the copied cells
	 * @param {{col_min: int, col_max: int, row_min: int, row_max: int}} matrix_corners
	 * @param {string} type col | row | cell
	 */
	function copyCellsInClipBoard(matrix_corners, type) {
		let cells_values = globalLD.model.getCellValues(matrix_corners, type);
		if (navigator.clipboard === undefined) {
			alertGently(__("Le presse-papier n'est pas accessible dans votre environnement"), "warning", 4000);
			return;
		}
		navigator.clipboard.writeText(cells_values).then(() => {
			let nb_cells = (1 + matrix_corners.col_max - matrix_corners.col_min) * (1 + matrix_corners.row_max - matrix_corners.row_min);
			alertGently(nb_cells + __(" cellule(s) copiée(s) dans le presse-papier"), "success", 2500);
		},() => {
			alertGently(__("Autorisez à LabNBook l'accès au presse-papier pour pouvoir faire des copier-coller"), "warning", 4000);
		});
	}

	function pasteDataInTable() {
		if (navigator.clipboard === undefined) {
			alertGently(__("Le presse-papier n'est pas accessible dans votre environnement"), "warning", 4000);
			return;
		}
		let $selected_cell = $table.find(".fx_selected_cell");
		if ($selected_cell.length === 0) {
			alertGently(__("Avant de coller, sélectionnez une cellule indiquant où placer le collage"), "warning", 4000);
			return;
		}
		try {
			navigator.clipboard.readText().then((cb_content) => {
				if (cb_content === "") {
					alertGently(__("Le presse-papier est vide"), "warning", 4000);
					return;
				}
				let pos = getCellPosition($selected_cell.first());
				// the content in the clipboard must be a text with values separated by tabs and (carriage returns or line feeds)
				cb_content = cb_content.replace(/\r\n|\r/g, '\n'); // normalize the ends of lines
				let rows = cb_content.split('\n');
				let nb_cols_added = 0;
				let values = [];
				for (let i = 0; i < rows.length; i++) {
					values[i] = rows[i].split('\t');
					if (values[i].length > nb_cols_added) {
						nb_cols_added = values[i].length;
					}
				}
				if (rows.length === 0 || nb_cols_added === 0) {
					alertGently(__("Les données du presse-papier n'ont pas pu être utilisées"), "error");
					return;
				}
				let updated = globalLD.model.updateCellValuesFromMatrix(values, pos);
				saveLocalLDVersion(id_ld);
				refreshDataset(updated, true);
			});
		} catch(e) {
			console.log(e);
			alertGently(__(`La fonction 'coller' n'est pas autorisée dans FireFox :((<br/> Pour l'autoriser, tapez <b>about:config</b> dans la barre d'adresse du navigateur,<br/>cliquez sur <b>Accepter le risque et poursuivre</b>,<br/>recherchez <b>dom.events.asyncClipboard.readText</b> et initialisez le à <b>true</b>.<br/>Rechargez ensuite la page de votre navigateur (F5)`), 'warning', 0);
		}
	}

	/**
	 * Set the special Key controls for the table
	 * Delete, backSpace, CTRL-V, CTRL-C, CTRL-X
	 */
	function setSpecialKeysControls() {
		$table.on("keydown", function(event) {
			if ($table.find(".fx_selected_cell").length + $table.find(".fx_highlighted_cell").length === 0) {
				return;
			}
			let key = event.which;
			// console.log("keydown detected", key);
			if (key === 8 || key === 46) {
				deleteItems();
			}
			if ((event.ctrlKey || event.metaKey)) {
				if (key === 86) { // CTRL-V
					event.stopPropagation();
					event.preventDefault();
					pasteDataInTable();
				}
				if (key === 67) { // CTRL-C
					event.stopPropagation();
					event.preventDefault();
					copyCells();
				} else if (key === 88) { // CTRL-X
					event.stopPropagation();
					event.preventDefault();
					copyCells();
					deleteItems();
				}
			}
		});
	}


	function setGraphControls() {

		// open graph properties
		$HTMLcontentLD.on("click", ".fx_btn_graph_prop, .fx_graph_title_txt", function () {
			let igraph = $(this).closest(".fx_graph").data("igraph");
			const graphType = globalLD.model.getGraphType(igraph);
			globalLD.view.displayGraphPropPanel(igraph, false, graphType);
		});

		// move up graph
		$HTMLcontentLD.on("click", ".fx_btn_graph_up", function () {
			let $graph = $(this).closest(".fx_graph");
			let igraph = $graph.data("igraph");
			globalLD.model.moveUpGraph(igraph);
			saveLocalLDVersion(id_ld) ;
			globalLD.view.displayGraphs();
		}) ;

		// delete graph
		$HTMLcontentLD.on("click", ".fx_btn_graph_delete", function () {
			let $graph = $(this).closest(".fx_graph");
			let igraph = $graph.data("igraph");
			globalLD.model.deleteGraph(igraph);
			saveLocalLDVersion(id_ld) ;
			globalLD.view.displayGraphs();
		}) ;
	}

	function setGraphPropertiesControls() {

		// change the color of a plot
		$HTMLcontentLD.on("click", ".fx_plot_col_pick", function () {
			$HTMLcontentLD.find(".fx_color_picker").data("item", "plot");
			globalLD.view.displayColorPicker(this);
		});
		
		// delete a plot
		$HTMLcontentLD.on("click", ".fx_delete_plot", function () {
			let element = $(this).closest("tr");
			let graphType = element.parents('.fx_panel').first().attr('class').includes('histogram') ? "histogram" : "X-Y";
			element.remove();
			if ($HTMLcontentLD.find('.fx_graph_plot').length === 0) {
				globalLD.view.addPlot(null, null, null, null, null, null, null, graphType); // at least one plot visible
			}
		});

		// add a plot
		$HTMLcontentLD.on("click", ".fx_add_plot i, .fx_add_plot span", function () {
			let element = $(this).closest("tr");
			let graphType = element.parents('.fx_panel').first().attr('class').includes('histogram') ? "histogram" : "X-Y";
			globalLD.view.addPlot(null, null, null, null, null, null, null, graphType);
		});

		// show axis y2
		$HTMLcontentLD.on("click", ".fx_add_axis_y2 i, .fx_add_axis_y2 span", function () {
			$HTMLcontentLD.find(".fx_graph_prop_panel").addClass("fx_show_y2_axis");
		});

		// hide axis y2
		$HTMLcontentLD.on("click", ".fx_remove_axis_y2", function () {
			$HTMLcontentLD.find(".fx_graph_prop_panel").removeClass("fx_show_y2_axis");
		});

		// cancel button
		$HTMLcontentLD.on("click", ".fx_graph_prop_cancel", function () {
			let $panel = $(event.target).parents('.fx_panel').first().attr('class').includes('histogram') ? 
				$HTMLcontentLD.find(".fx_histogram_prop_panel") : $HTMLcontentLD.find(".fx_graph_prop_panel");
			let igraph = $panel.data("igraph");
			if ($panel.data("new_graph") === 'true') {
				globalLD.model.deleteGraph(igraph);
				saveLocalLDVersion(id_ld);
				$HTMLcontentLD.find(".fx_graph").eq(igraph).remove();
			}
			$panel.hide() ;
			globalLD.view.toggleFXModal(false) ;
		});

		// validate button
		$HTMLcontentLD.on("click", ".fx_graph_prop_validate", function () {
			let graphType = $(event.target).parents('.fx_panel').first().attr('class').includes('histogram') ? "histogram" : "X-Y";
			let $panel = graphType === 'histogram' ? $HTMLcontentLD.find(".fx_histogram_prop_panel") : $HTMLcontentLD.find(".fx_graph_prop_panel");
			let igraph = $panel.data("igraph");
			let name, plots = [], xmin, xmax, xlog, xlabel, ymin, ymax, ylog, ylabel, y2min, y2max, y2log, y2label;
			let iplot, x, y, ux, uy, color, curve, axis;
			let y2 = $panel.hasClass("fx_show_y2_axis");
			// histogram specific
			let bins_number;
			let frequency;

			name = $panel.find(".fx_graph_name").val().trim();
			if (name === "") {
				alertGently(__("Vous devez donner un titre à votre graphique"));
				return;
			}

			function getBound(selector) {
				let bound = formatFXInputNumValue($panel.find(selector).val());
				$panel.find(selector).val(bound);
				if (bound !== "" && isNaN(Number(bound))) {
					alertGently(__("Au moins une borne d'axe n'est pas reconnue comme valeur numérique"));
					bound = false;
				}
				return bound;
			}
			xmin = getBound('.fx_axis_xmin');
			if (xmin === false) { return; }
			xmax = getBound('.fx_axis_xmax');
			if (xmax === false) { return; }
			ymin = getBound('.fx_axis_ymin');
			if (ymin === false) { return; }
			ymax = getBound('.fx_axis_ymax');
			if (ymax === false) { return; }
			y2min = getBound('.fx_axis_y2min');
			if (y2min === false) { return; }
			y2max = getBound('.fx_axis_y2max');
			if (y2max === false) { return; }

			if ((xmin || xmin === "0") && (xmax || xmax === "0") && parseFloat(xmin) >= parseFloat(xmax) ) {
				alertGently (__("xmax doit être supérieur à xmin"));
				return;
			}
			if (((ymin || ymin === "0") && (ymax || ymax === "0") && parseFloat(ymin) >= parseFloat(ymax))
				|| (y2 && (y2min || y2min === "0") && (y2max || y2max === "0") && parseFloat(y2min) >= parseFloat(y2max))
				) {
				alertGently (__("ymax doit être supérieur à ymin"));
				return;
			}
			xlog = $panel.find('.fx_axis_xscale').prop("checked");
			ylog = $panel.find('.fx_axis_yscale').prop("checked");
			y2log = $panel.find('.fx_axis_y2scale').prop("checked");
			if (
				(xlog && ((xmin !== "" && xmin <= 0) || (xmax !== "" && xmax <= 0)))
				|| (ylog && ((ymin !== "" && ymin <= 0) || (ymax !== "" && ymax <= 0)))
				|| (y2 && y2log && ((y2min !== "" && y2min <= 0) || (y2max !== "" && y2max <= 0)))
				) {
				alertGently (__("En cas d'échelle log, les bornes de l'axe ne peuvent pas être inférieures ou égales à 0"));
				return;
			}

			xlabel = $panel.find(".fx_axis_xlabel").val().trim();
			ylabel = $panel.find(".fx_axis_ylabel").val().trim();
			y2label = ($panel.find(".fx_axis_y2label").val() || '').trim();

			// histogram specific
			bins_number = $panel.find(".fx_global_options_bins_number").val();
			frequency = $panel.find(".fx_axis_frequency").prop('checked');

			$panel.find('.fx_graph_plot').each(function() {
				iplot = $(this).attr("data-iplot");
				x = $(this).find('.fx_graph_plot_x').val();
				y = $(this).find('.fx_graph_plot_y').val();
				if (x && (graphType === 'histogram' || y)) {
					ux = $(this).find('.fx_graph_plot_ux').val();
					uy = $(this).find('.fx_graph_plot_uy').val();
					color = $(this).find('.fx_plot_col_pick').data('color');
					curve = $(this).find('.fx_graph_curve').val();
					axis = y2 ? $(this).find('.fx_plot_axis_choice').val() : "y";
					plots.push({iplot: iplot, x: x, y: y, ux: ux, uy: uy, color: color, curve: curve, axis: axis})
				}
			});

			globalLD.model.updateGraph(igraph, name, xmin, xmax, xlog, xlabel, ymin, ymax, ylog, ylabel, y2, y2min, y2max, y2log, y2label, plots, bins_number, frequency);
			saveLocalLDVersion(id_ld);
			$panel.hide() ;
			globalLD.view.toggleFXModal(false) ;
			globalLD.view.displayGraphs(igraph);
		});

		validateOnEnter(".fx_graph_prop_panel, .fx_histogram_prop_panel");
	}

	function setPFuncsControls() {

		// click color-picker
		$HTMLcontentLD.on("click", ".fx_pfunc_div_col_pick", function () {
			let igraph = $(this).closest('.fx_graph').data('igraph');
			let ifunc = $(this).closest('.fx_graph_pfunc').data('ifunc');
			$HTMLcontentLD.find(".fx_color_picker").data("item", "pfunc").data("igraph", igraph).data("ifunc", ifunc);
			globalLD.view.displayColorPicker(this);
		});

		// validate on enter
		$HTMLcontentLD.on("keydown", ".fx_pfunc_input, .fx_pfunc_param_val, .fx_pfunc_div_interval input", function (event) {
			let keycode = event.which;
			if (keycode === 13 || keycode === 9) {
				if ($(this).hasClass("fx_pfunc_input")) {
					blurFormulaInput(); // special treatment for formulas
				} else {
					$(this).blur();
				}
			}
		});

		// display formula help
		$HTMLcontentLD.on("focus", ".fx_pfunc_input", function() {
			globalLD.view.displayMathFormulaHelp(this, ".fx_graph_pfunc");
			$(this).addClass("fx-active-formula");
		});
		$HTMLcontentLD.on("mouseup", ".fx_pfunc_input", function() {
			// stick the help panel when the textarea is resized
			if ($(this).hasClass("fx-active-formula")) {
				globalLD.view.displayMathFormulaHelp(this, ".fx_graph_pfunc");
			}
		});

		// focusout func parameter
		$HTMLcontentLD.on("focusout", ".fx_pfunc_param_val", function() {
			let val = formatFXInputNumValue($(this).text());
			if (val === "") {
				val = "0";
			}
			$(this).text(val);
			updateFXParamVal(this, val);
		});

		// increment or decrement func parameter
		$HTMLcontentLD.on("click", ".fx_pfunc_param_increment", function() {
			let incr;
			if ($(this).hasClass("increment_incr")) {
				incr = 1;
			} else {
				incr = -1;
			}
			let $val = $(this).closest('.fx_pfunc_param').find('.fx_pfunc_param_val');
			let val = $val.text();
			let new_val = incrementFXParamVal(val, incr);
			$val.text(new_val);
			updateFXParamVal(this, new_val);
		});
		
		// toggle parameter contenteditable according to its free/fixed attribute
		$HTMLcontentLD.on("click", ".fx_pfunc_param_name", function() {
			let igraph = $(this).closest('.fx_graph').data('igraph');
			let ifunc = $(this).closest('.fx_graph_pfunc').data('ifunc');
			let param = $(this).closest('.fx_pfunc_param').data('param');
			let new_adjustable = (globalLD.model.getPFuncParamAdjustable(igraph, ifunc, param) === "true") ? "false" : "true";
			// update model
			globalLD.model.updatePFuncParamAdjustable(igraph, ifunc, param, new_adjustable);
			saveLocalLDVersion(id_ld);
			// update view
			let div_param = $(this).closest(".fx_pfunc_param");
			let showOrHide = (new_adjustable === "true") ? 'show' : 'hide';
			let title = (new_adjustable === "true") ? __('Cliquez pour fixer ce paramètre') : __('Cliquez pour libérer ce paramètre');
			div_param.find(".fx_pfunc_param_val").attr('contenteditable', new_adjustable).toggleClass('readonly');
			div_param.find(".fx_pfunc_param_increment")[showOrHide]();
			div_param.find(".fx_pfunc_param_name").attr('title', title);
			globalLD.view.displayDistances(igraph, ifunc);
		});
		
		// Select plot to be fitted
		$HTMLcontentLD.on("click", ".fx_fit_div_col_pick", function() {
			let igraph = $(this).closest('.fx_graph').data('igraph');
			let ifunc = $(this).closest('.fx_graph_pfunc').data('ifunc');
			$HTMLcontentLD.find('.fx_color_picker').data('item', "fit_plot").data('igraph', igraph).data('ifunc', ifunc);
			globalLD.view.displayColorPicker(this, true);
		});

		// auto fit
		$HTMLcontentLD.on("click", ".fx_pfunc_div_auto_fit", function() {
			let updateParamatersViewModel = function(fittedParameters, that) {
				fittedParameters.forEach(function(param){
					let uncertainty = param.uncertainty ? formatFXNumber(param.uncertainty, null, 2) : '';
					let val = formatFXNumberWithU(param.value, null, uncertainty, 2);
					let target = that.closest('.fx_graph_pfunc').find(`.fx_pfunc_param[data-param='${param.name}']`);
					updateFXParamVal(target, val, false);
					target.children('.fx_pfunc_param_val').text(val);
				});
			}
			let igraph = $(this).closest('.fx_graph').data('igraph');
			let ifunc = $(this).closest('.fx_graph_pfunc').data('ifunc');
			let $function = $XMLcodeLD.find('graph').eq(igraph).find("function").eq(ifunc);
			let iplot = $function.attr('fit_iplot');
			let fittedParameters = globalLD.model.autoFit(igraph, ifunc, iplot);
			if (fittedParameters[0] === "error") {
				alertGently(fittedParameters[1]);
			} else {
				// update the model and view for parameters
				updateParamatersViewModel(fittedParameters, $(this));
				// re-run with new initial values
				let fittedParameters2 = globalLD.model.autoFit(igraph, ifunc, iplot);
				if (fittedParameters2[0] === "error") {
					alertGently(fittedParameters2[1]);
				} else {
					updateParamatersViewModel(fittedParameters2, $(this));
					fittedParameters = fittedParameters2;
				}
				// update the model and view for uncertainties
				fittedParameters.forEach(function(param){
					let uncertainty = param.uncertainty ? formatFXNumber(param.uncertainty, null, 2) : '';
					let $target = $function.find('parameter').filter(function() {
						return $(this).find('name').text() === param.name;
					});
					globalLD.model.updateUncertaintyModel($target, uncertainty);
					globalLD.view.updateUncertaintyView($target, uncertainty);
				});
				saveLocalLDVersion(id_ld);
			}
		});
		
		/**
		 *  update a parameter value
		 * @param {dom} target
		 * @param {string} val
		 * @param {boolean} save_locally
		 */
		function updateFXParamVal(target, val, save_locally=true) {
			let $param = $(target).closest('.fx_pfunc_param');
			let $val = $param.find(".fx_pfunc_param_val");
			if (isNaN(Number(val))) {
				$val.addClass("fx_error");
				alertGently(__("Cette valeur n'est pas reconnue"));
			} else {
				$val.removeClass("fx_error");
				let igraph = $(target).closest('.fx_graph').data('igraph');
				let ifunc = $(target).closest('.fx_graph_pfunc').data('ifunc');
				let param = $param.data('param');
				globalLD.model.updatePFuncParam(igraph, ifunc, param, val);
				if (save_locally){
					saveLocalLDVersion(id_ld);
				}
				globalLD.view.displayGraphFlot(igraph);
				globalLD.view.displayDistances(igraph, ifunc);
			}
		}

		/**
		 * positive or negative increment on the last significative digit of the number
		 * mandatory to use operations with integers => cut the decimal part of the number
		 * @param {string} txt_num
		 * @param {int} incr
		 */
		function incrementFXParamVal(txt_num, incr) {
			if (isNaN(Number(txt_num))) {
				return txt_num;
			}
			let significand, exp, int, dec, nb_dec;
			let epos = txt_num.toLowerCase().indexOf("e");
			let ppos = txt_num.indexOf(".");
			if (epos > 0) {
				exp = txt_num.substring(epos + 1);
				significand = txt_num.substring(0, epos);
			} else {
				exp = "";
				significand = txt_num;
			}
			if (ppos > -1) {
				dec = significand.substring(ppos + 1);
				nb_dec = dec.length;
				int = ppos === 0 ? 0 : significand.substring(0, ppos);
			} else {
				nb_dec = 0;
				int = significand;
			}
			let is_negative = parseFloat(significand) < 0;
			if (is_negative) {
				dec = String(parseInt(dec) - incr);
			} else {
				dec = String(parseInt(dec) + incr);
			}
			if (int === "-0" && parseInt(dec) === 0) {
				int = "0";
			}
			if (parseInt(dec) < 0) {
				if (int === "0") {
					int = "-0";
					dec = "0".repeat(nb_dec - 1) + "1";
				} else if (int === "-1") {
					int = "-0";
					dec = dec = "9".repeat(nb_dec);
				} else {
					int = String(parseInt(int) + incr);
					dec = "9".repeat(nb_dec);
				}
			} else if (dec.length > nb_dec) {
				dec = "0".repeat(nb_dec);
				int = String(parseInt(int) + incr);
			} else if (dec.length < nb_dec) {
				dec = "0".repeat(nb_dec - dec.length) + dec;
			}
			let incr_val = int + (nb_dec ? "." + dec : "");
			return incr_val + (exp ? "E" + exp : "");
		}

		// focusout func interval bound
		$HTMLcontentLD.on("focusout", ".fx_pfunc_div_interval input", function() {
			let val = formatFXInputNumValue($(this).val());
			$(this).val(val);
			if (val === "" || !isNaN(Number(val))) {
				$(this).removeClass("fx_error");
				let bound = $(this).attr('name');
				let igraph = $(this).closest('.fx_graph').data('igraph');
				let ifunc = $(this).closest('.fx_graph_pfunc').data('ifunc');
				globalLD.model.updatePFuncInterval(igraph, ifunc, bound, val);
				saveLocalLDVersion(id_ld);
				globalLD.view.displayGraphFlot(igraph);
				globalLD.view.displayDistances(igraph, ifunc);
			} else {
				$(this).addClass("fx_error");
				alertGently(__("Cette valeur n'est pas reconnue"));
			}
		});

		// click delete
		$HTMLcontentLD.on("click", ".fx_pfunc_div_delete", function () {
			let igraph = $(this).closest('.fx_graph').data('igraph');
			let ifunc = $(this).closest('.fx_graph_pfunc').data('ifunc');
			// updates
			globalLD.model.deletePFunc(igraph, ifunc);
			saveLocalLDVersion(id_ld) ;
			globalLD.view.displayGraphFlot(igraph) ;
			globalLD.view.displayGraphPFuncs(igraph) ;
		});

		// click the predefined funcs menu
		let pfuncs_menu = {
			selector: '.fx_pfunc_div_menu',
			appendTo: $HTMLcontentLD,
			trigger: 'left',
			/*trigger: 'hover',
            delay: 300,*/
			autoHide: false,
			position: function (opt) {
				opt.$menu.position({
					my: 'left top',
					at: 'left top',
					of: opt.$trigger,
					offset: "0 0"
				});
			},
			events: {
				show: function(opt) {
					let igraph = $(this).closest('.fx_graph').data('igraph');
					if (opt.items['log_x_axis']) {
						opt.items['log_x_axis'].visible = $XMLcodeLD.find('graph').eq(igraph).find('xscale').text() === 'log';
					}
					blurFormulaInput();
				}
			},
			items: {
				log_x_axis: {
					name: __("Fonctions en échelle log sur x"),
					items: {
						lin: {
							name: __("affine"),
							callback: function () { setPredefinedFunction(this, 'a*log(x)+b'); }
						}
					}
				},
				simples: { //sous menu
					name: __("Fonctions simples"),
					items: {
						lin: {
							name: __("affine"),
							callback: function(){ setPredefinedFunction(this, 'a*x+b'); }
						},
						pol2: {
							name: __("polynôme de degré") + " 2",
							callback: function(){ setPredefinedFunction(this, 'a*x^2+b*x+c'); }
						},
						pol3: {
							name: __("polynôme de degré") + " 3",
							callback: function(){ setPredefinedFunction(this, 'a*x^3+b*x^2+c*x+d'); }
						},
						pol4: {
							name: __("polynôme de degré") + " 4",
							callback: function(){ setPredefinedFunction(this, 'a*x^4+b*x^3+c*x^2+d*x+e'); }
						},
						pow: {
							name: __("puissance"),
							callback: function(){ setPredefinedFunction(this, 'a*(b*x+c)^d+e'); }
						},
						exp: {
							name: __("exponentielle"),
							callback: function(){ setPredefinedFunction(this, 'a*exp(b*x+c)+d	'); }
						},
						log: {
							name: __("logarithmique"),
							callback: function(){ setPredefinedFunction(this, 'a*ln(b*x+c)+d'); }
						},
						sin: {
							name: __("sinus"),
							callback: function(){ setPredefinedFunction(this, 'a*sin(2*pi/b*(x+c))+d'); }
						},
						tan: {
							name: __("tangente"),
							callback: function(){ setPredefinedFunction(this, 'a*tan(2*pi/b*(x+c))+d'); }
						},
						cosh: {
							name: __("cosinus hyperbolique"),
							callback: function(){ setPredefinedFunction(this, 'a/2*(exp(b*x)+exp(-b*x))+c'); }
						},
						sinh: {
							name: __("sinus hyperbolique"),
							callback: function(){ setPredefinedFunction(this, 'a/2*(exp(b*x)-exp(-b*x))+c'); }
						}
					}
				},
				oscillations: {
					name: __("Oscillations et battements"),
					items: {
						pp_osc: {
							name: __("oscillations amorties pseudo-périodiques"),
							callback: function(){ setPredefinedFunction(this, 'a*exp(-α*x)*cos(ω*x+φ)+b'); }
						},
						crit_osc: {
							name: __("oscillations en amortissement critique"),
							callback: function(){ setPredefinedFunction(this, '(a+b*x)*exp(c*x)+d'); }
						},
						beats: {
							name: __("battements"),
							callback: function(){ setPredefinedFunction(this, 'a*cos(ω1*x+φ1)*cos(ω2*x+φ2)+b'); }
						}
					}
				},
				pH: {
					name: __("Titrages pHmétriques"),
					items: {
						MAB: {
							name: __("titrage d'un monoacide fort par une base forte"),
							callback: function(){ setPredefinedFunction(this, '((Vacid+Vwater)*(1E-14/10^-y-10^-y)+Cacid*Vacid)/(10^-y+Cbase-1E-14/10^-y)', __("titrage d'un monoacide fort par une base forte")) ; }
						},
						wMAB: {
							name: __("titrage d'un monoacide faible par une base forte"),
							callback: function(){ setPredefinedFunction(this, '((Vacid+Vwater)*(1E-14/10^-y-10^-y)+Cacid*Vacid/(1+10^-y/10^-pKa))/(10^-y+Cbase-1E-14/10^-y)', __("titrage d'un monoacide faible par une base forte")) ; }
						},
						DAB: {
							name: __("titrage d'un diacide par une base forte"),
							callback: function(){ setPredefinedFunction(this, '((Vacid+Vwater)*(1E-14/10^-y-10^-y)+Cacid*Vacid*(10^-y+2*10^-pKa2)/(10^-pKa2+10^-y+10^(-2*y)/10^-pKa1))/(10^-y+Cbase-1E-14/10^-y)', __("titrage d'un diacide par une base forte")) ; }
						},
						TAB: {
							name: __("titrage d'un triacide par une base forte"),
							callback: function(){ setPredefinedFunction(this, '((Vacid+Vwater)*(1E-14/10^-y-10^-y)+Cacid*Vacid*(10^(-2*y)+2*10^-pKa2*10^-y+3*10^-pKa2*10^-pKa3)/(10^-pKa2*10^-pKa3+10^-pKa2*10^-y+10^(-2*y)+10^(-3*y)/10^-pKa1))/(10^-y+Cbase-1E-14/10^-y)', __("titrage d'un triacide par une base forte")) ; }
						},
						MBA: {
							name: __("titrage d'une monobase forte par un acide fort"),
							callback: function(){ setPredefinedFunction(this, '((Vbase+Vwater)*(-1E-14/10^-y+10^-y)+Cbase*Vbase)/(Cacid+1E-14/10^-y-10^-y)', __("titrage d'une monobase forte par un acide fort")) ; }
						},
						wMBA: {
							name: __("titrage d'une monobase faible par un acide fort"),
							callback: function(){ setPredefinedFunction(this, '((Vbase+Vwater)*(-1E-14/10^-y+10^-y)+Cbase*Vbase/(1+10^-pKa/10^-y))/(Cacid+1E-14/10^-y-10^-y)', __("titrage d'une monobase faible par un acide fort")) ; }
						},
						DBA: {
							name: __("titrage d'une dibase par un acide fort"),
							callback: function(){ setPredefinedFunction(this, '((Vbase+Vwater)*(-1E-14/10^-y+10^-y)+Cbase*Vbase*(10^-pKa1+2*10^-y)/(10^-pKa1*10^-pKa2/10^-y+10^-y+10^-pKa1))/(Cacid+1E-14/10^-y-10^-y)', __("titrage d'une dibase par un acide fort" )) ; }
						},
						TBA: {
							name: __("titrage d'une tribase par un acide fort"),
							callback: function(){ setPredefinedFunction(this, '((Vbase+Vwater)*(-1E-14/10^-y+10^-y)+Cbase*Vbase*(10^-pKa1*10^-pKa2+2*10^-pKa1*10^-y+3*10^(-2*y))/(10^-pKa1*10^-pKa2*10^-pKa3/10^-y+10^-pKa1*10^-pKa2+10^-pKa1*10^-y+10^(-2*y)))/(Cacid+1E-14/10^-y-10^-y)', __("titrage d'une tribase par un acide fort")) ; }
						}
					}
				},
				chem_cin: {
					name: __("Cinétique de réaction"),
					items: {
						first_order: {
							name: __("1er ordre"),
							callback: function(){ setPredefinedFunction(this, 'C0*exp(-a*k*x)'); }
						},
						second_order: {
							name: __("2ème ordre"),
							callback: function(){ setPredefinedFunction(this, 'C0/(1+a*k*C0*x)'); }
						},
						Michaelis: {
							name: __("cinétique de Michaëlis-Menten"),
							callback: function(){ setPredefinedFunction(this, 'Vm*x/(Km+x)'); }
						}
					}
				},
				stats: {
					name: __("Statistique"),
					items: {
						normal: {
							name: __("distribution normale (gaussienne)"),
							callback: function(){ setPredefinedFunction(this, 'a/(σ*sqrt(2*pi))*exp(-(x-xmoy)^2/(2*σ^2))'); }
						},
						logistic: {
							name: __("logistique (sigmoïde)"),
							callback: function(){ setPredefinedFunction(this, 'K/(1+a*exp(-r*(x-x0)))'); }
						}
					}
				}
			}
		}
		$HTMLcontentLD.contextMenu(pfuncs_menu);
		let histogram_pfuncs_menu = {
			selector: '.fx_pfunc_div_menu_histogram',
			appendTo: $HTMLcontentLD,
			trigger: pfuncs_menu.trigger,
			autoHide: pfuncs_menu.autoHide,
			position: pfuncs_menu.position,
			events: pfuncs_menu.events,
			items: {
				stats: {
					name: pfuncs_menu.items.stats.name,
					items: { 
						normal: pfuncs_menu.items.stats.items.normal,
						uniform: {
							name: __("distribution uniforme"),
							callback: function(){ setPredefinedFunction(this, 'x&lt;a?0:(x&gt;b?0:k/(b-a))'); }
						},
						exponential: {
							name: __("distribution exponentielle"),
							callback: function(){ setPredefinedFunction(this, 'x&lt;0?0:a*λ*exp(-λ*x)'); }
						},
						gamma: {
							name: __("distribution gamma"),
							callback: function(){ setPredefinedFunction(this, 'x&lt;0?0:a*x^(k-1)*exp(-x/θ)/(gamma(k)*θ^k)'); }
						},
						chi2: {
							name: __("distribution khi-2"),
							callback: function(){ setPredefinedFunction(this, 'x&lt;0?0:a*x^(k/2-1)*exp(-x/2)/(2^(k/2)*gamma(k/2))'); }
						},
					}
				}
			}
		}
		$HTMLcontentLD.contextMenu(histogram_pfuncs_menu);

		/**
		 * Select a predefined PFunc
		 * @param {dom} menu
		 * @param {string} formula
		 * @param {string} description
		 */
		function setPredefinedFunction(menu, formula, description = "") {
			let igraph = $(menu).closest('.fx_graph').data('igraph');
			let ifunc = $(menu).closest('.fx_graph_pfunc').data('ifunc');
			if (typeof ifunc === "undefined") {
				let $graph = $XMLcodeLD.find('graph').eq(igraph);
				ifunc = $graph.find('function').length;
			}
			globalLD.model.updatePFunc(formula, igraph, ifunc, description);
			saveLocalLDVersion(id_ld);
			globalLD.view.displayGraphFlot(igraph);
			globalLD.view.displayGraphPFuncs(igraph);
		}
	}
	

	function setColorPickerControls() {

		$HTMLcontentLD.on("mouseleave", ".fx_color_picker, .fx_plot_fit_color_picker", function () {
			$(this).appendTo($HTMLcontentLD.find(".fx_wrapper")); // keep the color picker alive
			$(this).hide();
			if ($(this).data('item', "fit_plot")){
				$(".fx_color_picker_plots").remove();
			}
		});

		/**
		 * @param action action to do = change color of a plot or of a function
		 * @param i_item number of the clicked item
		 */
		$HTMLcontentLD.on("click", ".fx_color_picker i, .fx_plot_fit_color_picker i", function (e) {
			e.stopPropagation();
			let color = $(this).data("color");
			let $col_picker = $(e.currentTarget).parent();
			$col_picker.hide();
			let item = $col_picker.data("item");
			if (item === "plot") { // set the color of a plot
				let $col_picker_btn = $(this).closest(".fx_color_picker_btn");
				$col_picker_btn.attr("data-color", color);
				$col_picker_btn.children("i").css("color", color);
				$col_picker.appendTo($HTMLcontentLD.find(".fx_wrapper")); // keep the color picker alive
			} else if (item === "pfunc") { // set the color of a Pfunc
				let igraph = $col_picker.data("igraph");
				let ifunc = $col_picker.data("ifunc");
				$col_picker.appendTo($HTMLcontentLD.find(".fx_wrapper")); // keep the color picker alive
				$XMLcodeLD.find('graph').eq(igraph).find("function").eq(ifunc).attr('color', color);
				saveLocalLDVersion(id_ld);
				globalLD.view.displayGraphFlot(igraph);
				globalLD.view.displayGraphPFuncs(igraph);
			} else if (item === "fit_plot"){
				let $col_picker_btn = $(this).closest('.fx_color_picker_btn');
				let igraph = $col_picker.data('igraph');
				let ifunc = $col_picker.data('ifunc');
				let iplot = $(this).data('iplot');
				$col_picker_btn.attr('data-color', color);
				$col_picker_btn.children('i').css('color', color);
				$XMLcodeLD.find('graph').eq(igraph).find('function').eq(ifunc).attr('fit_iplot', iplot);
				saveLocalLDVersion(id_ld);
				globalLD.view.displayGraphFlot(igraph);
				globalLD.view.displayGraphPFuncs(igraph);
				$col_picker.remove();
			}
		});
	}

	/**
	 * There are 2 inputs for formulas: in column controls panel textarea or in Pfuncs div
	 * this function doesn't fire when clicking on contextmenus => need to trigger blurFormulaInput manually
	 */
	function setFormulasControls() {
		let $help_panel2 = $HTMLcontentLD.find(".fx_math_formula_help2");
		$HTMLcontentLD.on("click", function (event) {
			let $active_formula = $HTMLcontentLD.find(".fx-active-formula");
			let $help_panel = $HTMLcontentLD.find(".fx_math_formula_help");
			let nb_active_formula = $active_formula.length;
			if (nb_active_formula) {
				if (nb_active_formula === 1 && $(event.target).hasClass("fx_formula_help_symbol")) { // a click on a symbol of the help panel
					// add a symbol from the help panel
					event.preventDefault();
					let symbol = $(event.target).data("symbol");
					if (symbol) {
						let caret_pos = $active_formula.get(0).selectionStart;
						let formula = $active_formula.val();
						$active_formula.val(formula.slice(0, caret_pos) + symbol + formula.slice(caret_pos));
						$active_formula.focus();
						$active_formula.get(0).selectionEnd = caret_pos + symbol.length;
					}
				} else if (nb_active_formula === 1 &&
					($(".fx_formula_help2_open").has(event.target).length > 0 || $(event.target).hasClass("fx_formula_help2_open"))){
					$help_panel2.show();
				} else if (nb_active_formula === 1 &&
					($help_panel.has(event.target).length > 0 || $help_panel2.has(event.target).length > 0)){
					// do not close the help panel
				} else if (nb_active_formula >= 2) { // a click on another formula input
					$(event.target).removeClass("fx-active-formula");
					blurFormulaInput();
					$(event.target).addClass("fx-active-formula");
					globalLD.view.displayMathFormulaHelp(event.target, ".fx_graph_pfunc");
				} else if (!($(event.target).is($active_formula))) { // not a click on the active formula
					blurFormulaInput();
				}
			}
		});
		$HTMLcontentLD.on("click", ".fx_math_formula_help2_close", function() {
			$help_panel2.hide();
		});
		$help_panel2.draggable({
			handle: ".fx_math_formula_help2_title"
		});
	}

	/**
	 * if there is an active formula (open formula panel), remove the formula panel and update the formula
	 */
	function blurFormulaInput() {
		let $active_formula = $HTMLcontentLD.find(".fx-active-formula");
		if ($active_formula.length) {
			// console.log("blurFormulaInput with", $active_formula.length, "formulas");
			$active_formula.removeClass("fx-active-formula");
			$active_formula.blur();

			// hide formula help
			let $formula_help = $HTMLcontentLD.find(".fx_math_formula_help");
			$formula_help.hide();
			$formula_help.appendTo($HTMLcontentLD.find(".fx_wrapper")); // keep the help panel alive

			// format formula
			let formula = $active_formula.val();
			let new_formula = formatFXInputFormula(formula);
			$active_formula.val(new_formula);

			// which formula is it ?
			let is_col_formula = false, is_pfunc_formula = false;
			if ($active_formula.hasClass("fx_col_formula")) {
				is_col_formula = true;
			} else if ($active_formula.hasClass("fx_pfunc_input")) {
				is_pfunc_formula = true;
			}

			// check and action
			let old_formula;
			if (is_col_formula) { // column formula
				// check
				let $col_prop_panel = $HTMLcontentLD.find(".fx_col_prop_panel");
				let icol = $col_prop_panel.data('icol');
				let $xml_col = $XMLcodeLD.find('column').eq(icol);
				old_formula = $xml_col.find('formula').text();
				if (new_formula !== old_formula && old_formula === "") {
					let nb_col_values = globalLD.model.getNbValuesInCol(icol);
					if (nb_col_values && !confirm(__("Vous définissez une formule dans une colonne qui contient des valeurs.\nVous allez écraser toutes ces valeurs par des valeurs calculées."))) {
						$active_formula.val("");
					}
				}
				// gives back the modal cover its default behaviour
				$HTMLcontentLD.find(".fx_modal_cover").click(function () {
					$col_prop_panel.hide();
					globalLD.view.toggleFXModal(false);
				});
			} else if (is_pfunc_formula) { // parametered function
				// check
				let igraph = $active_formula.closest('.fx_graph').data('igraph');
				let ifunc = $active_formula.closest('.fx_graph_pfunc').data('ifunc');
				let $graph = $XMLcodeLD.find('graph').eq(igraph);
				if (typeof ifunc !== "undefined") {
					old_formula = $graph.find('function').eq(ifunc).find('formula').text();
				} else { // it's a new Pfunc
					old_formula = "";
					ifunc = $graph.find('function').length;
				}
				// action
				if (new_formula !== old_formula) {
					try {
						globalLD.model.updatePFunc(new_formula, igraph, ifunc);
					} catch (e) {
						alertGently(globalLD.view.fx_errors[e]);
					}
					saveLocalLDVersion(id_ld);
					globalLD.view.displayGraphFlot(igraph);
					globalLD.view.displayGraphPFuncs(igraph);
				}
			}
		}
	}
	
	/**
	 * @param {int} icol
	 * @param {string} old_formula
	 * @param {string} formula
	 * @returns {boolean}
	 */
	function updateColFormula (icol, old_formula, formula) {
		let update_values = true;
		if (formula === "") {
			update_values = confirm(__("Vous supprimez une formule.\nVoulez-vous aussi supprimer toutes les valeurs de la colonne définies par cette formule ?"));
		}
		let updated = globalLD.model.updateColFormula(icol, formula, update_values);
		saveLocalLDVersion(id_ld);
		refreshDataset(updated, true);
		return true;
	}

}

/**
 * trim then replace accentuated characters by simple characters, spaces by _ and remove starting digits
 * @param {string} str
 * @param {Boolean} rmStartingDigits (optional)
 * @return {string}
 */
function formatStringToAlphanum(str, rmStartingDigits = true) {
	str = str
		.trim()
		.normalize('NFD')
		.replace(/[\u0300-\u036f]/g, "")
		.replace(/[^A-Za-z\d]+/g, '_');
	if (rmStartingDigits) {
		str = str.replace(/^\d+/, '');
	}
	return str;
}

/**
 * remove accents and replace , between digits by .
 * @param {string} formula
 * @returns {string}
 */
function formatFXInputFormula(formula) {
	formula = formula.trim();
	if (formula.charAt(0) === "=") {
		formula = formula.substring(1);
		formula = formula.trim();
	}
	return formula
		.replace(/(\d),(\d)/g, "$1.$2")
		.replace(/(\d)e([-+\d])/g, "$1E$2")
		.normalize('NFD')
		.replace(/[\u0300-\u036f]/g, "");
}

/**
 * remove blank space and replace , by . and e by E
 * @param {string} val should be a string
 * @returns {string}
 */
function formatFXInputNumValue(val) {
	if (typeof val === 'number') {
		return val.toString();
	}
	if (typeof val !== 'string') {
		return "";
	}
	return val
		.replace(/\s/g, '')
		.replace("e", "E")
		.replace(",", ".");
}

/**
 * check the bounds of the axis of a graph and returns their values
 * used by the model for x-axis and by the view for y-axis
 * @param {Number | null} min Number > 0 if is_log
 * @param {Number | null} max Number > 0 if is_log
 * @param {Boolean} update_min
 * @param {Boolean} update_max
 * @param {Boolean} is_log
 * @returns {{min: Number, max: Number}}
 */
function manageFXAxisBounds(min, max, update_min, update_max, is_log) {
	if (min === null && max === null) { // no points in the plots => default values
		if (is_log) {
			min = 0.1;
			max = 10;
		} else {
			min = -1;
			max = 1;
		}
	} else {
		if (min === null) {
			min = max;
		} else if (max === null) {
			max = min;
		}
		if (max === min) {
			if (update_min) {
				if (min === 0) {
					min = -1;
				} else {
					min = min - 0.1 * Math.abs(min);
				}
			}
			if (update_max) {
				if (max === 0) {
					max = 1;
				} else {
					max = max + 0.1 * Math.abs(max);
				}
			}
		} else if (is_log) {
			min = update_min ? 0.9 * min : min;
			max = update_max ? 1.1 * max : max;
		} else { // not log : min & max have 5% margins relatively to the plotted points
			let margin = (max - min) / 20 ;
			min = update_min ? min - margin : min;
			max = update_max ? max + margin : max;
		}
	}
	if (min === Infinity) {
		min = 1.7976931348623157e+305;
	}
	if (max === Infinity) {
		max = 1.7976931348623157e+308;
	}
	if (min === -Infinity) {
		min = -1.7976931348623157e+308;
	}
	if (max === Infinity) {
		max = -1.7976931348623157e+305;
	}
	return {min: min, max: max};
}
