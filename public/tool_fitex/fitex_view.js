/*
 * This file defines the fitex_view object
 * The display functions are called by the control functions. They define the HTML structure of the dom in $HTMLcontentLD
 * They get their data by interrogating directly the XML model $XMLcodeLD or by calling the get functions of the model : globalLD.model.getXXX()
 * */

let fitex_view = (function(id_ld, edition) {
	"use strict";

	const globalLD = global_tab_ld[id_ld];
	const $XMLcodeLD = $(globalLD.ld_content);
	const $settings = $XMLcodeLD.find('settings');
	const $HTMLcontentLD = $("#labdoc_content_"+id_ld);
	const stat_ind = new Map();
	stat_ind
		.set('n', {
			code: '<em>N</em>',
			name: __('Nombre de valeurs'),
			title: __('Nombre de valeurs')})
		.set('sum', {
			code: '∑',
			name: __('Somme'),
			title: __('Somme') + __('&nbsp;:') + ' ∑{ xi }' })
		.set('min', {
			code: '<em>min</em>',
			name: __('Minimum'),
			title: __('Minimum') })
		.set('max', {
			code: '<em>max</em>',
			name: __('Maximum'),
			title: __('Maximum') })
		.set('mean', {
			code: '<em>' + __("moy") + '</em>',
			name: __('Moyenne'),
			title: __('Moyenne') + __('&nbsp;:') + ' ∑{ xi } / N' })
		.set('med', {
			code: '<em>med</em>',
			name: __('Médiane'),
			title: __('Médiane') })
		.set('std_dev', {
			code: '<em>s</em><sub>n-1</sub>',
			name: __('Ecart-type'),
			title: __('Ecart-type') + __('&nbsp;:') + ' √{ ∑{ (xi-m)² } / (N-1) }' })
		.set('c_var', {
			code: '<em>CV</em>',
			name: __('Coef. variation'),
			title: __('Coef. variation') + __('&nbsp;:') + ' s(n-1) / m' });
	const fx_errors = { // in priority order as calculated in model
		"parse_error": __("Erreur de syntaxe - Impossible d'analyser la formule"),
		"circ_ref": __("Une référence circulaire empêche le calcul de la formule"), // only for col_formulas
		"undef_symbol": __("Symbole non reconnu - Impossible de calculer la formule"),
		"insert_in_col_formula": __("Des valeurs n'ont pas pu être insérées dans une colonne déjà définie par une formule"),
		"insert_text_in_num": __("Des valeurs de type 'texte' n'ont pas pu être insérées dans une colonne de type 'numérique'")
	};
	const fx_colors = ["#8400CD", "#009f81", "#009DF9",
		"#FF6E3A", "#E20134", "#000000",
		"#00deb2", "#FF5AAF", "#9F0162"];

	let html_struct = `
		<div class="fx_wrapper">
			<div class="fx_table_wrapper">
				<table class="fx_table" tabindex="0">
					<thead><tr class="fx_header"></tr></thead>
					<tbody><tr class='fx_formula_row'></tr></tbody>
				</table>
			</div>
			<div class='sizing-bar sizing-bar-vertical ui-resizable-s ui-resizable-n'/>
			<div class='fx_export_panel fx_panel' tabindex="0">
				<div class="fx_export_title">${__('Exporter toutes les données')}</div>
				<div class='fx_export_panel_format fx_export_div'>
					<label>${__('Format d’export') + __("&nbsp;:")} 
						<select name="fx_export_panel_format_select">
							<option value="xlsx" selected>xlsx</option>
							<option value="csv">csv</option>
						</select>
					</label>
				</div>
				<div class="fx_export_panel_options_title fx_export_div" style="display: none;"><p>${__('Options d’export') + __("&nbsp;:")}</p>
					<div class='fx_export_panel_decimal_separator fx_export_panel_csv'>
						<label>${__('Séparateur décimal') + __("&nbsp;:")} 
							<select name="fx_export_panel_decimal_separator_select">
								<option value="dot" ${window.langLong() === 'fr_FR' ? '' : 'selected'}>${__('point')}</option>
								<option value="comma" ${window.langLong() === 'fr_FR' ? 'selected' : 'disabled'}>${__('virgule')}</option>
							</select>
						</label>
					</div>
					<div class='fx_export_panel_column_separator fx_export_panel_csv'>
						<label>${__('Séparateur de colonnes') + __("&nbsp;:")} 
							<select name="fx_export_panel_column_separator_select">
								<option value="comma" ${window.langLong() === 'fr_FR' ? 'disabled' : 'selected'}>${__('virgule')}</option>
								<option value="semicolon" ${window.langLong() === 'fr_FR' ? 'selected' : ''}>${__('point virgule')}</option>
								<option value="tabulation">${__('tabulation')}</option>
							</select>
						</label>
					</div>
				</div>
				<div class="fx_button_div">
					<button class="fx_validate_btn fx_export_validate">${__('Valider')}</button>
					<button class="fx_export_cancel">${__('Annuler')}</button>
				</div>
			</div>
			<div class='fx_constant_panel fx_panel' tabindex="0">
				<table>
					<thead><tr>
						<th>${__('Constante')}</th>
						<th>${__('Valeur')}</th>
					</tr></thead>
					<tbody></tbody>
				</table>
				${ edition ?`<div class="fx_button_div">
					<button class="fx_validate_btn fx_constants_validate">${__('Valider')}</button>
					<button class="fx_constants_cancel">${__('Annuler')}</button>
				</div>` : "" }
			</div>`;
	if(edition) {
		html_struct += `
			<div class='fx_end_graph'></div>
			<div class='fx_import_panel fx_panel'>
				<form>
					<div class="fx_import_title">${__('Importer un fichier de données')} (.csv, .ods, .xlsx)</div>
					<div class="fx_import_div"><label>${__("Mode d'insertion des données") + __("&nbsp;:")}&nbsp
						<select name="mode">
							<option value="overwrite">${__('remplacer tout')}</option>
							<option value="appendColumns">${__('ajouter en colonnes')}</option>
							<option value="appendRows">${__('ajouter en lignes')}</option>
						</select>
					</label></div>
					<div class="fx_import_div">
						<label><input type="checkbox" name="has_header"/>&nbsp;${__("Les données importées comportent une ligne d'en-tête")}</label>
					</div>
					<div class="fx_import_div" id="fx_import_panel_csv_options_wrapper">
						<div class="fx_import_panel_csv_options_title" onclick="$('#fx_import_panel_csv_options').toggle(); $('#fx_import_panel_csv_options_wrapper i.toggle-section').toggleClass('fa-caret-right').toggleClass('fa-caret-down')">
							<i class="fa fa-caret-right toggle-section"></i>
							<span>${__('Options d’import pour les fichiers CSV')}</span>
						</div>
						<div id="fx_import_panel_csv_options" style="display: none;">
							<div class='fx_import_panel_column_separator fx_import_panel_csv'>
								<label>${__('Séparateur de colonnes') + __("&nbsp;:")} 
									<select name="fx_import_panel_column_separator_select">
										<option value="auto" selected>${__('auto')}</option>
										<option value="comma">${__('virgule')}</option>
										<option value="semicolon">${__('point virgule')}</option>
										<option value="tabulation">${__('tabulation')}</option>
									</select>
								</label>
							</div>
							<div class='fx_import_panel_inner_column_separator fx_import_panel_csv'>
								<label>${__('Séparateur secondaire de colonnes') + __("&nbsp;:")} 
									<select name="fx_import_panel_inner_column_separator_select" disabled>
										<option value="none" selected>${__('aucun')}</option>
										<option value="comma" disabled>${__('virgule')}</option>
										<option value="semicolon" disabled>${__('point virgule')}</option>
										<option value="tabulation" disabled>${__('tabulation')}</option>
									</select>
								</label>
							</div>
						</div>
					</div>
					<div class="fx_import_div">${__('Fichier') + __("&nbsp;:")} <input class="fx_file_import" type="file" accept=".csv,.ods,.xlsx"/></div>
					<div class="fx_import_alerts"></div>
				</form>
			</div>

			<div class="fx_settings_panel fx_panel">
				<div class="fx_setting_container">
					<div class="fx_setting_label">${__("Indicateur d'ajustement")}</div>
					<div class="fx_setting_option">
						<label><input type="checkbox" class="fx_setting_khi2_reduced"/>
							${__("S'il est calculable, afficher le χ² réduit plutôt que l'écart-type résiduel (s<sub>r</sub>)")}
						</label>
						<span class="fx_setting_label_help fx_khi2sr_help"><i class="fas fa-info-circle"></i></span>
					</div>
					<div class="fx_setting_option">
						<label><input type="checkbox" class="fx_setting_khi2_ux_y"/>
							${__("Pour le calcul du χ² réduit, reporter les incertitudes des abscisses sur l'axe Y")}
						</label>
						<span class="fx_setting_label_help fx_report_ux_help"><i class="fas fa-info-circle"></i></span>
					</div>
				</div>
				<div class="fx_setting_container fx_setting_auto_adjust">
					<div class="fx_setting_label">
						<span class="fx_setting_label">${__('Ajustement automatique')}</span>
						<span class="fx_setting_label_help fx_autofit_help"><i class="fas fa-info-circle"></i></span>
					</div>
					<div class="fx_setting_option">
						<label><input type="checkbox" class="fx_setting_normalize"/>
							${__("Pour calculer les incertitudes des paramètres du modèle, normaliser les incertitudes de mesure par le χ² réduit")}
						</label>
						<span class="fx_setting_label_help fx_normalize_help"><i class="fas fa-info-circle"></i></span>
					</div>
				</div>
				<div class="fx_button_div">
					<button class="fx_validate_btn fx_settings_validate">${__('Valider')}</button>
					<button class="fx_settings_cancel">${__('Annuler')}</button>
				</div>
			</div>

			<div class='fx_col_prop_panel fx_panel' tabindex="0">
				<p>${__('Propriétés de la colonne')}</p>
				<div class="fx_nowrap">${__('Type des données') + __("&nbsp;:")}
					<label><input type="radio" class="fx_col_type_num" value="num" name="fx_col_type" checked="checked"> ${__('numérique')}</label>
					<label><input type="radio" class="fx_col_type_txt" value="txt" name="fx_col_type"> ${__('texte')}</label>
				</div>
				<div title="${__('Le nom est affiché sur le graphique')}" class="fx_nowrap">
					<label>${__('Nom') + __("&nbsp;:")} <input type="text" class="fx_col_name" maxlength="60" value=""></label>
				</div>
				<div class="fx_nowrap">
					<span title="${__("Le code désigne la colonne et peut être réutilisé dans les formules") + "\n" + __("Il doit être alphanumérique, ne doit pas commencer par un chiffre et ne doit pas contenir d'espace")}">
						<label>${__('Code') + __("&nbsp;:")} <input type="text" class="fx_col_code" maxlength="12" placeholder="unique" value=""></label>
					</span>
					<span class="fx_col_unit_span fx_col_prop_num_type">
						<label>${__('Unité') + __("&nbsp;:")} <input type="text" class="fx_col_unit" value=""></label>
					</span>
				</div>
				<div class="fx_col_prop_num_type fx_col_prop_div_formula">
					<label for="fx_col_formula">${__('Formule') + __("&nbsp;:")}<br><textarea class="fx_col_formula" spellcheck="false"></textarea></label>
				</div>
				<div class="fx_col_prop_num_type">
					<label><input type="checkbox" class="fx_sc_notation"> ${__('Affichage scientifique')}</label>
				</div>
				<div class="fx_col_prop_num_type fx_nb_digits">
					<label>
						<span class="fx_nb_decimals">${__('Nombre de décimales') + __("&nbsp;:")}</span>
						<span class="fx_nb_significant_digits">${__('Nombre de chiffres significatifs') + __("&nbsp;:")}</span>
						<input type="text" value="" inputmode="decimal">
					</label>
				</div>
				<div class="fx_button_div">
					<button type="button" class="fx_validate_btn fx_col_prop_validate">${__('Valider')}</button>
					<button class="fx_col_cancel">${__('Annuler')}</button>
				</div>
			</div>
			
			<div class='fx_graph_prop_panel fx_panel' tabindex="0">
				<div><input type="text" class="fx_graph_name" placeholder="${__('Titre du graphique')}"></div>
				<table class="fx_graph_prop_plot" data-addplot="0">
					<tr>
						<td><strong>${__('Points')}</strong></td>
						<td>${__('abscisse')+'&nbsp;x&nbsp;'}</td>
						<td>${__('ordonnée')+'&nbsp;y&nbsp;'}</td>
						<td></td>
						<td>${__('incertitude')+'&nbsp;x&nbsp;'}</td>
						<td>${__('incertitude')+'&nbsp;y&nbsp;'}</td>
						<td></td>
						<td>${__("points reliés")}</td>
						<td class="fx_plot_axis">${__("axe")}</td>
					</tr>
					<tr class="fx_add_plot">
						<td><i class="fa fa-plus-circle" title="${__('Ajouter des points')}"></i></td>
						<td colspan="4" class="fx_btn_title"><span>${__('Ajouter des points')}</span></td>
					</tr>
				</table>
				<table class="fx_graph_prop_axis">
					<tr>
						<td><strong>${__('Axes')}&nbsp;&nbsp;</strong></td>
						<td>min</td>
						<td>max</td>
						<td>${__('échelle log.')}</td>
						<td>${__('légende')}</td>
						<td></td>
					</tr><tr>
						<td>x</td>
						<td><input type="text" placeholder="auto" class="fx_axis_bound fx_axis_xmin"/></td>
						<td><input type="text" placeholder="auto" class="fx_axis_bound fx_axis_xmax"/></td>
						<td><input type="checkbox" class="fx_axis_xscale"/></td>
						<td><input type="text" placeholder="auto" class="fx_axis_label fx_axis_xlabel"/></td>
						<td></td>
					</tr><tr>
						<td>y</td>
						<td><input type="text" placeholder="auto" class="fx_axis_bound fx_axis_ymin"/></td>
						<td><input type="text" placeholder="auto" class="fx_axis_bound fx_axis_ymax"/></td>
						<td><input type="checkbox" class="fx_axis_yscale"/></td>
						<td><input type="text" placeholder="auto" class="fx_axis_label fx_axis_ylabel"/></td>
						<td></td>
					</tr><tr class="fx_axis_y2">
						<td>y2</td>
						<td><input type="text" placeholder="auto" class="fx_axis_bound fx_axis_y2min"/></td>
						<td><input type="text" placeholder="auto" class="fx_axis_bound fx_axis_y2max"/></td>
						<td><input type="checkbox" class="fx_axis_y2scale"/></td>
						<td><input type="text" placeholder="auto" class="fx_axis_label fx_axis_y2label"/></td>
						<td><i class="far fa-trash-alt fx_remove_axis_y2" title="${__('Supprimer le deuxième axe y')}"></i></td>
					</tr><tr class="fx_add_axis_y2">
						<td><i class="fa fa-plus-circle" title="${__('Ajouter un deuxième axe y')}"></i></td>
						<td colspan="4" class="fx_btn_title"><span>${__('Ajouter un deuxième axe y')}</span></td>
					</tr>
				</table>
				<div class="fx_button_div">
					<button class="fx_validate_btn fx_graph_prop_validate">${__('Valider')}</button>
					<button class="fx_graph_prop_cancel">${__('Annuler')}</button>
				</div>
			</div>
			
			<div class='fx_histogram_prop_panel fx_panel' tabindex="0">
				<div><input type="text" class="fx_graph_name" placeholder="${__('Titre du graphique')}"></div>
				<table class="fx_graph_prop_plot" data-addplot="0">
					<tr>
						<td colspan="3"><strong>${__('Série')}</strong></td>
					</tr>
					<tr class="fx_add_plot">
						<td><i class="fa fa-plus-circle" title="${__('Ajouter une série')}"></i></td>
						<td colspan="2" class="fx_btn_title"><span> ${__('Ajouter une série')}</span></td>
					</tr>
				</table>
				<strong>${__('Nombre de classes')}</strong> <input type="text" placeholder="auto" class="fx_global_options_bins_number"/>
				<table class="fx_graph_prop_axis">
					<tr>
						<td><strong>${__('Axes')}&nbsp;&nbsp;</strong></td>
						<td>min</td>
						<td>max</td>
						<td>${__('fréquence')}</td>
						<td>${__('légende')}</td>
						<td></td>
					</tr><tr>
						<td>x</td>
						<td><input type="text" placeholder="auto" class="fx_axis_bound fx_axis_xmin"/></td>
						<td><input type="text" placeholder="auto" class="fx_axis_bound fx_axis_xmax"/></td>
						<td></td>
						<td><input type="text" placeholder="auto" class="fx_axis_label fx_axis_xlabel"/></td>
						<td></td>
					</tr><tr>
						<td>y</td>
						<td><input type="text" placeholder="0" class="fx_axis_bound fx_axis_ymin" value="0" disabled/></td>
						<td><input type="text" placeholder="auto" class="fx_axis_bound fx_axis_ymax"/></td>
						<td><input type="checkbox" class="fx_axis_frequency"/></td>
						<td><input type="text" placeholder="auto" class="fx_axis_label fx_axis_ylabel"/></td>
						<td></td>
					</tr>
				</table>
				<div class="fx_button_div">
					<button class="fx_validate_btn fx_graph_prop_validate">${__('Valider')}</button>
					<button class="fx_graph_prop_cancel">${__('Annuler')}</button>
				</div>
			</div>
			
			<div class='fx_color_picker fx_panel'>
				<i class='fa fa-square' style='color:${fx_colors[0]}' data-color='${fx_colors[0]}'></i>&nbsp;
				<i class='fa fa-square' style='color:${fx_colors[1]}' data-color='${fx_colors[1]}'></i>&nbsp;
				<i class='fa fa-square' style='color:${fx_colors[2]}' data-color='${fx_colors[2]}'></i><br/>
				<i class='fa fa-square' style='color:${fx_colors[3]}' data-color='${fx_colors[3]}'></i>&nbsp;
				<i class='fa fa-square' style='color:${fx_colors[4]}' data-color='${fx_colors[4]}'></i>&nbsp;
				<i class='fa fa-square' style='color:${fx_colors[5]}' data-color='${fx_colors[5]}'></i><br/>
				<i class='fa fa-square' style='color:${fx_colors[6]}' data-color='${fx_colors[6]}'></i>&nbsp;
				<i class='fa fa-square' style='color:${fx_colors[7]}' data-color='${fx_colors[7]}'></i>&nbsp;
				<i class='fa fa-square' style='color:${fx_colors[8]}' data-color='${fx_colors[8]}'></i>
			</div>
			
			<div class="fx_math_formula_help fx_panel" contenteditable="false">
				<div title="${__("Cliquez sur les éléments de ce panneau pour les insérer dans la formule")}"><u>${__("Opérateurs et fonctions")}</u>${__("&nbsp;: ")}
					<span class="fx_formula_help_symbol" title="${__("Plus")}" data-symbol="+">&nbsp;+ </span>
					<span title="${__("Moins")}" data-symbol="-">&nbsp;− </span>
					<span class="fx_formula_help_symbol" title="${__("Multiplier")}" data-symbol="*">&nbsp;* </span>
					<span title="${__("Diviser")}" data-symbol="/">&nbsp;/ </span>
					<span class="fx_formula_help_symbol" title="${__("Puissance de 10 - ex : 2E-2 = 0,02")}" data-symbol="E">&nbsp;E </span>
					<span class="fx_formula_help_symbol" title="${__("Puissance - ex : 3^2 = 9")}" data-symbol="^">&nbsp;^&nbsp;</span>
					<span class="fx_formula_top_right fx_formula_help2_open" title="${__("Afficher une aide supplémentaire")}">
						<i class="fas fa-question-circle"></i>
					</span>
				</div>
				<div>
					<span class="fx_formula_help_symbol" title="${__("Valeur absolue")}" data-symbol="abs()">abs()</span>
					<span class="fx_formula_help_symbol" title="${__("Racine carrée")}" data-symbol="sqrt()">sqrt()  |  rac()</span>
					<span class="fx_formula_help_symbol" title="${__("Exponentielle")}" data-symbol="exp()">exp()</span>
					<span class="fx_formula_help_symbol" title="${__("Logarithme népérien")}" data-symbol="ln()">ln()</span>
					<span class="fx_formula_help_symbol" title="${__("Logarithme en base 10")}" data-symbol="log()">log()</span>
				</div>
				<div>
					<span class="fx_formula_help_symbol" title="${__("Sinus")}" data-symbol="sin()">sin()</span>
					<span class="fx_formula_help_symbol" title="${__("Cosinus")}" data-symbol="cos()">cos()</span>
					<span class="fx_formula_help_symbol" title="${__("Tangeante")}" data-symbol="tan()">tan()</span>
					<span class="fx_formula_help_symbol" title="${__("Arc sinus")}" data-symbol="asin()">asin()</span>
					<span class="fx_formula_help_symbol" title="${__("Arc cosinus")}" data-symbol="acos()">acos()</span>
					<span class="fx_formula_help_symbol" title="${__("Arc tangente")}" data-symbol="atan()">atan()</span>
					<span class="fx_formula_help_symbol" title="${__("Sinus hyperbolique")}" data-symbol="sinh()">sinh()</span>
					<span class="fx_formula_help_symbol" title="${__("Cosinus hyperbolique")}" data-symbol="cosh()">cosh()</span>
					<span class="fx_formula_help_symbol" title="${__("Tangente hyperbolique")}" data-symbol="tanh()">tanh()</span>
				</div>
				<div title="${__("Pour séparer les paramètres, utiliser le point-virgule")}"><u>${__("Fonctions à plusieurs paramètres")}</u>${__("&nbsp;: ")}
					<span class="fx_formula_help_symbol" title="${__("Modulo : reste de la division euclidienne")}" data-symbol="mod(;)">mod(_;_)</span>
					<span class="fx_formula_help_symbol" title="${__("Opérateur conditionnel ternaire : si _ alors _ sinon _")}" data-symbol="?:">_?_:_</span>
				</div>
				<div class="fx_math_formula_help_col" title="${__("Pour créer une liste de valeurs...")}">
					<u>${__("Fonctions utilisant une liste de valeurs (val1 ; val2 ; ...) ou col(X : Y)")}</u>${__("&nbsp;: ")}
					<span class="fx_formula_help_symbol" title="${__("Minimum")}" data-symbol="min()">min()</span>
					<span class="fx_formula_help_symbol" title="${__("Maximum")}" data-symbol="max()">max()</span>
					<span class="fx_formula_help_symbol" title="${__("Moyenne")}" data-symbol="mean()">mean()</span>
					<span class="fx_formula_help_symbol" title="${__("Médiane")}" data-symbol="median()">median()</span>
					<span class="fx_formula_help_symbol" title="${__("Produit")}" data-symbol="prod()">prod()</span>
					<span class="fx_formula_help_symbol" title="${__("Somme")}" data-symbol="sum()">sum()</span>
					<span class="fx_formula_help_symbol" title="${__("Ecart-type s(n-1)")}" data-symbol="std()">std()</span>
					<span class="fx_formula_help_symbol" title="${__("Nombre de valeurs")}" data-symbol="count()">count()</span>
				</div>
				<div class="fx_const_for_formulas" title="${__("Les constantes sont définies dans le menu principal")}">
					<u>${__("Constantes")}</u>${__("&nbsp;: ")}
				</div>
				<div class="fx_math_formula_help_col" title="${__("Utilisez le code 'numrow' ou 'numline' pour faire des incréments à chaque ligne")}">
					<u>${__("Numéro de la ligne")}</u>${__("&nbsp;: ")}
					<span class="fx_formula_help_symbol" data-symbol="numrow">numrow</span>
				</div>
				<div class="fx_math_formula_help_col fx_col_for_formulas" title="${__("Vous pouvez ajouter un nombre positif ou négatif à côté du code de colonne pour réaliser un décalage - ex : col(-1) référence la valeur dans la colonne 'col' située une ligne au-dessus de la ligne courante")}">
					<u>${__("Codes des colonnes col ou col(X)")}</u>${__("&nbsp;: ")}
				</div>
			</div>
			<div class="fx_math_formula_help2 fx_panel">
				<span class="fx_formula_top_right fx_math_formula_help2_close"><i class="fa fa-times" title="${__("Fermer")}"></i></span>
				<div class="fx_math_formula_help2_title">${__("Aide pour l'écriture des formules")}</div>
				<div class="fx_math_formula_help2_content">
					<ul>
						<li>${__("En cliquant sur les différents intitulés d'opérateurs, fonctions, etc., ceux-ci sont insérés dans la formule au niveau de votre curseur.")}</li>
						<li>${__("Vous pouvez aussi utiliser les fonctions JavaScript listées dans")} <a href="https://mathjs.org/docs/reference/functions.html" target="_blank">${__("la page de référence")}</a> (${__("il n'est pas nécessaire de mettre 'Math.' avant une fonction.")})</li>
						<li>${__("Les constantes sont celles définies dans le menu principal du tableau de données.")}</li>
					</ul>
					<p>${__("Uniquement pour les formules permettant de calculer les valeurs d'une colonne du tableau de données :")}</p>
					<ul>
						<li>${__("Le code 'numrow' ou 'numline' vous permet d'utiliser le numéro de la ligne courante dans vos formules (utile par exemple pour faire des incréments).")}</li>
						<li>${__("Quand vous ajoutez un code de colonne dans votre formule, la valeur utilisée pour le calcul est celle contenue dans la colonne au niveau de la ligne courante. Si vous voulez appliquer un décalage de ligne, utilisez un index relatif. Par exemple, pour une référence à la colonne col, la valeur correspondant à la ligne courante est col ou col(0), la valeur pour la ligne juste au-dessus est col(-1) et la valeur deux lignes en-dessous est col(2).")}</br>
						<li>${__("Pour les fonctions utilisant une liste de valeurs, vous pouvez :")}</li>
						<ul>
							<li>${__("indiquer une liste de valeurs séparées par des points-virgules - par exemple : (3.2;5;1E-5)")}</li>
							<li>${__("ou choisir une liste de valeurs sélectionnées dans une colonne. Pour sélectionner les valeurs de la colonne col, utilisez la syntaxe col(X:Y) où X est l'index RELATIF de la première ligne et Y est l'index RELATIF de la dernière ligne ; aucune valeur au niveau de X indique que la liste débute à la première ligne de colonne ; aucune valeur au niveau de Y indique que la liste termine à la dernière valeur non vide de la colonne. Quelques exemples :")}</li>
							<ul>
								<li>${__("col(-1:+1) est la liste de 3 valeurs de la colonne col autour de la ligne courante")}</li>
								<li>${__("col1(:0) est la liste de toutes les valeurs de la colonne col, de la première ligne à la ligne courante")}</li>
								<li>${__("col(0:) est la liste de toutes les valeurs de la colonne col, de la ligne courante à la dernière ligne non vide")}</li>
								<li>${__("col(:) est la liste de toutes les valeurs de la colonne col, de la première ligne à la dernière ligne non vide")}</li>
							</ul>
						</ul>
					</ul>
				</div>
			</div>
			<div class='fx_modal_cover'></div>
		`;
	}
	html_struct += `</div>`;

	// Tooltips content
	const tt_khi2sr_title = __("χ² réduit et écart-type résiduel");
	let tt_khi2sr_content = '<p>' + __("Le χ² réduit = ∑{ ((y<sub>i</sub> - f(x<sub>i</sub>)) / u<sub>i</sub>)² } / (N-p) est calculable si TOUS les points expérimentaux ont une incertitude non nulle sur Y. Ces incertitudes peuvent être définies directement ou par le report des incertitudes des abscisses sur l'axe Y.") + '</p>';
	tt_khi2sr_content += '<p>' + __("Si le χ² réduit n'est pas calculable, c'est l'écart-type résiduel s<sub>r</sub> = √{ ∑{ (y<sub>i</sub> - f(x<sub>i</sub>))² } / (N-p) qui est affiché.") + '</p>';

	const tt_report_ux_title = __("Report des incertitudes des abscisses sur l'axe Y");
	let tt_report_ux_content = '<p>' + __("Pour reporter l'incertitude de l'abscisse d'un point expérimental sur son ordonnée, la valeur de l'incertitude en X est projetée sur l'axe Y selon la tangeante au modèle au niveau de l'abscisse considérée.") + '</p>';
	tt_report_ux_content += '<p>' + __("La composante en Y de l'incertitude de l'abscisse est ensuite combinée à l'incertitude de l'ordonnée.") + '</p>';

	const tt_autofit_title = __("Ajustement automatique");
	let tt_autofit_content = '<p>' + __("Le critère d'ajustement est le χ² réduit s'il est calculable, l'écart-type résiduel sinon.") + '</p>';
	tt_autofit_content += '<p>' + __("Le report des incertitudes des abscisses des points expérimentaux sur l'axe Y n'est pas implémenté dans l'algorithme d'ajustement automatique.") + '</p>';

	const tt_normalize_title = __("Calcul des incertitudes des paramètres du modèle");
	let tt_normalize_content = '<p>' + __("La normalisation des incertitudes de mesure par le χ² réduit a deux conséquences liées :") + '</p>';
	tt_normalize_content += '<ul><li>' + __("cela revient à supposer que le χ² réduit vaut 1,") + '</li>';
	tt_normalize_content += '<li>' + __("les valeurs absolues des incertitudes de mesure n'interviennent plus ; seules comptent les valeurs relatives des incertitudes de mesure les unes par rapport aux autres.") + '</li></ul>';
	tt_normalize_content += '<p>' + __("Lorsque les incertitudes de mesure ne sont pas renseignées, cette normalisation est toujours effectuée.") + '</p>';
	tt_normalize_content += '<p>' + __("La normalisation par le χ² réduit ne modifie pas le χ² réduit affiché.") + '</p>';

	// init the LD view
	$HTMLcontentLD.html(html_struct);
	let $table = $HTMLcontentLD.find(".fx_table");
	displayTable();
	let update_uncertainties = false;
	displayGraphs(-1, update_uncertainties);
	setLDHeight();

	// init the tooltips
	let $settings_panel = $HTMLcontentLD.find(".fx_settings_panel");
	$settings_panel.find(".fx_khi2sr_help i")
		.on("mouseover", function() {global_tooltip_popup.openTooltipPopup(tt_khi2sr_title, tt_khi2sr_content, this)})
		.on("click",     function() {global_tooltip_popup.openTooltipPopup(tt_khi2sr_title, tt_khi2sr_content, this)});
	$settings_panel.find(".fx_report_ux_help i")
		.on("mouseover", function() {global_tooltip_popup.openTooltipPopup(tt_report_ux_title, tt_report_ux_content, this)})
		.on("click",     function() {global_tooltip_popup.openTooltipPopup(tt_report_ux_title, tt_report_ux_content, this)});
	$settings_panel.find(".fx_autofit_help i")
		.on("mouseover", function() {global_tooltip_popup.openTooltipPopup(tt_autofit_title, tt_autofit_content, this)})
		.on("click",     function() {global_tooltip_popup.openTooltipPopup(tt_autofit_title, tt_autofit_content, this)});
	$settings_panel.find(".fx_normalize_help i")
		.on("mouseover", function() {global_tooltip_popup.openTooltipPopup(tt_normalize_title, tt_normalize_content, this)})
		.on("click",     function() {global_tooltip_popup.openTooltipPopup(tt_normalize_title, tt_normalize_content, this)});

	// *****************
	// Display functions
	// *****************

	function setLDHeight() {
		const default_height = 150;
		const tolerance = 30; // !edition => show all table if few is hidden
		let $resizable_div = $HTMLcontentLD.find(".fx_table_wrapper");
		let height = $XMLcodeLD.find('dataset').attr('display_height');
		if (!height) {
			height = default_height;
		}
		if (!edition) {
			// some height is gained when not in edition for some rows: header, formula & default rows (20=>18)
			// but stat indicators rows keep the same height (18)
			const edition_row_height = 20;
			const row_height = 18;
			let si_rows_height = $table.find(".fx_si_row").length * row_height;
			let coef = edition_row_height / row_height;
			height = (height - si_rows_height) / coef + si_rows_height;
			let table_height = $resizable_div.height();
			if (height > table_height - tolerance) {
				height = table_height;
			}
		}
		$resizable_div.css('height', height + 'px');
	}

	//******************************************************************************************************************
	//                                          TABLE
	//******************************************************************************************************************

	function displayTable() {
		displayTableHeader();
		displayTableFormulas(); // affichage de la ligne de formules
		displayTableStatIndicators(); //affichage des lignes d'indicateurs stats
		displayTableRows(); // display cells and buttons
	}

	
	//affichage de la ligne des titres
	function displayTableHeader() {
		let $col_headers = $table.find(".fx_header");
		// main menu
		let html_headers;
		if (edition) {
			html_headers = "<th class='fx_main_menu fx_button'><i class='fa fa-bars'></i></th>";
		} else {
			let constants = $XMLcodeLD.find('constant');
			let display_constants = $(constants).length > 1 || $(constants).find('name').first().text() !== "pi";
			html_headers = `<th class='fx_main_menu'>${display_constants ? "C" : ""}</th>`;
		}
		// columns
		let icol = 0;
		$XMLcodeLD.find('column').each(function () {
			let name = $(this).find('name').text();
			let code = $(this).find('code').text();
			let col_type = $(this).attr('type');
			let unit = $(this).find('unit').text();
			let formula = $(this).find('formula').text();
			if (code === "") { code = "data" + icol; } // legacy
			html_headers += `
			<th title="${ name !== code ? name + "\n" : ""}${ col_type === "num" ? __("données numériques") : __("données texte") }${ formula ? __(" calculées") : ""}" 
				class="fx_col_header ${ edition ? ' fx_col_menu fx_button' : ""} ${col_type === "txt" ? ' fx_col_header_txt' : ''}"
				data-col="${icol}">
				<div class="fx_header_name"> ${code} ${ unit ? ' ['+unit+']' : ""}</div>
			</th>
			`;
			icol++;
		});
		// button add column
		if (edition) {
			html_headers += `<th class="fx_add_col fx_button" title="${__('Ajouter une colonne')}"><i class="fa fa-plus"></i></th>`;
		}
		$col_headers.html(html_headers);
	}


	function displayTableFormulas() {
		let html_formulas = "<td class='fx_row_title'>ƒ( )</td>";
		let icol = 0;
		$XMLcodeLD.find('column').each(function () {
			let td_class = "fx_cell";
			let inner_html = "";
			let title = "";
			if ($(this).attr('type') === "num") {
				let $formula = $(this).children('formula');
				let formula = $formula.text();
				let error = $formula.attr('error');
				let div_classes = '';
				if (error) {
					title = fx_errors[error] + __(" dans") + __("&nbsp;: ") + formula;
					div_classes = 'fx_error';
				} else {
					title = formula;
				}
				if (edition) {
					div_classes += ' fx_editable_formula_cell';
					if (formula === "") {
						title = __("Définissez une formule pour calculer les données d'une colonne");
					}
				}
				inner_html += `<div class="fx_formula_cell ${div_classes}" spellcheck="false">${formula ? formula : '&nbsp;'}</div></td>`;
			} else { // text column
				td_class += ' fx_no_formula';
			}
			html_formulas += `<td data-col="${icol}" class="${td_class}" title="${title}">${inner_html}</td>`;
			icol++;
		});

		// displays
		$table.find('.fx_formula_row').html(html_formulas);
		if ($XMLcodeLD.find('dataset').attr('display_formula') === "true") {
			$HTMLcontentLD.find(".fx_formula_row").show();
		}
	}


	function displayTableStatIndicators() {
		let html_si = "";
		let show_stat_indicator = [];
		let columns_formats = globalLD.model.getColumnsFormats();
		$XMLcodeLD.find('stat').each(function() {
			show_stat_indicator.push($(this).attr("property"));
		});
		stat_ind.forEach((val, key)=>{ // use the Map in order to keep the same order
			if (show_stat_indicator.includes(key)) {
				html_si += `<tr class="fx_si_row"><td class="fx_row_title" title="${val.title}">${val.code}</td>${buildHtmlSICells(key)}</tr>`
			}
		});
		// displays
		$HTMLcontentLD.find(".fx_si_row").remove();
		$HTMLcontentLD.find(".fx_formula_row").after(html_si);

		function buildHtmlSICells(si) {
			let html_cells = "" ;
			let icol = 0;
			$XMLcodeLD.find('stat[property="' + si + '"]').find('value').each(function () {
				let num_txt = $(this).text();
				// Format number
				if (si !== "n" && si !== "std_dev" && si !== "c_var") {
					num_txt = formatFXNumber(num_txt, columns_formats[icol].scientific, columns_formats[icol].digits);
				} else if (si === "std_dev" || si === "c_var") {
					num_txt = formatFXNumber(num_txt, null, 6);
				}
				html_cells += `<td data-col="${icol}" class="fx_cell"><div class="fx_si_cell_val">${num_txt}</div></td>`;
				icol++ ;
			});
			return html_cells ;
		}
	}


	function displayTableRows() {
		let irow = 0;
		let html_rows = "";
		let columns_formats = globalLD.model.getColumnsFormats();
		let col_span = columns_formats.length+1;
		let $rows = $XMLcodeLD.find('row');
		let nb_rows = $rows.length;
		let max_rows = globalLD.max_display_rows; // null if all rows must be displayed
		let nb_diplayed_rows = max_rows ? Math.min(max_rows, nb_rows) : nb_rows;
		// display lines
		$rows.each(function () {
			while (irow < nb_diplayed_rows) { // do not display html_rows above max_display_rows
				html_rows += `<tr class="fx_default_row" data-row="${irow}">${buildHtmlRowCells(irow, columns_formats)}</tr>`;
				irow++;
			}
		});
		if (max_rows !== null && nb_rows > max_rows) {
			html_rows += `<tr class="fx_show_rows"><td colspan="${col_span}" class="fx_button">${ __('Afficher toutes les lignes')}</td></tr>`;
		} else if (edition) {
			let nb_added_rows = parseInt($table.find(".fx_num_rows_added").val());
			if (!nb_added_rows || isNaN(nb_added_rows)) {
				nb_added_rows = 5;
			}
			html_rows += `
				<tr class="fx_add_row"><td title="${__('Ajouter des lignes')}" colspan="${col_span}" class="fx_button">
				<i class="fa fa-plus"></i> <input type="text" class="fx_num_rows_added" inputmode="decimal" value="${nb_added_rows}"> ${__('lignes')}</td></tr>`;
		}
		$table.find(".fx_default_row").remove();
		$table.find(".fx_add_row").remove();
		$table.find(".fx_show_rows").remove();
		$table.find("tbody").append(html_rows);

		/**
		 *
		 * @param irow
		 * @param {array} columns_formats
		 * @returns {string}
		 */
		function buildHtmlRowCells(irow, columns_formats) {
			let num_row = parseInt(irow)+1; // le numéro de ligne qui s'affiche
			let html_content = `<td class="fx_row_title ${edition ? 'fx_row_menu fx_button' : ''}">${num_row}</td>`;
			let icol = 0 ;
			$XMLcodeLD.find('row').eq(irow).find('value').each(function () {
				// Format number
				let val = $(this).text();
				let ignored_val = $(this).attr("ignored") === "true";
				let isText = true;
				if (columns_formats[icol].type === 'num') {
					isText = false;
					val = formatFXNumber(val, columns_formats[icol].scientific, columns_formats[icol].digits);
				}

				let formula = columns_formats[icol].formula;
				let html_td_title = ""; // long titles are not displayed in edition for performance reasons
				if (!edition) {
					if (isText) {
						html_td_title += __("Donnée texte") + "\n" + val;
					} else {
						html_td_title += __("Donnée numérique ");
						html_td_title += ignored_val ? __("ignorée") : "";
						html_td_title += formula ? "\n" + __("définie par la formule") + __("&nbsp;:") + " " + formula : "";
					}
				} else if (ignored_val) {
					html_td_title += __("Donnée ignorée");
				}

				let html_input = "", html_editable = "", html_decimal = "";
				let html_td_classes = "fx_cell";
				let html_div_classes = "fx_cell_val";
				if (edition && !formula) { // cellule modifiable
					if (!isText) {  // gestion de la checkbox
						let html_input_title = __("Cette case indique si la donnée doit être utilisée ou non dans les calculs");
						html_input = `<input class="fx_ignored_cb" title="${html_input_title}" type="checkbox" ${ignored_val ? '' : ' checked'}>`;
						html_decimal = 'inputmode="decimal"';
					}
					html_div_classes += ' fx_editable_cell'
					html_editable = 'tabindex="0"';
				}
				if (formula) {
					html_td_classes += ' fx_calc';
				}
				if (ignored_val) {
					html_td_classes += ' fx_ignored';
				}
				if (isText) {
					html_td_classes += ' fx_text';
				}
				if (val === "") {
					val= "&nbsp;"; // in order to make the DIV fill the TD in height
				}
				html_content += `<td data-col="${icol}" title="${html_td_title}" class="${html_td_classes}">${html_input}`;
				html_content += `<div class="${html_div_classes}" ${html_editable} ${html_decimal}>${val}</div></td>`;
				icol++ ;
			});
			return html_content;
		}

	}


	function displayImportPanel() {
		let $import_panel = $HTMLcontentLD.find(".fx_import_panel");
		let $main_menu = $HTMLcontentLD.find(".fx_main_menu");
		// init content
		$import_panel.find('.fx_import_alerts').html("");
		// display : position the menu below the button
		let position = $main_menu.position();
		$import_panel.css('top', position.top + $main_menu.height());
		$import_panel.css('left', position.left);
		toggleFXModal(true, ".fx_import_panel") ;
		$import_panel.show() ;
	}


	function displayExportPanel() {
		let $export_panel = $HTMLcontentLD.find(".fx_export_panel");
		let $main_menu = $HTMLcontentLD.find(".fx_main_menu");
		if (edition) {
			toggleFXModal(true, ".fx_export_panel");
		}
		//display : positioning the panel below the main_menu
		let position = $main_menu.position();
		$export_panel.css('top', position.top + $main_menu.outerHeight());
		$export_panel.css('left', position.left + $main_menu.outerWidth());
		$export_panel.show();
		$export_panel.focus();
	}


	function displayConstantPanel() {
		let $c_panel = $HTMLcontentLD.find(".fx_constant_panel");
		let $main_menu = $HTMLcontentLD.find(".fx_main_menu");
		// init content
		let html = "";
		if (edition) { // Add a button
			html = `<tr><td class="fx_constant_panel_add fx_button" colspan="2" title="${__('Ajouter une constante')}"><i class="fa fa-plus"></i></td></tr>`;
			toggleFXModal(true, ".fx_constant_panel");
		}
		$c_panel.find('tbody').html(html);
		$XMLcodeLD.find('constant').each(function () {
			addConstantRowInPanel($(this).find('name').text(), $(this).find('value').text());
		});
		//display : positioning the panel below the main_menu
		let position = $main_menu.position();
		$c_panel.css('top', position.top + $main_menu.outerHeight());
		$c_panel.css('left', position.left + $main_menu.outerWidth());
		$c_panel.show();
		$c_panel.focus();
	}

	function addConstantRowInPanel(const_code = "", value = "") {
		let $c_panel = $HTMLcontentLD.find(".fx_constant_panel");
		let html_row = `
			<tr class="fx_constant">
				<td ${(edition && !const_code) ? 'class="fx_constant_editable"' : ''}>
					<div class="fx_constant_code" ${(edition && !const_code) ? 'contenteditable' : ''}>${const_code}</div>
				</td>
				<td ${edition ? 'class="fx_constant_editable"' : ''}><div ${edition ? 'contenteditable="true" inputmode="decimal"' : ''}>${value}</div></td>
				${edition ? `<td class="fx_constant_panel_del" title="${__('Supprimer la constante')}"><i class="far fa-trash-alt"></i></td>` : ''}
			</tr>`;
		if (edition) {
			$c_panel.find('.fx_constant_panel_add').closest('tr').before(html_row);
		} else {
			$c_panel.find('tbody').append(html_row);
		}
	}


	function displayFitSettings() {
		let $settings_panel = $HTMLcontentLD.find(".fx_settings_panel");
		let $main_menu = $HTMLcontentLD.find(".fx_main_menu");
		// init content
		if (edition) {
			toggleFXModal(true, ".fx_settings_panel");
		}
		$settings_panel.find('.fx_setting_khi2_reduced')[0].checked = parseInt($settings.find('reducedKhi2').text()) === 1;
		$settings_panel.find('.fx_setting_khi2_ux_y')[0].checked = parseInt($settings.find('uxShiftedToy').text()) === 1;
		if (globalLD.auto_fit) {
			$settings_panel.find('.fx_setting_auto_adjust').show();
			$settings_panel.find('.fx_setting_normalize')[0].checked = parseInt($settings.find('upNormalized').text()) === 1;
		} else {
			$settings_panel.find('.fx_setting_auto_adjust').hide();
		}
		//display : positioning the panel below the main_menu
		let position = $main_menu.position();
		$settings_panel.css('top', position.top + $main_menu.outerHeight());
		$settings_panel.css('left', position.left + $main_menu.outerWidth());
		$settings_panel.show();
		$settings_panel.focus();
	}


	/**
	 * @param {int} icol
	 * @param {boolean} cancel_add_col indicates if the columns must be deleted if the panel is canceled
	 * @param {string} focus dom selector on which to apply focus when the panel is opened
	 */
	function displayColPropPanel(icol, cancel_add_col=false, focus="") {
		let $col = $table.find('th[data-col="'+icol+'"]');
		let $panel = $HTMLcontentLD.find(".fx_col_prop_panel");
		$panel.data('icol', icol).data('cancel_add_col', cancel_add_col);
		// init content
		let $col_model = $XMLcodeLD.find('column').eq(icol);
		let nb_digits = typeof $col_model.attr('digits') === 'undefined' ? "" : $col_model.attr('digits');
		$panel.find(".fx_col_name").val($col_model.find('name').text());
		let code = $col_model.find('code').text();
		$panel.find(".fx_col_code").val(code);
		$panel.find(".fx_col_unit").val(unEscapeXML($col_model.find('unit').text()));
		let $formula = $panel.find(".fx_col_formula");
		$formula.val($col_model.find('formula').text());
		$formula.data('col_code', code);
		if ($col_model.attr('type') === "txt") {
			$panel.find(".fx_col_type_txt").click();
		} else {
			$panel.find(".fx_col_type_num").click();
		}
		if ($col_model.attr('scientific_notation') === "true") {
			$panel.find(".fx_sc_notation").prop('checked', true);
			$panel.find(".fx_nb_decimals").hide();
			$panel.find(".fx_nb_significant_digits").show();
		} else {
			$panel.find(".fx_sc_notation").prop('checked', false);
			$panel.find(".fx_nb_significant_digits").hide();
			$panel.find(".fx_nb_decimals").show();
		}
		$panel.find(".fx_nb_digits").find("input").val(nb_digits);
		toggleFXModal(true, ".fx_col_prop_panel");
		let col_pos = $col.position();
		$panel.css('top', col_pos.top + $col.outerHeight());
		if (col_pos.left + $panel.outerWidth() > $HTMLcontentLD.outerWidth()) {
			$panel.css('left', col_pos.left + $col.outerWidth() - $panel.outerWidth());
		} else {
			$panel.css('left', col_pos.left - 1);
		}
		$panel.show();
		if (focus) {
			$panel.find(focus).focus();
		} else if ($panel.find(".fx_col_name").val().substring(0, 4) === "data" ) {
			$panel.find(".fx_col_name").select();
		}
	}

	//******************************************************************************************************************
	//                                          GRAPHS
	//******************************************************************************************************************

	/**
	 * @param {int} sel_graph draw a specific graph id >= 0 ; draw all graphs if null or -1
	 * @param {boolean} update_uncertainties whether to update or not uncertainties
	 */
	function displayGraphs(sel_graph = -1, update_uncertainties=true) {
		let graphs;
		let $html_graphs = $HTMLcontentLD.find(".fx_graph");
		if (sel_graph === -1) {
			graphs = $XMLcodeLD.find('graph');
			$html_graphs.remove();
		} else {
			graphs = $XMLcodeLD.find('graph').eq(sel_graph);
		}
		graphs.each( function(igraph) {
			if (sel_graph !== -1) {
				igraph = sel_graph;
			}
			// mise en place de la structure générale
			let html_struct = `
				<div class="fx_graph" data-igraph="${igraph}">
					<div class="fx_graph_title">
						${edition ? '<div class="fx_btn_graph_prop"><i class="fa fa-cog" title="' + __('Afficher les paramètres du graphique') + '"></i></div>' : ""}
						<span class="fx_graph_title_txt">${$XMLcodeLD.find('graph').eq(igraph).children('name').text()}</span>
						${edition ? '<div class="fx_btn_graph_delete"><i class="far fa-trash-alt" title="' + __('Supprimer le graphique') + '"></i></div>' : ""}
						${edition && igraph ? `<div class="fx_btn_graph_up" title="${__('Déplacer vers le haut')}"><i class="fa-solid fa-arrow-up"></i></div>` : "" }
					</div>
					<div class ="fx_graph_flot"/>
					<div class="fx_graph_pfuncs"></div>
				</div>`;
			if (edition) {
				if (sel_graph !== -1 && igraph < $html_graphs.length) { // MAJ
					$html_graphs.eq(igraph).replaceWith(html_struct);
				} else { // update all
					$HTMLcontentLD.find(".fx_end_graph").before(html_struct);
				}
			} else {
				$HTMLcontentLD.append(html_struct);
			}
			displayGraphPFuncs(igraph, update_uncertainties);
			displayGraphFlot(igraph);
		});
	}

	/**
	 * @param {int} sel_graph graph to display; -1 means all
	 */
	function displayGraphFlot(sel_graph = -1) {

		function parseBound(bound) {
			bound = parseFloat(bound);
			return isNaN(bound) ? null : bound;
		}

		/**
		 * In case of log y scale, it is necessary to calculate the bounds of the axis
		 * @param {Number} xmin
		 * @param {Number} xmax
		 * @param {Number} ymin
		 * @param {Number} ymax
		 * @param {Boolean} y2
		 * @param {Number} igraph
		 * @return {{min: Number, max: Number}}
		 */
		function calculateYminYmax(xmin, xmax, ymin, ymax, y2, igraph) {
			let jsGraph = globalLD.graphs[igraph];
			ymin = ymin <= 0 ? null : ymin;
			ymax = ymax <= 0 ? null : ymax;
			let update_ymin = ymin === null;
			let update_ymax = ymax === null;
			if (ymin === null || ymin <= 0 || ymax === null || ymax <= 0) {
				function updateYminYmax(dataset) {
					dataset.forEach(point => {
						if (point[0] >= xmin && point[0] <= xmax && point[1] > 0) {
							if (update_ymin && (ymin === null || point[1] < ymin) && (update_ymax || point[1] <= ymax)) {
								ymin = point[1];
							}
							if (update_ymax && (ymax === null || point[1] > ymax) && (update_ymin || point[1] >= ymin)) {
								ymax = point[1];
							}
						}
					});
				}
				jsGraph.plots.forEach(function(plot, iplot) {
					let axis = $XMLcodeLD.find("graph").eq(igraph).find("plot").eq(iplot).attr("axis");
					if ((y2 && axis === "y2") || (!y2 && axis !== "y2")) {
						updateYminYmax(plot);
					}
				});
				jsGraph.pfuncs.forEach(pfunc => {
					updateYminYmax(pfunc.data);
				});
			}
			// manage min and max
			return manageFXAxisBounds(ymin, ymax, update_ymin, update_ymax, true);
		}

		/**
		 * build an array of nice ticks for a log axis
		 * This function is inspired from the plugin : https://gist.github.com/mojoaxel/f966cd18e60491bb8f363f45eaba24c2
		 * @param {Number} min min > 0
		 * @param {Number} max max > 0 & max >= min
		 * @return {Number[] | null}
		 */
		function setTicksForLogScale(min, max) {
			let ticks = null; // use the default Flot formating algorithm
			if (max / min >= 100) { // Redefine the ticks
				ticks = [];
				let tick = null;
				let start = Math.floor(Math.log10(min)),
					end = Math.ceil(Math.log10(max));
				let range = end - start; // >= 2
				let step = Math.ceil((range)/10); // >= 1
				for (let i = 0 ; i <= range; i += step) { // Make ticks at each power of ten
					tick = Math.pow(10, start + i);
					ticks.push(tick);
				}
				let length = ticks.length;
				if (range < 6) { // If not to many ticks also put a tick between the powers of ten
					for (let j = 1; j < length * 2 - 1; j += 2) {
						tick = ticks[j - 1] * 5;
						ticks.splice(j, 0, tick);
					}
				}
			}
			return ticks;
		}

		/**
		 * test if the values of an array are always the same
		 * @param {Array} arr
		 * @return {Boolean} true if the parameter is a non-empty array of constant value
		 */
		function isConstantArray(arr) {
			if (!arr || !(arr instanceof Array)) {
				return false;
			}
			return arr.every(function(v) {
				return v === arr[0];
			});
		}

		function buildLabel(names, units, user_label, yHistogram=false, frequency=true) {
			let label = "";
			if (isConstantArray(names)) {
				label = (names[0] !== "" && names[0] !== undefined) ? names[0] : "";
			}
			if (isConstantArray(units)) {
				label += (units[0] !== "" && units[0] !== undefined) ? " ["+units[0]+"]" : "";
			}
			if (yHistogram) {
				label = frequency ? __("fréquence") : __("nombre de valeurs");
			}
			label = (user_label.length && user_label.text() !== "") ? user_label.text() : label; // overwrite default label if defined
			return label;
		}

		let igraph = 0;
		$XMLcodeLD.find("graph").each(function() {
			if (sel_graph === -1 || sel_graph === igraph) {

				let has_y2 = $(this).attr('y2') === "true";

				let graphType = $(this).attr('type') || 'X-Y';

				// Get the axes bounds and scales
				let jsGraph = globalLD.graphs[igraph];
				let xmin = jsGraph.xmin; // can be positive or negative
				let xmax = jsGraph.xmax; // can be positive or negative
				let xscale = $(this).find('xscale').text();
				let y1min = parseBound($(this).find('ymin').text());
				let y1max = parseBound($(this).find('ymax').text());
				let y1scale = $(this).find('yscale').text();
				let y2min = parseBound($(this).find('y2min').text());
				let y2max = parseBound($(this).find('y2max').text());
				let y2scale = $(this).find('y2scale').text();
				// histogram specific
				let frequency = $(this).attr('frequency') === 'true';
				let bins_number = parseBound($(this).find('bins_number').text());
				let minClassesValue;
				let maxClassesValue;
				let xyBins = [];
				let binsWidth;

				// Management of log axes
				let xticks = null, y1ticks = null, y2ticks = null;
				if (xscale === 'log') {
					xticks = setTicksForLogScale(xmin, xmax);
				}
				if (y1scale === 'log') {
					let bounds = calculateYminYmax(xmin, xmax, y1min, y1max, false, igraph);
					y1min = bounds.min;
					y1max = bounds.max;
					y1ticks = setTicksForLogScale(y1min, y1max);
				}
				if (has_y2 && y2scale === 'log') {
					let bounds = calculateYminYmax(xmin, xmax, y2min, y2max, true, igraph);
					y2min = bounds.min;
					y2max = bounds.max;
					y2ticks = setTicksForLogScale(y2min, y2max);
				}

				// Axis labels & graph legend
				let x_names = [], x_units = [], y1_names = [], y1_units = [], y2_names = [], y2_units = [], plots_legend = [];
				let $cols = $XMLcodeLD.find('column');
				let y2, x_code, y_code, x_legend, y_legend, unit, name, $col, iplot = 0;
				$(this).find('plot').each( function() { // keep track of the names and units and build the graph legend
					y2 = $(this).attr('axis') === "y2";
					x_code = $(this).children('x').text();
					y_code = $(this).children('y').text();
					if (x_code && (graphType === 'histogram' || y_code)) {
						$col = $cols.filter(function() {
							return ($(this).find('code').text() === x_code);
						});
						unit = unEscapeXML($col.children("unit").text());
						name = unEscapeXML($col.children("name").text());
						x_names.push(name);
						x_units.push(unit);
						x_legend = name + (unit ? " [" + unit + "]" : "");
						$col = $cols.filter(function() {
							return ($(this).find('code').text() === y_code);
						});
						unit = unEscapeXML($col.children("unit").text());
						name = unEscapeXML($col.children("name").text());
						if (y2) {
							y2_names.push(name);
							y2_units.push(unit);
							y_legend = name + (unit ? " [" + unit + "]" : "");
							plots_legend.push(y_legend + " = f(" + x_legend + ")");
						} else {
							y1_names.push(name);
							y1_units.push(unit);
							y_legend = name + (unit ? " [" + unit + "]" : "");
							plots_legend.push(y_legend + ((graphType === 'histogram') ? (" = " + x_legend) : (" = f(" + x_legend + ")")));
						}
						iplot++;
					}
				});
				// build the labels
				let x_label = buildLabel(x_names, x_units, $(this).find('xlabel'));
				let y1_label = buildLabel(y1_names, y1_units, $(this).find('ylabel'), graphType === 'histogram', frequency);
				let y2_label = "";
				if (has_y2) {
					y2_label = buildLabel(y2_names, y2_units, $(this).find('y2label'));
				}
				let show_legend = iplot > 0;
				let legend_position = 'ne';
				if (jsGraph.plots !== undefined) { // optimize the legend position
					// array of x values for each y max
					let x_for_y_max = jsGraph.plots.filter(plt => plt.length > 0).map(plt => plt.reduce((car, cur) => cur[1] > car[1] ? cur : car, plt[0])[0]);
					// log or identity function
					let logOrId = (() => xscale === 'log' ? Math.log10 : a => a)();
					let legend_position_score = x_for_y_max
						// if x is less than a third, 1 point to the northEast.
						// if x is more than two thirds, 1 point to the northWest.
						.map(function(x){
							if (logOrId(x) < logOrId(xmin) + 1/3 * (logOrId(xmax) - logOrId(xmin))){
								return [0, 1];
							} else if (logOrId(x) > logOrId(xmin) + 2/3 * (logOrId(xmax) - logOrId(xmin))){
								return [1, 0];
							} else {
								return [0, 0];
							}
						})
						// sum all points
						.reduce((car, cur) => car.map((elt, idx) => elt + cur[idx]), [0, 0]);
					// the greatest wins
					if (legend_position_score[0] > legend_position_score[1]) {
						legend_position = 'nw';
					}
					// histogram specific: compute bins_number and classes
					if (graphType === 'histogram') {
						({ xyBins, bins_number, minClassesValue, maxClassesValue, binsWidth } = globalLD.model.initializeBinsData(jsGraph.plots, bins_number));
					}
				}

				// axes configuration
				let axes_general_options = {
					show: true,
					font: { size: 10, color: "#333" },
					axisLabelUseCanvas: true,
					axisLabelFontSizePixels: 12,
					axisLabelColour: "black",
					tickFormatter: function(v) { return formatFXNumber(v); }
				};
				let yaxes = [Object.assign({
					position: "left",
					min: y1min,
					max: y1max,
					ticks: y1ticks,
					axisLabel: y1_label,
					axisLabelPadding: 10,
					transform: (y1scale === 'log' ? function(v) { return v > 0 ? math.log10(v) : null } : null),
					inverseTransform: (y1scale === 'log' ? function(v) { return Math.pow(10, v) } : null)
				}, axes_general_options)];
				if (has_y2) {
					yaxes.push(Object.assign({
						position: "right",
						min: y2min,
						max: y2max,
						ticks: y2ticks,
						axisLabel: y2_label,
						axisLabelPadding: 10,
						transform: (y2scale === 'log' ? function(v) { return v > 0 ? math.log10(v) : null } : null),
						inverseTransform: (y2scale === 'log' ? function(v) { return Math.pow(10, v) } : null)
					}, axes_general_options));
				}
				let xaxis = Object.assign({
					min: xmin,
					max: xmax,
					ticks: xticks,
					axisLabel: x_label,
					axisLabelPadding: 15,
					transform: (xscale === 'log' ? function(v) { return v > 0 ? math.log10(v) : null } : null),
					inverseTransform: (xscale === 'log' ? function(v) { return Math.pow(10, v) } : null)
				}, axes_general_options);

				// Flot general options
				let display_options = {
					axisLabels: { show: true }, // plugin jquery.flot.axisLabels.js
					xaxis: xaxis,
					yaxes: yaxes,
					grid: {
						hoverable: true,
						borderWidth: 1,
						backgroundColor: "#fff"
					},
					shadowSize: 0,
					points: {
						show: true
					},
					tooltip: {
						show: true,
						content: function(label, xval, yval, flotItem){
							if (!flotItem.series.bars.show) {
								return "x = " + formatFXNumber(xval) + " | y = " + formatFXNumber(yval);
							} else {
								return __('classe') + " = [" + formatFXNumber(xval - binsWidth / 2, null, 3) + ", " 
									+ formatFXNumber(xval + binsWidth / 2, null, 3) +"] | y = " 
									+ (frequency ? (formatFXNumber(yval * 100, null, 4) + " %") : formatFXNumber(yval));
							}
						}
					},
					legend: {
						show: show_legend,
						backgroundOpacity: 0.5,
						position: legend_position
					}
				};

				// These options for plugins seem to be unused !
				if (typeof global_scope !== 'undefined' && global_scope === 'pdf') {
					display_options.canvas = true; // include axis or legend in canvas - plugin jquery.flot.canvas.js
					display_options.legend.legendInCanvas = true; // plugin jquery.flot.legendoncanvas.js
				} else {
					display_options.canvas = false;
					display_options.legend.legendInCanvas = false;
				}

				// plots
				let plots = []; // plots and functions of the canva
				let axis, color, show_segments, show_smoothed, show_u, point_size;
				iplot = 0;
				$(this).find('plot').each( function() {
					y2 = $(this).attr('axis') === "y2";
					color = $(this).attr('color') ? $(this).attr('color') : "#000"; // legacy
					show_segments = $(this).attr('curve') === 'segments';
					show_smoothed = $(this).attr('curve') === 'smoothed';
					show_u = $(this).find('ux').text() !== '' || $(this).find('uy').text() !== '';
					point_size = (show_segments || show_smoothed || show_u) ? 1 : 1.7;
					axis = y2 ? 2 : 1;
					let plot = { yaxis: axis, color: color, label : show_legend ? plots_legend[iplot] : null }
					if (graphType === 'X-Y') {
						plot.data = jsGraph.plots[iplot];
						plot.lines = { show: show_segments, lineWidth: 1 };
						plot.curvedLines = { apply: false };
						plot.points = { 
							radius: point_size, lineWidth: 1.3, errorbars : "xy",
							xerr: { show: true, asymmetric: false, upperCap: "-", lowerCap: "-", radius: 3, lineWidth: 1, color: "#888" },
							yerr: { show: true, asymmetric: false, upperCap: "-", lowerCap: "-", radius: 3, lineWidth: 1, color: "#888" }
						};
					} else if (graphType === 'histogram') {
						xyBins = xyBins.map(couple => [couple[0], 0]);
						let series = jsGraph.plots[iplot].map(quadruplet => quadruplet[0]).sort((a, b) => a > b);
						xyBins = globalLD.model.fillBinsValues(xyBins, series, bins_number, minClassesValue, maxClassesValue, frequency);
						plot.data = xyBins;
						plot.bars = { barWidth : binsWidth, show: true, align: "center", horizontal: false, lineWidth: 1 };
						plot.points = { show: false };
					}
					plots.push(plot);
					iplot++;
				});
				iplot = 0; // Add the smoothed curved after the regular points for legend format reasons
				let smoothed = false;
				$(this).find('plot').each( function() {
					show_smoothed = $(this).attr('curve') === 'smoothed';
					if (show_smoothed) {
						smoothed = true;
						color = $(this).attr('color') ? $(this).attr('color') : "#000"; // legacy
						plots.push( {
							data: jsGraph.plots[iplot],
							color: color,
							points: { show: false },
							lines: {
								show: true,
								lineWidth: 1
							},
							curvedLines: {
								apply: true
							}
						});
					}
					iplot++;
				});
				iplot = 0; // Add the outliers points after the regular points for legend format reasons
				$(this).find('plot').each( function() {
					color = $(this).attr('color') ? $(this).attr('color') : "#000"; // legacy
					plots.push( {
						data : globalLD.graphs[igraph].outliers_plots[iplot],
						color : color,
						points : {
							symbol : "cross",
							radius : 2.6,
							lineWidth: 1,
							errorbars : "xy",
							xerr: { show: true, asymmetric: false, upperCap: "-", lowerCap: "-", radius: 3, lineWidth: 1, color: "#888" },
							yerr: { show: true, asymmetric: false, upperCap: "-", lowerCap: "-", radius: 3, lineWidth: 1, color: "#888" }
						},
						curvedLines: {
							apply: false
						}
					});
					iplot++;
				});
				if (smoothed) {
					display_options.series = {
						curvedLines: {
							apply: true,
							active: true,
							monotonicFit: true
						}
					};
				}

				// functions
				let ifunc = 0;
				$(this).find('function').each( function() {
					let iplot = $(this).attr('fit_iplot');
					y2 = $XMLcodeLD.find('plot').eq(iplot).attr('axis') === "y2";
					axis = y2 ? 2 : 1;
					color = $(this).attr('color') ? $(this).attr('color') : "#000";
					plots.push( {
						yaxis: axis,
						data : globalLD.graphs[igraph].pfuncs[ifunc].data,
						color : color,
						points: { show : false },
						lines : { show: true, lineWidth: 1 }
					});
					ifunc++;
				});

				// Display the graph
				let graph;
				let $fx_graph_flot = $HTMLcontentLD.find(".fx_graph").eq(igraph).find(".fx_graph_flot");
				try {
					graph = $.plot($fx_graph_flot, plots, display_options);
				} catch(e) {
					logError("Fitex plot failed", "error", {
						exception: e.message,
						fileName: e.fileName,
						lineNumber: e.lineNumber,
						id_ld: id_ld,
						igraph: igraph,
						display_options: display_options
					});
				}
				if (typeof global_scope !== 'undefined' && global_scope === 'pdf'){
					let canvas = graph.getCanvas();
					let img = canvas.toDataURL("image/png"); //.replace("image/png","image/octet-stream")
					//window.open(image, "others");
					$fx_graph_flot.html('<img width="100%" class="canvas_img" src="" alt="' + __("Erreur : l'image du graphique n'a pas pu être générée") + '"/>');
					$fx_graph_flot.find(".canvas_img").attr("src", img);
				}
			}
			igraph++;
		});
	}

	/**
	 * @param {int} sel_graph
	 * @param {boolean} update_uncertainties whether to update or not uncertainties
	 */
	function displayGraphPFuncs(sel_graph = -1, update_uncertainties) {
		let igraph = 0;
		$XMLcodeLD.find("graph").each(function() {
			if (sel_graph === -1 || sel_graph === igraph) {
				let ifunc = 0;
				let func_descr, color, xstart, xend, html_funcs = "";
				let $XMLgraph = $XMLcodeLD.find('graph').eq(igraph);
				// histogram specific
				let graphType = $XMLgraph.attr("type") || 'X-Y';
				let fx_pfunc_div_menu = 'fx_pfunc_div_menu';
				let textareaDisabled = '';
				let textareaPlaceholder = __("Ajouter une fonction paramétrée : directement dans ce cadre ou avec le menu f(x)");
				if (graphType === 'histogram') {
					fx_pfunc_div_menu = 'fx_pfunc_div_menu_histogram';
					textareaDisabled = 'disabled';
					textareaPlaceholder = __("Ajouter une fonction paramétrée avec le menu f(x)");
				}
				let func_name = "f(x)";
				$(this).find('function').each(function () {

					let $XMLplots = $XMLgraph.find('plot');
					// function
					color = $(this).attr('color') ? $(this).attr('color') : "#000"; // legacy
					func_descr = $(this).find('description').text(); // the description (if defined) must be displayed in place of the formula

					// parameters
					let html_params = "", html_dist = "", html_interval = "", html_plot_picker = "";
					if (globalLD.auto_fit) {
						let fit_btn_title = __("Ajuster automatiquement les paramètres libres\n(cliquer sur le nom d'un paramètre pour le fixer ou le libérer)");
						if ($XMLplots.get().length >= 1 && graphType === 'X-Y') {
							html_params += `<div class="fx_pfunc_div_auto_fit"><i class="fas fa-sliders" title="${fit_btn_title}"></i></div>`;
						}
					}
					if (edition) {
						$(this).find('parameter').each(function () {
							let param_name = $(this).children('name').text();
							let tooltip;
							let readonly = "";
							let style;
							if ($(this).attr('adjustable') === 'true') {
								tooltip = __("Cliquer pour fixer le paramètre");
								style = "display: block;";
							} else {
								tooltip = __("Cliquer pour libérer le paramètre");
								readonly = "readonly";
								style = "display: none;";
							}
							html_params += `
							<div class="fx_pfunc_param" data-param="${param_name}" >
								<div class="fx_pfunc_param_name" title="${tooltip}">${param_name}&nbsp;=</div>
								<div class="fx_pfunc_param_val ${readonly}" contenteditable="${$(this).attr('adjustable')}" inputmode="decimal">${$(this).find('value').text()}</div>
								<div>
									<div class="fx_pfunc_param_increment increment_incr fx_button" title="${__("Incrémenter de 1 le dernier chiffre significatif")}" style="${style}"><i class="fa fa-caret-up"></i></div>
									<div class="fx_pfunc_param_increment increment_decr fx_button" title="${__("Décrémenter de 1 le dernier chiffre significatif")}" style="${style}"><i class="fa fa-caret-down"></i></div>
								</div>
								<div class="fx_pfunc_param_uncertainty">${ ($(this).find('uncertainty') && $(this).find('uncertainty').text()) ? ('± ' + $(this).find('uncertainty').text()) : '' }</div>
							</div>`;
						});
					} else {
						let params = [];
						$(this).find('parameter').each(function () {
							params.push(
								`${$(this).children('name').text()}&nbsp;=&nbsp;${$(this).find('value').text()}` 
								+ ($(this).find('uncertainty').text() !== '' ? (' ± ' + $(this).find('uncertainty').text()) : ''));
						});
						html_params = params.join("&nbsp;; ");
					}

					// interval
					xstart = $(this).find('xstart').text();
					xend = $(this).find('xend').text();
					if (xstart || xend || edition) {
						html_interval =
							`<div class="fx_pfunc_div_interval" title="${__("Intervalle de définition de la fonction")}">
								${edition
								? `[&nbsp;<input name="xstart" inputmode="decimal" value="${xstart}" placeholder="xmin"/>&nbsp;; <input name="xend" inputmode="decimal" value="${xend}" placeholder="xmax"/>&nbsp;]`
								: `[&nbsp;${xstart}&nbsp;; ${xend}&nbsp;]`
							}
							</div>`;
					}

					//distances
					let iplot = 0;
					$XMLgraph.find('plot').each(function () {
						let color = $(this).attr('color') ? $(this).attr('color') : "#000"; // legacy
						let style = `color: ${color}`;
						html_dist += `<span class="fx_distance" style="${style}" data-iplot="${iplot}">###</span>`;
						iplot++;
					});

					// html code
					let error = $(this).find('formula').attr('error');
					let formula = escapeXML($(this).find('formula').text());
					let div_classes = error ? ' fx_error' : '';
					div_classes += func_descr ? ' fx_pfunc_div_formula_description' : '';
					let div_error = error ? `title="${fx_errors[error]} - ${formula}"` : '';
					html_funcs += `
					<div class="fx_graph_pfunc ${edition ? '' : 'fx_graph_pfunc_not_edit'}" data-ifunc="${ifunc}">
						${edition ? '<div class="fx_pfunc_div_col_pick fx_color_picker_btn" title="' + __("Modifier la couleur de la courbe") + '"><i class="fa fa-square" style="color:' + color + '"></i></div>' : ''}
						<div style="color:${color}" class="${fx_pfunc_div_menu} ${edition ? 'fx_button' : ''}" ${edition ? `title="${__('Sélectionner une fonction prédéfinie')}"` : ''}>
							${func_name}&nbsp;${edition ? '<i class="fa fa-caret-right"></i>' : '='}
						</div>`;
					if (edition && !func_descr) { //input
						html_funcs += `<div class="fx_pfunc_div_formula fx_pfunc_div_formula_edit" ${div_error}><textarea rows="1" class="fx_pfunc_input ${div_classes}" spellcheck="false" ${textareaDisabled}>${formula}</textarea></div>`;
					} else { //div
						html_funcs += `<div class="fx_pfunc_div_formula ${div_classes}" ${div_error}>${func_descr ? func_descr : formula}</div>`;
					}
					if (edition && $XMLplots.get().length > 1) {
						let plot_index = $(this).attr('fit_iplot') === undefined ? 0 :  parseInt($(this).attr('fit_iplot'));
						let plot_color = $($XMLplots[plot_index]).attr('color');
						let picker_title = __("Choisir la série de points sur laquelle faire l'ajustement");
						html_plot_picker += `<div class="fx_fit_div_col_pick fx_color_picker_btn" title="${picker_title}" data-color="${plot_color}"><i class="fas fa-circle" style="color:${plot_color}"></i></div>`;
					}
					if (edition) {
						html_params = html_plot_picker + html_params;
					}
					html_funcs += `
						${html_interval}
						<div class="${edition ? 'fx_pfunc_div_param_edit' : 'fx_pfunc_div_param'}">${html_params}</div>
						<div class="fx_pfunc_div_dist">${html_dist}</div>
						${edition ? '<div class="fx_pfunc_div_delete"><i class="far fa-trash-alt" title="' + __('Supprimer la fonction') + '"></i></div>' : ''}
					</div>`;
					ifunc++;
				});

				// line for adding a func
				if (edition) {
					html_funcs += `
						<div class="fx_graph_pfunc">
							<div class="fx_pfunc_div_col_pick"></div>
							<div class="${fx_pfunc_div_menu} fx_button" title="${__('Sélectionner une fonction prédéfinie')}">
								${func_name}&nbsp;<i class="fa fa-caret-right"></i>
							</div>
							<div class="fx_pfunc_div_formula"><textarea rows="1" class="fx_pfunc_input" spellcheck="false" placeholder="${textareaPlaceholder}" ${textareaDisabled}></textarea></div>
							<div class="fx_pfunc_div_dist"></div>
						</div>`;
				}

				// Display
				$HTMLcontentLD.find(".fx_graph[data-igraph='"+igraph+"']").find('.fx_graph_pfuncs').html(html_funcs);
				displayDistances(igraph, -1, update_uncertainties);
				// expand the height of the textarea to the height of the line
				$(".fx_pfunc_input").each(function() {
					let p_height = $(this).parent('.fx_pfunc_div_formula').height();
					$(this).outerHeight(p_height);
				});
			}
			igraph++;
		});
	}
	
	/**
	 *
	 * @param {int} sel_graph
	 * @param {int} sel_func
	 * @param {boolean} update_uncertainties whether to update or not uncertainties
	 */
	function displayDistances(sel_graph = -1, sel_func = -1, update_uncertainties=true) {
		let igraph = 0;
		$XMLcodeLD.find("graph").each(function() { // graphs iteration
			if (sel_graph === -1 || sel_graph === igraph) {
				let iplot = 0;
				let $graph = $(this);
				$graph.find("plot").each(function() { // plots iteration
					let ifunc = 0;
					$graph.find("function").each(function() { // functions iteration
						if (sel_func === -1 || sel_func === ifunc) {
							let distance = {dist: null, khi2: false, u_dist: 0, nb_pts: 0};
							if (globalLD.graphs[igraph].plots[iplot] && globalLD.graphs[igraph].plots[iplot].length) { // only if the plot has points
								distance = globalLD.model.computeDistance(igraph, iplot, ifunc);
								if (update_uncertainties) {
									$(this).find("parameter").each(function () {
										globalLD.model.updateUncertaintyModel($(this), '');
										updateUncertaintyView($(this), '');
									});
								}
							}
							let $div = $HTMLcontentLD.find('.fx_graph[data-igraph="'+igraph+'"]')
								.find('.fx_graph_pfunc[data-ifunc="'+ifunc+'"]')
								.find('.fx_distance[data-iplot="'+iplot+'"]');

							let plot_index = $(this).attr('fit_iplot') === undefined ? 0 : parseInt($(this).attr('fit_iplot'));
							if (iplot === plot_index) {
								if (distance.dist === null) {
									$div.attr('title', __("La distance entre les points et la courbe n'a pas pu être calculée\nTrop peu de points expérimentaux ({{nb_pts}}) en regard du nombre de paramètres", {nb_pts: distance.nb_pts}));
									$div.html("###");
								} else {
									let nb_pts_txt = __("Calculé sur {{nb_pts}} points expérimentaux", {nb_pts: distance.nb_pts});
									if (distance.khi2) {
										let u = formatFXNumber(distance.u_dist, null, 2);
										let dist = formatFXNumberWithU(distance.dist, null, u, 2);
										$div.html("χ²&nbsp;=&nbsp;" + dist + "&nbsp;±&nbsp;" + u);
										$div.attr('title', (__("Khi-deux réduit") + " : " + "χ² = ∑{ ((yi-f(xi)) / ui)² } / (N-p)" + "\n" + nb_pts_txt));
									} else {
										let dist = formatFXNumber(distance.dist, null, 4);
										$div.html("<em>s<sub>r</sub></em>&nbsp;=&nbsp;" + dist);
										$div.attr('title', (__("Ecart-type résiduel") + " : " + "sr = √{ ∑{ (yi-f(xi))² } / (N-p) }" + "\n" + nb_pts_txt));
									}
								}
							} else {
								$div.remove();
							}
						}
						ifunc++;
					});
					iplot++;
				});
			}
			igraph++;
		});
	}

	/**
	 * @param {int} igraph
	 * @param {boolean} new_graph
	 * @param {string} graphType 'X-Y' or 'histogram'
	 */
	function displayGraphPropPanel(igraph, new_graph=false, graphType='X-Y') {
		let $panel = graphType === 'histogram' ? $HTMLcontentLD.find(".fx_histogram_prop_panel") : $HTMLcontentLD.find(".fx_graph_prop_panel");
		$panel.data('igraph', igraph);
		$panel.find('.fx_graph_plot').remove();
		let $XMLgraph = $XMLcodeLD.find('graph').eq(igraph);

		// init the panel
		$HTMLcontentLD.find(".fx_graph_prop_plot").attr("data-addplot", 0);
		if (new_graph) {
			$panel.data('new_graph', "true");
			addPlot(null, null, null, null, null, null, null, graphType); // a first line to define a plot is diplayed initially
			$panel.find('.fx_graph_name, .fx_axis_xmin, .fx_axis_xmax, .fx_axis_xlabel, .fx_axis_ymin, .fx_axis_ymax, .fx_axis_ylabel, .fx_axis_y2min, .fx_axis_y2max, .fx_axis_y2label, .fx_global_options_bins_number').val('');
			$panel.find('.fx_axis_xscale, .fx_axis_yscale, .fx_axis_y2scale, .fx_axis_frequency').prop('checked', false);
			$panel.removeClass("fx_show_y2_axis");
		} else {
			$panel.data('new_graph', "false");
			let y2 = $XMLgraph.attr("y2") === "true";
			let $XMLplots = $XMLgraph.find('plot');
			$XMLplots.each(function() {
				addPlot($(this).attr('color'), $(this).children("x").text(), $(this).children("y").text(), $(this).children("ux").text(), $(this).children("uy").text(), $(this).attr('curve'), $(this).attr('axis'), graphType);
			});
			if ($XMLplots.length === 0) {
				addPlot(null, null, null, null, null, null,null, graphType);
			}
			$panel.find('.fx_graph_name').val(unEscapeXML($XMLgraph.children("name").text()));
			$panel.find('.fx_global_options_bins_number').val(unEscapeXML($XMLgraph.children("bins_number").text()));
			updateAxisParams("x");
			updateAxisParams("y");
			if (y2) {
				$panel.addClass("fx_show_y2_axis");
				updateAxisParams("y2");
			} else {
				$panel.removeClass("fx_show_y2_axis");
			}
		}
		/**
		 * @param {string} axis
		 */
		function updateAxisParams(axis) {
			$panel.find('.fx_axis_'+axis+'min').val($XMLgraph.children(axis+'min').text());
			$panel.find('.fx_axis_'+axis+'max').val($XMLgraph.children(axis+'max').text());
			if ($XMLgraph.find(axis+'scale').text() === 'log') {
				$panel.find('.fx_axis_'+axis+'scale').prop('checked', true);
			} else {
				$panel.find('.fx_axis_'+axis+'scale').prop('checked', false);
			}
			let label = $XMLgraph.children(axis+'label').length ? unEscapeXML($XMLgraph.children(axis+'label').text()) : '';
			$panel.find('.fx_axis_'+axis+'label').val(label);
			// histogram specific
			if (graphType === 'histogram') {
				if ($XMLgraph.attr('frequency') === 'true') {
					$panel.find('.fx_axis_frequency').prop('checked', true);
				} else {
					$panel.find('.fx_axis_frequency').prop('checked', false);
				}
			}
		}

		// display the panel
		let $graph_title = $HTMLcontentLD.find(".fx_graph").eq(igraph).find('.fx_graph_title');
		let position = $graph_title.position();
		$panel.css('top', position.top + $graph_title.outerHeight());
		$panel.css('left', position.left);
		$panel.show();
		if (new_graph) {
			toggleFXModal(true);
			$panel.find(".fx_graph_name").focus();
		} else {
			let selector = graphType === 'histogram' ? ".fx_histogram_prop_panel" : ".fx_graph_prop_panel";
			toggleFXModal(true, selector);
			$panel.focus(); // used for validateOnEnter
		}
	}

	/**
	 * @param {string | null} color
	 * @param {string | null} code_x
	 * @param {string | null} code_y
	 * @param {string | null} code_ux
	 * @param {string | null} code_uy
	 * @param {string | null} curve
	 * @param {string | null} axis
	 * @param {string | null} graphType 'X-Y' or 'histogram'
	 */
	function addPlot(color = null, code_x = null, code_y = null, code_ux = null, code_uy= null, curve = null, axis = null, graphType='X-Y') {
		if (!color) {
			let $panel = graphType === 'histogram' ? $HTMLcontentLD.find(".fx_histogram_prop_panel") : $HTMLcontentLD.find(".fx_graph_prop_panel");
			let $plots = $panel.find(".fx_plot_col_pick");
			let used_colors = new Set();
			$plots.each(function() {
				used_colors.add($(this).data("color"));
			});
			let available_colors = fx_colors.filter(col => !used_colors.has(col));
			if (available_colors.length) {
				color = available_colors[0];
			} else {
				color = fx_colors[0];
			}
		}
		let html_plot;
		let iplot = $HTMLcontentLD.find(".fx_graph_prop_plot").attr("data-addplot");
		if (graphType === 'histogram') {
			html_plot =
				`<tr class="fx_graph_plot" data-iplot="${iplot}">
					<td class="fx_plot_col_pick fx_color_picker_btn" title="${__('Modifier la couleur des classes')}" data-color="${color}"><i class="fa fa-square" style="color:${color}"></i></td>
					<td><select class="fx_graph_plot_x">${buildColumnsSelectOptions(code_x)}</select></td>
					<td><i class="far fa-trash-alt fx_delete_plot" title="${__('Supprimer la série')}"></i></td>
				</tr>`;
			$HTMLcontentLD.find(".fx_histogram_prop_panel").find(".fx_add_plot").before(html_plot);
		} else {
			html_plot =
				`<tr class="fx_graph_plot" data-iplot="${iplot}">
					<td class="fx_plot_col_pick fx_color_picker_btn" title="${__('Modifier la couleur des points')}" data-color="${color}"><i class="fa fa-square" style="color:${color}"></i></td>
					<td><select class="fx_graph_plot_x">${buildColumnsSelectOptions(code_x)}</select></td>
					<td><select class="fx_graph_plot_y">${buildColumnsSelectOptions(code_y)}</select></td>
					<td></td>
					<td><select class="fx_graph_plot_ux">${buildColumnsSelectOptions(code_ux, true)}</select></td>
					<td><select class="fx_graph_plot_uy">${buildColumnsSelectOptions(code_uy, true)}</select></td>
					<td></td>
					<td><select class="fx_graph_curve">
						<option value="no">${__("non")}</option>
						<option value="segments" ${curve === 'segments' ? 'selected' : ''}>${__("segments")}</option>
						<option value="smoothed" ${curve === 'smoothed' ? 'selected' : ''}>${__("courbe")}</option>
					</select></td>
					<td class="fx_plot_axis"><select class="fx_plot_axis_choice">
						<option value="y">y</option>
						<option value="y2" ${axis === 'y2' ? 'selected' : ''}>y2</option>
					</select></td>
					<td><i class="far fa-trash-alt fx_delete_plot" title="${__('Supprimer les points')}"></i></td>
				</tr>`;
			$HTMLcontentLD.find(".fx_graph_prop_panel").find(".fx_add_plot").before(html_plot);
		}
		$HTMLcontentLD.find(".fx_graph_prop_plot").attr("data-addplot", ++iplot);

		/**
		 * @param {string|null} code code of the selected column
		 * @param {boolean} constant
		 * @returns {string} HTML code for the options of a select : all columns of type != string
		 */
		function buildColumnsSelectOptions(code, constant = false) {
			let plot_options = '<option value="">-</option>';
			let option;
			$XMLcodeLD.find("column").each(function () {
				if ($(this).attr('type') === "num") {
					option = $(this).find('code').text();
					plot_options += `<option value="${option}" ${code === option ? 'selected' : ''}>${option}</option>`;
				}
			});
			if (constant) {
				$XMLcodeLD.find("constant").each(function () {
					option = $(this).find('name').text();
					plot_options += `<option value="${option}" ${code === option ? 'selected' : ''}>${option}</option>`;
				});
			}
			return plot_options;
		}
	}


	//******************************************************************************************************************
	//                                          GENERAL
	//******************************************************************************************************************

	/**
	 * Display a panel over the LD => the opened panel becomes modal
	 * @param {Boolean} display hide or show the cover
	 * @param {String} selector the panel to hide if the modal cover is clicked
	 */
	function toggleFXModal(display, selector="") {
		let fx_modal = $HTMLcontentLD.find(".fx_modal_cover");
		if (display) {
			fx_modal.show() ;
			if (selector) {
				fx_modal.click(function () {
					$HTMLcontentLD.find(selector).hide();
					toggleFXModal(false) ;
				});
			}
		} else {
			fx_modal.hide();
			fx_modal.off("click");
		}
	}

	/**
	 * @param {dom} target
	 * @param {string} container selector of the container
	 */
	function displayMathFormulaHelp(target, container) {
		let $f_panel = $HTMLcontentLD.find(".fx_math_formula_help");
		// build the formula help
		$f_panel.find(".fx_const_for_formulas span").remove();
		$XMLcodeLD.find("constant").each(function() {
			let symbol = $(this).find("name").text();
			let html_const = `<span class="fx_formula_help_symbol" title="${$(this).find("value").text()}" data-symbol="${symbol}">${symbol}</span>`;
			$f_panel.find(".fx_const_for_formulas").append(html_const);
		});
		if (container === ".fx_graph_pfunc") {
			$f_panel.find(".fx_math_formula_help_col").hide();
		} else {
			$f_panel.find(".fx_math_formula_help_col").show();
			$f_panel.find(".fx_col_for_formulas span").remove();
			let col_code = $(target).data("col_code");
			$XMLcodeLD.find('column[type="num"]').each(function() {
				let symbol = $(this).find("code").text();
				if (symbol !== col_code) {
					let html_codes = `<span class="fx_formula_help_symbol" title="${$(this).find("name").text()}" data-symbol="${symbol}">${symbol}</span> `;
					$f_panel.find(".fx_col_for_formulas").append(html_codes);
				}
			});
		}
		// display
		let $container = $(target).closest(container);
		$f_panel.appendTo($container);
		let v_shift = $(target).outerHeight() + $(target).position().top + 3;
		$f_panel.css('top', v_shift);
		let container_left = $container.offset().left;
		if (container_left + $f_panel.outerWidth() > $HTMLcontentLD.outerWidth()) {
			$f_panel.css('left', $container.outerWidth() - $f_panel.outerWidth() + 2);
		} else {
			$f_panel.css('left', 0);
		}
		$f_panel.show();
	}

	/**
	 * @param {dom} target
	 * @param {boolean} only_plot_colors, if true, displays only the plots colors
	 */
	function displayColorPicker(target, only_plot_colors=false) {
		let $col_picker = $HTMLcontentLD.find(".fx_color_picker");
		if (only_plot_colors){
			let $XMLgraph = $XMLcodeLD.find('graph').eq($col_picker.data('igraph'));
			let data_colors = $XMLgraph.find('plot').get().map(e => $(e).attr('color'));
			$col_picker = $col_picker.clone(true).off().toggleClass("fx_color_picker fx_plot_fit_color_picker");
			$col_picker.html(data_colors.map(
				(e, i) => (((i > 0 && i % 3) === 0) ? '<br>': '')
				+ '<i class="fas fa-circle" title="' + __("Série de points n°") + (1 + i)
				+ `" style="color:${e};font-size:1em;padding:3px" data-color="${e}"  data-iplot="${i}"></i>`).join("")
			);
		}
		$col_picker.prependTo($(target));
		$col_picker.show();
	}
	
	function updateUncertaintyView ($parameter, uncertaintyValue='') {
		let parameter_idx = $parameter.parent().find('parameter').index($parameter);
		let $function = $parameter.parent();
		let function_idx = $function.parent().find('function').index($function);
		let $graph = $function.parent();
		let graph_idx = $graph.parent().find('graph').index($graph);
		let $uncertainty = $HTMLcontentLD
			.find('.fx_graph').eq(graph_idx)
			.find('.fx_graph_pfuncs')
			.find('.fx_graph_pfunc ').eq(function_idx)
			.find('.fx_pfunc_param').eq(parameter_idx)
			.find('.fx_pfunc_param_uncertainty');
		if (uncertaintyValue === '') {
			$uncertainty.text('');
		} else {
			$uncertainty.text('± ' + uncertaintyValue);
		}
	}

	// API
	return {
		// var
		fx_colors: fx_colors,
		stat_ind: stat_ind,
		fx_errors: fx_errors,
		// basis display funcs
		displayTable: displayTable,
		displayTableStatIndicators: displayTableStatIndicators, // called by displayTable
		displayTableRows: displayTableRows,                     // called by displayTable
		displayGraphs: displayGraphs,
		displayGraphFlot: displayGraphFlot,   // called by displayGraphs
		displayGraphPFuncs: displayGraphPFuncs, // called by displayGraphs
		displayDistances: displayDistances,   // called by displayGraphPFuncs
		// displays only on demand
		toggleFXModal: toggleFXModal,
		displayExportPanel: displayExportPanel,
		displayConstantPanel: displayConstantPanel,
		displayFitSettings: displayFitSettings,
		addConstantRowInPanel: addConstantRowInPanel,
		displayImportPanel: displayImportPanel,
		displayColPropPanel: displayColPropPanel,
		displayGraphPropPanel: displayGraphPropPanel,
		addPlot: addPlot,
		displayMathFormulaHelp: displayMathFormulaHelp,
		displayColorPicker: displayColorPicker,
		updateUncertaintyView: updateUncertaintyView
	};
});

/**
 * Format a number with the column parameters in order to display it in Fitex
 * @param {string | number} num
 * @param {boolean | null} scientific null means that the system choose depending on the value
 * @param {number | null} digits number of digits after comma if scientific is false; significant digits else
 * @returns {string}
 * scientific == false => digits (if not null) indicates the number of decimals to show
 * scientific == true  => digits (if not null) indicates the number of significant digits to show
 * scientific == null  => digits (if not null) indicates the number of significant digits to show
 */
function formatFXNumber(num, scientific = null, digits = null) {
	const min_not_scientific = 0.0001, max_not_scientific = 1e9;
	let string_num = "";
	if (typeof num === 'string') {
		if (num === "" || num === "###") { // error code
			return num;
		}
		num = num.replace(/(\d),/, '.');
		string_num = num;
	}
	num = parseFloat(num);
	if (isNaN(num)) {
		return "";
	}

	let absnum = Math.abs(num);
	if (absnum === 0) {
		return "0";
	}

	if (scientific === null) { // digits indicates the number of significant digits to show
		scientific = (absnum < min_not_scientific || absnum >= max_not_scientific);
		if (digits > 0 && Math.log10(absnum) > digits) {
			scientific = true;
		}
		if (scientific === false && digits > 0) {
			digits = digits - Math.floor(Math.log10(absnum)) - 1; // number of decimals
		}
	}
	if (scientific) { // digits indicates the number of significant digits to show
		if (digits === null || digits < 0) {
			num = parseFloat(math.format(num, {precision: 10})); // avoid the problems of display such as 6.000000000000001E-5
			if (absnum < 1 || absnum >= 10) {
				num = num.toExponential();
			}
		} else {
			num = num.toExponential(digits - 1);
		}
	} else { // digits indicates the number of decimals to show
		if (digits === null || digits < 0) { // default case
			if (string_num && string_num.length < 8) { // keep the shape of the number written by the user
				num = string_num;
			}
			else {
				num = parseFloat(math.format(num, {precision: 9})); // avoid the problems of display such as 6.000000000000001E-5
				num = math.format(num, {notation: "fixed"});
			}
		} else {
			num = num.toFixed(digits);
		}
	}
	return num.toString().replace("e", "E").replace("E+0", "");
}

/**
 * Format a number regarding the last decimal position of its uncertainty
 * @param {string | number} num
 * @param {true | null} scientific null means that the system choose depending on the value
 * @param {string} u_value value of uncertainty
 * @param {number} u_sign_digits number of significant digits in the uncertainty
 * @returns {string}
 * scientific == false => digits (if not null) indicates the number of decimals to show
 * scientific == true  => digits (if not null) indicates the number of significant digits to show
 * scientific == null  => digits (if not null) indicates the number of significant digits to show
 */
function formatFXNumberWithU(num, scientific=null, u_value, u_sign_digits) {
	let u = Number(u_value);
	if (isNaN(u)) {
		return formatFXNumber(num, scientific, 4);
	}
	let first_sd_pos_num = Math.floor(Math.log10(Math.abs(num)));
	let last_sd_dpos_u = Math.floor(Math.log10(u)) - u_sign_digits + 1;
	let sign_digits_num = first_sd_pos_num - last_sd_dpos_u + 1;
	if (sign_digits_num < 1) {
		sign_digits_num = 1;
	}
	if (sign_digits_num > 4) {
		sign_digits_num = 4;
	}
	return formatFXNumber(num, scientific, sign_digits_num);
}
