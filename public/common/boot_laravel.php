<?php
/**
 * Transitional file that wraps the legacy code into Laravel
 */

require_once dirname(__DIR__, 2) . '/vendor/autoload.php';

if (!defined('LARAVEL_START')) {
    define('LARAVEL_START', microtime(true));

    // create the Laravel context
	$middlewares = [
			\App\Http\Middleware\EncryptCookies::class,
			\Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
			\Illuminate\Session\Middleware\StartSession::class,
			// \Illuminate\Session\Middleware\AuthenticateSession::class,
			\Illuminate\View\Middleware\ShareErrorsFromSession::class,
			//\App\Http\Middleware\VerifyCsrfToken::class,
			//\Illuminate\Routing\Middleware\SubstituteBindings::class,
			\Fruitcake\Cors\HandleCors::class,
			\App\Http\Middleware\Language::class,
	];
    $app = require_once dirname(__DIR__, 2) . '/bootstrap/app.php';
    $kernel = $app->make(Illuminate\Contracts\Http\Kernel::class);
    $request = Illuminate\Http\Request::capture();

    $app->instance('request', $request);
    \Illuminate\Support\Facades\Facade::clearResolvedInstance('request');
	$kernel->bootstrap();
	(new \Illuminate\Routing\Pipeline($app))
		->send($request)
		->through($middlewares)
		->then(function() use ($kernel, $request) {
			if (file_exists(storage_path('framework/down'))) {
				abort(503); // Maintenance mode
			}

			return $kernel->handle($request);
	});
}
