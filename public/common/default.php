<?php
// Use Laravel session handler
require_once __DIR__.'/boot_laravel.php';
require_once dirname(dirname(__DIR__ )) . '/vendor/autoload.php';

bootstrap();

function bootstrap() {

	// autoload classes from dir classes/
	spl_autoload_register(function ($className) {
        if (strpos($className, '\\') !== false) {
            $splits = array_filter(explode('\\', $className));
            if (count($splits) === 1) {
                // root namespace: \ClassName
                $fileName = $splits[0];
            } else if (count($splits) === 2 && $splits[0] === 'Legacy') {
                // \Legacy\ClassName
                $fileName = $splits[1];
            } else {
                return null;
            }
        } else {
            $fileName = $className;
        }
        $path = dirname(__DIR__) . "/classes/$fileName.php";
        include_once $path;
    });

	if (!config('app.debug')) {
		ini_set('display_errors', "0");
	} else {
		error_reporting(E_ALL);
		ini_set('display_errors', "1");
	}
}

/**
 * Append the message to the log file.
 *
 * @param string $message
 * @param string $file Path to the log file (default: "storage/app/laravel_<date>.log")
 * @param int $time (optional)
 * @param string $trace (optional)
 */
function logIntoFile($message, $file = null, $time = null, $trace = "") {
	\App\Helper::logIntoFile($message, $file, $time, $trace);
}

function logJsError($message, $time, $trace) {
	\App\Helper::logJsError($message, $time, $trace);
}

/**
 * From an path relative to LabNBook's root or an URL, return the HTML that loads the asset.
 *
 * E.g. < ?= loadAsset('/js/common.js') ? >
 * (if you copy this, remove the unbreakable spaces)
 *
 * @param string $url /repository/root/to/asset OR https://external.doc/asset OR //local/url/asset
 * @param array|string $htmlAttributes E.g. "async" OR ["charset" => "utf-8"]
 * @return string HTML
 * @throws Exception
 */
function loadAsset($url, $htmlAttributes = []) {
	return \App\Helper::loadAsset($url, $htmlAttributes);
}

/**
 * HTML block to insert at the top of the body.
 *
 * @return string
 */
function commonHtmlAlerts() {
	return view('_flash-alerts-data');
}

function addAlert($category, $message) {
    \App\Helper::addAlert($category,$message);
}
