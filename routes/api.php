<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */


/*
|--------------------------------------------------------------------------
| API v1
|--------------------------------------------------------------------------
 */
// Route for checking LabNBook status
Route::get('v1/status', 'API\V1\AuthController@status');

Route::group([
    'prefix' => 'v1/',
    'middleware' => [
        'jwtserver'
    ],
], function () {
    $namespace = 'API\V1';
    /*
    |--------------------------------------------------------------------------
    | Routes accessible to any authorized external institution
    |--------------------------------------------------------------------------
    */
    // Standard auth routes
    Route::post('/auth/login', $namespace.'\AuthController@login');

    // Search for users
    Route::get('/user/search', $namespace.'\UserController@search');
    Route::get('/user/exportData', $namespace.'\UserController@exportData');
    Route::post('/user/deleteData', $namespace.'\UserController@deleteData');
    Route::get('/user/fromTeamConfigIds', $namespace.'\UserController@fromTeamConfigIds');

    Route::get('/redirect', $namespace . '\RedirectController@index');
    Route::post('/teamconfig/{teamconfig}/delete', $namespace.'\TeamconfigController@delete');
    Route::post('/extplatform', $namespace.'\ExtplatformController@update');
});

Route::group([
    'prefix' => 'v1/',
    'middleware' => [
        'jwtserver',
        'auth:api',
    ],
], function () {

    $namespace = 'API\V1';

    /*
    |--------------------------------------------------------------------------
    | Authenticated routes : authorized user provider + logged user (JWTAuth)
    |--------------------------------------------------------------------------
    */
    // Standard auth routes
    Route::post('/auth/logout', $namespace.'\AuthController@logout');
    Route::post('/auth/refresh', $namespace.'\AuthController@refresh');
    Route::post('/auth/me', $namespace.'\AuthController@me');

    // liste des missions accessibles à l'utilisateur-enseignant
    Route::get('/mission/', $namespace.'\MissionController@index');
    // Detail d'une mission
    Route::get('/mission/{mission}', $namespace.'\MissionController@show');
    // Crée une mise en équipe
    Route::post('/mission/{mission}/use', $namespace.'\MissionController@createTeamConfig');
    // Modifie une mise en equipe
    Route::post('/mission/{mission}/update', $namespace.'\MissionController@updateTeamConfig');
    // ajoute l'enseignant comme tuteur
    Route::post('/mission/grant', $namespace.'\MissionController@grant');
    Route::post('/user/grantteacher', $namespace.'\UserController@grantteacher');
    // Liste les classes L non-vides rattachées à un cours M
    Route::post('/classe/updateParticipants', $namespace.'\ClasseController@updateParticipants');

    Route::get('/teamconfig/{teamconfig}', $namespace.'\TeamconfigController@show');
    Route::get('/teamconfig/{teamconfig}/countStartedReports', $namespace.'\TeamconfigController@countStartedReports');
    Route::get('/teamconfig/{teamconfig}/countSubmittededReports', $namespace.'\TeamconfigController@countSubmittedReports');

});
