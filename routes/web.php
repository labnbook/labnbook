<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

/*
|--------------------------------------------------------------------------
| Public routes
|--------------------------------------------------------------------------
 */
// Authentication
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login')->middleware('disconnectTest');
Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

Route::any('/classe/joinByCode', 'ClasseController@joinByCode')->name('joinClassByCode');
// CAS
Route::any('/auth/cas/login', 'Auth\CasController@login')->name('login-cas');
Route::any('/auth/cas/logout', 'Auth\CasController@logout')->name('logout-cas');

// User creation
Route::post('user/create', 'UserController@create');

// API redirection
Route::get('/redirect', 'HomeController@redirect');
// User binding preparation
Route::get('user/preBind', 'UserController@preBind');
Route::post('user/createFromAPI', 'UserController@createFromAPI');
Route::post('user/getHomonymes', 'UserController@getHomonymes');

// Tools standalone
Route::get('/fitex', function () {
    return redirect('/tools_standalone/fitex_stdalone.php');
});
Route::get('/fitex/auto', function () {
    return redirect('/tools_standalone/fitex_stdalone.php?auto=1');
});

// Accessed only from puppeteer
Route::get('/report/{report}/printPDF', 'ReportController@printPDF')->middleware('puppeteerAuth');

// Js errors
Route::post('log/jserror', 'LogController@jserror');

Route::middleware(['guest'])->group(function () {
    /*
    |--------------------------------------------------------------------------
    | Unauthenticated users only
    | Others are redirected to '/'
    |--------------------------------------------------------------------------
     */
    Route::any('/auth/cas/create-teacher', 'Auth\CasController@createTeacher');

    // Reset password
    Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::post('/password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');
    Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
});
// Report test
Route::get('/report/test', 'ReportController@test');


// Task index can be accessed without being logged in
Route::get('/task/', 'TaskController@index');

Route::middleware(['auth'])->group(function () {
    /*
    |--------------------------------------------------------------------------
    | Authenticated routes
    |--------------------------------------------------------------------------
     */

    // CAS
    Route::get('/auth/cas/confirm', 'Auth\CasController@confirm');
    Route::get('/classe/{classe}/join', 'ClasseController@join');
    Route::post('/auth/cas/merge', 'Auth\CasController@merge')->name('merge-cas');
    // Redirecting home page
    Route::get('/', 'HomeController@index')->name('home');

    // Reports (students)
    Route::get('/reports', 'ReportController@index')->name('report.index')->middleware('disconnectTest');
    Route::get('/report/displayPDF', 'ReportController@displayPDF');
    Route::post('/reports/getRecordDescription', 'ReportController@getRecordDescription');
    Route::get('/report/{report}', 'ReportController@show');
    Route::get('/report/enter/{teamconfig}', 'ReportController@enter');
    Route::get('/reports/enterTutorial/{mission}', 'ReportController@enterTutorial');

    // Links an external user id with an existing user, params : external_id, role, backtourl
    Route::get('user/bind', 'UserController@bind');

    // User edition
    Route::get('/user', 'UserController@index');
    Route::get('/user/{user}/edit', 'UserController@edit')->middleware('disconnectTest');
    Route::post('/user/{user}/update', 'UserController@update')->middleware('disconnectTest');
    Route::post('/user/acceptConditions', 'UserController@acceptConditions');

    // Retourne les initiales d'un utilisateur
    Route::post('user/getInitials', 'UserController@getInitials');

    /*
     |-------------------------------------------------------------------------
     | Ajax
     |-------------------------------------------------------------------------
     */
    // Upload files
    Route::post('storage/upload/', 'StorageController@upload');
    // Report (students)
    Route::post('report/{report}/listLdToPrint/', 'ReportController@listLdToPrint');
    Route::post('report/{report}/', 'ReportController@show');
    Route::post('report/{report}/duplicateLD', 'ReportController@duplicateLD');
    Route::post('report/{report}/addLD', 'ReportController@addLD');
    Route::post('report/{report}/editLD', 'ReportController@editLD');
    Route::post('report/{report}/validateLD', 'ReportController@validateLD');
    Route::post('report/{report}/deleteLD', 'ReportController@deleteLD');
    Route::post('report/{report}/synchronize', 'ReportController@synchronize');
    Route::post('report/{report}/trace', 'ReportController@trace');
    Route::post('report/{report}/submit', 'ReportController@submit');
    Route::post('report/{report}/displayLDToImport', 'ReportController@displayLDToImport');
    Route::post('report/{report}/toggleAllLD', 'ReportController@toggleAllLD');
    Route::post('report/{report}/openLDVersioning', 'ReportController@openLDVersioning');
    Route::post('report/{report}/closeLDVersioning', 'ReportController@closeLDVersioning');
    // Report (teachers)
    Route::post('report/{report}/freezeReportTrace', 'ReportController@freezeReportTrace');
    
    // Labdoc
    Route::get('labdoc/{labdoc}/', 'LabdocController@show');
    Route::get('labdoc/{labdoc}/getData', 'LabdocController@getData');
    Route::post('labdoc/{labdoc}/extend', 'LabdocController@extend');
    Route::post('labdoc/{labdoc}/update', 'LabdocController@update');
    Route::post('labdoc/{labdoc}/getLDHeader', 'LabdocController@getLDHeader');
    Route::post('labdoc/{labdoc}/getDiff', 'LabdocController@getDiff');
    Route::post('labdoc/{labdoc}/logDisplayDiffsTrace', 'LabdocController@logDisplayDiffsTrace');
    Route::post('labdoc/{labdoc}/recover', 'LabdocController@recover');
    Route::post('labdoc/{labdoc}/toggleDraftStatus', 'LabdocController@toggleDraftStatus');
    Route::post('labdoc/{labdoc}/move', 'LabdocController@move');
    Route::post('labdoc/{labdoc}/removedStatus', 'LabdocController@removedStatus');
    Route::post('labdoc/{labdoc}/hideStar', 'LabdocController@hideStar');
    Route::post('labdoc/{labdoc}/getCopexTemplate', 'LabdocController@getCopexTemplate');

    // Team_config
    Route::get('teamconfig/{teamconfig}/select', 'TeamConfigController@select');
    Route::get('teamconfig/{teamconfig}/getTeams', 'TeamConfigController@getTeams');
    Route::get('teamconfig/{teamconfig}/getFullTeams', 'TeamConfigController@getFullTeams');
    Route::post('teamconfig/{teamconfig}/selectTeam', 'TeamConfigController@selectTeam');

    // Classe
    Route::post('classe/{classe}/isExternal', 'ClasseController@isExternal');

    // Widgets
    Route::post('widgetconfig/saveUserWidgetConfig', 'UserWidgetConfigController@saveUserWidgetConfig');
    Route::post('widget/saveParams', 'WidgetController@saveParams');

    // Trash
    Route::post('trash/listDeletedLabdoc', 'TrashController@listDeletedLabdoc');
    Route::post('trash/deleteLabdoc', 'TrashController@deleteLabdoc');
    Route::post('trash/restoreLabdoc', 'TrashController@restoreLabdoc');
    // Comments
    Route::post('comment/displayComments', 'CommentController@displayComments');
    Route::post('comment/addComment', 'CommentController@addComment');
    Route::post('comment/{comment}/delete', 'CommentController@delete');
    // Messages
    Route::post('conversations/', 'ConversationController@index');
    Route::post('conversation/displayLD2Send', 'ConversationController@displayLD2Send');
    Route::post('conversation/editParticipants', 'ConversationController@editParticipants');
    Route::get('conversation/getParticipantsByRole', 'ConversationController@getParticipantsByRole');
    Route::get('conversation/getClasses', 'ConversationController@getClasses');
    Route::post('conversation/show', 'ConversationController@show');
    Route::post('conversation/{conversation}/addMessage', 'ConversationController@addMessage');
    Route::post('conversation/{conversation}/delete', 'ConversationController@delete');
    Route::get('conversation/fetchMessages', 'ConversationController@fetchMessages');
    // Ressources
    Route::post('resource/updateLearnerDocs', 'ResourceController@updateLearnerDocs');
    Route::post('resource/addUrl', 'ResourceController@addUrl');
    Route::post('resource/{resource}/delDoc', 'ResourceController@delDoc');
    Route::post('resource/{resource}/renameDoc', 'ResourceController@renameDoc');
    Route::post('resource/{resource}/updateOrInsertLinkResourceUser', 'ResourceController@updateOrInsertLinkResourceUser');
    // Annotations
    Route::post('annotation/get', 'AnnotationController@get');
    Route::post('annotation/store', 'AnnotationController@store');
    Route::post('annotation/{annotation}/update', 'AnnotationController@update');
    Route::post('annotation/{annotation}/delete', 'AnnotationController@delete');
    Route::post('annotation/{annotation}/trace', 'AnnotationController@trace');
    Route::post('annotation/{annotation}/updatePopularity', 'AnnotationController@updatePopularity');
    // Tasks
    Route::post('/task/', 'TaskController@create');
    Route::patch('/task/{task}', 'TaskController@update');
    Route::delete('/task/{task}', 'TaskController@destroy');
    Route::patch('/tasks/updatePositions', 'TaskController@updatePositions');
    Route::post('task/trace', 'TaskController@trace');
    
});


/*
|------------------------------------------------------------------------------
| Teacher routes
|------------------------------------------------------------------------------
*/
Route::middleware(['auth', 'disconnectTest', 'teacher'])->group(function () {

    Route::get('teacher/reports/', 'Teacher\ReportController@index')->middleware('addPermissionIfAllowedByAPI');
    Route::get('teacher/students/', 'Teacher\StudentsController@index')->middleware('addPermissionIfAllowedByAPI');
    Route::get('teacher/simulations/', 'SimulationController@editSimulation');
    Route::get('teacher/missions/', 'Teacher\MissionController@index');
    Route::get('teacher/mission/{mission}/', 'Teacher\MissionController@edit')->middleware('addPermissionIfAllowedByAPI');
    Route::get('teacher/mission/{mission}/getImportLDHTML/{reportPart}', 'Teacher\MissionController@getImportLDHTML');

    Route::get('manager/teachers/', 'Manager\ManagerController@index');

    /*
    |--------------------------------------------------------------------------
    | Ajax reports
    |--------------------------------------------------------------------------
    */
    Route::get('teacher/reports/getReports', 'Teacher\ReportController@getReports');
    Route::get('teacher/reports/getClassesForFilters', 'Teacher\ReportController@getClassesForFilters');
    Route::get('teacher/reports/getMissionsForFilters', 'Teacher\ReportController@getMissionsForFilters');
    Route::get('teacher/report/{report}/getReportAndLearners', 'Teacher\ReportController@getReportAndLearners');
    Route::get('teacher/report/{report}/getStructure', 'Teacher\ReportController@getStructure');
    Route::get('teacher/report/getTeachersFromMission', 'Teacher\ReportController@getTeachersFromMission');
    // Indicators
    Route::get('teacher/reports/getIndicators', 'Teacher\ReportController@getIndicators');
    Route::get('teacher/reports/getInfoClass', 'Teacher\ReportController@getInfoClass');
    Route::get('teacher/reports/getInfoMission', 'Teacher\ReportController@getInfoMission');
    Route::get('teacher/reports/getInfoAverageReports', 'Teacher\ReportController@getInfoAverageReports');
    // Report ajax
    Route::post('teacher/reports/batchUpdate', 'Teacher\ReportController@batchUpdate');
    Route::post('teacher/reports/batchRestore', 'Teacher\ReportController@batchRestore');
    Route::post('teacher/report/{report}/traceSelectReport', 'Teacher\ReportController@traceSelectReport');
    Route::post('teacher/report/{report}/updateStatus', 'Teacher\ReportController@updateStatus');
    Route::post('teacher/report/{report}/delete', 'Teacher\ReportController@delete');
    Route::post('teacher/reports/batchDelete', 'Teacher\ReportController@batchDelete');
    Route::post('teacher/reports/batchArchive', 'Teacher\ReportController@batchArchive');

    /*
    |--------------------------------------------------------------------------
    | Ajax students
    |--------------------------------------------------------------------------
    */
    Route::post('teacher/students/initEditLearnerData', 'Teacher\StudentsController@initEditLearnerData');
    Route::get('teacher/students/getClasses', 'Teacher\StudentsController@getClasses');
    Route::post('teacher/students/findAllByStudentNumbers', 'Teacher\StudentsController@findAllByStudentNumbers');
    Route::get('teacher/students/getAddStudentsView', 'Teacher\StudentsController@getAddStudentsView');

    Route::post('classes/addOrUpdate', 'ClasseController@addOrUpdate');
    Route::post('classe/{classe}/delete', 'ClasseController@delete');
    Route::post('classe/{classe}/unsubscribe', 'ClasseController@unsubscribe');
    Route::post('classe/{classe}/addLearner', 'ClasseController@addLearner');
    Route::post('classe/{classe}/addLearners', 'ClasseController@addLearners');
    Route::post('classe/{classe}/updateClassCode', 'ClasseController@updateClassCode');
    Route::post('classe/{classe}/addOrUpdateUser', 'ClasseController@addOrUpdateUser');
    Route::post('classe/{classe}/checkAndAddCSVLearners', 'ClasseController@checkAndAddCSVLearners');
    Route::post('classe/{classe}/archive', 'ClasseController@archive');
    Route::post('classe/{classe}/unpack', 'ClasseController@unpack');
    Route::get('classe/{classe}/hasLdap', 'ClasseController@hasLdap');
    Route::get('classe/{classe}/getStudents', 'ClasseController@getStudents');
    Route::get('classes/getLearnersData', 'ClasseController@getLearnersData');
    Route::get('classe/initEdit', 'ClasseController@initEdit');

    Route::post('user/{user}/canSoftDeleteLearner', 'UserController@canSoftDeleteLearner');
    Route::post('user/{user}/deleteLearnerFromClasse', 'UserController@deleteLearnerFromClasse');
    Route::post('users/getUserWithInstBy', 'UserController@getUserWithInstBy');

    Route::post('teamconfig/saveTeamSettings', 'TeamConfigController@saveTeamSettings');
    Route::post('teamconfig/{teamconfig}/saveTeams', 'TeamConfigController@saveTeams');
    Route::get('teamconfig/getFromClasseMission', 'TeamConfigController@getFromClasseMission');
    Route::get('teamconfig/getConfigAndTeamsFromClasseMission', 'TeamConfigController@getConfigAndTeamsFromClasseMission');
    Route::post('teamconfig/{teamconfig}/startedReports', 'TeamConfigController@startedReports');
    Route::post('teamconfig/{teamconfig}/delete', 'TeamConfigController@delete');
    
    /*
   |--------------------------------------------------------------------------
   | Ajax teacher team
   |--------------------------------------------------------------------------
   */
    //TODO add teacher prefix
    Route::get('manager/initEdit', 'Manager\ManagerController@initEdit');
    Route::post('manager/getTeacherDataList', 'Manager\ManagerController@getTeacherDataList');
    Route::post('manager/addOrUpdate', 'Manager\ManagerController@addOrUpdate');
    Route::post('manager/{teacherTeam}/delete', 'Manager\ManagerController@delete');
    Route::post('manager/{teacherTeam}/archive', 'Manager\ManagerController@archive');
    Route::post('manager/{teacherTeam}/unarchive', 'Manager\ManagerController@unarchive');
    Route::post('manager/{teacherTeam}/duplicate', 'Manager\ManagerController@duplicate');
    Route::post('manager/getUserFromLdap', 'Manager\ManagerController@getUserFromLdap');
    Route::post('manager/addTeacherFromLdap', 'Manager\ManagerController@addTeacherFromLdap');
    Route::post('manager/getTeachersFromTeacherTeam', 'Manager\ManagerController@getTeachersFromTeacherTeam');
    Route::get('manager/getTeacherTeams', 'Manager\ManagerController@getTeacherTeams');
    Route::post('manager/grantTeacherRights', 'Manager\ManagerController@grantTeacherRights');
    Route::post('manager/removeTeacherRights', 'Manager\ManagerController@removeTeacherRights');
    Route::post('manager/addTeacher', 'Manager\ManagerController@addTeacher');
    Route::post('manager/getTeacherWithInstAndLinkBy','Manager\ManagerController@getTeacherWithInstAndLinkBy');   

    /*
    |--------------------------------------------------------------------------
    | Ajax missions
    |--------------------------------------------------------------------------
    */
    Route::post('teacher/mission/{mission}/archive', 'Teacher\MissionController@archive');
    Route::post('teacher/missions/create', 'Teacher\MissionController@create');
    Route::post('teacher/mission/{mission}/delete', 'Teacher\MissionController@delete');
    Route::post('teacher/mission/{mission}/unsubscribe', 'Teacher\MissionController@unsubscribe');
    Route::post('teacher/mission/{mission}/duplicate', 'Teacher\MissionController@duplicate');
    Route::post('teacher/mission/{mission}/correct', 'Teacher\MissionController@correct');
    Route::get('teacher/missions/getPublic', 'Teacher\MissionController@getPublic');
    Route::get('teacher/missions/getMine', 'Teacher\MissionController@getMine');
    Route::get('teacher/missions/getMineForTeamCopy', 'Teacher\MissionController@getMineForTeamCopy');

    /*
    |--------------------------------------------------------------------------
    | Ajax missions edit
    |--------------------------------------------------------------------------
    */
    Route::post('teacher/mission/{mission}/updateField', 'Teacher\MissionController@updateField');

    Route::post('teacher/mission/{mission}/linkTeacher', 'Teacher\MissionController@linkTeacher');
    Route::post('teacher/mission/{mission}/linkTeacherTeamFromId', 'Teacher\MissionController@linkTeacherTeamFromId');
    Route::post('teacher/mission/{mission}/updateLinkedTeacher', 'Teacher\MissionController@updateLinkedTeacher');
    Route::post('teacher/mission/{mission}/removeLinkedTeacher', 'Teacher\MissionController@removeLinkedTeacher');

    Route::post('teacher/mission/{mission}/editRP', 'Teacher\MissionController@editRP');
    Route::post('teacher/mission/{mission}/removeRP', 'Teacher\MissionController@removeRP');

    Route::post('teacher/mission/{mission}/validateAddLD', 'Teacher\MissionController@validateAddLD');
    Route::post('teacher/mission/{mission}/refreshLDView', 'Teacher\MissionController@refreshLDView');
    Route::post('teacher/mission/{mission}/validateEditLD', 'Teacher\MissionController@validateEditLD');
    Route::post('teacher/mission/{mission}/duplicateLD', 'Teacher\MissionController@duplicateLD');
    Route::post('teacher/mission/{mission}/removeLD', 'Teacher\MissionController@removeLD');

    Route::post('teacher/mission/{mission}/getLDXMLCode', 'Teacher\MissionController@getLDXMLCode');
    Route::post('teacher/mission/{mission}/updateLDXMLCode', 'Teacher\MissionController@updateLDXMLCode');

    Route::post('teacher/mission/{mission}/setItemPos', 'Teacher\MissionController@setItemPos');
    Route::post('teacher/mission/{mission}/updateAllItemPos', 'Teacher\MissionController@updateAllItemPos');
    Route::post('teacher/mission/{mission}/addURLRD', 'Teacher\MissionController@addURLRD');

    Route::post('resource/{resource}/removeFromMission', 'ResourceController@removeFromMission');
    Route::post('resource/{resource}/updateFromMission', 'ResourceController@updateFromMission');

    /*
    |--------------------------------------------------------------------------
    | Ajax traces
    |--------------------------------------------------------------------------
    */
    Route::post('teacher/traces/selectDataToPlotHeatmap', 'Teacher\TracesController@selectDataToPlotHeatmap');
    Route::post('teacher/traces/selectDataToPlotDashboardCollab', 'Teacher\TracesController@selectDataToPlotDashboardCollab');
    
    /*
    |--------------------------------------------------------------------------
    | Common
    |--------------------------------------------------------------------------
    */

    Route::post('teacher/mission/{mission}/createTestReport', 'Teacher\MissionController@createTestReport');

    Route::get('teacher/students/getLearnerNameDataList', 'Teacher\StudentsController@getLearnerNameDataList');
    Route::post('teacher/reports/{report}', 'Teacher\ReportController@update');

    Route::get('teacher/missions/exportLabdocs', 'Teacher\MissionController@exportLabdocs');
});


/*
 * Admin routes
 */
Route::middleware(['auth', 'admin'])->group(function () {
    Route::get('/admin', 'AdminController@index');
    Route::post('/admin/selfTest', 'AdminController@selfTest');
    Route::post('/admin/sendTestMail', 'AdminController@sendTestMail');
    // Teacher management
    Route::post('/admin/changeRoleTeacher', 'Admin\InstitutionController@changeRoleTeacher');
    // Institutions
    Route::get('/admin/institutions', 'Admin\InstitutionController@index');
    Route::post('/admin/institutions/create', 'Admin\InstitutionController@create');
    Route::get('/admin/institution/{institution}/getClasses', 'Admin\InstitutionController@getClasses');
    Route::post('/admin/institution/{institution}', 'Admin\InstitutionController@update');
    Route::post('/admin/institution/{institution}/delete', 'Admin\InstitutionController@delete');
    // Extplatforms
    Route::get('/admin/extplatforms', 'Admin\ExtplatformController@index');
    Route::post('/admin/extplatforms/create', 'Admin\ExtplatformController@create');
    Route::post('/admin/extplatform/{extplatform}', 'Admin\ExtplatformController@update');
    Route::post('/admin/extplatform/{extplatform}/delete', 'Admin\ExtplatformController@delete');
    // Users
    Route::get('/admin/users', 'Admin\UserController@index');
    Route::get('/admin/user/{user}/getDescr', 'Admin\UserController@getDescr');
    Route::post('/admin/user/{user}', 'Admin\UserController@update');
    Route::post('/admin/user/{user}/impersonate', 'Admin\UserController@impersonate');
    Route::post('/admin/user/{user}/delete', 'Admin\UserController@delete');
    Route::post('/admin/user/{user}/anonymize', 'Admin\UserController@anonymize');
    Route::get('/admin/user/{user}/GDPRDump', 'Admin\UserController@GDPRDump');
});

/*
 * Scoring routes, will be moved to a package
 */
Route::group(
    [
        'prefix' => config('scoring.url_prefix'),
        'middleware' => ['auth']
    ],
    function () {
        $namespace = 'Scoring';
        Route::get('/rubric/getMyLinkedActivities', $namespace . '\RubricController@getMyLinkedActivities');
        Route::get('/rubric/{id_activity}', $namespace . '\RubricController@get');
        Route::get('/rubric/{id_activity}/sections', $namespace . '\RubricController@sections');
        Route::post('/rubric/{id_activity}', $namespace . '\RubricController@update');
        Route::delete('/rubric/{id_rubric}', $namespace . '\RubricController@delete');
        Route::get('/assessment/{id_production}', $namespace . '\AssessmentController@get');
        Route::get('/assessments/getGrades', $namespace . '\AssessmentController@getGrades');
        Route::post('/assessments/publishBatch', $namespace . '\AssessmentController@publishBatch');
        Route::post('/assessment/{id_production}', $namespace . '\AssessmentController@update');
        Route::delete('/assessment/{id_assessment}', $namespace . '\AssessmentController@delete');
        Route::post('/assessment/{id_assessment}/traceView', $namespace . '\AssessmentController@traceView');
        Route::post('/assessment/{id_assessment}/traceEdit', $namespace . '\AssessmentController@traceEdit');
    }
);

/*
 * Simulation routes
 */
Route::group(
    [
        'prefix' => 'simulation',
        'middleware' => ['auth']
    ],
    function () {
        Route::post('/checkProtocolAndGetFeedback', 'SimulationController@checkProtocolAndGetFeedback');
        Route::post('/resetUserSkills', 'SimulationController@resetUserSkills');
        Route::post('/updateDataMigration', 'SimulationController@updateDataMigration');
        Route::get('/getDataJsonMigration', 'SimulationController@getDataJsonMigration');
        Route::get('/getReportSimulationDashboard', 'SimulationController@getReportSimulationDashboard');
        Route::get('/getClassSimulationDashboard', 'SimulationController@getClassSimulationDashboard');
        Route::get('/getSynchronizeData', 'SimulationController@getSynchronizeData');
        Route::post('/traceSimulationNodeAction', 'SimulationController@traceSimulationNodeAction');
        Route::post('/traceSimulationUserAction', 'SimulationController@traceSimulationUserAction');
        Route::post('/traceSimulationInit', 'SimulationController@traceSimulationInit');
        Route::post('/traceSimulationCorrectAnswer', 'SimulationController@traceSimulationCorrectAnswer');
    }
);
