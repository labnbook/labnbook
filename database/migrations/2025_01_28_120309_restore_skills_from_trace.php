<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $latestTraces = DB::table('simulation_trace')
            ->select('id_user', 'simulation_code', DB::raw('MAX(id_simulation_trace) as last_id'))
            ->where('trace_action', 'student_update_knowledge')
            ->groupBy('id_user', 'simulation_code');
        $skillsSimulationTraces = DB::table('simulation_trace as t1')
            ->joinSub($latestTraces, 'simulation_trace', function ($join) {
                $join->on('t1.id_simulation_trace', '=', 'simulation_trace.last_id');
            })->orderByDesc('t1.id_simulation_trace')->limit(20)->get();
        foreach ($skillsSimulationTraces as $skillsSimulationTrace) {
            $skillData = json_decode($skillsSimulationTrace->attributes);
            if($skillData) {
                foreach ($skillData as $skill) {
                    $taskType = \App\SimulationTaskType::find($skill->id_task_type);
                    if($taskType) {
                        $userTaskProfile = $taskType->learners->find(['simulation_code' => $skill->simulation_code, 'id_learner' => $skill->id_user])->first();
                        $oldKnowledge = $userTaskProfile?$userTaskProfile->pivot->knowledge:0;
                        $oldUpdateNumber = $userTaskProfile?$userTaskProfile->pivot->update_number:0;
                        if($oldKnowledge !== $skill->knowledge) {
                            if ($taskType->learners->contains($skill->id_user)) {
                                $taskType->learners()->updateExistingPivot($skill->id_user, ['simulation_code' => $skill->simulation_code,
                                    'feedback_level' => $skill->feedback_level,
                                    'knowledge' => $skill->knowledge,
                                    'update_number' => $oldUpdateNumber+1]);
                            } else {
                                $taskType->learners()->attach($skill->id_user, ['simulation_code' => $skill->simulation_code,
                                    'feedback_level' => $skill->feedback_level,
                                    'knowledge' => $skill->knowledge,
                                    'update_number' => $oldUpdateNumber+1]);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
};
