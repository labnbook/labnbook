<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('trace_action')
            ->insert([
                'id_action' => 57,
                'action' => 'user_merged',
                'action_type' => 'administration'
            ]);
        DB::statement('ALTER TABLE trace ADD INDEX (id_user)');
        DB::statement('ALTER TABLE trace ADD INDEX (id_mission)');
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
