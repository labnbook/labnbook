<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use \App\Labdoc;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $CHUNK_SIZE = 10000;
        $counter = 0;
        // We have checked that:
        //   - no labdoc_data of type dataset contains `type='` (only double quotes in type attribute)
        //   - there are as many labdoc_data containing `<column type="double"` as containing `"double"` (or "string" or "text")
        Labdoc::where('type_labdoc', 'dataset')
            ->where('labdoc_data', 'regexp', '.*<column type="(double|string|text)".*')
            ->chunkById($CHUNK_SIZE, function ($labdocs) use (&$counter) {
                foreach ($labdocs as $ld) {
                    $domDocument = new \DOMDocument();
                    @$domDocument->loadXML($ld->labdoc_data);
                    $columnElements = $domDocument->getElementsByTagName('column');
                    foreach ($columnElements as $column) {
                        if ($column->getAttribute('type') == "double") {
                            $column->setAttribute('type', "num");
                        } elseif ($column->getAttribute('type') == "string") {
                            $column->setAttribute('type', "txt");
                        } elseif ($column->getAttribute('type') == "text") {
                            $column->setAttribute('type', "txt");
                        }
                    }
                    if ($columnElements->length !== 0) {
                        $ld->labdoc_data = $domDocument->saveXML();
                        $ld->updateNoTimestamps(['labdoc_data' => $ld->labdoc_data]);
                        $counter++;
                    }
                }
            });
        Log::info($counter." labdocs modified by migration.");
        error_log($counter." labdocs modified by migration.");
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
