<?php

use App\Mission;
use App\ReportPart;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('simulation_link_super_manager', function(Blueprint $table) {
            $table->string('simulation_code', 32)->nullable(false);
            $table->unsignedInteger('id_super_manager')->nullable(false);
            $table->foreign('id_super_manager')->references('id_user')->on('user')->onDelete('cascade')->onUpdate('cascade');
            $table->primary(['simulation_code', 'id_super_manager'], 'sim_super_manager');
        });
        
        foreach (['titrab', 'dosage-spectro'] as $simulationCode) {
            DB::table('simulation_link_super_manager')->insertOrIgnore(array('simulation_code' => $simulationCode, 'id_super_manager' => 2));
            DB::table('simulation_link_super_manager')->insertOrIgnore(array('simulation_code' => $simulationCode, 'id_super_manager' => 7));
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
};
