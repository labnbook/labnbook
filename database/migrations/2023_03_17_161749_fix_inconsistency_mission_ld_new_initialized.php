<?php

use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('labdoc')
            ->whereNull('labdoc.last_edition')
            ->whereIn('labdoc.id_report',  function($query)
            {
                $query->select('report.id_report')
                    ->from('report')
                    ->where('report.status', '=', 'new')
                    ->where('report.initialized', '=', 0);
            })
            ->delete();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
};
