<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Doctrine\DBAL\Types\FloatType;
use Doctrine\DBAL\Types\Type;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('simulation_task_type', function($table) {
            $table->longText('technological_feedback')->nullable()->default(null);
        });
        
        Schema::table('simulation_constraint', function($table) {
            $table->longText('theoretical_feedback')->nullable()->default(null);
            $table->longText('technical_feedback')->nullable()->default(null);
            $table->longText('correction_feedback')->nullable()->default(null);
        });
        
        $feedbacks = DB::table('simulation_feedback')->get();
        foreach ($feedbacks as $feedback) {
            $taskType = \App\SimulationTaskType::find($feedback->id_task_type);
            if($taskType) {
                $taskType->technological_feedback = $feedback->technological_feedback;
                $taskType->save();
            }
            $constraint = \App\SimulationConstraint::find($feedback->id_constraint);
            $constraint->theoretical_feedback = $feedback->theoretical_feedback;
            $constraint->technical_feedback = $feedback->technical_feedback;
            $constraint->correction_feedback = $feedback->correction_feedback;
            $constraint->save();
        }
        
        Schema::disableForeignKeyConstraints();
        Schema::table('simulation_constraint', function($table) {
            $table->dropForeign(['id_feedback']);
            $table->dropColumn('id_feedback');
        });
        Schema::dropIfExists('simulation_feedback');
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
