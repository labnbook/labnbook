<?php

use App\Mission;
use App\ReportPart;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // update constraint keys
        DB::table('simulation_constraint')
            ->where('simulation_code', 'dosage-spectro')
            ->where('satisfaction_condition_key', 'oneAbsorbanceMeasureShouldExist')
            ->update(['satisfaction_condition_key' => 'oneAbsorbanceMeasureOrSpectrumShouldExist']);
        
        // update material properties
        $labdocs = \App\Labdoc::join('report_part', 'report_part.id_report_part', '=', 'labdoc.id_report_part')
            ->join('mission', 'report_part.id_mission', '=', 'mission.id_mission')
            ->where('mission.simulation_code', 'dosage-spectro')
            ->where('labdoc.type_labdoc', 'procedure')
            ->get(['labdoc.*']);
        
        foreach ($labdocs as $labdoc) {
            $data = json_decode($labdoc->labdoc_data, true);
            $stockSolutionIdx = collect($data['materialList']['listItems'])->search(function($item) {
                return count(collect($item['properties'])->where('name', '=', 'shortName')->whereIn('value', 'stockSolution')) > 0;
            });
            // Change name 
            $minAcceptableWaveLengthIdx = collect($data['materialList']['listItems'][$stockSolutionIdx]['properties'])->search(function($property) {
                return $property['name'] === 'minAcceptableWaveLength';
            });
            if($minAcceptableWaveLengthIdx) {
                $data['materialList']['listItems'][$stockSolutionIdx]['properties'][$minAcceptableWaveLengthIdx]['name'] = 'minAcceptableSpectrumWaveLength';
            }
            $maxAcceptableWaveLengthIdx = collect($data['materialList']['listItems'][$stockSolutionIdx]['properties'])->search(function($property) {
                return $property['name'] === 'maxAcceptableWaveLength';
            });
            if($maxAcceptableWaveLengthIdx) {
                $data['materialList']['listItems'][$stockSolutionIdx]['properties'][$maxAcceptableWaveLengthIdx]['name'] = 'maxAcceptableSpectrumWaveLength';
            }
            $labdoc->labdoc_data = json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
            $labdoc->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
};
