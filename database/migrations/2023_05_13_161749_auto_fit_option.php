<?php

use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mission', function($table) {
            $table->boolean('auto_fit')
                ->nullable(false)
                ->default(0)
                ->comment('Allows/disallows students to automatically adjust parameters on the graphs of all the dataset labdocs')
                ->after('assignment');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
};
