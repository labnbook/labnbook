<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_widget_config', function(Blueprint $table) {
            $table->unsignedInteger( 'id_user');
            $table->string('widget_type', 100)->nullable()->default(null);
            $table->boolean('widget_reduced')->nullable(false)->default(true);
            $table->enum('widget_position', ['bottom', 'right'])->nullable(false)->default('right');
            $table->smallInteger('widget_size_ratio')->nullable()->default(null);
            $table->primary(['id_user']);
            $table->foreign('id_user')->references('id_user')->on('user')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
};
