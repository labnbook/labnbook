<?php

use App\Mission;
use App\ReportPart;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('simulation_constraint')
            ->where('satisfaction_condition_key', 'oneAbsorbanceMeasureOnStockSolutionOrItsDilutionAtStep2')
            ->update(['satisfaction_condition_key' => 'oneAbsorbanceMeasureOnStockSolutionOrItsDilutionAtStep3']);
        DB::table('simulation_constraint')
            ->where('satisfaction_condition_key', 'firstAbsorbanceMeasurementShouldBeInTheAppropriateRangeAtStep2')
            ->update(['satisfaction_condition_key' => 'firstAbsorbanceMeasurementShouldBeInTheAppropriateRangeAtStep3']);
        DB::table('simulation_constraint')
            ->where('satisfaction_condition_key', 'oneAbsorbanceMeasureAtStep3')
            ->update(['satisfaction_condition_key' => 'oneAbsorbanceMeasureAtStep4']);
        DB::table('simulation_constraint')
            ->where('satisfaction_condition_key', 'oneAbsorbanceMeasureOnUnknownSolutionOrItsDilutionAtStep3')
            ->update(['satisfaction_condition_key' => 'oneAbsorbanceMeasureOnUnknownSolutionOrItsDilutionAtStep4']);
        DB::table('simulation_constraint')
            ->where('satisfaction_condition_key', 'absorbanceMeasureOnUnknownSolutionOrItsDilutionShouldFallWithinSpectroRangeAtStep3')
            ->update(['satisfaction_condition_key' => 'absorbanceMeasureOnUnknownSolutionOrItsDilutionShouldFallWithinSpectroRangeAtStep4']);
        DB::table('simulation_constraint')
            ->where('satisfaction_condition_key', 'absorbanceMeasureOnUnknownSolutionOrItsDilutionShouldFallWithinLinearRangeAtStep3')
            ->update(['satisfaction_condition_key' => 'absorbanceMeasureOnUnknownSolutionOrItsDilutionShouldFallWithinLinearRangeAtStep4']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
};
