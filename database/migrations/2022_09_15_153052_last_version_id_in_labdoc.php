<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Log;
use App\LabdocVersioning;
use App\Labdoc;


return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('labdoc', function (Blueprint $table) {
            $table->bigInteger('last_id_version')
                ->nullable()
                ->comment('Last id version stored in the versioning files system, (the version is prior or equal to the current labdoc_data content)')
                ->after('last_edition');
        });
        $CHUNK_SIZE = 1000;
        Labdoc::chunk($CHUNK_SIZE, function ($labdocs) {
                foreach ($labdocs as $ld) {
                    try {
                        $labdocVersioning = new LabdocVersioning($ld->report->id_mission);
                        $id_version = $labdocVersioning->lastVersionIdInArchives($ld->id_labdoc);
                    } catch (\Exception $ex) {
                        Log::Error("Erreur lors du calcul du numéro de version sur le ld " . $ld->id_labdoc . " type " . $ld->type_labdoc);
                        continue;
                    }
                    if ($id_version !== 0) {
                        $ld->last_id_version = $id_version;
                        $ld->save();
                    }
                }
            });
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('labdoc', function($table) {
            $table->dropColumn('last_id_version');
        });
    }
};
