<?php

use App\Report;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE report MODIFY status ENUM('new','on','wait','arc','test','solution','tuto') NOT NULL DEFAULT 'new'");

        $CHUNK_SIZE = 10000;
        $counter = 0;
        Report::join('mission', 'report.id_mission', '=', 'mission.id_mission')
            ->withTrashed()
            ->where('mission.status', '=', 'tutorial')
            ->where('report.status', '=', '')
            ->chunkById($CHUNK_SIZE, function ($reports) use (&$counter){
                foreach ($reports as $report) {
                    $report->status = 'tuto';
                    $report->save();
                    $counter++;
                }
            });
        Log::info($counter." reports modified by migration.");
        error_log($counter." reports modified by migration.");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
};
