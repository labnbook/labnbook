<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Log;
use App\LabdocVersioning;
use App\Labdoc;


return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $CHUNK_SIZE = 1000;
        Labdoc::whereNull('last_id_version')
            ->whereNotNull('id_report')
            ->chunk($CHUNK_SIZE, function ($labdocs) {
                foreach ($labdocs as $ld) {
                    try {
                        if (!$ld->report) {
                            continue;
                        }
                        $labdocVersioning = new LabdocVersioning($ld->report->id_mission);
                        $id_version = $labdocVersioning->lastVersionIdInArchives($ld->id_labdoc);
                    } catch (\Exception $ex) {
                        Log::Error("Erreur {$ex->getMessage()} lors du calcul du numéro de version sur le ld " . $ld->id_labdoc . " type " . $ld->type_labdoc);
                        continue;
                    }
                    if ($id_version !== 0) {
                        $ld->last_id_version = $id_version;
                        $ld->save();
                    }
                }
            });
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
};
