<?php

use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mission', function($table) {
            $table->boolean('rubric_broadcast')
                ->nullable(false)
                ->default(0)
                ->comment('Allows/disallows students to view an unpublished rubric')
                ->after('auto_fit');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
};
