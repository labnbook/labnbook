<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Log;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $query = DB::table('labdoc as l')
            ->join('labdoc as l1', function ($join) {
                $join->on('l.id_ld_origin', '=', 'l1.id_ld_origin')
                     ->on('l.id_report', '=', 'l1.id_report')
                     ->on('l.id_labdoc', '<>', 'l1.id_labdoc');
            })
            ->whereColumn('l.last_edition', '<', 'l1.last_edition')
            ->orWhere(function ($query) {
                $query->whereNull('l.last_edition')
                      ->whereNotNull('l1.last_edition');
            })
            ->orWhere(function ($query) {
                $query->whereNull('l.last_edition')
                      ->whereNull('l1.last_edition')
                      ->whereColumn('l.id_labdoc', '<', 'l1.id_labdoc');
            })
            ->orWhere(function ($query) {
                $query->whereColumn('l.last_edition', '=', 'l1.last_edition')
                      ->whereColumn('l.id_labdoc', '<', 'l1.id_labdoc');
            });
        $count = $query->clone()->count();
        Log::Info("{$count} Duplicated Labdoc with same id_ld_origin to delete");
        $query->delete();
        Schema::table('labdoc', function (Blueprint $table) {
            $table->unique(['id_ld_origin', 'id_team_config']);
            $table->unique(['id_ld_origin', 'id_report']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
