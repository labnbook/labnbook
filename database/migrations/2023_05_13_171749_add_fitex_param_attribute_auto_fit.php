<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use \App\Labdoc;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $CHUNK_SIZE = 10000;
        $counter = 0;

        Labdoc::where('type_labdoc', 'dataset')
            ->where('labdoc_data', 'regexp', '.*<parameter>.*')
            ->chunkById($CHUNK_SIZE, function ($labdocs) use (&$counter) {
                foreach ($labdocs as $ld) {
                    $domDocument = new \DOMDocument();
                    @$domDocument->loadXML($ld->labdoc_data);
                    $paramElements = $domDocument->getElementsByTagName('parameter');
                    foreach ($paramElements as $param) {
                        $param->setAttribute('adjustable', "true");
                    }
                    if ($paramElements->length !== 0) {
                        $ld->labdoc_data = $domDocument->saveXML();
                        $ld->updateNoTimestamps(['labdoc_data' => $ld->labdoc_data]);
                        $counter++;
                    }
                }
            });
        Log::info($counter." labdocs modified by migration.");
        error_log($counter." labdocs modified by migration.");
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $CHUNK_SIZE = 10000;
        $counter = 0;

        Labdoc::where('type_labdoc', 'dataset')
            ->where('labdoc_data', 'regexp', '.*<parameter adjustable="true">.*')
            ->chunkById($CHUNK_SIZE, function ($labdocs) use (&$counter) {
                foreach ($labdocs as $ld) {
                    $domDocument = new \DOMDocument();
                    @$domDocument->loadXML($ld->labdoc_data);
                    $paramElements = $domDocument->getElementsByTagName('parameter');
                    foreach ($paramElements as $param) {
                        $param->removeAttribute('adjustable');
                    }
                    if ($paramElements->length !== 0) {
                        $ld->labdoc_data = $domDocument->saveXML();
                        $ld->updateNoTimestamps(['labdoc_data' => $ld->labdoc_data]);
                        $counter++;
                    }
                }
            });
        Log::info($counter." labdocs modified by migration rollback.");
        error_log($counter." labdocs modified by migration rollback.");
    }
};
