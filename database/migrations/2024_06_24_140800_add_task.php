<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('task', function (Blueprint $table) {
            $table->increments('id_task');
            $table->string('title', 200);
            $table->enum('state', ['inprogress', 'done']);
            $table->integer('estimated_duration')->nullable();
            $table->date('due_date')->nullable();
            $table->enum('priority', ['low', 'medium', 'high'])->default('medium');
            $table->integer('id_parent_task')->unsigned()->nullable();
            $table->unsignedInteger('id_report')->nullable();
            $table->unsignedInteger('position')->default(1);
            $table->dateTime('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->unsignedInteger('id_user_creator')->nullable(false);
            $table->dateTime('last_modified_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->unsignedInteger('id_user_last_editor')->nullable(false);

            $table->index('id_user_creator', 'task_creator_constraint');
            $table->index('id_user_last_editor', 'task_last_editor_constraint');
            
            $table->foreign('id_user_creator', 'task_creator_constraint')
                ->references('id_user')->on('user')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('id_user_last_editor', 'task_last_editor_constraint')
                ->references('id_user')->on('user')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task');
    }
};
