<?php

use App\Mission;
use App\ReportPart;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('simulation_constraint')
            ->where('satisfaction_condition_key', 'measureWithBuretShouldFollowDropLiquidInStep3')
            ->update(['satisfaction_condition_key' => 'measureWithBuretShouldBeInStep3']);
        DB::table('simulation_constraint')
            ->where('satisfaction_condition_key', 'measureWithphMeterShouldFollowDropLiquidInStep3')
            ->update(['satisfaction_condition_key' => 'measureWithphMeterShouldBeInStep3']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
};
