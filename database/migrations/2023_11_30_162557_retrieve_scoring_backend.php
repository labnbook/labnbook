<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

return new class extends Migration
{

    private function runQuery($raw)
    {
        Log::Debug("Executing query $raw");
        $ret = DB::statement(DB::raw($raw));
        Log::Debug("Statement returned : $ret");
        return $ret;
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->createTables();
        $this->importData();
        $this->cleanUp();
        $this->computeCriteriaGroupIdRubricAndIdAssessment();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }


    private function computeCriteriaGroupIdRubricAndIdAssessment()
    {
        Log::Debug("Computing id_rubric and id_assessment for criteria_group");
        $sql = <<<EOSQL
-- ADD new columns
ALTER TABLE criteria_group ADD COLUMN `id_rubric` int(10) unsigned DEFAULT NULL;
ALTER TABLE criteria_group ADD COLUMN  `id_assessment` int(10) unsigned DEFAULT NULL;
ALTER TABLE criteria_group ADD  CONSTRAINT `criteria_group_ibfk2` FOREIGN KEY (`id_rubric`) REFERENCES `rubric` (`id_rubric`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE criteria_group ADD  CONSTRAINT `criteria_group_ibfk3` FOREIGN KEY (`id_assessment`) REFERENCES `assessment` (`id_assessment`) ON DELETE CASCADE ON UPDATE CASCADE;
-- Compute id_rubric
UPDATE criteria_group cg SET id_rubric = (SELECT DISTINCT c1.id_rubric FROM criteria_group cg1 JOIN criterion c1 USING (id_criteria_group) WHERE cg1.id_criteria_group = cg.id_criteria_group);
-- Compute id_assessment
UPDATE criteria_group cg SET id_assessment = (SELECT DISTINCT c1.id_assessment FROM criteria_group cg1 JOIN criterion c1 USING (id_criteria_group) WHERE cg1.id_criteria_group = cg.id_criteria_group);
EOSQL;
        $this->runQuery($sql);
    }

    private function cleanUp()
    {
        Log::Debug("Running cleanup");
        $sql = <<<EOSQL
UNLOCK TABLES;
-- Remove empty criteria group
DELETE FROM criterion WHERE id_rubric IS NULL AND id_assessment IS NULL;
DELETE FROM criteria_group WHERE id_criteria_group IN (SELECT cg.id_criteria_group FROM criteria_group cg LEFT OUTER JOIN criterion c USING(id_criteria_group) WHERE c.id_criterion IS NULL);
-- Remove criteria_group without assessment or rubric
DELETE FROM criteria_group cg JOIN criterion c USING (id_criteria_group) WHERE id_rubric IS NULL AND id_assessment IS NULL;
-- Remove criterion without assessment or rubric
EOSQL;
        $this->runQuery($sql);
    }

    private function readZippedBackup($filename)
    {
        Log::Debug($filename);
        $zd = gzopen($filename, 'r');
        $data = "";
        while (!gzeof($zd)) {
            $data .= gzgets($zd);
        }
        gzclose($zd);
        Log::Debug($data);
        return $data;
    }

    private function importData()
    {
        Log::Debug("Importing data");
        // TODO if ./grilles_criteriees/, then run grilles_criteriees/gc backup_only_data
        // 1. make sure we are in source directory
        $gc_path = base_path() . '/grilles_criteriees/';
        if (!is_dir($gc_path)) {
            Log::Debug("$gc_path not found, not importing data");
            return;
        }
        // 2. backup gc data
        chdir($gc_path);
        shell_exec("./gc backup_only_data");
        $backups = glob($gc_path . '/gc_data_only_' . date('Ymd') . '*.sql.gz');
        // 3. retrieve the file gc_data_yyymmmdd_HHMM.sql.gz
        $last_backup = $backups[count($backups)-1];
        $this->runQuery($this->readZippedBackup($last_backup));
    }

    private function createTables()
    {
        Log::Debug("Creating table structure");
        $sql = <<<EOSQL
--
-- Dump récupéré par mysqldump --no-data sur les grilles critériées
-- Modification manuelles :
-- + Reordonnement des tables pour respecter les contraintes
-- + Transformation des bigint(20) en int(10) sur les id pour cohérence avec LabNbook
-- + Renommage des contraintes
-- + Ajout de contraintes pour lier aux tables LabNbook
--     + rubric : activity => mission
--     + assessment : production => report
--     + assessed_student : student => user
-- + Mise à jour des on delete : origin toujours set null, mais on ne garde pas de données innaccessible

--
-- Table structure for table `rubric`
--

DROP TABLE IF EXISTS `rubric`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rubric` (
  `id_rubric` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_activity` int(10) unsigned DEFAULT NULL,
  `name` char(255) DEFAULT NULL,
  `grading` double(8,2) DEFAULT NULL,
  `min_grade` double(8,2) DEFAULT NULL,
  `max_grade` double(8,2) DEFAULT NULL,
  `min_bonus` double(8,2) DEFAULT NULL,
  `max_bonus` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_rubric`),
  CONSTRAINT `rubric_ibfk1` FOREIGN KEY (`id_activity`) REFERENCES `mission` (`id_mission`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
--
-- Table structure for table `assessment`
--

DROP TABLE IF EXISTS `assessment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assessment` (
  `id_assessment` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_production` int(10) unsigned NOT NULL,
  `id_rubric_origin` int(10) unsigned DEFAULT NULL,
  `bonus` double(8,2) DEFAULT NULL,
  `bonus_comment` longtext DEFAULT NULL,
  `score` double(8,2) DEFAULT NULL,
  `score_comment` longtext DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT 0,
  `grading` double(8,2) DEFAULT NULL,
  `min_grade` double(8,2) DEFAULT NULL,
  `max_grade` double(8,2) DEFAULT NULL,
  `min_bonus` double(8,2) DEFAULT NULL,
  `max_bonus` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_assessment`),
  CONSTRAINT `assessment_ibfk1` FOREIGN KEY (`id_production`) REFERENCES `report` (`id_report`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;


DROP TABLE IF EXISTS `assessed_student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assessed_student` (
  `id_assessed_student` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_assessment` int(10) unsigned NOT NULL,
  `id_student` int(10) unsigned NOT NULL,
  `missing` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_assessed_student`),
  UNIQUE KEY `assessed_student_id_student_id_assessment_unique` (`id_student`,`id_assessment`),
  KEY `assessed_student_id_assessment_foreign` (`id_assessment`),
  CONSTRAINT `assessed_ibfk1` FOREIGN KEY (`id_assessment`) REFERENCES `assessment` (`id_assessment`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `assessed_ibfk2` FOREIGN KEY (`id_student`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `criteria_group`
--

DROP TABLE IF EXISTS `criteria_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `criteria_group` (
  `id_criteria_group` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_origin` int(10) unsigned DEFAULT NULL,
  `position` smallint(6) NOT NULL DEFAULT 0,
  `description` longtext DEFAULT NULL,
  `relative_weight` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_criteria_group`),
  KEY `criteria_group_id_origin_foreign` (`id_origin`),
  CONSTRAINT `criteria_group_ibfk1` FOREIGN KEY (`id_origin`) REFERENCES `criteria_group` (`id_criteria_group`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `criterion`
--

DROP TABLE IF EXISTS `criterion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `criterion` (
  `id_criterion` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_origin` int(10) unsigned DEFAULT NULL,
  `id_rubric` int(10) unsigned DEFAULT NULL,
  `id_assessment` int(10) unsigned DEFAULT NULL,
  `id_section` int(10) unsigned DEFAULT NULL COMMENT 'Not used yet',
  `id_criteria_group` int(10) unsigned DEFAULT NULL,
  `type` enum('descr','ob') NOT NULL DEFAULT 'descr',
  `position` smallint(6) NOT NULL DEFAULT 0,
  `description` longtext DEFAULT NULL,
  `relative_weight` double(8,2) DEFAULT NULL,
  `comment` longtext DEFAULT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_criterion`),
  KEY `criterion_id_rubric_foreign` (`id_rubric`),
  KEY `criterion_id_assessment_foreign` (`id_assessment`),
  KEY `criterion_id_criteria_group_foreign` (`id_criteria_group`),
  KEY `criterion_id_origin_foreign` (`id_origin`),
  CONSTRAINT `criterion_ibfk1` FOREIGN KEY (`id_origin`) REFERENCES `criterion` (`id_criterion`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `criterion_ibfk2` FOREIGN KEY (`id_criteria_group`) REFERENCES `criteria_group` (`id_criteria_group`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `criterion_ibfk3` FOREIGN KEY (`id_rubric`) REFERENCES `rubric` (`id_rubric`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `criterion_ibfk4` FOREIGN KEY (`id_assessment`) REFERENCES `assessment` (`id_assessment`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `descrob`
--

DROP TABLE IF EXISTS `descrob`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `descrob` (
  `id_descrob` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_origin` int(10) unsigned DEFAULT NULL,
  `id_criterion` int(10) unsigned DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `value` double(8,2) DEFAULT NULL,
  `position` smallint(6) NOT NULL DEFAULT 0,
  `selected` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_descrob`),
  KEY `descrob_id_criterion_foreign` (`id_criterion`),
  KEY `descrob_id_origin_foreign` (`id_origin`),
  CONSTRAINT `descrob_ibfk1` FOREIGN KEY (`id_criterion`) REFERENCES `criterion` (`id_criterion`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `descrob_ibfk2` FOREIGN KEY (`id_origin`) REFERENCES `descrob` (`id_descrob`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

EOSQL;
        $ret = $this->runQuery($sql);
    }
};
