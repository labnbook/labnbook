<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ClassExternalCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if (!Schema::hasColumn('class', 'ext_code')) {
            Schema::table('class', function (Blueprint $table) {
                $table->string('ext_code')->charset('ascii')->collation('ascii_bin')->nullable()
                    ->comment('Class identifier in external institution');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
