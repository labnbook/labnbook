<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('trace_action')->insert([
            ['id_action' => 55, 'action' => 'open_collaborative_dashboard', 'action_type' => 't_report'],
            ['id_action' => 56, 'action' => 'display_diffs', 'action_type' => 't_report']
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
