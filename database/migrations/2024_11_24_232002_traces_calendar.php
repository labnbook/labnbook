<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('trace_action')
            ->insert([
                'id_action' => 77,
                'action' => 'calendar_open_widget',
                'action_type' => 'information'
            ]);
        DB::table('trace_action')->insert([
            'id_action' => 78,
            'action' => 'calendar_close_widget',
            'action_type' => 'information'
        ]);
        DB::table('trace_action')->insert([
            'id_action' => 79,
            'action' => 'calendar_change_view',
            'action_type' => 'information'
        ]);
        DB::table('trace_action')->insert([
            'id_action' => 80,
            'action' => 'calendar_open_task',
            'action_type' => 'information'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('trace_action')
            ->where('id_action', 65)
            ->delete();
        DB::table('trace_action')
            ->where('id_action', 66)
            ->delete();
        DB::table('trace_action')
            ->where('id_action', 67)
            ->delete();
        DB::table('trace_action')
            ->where('id_action', 68)
            ->delete();
        DB::table('trace_action')
            ->where('id_action', 69)
            ->delete();
        DB::table('trace_action')
            ->where('id_action', 70)
            ->delete();
        DB::table('trace_action')
            ->where('id_action', 71)
            ->delete();
        DB::table('trace_action')
            ->where('id_action', 72)
            ->delete();
        DB::table('trace_action')
            ->where('id_action', 73)
            ->delete();
    }
};
