<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('labdoc', function (Blueprint $table) {
            $table->boolean('shared')
                  ->default(0)
                  ->after('draggable');
            $table->integer('id_team_config')
                  ->unsigned()
                  ->nullable()
                  ->default(null)
                  ->after('id_report');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
