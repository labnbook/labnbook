<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixZwiblerUrls extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $labdocs = \App\Labdoc::where('type_labdoc', 'drawing')->get();
        foreach ($labdocs as $ld) {
            $ld->labdoc_data = str_replace('"url":"../tool_zwibbler', '"url":"/tool_zwibbler', $ld->labdoc_data);
            $ld->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
