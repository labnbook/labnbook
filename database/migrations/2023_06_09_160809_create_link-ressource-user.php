<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('link_ressource_user',function (Blueprint $table){
            $table->unsignedInteger('id_ressource')->nullable(false);
            $table->unsignedInteger('id_user')->nullable(false);
            $table->foreign('id_ressource')->references('id_ressource')->on('ressource')->cascadeOnUpdate()->cascadeOnDelete();
            $table->foreign('id_user')->references('id_user')->on('user')->cascadeOnUpdate()->cascadeOnDelete();
            $table->unique(['id_ressource','id_user']);
            $table->timestamp('already_seen')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
