<?php

use App\Mission;
use App\ReportPart;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('simulation_constraint')
            ->where('satisfaction_condition_key', 'exactlyOneHomogenizeActionShouldFollowDiluteActionWithRelevantSolution')
            ->update(['satisfaction_condition_key' => 'exactlyOneHomogenizeActionShouldImmediatelyFollowDiluteActionWithRelevantSolution']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
};
