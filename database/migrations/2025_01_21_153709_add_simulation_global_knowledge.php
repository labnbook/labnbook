<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('simulation_global_knowledge', function(Blueprint $table) {
            $table->string('simulation_code', 32)->nullable(false);
            $table->increments('id_global_knowledge')->nullable(false);
            $table->longText('description')->nullable()->default(null);
            $table->smallInteger('position')->nullable()->default(null);
            $table->timestamps();
            $table->unique(['simulation_code', 'id_global_knowledge'], 'sim_global_knowledge');
        });

        if(!Schema::hasColumn('simulation_task_type','id_global_knowledge')) {
            Schema::table('simulation_task_type', function (Blueprint $table) {
                $table->unsignedInteger('id_global_knowledge')->nullable()->default(null);
                $table->foreign('id_global_knowledge')->references('id_global_knowledge')->on('simulation_global_knowledge')->nullOnDelete()->onUpdate('cascade');
            });
        }
        
        if(!Schema::hasColumn('simulation_link_learner_task','update_number')) {
            Schema::table('simulation_link_learner_task', function (Blueprint $table) {
                $table->smallInteger('update_number')->nullable()->default(null);
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
};
