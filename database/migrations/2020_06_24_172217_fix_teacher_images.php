<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixTeacherImages extends Migration
{
    private $pattern ='"../storage/';
    private $replace = '"/storage/';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->fixUrls(\App\Labdoc::where('type_labdoc', 'text'), 'labdoc_data');
        $this->fixUrls(\App\Labdoc::where('type_labdoc', 'text'), 'labdoc_data');
        $this->fixUrls(\App\ReportPart::whereRaw('1'), 'assignment');
        $this->fixUrls(\App\Mission::whereRaw('1'), 'assignment');
        $this->fixUrls(\App\Mission::whereRaw('1'), 'description');
    }

    private function fixUrls($query, $field)
    {
        foreach (['src', 'href'] as $tag) {
            $pat = $tag.'='.$this->pattern;
            $sql_pat = '%'.$pat.'%';
            $entries = $query->where($field, 'like', $sql_pat)->get();
            foreach ($entries as $entry) {
                $entry->$field = str_replace($pat, $tag.'='.$this->replace, $entry->$field);
                $entry->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
