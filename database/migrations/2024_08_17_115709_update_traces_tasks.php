<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('trace_action')
            ->where('id_action', 66)
            ->delete();
        DB::table('trace_action')
            ->where('id_action', 73)
            ->delete();
        DB::table('trace_action')
            ->insert([
                'id_action' => 74,
                'action' => 'task_move',
                'action_type' => 'information'
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('trace_action')->insert([
            'id_action' => 66,
            'action' => 'task_close_widget',
            'action_type' => 'information'
        ]);
        DB::table('trace_action')->
            insert([
                'id_action' => 73,
                'action' => 'task_search',
                'action_type' => 'information'
            ]);
        DB::table('trace_action')->
            where('id_action', 74)->
            delete();
    }
};
