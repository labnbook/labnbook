<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use \App\Trace;

class FixTraceModifyLDEnterReportIssue743 extends Migration
{
    protected const CHUNK_SIZE = 10000;

    protected function getDate($time)
    {
        return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $time);
    }
    protected function diffSeconds($t1, $t2)
    {
        return $this->getDate($t1)->diffInSeconds($this->getDate($t2));
    }

    protected function removeEnterReportAfterEditLD()
    {
        $ID_TRACES = [Trace::EDIT_LD, Trace::ENTER_REPORT];
        $traces = DB::Table('trace')
            ->where('action_time', '>=', '2021-11-18')
            ->whereIn('id_action', $ID_TRACES)
            ->orderBy('id_user')
            ->orderBy('id_trace')
            ->chunk(self::CHUNK_SIZE, function ($traces) {
                static $last = null;
                foreach ($traces as $t) {
                    if (
                        $last &&
                        $last->id_action == Trace::EDIT_LD &&
                        $last->id_user == $t->id_user &&
                        $t->id_action == Trace::ENTER_REPORT &&
                        $this->diffseconds($last->action_time, $t->action_time) < 3 &&
                        json_decode($last->attributes)->type === 'text'
                    ) {
                        Log::Info("Deleting trace {$t->id_trace} see issue #743");
                        Log::Debug(json_encode($t, JSON_PRETTY_PRINT));
                        Log::Debug(json_encode($last, JSON_PRETTY_PRINT));
                        DB::Table('trace')
                            ->where('id_trace', $t->id_trace)
                            ->delete();
                    }
                    $last = $t;
                }
            });
    }

    protected function removeFollowAndEnterDuringWork()
    {
        $bad_traces = [Trace::ENTER_REPORT, Trace::TEACHER_FOLLOW_REPORT];
        $traces = DB::Table('trace')
            ->whereNotNull('id_report')
            ->where('action_time', '>=', '2021-11-18')
            ->where('id_action', '>', Trace::SUBMIT_REPORT)
            ->where('id_action', '<=', Trace::HARD_DELETE_LD)
            ->orWhere('id_action', Trace::TEACHER_FOLLOW_REPORT)
            ->orderBy('id_user')
            ->orderBy('id_trace')
            ->chunk(self::CHUNK_SIZE, function ($traces) use ($bad_traces) {
                static $last = null;
                foreach ($traces as $t) {
                    if (
                        $last &&
                        $last->id_user == $t->id_user &&
                        $last->id_report == $t->id_report &&
                        in_array($t->id_action, $bad_traces) &&
                        !in_array($last->id_action, $bad_traces) &&
                        $this->diffseconds($last->action_time, $t->action_time) < 3
                    ) {
                        Log::Info("Deleting trace {$t->id_trace} see issue #743");
                        Log::Debug(json_encode($t, JSON_PRETTY_PRINT));
                        Log::Debug(json_encode($last, JSON_PRETTY_PRINT));
                        DB::Table('trace')
                            ->where('id_trace', $t->id_trace)
                            ->delete();
                    }
                    $last = $t;
                }
            });
    }

    protected function changeTraceTeacherForTestUser()
    {
        DB::Table('trace')
            ->where('id_user', config('labnbook.id_user_test'))
            ->where('id_mission', config('labnbook.id_mission_test'))
            ->where('id_action', Trace::TEACHER_TEST_MISSION)
            ->update(['id_action' => Trace::ENTER_REPORT]);
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->removeEnterReportAfterEditLD();
        $this->removeFollowAndEnterDuringWork();
        $this->changeTraceTeacherForTestUser();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
