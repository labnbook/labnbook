<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use \App\Trace;

class FixTraceRecoverLdInsteadOfValidateLdIssue738 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $ID_TRACES = [Trace::EDIT_LD, Trace::MODIFY_LD, Trace::VALIDATE_LD, Trace::OPEN_LD_VERSIONING, Trace::RECOVER_LD];
        $CHUNK_SIZE = 1000;
        $traces = DB::Table('trace')
            ->where('action_time', '>', '2022-03-03 17')
            ->whereIn('id_action', $ID_TRACES)
            ->orderBy('id_user')
            ->orderBy('id_labdoc')
            ->orderBy('id_trace')
            ->chunk($CHUNK_SIZE, function ($traces) {
                static $last = null;
                foreach ($traces as $t) {
                    if (
                        $last &&
                        $last->id_user == $t->id_user &&
                        $last->id_labdoc == $t->id_labdoc &&
                        $last->id_action != Trace::OPEN_LD_VERSIONING &&
                        $t->id_action == Trace::RECOVER_LD
                    ) {
                        DB::Table('trace')
                            ->where('id_trace', $t->id_trace)
                            ->update(['id_action' => Trace::VALIDATE_LD]);
                    }
                    $last = $t;
                }
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
