<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixTinymceImgSrc extends Migration
{
    private $pattern ='"../files/';
    private $pattern_ld ='"/files/';
    private $replace = '"/storage/';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->fixUrls(\App\Labdoc::where('type_labdoc', 'text'), 'labdoc_data', $this->pattern_ld);
        $this->fixUrls(\App\Labdoc::where('type_labdoc', 'text'), 'labdoc_data', $this->pattern);
        $this->fixUrls(\App\ReportPart::whereRaw('1'), 'assignment', $this->pattern);
        $this->fixUrls(\App\Mission::whereRaw('1'), 'assignment', $this->pattern);
        $this->fixUrls(\App\Mission::whereRaw('1'), 'description', $this->pattern);
    }

    private function fixUrls($query, $field, $pattern)
    {
        foreach (['src', 'href'] as $tag) {
            $pat = $tag.'='.$pattern;
            $sql_pat = '%'.$pat.'%';
            $entries = $query->where($field, 'like', $sql_pat)->get();
            foreach ($entries as $entry) {
                $entry->$field = str_replace($pat, $tag.'='.$this->replace, $entry->$field);
                $entry->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
