<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('link_task_user', function (Blueprint $table) {
            $table->unsignedInteger('id_task');
            $table->unsignedInteger('id_user');
            $table->primary(['id_task', 'id_user']);

            $table->foreign('id_task', 'link_task_user_task_constraint')
                ->references('id_task')->on('task')
                ->onDelete('cascade');

            $table->foreign('id_user', 'link_task_user_user_constraint')
                ->references('id_user')->on('user')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('link_task_user');
    }

};
