<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('simulation_constraint', function(Blueprint $table) {
            $table->unsignedInteger('id_task_type')->nullable()->change();
        });

        Schema::table('simulation_feedback', function(Blueprint $table) {
            $table->unsignedInteger('id_task_type')->nullable()->change();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
};
