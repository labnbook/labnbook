<?php

use Illuminate\Database\Migrations\Migration;
use \App\Labdoc;
use \App\ImagesExtractor;
use Illuminate\Support\Facades\DB;

class ExtractZwibblerImages extends Migration
{
    private $pattern ='"../files/';
    private $pattern_ld ='"/files/';
    private $replace = '"/storage/';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $CHUNK_SIZE = 100;
        Labdoc::where('type_labdoc', 'drawing')
            ->whereRaw('LENGTH(labdoc_data) > 10000')
            ->chunk($CHUNK_SIZE, function ($labdocs) {
                foreach ($labdocs as $ld) {
                    try {
                        $images = new ImagesExtractor($ld->id_mission, $ld->id_labdoc, $ld->labdoc_data);
                    } catch (\Exception $ex) {
                        Log::Error("Erreur lors de l'extraction d'image sur le ld " . $ld->id_labdoc . " type " . $ld->type_labdoc);
                        continue;
                    }
                    if ($images->getCount() > 0) {
                        Log::Info($images->getCount() . " Images extraites pour le LD " . $ld->id_labdoc);
                        $ld->labdoc_data = $images->getContent();
                        $ld->save();
                    }
                }
            });
        DB::statement('OPTIMIZE TABLE labdoc');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
