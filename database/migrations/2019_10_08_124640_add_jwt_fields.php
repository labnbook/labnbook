<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddJwtFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('institution', 'secret')) {
            Schema::table('institution', function (Blueprint $table) {
                $table->string('secret')->charset('ascii')->collation('ascii_bin')->nullable()
                    ->comment('secret key for cyphering the initial JWT messages');
            });
        }
        if(!Schema::hasTable('link_ext_user_inst')){
            Schema::create('link_ext_user_inst', function (Blueprint $table) {
                $table->integer('id_inst');
                $table->foreign('id_inst')
                      ->references('id_inst')->on('institution')
                      ->onDelete('cascade');
                $table->string('id_user_ext');
                $table->unsignedInteger('id_user');
                $table->foreign('id_user')
                      ->references('id_user')->on('user')
                      ->onDelete('cascade');
                $table->timestamps();
                $table->primary(['id_inst', 'id_user_ext']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
