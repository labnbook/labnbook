<?php

use App\ReportPart;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;


return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // AFAIK Laravel does not handle enum type modification
        DB::statement("ALTER TABLE `labdoc` CHANGE COLUMN `type_labdoc` `type_labdoc` ENUM('text', 'drawing', 'dataset', 'procedure', 'code') NOT NULL;");
        // Add a column 'code' to report_part
        Schema::table('report_part', function (Blueprint $table) {
            $table->tinyInteger('code')->unsigned()->default(1)->after('procedure');
        });
        // Fill code column with 0
        ReportPart::query()->update(['code'=> 0]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
