<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('trace_action')->insert([
            ['id_action' => 58, 'action' => 'scoring_save_grid', 'action_type' => 't_mission'],
            ['id_action' => 59, 'action' => 'scoring_save_eval', 'action_type' => 't_report'],
            ['id_action' => 60, 'action' => 'scoring_publish_eval', 'action_type' => 't_report'],
            ['id_action' => 61, 'action' => 'scoring_edit_eval', 'action_type' => 't_report'],
            ['id_action' => 62, 'action' => 'scoring_view_eval', 'action_type' => 'information'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
