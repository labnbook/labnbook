<?php

use App\Mission;
use App\ReportPart;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('trash_var');
        Schema::dropIfExists('comment_var');
        Schema::dropIfExists('scoring_var');
        Schema::dropIfExists('ressource_var');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
};
