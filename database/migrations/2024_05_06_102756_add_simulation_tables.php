<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('simulation_task_type', function(Blueprint $table) {
            $table->increments('id_task_type')->nullable(false);
            $table->unsignedInteger('id_overtask_type')->nullable()->default(null);
            $table->string('simulation_code', 32)->nullable(false);
            $table->longText('description')->nullable()->default(null);
            $table->smallInteger('learning_goal')->nullable()->default(null);
            $table->timestamps();
            $table->unique(['simulation_code', 'id_task_type']);
        });

        Schema::create('simulation_constraint', function(Blueprint $table) {
            $table->increments('id_constraint')->nullable(false);
            $table->string('simulation_code', 32)->nullable(false);
            $table->unsignedInteger('id_task_type')->nullable(false);
            $table->unsignedInteger('id_feedback')->nullable(false);
            $table->longText('description')->nullable()->default(null);
            $table->smallInteger('position')->nullable()->default(null);
            $table->text('satisfaction_condition_key')->nullable()->default(null);
            $table->longText('constraint_message')->nullable()->default(null);
            $table->boolean('simulation_enabled')->nullable(false)->default(false);
            $table->timestamps();
            $table->foreign('id_task_type')->references('id_task_type')->on('simulation_task_type')->onDelete('cascade')->onUpdate('cascade');
            $table->unique(['simulation_code', 'id_constraint']);
        });

        Schema::create('simulation_link_relevance_constraint', function(Blueprint $table) {
            $table->string('simulation_code', 32)->nullable(false);
            $table->unsignedInteger('id_constraint')->nullable(false);
            $table->unsignedInteger('id_relevance_constraint')->nullable(false);
            $table->foreign('id_constraint', 'sim_constraint_foreign')->references('id_constraint')->on('simulation_constraint')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('id_relevance_constraint', 'sim_relevance_constraint_foreign')->references('id_constraint')->on('simulation_constraint')->onDelete('cascade')->onUpdate('cascade');
            $table->primary(['simulation_code', 'id_constraint', 'id_relevance_constraint'], 'sim_relevance_constraint');
        });
        
        Schema::create('simulation_link_learner_task', function(Blueprint $table) {
            $table->string('simulation_code', 32)->nullable(false);
            $table->unsignedInteger('id_learner')->nullable(false);
            $table->unsignedInteger('id_task_type')->nullable(false);
            $table->smallInteger('feedback_level')->nullable()->default(null);
            $table->float('knowledge', 10, 9)->nullable()->default(null);
            $table->foreign('id_learner')->references('id_user')->on('user')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('id_task_type')->references('id_task_type')->on('simulation_task_type')->onDelete('cascade')->onUpdate('cascade');
            $table->primary(['simulation_code', 'id_learner', 'id_task_type'], 'sim_learner_task');
        });

        Schema::create('simulation_feedback', function(Blueprint $table) {
            $table->increments('id_feedback')->nullable(false);
            $table->string('simulation_code', 32)->nullable(false);
            $table->unsignedInteger('id_task_type')->nullable(false);
            $table->unsignedInteger('id_constraint')->nullable()->default(null);
            $table->longText('theoretical_feedback')->nullable()->default(null);
            $table->longText('technological_feedback')->nullable()->default(null);
            $table->longText('technical_feedback')->nullable()->default(null);
            $table->longText('correction_feedback')->nullable()->default(null);
            $table->timestamps();
            $table->foreign('id_constraint')->references('id_constraint')->on('simulation_constraint')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('id_task_type')->references('id_task_type')->on('simulation_task_type')->onDelete('cascade')->onUpdate('cascade');
            $table->unique(['simulation_code', 'id_task_type', 'id_constraint'], 'sim_feedback__code_id_task_id_constraint_unique');
        });

        Schema::create('simulation_trace', function(Blueprint $table) {
            $table->bigIncrements('id_simulation_trace')->nullable(false);
            $table->string('simulation_code', 32)->nullable(false);
            $table->string('trace_action', 32)->nullable(false);
            $table->timestamp('action_time')->nullable(false)->useCurrent();
            $table->unsignedInteger('id_user')->nullable(false);
            $table->unsignedInteger('id_mission')->nullable()->default(null);
            $table->unsignedInteger('id_report')->nullable()->default(null);
            $table->unsignedInteger('id_labdoc')->nullable(false);
            $table->jsonb('attributes');
            $table->timestamps();
        });

        Schema::table('simulation_constraint', function($table) {
            $table->foreign('id_feedback')->references('id_feedback')->on('simulation_feedback')->onDelete('cascade')->onUpdate('cascade');
        });
        Schema::table('simulation_task_type', function($table) {
            $table->foreign('id_overtask_type')->references('id_task_type')->on('simulation_task_type')->onDelete('set null')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
};
