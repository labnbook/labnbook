<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Scoring\Criterion;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $criterions = Criterion::whereNull('id_criteria_group')->get();
        foreach ($criterions as $c) {
            $assessment = $c->assessment;
            if ($c->origin) {
                foreach ($assessment->criteria_group as $cg) {
                    if ((int)$cg->id_origin === (int)$c->origin->id_criteria_group) {
                        $c->id_criteria_group = $cg->id_criteria_group;
                        $c->update();
                        break;
                    }
                }
            } else {
                Log::Error("Criterion {$c->id_criterion} has no origin and no criteria_group");
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rubric', function (Blueprint $table) {
            //
        });
    }
};
