<?php

use App\Mission;
use App\ReportPart;
use App\SimulationTaskType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('simulation_task_type','position')) {
            Schema::table('simulation_task_type', function (Blueprint $table) {
                $table->smallInteger('position')->nullable()->default(null);
            });
        }

        $titrabTaskTypes = SimulationTaskType::where('simulation_code', 'titrab')
            ->orderBy('id_task_type')->get();
        foreach ($titrabTaskTypes as $index => $titrabTaskType) {
            $titrabTaskType->position = $index*10+10;
            $titrabTaskType->save();
        }
        $spectroTaskTypes = SimulationTaskType::where('simulation_code', 'dosage-spectro')
            ->orderBy('id_task_type')->get();
        foreach ($spectroTaskTypes as $index => $spectroTaskType) {
            $spectroTaskType->position = $index*10+10;
            $spectroTaskType->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
};
