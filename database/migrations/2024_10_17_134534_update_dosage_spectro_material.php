<?php

use App\Labdoc;
use App\Mission;
use App\ReportPart;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $labdocs = Labdoc::join('report_part', 'report_part.id_report_part', '=', 'labdoc.id_report_part')
            ->join('mission', 'report_part.id_mission', '=', 'mission.id_mission')
            ->where('mission.simulation_code', 'dosage-spectro')
            ->where('labdoc.type_labdoc', 'procedure')
            ->get(['labdoc.*']);
        foreach ($labdocs as $labdoc) {
            $data = json_decode($labdoc->labdoc_data, true);
            $spectroIdx = collect($data['materialList']['listItems'])->search(function($item) {
                return count(collect($item['properties'])->where('name', '=', 'shortName')->whereIn('value', 'spectrophotometer')) > 0;
            });
            $epsilonProperties = collect($data['materialList']['listItems'][$spectroIdx]['properties'])->first(function($property) {
                return $property['name'] === 'epsilon';
            });
            $epsilonPropertyIdx = collect($data['materialList']['listItems'][$spectroIdx]['properties'])->search(function($property) {
                return $property['name'] === 'epsilon';
            });
            if ($spectroIdx !== null && $epsilonPropertyIdx !== null) {
                array_splice($data['materialList']['listItems'][$spectroIdx]['properties'], $epsilonPropertyIdx, 1);
                $stockSolution = collect($data['materialList']['listItems'])->first(function($item) {
                    return count(collect($item['properties'])->where('name', '=', 'shortName')->whereIn('value', 'stockSolution')) > 0;
                });
                $stockSolutionIdx = collect($data['materialList']['listItems'])->search(function($item) {
                    return count(collect($item['properties'])->where('name', '=', 'shortName')->whereIn('value', 'stockSolution')) > 0;
                });
                $newEpsilonId = strval(collect($stockSolution['properties'])->max('id') + 1);
                $epsilonProperties['id'] = $newEpsilonId;
                $data['materialList']['listItems'][$stockSolutionIdx]['properties'][] = $epsilonProperties;
                $labdoc->labdoc_data = json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
                $labdoc->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
};
