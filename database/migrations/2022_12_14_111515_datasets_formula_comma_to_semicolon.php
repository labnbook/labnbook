<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use \App\Labdoc;
use Illuminate\Support\Facades\Log;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $CHUNK_SIZE = 10000;
        $counter = 0;
        Labdoc::where('type_labdoc', 'dataset')
            ->where('labdoc_data', 'like', '%<formula>%')
            ->chunkById($CHUNK_SIZE, function ($labdocs) use (&$counter){
                foreach ($labdocs as $ld) {
                    $domDocument = new \DOMDocument();
                    @$domDocument->loadXML($ld->labdoc_data);
                    $formulaElements = $domDocument->getElementsByTagName('formula');
                    $has_comma = false;
                    foreach ($formulaElements as $formula) {
                        if (str_contains($formula->nodeValue, ',')) {
                            $has_comma = true;
                            $formula->nodeValue = str_replace(",", ";", $formula->nodeValue);
                        }
                    }
                    if ($formulaElements->length !== 0 && $has_comma) {
                        $ld->labdoc_data = $domDocument->saveXML();
                        $ld->updateNoTimestamps(['labdoc_data' => $ld->labdoc_data]);
                        $counter++;
                    }
                }
            });
        Log::info($counter." labdocs modified by migration.");
        error_log($counter." labdocs modified by migration.");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
