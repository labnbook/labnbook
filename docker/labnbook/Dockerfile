FROM debian:bullseye

ARG PHPVER=8.1
ENV PHPVER=$PHPVER

# Install sury repositoies for php 8
RUN apt-get update && \
    apt-get -y install apt-transport-https lsb-release ca-certificates curl && \
    curl -sSLo /usr/share/keyrings/deb.sury.org-php.gpg https://packages.sury.org/php/apt.gpg && \
    sh -c 'echo "deb [signed-by=/usr/share/keyrings/deb.sury.org-php.gpg] https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list'

RUN  apt-get update && apt-get install -y \
    apache2 \
    apt-transport-https\
    chromium-driver\
    chromium-l10n \
    chromium\
    cron \
    curl \
    git\
    gnupg\
    libldap2-dev\
    libxml2-dev\
    libzip-dev\
    mariadb-client\
    php$PHPVER \
    php$PHPVER-bcmath \
    php$PHPVER-ctype \
    php$PHPVER-curl \
    php$PHPVER-fpm \
    php$PHPVER-ldap \
    php$PHPVER-mbstring \
    php$PHPVER-mysqli \
    php$PHPVER-pdo-mysql \
    php$PHPVER-tokenizer \
    php$PHPVER-xml \
    php$PHPVER-zip \
    sudo\
    unzip\
    vim-nox \
    wget \
    php$PHPVER-xdebug
    
# Install YARN & nodejs
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list
RUN curl -sL https://deb.nodesource.com/setup_16.x | bash -
RUN apt-get update
RUN apt-get install -y yarn nodejs

# Install composer
COPY install_composer.sh /bin
RUN /bin/install_composer.sh
RUN mv /usr/local/bin/composer.phar /usr/local/bin/composer

COPY labnbook.conf /etc/apache2/sites-enabled/000-default.conf
COPY labnbook.ini /etc/php/$PHPVER/fpm/conf.d/00-labnbook.ini
RUN rm /etc/php/$PHPVER/cli/conf.d/20-xdebug.ini
RUN sed -i -e 's/80/8000/g' /etc/apache2/ports.conf

RUN mkdir -p /var/www/
RUN chown -R www-data /var/www/

RUN mkdir -p /run/php

RUN a2enconf php$PHPVER-fpm.conf
RUN a2enmod rewrite
RUN a2enmod proxy
RUN a2enmod proxy_fcgi

# Dbus issue: https://github.com/SeleniumHQ/docker-selenium/issues/87
# Try to fix chrome issues
ENV DBUS_SESSION_BUS_ADDRESS=/dev/null
RUN echo 'Defaults env_keep += "DBUS_SESSION_BUS_ADDRESS"' >> /etc/sudoers
RUN echo "DBUS_SESSION_BUS_ADDRESS=/dev/null" >> /etc/environment

COPY cronjob /etc/cron.d/labnbook

RUN  chmod 0644 /etc/cron.d/labnbook

RUN touch /var/log/cron.log

RUN chown 33:33  /var/log/cron.log

WORKDIR /var/www/labnbook

EXPOSE 8000
#RUN sudo -u www-data composer global require laravel/installer
#RUN ln -s /var/www/.composer/vendor/bin/laravel /usr/local/bin/laravel
COPY entrypoint.sh /
ENTRYPOINT ["/entrypoint.sh"]
