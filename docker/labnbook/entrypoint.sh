#!/bin/bash
./lnb perms
./lnb update
if [ "$APP_DEBUG" == "true" ]
then
    ./lnb dev &
else
    # TODO find a solution less brutal to avoid xdebug in production
    rm /etc/php/7.3/fpm/conf.d/20-xdebug.ini
    sed -i -e '/xdebug/d' /etc/php/7.3/fpm/conf.d/00-labnbook.ini
fi
cron &
tail -f /var/log/cron.log &
php-fpm$PHPVER -D
exec apache2ctl -DFOREGROUND
