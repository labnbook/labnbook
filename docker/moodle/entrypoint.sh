#!/bin/bash

grunt_watch() {
    npm install
    cd mod/labnbook
    grunt watch
}

cron &
tail -f /var/log/cron.log &
grunt_watch &
exec apache2ctl -DFOREGROUND
